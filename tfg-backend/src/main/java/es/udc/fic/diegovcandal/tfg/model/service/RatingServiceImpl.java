package es.udc.fic.diegovcandal.tfg.model.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.FollowingListTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.ReviewTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.ReviewTitleLikeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitleLike;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ReviewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;

@Service
@Transactional
public class RatingServiceImpl implements RatingService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private ReviewTitleDAO reviewTitleDAO;

	@Autowired
	private ReviewTitleLikeDAO reviewTitleLikeDAO;

	@Autowired
	private FollowingListTitleDAO followingListTitleDAO;

	@Override
	public void rateTitle(Long userId, Long titleId, byte rating) throws IncorrectOperationException,
			UserNotFoundException, TitleNotFoundException, IncorrectRatingException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (reviewTitle.isPresent()) {
			throw new IncorrectOperationException();
		}

		if (rating < 0 || rating > 10) {
			throw new IncorrectRatingException();
		}

		ReviewTitle newRating = new ReviewTitle(title.get(), user.get(), rating, false, null, null, 0, LocalDate.now());

		float oldAvgRating = title.get().getAvgRating();
		int timesRated = title.get().getTimesRated();

		title.get().setAvgRating(((oldAvgRating * timesRated) + rating) / (timesRated + 1));
		title.get().setTimesRated(timesRated + 1);

		updateFollowingListRating(rating, false, title, user);

		titleDAO.save(title.get());
		reviewTitleDAO.save(newRating);
	}

	@Override
	public void editTitleRating(Long userId, Long titleId, byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (!reviewTitle.isPresent()) {
			throw new ReviewNotFoundException();
		}

		if (rating < 0 || rating > 10) {
			throw new IncorrectRatingException();
		}

		byte oldRating = reviewTitle.get().getRating();
		float oldAvgRating = title.get().getAvgRating();
		int timesRated = title.get().getTimesRated();

		title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating + rating) / timesRated);
		reviewTitle.get().setRating(rating);

		updateFollowingListRating(rating, false, title, user);

		reviewTitleDAO.save(reviewTitle.get());
		titleDAO.save(title.get());
	}

	@Override
	public void deleteTitleRating(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (!reviewTitle.isPresent()) {
			throw new ReviewNotFoundException();
		}

		byte oldRating = reviewTitle.get().getRating();
		float oldAvgRating = title.get().getAvgRating();
		int timesRated = title.get().getTimesRated();

		if (timesRated <= 1) {
			title.get().setAvgRating(0);
			title.get().setTimesRated(0);
		} else {
			title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating) / (timesRated - 1));
			title.get().setTimesRated(timesRated - 1);
		}

		deleteFollowingListRating(title, user);

		reviewTitleDAO.delete(reviewTitle.get());
		titleDAO.save(title.get());
	}

	@Override
	public void addLike(Long userId, Long reviewId)
			throws IncorrectOperationException, UserNotFoundException, ReviewNotFoundException {

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findById(reviewId);

		if (!reviewTitle.isPresent()) {
			throw new ReviewNotFoundException();
		}

		if (!reviewTitle.get().isContainsReview()) {
			throw new IncorrectOperationException();
		}

		Optional<ReviewTitleLike> reviewTitleLike = reviewTitleLikeDAO.findByReviewIdAndUserId(reviewTitle.get(),
				user.get());

		if (reviewTitleLike.isPresent()) {
			throw new IncorrectOperationException();
		}

		ReviewTitleLike newReviewTitleLike = new ReviewTitleLike(reviewTitle.get(), user.get());

		reviewTitle.get().setLikeCount(reviewTitle.get().getLikeCount() + 1);

		reviewTitleLikeDAO.save(newReviewTitleLike);
		reviewTitleDAO.save(reviewTitle.get());
	}

	@Override
	public void removeLike(Long userId, Long reviewId)
			throws IncorrectOperationException, UserNotFoundException, ReviewNotFoundException {

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findById(reviewId);

		if (!reviewTitle.isPresent()) {
			throw new ReviewNotFoundException();
		}

		Optional<ReviewTitleLike> reviewTitleLike = reviewTitleLikeDAO.findByReviewIdAndUserId(reviewTitle.get(),
				user.get());

		if (!reviewTitleLike.isPresent()) {
			throw new ReviewNotFoundException();
		}

		reviewTitle.get().setLikeCount(reviewTitle.get().getLikeCount() - 1);

		reviewTitleLikeDAO.delete(reviewTitleLike.get());
		reviewTitleDAO.save(reviewTitle.get());
	}

	@Override
	public ReviewTitle addNewReview(Long userId, Long titleId, String reviewTitle, String content, byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		if (rating < 0 || rating > 10) {
			throw new IncorrectRatingException();
		}

		Optional<ReviewTitle> review = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (!review.isPresent()) {

			ReviewTitle newRewTitle = new ReviewTitle(title.get(), user.get(), rating, true, reviewTitle, content, 0,
					LocalDate.now());

			float oldAvgRating = title.get().getAvgRating();
			int timesRated = title.get().getTimesRated();

			title.get().setAvgRating(((oldAvgRating * timesRated) + rating) / (timesRated + 1));
			title.get().setTimesRated(timesRated + 1);

			updateFollowingListRating(rating, true, title, user);

			titleDAO.save(title.get());
			return reviewTitleDAO.save(newRewTitle);

		} else {

			byte oldRating = review.get().getRating();

			review.get().setTitle(reviewTitle);
			review.get().setContent(content);
			review.get().setRating(rating);
			review.get().setContainsReview(true);

			float oldAvgRating = title.get().getAvgRating();
			int timesRated = title.get().getTimesRated();

			title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating + rating) / timesRated);

			updateFollowingListRating(rating, true, title, user);

			titleDAO.save(title.get());
			return reviewTitleDAO.save(review.get());
		}

	}

	@Override
	public void editReview(Long userId, Long titleId, String reviewTitle, String content, byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> review = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (!review.isPresent()) {
			throw new ReviewNotFoundException();
		}

		if (rating < 0 || rating > 10) {
			throw new IncorrectRatingException();
		}

		byte oldRating = review.get().getRating();

		review.get().setTitle(reviewTitle);
		review.get().setContent(content);
		review.get().setRating(rating);
		review.get().setContainsReview(true);

		float oldAvgRating = title.get().getAvgRating();
		int timesRated = title.get().getTimesRated();

		title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating + rating) / timesRated);

		updateFollowingListRating(rating, review.get().isContainsReview(), title, user);

		reviewTitleDAO.save(review.get());
		titleDAO.save(title.get());

	}

	@Override
	public void deleteReview(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> review = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (!review.isPresent()) {
			throw new ReviewNotFoundException();
		}

		byte oldRating = review.get().getRating();
		float oldAvgRating = title.get().getAvgRating();
		int timesRated = title.get().getTimesRated();

		if (timesRated <= 1) {
			title.get().setAvgRating(0);
			title.get().setTimesRated(0);
		} else {
			title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating) / (timesRated - 1));
			title.get().setTimesRated(timesRated - 1);
		}

		deleteFollowingListRating(title, user);

		titleDAO.save(title.get());
		reviewTitleDAO.delete(review.get());

	}

	@Override
	@Transactional(readOnly = true)
	public ReviewTitle getReviewTitle(Long userId, Long titleId) throws UserNotFoundException, TitleNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user.get());

		if (!reviewTitle.isPresent()) {
			return null;
		}

		return reviewTitle.get();
	}

	@Override
	@Transactional(readOnly = true)
	public Block<ReviewTitle> getReviews(Long titleId, Long userId, int pageNumber, int size)
			throws TitleNotFoundException, UserNotFoundException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Page<ReviewTitle> page = reviewTitleDAO.findByTitleIdAndContainsReviewTrue(title.get(),
				PageRequest.of(pageNumber, size));

		if (userId != null) {

			Optional<User> user = userDao.findById(userId);

			if (!user.isPresent()) {
				throw new UserNotFoundException(userId);
			}

			for (ReviewTitle reviewTitle : page.getContent()) {
				Optional<ReviewTitleLike> reviewTitleLike = reviewTitleLikeDAO.findByReviewIdAndUserId(reviewTitle,
						user.get());

				if (reviewTitleLike.isPresent()) {
					reviewTitle.setLiked(true);
				}
			}
		}

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());
	}

	private void updateFollowingListRating(byte rating, boolean hasReview, Optional<Title> title, Optional<User> user) {

		Optional<FollowingListTitle> listTitleOptional = followingListTitleDAO.findByTitleIdAndListId(title.get(),
				user.get().getFollowingList());

		if (listTitleOptional.isPresent()) {
			listTitleOptional.get().setHasReview(hasReview);
			listTitleOptional.get().setRating(rating);
			followingListTitleDAO.save(listTitleOptional.get());
		}

	}

	private void deleteFollowingListRating(Optional<Title> title, Optional<User> user) {
		Optional<FollowingListTitle> listTitleOptional = followingListTitleDAO.findByTitleIdAndListId(title.get(),
				user.get().getFollowingList());

		if (listTitleOptional.isPresent()) {
			listTitleOptional.get().setHasReview(false);
			listTitleOptional.get().setRating((byte) -1);
			followingListTitleDAO.save(listTitleOptional.get());
		}
	}

}
