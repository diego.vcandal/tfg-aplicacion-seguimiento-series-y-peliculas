package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class CustomListTranslatedTitleDTO {

	private Long id;
	private int position;
	private Long idLocale;
	private String originalTitle;
	private String title;
	private Long sourceType;
	private Long titleType;
	private Long externalId;
	private float avgRating;
	private int timesRated;
	private String imageExternalPath;

	public CustomListTranslatedTitleDTO(Long titleId, int position, Long idLocale, String originalTitle, String title,
			Long sourceType, Long titleType, Long externalId, String imageExternalPath, float avgRating,
			int timesRated) {
		this.id = titleId;
		this.position = position;
		this.idLocale = idLocale;
		this.originalTitle = originalTitle;
		this.title = title;
		this.sourceType = sourceType;
		this.titleType = titleType;
		this.externalId = externalId;
		this.imageExternalPath = imageExternalPath;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public CustomListTranslatedTitleDTO(Long titleId, int position, String originalTitle, Long sourceType,
			Long titleType, Long externalId, String imageExternalPath, float avgRating, int timesRated) {
		this.id = titleId;
		this.position = position;
		this.originalTitle = originalTitle;
		this.sourceType = sourceType;
		this.titleType = titleType;
		this.externalId = externalId;
		this.imageExternalPath = imageExternalPath;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public Long getId() {
		return id;
	}

	public int getPosition() {
		return position;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getTitle() {
		return title;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public Long getTitleType() {
		return titleType;
	}

	public Long getExternalId() {
		return externalId;
	}

	public String getImageExternalPath() {
		return imageExternalPath;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public void setTitleType(Long titleType) {
		this.titleType = titleType;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public void setImageExternalPath(String imageExternalPath) {
		this.imageExternalPath = imageExternalPath;
	}

	public float getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}

	public int getTimesRated() {
		return timesRated;
	}

	public void setTimesRated(int timesRated) {
		this.timesRated = timesRated;
	}

}
