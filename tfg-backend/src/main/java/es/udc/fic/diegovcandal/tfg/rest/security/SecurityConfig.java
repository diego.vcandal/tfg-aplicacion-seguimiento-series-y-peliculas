package es.udc.fic.diegovcandal.tfg.rest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtProvider jwtProvider;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.cors().and().csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and().addFilter(new JwtFilter(authenticationManager(), jwtProvider)).authorizeRequests()
				.antMatchers(HttpMethod.POST, "/users/signUp").permitAll()
				.antMatchers(HttpMethod.POST,"/users/login").permitAll()
				.antMatchers(HttpMethod.POST,"/users/loginFromServiceToken").permitAll()
				.antMatchers(HttpMethod.GET,"/users/**").permitAll()
				.antMatchers(HttpMethod.GET,"/search/**").permitAll()
				.antMatchers(HttpMethod.GET,"/titles/**").permitAll()
				.antMatchers(HttpMethod.POST,"/titles/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.PUT,"/titles/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.DELETE,"/titles/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.POST,"/people/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.PUT,"/people/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET,"/people/internal/*").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET,"/people/**").permitAll()
				.antMatchers(HttpMethod.GET,"/credits/*/titles/*/internal").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET,"/credits/**").permitAll()
				.antMatchers(HttpMethod.PUT,"/credits/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.POST,"/credits/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/credits/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET,"/lists/**").permitAll()
				.anyRequest().hasAnyRole("USER", "ADMIN");

	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {

		CorsConfiguration config = new CorsConfiguration();
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

		config.setAllowCredentials(true);
		config.addAllowedOrigin("http://localhost:3000");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");

		source.registerCorsConfiguration("/**", config);

		return source;

	}
}
