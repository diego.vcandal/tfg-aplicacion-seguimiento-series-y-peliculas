package es.udc.fic.diegovcandal.tfg.rest.util;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TitleInfoDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

public class TitleInfoConversor {

	public static final List<TitleInfoDTO> toTitleInfoDTOs(List<TitleInfo> titles, SourceType sourceType) {
		return titles.stream().map(p -> toTitleInfoDTO(p, sourceType)).collect(Collectors.toList());
	}

	public static final TitleInfoDTO toTitleInfoDTO(TitleInfo title, SourceType sourceType) {

		return new TitleInfoDTO(title.getId(), title.getExternalId(), title.getOriginalTitle(), title.getTitleName(),
				title.getDescription(), title.getTitleType().getId(),
				sourceType != null ? sourceType.getId() : title.getSourceType().getId(), title.getOriginalDescription(),
				title.getImage() != null, title.getYear(), title.getAvgRating(), title.getTimesRated());
	}

	public static final List<TitleInfoDTO> externalToMovieInfoDTOs(List<MovieExternalDTO> titles) {
		return titles.stream().map(p -> externalToMovieInfoDTO(p)).collect(Collectors.toList());
	}

	public static final TitleInfoDTO externalToMovieInfoDTO(MovieExternalDTO title) {

		short year = title.getRelease_date() == null || title.getRelease_date().equals("") ? -1
				: Short.parseShort(title.getRelease_date().substring(0, 4));

		return new TitleInfoDTO(title.getId(), title.getId(), title.getOriginal_title(), title.getTitle(),
				title.getOverview(), TitleType.MOVIE.getId(), SourceType.EXTERNAL_TMDB_NO_CACHED.getId(),
				title.getOverview(), true, title.getPoster_path(), year, 0, 0);
	}

	public static final List<TitleInfoDTO> externalToTVShowInfoDTOs(List<TVShowExternalDTO> titles) {
		return titles.stream().map(p -> externalToTVShowInfoDTO(p)).collect(Collectors.toList());
	}

	public static final TitleInfoDTO externalToTVShowInfoDTO(TVShowExternalDTO title) {

		short year = title.getFirst_air_date() == null || title.getFirst_air_date().equals("") ? -1
				: Short.parseShort(title.getFirst_air_date().substring(0, 4));

		return new TitleInfoDTO(title.getId(), title.getId(), title.getOriginal_name(), title.getName(),
				title.getOverview(), TitleType.TV_SHOW.getId(), SourceType.EXTERNAL_TMDB_NO_CACHED.getId(),
				title.getOverview(), true, title.getPoster_path(), year, 0, 0);
	}

}
