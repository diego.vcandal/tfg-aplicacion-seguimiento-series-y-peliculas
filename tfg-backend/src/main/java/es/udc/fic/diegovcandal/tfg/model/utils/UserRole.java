package es.udc.fic.diegovcandal.tfg.model.utils;

public enum UserRole {
	USER,
	ADMIN
}
