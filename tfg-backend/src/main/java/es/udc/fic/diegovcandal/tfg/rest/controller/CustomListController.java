package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CustomListConversor.toCustomList;
import static es.udc.fic.diegovcandal.tfg.rest.util.CustomListConversor.toCustomListDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.CustomListConversor.toCustomListTitlesSet;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ListElementsLimitExceededException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPIClientListService;
import es.udc.fic.diegovcandal.tfg.model.service.CustomListService;
import es.udc.fic.diegovcandal.tfg.model.service.ImageService;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCustomListDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCustomListTitleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CustomListDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.IdDTO;
import es.udc.fic.diegovcandal.tfg.rest.util.Util;

@RestController
@RequestMapping("/lists")
public class CustomListController {

	private static final String CUSTOM_LIST_NOT_FOUND_EXCEPTION_ID = "exceptions.CustomListNotFoundException";
	private static final String LIST_ELEMENTS_LIMIT_EXCEEDED_EXCEPTION_ID = "exceptions.ListElementsLimitExceededException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private CustomListService customListService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private TmdbAPIClientListService tmdbAPIClientListService;

	@ExceptionHandler(CustomListNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleCustomListNotFoundException(CustomListNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(CUSTOM_LIST_NOT_FOUND_EXCEPTION_ID, null,
				CUSTOM_LIST_NOT_FOUND_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(ListElementsLimitExceededException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleListElementsLimitExceededException(ListElementsLimitExceededException exception,
			Locale locale) {

		String errorMessage = messageSource.getMessage(LIST_ELEMENTS_LIMIT_EXCEEDED_EXCEPTION_ID, null,
				LIST_ELEMENTS_LIMIT_EXCEEDED_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@PostMapping()
	public IdDTO createCustomList(@RequestAttribute Long userId,
			@Validated @RequestBody CreateCustomListDTO customListDTO)
			throws UserNotFoundException, TitleNotFoundException {

		return new IdDTO(customListService.createCustomList(userId, toCustomList(customListDTO)));

	}

	@GetMapping("/{id}")
	public CustomListDTO getCustomList(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, CustomListNotFoundException, PrivateResourceException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toCustomListDTO(customListService.getCustomList(userId, id, localeId));

	}

	@PostMapping("/{id}/add")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addToList(@RequestAttribute Long userId, @PathVariable Long id, @Validated @RequestBody IdDTO idDTO)
			throws TitleNotFoundException, UserNotFoundException, CustomListNotFoundException, PermissionException,
			ListElementsLimitExceededException {

		customListService.addToList(userId, id, idDTO.getId());

	}

	@PostMapping("/{id}/add-movie-tmdb")
	public IdDTO addToListTMDbMovie(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody IdDTO idDTO,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, CustomListNotFoundException, TitleNotFoundException,
			IncorrectOperationException, PermissionException, ExternalIdAlreadyLinkedException,
			ListElementsLimitExceededException {

		return new IdDTO(
				tmdbAPIClientListService.addToList(userId, id, idDTO.getId(), TitleType.MOVIE, acceptLanguage));

	}

	@PostMapping("/{id}/add-tvshow-tmdb")
	public IdDTO addToListTMDbTVShow(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody IdDTO idDTO,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, CustomListNotFoundException, TitleNotFoundException,
			IncorrectOperationException, PermissionException, ExternalIdAlreadyLinkedException,
			ListElementsLimitExceededException {

		return new IdDTO(
				tmdbAPIClientListService.addToList(userId, id, idDTO.getId(), TitleType.TV_SHOW, acceptLanguage));

	}

	@PutMapping("/{id}/update-order")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateElementsOrder(@RequestAttribute Long userId, @PathVariable Long id,
			@RequestBody List<CreateCustomListTitleDTO> titles)
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		customListService.updateElementsOrder(userId, id, toCustomListTitlesSet(titles));

	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateCustomList(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody CreateCustomListDTO customListDTO)
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		customListService.updateCustomList(userId, id, toCustomList(customListDTO));

	}

	@GetMapping("/{id}/image")
	public void findCustomListImage(@PathVariable Long id, HttpServletResponse response)
			throws CustomListNotFoundException {

		String image = imageService.getCustomListImage(id);

		Util.sendImage(response, image);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCustomList(@RequestAttribute Long userId, @PathVariable Long id)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		customListService.deleteCustomList(userId, id);

	}

	@PostMapping("/{id}/delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteFromList(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody IdDTO idDTO)
			throws TitleNotFoundException, UserNotFoundException, CustomListNotFoundException, PermissionException {

		customListService.deleteFromList(userId, id, idDTO.getId());

	}

	@PostMapping("/{id}/likes")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addLike(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException {
		customListService.addLike(userId, id);
	}

	@DeleteMapping("/{id}/likes")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeLike(@PathVariable Long id, @RequestAttribute Long userId)
			throws CustomListNotFoundException, UserNotFoundException, PermissionException {
		customListService.removeLike(userId, id);
	}

}
