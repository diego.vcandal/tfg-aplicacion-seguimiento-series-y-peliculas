package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class DuplicateInstanceException extends Exception{

	private String entity;
	private Object key;

	protected DuplicateInstanceException(String message) {
		super(message);
	}

	public DuplicateInstanceException(String entity, Object key) {
		this.entity =entity;
		this.key = key;
	}

	public String getEntity() {
		return entity;
	}

	public Object getKey() {
		return key;
	}
	
}
