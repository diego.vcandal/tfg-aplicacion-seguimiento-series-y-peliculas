package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class EpisodeSummaryDTO {

	private Long id;
	private Long externalId;
	private short episodeNumber;
	private String originalTitle;
	private String originalDescription;
	private String title;
	private String description;
	private Long airDate;
	private short totalDuration;
	private boolean image;
	private String imagePath;
	private Long sourceType;
	private boolean setForDelete;
	private String tvshowOriginalTitle;
	private String seasonOriginalTitle;

	private List<EpisodeTraductionDTO> traductions;

	public EpisodeSummaryDTO(Long id, Long externalId, short episodeNumber, String originalTitle,
			String originalDescription, Long airDate, short totalDuration, boolean image, Long sourceType) {
		this.id = id;
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.airDate = airDate;
		this.totalDuration = totalDuration;
		this.image = image;

		this.title = originalTitle;
		this.description = originalDescription;

		this.sourceType = sourceType;
	}

	public EpisodeSummaryDTO(Long id, Long externalId, short episodeNumber, String originalTitle,
			String originalDescription, Long airDate, short totalDuration, String imagePath, Long sourceType) {
		this.id = id;
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.airDate = airDate;
		this.totalDuration = totalDuration;
		this.imagePath = imagePath;

		this.title = originalTitle;
		this.description = originalDescription;

		this.sourceType = sourceType;
	}

	public EpisodeSummaryDTO(Long id, Long externalId, short episodeNumber, String originalTitle,
			String originalDescription, Long airDate, boolean image, boolean setForDelete,
			List<EpisodeTraductionDTO> traductions) {
		this.id = id;
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.airDate = airDate;
		this.image = image;
		this.setForDelete = setForDelete;
		this.traductions = traductions;
	}

	public Long getId() {
		return id;
	}

	public short getEpisodeNumber() {
		return episodeNumber;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public Long getAirDate() {
		return airDate;
	}

	public short getTotalDuration() {
		return totalDuration;
	}

	public boolean isImage() {
		return image;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEpisodeNumber(short episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAirDate(Long airDate) {
		this.airDate = airDate;
	}

	public void setTotalDuration(short totalDuration) {
		this.totalDuration = totalDuration;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public List<EpisodeTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public void setTraductions(List<EpisodeTraductionDTO> traductions) {
		this.traductions = traductions;
	}

	public void setTvshowOriginalTitle(String tvshowOriginalTitle) {
		this.tvshowOriginalTitle = tvshowOriginalTitle;
	}

	public void setSeasonOriginalTitle(String seasonOriginalTitle) {
		this.seasonOriginalTitle = seasonOriginalTitle;
	}

	public String getTvshowOriginalTitle() {
		return tvshowOriginalTitle;
	}

	public String getSeasonOriginalTitle() {
		return seasonOriginalTitle;
	}

}
