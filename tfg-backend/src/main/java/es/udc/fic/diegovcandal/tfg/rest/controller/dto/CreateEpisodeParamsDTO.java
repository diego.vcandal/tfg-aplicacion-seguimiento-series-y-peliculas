package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CreateEpisodeParamsDTO {

	private Long externalId;
	private short episodeNumber;
	private String originalTitle;
	private String originalDescription;
	private Long airDate;
	private short totalDuration;
	private String imageContent;

	@Valid
	private List<EpisodeTraductionDTO> traductions;

	public CreateEpisodeParamsDTO(Long externalId, short episodeNumber, String originalTitle,
			String originalDescription, Long airDate, short totalDuration, String imageContent) {
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.airDate = airDate;
		this.totalDuration = totalDuration;
		this.imageContent = imageContent;
	}

	@NotNull()
	@Min(1)
	public Long getExternalId() {
		return externalId;
	}

	@NotNull()
	@Min(0)
	public short getEpisodeNumber() {
		return episodeNumber;
	}

	@NotNull()
	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public Long getAirDate() {
		return airDate;
	}

	public short getTotalDuration() {
		return totalDuration;
	}

	public String getImageContent() {
		return imageContent;
	}

	public List<EpisodeTraductionDTO> getTraductions() {
		return traductions;
	}

}
