package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;

public class TVShowWithSeasons {

	private TVShow tvShow;
	private List<SeasonWithTraduction> seasons;

	public TVShowWithSeasons(TVShow tvShow, List<SeasonWithTraduction> seasons) {
		this.tvShow = tvShow;
		this.seasons = seasons;
	}

	public TVShow getTvShow() {
		return tvShow;
	}

	public List<SeasonWithTraduction> getSeasons() {
		return seasons;
	}

	public void setTvShow(TVShow tvShow) {
		this.tvShow = tvShow;
	}

	public void setSeasons(List<SeasonWithTraduction> seasons) {
		this.seasons = seasons;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((seasons == null) ? 0 : seasons.hashCode());
		result = prime * result + ((tvShow == null) ? 0 : tvShow.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TVShowWithSeasons other = (TVShowWithSeasons) obj;
		if (seasons == null) {
			if (other.seasons != null)
				return false;
		} else if (!seasons.equals(other.seasons))
			return false;
		if (tvShow == null) {
			if (other.tvShow != null)
				return false;
		} else if (!tvShow.equals(other.tvShow))
			return false;
		return true;
	}

}
