package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Genre {

	private Long id;
	private Long externalId;
	private String name;

	public Genre() {
	}

	public Genre(Long id, Long externalId, String name) {
		this.id = id;
		this.name = name;
		this.externalId = externalId;
	}

	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

}
