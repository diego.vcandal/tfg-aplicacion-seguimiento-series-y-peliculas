package es.udc.fic.diegovcandal.tfg.model.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Crew {

	private Long id;
	private Title titleId;
	private People peopleId;
	private CrewType crewTypeId;
	private short episodeCount;

	private Set<CrewSeason> crewSeasons;

	public Crew() {
		this.crewSeasons = new HashSet<>();
	}

	public Crew(Title titleId, People peopleId, CrewType crewTypeId, short episodeCount) {
		this.titleId = titleId;
		this.crewTypeId = crewTypeId;
		this.episodeCount = episodeCount;
		this.peopleId = peopleId;

		this.crewSeasons = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "titleId")
	public Title getTitleId() {
		return titleId;
	}

	public void setTitleId(Title titleId) {
		this.titleId = titleId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "peopleId")
	public People getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(People peopleId) {
		this.peopleId = peopleId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "crewTypeId")
	public CrewType getCrewTypeId() {
		return crewTypeId;
	}

	public void setCrewTypeId(CrewType crewTypeId) {
		this.crewTypeId = crewTypeId;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	@OneToMany(mappedBy = "crewId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CrewSeason> getCrewSeasons() {
		return crewSeasons;
	}

	public void setCrewSeasons(Set<CrewSeason> crewSeasons) {
		this.crewSeasons = crewSeasons;
	}

}
