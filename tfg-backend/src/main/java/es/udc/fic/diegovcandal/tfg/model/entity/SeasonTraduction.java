package es.udc.fic.diegovcandal.tfg.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.SeasonTraductionCompositeKey;

@Entity
@IdClass(SeasonTraductionCompositeKey.class)
public class SeasonTraduction implements Serializable {

	private static final long serialVersionUID = 4362986902941295837L;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "idSeason")
	private Season idSeason;

	@Id
	private Long idLocale;

	private String title;
	private String description;

	public SeasonTraduction() {

	}

	public SeasonTraduction(Season idSeason, Long idLocale, String title, String description) {
		this.idSeason = idSeason;
		this.idLocale = idLocale;
		this.title = title;
		this.description = description;
	}

	public Season getIdSeason() {
		return idSeason;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setIdSeason(Season idSeason) {
		this.idSeason = idSeason;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
