package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthenticatedUserDTO {

	private String serviceToken;
	private UserDTO userDTO;
	private List<FriendPetitionDTO> petitions;

	public AuthenticatedUserDTO() {
	}

	public AuthenticatedUserDTO(String serviceToken, UserDTO userDTO, List<FriendPetitionDTO> petitions) {

		this.serviceToken = serviceToken;
		this.userDTO = userDTO;
		this.petitions = petitions;

	}

	public String getServiceToken() {
		return serviceToken;
	}

	public void setServiceToken(String serviceToken) {
		this.serviceToken = serviceToken;
	}

	@JsonProperty("user")
	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public List<FriendPetitionDTO> getPetitions() {
		return petitions;
	}

	public void setPetitions(List<FriendPetitionDTO> petitions) {
		this.petitions = petitions;
	}

}
