package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.FriendPetition;
import es.udc.fic.diegovcandal.tfg.model.entity.UserFriendOf;
import es.udc.fic.diegovcandal.tfg.model.exceptions.AlreadyFriendsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionAlreadySentException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionNotFound;
import es.udc.fic.diegovcandal.tfg.model.exceptions.InstanceNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;

public interface FriendService {

	List<FriendPetition> getReceivedFriendPetitions(Long destinationUser) throws UserNotFoundException;

	List<FriendPetition> getSentFriendPetitions(Long originUser) throws UserNotFoundException;

	void sendFriendRequest(Long originUser, Long destinationUser) throws FriendPetitionAlreadySentException,
			UserNotFoundException, AlreadyFriendsException, FriendPetitionNotFound;

	void acceptFriendRequest(Long originUser, Long destinationUser) throws FriendPetitionNotFound;

	void rejectFriendRequest(Long originUser, Long destinationUser) throws FriendPetitionNotFound;

	void deleteFriend(Long userId, Long friendId) throws InstanceNotFoundException;

	Block<UserFriendOf> getFriends(Long userId, int pageNumber, int size) throws UserNotFoundException;

	boolean areFriends(Long userId, Long friendId) throws UserNotFoundException;

	boolean hasSentPetition(Long userId, Long destinationId) throws UserNotFoundException;

}
