package es.udc.fic.diegovcandal.tfg.model.utils;

public enum TitleType {
	
	MOVIE(0L),
	TV_SHOW(1L);
	
	private TitleType(Long id) {
		this.id = id;
	}

	private Long id;

	public Long getId() {
		return id;
	}
}
