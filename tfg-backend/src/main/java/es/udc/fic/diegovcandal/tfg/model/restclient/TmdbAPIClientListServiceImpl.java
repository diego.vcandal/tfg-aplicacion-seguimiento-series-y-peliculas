package es.udc.fic.diegovcandal.tfg.model.restclient;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CustomListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ListElementsLimitExceededException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

@Service
@Transactional
public class TmdbAPIClientListServiceImpl implements TmdbAPIClientListService {

	@Autowired
	private PermissionManager permissionManager;

	@Autowired
	private TmdbAPIClient tmdbAPIClient;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private CustomListDAO customListDAO;

	@Value("${project.lists.limit}")
	private int listLimit;

	@Override
	public Long addToList(Long userId, Long listId, Long titleId, TitleType titleType, String acceptLanguage)
			throws UserNotFoundException, IncorrectOperationException, CustomListNotFoundException, PermissionException,
			TitleNotFoundException, ExternalIdAlreadyLinkedException, ListElementsLimitExceededException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		if (list.get().getTitleCount() >= listLimit) {
			throw new ListElementsLimitExceededException();
		}

		Optional<Title> foundTitle = titleDAO.findByExternalId(titleId);

		Title title = null;

		if (!foundTitle.isPresent()) {

			if (titleType == TitleType.MOVIE) {
				title = tmdbAPIClient.createExternalMovie(userId, titleId, acceptLanguage, true);
			} else if (titleType == TitleType.TV_SHOW) {
				title = tmdbAPIClient.createExternalTVShow(userId, titleId, acceptLanguage, true);
			}

		} else {
			title = foundTitle.get();
		}

		list.get().getCustomListTitles().add(new CustomListTitle(list.get(), title, list.get().getTitleCount() + 1));
		list.get().setTitleCount(list.get().getTitleCount() + 1);
		list.get().setLastUpdated(LocalDateTime.now());

		customListDAO.save(list.get());

		return title.getId();

	}

}
