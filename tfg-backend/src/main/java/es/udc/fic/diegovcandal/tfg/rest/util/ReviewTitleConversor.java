package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ReviewTitleDTO;

public class ReviewTitleConversor {

	public static final List<ReviewTitleDTO> toReviewTitleDTOs(List<ReviewTitle> reviews) {
		return reviews.stream().map(r -> toReviewTitleDTO(r)).collect(Collectors.toList());
	}

	public static final ReviewTitleDTO toReviewTitleDTO(ReviewTitle review) {

		return new ReviewTitleDTO(review.getId(), review.getTitleId().getId(), review.getUserId().getId(),
				review.getUserId().getUserName(), review.getRating(), review.isContainsReview(), review.getTitle(),
				review.getContent(), review.getLikeCount(), toMillis(review.getAddedDate()), review.isLiked());

	}

	private static final Long toMillis(LocalDate date) {

		if (date == null)
			return null;

		return date.atStartOfDay().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant()
				.toEpochMilli();
	}

}
