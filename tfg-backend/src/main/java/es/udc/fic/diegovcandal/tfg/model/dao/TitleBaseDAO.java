package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction;

@NoRepositoryBean
public interface TitleBaseDAO<T extends Title> extends PagingAndSortingRepository<T, Long> {
	
	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction(t, tt) "
			+ "FROM Title t LEFT JOIN TitleTraduction tt "
			+ "on t.id = tt.idTitle AND "
			+ "tt.idLocale = ?2 "
			+ "WHERE t.id = ?1")
	Optional<TitleWithTraduction<T>> findTitleWithTraduction(Long id, Long localeId);
	
	@Query("SELECT t.image "
			+ "FROM TitleImage t "
			+ "WHERE t.title = ?1 AND t.header = true")
	String findHeaderImage(Title title);
	
	@Query("SELECT t.image "
			+ "FROM TitleImage t "
			+ "WHERE t.title = ?1 AND t.portrait = true")
	String findPortraitImage(Title title);

	boolean existsByExternalId(Long externalId);
	
	Optional<Title> findByExternalId(Long externalId);
	
}
