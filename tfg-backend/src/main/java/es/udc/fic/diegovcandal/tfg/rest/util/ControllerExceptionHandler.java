package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import es.udc.fic.diegovcandal.tfg.model.exceptions.DuplicateInstanceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FieldErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.exceptions.ApiNotEnabledException;

@RestControllerAdvice
public class ControllerExceptionHandler {

	private static final String DUPLICATE_INSTANCE_EXCEPTION_ID = "exceptions.DuplicateInstanceException";
	private static final String PERMISSION_EXCEPTION_ID = "exceptions.PermissionException";

	private static final String INCORRECT_OPERATION_EXCEPTION = "exceptions.IncorrectOperationException";
	private static final String USER_NOT_FOUND_EXCEPTION_ID = "exceptions.UserNotFoundException";

	private static final String EXTERNAL_ID_ALREADY_LINKED_EXCEPTION_ID = "exceptions.ExternalIdAlreadyLinkedException";

	private static final String PRIVATE_RESOURCE_EXCEPTION_ID = "exceptions.PrivateResourceException";

	private static final String TITLE_NOT_FOUND_EXCEPTION_ID = "exceptions.TitleNotFoundException";

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(ApiNotEnabledException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public Map<String, String> handleExternalIdAlreadyLinkedException() {
		HashMap<String, String> response = new HashMap<>();
		response.put("timestamp", LocalDateTime.now().toString());
		response.put("status", "404");
		response.put("message", "No message available");
		return response;
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, Locale locale) {

		List<FieldErrorDTO> fieldErrors = exception.getBindingResult().getFieldErrors().stream()
				.map(error -> new FieldErrorDTO(error.getField(), error.getDefaultMessage()))
				.collect(Collectors.toList());

		return new ErrorDTO(fieldErrors);

	}

	@ExceptionHandler(DuplicateInstanceException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorDTO handleDuplicateInstanceException(DuplicateInstanceException exception, Locale locale) {

		String nameMessage = messageSource.getMessage(exception.getEntity(), null, exception.getEntity(), locale);
		String errorMessage = messageSource.getMessage(DUPLICATE_INSTANCE_EXCEPTION_ID,
				new Object[] { nameMessage, exception.getKey().toString() }, DUPLICATE_INSTANCE_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(PermissionException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handlePermissionException(PermissionException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(PERMISSION_EXCEPTION_ID, null, PERMISSION_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(IncorrectOperationException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleIncorrectOperationException(IncorrectOperationException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(INCORRECT_OPERATION_EXCEPTION, null,
				INCORRECT_OPERATION_EXCEPTION, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleUserNotFoundException(UserNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(USER_NOT_FOUND_EXCEPTION_ID, null, USER_NOT_FOUND_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(ExternalIdAlreadyLinkedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleExternalIdAlreadyLinkedException(ExternalIdAlreadyLinkedException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(EXTERNAL_ID_ALREADY_LINKED_EXCEPTION_ID, null,
				EXTERNAL_ID_ALREADY_LINKED_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(PrivateResourceException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handlePrivateResourceException(PrivateResourceException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(PRIVATE_RESOURCE_EXCEPTION_ID, null,
				PRIVATE_RESOURCE_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(TitleNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleTitleNotFoundException(TitleNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(TITLE_NOT_FOUND_EXCEPTION_ID, null, TITLE_NOT_FOUND_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

}
