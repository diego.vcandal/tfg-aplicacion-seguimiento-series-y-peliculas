package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

public interface CustomListDAO extends PagingAndSortingRepository<CustomList, Long> {

	List<CustomList> findByUserId(User userId);

	List<CustomList> findByUserIdAndVisibility(User userId, Visibility visibility);
	
	@Query("SELECT c "
			+ "FROM CustomList c "
			+ "WHERE c.title LIKE %?1% AND "
			+ "visibility = 0 "
			+ "ORDER BY c.likeCount DESC")
	Page<CustomList> findPublicCustomListsByTitle(String title, Pageable pageable);
	
	Page<CustomList> findByUserIdOrderByIdDesc(User userId, Pageable pageable);

	Page<CustomList> findByUserIdAndVisibilityOrderByIdDesc(User userId, Visibility visibility, Pageable pageable);

}
