package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserCreationParamsDTO {

	public interface AllValidations {
	}

	public interface UpdateValidations {
	}

	private Long id;
	private String email;
	private String country;
	private String userName;
	private String password;
	private String role;
	private String image;
	private boolean imageChanged;
	private RelationshipInfoDTO relationshipInfo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull(groups = { AllValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class })
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName.trim();
	}

	@NotNull(groups = { AllValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class })
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@NotNull(groups = { AllValidations.class, UpdateValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class, UpdateValidations.class })
	@Email(groups = { AllValidations.class, UpdateValidations.class })
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.trim();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@NotNull(groups = { AllValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class })
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isImageChanged() {
		return imageChanged;
	}

	public void setImageChanged(boolean imageChanged) {
		this.imageChanged = imageChanged;
	}

	public RelationshipInfoDTO getRelationshipInfo() {
		return relationshipInfo;
	}

	public void setRelationshipInfo(RelationshipInfoDTO relationshipInfo) {
		this.relationshipInfo = relationshipInfo;
	}

}
