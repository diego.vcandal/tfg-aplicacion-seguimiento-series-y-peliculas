package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class PersonCrewSummaryDTO {

	private Long titleId;
	private String originalTitle;
	private String titleName;
	private short year;
	private Long crewTypeId;
	private String department;
	private short episodeCount;

	public PersonCrewSummaryDTO(Long titleId, String originalTitle, String titleName, short year, Long crewTypeId,
			short episodeCount) {
		this.titleId = titleId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.year = year;
		this.crewTypeId = crewTypeId;
		this.episodeCount = episodeCount;
	}

	public PersonCrewSummaryDTO(Long titleId, String originalTitle, String titleName, short year, String department,
			short episodeCount) {
		this.titleId = titleId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.year = year;
		this.department = department;
		this.episodeCount = episodeCount;
	}

	public Long getTitleId() {
		return titleId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getTitleName() {
		return titleName;
	}

	public short getYear() {
		return year;
	}

	public Long getCrewTypeId() {
		return crewTypeId;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public String getDepartment() {
		return department;
	}

}
