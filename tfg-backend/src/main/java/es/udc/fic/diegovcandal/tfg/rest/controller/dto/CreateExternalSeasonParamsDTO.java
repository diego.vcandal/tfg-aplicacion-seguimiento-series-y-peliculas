package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.NotNull;

public class CreateExternalSeasonParamsDTO {

	private Long externalSeasonId;

	@NotNull
	public Long getExternalSeasonId() {
		return externalSeasonId;
	}

}
