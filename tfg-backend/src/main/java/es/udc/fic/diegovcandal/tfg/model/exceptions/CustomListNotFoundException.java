package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class CustomListNotFoundException extends InstanceNotFoundException {

	public CustomListNotFoundException(Object key) {
		super("entities.customList", key);
	}

}
