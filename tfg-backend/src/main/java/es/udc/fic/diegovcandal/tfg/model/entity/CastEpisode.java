package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CastEpisode {

	private Long id;
	private CastSeason castSeasonId;
	private Episode episodeId;
	private String characterName;

	public CastEpisode() {
	}

	public CastEpisode(CastSeason castSeasonId, Episode episodeId, String characterName) {
		this.castSeasonId = castSeasonId;
		this.episodeId = episodeId;
		this.characterName = characterName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "castSeasonId")
	public CastSeason getCastSeasonId() {
		return castSeasonId;
	}

	public void setCastSeasonId(CastSeason castSeasonId) {
		this.castSeasonId = castSeasonId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "episodeId")
	public Episode getEpisodeId() {
		return episodeId;
	}

	public void setEpisodeId(Episode episodeId) {
		this.episodeId = episodeId;
	}

	public String getCharacterName() {
		return characterName;
	}

	public void setCharacterName(String characterName) {
		this.characterName = characterName;
	}

}
