package es.udc.fic.diegovcandal.tfg.model.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Cast {

	private Long id;
	private Title titleId;
	private People peopleId;
	private short episodeCount;
	private String characterName;
	private boolean isMainCast;

	private Set<CastSeason> castSeasons;

	public Cast() {
		this.castSeasons = new HashSet<>();
	}

	public Cast(Title titleId, People peopleId, short episodeCount, String characterName, boolean isMainCast) {
		this.titleId = titleId;
		this.episodeCount = episodeCount;
		this.characterName = characterName;
		this.isMainCast = isMainCast;
		this.peopleId = peopleId;

		this.castSeasons = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "titleId")
	public Title getTitleId() {
		return titleId;
	}

	public void setTitleId(Title titleId) {
		this.titleId = titleId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "peopleId")
	public People getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(People peopleId) {
		this.peopleId = peopleId;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	public String getCharacterName() {
		return characterName;
	}

	public void setCharacterName(String characterName) {
		this.characterName = characterName;
	}

	public boolean isMainCast() {
		return isMainCast;
	}

	public void setMainCast(boolean isMainCast) {
		this.isMainCast = isMainCast;
	}

	@OneToMany(mappedBy = "castId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CastSeason> getCastSeasons() {
		return castSeasons;
	}

	public void setCastSeasons(Set<CastSeason> castSeasons) {
		this.castSeasons = castSeasons;
	}

}
