package es.udc.fic.diegovcandal.tfg.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.Genre;

public interface GenreDao extends PagingAndSortingRepository<Genre, Long> {

}
