package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class ReviewTitle {

	private Long id;
	private Title titleId;
	private User userId;
	private byte rating;
	private boolean containsReview;
	private String title;
	private String content;
	private int likeCount;
	private LocalDate addedDate;
	@Transient
	private boolean liked;

	public ReviewTitle() {
	}

	public ReviewTitle(Title titleId, User userId, byte rating, boolean containsReview, String title, String content,
			int likeCount, LocalDate addedDate) {
		this.titleId = titleId;
		this.userId = userId;
		this.rating = rating;
		this.containsReview = containsReview;
		this.title = title;
		this.content = content;
		this.likeCount = likeCount;
		this.addedDate = addedDate;
		this.liked = false;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "titleId")
	public Title getTitleId() {
		return titleId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public User getUserId() {
		return userId;
	}

	public byte getRating() {
		return rating;
	}

	public boolean isContainsReview() {
		return containsReview;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitleId(Title titleId) {
		this.titleId = titleId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public void setRating(byte rating) {
		this.rating = rating;
	}

	public void setContainsReview(boolean containsReview) {
		this.containsReview = containsReview;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	@Transient
	public boolean isLiked() {
		return liked;
	}

	@Transient
	public void setLiked(boolean liked) {
		this.liked = liked;
	}

}
