package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class GenreDTO {

	private Long id;
	private Long externalId;

	public GenreDTO(Long id, Long externalId) {
		this.id = id;
		this.externalId = externalId;
	}

	@NotNull
	@Min(1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Min(1)
	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

}
