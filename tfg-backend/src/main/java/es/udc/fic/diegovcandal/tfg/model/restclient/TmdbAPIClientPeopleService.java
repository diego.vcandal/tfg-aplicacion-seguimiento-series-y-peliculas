package es.udc.fic.diegovcandal.tfg.model.restclient;

import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.BlockExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalPersonCreditsSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.PeopleExternalDTO;

public interface TmdbAPIClientPeopleService {

	Long createExternalPerson(Long userId, Long externalId, String acceptLanguage)
			throws ExternalIdAlreadyLinkedException, PeopleNotFoundException, UserNotFoundException,
			PermissionException;

	PeopleExternalDTO findCachedPerson(Long externalId, Long internalId, String acceptLanguage)
			throws PeopleNotFoundException;

	PeopleExternalDTO findExternalPeople(Long externalId, String acceptLanguage) throws PeopleNotFoundException;

	BlockExternalDTO<PeopleExternalDTO> findExternalPersons(String name, int page, String acceptLanguage);

	byte[] getExternalImage(String image);

	ExternalPersonCreditsSummaryDTO findPersonCastAndCrewHistory(Long externalId, String acceptLanguage);

}
