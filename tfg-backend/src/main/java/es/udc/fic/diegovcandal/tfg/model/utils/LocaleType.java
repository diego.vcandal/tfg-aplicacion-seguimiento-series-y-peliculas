package es.udc.fic.diegovcandal.tfg.model.utils;

public enum LocaleType {

	gl(0L, "gl"), esES(1L, "es-ES"), en(2L, "en");

	private Long id;
	private String language;

	public static final LocaleType DEFAULT_LOCALE = LocaleType.esES;

	private LocaleType(Long id, String language) {
		this.id = id;
		this.language = language;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public static LocaleType valueOfLanguage(String language) {
		for (LocaleType e : values()) {
			if (e.language != null && e.language.trim().equals(language)) {
				return e;
			}
		}
		return null;
	}

	public static Long valueOfLanguageOrDefault(String language) {

		LocaleType localeType = valueOfLanguage(language);

		return localeType == null ? DEFAULT_LOCALE.getId() : localeType.getId();
	}

}
