package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;

public class PeopleWithTraduction {

	private People people;
	private PeopleTraduction traduction;

	public PeopleWithTraduction(People people, PeopleTraduction traduction) {
		this.people = people;
		this.traduction = traduction;
	}

	public People getPeople() {
		return people;
	}

	public void setPeople(People people) {
		this.people = people;
	}

	public PeopleTraduction getTraduction() {
		return traduction;
	}

	public void setTraduction(PeopleTraduction traduction) {
		this.traduction = traduction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((people == null) ? 0 : people.hashCode());
		result = prime * result + ((traduction == null) ? 0 : traduction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeopleWithTraduction other = (PeopleWithTraduction) obj;
		if (people == null) {
			if (other.people != null)
				return false;
		} else if (!people.equals(other.people))
			return false;
		if (traduction == null) {
			if (other.traduction != null)
				return false;
		} else if (!traduction.equals(other.traduction))
			return false;
		return true;
	}

}
