package es.udc.fic.diegovcandal.tfg.model.service;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

public interface SearchService {

	Block<User> findUsers(String userName, int page, int size);

	Block<TitleInfo> findTitles(Long userId, String title, Long idLocale, int page, int size)
			throws UserNotFoundException;

	Block<TitleInfo> findLastTitles(Long idLocale, TitleType titleType);

	Block<People> findPeople(Long userId, String name, int pageNumber, int size) throws UserNotFoundException;

	Block<CustomList> findCustomLists(String title, int pageNumber, int size);

}
