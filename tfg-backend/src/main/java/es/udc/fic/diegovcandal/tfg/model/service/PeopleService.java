package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PeopleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCastSummary;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCrewSummary;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface PeopleService {

	Long createPerson(Long userId, People people)
			throws UserNotFoundException, PermissionException, ExternalIdAlreadyLinkedException;

	void uptadePerson(Long userId, Long peopleId, People updatedPeople)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException;

	Long existsByExternalId(Long externalId);

	People getInternalPerson(Long id, Long userId)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException;

	PeopleWithTraduction getPersonDetails(Long userId, Long id, Long localeId)
			throws PeopleNotFoundException, UserNotFoundException;

	void changeSourceType(Long userId, Long personId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException;

	void setPersonForDeletion(Long userId, Long personId, boolean setForDeletion)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException;

	void makePersonPublic(Long userId, Long personId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException;

	People findByExternalId(Long externalId) throws PeopleNotFoundException;

	List<PersonCrewSummary> findCrewHistory(Long personId, Long localeId) throws PeopleNotFoundException;

	List<PersonCastSummary> findCastHistory(Long personId, Long localeId) throws PeopleNotFoundException;

}
