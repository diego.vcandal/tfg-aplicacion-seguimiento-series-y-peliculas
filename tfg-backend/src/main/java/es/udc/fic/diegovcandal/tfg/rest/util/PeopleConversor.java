package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PeopleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreatePersonDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PeopleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PeopleWithTraductionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonTraductionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.PeopleExternalDTO;

public class PeopleConversor {

	public static final PeopleWithTraductionDTO fromExternalPeopleToPeopleWithTraductionDTO(
			PeopleExternalDTO peopleExternalDTO, boolean setForDelete) {

		return new PeopleWithTraductionDTO(peopleExternalDTO.getId(), peopleExternalDTO.getExternalId(),
				SourceType.EXTERNAL_TMDB.getId(), peopleExternalDTO.getName(), peopleExternalDTO.getName(),
				peopleExternalDTO.getBiography(), peopleExternalDTO.getPlace_of_birth(),
				toMillisFromString(peopleExternalDTO.getBirthday()),
				toMillisFromString(peopleExternalDTO.getDeathday()), true, true, setForDelete,
				peopleExternalDTO.getProfile_path());
	}

	public static final PeopleWithTraductionDTO toPeopleWithTraductionDTO(PeopleWithTraduction peopleWithTraduction,
			SourceType sourceType) {

		People people = peopleWithTraduction.getPeople();
		PeopleTraduction traduction = peopleWithTraduction.getTraduction();

		boolean hasImage = people.getImage() != null;

		return new PeopleWithTraductionDTO(people.getId(), people.getExternalId(), sourceType.getId(),
				people.getOriginalName(), traduction.getName(), traduction.getBiography(), traduction.getBirthPlace(),
				toMillis(people.getBirthday()), toMillis(people.getDeathday()), hasImage,
				people.getSourceType() == SourceType.INTERNAL ? people.getVisibility() == Visibility.PUBLIC : true,
				people.isSetForDelete(), null);

	}

	public static final People toPeople(CreatePersonDTO createPersonDTO) {

		People people = new People(createPersonDTO.getExternalId(), SourceType.INTERNAL,
				createPersonDTO.getOriginalName(), toLocalDate(createPersonDTO.getBirthday()),
				toLocalDate(createPersonDTO.getDeathday()), createPersonDTO.getImage());

		if (createPersonDTO.getTraductions() != null) {
			people.setPeopleTraductions(toPeopleTraductions(people, createPersonDTO.getTraductions()));
		}

		return people;
	}

	public static final PeopleDTO toPeopleDTO(People people, boolean isInternal) {

		List<PersonTraductionDTO> traductions = people.getPeopleTraductions().stream()
				.map(pt -> toPersonTraductionDTO(pt)).collect(Collectors.toList());

		boolean hasImage = people.getImage() != null;

		PeopleDTO peopleDTO = new PeopleDTO(people.getId(), people.getExternalId(), people.getSourceType().getId(),
				people.getOriginalName(), toMillis(people.getBirthday()), toMillis(people.getDeathday()), null,
				people.getExternalImage(), people.isSetForDelete(), traductions);

		peopleDTO.setHasImage(hasImage);
		peopleDTO.setIsInternal(isInternal);
		peopleDTO.setVisibility(
				people.getSourceType() == SourceType.INTERNAL ? people.getVisibility() == Visibility.PUBLIC : true);

		return peopleDTO;
	}

	public static final List<PersonSummaryDTO> toPersonSummaryDTOs(List<People> peoples) {
		return peoples.stream().map(p -> toPersonSummaryDTO(p)).collect(Collectors.toList());
	}

	private static final PersonSummaryDTO toPersonSummaryDTO(People people) {

		boolean hasImage = people.getImage() != null;

		return new PersonSummaryDTO(people.getId(), people.getExternalId(), people.getSourceType().getId(),
				people.getOriginalName(), hasImage, null);
	}

	public static final List<PersonSummaryDTO> externalToPersonSummaryDTOs(List<PeopleExternalDTO> peoples) {
		return peoples.stream().map(p -> externalToPersonSummaryDTO(p)).collect(Collectors.toList());
	}

	public static final PersonSummaryDTO externalToPersonSummaryDTO(PeopleExternalDTO people) {
		return new PersonSummaryDTO(people.getId(), people.getExternalId(), SourceType.EXTERNAL_TMDB_NO_CACHED.getId(),
				people.getName(), true, people.getProfile_path());
	}

	private static final Set<PeopleTraduction> toPeopleTraductions(People people,
			List<PersonTraductionDTO> traductions) {
		return traductions.stream().map(pt -> toPeopleTraduction(people, pt)).collect(Collectors.toSet());
	}

	private static final PeopleTraduction toPeopleTraduction(People people, PersonTraductionDTO traduction) {
		return new PeopleTraduction(people, traduction.getIdLocale(), traduction.getName(), traduction.getBiography(),
				traduction.getBirthPlace(), traduction.isOriginalLanguage());
	}

	private static final PersonTraductionDTO toPersonTraductionDTO(PeopleTraduction peopleTraduction) {
		return new PersonTraductionDTO(peopleTraduction.getIdLocale(), peopleTraduction.getName(),
				peopleTraduction.getBiography(), peopleTraduction.getBirthPlace(),
				peopleTraduction.isOriginalLanguage());
	}

	private static final Long toMillisFromString(String date) {

		if (date == null || date.trim().equals("")) {
			return null;
		}

		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		return toMillis(LocalDate.parse(date, dtf));
	}

	private static final LocalDate toLocalDate(Long date) {

		if (date == null)
			return null;

		return Instant.ofEpochMilli(date).atZone(ZoneOffset.systemDefault()).toLocalDate();
	}

	private static final Long toMillis(LocalDate date) {

		if (date == null)
			return null;

		return date.atStartOfDay().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant()
				.toEpochMilli();
	}
}
