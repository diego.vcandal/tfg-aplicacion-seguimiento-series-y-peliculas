package es.udc.fic.diegovcandal.tfg.model.utils;

public enum SourceType {
	
	INTERNAL(0L),
	EXTERNAL_TMDB(1L),
	EXTERNAL_TMDB_NO_CACHED(2L);
	
	private SourceType(Long id) {
		this.id = id;
	}

	private Long id;

	public Long getId() {
		return id;
	}
	
	
}