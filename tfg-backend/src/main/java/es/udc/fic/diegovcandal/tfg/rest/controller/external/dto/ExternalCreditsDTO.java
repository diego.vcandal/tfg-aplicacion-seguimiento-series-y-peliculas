package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class ExternalCreditsDTO {

	private List<CastExternalDTO> cast;
	private List<CrewExternalDTO> crew;

	public List<CastExternalDTO> getCast() {
		return cast;
	}

	public void setCast(List<CastExternalDTO> cast) {
		this.cast = cast;
	}

	public List<CrewExternalDTO> getCrew() {
		return crew;
	}

	public void setCrew(List<CrewExternalDTO> crew) {
		this.crew = crew;
	}

}
