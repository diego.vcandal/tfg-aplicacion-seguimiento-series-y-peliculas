package es.udc.fic.diegovcandal.tfg.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CustomListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Service
@Transactional(readOnly = true)
public class SearchServiceImpl implements SearchService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private CustomListDAO customListDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	public Block<User> findUsers(String userName, int pageNumber, int size) {

		Page<User> page = userDao.findByUserNameStartsWith(userName, PageRequest.of(pageNumber, size));

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());
	}

	@Override
	public Block<TitleInfo> findTitles(Long userId, String title, Long idLocale, int page, int size)
			throws UserNotFoundException {

		Visibility visibility = Visibility.PUBLIC;

		if (userId != null)
			visibility = permissionManager.findUser(userId).getRole() == UserRole.ADMIN ? Visibility.PRIVATE
					: Visibility.PUBLIC;

		return titleDAO.getTitles(title, idLocale, visibility, false, null, page, size);

	}

	@Override
	public Block<TitleInfo> findLastTitles(Long idLocale, TitleType titleType) {
		return titleDAO.getTitles("", idLocale, Visibility.PUBLIC, true, titleType, 0, 12);
	}

	@Override
	public Block<People> findPeople(Long userId, String name, int pageNumber, int size) throws UserNotFoundException {

		Page<People> page = null;

		if (userId != null && permissionManager.findUser(userId).getRole() == UserRole.ADMIN)
			page = peopleDAO.findAllPeopleByName(name, PageRequest.of(pageNumber, size));
		else
			page = peopleDAO.findPublicPeopleByName(name, PageRequest.of(pageNumber, size));

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());
	}

	@Override
	public Block<CustomList> findCustomLists(String title, int pageNumber, int size) {

		Page<CustomList> page = customListDAO.findPublicCustomListsByTitle(title, PageRequest.of(pageNumber, size));

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());
	}

}
