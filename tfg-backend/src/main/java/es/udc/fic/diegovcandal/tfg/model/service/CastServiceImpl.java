package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CastDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CastEpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CastSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

@Service
@Transactional()
public class CastServiceImpl implements CastService {

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private CastDAO castDAO;

	@Autowired
	private CastEpisodeDAO castEpisodeDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	public Long addPersonNewCastParticipation(Long userId, Long personId, Long titleId, Cast newCast)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException, TitleNotFoundException,
			CastAlreadyExistsException {

		permissionManager.findAdminUser(userId);

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<Cast> cast = castDAO.findByPeopleIdAndTitleId(people.get(), title.get());

		if (cast.isPresent()) {
			throw new CastAlreadyExistsException();
		}

		int totalEpisodes = 0;
		for (CastSeason castSeason : newCast.getCastSeasons()) {

			Optional<Season> season = seasonDAO.findSeason((TVShow) title.get(),
					castSeason.getSeasonId().getSeasonNumber());

			if (!season.isPresent()) {
				throw new TitleNotFoundException(titleId);
			}

			castSeason.setSeasonId(season.get());

			int episodes = addCastEpisodes(castSeason);
			totalEpisodes += episodes;

			castSeason.setEpisodeCount((short) episodes);

		}

		newCast.setEpisodeCount((short) totalEpisodes);
		newCast.setTitleId(title.get());
		newCast.setPeopleId(people.get());

		return castDAO.save(newCast).getId();

	}

	@Override
	public void updatePersonCastParticipation(Long userId, Long castId, Cast newCast) throws PeopleNotFoundException,
			UserNotFoundException, PermissionException, TitleNotFoundException, CastNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<Cast> cast = castDAO.findById(castId);

		if (!cast.isPresent()) {
			throw new CastNotFoundException();
		}

		int totalEpisodes = 0;
		Set<CastSeason> castSeasons = new HashSet<>();
		for (CastSeason newCastSeason : newCast.getCastSeasons()) {

			byte seasonNumber = newCastSeason.getSeasonId().getSeasonNumber();
			int episodes = 0;

			Optional<CastSeason> castSeason = cast.get().getCastSeasons().stream()
					.filter(cs -> cs.getSeasonId().getSeasonNumber() == seasonNumber).findFirst();

			if (!castSeason.isPresent()) {

				Optional<Season> season = seasonDAO.findSeason(
						tvShowDAO.findById(cast.get().getTitleId().getId()).get(),
						newCastSeason.getSeasonId().getSeasonNumber());

				if (!season.isPresent()) {
					throw new TitleNotFoundException(cast.get().getTitleId());
				}

				newCastSeason.setSeasonId(season.get());
				episodes = addCastEpisodes(newCastSeason);
				newCastSeason.setEpisodeCount((short) episodes);
				newCastSeason.setCastId(cast.get());

				totalEpisodes += episodes;

				castSeasons.add(newCastSeason);

			} else {

				Set<CastEpisode> castEpisodes = new HashSet<>();

				for (CastEpisode newCastEpisode : newCastSeason.getCastEpisodes()) {

					short episodeNumber = newCastEpisode.getEpisodeId().getEpisodeNumber();

					Optional<CastEpisode> castEpisode = castSeason.get().getCastEpisodes().stream()
							.filter(ce -> ce.getEpisodeId().getEpisodeNumber() == episodeNumber).findFirst();

					if (!castEpisode.isPresent()) {

						Optional<Episode> episode = episodeDAO.findEpisode(castSeason.get().getSeasonId(),
								newCastEpisode.getEpisodeId().getEpisodeNumber());

						if (!episode.isPresent()) {
							throw new TitleNotFoundException(cast.get().getTitleId());
						}

						newCastEpisode.setEpisodeId(episode.get());
						newCastEpisode.setCastSeasonId(castSeason.get());

						episodes++;

						castEpisodes.add(newCastEpisode);

					} else {

						castEpisodes.add(castEpisode.get());
					}

					totalEpisodes++;

				}

				castSeason.get().setEpisodeCount((short) episodes);
				castSeason.get().getCastEpisodes().removeAll(castSeason.get().getCastEpisodes());
				castSeason.get().getCastEpisodes().addAll(castEpisodes);

				castSeasons.add(castSeason.get());

			}

		}

		cast.get().setCharacterName(newCast.getCharacterName());

		if (cast.get().getTitleId().getTitleType() == TitleType.TV_SHOW) {
			cast.get().setEpisodeCount((short) totalEpisodes);
			cast.get().getCastSeasons().removeAll(cast.get().getCastSeasons());
			cast.get().getCastSeasons().addAll(castSeasons);
		}

		castDAO.save(cast.get());

	}

	@Override
	public void removePersonCastParticipation(Long userId, Long castId)
			throws UserNotFoundException, PermissionException, CastNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<Cast> cast = castDAO.findById(castId);

		if (!cast.isPresent()) {
			throw new CastNotFoundException();
		}

		castDAO.delete(cast.get());

	}

	@Override
	public Long addPersonEpisodeCastParticipation(Long userId, Long peopleId, Long titleId, byte seasonNumber,
			CastEpisode newCastEpisode)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException, TitleNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<People> people = peopleDAO.findById(peopleId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(peopleId);
		}

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<Cast> cast = castDAO.findByPeopleIdAndTitleId(people.get(), title.get());

		if (cast.isPresent()) {

			Optional<CastSeason> castSeason = cast.get().getCastSeasons().stream()
					.filter(cs -> cs.getSeasonId().getSeasonNumber() == seasonNumber).findFirst();

			if (castSeason.isPresent()) {

				Optional<CastEpisode> castEpisode = castSeason.get().getCastEpisodes().stream().filter(
						ce -> ce.getEpisodeId().getEpisodeNumber() == newCastEpisode.getEpisodeId().getEpisodeNumber())
						.findFirst();

				if (castEpisode.isPresent()) {
					return castEpisode.get().getId();
				}

				addEpisodeToCastSeason(castSeason.get(), newCastEpisode);

				castSeason.get().setEpisodeCount((short) (castSeason.get().getEpisodeCount() + 1));

			} else {

				addCastSeasonWithCastEpisode(cast.get(), seasonNumber, newCastEpisode);

			}

			cast.get().setEpisodeCount((short) (cast.get().getEpisodeCount() + 1));

		} else {

			Cast newCast = new Cast(title.get(), people.get(), (short) 1, newCastEpisode.getCharacterName(), false);

			addCastSeasonWithCastEpisode(newCast, seasonNumber, newCastEpisode);

			return castDAO.save(newCast).getCastSeasons().stream().findFirst().get().getCastEpisodes().stream()
					.findFirst().get().getId();

		}

		return castDAO.save(cast.get()).getCastSeasons().stream()
				.filter(cs -> cs.getSeasonId().getSeasonNumber() == seasonNumber).findFirst().get().getCastEpisodes()
				.stream()
				.filter(ce -> ce.getEpisodeId().getEpisodeNumber() == newCastEpisode.getEpisodeId().getEpisodeNumber())
				.findFirst().get().getId();

	}

	@Override
	public void updatePersonEpisodeCastParticipation(Long userId, Long episodeCastId, String characterName)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException, TitleNotFoundException,
			CastNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<CastEpisode> castEpisode = castEpisodeDAO.findById(episodeCastId);

		if (!castEpisode.isPresent()) {
			throw new CastNotFoundException();
		}

		castEpisode.get().setCharacterName(characterName);

		castEpisodeDAO.save(castEpisode.get());

	}

	@Override
	public void removePersonEpisodeCastParticipation(Long userId, Long episodeCastId) throws UserNotFoundException,
			PermissionException, PeopleNotFoundException, TitleNotFoundException, CastNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<CastEpisode> castEpisode = castEpisodeDAO.findById(episodeCastId);

		if (!castEpisode.isPresent()) {
			throw new CastNotFoundException();
		}

		CastSeason castSeason = castEpisode.get().getCastSeasonId();
		Cast cast = castSeason.getCastId();

		castSeason.getCastEpisodes().remove(castEpisode.get());
		castSeason.setEpisodeCount((short) (castSeason.getEpisodeCount() - 1));
		cast.setEpisodeCount((short) (cast.getEpisodeCount() - 1));

		castDAO.save(cast);

	}

	@Override
	@Transactional(readOnly = true)
	public List<Cast> findMainCast(Long titleId, Long localeId) {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			return new ArrayList<>();
		}

		List<Cast> list = castDAO.findByTitleIdAndMainCastTrue(title.get()).stream().limit(10)
				.collect(Collectors.toList());

		for (Cast cast : list) {
			translatePerson(cast.getPeopleId(), localeId);
		}

		return list;

	}

	@Override
	@Transactional(readOnly = true)
	public List<Cast> findAllCast(Long titleId, Long localeId) {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			return new ArrayList<>();
		}

		List<Cast> list = castDAO.findByTitleId(title.get());

		for (Cast cast : list) {
			translatePerson(cast.getPeopleId(), localeId);
		}

		return list;

	}

	@Override
	@Transactional(readOnly = true)
	public List<CastEpisode> findEpisodeCast(Long episodeId, Long localeId) {

		Optional<Episode> episode = episodeDAO.findById(episodeId);

		if (!episode.isPresent()) {
			return new ArrayList<>();
		}

		List<CastEpisode> list = castEpisodeDAO.findByEpisodeId(episode.get());

		for (CastEpisode cast : list) {
			translatePerson(cast.getCastSeasonId().getCastId().getPeopleId(), localeId);
		}

		return list;

	}

	private void translatePerson(People person, Long localeId) {

		Optional<PeopleTraduction> peopleTraduction = person.getPeopleTraductions().stream()
				.filter(t -> t.getIdLocale() == localeId).findFirst();

		if (!peopleTraduction.isPresent()) {
			return;
		}

		person.setOriginalName(peopleTraduction.get().getName());

	}

	private int addCastEpisodes(CastSeason castSeason) throws TitleNotFoundException {

		int episodes = 0;

		for (CastEpisode castEpisode : castSeason.getCastEpisodes()) {

			addEpisodeToCastSeason(castSeason, castEpisode);

			episodes++;

		}

		return episodes;
	}

	private void addEpisodeToCastSeason(CastSeason castSeason, CastEpisode castEpisode) throws TitleNotFoundException {

		Optional<Episode> episode = episodeDAO.findEpisode(castSeason.getSeasonId(),
				castEpisode.getEpisodeId().getEpisodeNumber());

		if (!episode.isPresent()) {
			throw new TitleNotFoundException(castSeason.getSeasonId().getId());
		}

		castEpisode.setEpisodeId(episode.get());
		castEpisode.setCastSeasonId(castSeason);
		castSeason.getCastEpisodes().add(castEpisode);

	}

	private void addCastSeasonWithCastEpisode(Cast cast, byte seasonNumber, CastEpisode castEpisode)
			throws TitleNotFoundException {

		Optional<Season> season = seasonDAO.findSeason((TVShow) cast.getTitleId(), seasonNumber);

		if (!season.isPresent()) {
			throw new TitleNotFoundException(cast.getTitleId().getId());
		}

		CastSeason newCastSeason = new CastSeason(cast, season.get(), (short) 1);
		addEpisodeToCastSeason(newCastSeason, castEpisode);

		newCastSeason.getCastEpisodes().add(castEpisode);
		castEpisode.setCastSeasonId(newCastSeason);
		cast.getCastSeasons().add(newCastSeason);

	}

}
