package es.udc.fic.diegovcandal.tfg.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.TitleTraductionCompositeKey;

@Entity
@IdClass(TitleTraductionCompositeKey.class)
public class TitleTraduction implements Serializable {

	private static final long serialVersionUID = -4519315040587733601L;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "idTitle")
	private Title idTitle;

	@Id
	private Long idLocale;

	private String titleName;
	private String country;
	private String description;
	private String originalLanguage;

	public TitleTraduction() {
	}

	public TitleTraduction(Title title, Long idLocale, String titleName, String description) {
		this.idTitle = title;
		this.idLocale = idLocale;
		this.titleName = titleName;
		this.description = description;
	}

	public TitleTraduction(Title title, Long idLocale, String titleName, String country, String description,
			String originalLanguage) {
		this.idTitle = title;
		this.idLocale = idLocale;
		this.titleName = titleName;
		this.country = country;
		this.description = description;
		this.originalLanguage = originalLanguage;
	}

	public Title getIdTitle() {
		return idTitle;
	}

	public void setIdTitle(Title idTitle) {
		this.idTitle = idTitle;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOriginalLanguage() {
		return originalLanguage;
	}

	public void setOriginalLanguage(String originalLanguage) {
		this.originalLanguage = originalLanguage;
	}

}
