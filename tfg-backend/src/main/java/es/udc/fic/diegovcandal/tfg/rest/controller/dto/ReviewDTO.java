package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class ReviewDTO {

	private String reviewTitle;
	private String content;
	private byte rating;

	public String getReviewTitle() {
		return reviewTitle;
	}

	public String getContent() {
		return content;
	}

	public byte getRating() {
		return rating;
	}

	public void setReviewTitle(String reviewTitle) {
		this.reviewTitle = reviewTitle;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setRating(byte rating) {
		this.rating = rating;
	}

}
