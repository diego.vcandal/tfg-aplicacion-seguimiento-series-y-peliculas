package es.udc.fic.diegovcandal.tfg.model.restclient;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import es.udc.fic.diegovcandal.tfg.model.dao.FollowingListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.MovieDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.ReviewTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.TitleService;
import es.udc.fic.diegovcandal.tfg.model.utils.FollowingStatus;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.EpisodeSummaryExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.SeasonSummaryExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

@Service
@Transactional
public class TmdbAPIClienteImpl implements TmdbAPIClient {

	@Value("${project.external.api.apiKey}")
	private String apiKey;

	@Value("${project.external.api.apiEndpoint}")
	private String apiEndpoint;

	@Value("${project.external.api.apiImageEndpoint}")
	private String apiImageEndpoint;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TitleService titleService;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private MovieDAO movieDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private UserDao userDao;

	@Autowired
	private ReviewTitleDAO reviewTitleDAO;

	@Autowired
	private FollowingListDAO followingListDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	@Transactional(readOnly = true)
	public MovieExternalDTO findCachedMovie(Long externalId, Long internalId, String acceptLanguage)
			throws TitleNotFoundException {

		MovieExternalDTO movieExternalDTO = null;

		try {
			movieExternalDTO = getExternalMovieDTO(externalId, acceptLanguage);
		} catch (HttpClientErrorException e) {
			throw new TitleNotFoundException(internalId);
		}

		movieExternalDTO.setExternalId(movieExternalDTO.getId());
		movieExternalDTO.setId(internalId);

		return movieExternalDTO;
	}

	@Override
	@Transactional(readOnly = true)
	public MovieExternalDTO findExternalMovie(Long externalId, String acceptLanguage) throws TitleNotFoundException {

		return findCachedMovie(externalId, -1L, acceptLanguage);

	}

	@Override
	@Transactional(readOnly = true)
	public TVShowExternalDTO findCachedTVShow(Long externalId, Long internalId, String acceptLanguage)
			throws TitleNotFoundException {

		TVShowExternalDTO tvShowExternalDTO = null;

		try {
			tvShowExternalDTO = getExternalTVShowDTO(externalId, acceptLanguage);
		} catch (HttpClientErrorException e) {
			throw new TitleNotFoundException(internalId);
		}

		tvShowExternalDTO.setExternalId(tvShowExternalDTO.getId());
		tvShowExternalDTO.setId(internalId);

		return tvShowExternalDTO;
	}

	@Override
	@Transactional(readOnly = true)
	public TVShowExternalDTO findExternalTVShow(Long externalId, String acceptLanguage) throws TitleNotFoundException {

		return findCachedTVShow(externalId, -1L, acceptLanguage);

	}

	@Override
	@Transactional(readOnly = true)
	public SeasonSummaryExternalDTO findExternalSeason(Long externalId, byte seasonNumber, String acceptLanguage)
			throws TitleNotFoundException {

		SeasonSummaryExternalDTO season = null;

		try {
			season = getExternalSeason(externalId, seasonNumber, acceptLanguage);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			throw new TitleNotFoundException(externalId);
		}

		return season;
	}

	@Override
	@Transactional(readOnly = true)
	public byte[] getExternalPortraitImage(String image) throws TitleNotFoundException {

		try {
			return restTemplate
					.exchange(apiImageEndpoint + "/w500" + image, HttpMethod.GET, HttpEntity.EMPTY, byte[].class)
					.getBody();

		} catch (HttpClientErrorException e) {
			return new byte[0];
		}

	}

	@Override
	public TVShow createExternalTVShow(Long userId, Long externalId, String acceptLanguage, boolean automaticCreation)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException,
			PermissionException {

		User user = null;

		if (!automaticCreation)
			user = permissionManager.findAdminUser(userId);

		Long titleId = titleService.titleExistsByExternalId(externalId);

		if (titleId != -1L)
			throw new ExternalIdAlreadyLinkedException();

		TVShowExternalDTO tvShowExternalDTO = null;

		try {
			tvShowExternalDTO = getExternalTVShowDTO(externalId, acceptLanguage);
		} catch (HttpClientErrorException e) {
			throw new TitleNotFoundException(externalId);
		}

		Title title = new Title(externalId, SourceType.EXTERNAL_TMDB, TitleType.TV_SHOW,
				tvShowExternalDTO.getOriginal_name(), null, null, (short) 0);

		TVShow tvShow = new TVShow(title, (short) 0, (short) 0, (byte) tvShowExternalDTO.getNumber_of_seasons(), null,
				null);

		tvShow.setExternalImage(tvShowExternalDTO.getPoster_path());
		tvShow.setAddedByUser(user);
		tvShow.setVisibility(Visibility.PUBLIC);

		Set<Season> seasons = new HashSet<>();

		for (SeasonSummaryExternalDTO season : tvShowExternalDTO.getSeasons()) {
			seasons.add(new Season(season.getId(), tvShow, (byte) season.getSeason_number(), season.getName(), null,
					null, null, null, (short) 0, (short) 0, null));
		}

		tvShow.setSeasons(seasons);

		return tvShowDAO.save(tvShow);

	}

	@Override
	public Movie createExternalMovie(Long userId, Long externalId, String acceptLanguage, boolean automaticCreation)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException,
			PermissionException {

		User user = null;

		if (!automaticCreation)
			user = permissionManager.findAdminUser(userId);

		Long titleId = titleService.titleExistsByExternalId(externalId);

		if (titleId != -1L)
			throw new ExternalIdAlreadyLinkedException();

		MovieExternalDTO movieExternalDTO = null;

		try {
			movieExternalDTO = getExternalMovieDTO(externalId, acceptLanguage);
		} catch (HttpClientErrorException e) {
			throw new TitleNotFoundException(externalId);
		}

		Movie movie = new Movie(externalId, SourceType.EXTERNAL_TMDB, movieExternalDTO.getOriginal_title(), null, null,
				(short) 0, (short) 0);

		movie.setExternalImage(movieExternalDTO.getPoster_path());
		movie.setAddedByUser(user);
		movie.setVisibility(Visibility.PUBLIC);

		return movieDAO.save(movie);

	}

	@Override
	public Season createExternalSeason(Long userId, Long externalId, Long externalSeasonId, byte seasonNumber,
			String acceptLanguage) throws TitleNotFoundException, ExternalIdAlreadyLinkedException,
			UserNotFoundException, PermissionException {

		Long seasonId = titleService.seasonExistsByExternalId(externalSeasonId);

		if (seasonId != -1L)
			throw new ExternalIdAlreadyLinkedException();

		permissionManager.findAdminUser(userId);

		Optional<Title> tvshowOptional = tvShowDAO.findByExternalId(externalId);
		TVShow tvShow = null;

		if (!tvshowOptional.isPresent()) {
			tvShow = createExternalTVShow(userId, externalId, acceptLanguage, false);

			Optional<Season> season = tvShow.getSeasons().stream()
					.filter(s -> s.getExternalId().equals(externalSeasonId)).findFirst();

			if (!season.isPresent())
				throw new TitleNotFoundException(externalSeasonId);

			return season.get();

		} else {
			tvShow = (TVShow) tvshowOptional.get();
		}

		SeasonSummaryExternalDTO seasonExternalDTO = null;

		try {
			seasonExternalDTO = getExternalSeason(externalId, seasonNumber, acceptLanguage);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			throw new TitleNotFoundException(externalSeasonId);
		}

		tvShow.setSeasonNumber((byte) (tvShow.getSeasonNumber() + (seasonNumber == 0 ? 0 : 1)));
		tvShowDAO.save(tvShow);

		return seasonDAO.save(new Season(externalSeasonId, tvShow, seasonNumber, seasonExternalDTO.getName(), null,
				null, null, null, (short) 0, seasonExternalDTO.getEpisode_count(), null));

	}

	@Override
	public void createExternalEpisodes(Long userId, Long externalId, Long externalSeasonId, byte seasonNumber,
			String acceptLanguage) throws TitleNotFoundException, ExternalIdAlreadyLinkedException,
			UserNotFoundException, PermissionException {

		Optional<Season> seasonOptional = seasonDAO.findByExternalId(externalSeasonId);
		Season season = null;

		if (!seasonOptional.isPresent()) {
			season = createExternalSeason(userId, externalId, externalSeasonId, seasonNumber, acceptLanguage);
		} else {
			season = seasonOptional.get();
		}

		permissionManager.findAdminUser(userId);

		SeasonSummaryExternalDTO seasonExternalDTO = null;

		try {
			seasonExternalDTO = getExternalSeason(externalId, seasonOptional.get().getSeasonNumber(), acceptLanguage);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			throw new TitleNotFoundException(externalSeasonId);
		}

		short episodesAdded = 0;
		for (EpisodeSummaryExternalDTO episode : seasonExternalDTO.getEpisodes()) {
			if (season.getEpisodes().stream().noneMatch(e -> e.getExternalId().equals(episode.getId()))) {
				episodesAdded++;
				season.getEpisodes().add(new Episode(episode.getId(), season, episode.getEpisode_number(),
						episode.getName(), null, null, (short) 0, null));
			}
		}

		TVShow tvShow = season.getTvShow();
		tvShow.setEpisodeCount((short) (tvShow.getEpisodeCount() + episodesAdded));
		tvShowDAO.save(tvShow);

		season.setEpisodeCount((short) (season.getEpisodeCount() + episodesAdded));
		seasonDAO.save(season);

	}

	@Override
	public void createExternalEpisode(Long userId, Long externalId, Long externalSeasonId, byte seasonNumber,
			short episodeNumber, String acceptLanguage) throws TitleNotFoundException, ExternalIdAlreadyLinkedException,
			UserNotFoundException, PermissionException {

		Optional<Season> seasonOptional = seasonDAO.findByExternalId(externalSeasonId);
		Season season = null;

		if (!seasonOptional.isPresent()) {
			season = createExternalSeason(userId, externalId, externalSeasonId, seasonNumber, acceptLanguage);
		} else {
			season = seasonOptional.get();
		}

		permissionManager.findAdminUser(userId);

		EpisodeSummaryExternalDTO episodeSummaryExternalDTO = null;

		try {
			episodeSummaryExternalDTO = getExternalEpisode(externalId, season.getSeasonNumber(), episodeNumber,
					acceptLanguage);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			throw new TitleNotFoundException(externalSeasonId);
		}

		Long episodeId = episodeSummaryExternalDTO.getId();
		if (season.getEpisodes().stream().anyMatch(e -> e.getExternalId().equals(episodeId)))
			return;

		season.getEpisodes()
				.add(new Episode(episodeSummaryExternalDTO.getId(), season,
						episodeSummaryExternalDTO.getEpisode_number(), episodeSummaryExternalDTO.getName(), null, null,
						(short) 0, null));

		TVShow tvShow = season.getTvShow();
		tvShow.setEpisodeCount((short) (tvShow.getEpisodeCount() + 1));
		tvShowDAO.save(tvShow);

		season.setEpisodeCount((short) (season.getEpisodeCount() + 1));

		seasonDAO.save(season);

	}

	@Override
	public void rateTitle(Long userId, Long titleId, byte rating, TitleType titleType, String acceptLanguage)
			throws IncorrectRatingException, TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException {

		Optional<Title> foundTitle = titleDAO.findByExternalId(titleId);

		if (foundTitle.isPresent()) {
			throw new IncorrectOperationException();
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Title title = null;

		if (titleType == TitleType.MOVIE) {
			title = createExternalMovie(userId, titleId, acceptLanguage, true);
		} else if (titleType == TitleType.TV_SHOW) {
			title = createExternalTVShow(userId, titleId, acceptLanguage, true);
		}

		if (rating < 0 || rating > 10) {
			throw new IncorrectRatingException();
		}

		ReviewTitle newRating = new ReviewTitle(title, user.get(), rating, false, null, null, 0, LocalDate.now());

		float oldAvgRating = title.getAvgRating();
		int timesRated = title.getTimesRated();

		title.setAvgRating(((oldAvgRating * timesRated) + rating) / (timesRated + 1));
		title.setTimesRated(timesRated + 1);

		titleDAO.save(title);
		reviewTitleDAO.save(newRating);
	}

	@Override
	public ReviewTitle addNewReview(Long userId, Long titleId, String reviewTitle, String content, byte rating,
			TitleType titleType, String acceptLanguage) throws IncorrectRatingException, TitleNotFoundException,
			UserNotFoundException, ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException {

		Optional<Title> foundTitle = titleDAO.findByExternalId(titleId);

		if (foundTitle.isPresent()) {
			throw new IncorrectOperationException();
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Title title = null;

		if (titleType == TitleType.MOVIE) {
			title = createExternalMovie(userId, titleId, acceptLanguage, true);
		} else if (titleType == TitleType.TV_SHOW) {
			title = createExternalTVShow(userId, titleId, acceptLanguage, true);
		}

		if (rating < 0 || rating > 10) {
			throw new IncorrectRatingException();
		}

		ReviewTitle newRewTitle = new ReviewTitle(title, user.get(), rating, true, reviewTitle, content, 0,
				LocalDate.now());

		float oldAvgRating = title.getAvgRating();
		int timesRated = title.getTimesRated();

		title.setAvgRating(((oldAvgRating * timesRated) + rating) / (timesRated + 1));
		title.setTimesRated(timesRated + 1);

		titleDAO.save(title);
		return reviewTitleDAO.save(newRewTitle);

	}

	@Override
	public void addToFollowingList(Long userId, Long titleId, Long status, TitleType titleType, String acceptLanguage)
			throws IncorrectOperationException, UserNotFoundException, TitleNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException {

		Optional<Title> foundTitle = titleDAO.findByExternalId(titleId);

		if (foundTitle.isPresent()) {
			throw new IncorrectOperationException();
		}

		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		Title title = null;

		if (titleType == TitleType.MOVIE)
			title = createExternalMovie(userId, titleId, acceptLanguage, true);
		else if (titleType == TitleType.TV_SHOW)
			title = createExternalTVShow(userId, titleId, acceptLanguage, true);

		FollowingList list = user.get().getFollowingList();

		list.getTitles().add(new FollowingListTitle(title, list, FollowingStatus.valueOfStatusOrDefault(status),
				LocalDateTime.now(), null, null, (byte) -1, false));

		if (titleType == TitleType.MOVIE)
			list.setMovieCount((short) (list.getMovieCount() + 1));
		else
			list.setTvshowCount((short) (list.getTvshowCount() + 1));

		list.setLastUpdated(LocalDateTime.now());

		titleDAO.save(title);
		followingListDAO.save(list);

	}

	private MovieExternalDTO getExternalMovieDTO(Long externalId, String acceptLanguage) {

		return restTemplate
				.exchange(apiEndpoint + "/movie/" + externalId + "?api_key=" + apiKey + "&language=" + acceptLanguage,
						HttpMethod.GET, HttpEntity.EMPTY, MovieExternalDTO.class)
				.getBody();

	}

	private TVShowExternalDTO getExternalTVShowDTO(Long externalId, String acceptLanguage) {

		return restTemplate
				.exchange(apiEndpoint + "/tv/" + externalId + "?api_key=" + apiKey + "&language=" + acceptLanguage,
						HttpMethod.GET, HttpEntity.EMPTY, TVShowExternalDTO.class)
				.getBody();

	}

	private SeasonSummaryExternalDTO getExternalSeason(Long externalId, byte seasonNumber, String acceptLanguage) {

		return restTemplate
				.exchange(
						apiEndpoint + "/tv/" + externalId + "/season/" + seasonNumber + "?api_key=" + apiKey
								+ "&language=" + acceptLanguage,
						HttpMethod.GET, HttpEntity.EMPTY, SeasonSummaryExternalDTO.class)
				.getBody();

	}

	private EpisodeSummaryExternalDTO getExternalEpisode(Long externalId, byte seasonNumber, short episodeNumber,
			String acceptLanguage) {

		return restTemplate.exchange(
				apiEndpoint + "/tv/" + externalId + "/season/" + seasonNumber + "/episode/" + episodeNumber
						+ "?api_key=" + apiKey + "&language=" + acceptLanguage,
				HttpMethod.GET, HttpEntity.EMPTY, EpisodeSummaryExternalDTO.class).getBody();

	}

}
