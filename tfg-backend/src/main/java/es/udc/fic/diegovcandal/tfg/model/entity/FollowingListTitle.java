package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.utils.FollowingStatus;

@Entity
public class FollowingListTitle {

	private Long id;
	private Title titleId;
	private FollowingList listId;
	private FollowingStatus status;
	private LocalDateTime dateAdded;
	private LocalDate dateStarted;
	private LocalDate dateFinalized;
	private short rating;
	private boolean hasReview;

	public FollowingListTitle() {
	}

	public FollowingListTitle(Title titleId, FollowingList listId, FollowingStatus status, LocalDateTime dateAdded,
			LocalDate dateStarted, LocalDate dateFinalized, short rating, boolean hasReview) {
		this.titleId = titleId;
		this.listId = listId;
		this.status = status;
		this.dateAdded = dateAdded;
		this.dateStarted = dateStarted;
		this.dateFinalized = dateFinalized;
		this.rating = rating;
		this.hasReview = hasReview;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "titleId")
	public Title getTitleId() {
		return titleId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "listId")
	public FollowingList getListId() {
		return listId;
	}

	public FollowingStatus getStatus() {
		return status;
	}

	public LocalDateTime getDateAdded() {
		return dateAdded;
	}

	public LocalDate getDateStarted() {
		return dateStarted;
	}

	public LocalDate getDateFinalized() {
		return dateFinalized;
	}

	public short getRating() {
		return rating;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitleId(Title titleId) {
		this.titleId = titleId;
	}

	public void setListId(FollowingList listId) {
		this.listId = listId;
	}

	public void setStatus(FollowingStatus status) {
		this.status = status;
	}

	public void setDateAdded(LocalDateTime dateAdded) {
		this.dateAdded = dateAdded;
	}

	public void setDateStarted(LocalDate dateStarted) {
		this.dateStarted = dateStarted;
	}

	public void setDateFinalized(LocalDate dateFinalized) {
		this.dateFinalized = dateFinalized;
	}

	public void setRating(short rating) {
		this.rating = rating;
	}

	public boolean isHasReview() {
		return hasReview;
	}

	public void setHasReview(boolean hasReview) {
		this.hasReview = hasReview;
	}

}
