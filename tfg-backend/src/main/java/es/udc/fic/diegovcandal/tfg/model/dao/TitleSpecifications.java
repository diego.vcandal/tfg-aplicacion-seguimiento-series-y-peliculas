package es.udc.fic.diegovcandal.tfg.model.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

public class TitleSpecifications {

	public static Specification<Title> getFindTitleSpecificationIds(String titleName, Visibility visibility,
			TitleType titleType) {

		return new Specification<Title>() {

			private static final long serialVersionUID = -7139771009385016579L;

			@Override
			public Predicate toPredicate(Root<Title> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

				Join<Title, TitleTraduction> join = root.join("titleTraductions", JoinType.LEFT);

				Predicate pred = null;

				if (visibility == Visibility.PUBLIC) {
					pred = criteriaBuilder.and(
							criteriaBuilder.or(criteriaBuilder.like(root.get("originalTitle"), "%" + titleName + "%"),
									criteriaBuilder.like(join.get("titleName"), "%" + titleName + "%")),
							criteriaBuilder.equal(root.get("visibility"), Visibility.PUBLIC));
				} else {
					pred = criteriaBuilder.and(
							criteriaBuilder.or(criteriaBuilder.like(root.get("originalTitle"), "%" + titleName + "%"),
									criteriaBuilder.like(join.get("titleName"), "%" + titleName + "%")));
				}

				if (titleType != null)
					return criteriaBuilder.and(pred, criteriaBuilder.equal(root.get("titleType"), titleType.getId()));
				else
					return pred;

			}

		};

	}

}
