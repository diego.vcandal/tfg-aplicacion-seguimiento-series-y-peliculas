package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class CreatePersonCastDTO {

	private Long id;
	private Long titleId;
	private Long personId;
	private String name;
	private boolean isMainCast;

	private List<CreateSeasonCastDTO> seasons;

	public CreatePersonCastDTO(Long id, Long titleId, Long personId, String name, boolean isMainCast,
			List<CreateSeasonCastDTO> seasons) {
		this.id = id;
		this.titleId = titleId;
		this.personId = personId;
		this.seasons = seasons;
		this.name = name;
		this.isMainCast = isMainCast;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<CreateSeasonCastDTO> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<CreateSeasonCastDTO> seasons) {
		this.seasons = seasons;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMainCast() {
		return isMainCast;
	}

	public void setMainCast(boolean isMainCast) {
		this.isMainCast = isMainCast;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
