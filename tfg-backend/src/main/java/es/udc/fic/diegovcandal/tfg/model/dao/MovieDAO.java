package es.udc.fic.diegovcandal.tfg.model.dao;

import es.udc.fic.diegovcandal.tfg.model.entity.Movie;

public interface MovieDAO extends TitleBaseDAO<Movie> {

}
