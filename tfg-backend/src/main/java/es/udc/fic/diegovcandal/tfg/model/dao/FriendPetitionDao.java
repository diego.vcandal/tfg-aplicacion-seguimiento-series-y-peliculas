package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.FriendPetition;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.FriendPetitionCompositeKey;

public interface FriendPetitionDao extends PagingAndSortingRepository<FriendPetition, FriendPetitionCompositeKey> {

	List<FriendPetition> findByOriginUserOrderByDateDesc(User originUser);

	List<FriendPetition> findByDestinationUserOrderByDateDesc(User destinationUser);

}
