package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class CastDTO {

	private Long id;
	private Long peopleId;
	private Long externalId;
	private String name;
	private String charName;
	private boolean image;
	private String externalImage;
	private Long sourceType;
	private boolean isMainCast;
	private short episodeNumber;

	public CastDTO(Long id, Long peopleId, Long externalId, String name, String charName, boolean image,
			String externalImage, Long sourceType, boolean isMainCast, short episodeNumber) {
		this.id = id;
		this.peopleId = peopleId;
		this.name = name;
		this.image = image;
		this.charName = charName;
		this.externalImage = externalImage;
		this.sourceType = sourceType;
		this.isMainCast = isMainCast;
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isImage() {
		return image;
	}

	public String getCharName() {
		return charName;
	}

	public void setCharName(String charName) {
		this.charName = charName;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public String getExternalImage() {
		return externalImage;
	}

	public void setExternalImage(String externalImage) {
		this.externalImage = externalImage;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public boolean isMainCast() {
		return isMainCast;
	}

	public void setMainCast(boolean isMainCast) {
		this.isMainCast = isMainCast;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public short getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(short episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

}
