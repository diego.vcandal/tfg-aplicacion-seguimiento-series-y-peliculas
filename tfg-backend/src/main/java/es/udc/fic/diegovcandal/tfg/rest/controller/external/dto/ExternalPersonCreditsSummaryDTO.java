package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class ExternalPersonCreditsSummaryDTO {

	private List<ExternalPersonCastSummaryDTO> cast;
	private List<ExternalPersonCrewSummaryDTO> crew;

	public List<ExternalPersonCastSummaryDTO> getCast() {
		return cast;
	}

	public List<ExternalPersonCrewSummaryDTO> getCrew() {
		return crew;
	}

	public void setCast(List<ExternalPersonCastSummaryDTO> cast) {
		this.cast = cast;
	}

	public void setCrew(List<ExternalPersonCrewSummaryDTO> crew) {
		this.crew = crew;
	}

}
