package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;

public interface CastEpisodeDAO extends PagingAndSortingRepository<CastEpisode, Long> {

	CastEpisode findFirstByOrderByIdAsc();

	List<CastEpisode> findByEpisodeId(Episode episodeId);

}
