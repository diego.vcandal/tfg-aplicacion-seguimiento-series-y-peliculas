package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.GenreDao;
import es.udc.fic.diegovcandal.tfg.model.dao.MovieDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.EpisodeTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.Genre;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.SeasonTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleImage;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithSeasons;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.GenreDoesntExistException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {

	@Autowired
	private MovieDAO movieDAO;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private GenreDao genreDao;

	@Override
	@Transactional(readOnly = true)
	public Long titleExistsByExternalId(Long externalId) {

		Optional<Title> title = titleDAO.findByExternalId(externalId);

		if (!title.isPresent()) {
			return -1L;
		}

		return title.get().getId();

	}

	@Override
	@Transactional(readOnly = true)
	public Title getTitleById(Long id) {

		Optional<Title> title = titleDAO.findById(id);

		if (!title.isPresent()) {
			return null;
		}

		return title.get();

	}

	@Override
	@Transactional(readOnly = true)
	public Long seasonExistsByExternalId(Long externalId) {

		Optional<Season> season = seasonDAO.findByExternalId(externalId);

		if (!season.isPresent()) {
			return -1L;
		}

		return season.get().getId();

	}

	@Override
	@Transactional(readOnly = true)
	public Long episodeExistsByExternalId(Long externalId) {

		Optional<Episode> episode = episodeDAO.findByExternalId(externalId);

		if (!episode.isPresent()) {
			return -1L;
		}

		return episode.get().getId();

	}

	@Override
	@Transactional(readOnly = true)
	public TitleWithTraduction<Movie> getMovieDetails(Long userId, Long id, Long localeId)
			throws TitleNotFoundException, UserNotFoundException {

		Optional<TitleWithTraduction<Movie>> title = movieDAO.findTitleWithTraduction(id, localeId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		if (!(title.get().getTitle() instanceof Movie)) {
			throw new TitleNotFoundException(id);
		}

		boolean isAdmin = false;

		if (userId != null)
			isAdmin = permissionManager.findUser(userId).getRole() == UserRole.ADMIN;

		if (!isAdmin && title.get().getTitle().getVisibility() == Visibility.PRIVATE)
			throw new TitleNotFoundException(id);

		return title.get();
	}

	@Override
	@Transactional(readOnly = true)
	public TitleWithTraduction<TVShow> getTVShowDetails(Long userId, Long id, Long localeId)
			throws TitleNotFoundException, UserNotFoundException {

		Optional<TitleWithTraduction<TVShow>> title = tvShowDAO.findTitleWithTraduction(id, localeId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		if (!(title.get().getTitle() instanceof TVShow)) {
			throw new TitleNotFoundException(id);
		}

		boolean isAdmin = false;

		if (userId != null)
			isAdmin = permissionManager.findUser(userId).getRole() == UserRole.ADMIN;

		if (!isAdmin && title.get().getTitle().getVisibility() == Visibility.PRIVATE)
			throw new TitleNotFoundException(id);

		return title.get();
	}

	@Override
	@Transactional(readOnly = true)
	public TVShowWithSeasons getSeasons(Long userId, Long id, Long localeId) throws UserNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(id);

		if (!title.isPresent()) {
			return new TVShowWithSeasons(null, new ArrayList<>());
		}

		boolean isAdmin = false;

		if (userId != null)
			isAdmin = permissionManager.findUser(userId).getRole() == UserRole.ADMIN;

		if (!isAdmin && title.get().getVisibility() == Visibility.PRIVATE)
			return new TVShowWithSeasons(null, new ArrayList<>());

		return new TVShowWithSeasons(title.get(), seasonDAO.findAllSeasonsWithTraduction(title.get(), localeId));
	}

	@Override
	@Transactional(readOnly = true)
	public SeasonWithTraduction getFullSeason(Long userId, Long id, byte seasonNumber, Long localeId)
			throws TitleNotFoundException, UserNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		boolean isAdmin = false;

		if (userId != null)
			isAdmin = permissionManager.findUser(userId).getRole() == UserRole.ADMIN;

		if (!isAdmin && title.get().getVisibility() == Visibility.PRIVATE)
			throw new TitleNotFoundException(id);

		Optional<SeasonWithTraduction> season = seasonDAO.findSeasonWithTraduction(title.get(), seasonNumber, localeId);

		if (!season.isPresent()) {

			if (title.get().getSourceType() != SourceType.INTERNAL) {

				Season empty = new Season();
				empty.setTvShow(title.get());

				SeasonWithTraduction seasonTraductionEmpty = new SeasonWithTraduction(empty, null);
				seasonTraductionEmpty.setNoContent(true);

				return seasonTraductionEmpty;
			}

			throw new TitleNotFoundException(id);

		}

		season.get().setEpisodes(episodeDAO.findAllEpisodeWithTraduction(season.get().getSeason(), localeId));

		return season.get();
	}

	@Override
	@Transactional(readOnly = true)
	public TVShowWithSeasons getLastSeasonPreview(Long userId, Long id, Long localeId)
			throws TitleNotFoundException, UserNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		boolean isAdmin = false;

		if (userId != null)
			isAdmin = permissionManager.findUser(userId).getRole() == UserRole.ADMIN;

		if (!isAdmin && title.get().getVisibility() == Visibility.PRIVATE)
			throw new TitleNotFoundException(id);

		Optional<SeasonWithTraduction> season = seasonDAO.findSeasonWithTraduction(title.get(),
				title.get().getSeasonNumber(), localeId);

		if (!season.isPresent()) {
			return new TVShowWithSeasons(title.get(), new ArrayList<>());
		}

		return new TVShowWithSeasons(title.get(), Arrays.asList(season.get()));
	}

	@Override
	public Long createMovie(Long userId, Movie movie) throws ExternalIdAlreadyLinkedException, UserNotFoundException,
			PermissionException, GenreDoesntExistException {

		if (movieDAO.existsByExternalId(movie.getExternalId())) {
			throw new ExternalIdAlreadyLinkedException();
		}

		for (Genre genre : movie.getGenres()) {
			if (!genreDao.existsById(genre.getId())) {
				throw new GenreDoesntExistException();
			}
		}

		movie.setAddedByUser(permissionManager.findAdminUser(userId));
		movie.setTitleType(TitleType.MOVIE);
		movie.setVisibility(Visibility.PRIVATE);

		return movieDAO.save(movie).getId();

	}

	@Override
	public void updateMovie(Long userId, Long movieId, Movie updatedMovie)
			throws UserNotFoundException, PermissionException, GenreDoesntExistException, TitleNotFoundException {

		Optional<Movie> movieOptional = movieDAO.findById(movieId);

		if (!movieOptional.isPresent()) {
			throw new TitleNotFoundException(movieId);
		}

		permissionManager.findAdminUser(userId);

		Movie movie = movieOptional.get();

		checkGenres(updatedMovie);

		updateTraductions(movie, updatedMovie);

		if (updatedMovie.getTitleImages().size() > 0) {
			updateHeaderAndPortrait(movie, updatedMovie);
		}

		movie.setOriginalTitle(updatedMovie.getOriginalTitle());
		movie.setOriginalDescription(updatedMovie.getOriginalDescription());
		movie.setCompanyName(updatedMovie.getCompanyName());
		movie.setYear(updatedMovie.getYear());
		movie.setDuration(updatedMovie.getDuration());
		movie.setGenres(updatedMovie.getGenres());

		movieDAO.save(movie);
	}

	@Override
	public void makeTitlePublic(Long userId, Long titleId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		permissionManager.findAdminUser(userId);

		title.get().setVisibility(Visibility.PUBLIC);

		titleDAO.save(title.get());
	}

	@Override
	public void setTitleForDeletion(Long userId, Long titleId, boolean setForDeletion)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		permissionManager.findAdminUser(userId);

		title.get().setSetForDelete(setForDeletion);
		title.get().setSourceType(setForDeletion ? SourceType.EXTERNAL_TMDB : SourceType.INTERNAL);

		titleDAO.save(title.get());
	}

	@Override
	public void deleteTitle(Long userId, Long titleId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		if (!title.get().isSetForDelete()) {
			throw new PermissionException();
		}

		permissionManager.findAdminUser(userId);

		titleDAO.delete(title.get());
	}

	@Override
	public void changeSourceType(Long userId, Long titleId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		permissionManager.findAdminUser(userId);

		if (title.get().isSetForDelete())
			return;

		if (title.get().getVisibility() == Visibility.PRIVATE)
			return;

		title.get().setSourceType(
				title.get().getSourceType() == SourceType.INTERNAL ? SourceType.EXTERNAL_TMDB : SourceType.INTERNAL);

		if (title.get().getTitleType() == TitleType.TV_SHOW) {

			byte seasonCount = 0;
			short episodeCount = 0;

			for (Season season : ((TVShow) title.get()).getSeasons()) {
				seasonCount += season.getSeasonNumber() == 0 ? 0 : 1;
				episodeCount += season.getEpisodes().size();
			}

			((TVShow) title.get()).setSeasonNumber(seasonCount);
			((TVShow) title.get()).setEpisodeCount(episodeCount);

			tvShowDAO.save((TVShow) title.get());
			return;

		}

		titleDAO.save(title.get());
	}

	@Override
	public Long createTVShow(Long userId, TVShow tvShow) throws ExternalIdAlreadyLinkedException, UserNotFoundException,
			PermissionException, GenreDoesntExistException {

		if (tvShowDAO.existsByExternalId(tvShow.getExternalId())) {
			throw new ExternalIdAlreadyLinkedException();
		}

		for (Genre genre : tvShow.getGenres()) {
			if (!genreDao.existsById(genre.getId())) {
				throw new GenreDoesntExistException();
			}
		}

		tvShow.setAddedByUser(permissionManager.findAdminUser(userId));
		tvShow.setTitleType(TitleType.TV_SHOW);
		tvShow.setVisibility(Visibility.PRIVATE);

		return tvShowDAO.save(tvShow).getId();

	}

	@Override
	public void updateTVShow(Long userId, Long tvShowId, TVShow updatedTVShow)
			throws UserNotFoundException, PermissionException, GenreDoesntExistException, TitleNotFoundException {

		Optional<TVShow> tvShowOptional = tvShowDAO.findById(tvShowId);

		if (!tvShowOptional.isPresent()) {
			throw new TitleNotFoundException(tvShowId);
		}

		permissionManager.findAdminUser(userId);

		TVShow tvShow = tvShowOptional.get();

		checkGenres(updatedTVShow);

		updateTraductions(tvShow, updatedTVShow);

		if (updatedTVShow.getTitleImages().size() > 0) {
			updateHeaderAndPortrait(tvShow, updatedTVShow);
		}

		tvShow.setOriginalTitle(updatedTVShow.getOriginalTitle());
		tvShow.setOriginalDescription(updatedTVShow.getOriginalDescription());
		tvShow.setCompanyName(updatedTVShow.getCompanyName());
		tvShow.setYear(updatedTVShow.getYear());
		tvShow.setGenres(updatedTVShow.getGenres());

		tvShow.setFirstAirDate(updatedTVShow.getFirstAirDate());
		tvShow.setLastAirDate(updatedTVShow.getLastAirDate());

		tvShowDAO.save(tvShow);
	}

	@Override
	@Transactional(readOnly = true)
	public Movie getInternalMovieDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		permissionManager.findAdminUser(userId);

		Optional<Movie> title = movieDAO.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		return title.get();
	}

	@Override
	@Transactional(readOnly = true)
	public TVShow getInternalTVShowDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		permissionManager.findAdminUser(userId);

		Optional<TVShow> title = tvShowDAO.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		return title.get();
	}

	@Override
	public Long createSeason(Long userId, Long titleId, Season season)
			throws UserNotFoundException, PermissionException, TitleNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		permissionManager.findAdminUser(userId);

		title.get().setSeasonNumber((byte) (title.get().getSeasonNumber() + (season.getSeasonNumber() == 0 ? 0 : 1)));
		season.setTvShow(title.get());

		titleDAO.save(title.get());

		return seasonDAO.save(season).getId();

	}

	@Override
	public void updateSeason(Long userId, Long seasonId, Season updatedSeason)
			throws UserNotFoundException, PermissionException, TitleNotFoundException {

		Optional<Season> optional = seasonDAO.findById(seasonId);

		if (!optional.isPresent()) {
			throw new TitleNotFoundException(seasonId);
		}

		permissionManager.findAdminUser(userId);

		Season season = optional.get();

		updateTraductions(season, updatedSeason);

		season.setImage(updatedSeason.getImage());
		season.setOriginalTitle(updatedSeason.getOriginalTitle());
		season.setOriginalDescription(updatedSeason.getOriginalDescription());
		season.setFirstAirDate(updatedSeason.getFirstAirDate());
		season.setLastAirDate(updatedSeason.getLastAirDate());
		season.setSeasonNumber(updatedSeason.getSeasonNumber());

		seasonDAO.save(season);
	}

	@Override
	public void setSeasonForDeletion(Long userId, Long seasonId, boolean setForDeletion)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Optional<Season> optional = seasonDAO.findById(seasonId);

		if (!optional.isPresent()) {
			throw new TitleNotFoundException(seasonId);
		}

		permissionManager.findAdminUser(userId);

		optional.get().setSetForDelete(setForDeletion);

		seasonDAO.save(optional.get());
	}

	@Override
	@Transactional(readOnly = true)
	public TVShowWithInternalSeason getInternalSeasonDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		permissionManager.findAdminUser(userId);

		Optional<Season> optional = seasonDAO.findById(id);

		if (!optional.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		TVShow title = optional.get().getTvShow();
		optional.get().setSourceType(title.getSourceType());

		return new TVShowWithInternalSeason(title.getOriginalTitle(), optional.get());
	}

	@Override
	public Long createEpisode(Long userId, Long titleId, byte seasonNumber, Episode episode)
			throws UserNotFoundException, PermissionException, TitleNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<Season> season = seasonDAO.findSeason(title.get(), seasonNumber);

		if (!season.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		permissionManager.findAdminUser(userId);

		season.get().setEpisodeCount((short) (season.get().getEpisodeCount() + 1));
		title.get().setEpisodeCount((byte) (title.get().getEpisodeCount() + 1));
		episode.setSeason(season.get());

		titleDAO.save(title.get());
		seasonDAO.save(season.get());

		return episodeDAO.save(episode).getId();

	}

	@Override
	public void updateEpisode(Long userId, Long episodeId, Episode updatedEpisode)
			throws UserNotFoundException, PermissionException, TitleNotFoundException {

		Optional<Episode> optional = episodeDAO.findById(episodeId);

		if (!optional.isPresent()) {
			throw new TitleNotFoundException(episodeId);
		}

		permissionManager.findAdminUser(userId);

		Episode episode = optional.get();

		updateTraductions(episode, updatedEpisode);

		episode.setImage(updatedEpisode.getImage());
		episode.setOriginalTitle(updatedEpisode.getOriginalTitle());
		episode.setOriginalDescription(updatedEpisode.getOriginalDescription());
		episode.setAirDate(updatedEpisode.getAirDate());
		episode.setEpisodeNumber(updatedEpisode.getEpisodeNumber());

		episodeDAO.save(episode);
	}

	@Override
	public void setEpisodeForDeletion(Long userId, Long episodeId, boolean setForDeletion)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Optional<Episode> optional = episodeDAO.findById(episodeId);

		if (!optional.isPresent()) {
			throw new TitleNotFoundException(episodeId);
		}

		permissionManager.findAdminUser(userId);

		optional.get().setSetForDelete(setForDeletion);

		episodeDAO.save(optional.get());
	}

	@Override
	@Transactional(readOnly = true)
	public TVShowWithInternalEpisode getInternalEpisodeDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		permissionManager.findAdminUser(userId);

		Optional<Episode> optional = episodeDAO.findById(id);

		if (!optional.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		Season season = optional.get().getSeason();
		TVShow tvShow = season.getTvShow();
		optional.get().setSourceType(tvShow.getSourceType());

		return new TVShowWithInternalEpisode(tvShow.getOriginalTitle(), season.getOriginalTitle(), optional.get());
	}

	private void updateTraductions(Title title, Title updatedTitle) {
		title.getTitleTraductions().removeAll(title.getTitleTraductions());
		for (TitleTraduction traduction : updatedTitle.getTitleTraductions()) {
			traduction.setIdTitle(title);
			title.getTitleTraductions().add(traduction);
		}
	}

	private void updateTraductions(Season season, Season updatedSeason) {
		season.getSeasonTraductions().removeAll(season.getSeasonTraductions());
		for (SeasonTraduction traduction : updatedSeason.getSeasonTraductions()) {
			traduction.setIdSeason(season);
			season.getSeasonTraductions().add(traduction);
		}
	}

	private void updateTraductions(Episode episode, Episode updatedEpisode) {
		episode.getEpisodeTraductions().removeAll(episode.getEpisodeTraductions());
		for (EpisodeTraduction traduction : updatedEpisode.getEpisodeTraductions()) {
			traduction.setIdEpisode(episode);
			episode.getEpisodeTraductions().add(traduction);
		}
	}

	private void checkGenres(Title updatedTitle) throws GenreDoesntExistException {
		for (Genre genre : updatedTitle.getGenres()) {
			if (!genreDao.existsById(genre.getId())) {
				throw new GenreDoesntExistException();
			}
		}
	}

	private void updateHeaderAndPortrait(Title title, Title updatedTitle) {

		Optional<TitleImage> newHeader = updatedTitle.getTitleImages().stream().filter(i -> i.isHeader()).findFirst();
		Optional<TitleImage> newPortrait = updatedTitle.getTitleImages().stream().filter(i -> i.isPortrait())
				.findFirst();

		TitleImage headerImage = null;
		TitleImage portraitImage = null;

		for (TitleImage img : title.getTitleImages()) {

			if (img.isHeader())
				headerImage = img;

			if (img.isPortrait())
				portraitImage = img;

		}

		if (newHeader.isPresent()) {
			if (headerImage == null)
				title.getTitleImages().add(new TitleImage(title, newHeader.get().getImage(), false, true));
			else
				headerImage.setImage(newHeader.get().getImage());
		}

		if (newPortrait.isPresent()) {
			if (portraitImage == null)
				title.getTitleImages().add(new TitleImage(title, newPortrait.get().getImage(), true, false));
			else
				portraitImage.setImage(newPortrait.get().getImage());
		}

	}
}
