package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Entity
public class FollowingList {

	private Long id;
	private User userId;
	private Visibility visibility;
	private LocalDateTime lastUpdated;
	private short movieCount;
	private short tvshowCount;

	private Set<FollowingListTitle> titles;

	public FollowingList() {
		this.titles = new HashSet<>();
	}

	public FollowingList(User userId, Visibility visibility, LocalDateTime lastUpdated, short movieCount,
			short tvshowCount) {
		this.userId = userId;
		this.visibility = visibility;
		this.lastUpdated = lastUpdated;
		this.movieCount = movieCount;
		this.tvshowCount = tvshowCount;
		this.titles = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public User getUserId() {
		return userId;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public short getMovieCount() {
		return movieCount;
	}

	public short getTvshowCount() {
		return tvshowCount;
	}

	@OneToMany(mappedBy = "listId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<FollowingListTitle> getTitles() {
		return titles;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public void setMovieCount(short movieCount) {
		this.movieCount = movieCount;
	}

	public void setTvshowCount(short tvshowCount) {
		this.tvshowCount = tvshowCount;
	}

	public void setTitles(Set<FollowingListTitle> titles) {
		this.titles = titles;
	}

}
