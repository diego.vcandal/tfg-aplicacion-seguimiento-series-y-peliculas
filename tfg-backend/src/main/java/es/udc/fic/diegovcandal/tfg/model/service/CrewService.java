package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewTypeNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface CrewService {

	Long addPersonNewCrewParticipation(Long userId, Long personId, Long titleId, Crew newCrew)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException, TitleNotFoundException,
			CrewAlreadyExistsException, CrewTypeNotFoundException;

	void updatePersonCrewParticipation(Long userId, Long crewId, Crew newCrew) throws PeopleNotFoundException,
			UserNotFoundException, PermissionException, TitleNotFoundException, CrewNotFoundException;

	void removePersonCrewParticipation(Long userId, Long crewId)
			throws UserNotFoundException, PermissionException, CrewNotFoundException;

	Long addPersonEpisodeCrewParticipation(Long userId, Long peopleId, Long titleId, byte seasonNumber,
			CrewEpisode newCrewEpisode) throws UserNotFoundException, PermissionException, PeopleNotFoundException,
			TitleNotFoundException, CrewTypeNotFoundException;

	void removePersonEpisodeCrewParticipation(Long userId, Long episodeCrewId) throws UserNotFoundException,
			PermissionException, PeopleNotFoundException, TitleNotFoundException, CrewNotFoundException;

	List<Crew> findAllCrew(Long titleId, Long localeId);

	List<CrewEpisode> findEpisodeCrew(Long episodeId, Long localeId);

}
