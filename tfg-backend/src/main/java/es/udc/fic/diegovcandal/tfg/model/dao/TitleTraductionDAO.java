package es.udc.fic.diegovcandal.tfg.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.TitleTraductionCompositeKey;

public interface TitleTraductionDAO extends PagingAndSortingRepository<TitleTraduction, TitleTraductionCompositeKey> {

}
