package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreatePersonDTO {

	private Long externalId;
	private String originalName;

	private Long birthday;
	private Long deathday;
	private String image;

	@Valid
	private List<PersonTraductionDTO> traductions;

	public CreatePersonDTO(Long externalId, String originalName, Long birthday, Long deathday, String image,
			List<PersonTraductionDTO> traductions) {
		this.externalId = externalId;
		this.originalName = originalName;
		this.birthday = birthday;
		this.deathday = deathday;
		this.image = image;
		this.traductions = traductions;
	}

	@NotNull()
	@Min(1)
	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	@NotNull()
	@Size(min = 1, max = 50)
	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public Long getBirthday() {
		return birthday;
	}

	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}

	public Long getDeathday() {
		return deathday;
	}

	public void setDeathday(Long deathday) {
		this.deathday = deathday;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<PersonTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<PersonTraductionDTO> traductions) {
		this.traductions = traductions;
	}

}
