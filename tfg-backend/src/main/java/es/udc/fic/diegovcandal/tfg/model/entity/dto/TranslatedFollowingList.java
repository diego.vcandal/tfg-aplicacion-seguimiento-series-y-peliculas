package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import java.util.ArrayList;
import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;

public class TranslatedFollowingList {

	private FollowingList followingList;

	private List<TranslatedFollowingListTitle> planToWatch;
	private List<TranslatedFollowingListTitle> watching;
	private List<TranslatedFollowingListTitle> completed;
	private List<TranslatedFollowingListTitle> dropped;

	public TranslatedFollowingList() {
		this.planToWatch = new ArrayList<>();
		this.watching = new ArrayList<>();
		this.completed = new ArrayList<>();
		this.dropped = new ArrayList<>();
	}

	public TranslatedFollowingList(FollowingList followingList) {
		this.followingList = followingList;

		this.planToWatch = new ArrayList<>();
		this.watching = new ArrayList<>();
		this.completed = new ArrayList<>();
		this.dropped = new ArrayList<>();
	}

	public FollowingList getFollowingList() {
		return followingList;
	}

	public List<TranslatedFollowingListTitle> getPlanToWatch() {
		return planToWatch;
	}

	public List<TranslatedFollowingListTitle> getWatching() {
		return watching;
	}

	public List<TranslatedFollowingListTitle> getCompleted() {
		return completed;
	}

	public List<TranslatedFollowingListTitle> getDropped() {
		return dropped;
	}

	public void setFollowingList(FollowingList followingList) {
		this.followingList = followingList;
	}

	public void setPlanToWatch(List<TranslatedFollowingListTitle> planToWatch) {
		this.planToWatch = planToWatch;
	}

	public void setWatching(List<TranslatedFollowingListTitle> watching) {
		this.watching = watching;
	}

	public void setCompleted(List<TranslatedFollowingListTitle> completed) {
		this.completed = completed;
	}

	public void setDropped(List<TranslatedFollowingListTitle> dropped) {
		this.dropped = dropped;
	}

}
