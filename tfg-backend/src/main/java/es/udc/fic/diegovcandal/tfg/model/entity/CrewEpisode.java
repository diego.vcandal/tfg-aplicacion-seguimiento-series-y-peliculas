package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CrewEpisode {

	private Long id;
	private CrewSeason crewSeasonId;
	private Episode episodeId;
	private CrewType crewTypeId;

	public CrewEpisode() {
	}

	public CrewEpisode(CrewSeason crewSeasonId, Episode episodeId, CrewType crewTypeId) {
		this.crewSeasonId = crewSeasonId;
		this.episodeId = episodeId;
		this.crewTypeId = crewTypeId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "crewSeasonId")
	public CrewSeason getCrewSeasonId() {
		return crewSeasonId;
	}

	public void setCrewSeasonId(CrewSeason crewSeasonId) {
		this.crewSeasonId = crewSeasonId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "episodeId")
	public Episode getEpisodeId() {
		return episodeId;
	}

	public void setEpisodeId(Episode episodeId) {
		this.episodeId = episodeId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "crewTypeId")
	public CrewType getCrewTypeId() {
		return crewTypeId;
	}

	public void setCrewTypeId(CrewType crewTypeId) {
		this.crewTypeId = crewTypeId;
	}

}
