package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class TVShowDTO {

	private Long id;
	private Long externalId;
	private Long sourceType;
	private String originalTitle;
	private String companyName;
	private String originalDescription;
	private short year;
	private String title;
	private String description;
	private boolean visible;
	private boolean setForDelete;

	private float avgRating;
	private int timesRated;

	private short totalDuration;
	private short episodeCount;
	private byte seasonNumber;
	private Long firstAirDate;
	private Long lastAirDate;

	private String imageHeaderPath;
	private boolean imageHeader;

	private String imagePortraitPath;
	private boolean imagePortrait;

	private List<GenreDTO> genres;
	private List<TitleTraductionDTO> traductions;

	private List<CreateSeasonCastDTO> seasons;

	public TVShowDTO() {
	}

	public TVShowDTO(Long id, Long externalId, Long sourceType, String originalTitle, String companyName,
			String originalDescription, short year, short totalDuration, short episodeCount, byte seasonNumber,
			Long firstAirDate, Long lastAirDate, String imageHeaderPath, String imagePortraitPath, boolean imageHeader,
			boolean imagePortrait, float avgRating, int timesRated) {
		this.id = id;
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.originalTitle = originalTitle;
		this.companyName = companyName;
		this.originalDescription = originalDescription;
		this.year = year;
		this.totalDuration = totalDuration;
		this.episodeCount = episodeCount;
		this.seasonNumber = seasonNumber;
		this.firstAirDate = firstAirDate;
		this.lastAirDate = lastAirDate;
		this.imageHeaderPath = imageHeaderPath;
		this.imagePortraitPath = imagePortraitPath;
		this.imageHeader = imageHeader;
		this.imagePortrait = imagePortrait;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public Long getId() {
		return id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public short getYear() {
		return year;
	}

	public short getTotalDuration() {
		return totalDuration;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public Long getFirstAirDate() {
		return firstAirDate;
	}

	public Long getLastAirDate() {
		return lastAirDate;
	}

	public List<GenreDTO> getGenres() {
		return genres;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public void setTotalDuration(short totalDuration) {
		this.totalDuration = totalDuration;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public void setFirstAirDate(Long firstAirDate) {
		this.firstAirDate = firstAirDate;
	}

	public void setLastAirDate(Long lastAirDate) {
		this.lastAirDate = lastAirDate;
	}

	public void setGenres(List<GenreDTO> genres) {
		this.genres = genres;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageHeaderPath() {
		return imageHeaderPath;
	}

	public boolean isImageHeader() {
		return imageHeader;
	}

	public String getImagePortraitPath() {
		return imagePortraitPath;
	}

	public boolean isImagePortrait() {
		return imagePortrait;
	}

	public void setImageHeaderPath(String imageHeaderPath) {
		this.imageHeaderPath = imageHeaderPath;
	}

	public void setImageHeader(boolean imageHeader) {
		this.imageHeader = imageHeader;
	}

	public void setImagePortraitPath(String imagePortraitPath) {
		this.imagePortraitPath = imagePortraitPath;
	}

	public void setImagePortrait(boolean imagePortrait) {
		this.imagePortrait = imagePortrait;
	}

	public List<TitleTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<TitleTraductionDTO> traductions) {
		this.traductions = traductions;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public List<CreateSeasonCastDTO> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<CreateSeasonCastDTO> seasons) {
		this.seasons = seasons;
	}

	public float getAvgRating() {
		return avgRating;
	}

	public int getTimesRated() {
		return timesRated;
	}

	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}

	public void setTimesRated(int timesRated) {
		this.timesRated = timesRated;
	}

}
