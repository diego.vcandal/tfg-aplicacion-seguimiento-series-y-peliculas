package es.udc.fic.diegovcandal.tfg.model.utils;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

@Service
@Transactional(readOnly = true)
public class PermissionManagerImpl implements PermissionManager {

	@Autowired
	private UserDao userDao;

	@Override
	public User findUser(Long userId) throws UserNotFoundException {
		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		return user.get();

	}

	@Override
	public User findAdminUser(Long userId) throws UserNotFoundException, PermissionException {
		Optional<User> user = userDao.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundException(userId);
		}

		if (user.get().getRole() != UserRole.ADMIN) {
			throw new PermissionException();
		}

		return user.get();

	}

}
