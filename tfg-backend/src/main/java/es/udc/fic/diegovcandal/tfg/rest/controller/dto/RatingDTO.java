package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class RatingDTO {

	private byte rating;

	public byte getRating() {
		return rating;
	}

	public void setRating(byte rating) {
		this.rating = rating;
	}

}
