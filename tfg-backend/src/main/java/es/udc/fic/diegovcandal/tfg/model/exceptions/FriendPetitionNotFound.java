package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class FriendPetitionNotFound extends InstanceNotFoundException {

	public FriendPetitionNotFound(String name, Object key) {
		super(name, key);
	}
}
