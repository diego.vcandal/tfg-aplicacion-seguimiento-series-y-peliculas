package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class MovieExternalDTO {

	private Long id;
	private Long externalId;
	private String adult;
	private String backdrop_path;
	private String poster_path;
	private String original_description;
	private List<GenreExternalDTO> genres;
	private String original_language;
	private String original_title;
	private String overview;
	private List<CompanyExternalDTO> production_companies;
	private String title;
	private String release_date;
	private int runtime;
	private short popularity;

	public Long getId() {
		return id;
	}

	public String getAdult() {
		return adult;
	}

	public String getBackdrop_path() {
		return backdrop_path;
	}

	public String getPoster_path() {
		return poster_path;
	}

	public String getOriginal_description() {
		return original_description;
	}

	public void setOriginal_description(String original_description) {
		this.original_description = original_description;
	}

	public List<GenreExternalDTO> getGenres() {
		return genres;
	}

	public String getOriginal_language() {
		return original_language;
	}

	public String getOriginal_title() {
		return original_title;
	}

	public String getOverview() {
		return overview;
	}

	public List<CompanyExternalDTO> getProduction_companies() {
		return production_companies;
	}

	public String getTitle() {
		return title;
	}

	public String getRelease_date() {
		return release_date;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAdult(String adult) {
		this.adult = adult;
	}

	public void setBackdrop_path(String backdrop_path) {
		this.backdrop_path = backdrop_path;
	}

	public void setPoster_path(String poster_path) {
		this.poster_path = poster_path;
	}

	public void setGenres(List<GenreExternalDTO> genres) {
		this.genres = genres;
	}

	public void setOriginal_language(String original_language) {
		this.original_language = original_language;
	}

	public void setOriginal_title(String original_title) {
		this.original_title = original_title;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setProduction_companies(List<CompanyExternalDTO> production_companies) {
		this.production_companies = production_companies;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public short getPopularity() {
		return popularity;
	}

	public void setPopularity(short popularity) {
		this.popularity = popularity;
	}

}
