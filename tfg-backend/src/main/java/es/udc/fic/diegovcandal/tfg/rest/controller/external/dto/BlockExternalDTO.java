package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class BlockExternalDTO<T> {

	private List<T> results;
	private int page;
	private int total_results;
	private int total_pages;

	public BlockExternalDTO() {
	}

	public BlockExternalDTO(List<T> results, int page, int total_results, int total_pages) {
		this.results = results;
		this.page = page;
		this.total_results = total_results;
		this.total_pages = total_pages;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal_results() {
		return total_results;
	}

	public void setTotal_results(int total_results) {
		this.total_results = total_results;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

}
