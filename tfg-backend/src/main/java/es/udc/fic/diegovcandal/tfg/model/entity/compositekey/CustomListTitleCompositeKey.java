package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class CustomListTitleCompositeKey implements Serializable {

	private static final long serialVersionUID = 5833708054899059809L;

	private Long listId;
	private Long titleId;

	public CustomListTitleCompositeKey() {
	}

	public CustomListTitleCompositeKey(Long listId, Long titleId) {
		this.listId = listId;
		this.titleId = titleId;
	}

	public Long getListId() {
		return listId;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setListId(Long listId) {
		this.listId = listId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listId == null) ? 0 : listId.hashCode());
		result = prime * result + ((titleId == null) ? 0 : titleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomListTitleCompositeKey other = (CustomListTitleCompositeKey) obj;
		if (listId == null) {
			if (other.listId != null)
				return false;
		} else if (!listId.equals(other.listId))
			return false;
		if (titleId == null) {
			if (other.titleId != null)
				return false;
		} else if (!titleId.equals(other.titleId))
			return false;
		return true;
	}

}
