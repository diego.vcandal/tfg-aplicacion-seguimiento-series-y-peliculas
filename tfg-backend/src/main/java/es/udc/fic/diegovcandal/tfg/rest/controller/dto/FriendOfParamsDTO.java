package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.NotNull;

public class FriendOfParamsDTO {
	
	private Long user;
	private Long friend;

	public FriendOfParamsDTO() {

	}

	@NotNull
	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	@NotNull
	public Long getFriend() {
		return friend;
	}

	public void setFriend(Long friend) {
		this.friend = friend;
	}

	

}
