package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class TVShowExternalDTO {

	private Long id;
	private Long externalId;
	private String backdrop_path;
	private List<GenreExternalDTO> genres;
	private List<CompanyExternalDTO> production_companies;
	private String original_language;
	private String original_name;
	private String original_description;
	private String overview;
	private String poster_path;
	private String name;
	private String first_air_date;
	private String last_air_date;
	private short number_of_episodes;
	private short number_of_seasons;
	private short popularity;
	private List<Long> episode_run_time;

	private List<SeasonSummaryExternalDTO> seasons;

	public Long getId() {
		return id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public String getBackdrop_path() {
		return backdrop_path;
	}

	public List<GenreExternalDTO> getGenres() {
		return genres;
	}

	public List<CompanyExternalDTO> getProduction_companies() {
		return production_companies;
	}

	public String getOriginal_language() {
		return original_language;
	}

	public String getOriginal_name() {
		return original_name;
	}

	public String getOverview() {
		return overview;
	}

	public String getPoster_path() {
		return poster_path;
	}

	public String getName() {
		return name;
	}

	public String getFirst_air_date() {
		return first_air_date;
	}

	public String getLast_air_date() {
		return last_air_date;
	}

	public short getNumber_of_episodes() {
		return number_of_episodes;
	}

	public short getNumber_of_seasons() {
		return number_of_seasons;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBackdrop_path(String backdrop_path) {
		this.backdrop_path = backdrop_path;
	}

	public void setGenres(List<GenreExternalDTO> genres) {
		this.genres = genres;
	}

	public void setProduction_companies(List<CompanyExternalDTO> production_companies) {
		this.production_companies = production_companies;
	}

	public void setOriginal_language(String original_language) {
		this.original_language = original_language;
	}

	public void setOriginal_name(String original_name) {
		this.original_name = original_name;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setPoster_path(String poster_path) {
		this.poster_path = poster_path;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFirst_air_date(String first_air_date) {
		this.first_air_date = first_air_date;
	}

	public void setLast_air_date(String last_air_date) {
		this.last_air_date = last_air_date;
	}

	public void setNumber_of_episodes(short number_of_episodes) {
		this.number_of_episodes = number_of_episodes;
	}

	public void setNumber_of_seasons(short number_of_seasons) {
		this.number_of_seasons = number_of_seasons;
	}

	public String getOriginal_description() {
		return original_description;
	}

	public void setOriginal_description(String original_description) {
		this.original_description = original_description;
	}

	public List<SeasonSummaryExternalDTO> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<SeasonSummaryExternalDTO> seasons) {
		this.seasons = seasons;
	}

	public List<Long> getEpisode_run_time() {
		return episode_run_time;
	}

	public void setEpisode_run_time(List<Long> episode_run_time) {
		this.episode_run_time = episode_run_time;
	}

	public short getPopularity() {
		return popularity;
	}

	public void setPopularity(short popularity) {
		this.popularity = popularity;
	}

}
