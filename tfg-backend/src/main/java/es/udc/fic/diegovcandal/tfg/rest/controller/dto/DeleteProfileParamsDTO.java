package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DeleteProfileParamsDTO {

	private String password;

	public DeleteProfileParamsDTO() {
	}

	@NotNull
	@Size(min = 1, max = 60)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
