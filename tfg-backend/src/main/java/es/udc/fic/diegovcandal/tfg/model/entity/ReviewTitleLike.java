package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ReviewTitleLike {

	private Long id;
	private ReviewTitle reviewId;
	private User userId;

	public ReviewTitleLike() {
	}

	public ReviewTitleLike(ReviewTitle reviewId, User userId) {
		this.reviewId = reviewId;
		this.userId = userId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "reviewId")
	public ReviewTitle getReviewId() {
		return reviewId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public User getUserId() {
		return userId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setReviewId(ReviewTitle reviewId) {
		this.reviewId = reviewId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

}
