package es.udc.fic.diegovcandal.tfg.model.entity.dto;

public class PersonCastSummary {

	private Long titleId;
	private String originalTitle;
	private String titleName;
	private short year;
	private String characterName;
	private short episodeCount;

	public PersonCastSummary(Long titleId, short year, String originalTitle, String titleName, String characterName,
			short episodeCount) {
		this.titleId = titleId;
		this.titleName = titleName;
		this.year = year;
		this.originalTitle = originalTitle;
		this.characterName = characterName;
		this.episodeCount = episodeCount;
	}

	public Long getTitleId() {
		return titleId;
	}

	public String getTitleName() {
		return titleName;
	}

	public short getYear() {
		return year;
	}

	public String getCharacterName() {
		return characterName;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public void setCharacterName(String characterName) {
		this.characterName = characterName;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

}
