package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class PersonSummaryDTO {

	private Long id;
	private Long externalId;
	private Long sourceType;
	private String originalName;
	private boolean image;
	private String imageExternalPath;

	public PersonSummaryDTO() {
	}

	public PersonSummaryDTO(Long id, Long externalId, Long sourceType, String originalName, boolean image,
			String imageExternalPath) {
		this.id = id;
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.originalName = originalName;
		this.image = image;
		this.imageExternalPath = imageExternalPath;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public String getImageExternalPath() {
		return imageExternalPath;
	}

	public void setImageExternalPath(String imageExternalPath) {
		this.imageExternalPath = imageExternalPath;
	}

}
