package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Movie extends Title {

	private short duration;

	public Movie() {
	}

	public Movie(Long externalId, SourceType sourceType, String originalTitle, String companyName,
			String originalDescription, short year, short duration) {

		super(externalId, sourceType, TitleType.MOVIE, originalTitle, companyName, originalDescription, year);
		this.duration = duration;

	}

	public short getDuration() {
		return duration;
	}

	public void setDuration(short duration) {
		this.duration = duration;
	}

}
