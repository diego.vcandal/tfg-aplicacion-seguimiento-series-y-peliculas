package es.udc.fic.diegovcandal.tfg.model.restclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.CastExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalCreditsDTO;

@Service
public class TmdbAPICastCrewServiceImpl implements TmdbAPICastCrewService {

	@Value("${project.external.api.apiKey}")
	private String apiKey;

	@Value("${project.external.api.apiEndpoint}")
	private String apiEndpoint;

	@Value("${project.external.api.apiImageEndpoint}")
	private String apiImageEndpoint;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<CastExternalDTO> findMainCast(Long externalId, String acceptLanguage, TitleType titleType) {

		ExternalCreditsDTO credits = null;

		if (titleType == TitleType.MOVIE)
			credits = getMovieCast(externalId, acceptLanguage);
		else if (titleType == TitleType.TV_SHOW)
			credits = getTVShowMainCast(externalId, acceptLanguage);

		List<CastExternalDTO> list = credits.getCast();

		return list.size() > 9 ? list.subList(0, 9) : list;

	}

	@Override
	public ExternalCreditsDTO findAllCastAndCrew(Long externalId, String acceptLanguage, TitleType titleType) {

		ExternalCreditsDTO credits = null;

		if (titleType == TitleType.MOVIE)
			credits = getMovieCast(externalId, acceptLanguage);
		else if (titleType == TitleType.TV_SHOW)
			credits = getTVShowCast(externalId, acceptLanguage);

		return credits;

	}

	@Override
	public ExternalCreditsDTO findAllEpisodeCastAndCrew(Long externalId, byte season, short episode,
			String acceptLanguage) {

		return getEpisodeCast(externalId, season, episode, acceptLanguage);

	}

	private ExternalCreditsDTO getTVShowMainCast(Long externalId, String acceptLanguage) {

		return restTemplate.exchange(
				apiEndpoint + "/tv/" + externalId + "/credits?api_key=" + apiKey + "&language=" + acceptLanguage,
				HttpMethod.GET, HttpEntity.EMPTY, ExternalCreditsDTO.class).getBody();

	}

	private ExternalCreditsDTO getMovieCast(Long externalId, String acceptLanguage) {

		return restTemplate.exchange(
				apiEndpoint + "/movie/" + externalId + "/credits?api_key=" + apiKey + "&language=" + acceptLanguage,
				HttpMethod.GET, HttpEntity.EMPTY, ExternalCreditsDTO.class).getBody();

	}

	private ExternalCreditsDTO getTVShowCast(Long externalId, String acceptLanguage) {

		return restTemplate.exchange(apiEndpoint + "/tv/" + externalId + "/aggregate_credits?api_key=" + apiKey
				+ "&language=" + acceptLanguage, HttpMethod.GET, HttpEntity.EMPTY, ExternalCreditsDTO.class).getBody();

	}

	private ExternalCreditsDTO getEpisodeCast(Long externalId, byte season, short episode, String acceptLanguage) {

		return restTemplate.exchange(
				apiEndpoint + "/tv/" + externalId + "/season/" + season + "/episode/" + episode + "/credits?api_key="
						+ apiKey + "&language=" + acceptLanguage,
				HttpMethod.GET, HttpEntity.EMPTY, ExternalCreditsDTO.class).getBody();

	}

}
