package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class CrewExternalDTO {

	private Long id;
	private String name;
	private String original_name;
	private String profile_path;
	private String department;
	private String job;

	private List<JobCrewExternalDTO> jobs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOriginal_name() {
		return original_name;
	}

	public void setOriginal_name(String original_name) {
		this.original_name = original_name;
	}

	public String getProfile_path() {
		return profile_path;
	}

	public void setProfile_path(String profile_path) {
		this.profile_path = profile_path;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public List<JobCrewExternalDTO> getJobs() {
		return jobs;
	}

	public void setJobs(List<JobCrewExternalDTO> jobs) {
		this.jobs = jobs;
	}

}
