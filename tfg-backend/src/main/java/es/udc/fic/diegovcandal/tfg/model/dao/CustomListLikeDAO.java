package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListLike;
import es.udc.fic.diegovcandal.tfg.model.entity.User;

public interface CustomListLikeDAO extends PagingAndSortingRepository<CustomListLike, Long> {

	Optional<CustomListLike> findByListIdAndUserId(CustomList listId, User userId);

}
