package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;

@Entity
public class Episode {

	private Long id;
	private Long externalId;
	private Season season;
	private short episodeNumber;
	private String originalTitle;
	private String originalDescription;
	private LocalDate airDate;
	private short totalDuration;
	private String image;
	private boolean setForDelete;
	private SourceType sourceType;

	private Set<EpisodeTraduction> episodeTraductions;

	private Set<CrewEpisode> crewEpisodes;
	private Set<CastEpisode> castEpisodes;

	public Episode() {
		this.episodeTraductions = new HashSet<>();
		this.crewEpisodes = new HashSet<>();
		this.castEpisodes = new HashSet<>();
		this.setForDelete = false;
	}

	public Episode(Long externalId, Season season, short episodeNumber, String originalTitle,
			String originalDescription, LocalDate airDate, short totalDuration, String image) {
		this.externalId = externalId;
		this.season = season;
		this.episodeNumber = episodeNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.airDate = airDate;
		this.totalDuration = totalDuration;
		this.image = image;

		this.episodeTraductions = new HashSet<>();
		this.crewEpisodes = new HashSet<>();
		this.castEpisodes = new HashSet<>();
		this.setForDelete = false;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "seasonId")
	public Season getSeason() {
		return season;
	}

	public short getEpisodeNumber() {
		return episodeNumber;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public LocalDate getAirDate() {
		return airDate;
	}

	public short getTotalDuration() {
		return totalDuration;
	}

	public String getImage() {
		return image;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public void setEpisodeNumber(short episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public void setAirDate(LocalDate airDate) {
		this.airDate = airDate;
	}

	public void setTotalDuration(short totalDuration) {
		this.totalDuration = totalDuration;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	@OneToMany(mappedBy = "idEpisode", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<EpisodeTraduction> getEpisodeTraductions() {
		return episodeTraductions;
	}

	public void setEpisodeTraductions(Set<EpisodeTraduction> episodeTraductions) {
		this.episodeTraductions = episodeTraductions;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	@Transient
	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	@OneToMany(mappedBy = "episodeId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CrewEpisode> getCrewEpisodes() {
		return crewEpisodes;
	}

	public void setCrewEpisodes(Set<CrewEpisode> crewEpisodes) {
		this.crewEpisodes = crewEpisodes;
	}

	@OneToMany(mappedBy = "episodeId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CastEpisode> getCastEpisodes() {
		return castEpisodes;
	}

	public void setCastEpisodes(Set<CastEpisode> castEpisodes) {
		this.castEpisodes = castEpisodes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airDate == null) ? 0 : airDate.hashCode());
		result = prime * result + episodeNumber;
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((originalDescription == null) ? 0 : originalDescription.hashCode());
		result = prime * result + ((originalTitle == null) ? 0 : originalTitle.hashCode());
		result = prime * result + (setForDelete ? 1231 : 1237);
		result = prime * result + ((sourceType == null) ? 0 : sourceType.hashCode());
		result = prime * result + totalDuration;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Episode other = (Episode) obj;
		if (airDate == null) {
			if (other.airDate != null)
				return false;
		} else if (!airDate.equals(other.airDate))
			return false;
		if (episodeNumber != other.episodeNumber)
			return false;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (originalDescription == null) {
			if (other.originalDescription != null)
				return false;
		} else if (!originalDescription.equals(other.originalDescription))
			return false;
		if (originalTitle == null) {
			if (other.originalTitle != null)
				return false;
		} else if (!originalTitle.equals(other.originalTitle))
			return false;
		if (setForDelete != other.setForDelete)
			return false;
		if (sourceType != other.sourceType)
			return false;
		if (totalDuration != other.totalDuration)
			return false;
		return true;
	}

}
