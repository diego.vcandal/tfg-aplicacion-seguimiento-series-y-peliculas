package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.FriendPetition;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FriendPetitionDTO;

public class PetitionConversor {

	public static final List<FriendPetitionDTO> toPetitionsDTOs(List<FriendPetition> petitions) {
		return petitions.stream().map(p -> toPetitionsDTO(p)).collect(Collectors.toList());
	}

	private static final FriendPetitionDTO toPetitionsDTO(FriendPetition petition) {

		return new FriendPetitionDTO(petition.getOriginUser().getId(), petition.getDestinationUser().getId(),
				petition.getOriginUser().getUserName(), petition.getOriginUser().getImage() != null,
				toMillis(petition.getDate()));
	}

	private static final long toMillis(LocalDateTime date) {
		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
	}
}
