package es.udc.fic.diegovcandal.tfg.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.EpisodeTraductionCompositeKey;

@Entity
@IdClass(EpisodeTraductionCompositeKey.class)
public class EpisodeTraduction implements Serializable {

	private static final long serialVersionUID = -451706875493312865L;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "idEpisode")
	private Episode idEpisode;

	@Id
	private Long idLocale;

	private String title;
	private String description;

	public EpisodeTraduction() {
	}

	public EpisodeTraduction(Episode episode, Long idLocale, String title, String description) {
		this.idEpisode = episode;
		this.idLocale = idLocale;
		this.title = title;
		this.description = description;
	}

	public Episode getIdEpisode() {
		return idEpisode;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setIdEpisode(Episode idEpisode) {
		this.idEpisode = idEpisode;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
