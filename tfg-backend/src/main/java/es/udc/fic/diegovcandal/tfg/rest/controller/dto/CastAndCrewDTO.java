package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class CastAndCrewDTO {

	private List<CastDTO> cast;
	private List<CrewDTO> crew;

	public CastAndCrewDTO(List<CastDTO> cast, List<CrewDTO> crew) {
		this.cast = cast;
		this.crew = crew;
	}

	public List<CastDTO> getCast() {
		return cast;
	}

	public void setCast(List<CastDTO> cast) {
		this.cast = cast;
	}

	public List<CrewDTO> getCrew() {
		return crew;
	}

	public void setCrew(List<CrewDTO> crew) {
		this.crew = crew;
	}

}
