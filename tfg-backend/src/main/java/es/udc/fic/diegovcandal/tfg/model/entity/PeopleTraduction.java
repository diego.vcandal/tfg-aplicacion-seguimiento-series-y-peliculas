package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.PeopleTraductionCompositeKey;

@Entity
@IdClass(PeopleTraductionCompositeKey.class)
public class PeopleTraduction {

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "idPeople")
	private People idPeople;

	@Id
	private Long idLocale;

	private String name;
	private String biography;
	private String birthPlace;
	private boolean originalLanguage;

	public PeopleTraduction() {
	}

	public PeopleTraduction(People idPeople, Long idLocale, String name, String biography, String birthPlace,
			boolean originalLanguage) {
		this.idPeople = idPeople;
		this.idLocale = idLocale;
		this.name = name;
		this.biography = biography;
		this.birthPlace = birthPlace;
		this.originalLanguage = originalLanguage;
	}

	public People getIdPeople() {
		return idPeople;
	}

	public void setIdPeople(People idPeople) {
		this.idPeople = idPeople;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public boolean isOriginalLanguage() {
		return originalLanguage;
	}

	public void setOriginalLanguage(boolean originalLanguage) {
		this.originalLanguage = originalLanguage;
	}

}
