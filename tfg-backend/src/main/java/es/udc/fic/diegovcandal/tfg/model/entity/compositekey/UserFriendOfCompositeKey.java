package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class UserFriendOfCompositeKey implements Serializable {

	private static final long serialVersionUID = -3718402880287800326L;

	private Long firstFriendId;
	private Long secondFriendId;

	public UserFriendOfCompositeKey() {
	}

	public UserFriendOfCompositeKey(Long firstFriendId, Long secondFriendId) {
		this.firstFriendId = firstFriendId;
		this.secondFriendId = secondFriendId;
	}

	public Long getFirstFriendId() {
		return firstFriendId;
	}

	public void setFirstFriendId(Long firstFriendId) {
		this.firstFriendId = firstFriendId;
	}

	public Long getSecondFriendId() {
		return secondFriendId;
	}

	public void setSecondFriendId(Long secondFriendId) {
		this.secondFriendId = secondFriendId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstFriendId == null) ? 0 : firstFriendId.hashCode());
		result = prime * result + ((secondFriendId == null) ? 0 : secondFriendId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserFriendOfCompositeKey other = (UserFriendOfCompositeKey) obj;
		if (firstFriendId == null) {
			if (other.firstFriendId != null)
				return false;
		} else if (!firstFriendId.equals(other.firstFriendId))
			return false;
		if (secondFriendId == null) {
			if (other.secondFriendId != null)
				return false;
		} else if (!secondFriendId.equals(other.secondFriendId))
			return false;
		return true;
	}

}
