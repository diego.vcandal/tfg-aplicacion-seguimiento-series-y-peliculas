package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toCastEpisodeDTOsfromExternal;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toCrewEpisodeDTOsfromExternal;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.fromExternalMovieToMovieDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.fromExternalTVShowToTVShowDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.ReviewTitleConversor.toReviewTitleDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeasonSummaryDTOWithEpisodesFromExternal;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeasonSummaryDTOsFromExternal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPICastCrewService;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPIClient;
import es.udc.fic.diegovcandal.tfg.model.service.TitleService;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CastAndCrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateExternalSeasonParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.EditFollowingListItemDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.MovieDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.RatingDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ReviewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ReviewTitleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.SeasonSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TVShowDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalCreditsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

@RestController
@ConditionalOnProperty("project.external.api.enabled")
@RequestMapping("/titles/tmdb")
public class ExternalTitleController {

	private static final String TITLE_NOT_FOUND_EXCEPTION_ID = "exceptions.TitleNotFoundException";
	private static final String EXTERNAL_ID_ALREADY_LINKED_EXCEPTION_ID = "exceptions.ExternalIdAlreadyLinkedException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private TitleService titleService;

	@Autowired
	private TmdbAPIClient tmdbAPIClient;

	@Autowired
	private TitleController titleController;

	@Autowired
	private TmdbAPICastCrewService tmdbAPICastCrewService;

	@ExceptionHandler(TitleNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleTitleNotFoundException(TitleNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(TITLE_NOT_FOUND_EXCEPTION_ID, null, TITLE_NOT_FOUND_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(ExternalIdAlreadyLinkedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleExternalIdAlreadyLinkedException(ExternalIdAlreadyLinkedException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(EXTERNAL_ID_ALREADY_LINKED_EXCEPTION_ID, null,
				EXTERNAL_ID_ALREADY_LINKED_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@GetMapping("/movies/{id}")
	public MovieDTO getExternalMovieDetails(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long titleId = titleService.titleExistsByExternalId(id);

		if (titleId == -1L)
			return fromExternalMovieToMovieDTO(tmdbAPIClient.findExternalMovie(id, acceptLanguage), false, 0, 0);

		try {

			return titleController.getMovieDetails(userId, titleId, acceptLanguage);

		} catch (TitleNotFoundException e) {

			return fromExternalMovieToMovieDTO(tmdbAPIClient.findExternalMovie(id, acceptLanguage), false, 0, 0);

		}

	}

	@GetMapping("/tvshow/{id}")
	public TVShowDTO getExternalTvShowDetails(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long titleId = titleService.titleExistsByExternalId(id);

		if (titleId == -1L)
			return fromExternalTVShowToTVShowDTO(tmdbAPIClient.findExternalTVShow(id, acceptLanguage), false, 0, 0);

		try {

			return titleController.getTVShowDetails(userId, titleId, acceptLanguage);

		} catch (TitleNotFoundException e) {

			return fromExternalTVShowToTVShowDTO(tmdbAPIClient.findExternalTVShow(id, acceptLanguage), false, 0, 0);

		}

	}

	@GetMapping("/tvshow/{id}/seasons")
	public List<SeasonSummaryDTO> getExternalTvShowSeasonsDetails(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException {

		return toSeasonSummaryDTOsFromExternal(tmdbAPIClient.findExternalTVShow(id, acceptLanguage).getSeasons(),
				new ArrayList<>());

	}

	@GetMapping("/tvshow/{id}/seasons/{seasonNumber}")
	public SeasonSummaryDTO getExternalSeasonAndEpisodesDetails(@PathVariable Long id, @PathVariable byte seasonNumber,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException {

		return toSeasonSummaryDTOWithEpisodesFromExternal(
				tmdbAPIClient.findExternalSeason(id, seasonNumber, acceptLanguage));

	}

	@GetMapping("/tvshow/{id}/lastSeason")
	public List<SeasonSummaryDTO> getExternalTvShowLastSeason(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException {

		TVShowExternalDTO tvShowExternalDTO = tmdbAPIClient.findExternalTVShow(id, acceptLanguage);

		return toSeasonSummaryDTOsFromExternal(tvShowExternalDTO.getSeasons().stream()
				.filter(s -> s.getSeason_number() == tvShowExternalDTO.getNumber_of_seasons())
				.collect(Collectors.toList()), new ArrayList<>());

	}

	@PostMapping("/movies/{id}/create")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createExternalMovie(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException {

		tmdbAPIClient.createExternalMovie(userId, id, acceptLanguage, false);

	}

	@PostMapping("/tvshow/{id}/create")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createExternalTVShow(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException {

		tmdbAPIClient.createExternalTVShow(userId, id, acceptLanguage, false);

	}

	@PostMapping("/tvshow/{id}/seasons/{seasonNumber}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createExternalSeason(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@PathVariable byte seasonNumber,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody CreateExternalSeasonParamsDTO externalSeasonParamsDTO)
			throws TitleNotFoundException, UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException {

		tmdbAPIClient.createExternalSeason(userId, id, externalSeasonParamsDTO.getExternalSeasonId(), seasonNumber,
				acceptLanguage);

	}

	@PostMapping("/tvshow/{id}/seasons/{seasonNumber}/episodes/addAll")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createExternalEpisodes(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@PathVariable byte seasonNumber,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody CreateExternalSeasonParamsDTO externalSeasonParamsDTO)
			throws TitleNotFoundException, UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException {

		tmdbAPIClient.createExternalEpisodes(userId, id, externalSeasonParamsDTO.getExternalSeasonId(), seasonNumber,
				acceptLanguage);

	}

	@PostMapping("/tvshow/{id}/seasons/{seasonNumber}/episodes/{episodeNumber}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createExternalEpisode(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@PathVariable byte seasonNumber, @PathVariable short episodeNumber,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody CreateExternalSeasonParamsDTO externalSeasonParamsDTO)
			throws TitleNotFoundException, UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException {

		tmdbAPIClient.createExternalEpisode(userId, id, externalSeasonParamsDTO.getExternalSeasonId(), seasonNumber,
				episodeNumber, acceptLanguage);

	}

	@GetMapping("/movies/{id}/main-cast")
	public List<CastDTO> findMovieMainCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		return toCastEpisodeDTOsfromExternal(tmdbAPICastCrewService.findMainCast(id, acceptLanguage, TitleType.MOVIE),
				true);

	}

	@GetMapping("/tvshow/{id}/main-cast")
	public List<CastDTO> findTVShowMainCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		return toCastEpisodeDTOsfromExternal(tmdbAPICastCrewService.findMainCast(id, acceptLanguage, TitleType.TV_SHOW),
				true);

	}

	@GetMapping("/movies/{id}/all-cast")
	public CastAndCrewDTO findAllMovieCastAndCrew(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		ExternalCreditsDTO externalCreditsDTO = tmdbAPICastCrewService.findAllCastAndCrew(id, acceptLanguage,
				TitleType.MOVIE);

		return new CastAndCrewDTO(toCastEpisodeDTOsfromExternal(externalCreditsDTO.getCast(), false),
				toCrewEpisodeDTOsfromExternal(externalCreditsDTO.getCrew()));

	}

	@GetMapping("/tvshow/{id}/all-cast")
	public CastAndCrewDTO findAllTVShowCastAndCrew(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		ExternalCreditsDTO externalCreditsDTO = tmdbAPICastCrewService.findAllCastAndCrew(id, acceptLanguage,
				TitleType.TV_SHOW);

		return new CastAndCrewDTO(toCastEpisodeDTOsfromExternal(externalCreditsDTO.getCast(), false),
				toCrewEpisodeDTOsfromExternal(externalCreditsDTO.getCrew()));

	}

	@PostMapping("/movies/{id}/rate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void rateMovie(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody RatingDTO ratingDTO)
			throws TitleNotFoundException, UserNotFoundException, IncorrectRatingException,
			ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException {

		tmdbAPIClient.rateTitle(userId, id, ratingDTO.getRating(), TitleType.MOVIE, acceptLanguage);

	}

	@PostMapping("/tvshow/{id}/rate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void rateTVShow(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody RatingDTO ratingDTO)
			throws TitleNotFoundException, UserNotFoundException, IncorrectRatingException,
			ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException {

		tmdbAPIClient.rateTitle(userId, id, ratingDTO.getRating(), TitleType.TV_SHOW, acceptLanguage);

	}

	@PostMapping("/movies/{id}/reviews")
	public ReviewTitleDTO addNewMoviewReview(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody ReviewDTO reviewDTO)
			throws TitleNotFoundException, UserNotFoundException, IncorrectRatingException,
			ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException {

		return toReviewTitleDTO(tmdbAPIClient.addNewReview(userId, id, reviewDTO.getReviewTitle(),
				reviewDTO.getContent(), reviewDTO.getRating(), TitleType.MOVIE, acceptLanguage));

	}

	@PostMapping("/tvshow/{id}/reviews")
	public ReviewTitleDTO addNewTVShowReview(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody ReviewDTO reviewDTO)
			throws TitleNotFoundException, UserNotFoundException, IncorrectRatingException,
			ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException {

		return toReviewTitleDTO(tmdbAPIClient.addNewReview(userId, id, reviewDTO.getReviewTitle(),
				reviewDTO.getContent(), reviewDTO.getRating(), TitleType.TV_SHOW, acceptLanguage));

	}

	@PostMapping("/movies/{id}/following-add")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addMovieToFollowingList(@RequestAttribute Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody EditFollowingListItemDTO editDTO) throws UserNotFoundException,
			TitleNotFoundException, IncorrectOperationException, ExternalIdAlreadyLinkedException, PermissionException {

		tmdbAPIClient.addToFollowingList(userId, id, editDTO.getStatus(), TitleType.MOVIE, acceptLanguage);

	}

	@PostMapping("/tvshow/{id}/following-add")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addTVShowToFollowingList(@RequestAttribute Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody EditFollowingListItemDTO editDTO) throws UserNotFoundException,
			TitleNotFoundException, IncorrectOperationException, ExternalIdAlreadyLinkedException, PermissionException {

		tmdbAPIClient.addToFollowingList(userId, id, editDTO.getStatus(), TitleType.TV_SHOW, acceptLanguage);

	}

}
