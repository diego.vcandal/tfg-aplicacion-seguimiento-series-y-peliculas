package es.udc.fic.diegovcandal.tfg.model.utils;

import java.util.List;

public class Block<T> {

	private List<T> items;
	private boolean existMoreItems;
	private int totalPages;
	private long totalElements;

	private String searchCondition;

	public Block(List<T> items, boolean existMoreItems, int totalPages, long totalElements) {
		this.items = items;
		this.existMoreItems = existMoreItems;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
	}

	public Block(List<T> items, boolean existMoreItems, int totalPages, long totalElements, String searchCondition) {
		this.items = items;
		this.existMoreItems = existMoreItems;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
		this.searchCondition = searchCondition;
	}

	public List<T> getItems() {
		return items;
	}

	public boolean getExistMoreItems() {
		return existMoreItems;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public String getSearchCondition() {
		return searchCondition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (existMoreItems ? 1231 : 1237);
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + (int) (totalElements ^ (totalElements >>> 32));
		result = prime * result + totalPages;
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		@SuppressWarnings("rawtypes")
		Block other = (Block) obj;

		if (existMoreItems != other.existMoreItems)
			return false;

		if (totalElements != other.totalElements)
			return false;

		if (totalPages != other.totalPages)
			return false;

		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items)) {
			return false;
		}

		return true;
	}

}
