package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateMovieParamsDTO {

	private Long externalId;
	private String originalTitle;
	private String originalDescription;
	private String companyName;
	private short year;
	private short duration;

	private String headerImg;
	private String portraitImg;

	@Valid
	private List<GenreDTO> genres;

	@Valid
	private List<TitleTraductionDTO> traductions;

	public CreateMovieParamsDTO(Long externalId, String originalTitle, String originalDescription, String companyName,
			short year, short duration) {
		this.externalId = externalId;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.companyName = companyName;
		this.year = year;
		this.duration = duration;
	}

	@NotNull()
	@Min(1)
	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	@NotNull()
	@Size(min = 1, max = 100)
	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	@Size(min = 0, max = 500)
	public String getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	@Size(min = 0, max = 70)
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@NotNull()
	public short getYear() {
		return year;
	}

	public void setYear(short year) {
		this.year = year;
	}

	@NotNull()
	@Min(0)
	public short getDuration() {
		return duration;
	}

	public void setDuration(short duration) {
		this.duration = duration;
	}

	public String getHeaderImg() {
		return headerImg;
	}

	public void setHeaderImg(String headerImg) {
		this.headerImg = headerImg;
	}

	public String getPortraitImg() {
		return portraitImg;
	}

	public void setPortraitImg(String portraitImg) {
		this.portraitImg = portraitImg;
	}

	public List<GenreDTO> getGenres() {
		return genres;
	}

	public void setGenres(List<GenreDTO> genresDtos) {
		this.genres = genresDtos;
	}

	public List<TitleTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<TitleTraductionDTO> traductions) {
		this.traductions = traductions;
	}

}
