package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Entity
public class People {

	private Long id;
	private Long externalId;
	private SourceType sourceType;
	private String originalName;
	private LocalDate birthday;
	private LocalDate deathday;
	private String image;
	private String externalImage;
	private Visibility visibility;
	private boolean setForDelete;
	private User addedByUser;

	private Set<PeopleTraduction> peopleTraductions;

	public People() {
		this.peopleTraductions = new HashSet<>();
		this.visibility = Visibility.PRIVATE;
		this.setForDelete = false;
	}

	public People(Long externalId, SourceType sourceType, String originalName, LocalDate birthday, LocalDate deathday,
			String image) {
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.originalName = originalName;
		this.birthday = birthday;
		this.deathday = deathday;
		this.image = image;

		this.visibility = Visibility.PRIVATE;
		this.setForDelete = false;
		this.peopleTraductions = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public LocalDate getDeathday() {
		return deathday;
	}

	public void setDeathday(LocalDate deathday) {
		this.deathday = deathday;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "addedBy")
	public User getAddedByUser() {
		return addedByUser;
	}

	public void setAddedByUser(User addedByUser) {
		this.addedByUser = addedByUser;
	}

	@OneToMany(mappedBy = "idPeople", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<PeopleTraduction> getPeopleTraductions() {
		return peopleTraductions;
	}

	public void setPeopleTraductions(Set<PeopleTraduction> peopleTraductions) {
		this.peopleTraductions = peopleTraductions;
	}

	public String getExternalImage() {
		return externalImage;
	}

	public void setExternalImage(String externalImage) {
		this.externalImage = externalImage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addedByUser == null) ? 0 : addedByUser.hashCode());
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((deathday == null) ? 0 : deathday.hashCode());
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((originalName == null) ? 0 : originalName.hashCode());
		result = prime * result + ((peopleTraductions == null) ? 0 : peopleTraductions.hashCode());
		result = prime * result + (setForDelete ? 1231 : 1237);
		result = prime * result + ((sourceType == null) ? 0 : sourceType.hashCode());
		result = prime * result + ((visibility == null) ? 0 : visibility.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		People other = (People) obj;
		if (addedByUser == null) {
			if (other.addedByUser != null)
				return false;
		} else if (!addedByUser.equals(other.addedByUser))
			return false;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (deathday == null) {
			if (other.deathday != null)
				return false;
		} else if (!deathday.equals(other.deathday))
			return false;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (originalName == null) {
			if (other.originalName != null)
				return false;
		} else if (!originalName.equals(other.originalName))
			return false;
		if (peopleTraductions == null) {
			if (other.peopleTraductions != null)
				return false;
		} else if (!peopleTraductions.equals(other.peopleTraductions))
			return false;
		if (setForDelete != other.setForDelete)
			return false;
		if (sourceType != other.sourceType)
			return false;
		if (visibility != other.visibility)
			return false;
		return true;
	}

}
