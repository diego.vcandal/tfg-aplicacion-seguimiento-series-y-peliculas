package es.udc.fic.diegovcandal.tfg.model.utils;

public enum FollowingStatus {

	PLAN_TO_WATCH(0L), WATCHING(1L), COMPLETED(2L), DROPPED(3L);

	private FollowingStatus(Long id) {
		this.id = id;
	}

	private Long id;

	public Long getId() {
		return id;
	}

	public static FollowingStatus valueOfStatusOrDefault(Long id) {
		for (FollowingStatus e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return WATCHING;
	}

}
