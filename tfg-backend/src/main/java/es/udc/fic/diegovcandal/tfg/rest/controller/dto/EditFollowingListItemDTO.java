package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class EditFollowingListItemDTO {

	private Long dateStarted;
	private Long dateFinalized;
	private Long status;
	private Byte rating;

	public EditFollowingListItemDTO(Long dateStarted, Long dateFinalized, Long status, byte rating) {
		this.dateStarted = dateStarted;
		this.dateFinalized = dateFinalized;
		this.status = status;
		this.rating = rating;
	}

	@NotNull()
	public Long getStatus() {
		return status;
	}

	@Min(0)
	public Byte getRating() {
		return rating;
	}

	public Long getDateStarted() {
		return dateStarted;
	}

	public Long getDateFinalized() {
		return dateFinalized;
	}

	public void setDateStarted(Long dateStarted) {
		this.dateStarted = dateStarted;
	}

	public void setDateFinalized(Long dateFinalized) {
		this.dateFinalized = dateFinalized;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public void setRating(Byte rating) {
		this.rating = rating;
	}

}
