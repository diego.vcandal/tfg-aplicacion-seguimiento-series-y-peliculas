package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FollowingListDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FollowingListTitleDTO;

public class FollowingListConversor {

	public static final FollowingListDTO toFollowingListDTO(TranslatedFollowingList translatedList,
			boolean apiEnabled) {

		List<FollowingListTitleDTO> all = new ArrayList<>();
		List<FollowingListTitleDTO> planToWatch = new ArrayList<>();
		List<FollowingListTitleDTO> watching = new ArrayList<>();
		List<FollowingListTitleDTO> completed = new ArrayList<>();
		List<FollowingListTitleDTO> dropped = new ArrayList<>();

		FollowingList list = translatedList.getFollowingList();
		FollowingListDTO followingListDTO = new FollowingListDTO(list.getId(), list.getUserId().getId(),
				list.getUserId().getUserName(), list.getVisibility() == Visibility.PUBLIC,
				toMillis(list.getLastUpdated()), list.getMovieCount(), list.getTvshowCount(), all, planToWatch,
				watching, completed, dropped);

		addToFollowingStatusList(translatedList.getWatching(), watching, all, apiEnabled);
		addToFollowingStatusList(translatedList.getCompleted(), completed, all, apiEnabled);
		addToFollowingStatusList(translatedList.getDropped(), dropped, all, apiEnabled);
		addToFollowingStatusList(translatedList.getPlanToWatch(), planToWatch, all, apiEnabled);

		return followingListDTO;
	}

	public static final FollowingListTitleDTO toFollowingListTitleDTO(FollowingListTitle followingListTitle) {

		return new FollowingListTitleDTO(followingListTitle.getId(), followingListTitle.getTitleId().getId(),
				followingListTitle.getTitleId().getOriginalTitle(), null,
				followingListTitle.getTitleId().getSourceType().getId(),
				followingListTitle.getTitleId().getTitleType().getId(), followingListTitle.getStatus().getId(),
				toMillis(followingListTitle.getDateAdded()), toMillis(followingListTitle.getDateStarted()),
				toMillis(followingListTitle.getDateFinalized()), followingListTitle.getRating(),
				followingListTitle.isHasReview(), followingListTitle.getTitleId().getExternalImage());
	}

	private static final Long toMillis(LocalDateTime date) {

		if (date == null)
			return null;

		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
	}

	private static final Long toMillis(LocalDate date) {

		if (date == null)
			return null;

		return date.atStartOfDay().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant()
				.toEpochMilli();
	}

	private static void addToFollowingStatusList(List<TranslatedFollowingListTitle> translatedList,
			List<FollowingListTitleDTO> list, List<FollowingListTitleDTO> allList, boolean apiEnabled) {
		translatedList.stream().forEach(t -> {

			FollowingListTitle flTitle = t.getFollowingListTitle();
			Title title = t.getFollowingListTitle().getTitleId();
			String titleName = t.getTitleTraduction() != null ? t.getTitleTraduction().getTitleName() : null;

			FollowingListTitleDTO titleDTO = new FollowingListTitleDTO(flTitle.getId(), title.getId(),
					title.getOriginalTitle(), titleName,
					apiEnabled ? title.getSourceType().getId() : SourceType.INTERNAL.getId(),
					title.getTitleType().getId(), flTitle.getStatus().getId(), toMillis(flTitle.getDateAdded()),
					toMillis(flTitle.getDateStarted()), toMillis(flTitle.getDateFinalized()), flTitle.getRating(),
					flTitle.isHasReview(), title.getExternalImage());

			list.add(titleDTO);
			allList.add(titleDTO);

		});
	}

}
