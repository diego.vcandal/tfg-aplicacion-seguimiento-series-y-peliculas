package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

public class TitleInfo {

	private Long id;
	private Long externalId;
	private String originalTitle;
	private String titleName;
	private String description;
	private TitleType titleType;
	private SourceType sourceType;
	private String originalDescription;
	private String image;
	private short year;

	private float avgRating;
	private int timesRated;

	public TitleInfo(Long id, Long externalId, String originalTitle, String titleName, String description,
			TitleType titleType, SourceType sourceType, String originalDescription, short year, float avgRating,
			int timesRated) {
		this.id = id;
		this.externalId = externalId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.description = description;
		this.titleType = titleType;
		this.sourceType = sourceType;
		this.originalDescription = originalDescription;
		this.year = year;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public TitleInfo(Long id, Long externalId, String originalTitle, String titleName, String description,
			TitleType titleType, SourceType sourceType, String originalDescription, String image, short year,
			float avgRating, int timesRated) {
		this.id = id;
		this.externalId = externalId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.description = description;
		this.titleType = titleType;
		this.sourceType = sourceType;
		this.originalDescription = originalDescription;
		this.image = image;
		this.year = year;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public TitleInfo() {
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public TitleType getTitleType() {
		return titleType;
	}

	public void setTitleType(TitleType titleType) {
		this.titleType = titleType;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public short getYear() {
		return year;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public float getAvgRating() {
		return avgRating;
	}

	public int getTimesRated() {
		return timesRated;
	}

	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}

	public void setTimesRated(int timesRated) {
		this.timesRated = timesRated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((originalDescription == null) ? 0 : originalDescription.hashCode());
		result = prime * result + ((originalTitle == null) ? 0 : originalTitle.hashCode());
		result = prime * result + ((sourceType == null) ? 0 : sourceType.hashCode());
		result = prime * result + ((titleName == null) ? 0 : titleName.hashCode());
		result = prime * result + ((titleType == null) ? 0 : titleType.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitleInfo other = (TitleInfo) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (originalDescription == null) {
			if (other.originalDescription != null)
				return false;
		} else if (!originalDescription.equals(other.originalDescription))
			return false;
		if (originalTitle == null) {
			if (other.originalTitle != null)
				return false;
		} else if (!originalTitle.equals(other.originalTitle))
			return false;
		if (sourceType != other.sourceType)
			return false;
		if (titleName == null) {
			if (other.titleName != null)
				return false;
		} else if (!titleName.equals(other.titleName))
			return false;
		if (titleType != other.titleType)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

}
