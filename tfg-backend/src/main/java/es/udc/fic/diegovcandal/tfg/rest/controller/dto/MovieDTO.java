package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class MovieDTO {

	private Long id;
	private Long externalId;
	private Long sourceType;
	private String originalTitle;
	private String companyName;
	private String originalDescription;
	private String title;
	private String description;
	private short year;
	private boolean visible;
	private boolean setForDelete;

	private float avgRating;
	private int timesRated;

	private short duration;

	private String imageHeaderPath;
	private boolean imageHeader;

	private String imagePortraitPath;
	private boolean imagePortrait;

	private List<GenreDTO> genres;
	private List<TitleTraductionDTO> traductions;

	public MovieDTO() {
	}

	public MovieDTO(Long id, Long externalId, Long sourceType, String originalTitle, String companyName,
			String originalDescription, short year, short duration, String imageHeaderPath, String imagePortraitPath,
			boolean imageHeader, boolean imagePortrait, float avgRating, int timesRated) {
		this.id = id;
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.originalTitle = originalTitle;
		this.companyName = companyName;
		this.originalDescription = originalDescription;
		this.year = year;
		this.imageHeaderPath = imageHeaderPath;
		this.imagePortraitPath = imagePortraitPath;
		this.duration = duration;
		this.imageHeader = imageHeader;
		this.imagePortrait = imagePortrait;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public short getYear() {
		return year;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public List<GenreDTO> getGenres() {
		return genres;
	}

	public void setGenres(List<GenreDTO> genres) {
		this.genres = genres;
	}

	public short getDuration() {
		return duration;
	}

	public void setDuration(short duration) {
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageHeaderPath() {
		return imageHeaderPath;
	}

	public boolean isImageHeader() {
		return imageHeader;
	}

	public String getImagePortraitPath() {
		return imagePortraitPath;
	}

	public boolean isImagePortrait() {
		return imagePortrait;
	}

	public void setImageHeaderPath(String imageHeaderPath) {
		this.imageHeaderPath = imageHeaderPath;
	}

	public void setImageHeader(boolean imageHeader) {
		this.imageHeader = imageHeader;
	}

	public void setImagePortraitPath(String imagePortraitPath) {
		this.imagePortraitPath = imagePortraitPath;
	}

	public void setImagePortrait(boolean imagePortrait) {
		this.imagePortrait = imagePortrait;
	}

	public List<TitleTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<TitleTraductionDTO> traductions) {
		this.traductions = traductions;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public float getAvgRating() {
		return avgRating;
	}

	public int getTimesRated() {
		return timesRated;
	}

	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}

	public void setTimesRated(int timesRated) {
		this.timesRated = timesRated;
	}

}
