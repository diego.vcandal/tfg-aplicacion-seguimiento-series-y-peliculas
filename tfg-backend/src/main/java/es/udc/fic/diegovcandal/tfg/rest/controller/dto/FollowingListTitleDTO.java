package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class FollowingListTitleDTO {

	private Long id;
	private Long titleId;
	private String originalTitle;
	private String title;
	private Long sourceType;
	private Long titleType;
	private String externalImagePath;
	private Long status;
	private Long dateAdded;
	private Long dateStarted;
	private Long dateFinalized;
	private short rating;
	private boolean hasReview;

	public FollowingListTitleDTO(Long id, Long titleId, String originalTitle, String title, Long sourceType,
			Long titleType, Long status, Long dateAdded, Long dateStarted, Long dateFinalized, short rating,
			boolean hasReview, String externalImagePath) {
		this.id = id;
		this.titleId = titleId;
		this.originalTitle = originalTitle;
		this.title = title;
		this.sourceType = sourceType;
		this.titleType = titleType;
		this.status = status;
		this.dateAdded = dateAdded;
		this.dateStarted = dateStarted;
		this.dateFinalized = dateFinalized;
		this.rating = rating;
		this.externalImagePath = externalImagePath;
		this.hasReview = hasReview;
	}

	public Long getId() {
		return id;

	}

	public Long getTitleId() {
		return titleId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getTitle() {
		return title;
	}

	public Long getStatus() {
		return status;
	}

	public Long getDateAdded() {
		return dateAdded;
	}

	public Long getDateStarted() {
		return dateStarted;
	}

	public Long getDateFinalized() {
		return dateFinalized;
	}

	public short getRating() {
		return rating;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public void setDateAdded(Long dateAdded) {
		this.dateAdded = dateAdded;
	}

	public void setDateStarted(Long dateStarted) {
		this.dateStarted = dateStarted;
	}

	public void setDateFinalized(Long dateFinalized) {
		this.dateFinalized = dateFinalized;
	}

	public void setRating(short rating) {
		this.rating = rating;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getExternalImagePath() {
		return externalImagePath;
	}

	public void setExternalImagePath(String externalImagePath) {
		this.externalImagePath = externalImagePath;
	}

	public boolean isHasReview() {
		return hasReview;
	}

	public void setHasReview(boolean hasReview) {
		this.hasReview = hasReview;
	}

	public Long getTitleType() {
		return titleType;
	}

	public void setTitleType(Long titleType) {
		this.titleType = titleType;
	}

}
