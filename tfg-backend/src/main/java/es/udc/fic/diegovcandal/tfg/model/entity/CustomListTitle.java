package es.udc.fic.diegovcandal.tfg.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.CustomListTitleCompositeKey;

@Entity
@IdClass(CustomListTitleCompositeKey.class)
public class CustomListTitle implements Serializable {

	private static final long serialVersionUID = -5472760587984186929L;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "listId")
	private CustomList listId;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "titleId")
	private Title titleId;

	private int position;

	public CustomListTitle() {

	}

	public CustomListTitle(CustomList listId, Title titleId, int position) {
		this.listId = listId;
		this.titleId = titleId;
		this.position = position;
	}

	public CustomList getListId() {
		return listId;
	}

	public Title getTitleId() {
		return titleId;
	}

	public int getPosition() {
		return position;
	}

	public void setListId(CustomList listId) {
		this.listId = listId;
	}

	public void setTitleId(Title titleId) {
		this.titleId = titleId;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
