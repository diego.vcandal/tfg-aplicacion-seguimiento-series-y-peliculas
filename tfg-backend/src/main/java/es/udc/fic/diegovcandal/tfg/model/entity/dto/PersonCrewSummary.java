package es.udc.fic.diegovcandal.tfg.model.entity.dto;

public class PersonCrewSummary {

	private Long titleId;
	private String originalTitle;
	private String titleName;
	private short year;
	private Long crewTypeId;
	private short episodeCount;

	public PersonCrewSummary(Long titleId, short year, String originalTitle, String titleName, Long crewTypeId,
			short episodeCount) {
		this.titleId = titleId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.year = year;
		this.crewTypeId = crewTypeId;
		this.episodeCount = episodeCount;
	}

	public Long getTitleId() {
		return titleId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getTitleName() {
		return titleName;
	}

	public short getYear() {
		return year;
	}

	public Long getCrewTypeId() {
		return crewTypeId;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public void setCrewTypeId(Long crewTypeId) {
		this.crewTypeId = crewTypeId;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

}
