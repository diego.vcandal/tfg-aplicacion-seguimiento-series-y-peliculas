package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class CrewDTO {

	private Long id;
	private Long peopleId;
	private Long externalId;
	private String name;
	private Long crewTypeId;
	private boolean image;
	private String externalImage;
	private Long sourceType;
	private String department;
	private String job;
	private short episodeNumber;

	public CrewDTO(Long id, Long peopleId, Long externalId, String name, Long crewTypeId, boolean image,
			String externalImage, Long sourceType, short episodeNumber) {
		this.id = id;
		this.peopleId = peopleId;
		this.name = name;
		this.crewTypeId = crewTypeId;
		this.image = image;
		this.externalImage = externalImage;
		this.sourceType = sourceType;
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
	}

	public CrewDTO(Long id, Long peopleId, Long externalId, String name, boolean image, String externalImage,
			Long sourceType, String department, String job, short episodeNumber) {
		this.id = id;
		this.peopleId = peopleId;
		this.name = name;
		this.image = image;
		this.externalImage = externalImage;
		this.sourceType = sourceType;
		this.department = department;
		this.job = job;
		this.externalId = externalId;
		this.episodeNumber = episodeNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public Long getCrewTypeId() {
		return crewTypeId;
	}

	public void setCrewTypeId(Long crewTypeId) {
		this.crewTypeId = crewTypeId;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public String getExternalImage() {
		return externalImage;
	}

	public void setExternalImage(String externalImage) {
		this.externalImage = externalImage;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getDepartment() {
		return department;
	}

	public String getJob() {
		return job;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public short getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(short episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

}
