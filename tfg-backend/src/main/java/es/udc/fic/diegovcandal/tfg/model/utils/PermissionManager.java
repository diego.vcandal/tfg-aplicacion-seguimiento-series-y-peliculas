package es.udc.fic.diegovcandal.tfg.model.utils;

import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface PermissionManager {

	public User findUser(Long userId) throws UserNotFoundException;

	User findAdminUser(Long userId) throws UserNotFoundException, PermissionException;

}
