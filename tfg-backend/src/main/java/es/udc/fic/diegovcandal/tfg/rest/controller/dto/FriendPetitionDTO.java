package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class FriendPetitionDTO {

	private Long originUser;
	private Long destinationUser;
	private String originUserName;
	private boolean originUserImage;
	private Long date;

	public FriendPetitionDTO(Long originUser, Long destinationUser, String originUserName, boolean originUserImage,
			Long date) {
		this.originUser = originUser;
		this.destinationUser = destinationUser;
		this.originUserName = originUserName;
		this.originUserImage = originUserImage;
		this.date = date;
	}

	public Long getOriginUser() {
		return originUser;
	}

	public void setOriginUser(Long originUser) {
		this.originUser = originUser;
	}

	public Long getDestinationUser() {
		return destinationUser;
	}

	public void setDestinationUser(Long destinationUser) {
		this.destinationUser = destinationUser;
	}

	public String getOriginUserName() {
		return originUserName;
	}

	public void setOriginUserName(String originUserName) {
		this.originUserName = originUserName;
	}

	public boolean getOriginUserImage() {
		return originUserImage;
	}

	public void setOriginUserImage(boolean originUserImage) {
		this.originUserImage = originUserImage;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

}
