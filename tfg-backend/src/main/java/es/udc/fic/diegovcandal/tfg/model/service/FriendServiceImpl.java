package es.udc.fic.diegovcandal.tfg.model.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.FriendPetitionDao;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.dao.UserFriendOfDao;
import es.udc.fic.diegovcandal.tfg.model.entity.FriendPetition;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.UserFriendOf;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.FriendPetitionCompositeKey;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.UserFriendOfCompositeKey;
import es.udc.fic.diegovcandal.tfg.model.exceptions.AlreadyFriendsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionAlreadySentException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionNotFound;
import es.udc.fic.diegovcandal.tfg.model.exceptions.InstanceNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;

@Service
@Transactional
public class FriendServiceImpl implements FriendService {

	private FriendPetitionDao friendPetitionDao;

	private UserFriendOfDao userFriendOfDao;

	private UserDao userDao;

	@Autowired
	public FriendServiceImpl(FriendPetitionDao friendPetitionDao, UserDao userDao, UserFriendOfDao userFriendOfDao) {
		this.friendPetitionDao = friendPetitionDao;
		this.userDao = userDao;
		this.userFriendOfDao = userFriendOfDao;
	}

	@Override
	@Transactional(readOnly = true)
	public List<FriendPetition> getReceivedFriendPetitions(Long destinationUser) throws UserNotFoundException {

		Optional<User> destination = userDao.findById(destinationUser);
		if (!destination.isPresent())
			throw new UserNotFoundException(destination);

		return friendPetitionDao.findByDestinationUserOrderByDateDesc(destination.get());
	}

	@Override
	@Transactional(readOnly = true)
	public List<FriendPetition> getSentFriendPetitions(Long originUser) throws UserNotFoundException {

		Optional<User> origin = userDao.findById(originUser);
		if (!origin.isPresent())
			throw new UserNotFoundException(originUser);

		return friendPetitionDao.findByOriginUserOrderByDateDesc(origin.get());
	}

	@Override
	public void sendFriendRequest(Long originUser, Long destinationUser) throws FriendPetitionAlreadySentException,
			UserNotFoundException, AlreadyFriendsException, FriendPetitionNotFound {

		if (friendPetitionDao.existsById(new FriendPetitionCompositeKey(destinationUser, originUser))) {
			acceptFriendRequest(destinationUser, originUser);
			return;
		}

		if (friendPetitionDao.existsById(new FriendPetitionCompositeKey(originUser, destinationUser)))
			throw new FriendPetitionAlreadySentException();

		if (userFriendOfDao.existsById(new UserFriendOfCompositeKey(originUser, destinationUser))
				|| userFriendOfDao.existsById(new UserFriendOfCompositeKey(destinationUser, originUser)))
			throw new AlreadyFriendsException();

		Optional<User> origin = userDao.findById(originUser);
		if (!origin.isPresent())
			throw new UserNotFoundException(originUser);

		Optional<User> destination = userDao.findById(destinationUser);
		if (!destination.isPresent())
			throw new UserNotFoundException(destination);

		friendPetitionDao.save(new FriendPetition(origin.get(), destination.get(), LocalDateTime.now()));

	}

	@Override
	public void acceptFriendRequest(Long originUser, Long destinationUser) throws FriendPetitionNotFound {

		FriendPetitionCompositeKey key = new FriendPetitionCompositeKey(originUser, destinationUser);

		Optional<FriendPetition> petition = friendPetitionDao.findById(key);

		if (!petition.isPresent())
			throw new FriendPetitionNotFound("entities.friendPetition", key);

		Optional<User> firstFriend = userDao.findById(originUser);
		Optional<User> secondFriend = userDao.findById(destinationUser);

		userFriendOfDao.save(new UserFriendOf(firstFriend.get(), secondFriend.get(), LocalDateTime.now()));

		friendPetitionDao.delete(petition.get());
	}

	@Override
	public void rejectFriendRequest(Long originUser, Long destinationUser) throws FriendPetitionNotFound {

		FriendPetitionCompositeKey key = new FriendPetitionCompositeKey(originUser, destinationUser);

		Optional<FriendPetition> petition = friendPetitionDao.findById(key);

		if (!petition.isPresent())
			throw new FriendPetitionNotFound("entities.friendPetition", key);

		friendPetitionDao.delete(petition.get());

	}

	@Override
	@Transactional(readOnly = true)
	public Block<UserFriendOf> getFriends(Long userId, int pageNumber, int size) throws UserNotFoundException {

		Optional<User> user = userDao.findById(userId);
		if (!user.isPresent())
			throw new UserNotFoundException(userId);

		Page<UserFriendOf> page = userFriendOfDao.findFriendsByUser(user.get(), PageRequest.of(pageNumber, size));

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());
	}

	@Override
	public void deleteFriend(Long userId, Long friendId) throws InstanceNotFoundException {

		Optional<User> user = userDao.findById(userId);
		if (!user.isPresent())
			throw new UserNotFoundException(userId);

		Optional<User> friend = userDao.findById(friendId);
		if (!friend.isPresent())
			throw new UserNotFoundException(friend);

		Optional<UserFriendOf> userFriendOf = userFriendOfDao.findFriendByUsers(user.get(), friend.get());
		if (!userFriendOf.isPresent())
			throw new InstanceNotFoundException("entities.userFriendOf", friend);

		userFriendOfDao.delete(userFriendOf.get());

	}

	@Override
	@Transactional(readOnly = true)
	public boolean areFriends(Long userId, Long friendId) throws UserNotFoundException {

		Optional<User> user = userDao.findById(userId);
		if (!user.isPresent())
			throw new UserNotFoundException(userId);

		Optional<User> friend = userDao.findById(friendId);
		if (!friend.isPresent())
			throw new UserNotFoundException(friend);

		Optional<UserFriendOf> userFriendOf = userFriendOfDao.findFriendByUsers(user.get(), friend.get());
		return userFriendOf.isPresent();
	}

	@Override
	@Transactional(readOnly = true)
	public boolean hasSentPetition(Long userId, Long destinationId) throws UserNotFoundException {
		return friendPetitionDao.existsById(new FriendPetitionCompositeKey(userId, destinationId));
	}

}
