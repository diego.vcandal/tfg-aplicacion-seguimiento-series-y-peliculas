package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDTO {

	public interface AllValidations {
	}

	public interface UpdateValidations {
	}

	private Long id;
	private String email;
	private String country;
	private String userName;
	private String password;
	private String role;
	private boolean image;
	private RelationshipInfoDTO relationshipInfo;

	public UserDTO() {
	}

	public UserDTO(Long id, String email, String country, String userName, String role, boolean image) {

		this.id = id;
		this.email = email.trim();
		this.country = country.trim();
		this.userName = userName != null ? userName.trim() : null;
		this.role = role;
		this.image = image;
	}

	public UserDTO(Long id, String email, String country, String userName, String role, boolean image,
			RelationshipInfoDTO relationshipInfo) {

		this.id = id;
		this.email = email.trim();
		this.country = country.trim();
		this.userName = userName != null ? userName.trim() : null;
		this.role = role;
		this.image = image;
		this.relationshipInfo = relationshipInfo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull(groups = { AllValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class })
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName.trim();
	}

	@NotNull(groups = { AllValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class })
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@NotNull(groups = { AllValidations.class, UpdateValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class, UpdateValidations.class })
	@Email(groups = { AllValidations.class, UpdateValidations.class })
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.trim();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@NotNull(groups = { AllValidations.class })
	@Size(min = 1, max = 60, groups = { AllValidations.class })
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean getImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public RelationshipInfoDTO getRelationshipInfoDTO() {
		return relationshipInfo;
	}

	public void setRelationshipInfoDTO(RelationshipInfoDTO relationshipInfo) {
		this.relationshipInfo = relationshipInfo;
	}

}
