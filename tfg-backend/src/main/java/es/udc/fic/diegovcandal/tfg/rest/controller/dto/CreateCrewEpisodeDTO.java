package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class CreateCrewEpisodeDTO {

	private Long id;
	private Long titleId;
	private Long personId;
	private byte seasonNumber;
	private short episodeNumber;
	private Long crewTypeId;

	public CreateCrewEpisodeDTO(Long id, Long titleId, Long personId, byte seasonNumber, short episodeNumber,
			Long crewTypeId) {
		this.id = id;
		this.titleId = titleId;
		this.personId = personId;
		this.seasonNumber = seasonNumber;
		this.episodeNumber = episodeNumber;
		this.crewTypeId = crewTypeId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public short getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(short episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public Long getCrewTypeId() {
		return crewTypeId;
	}

	public void setCrewTypeId(Long crewTypeId) {
		this.crewTypeId = crewTypeId;
	}

}
