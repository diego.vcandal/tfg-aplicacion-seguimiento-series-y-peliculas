package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toPersonCastSummaryDTOFromExternals;
import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toPersonCastSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toPersonCrewSummaryDTOFromExternals;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toPersonCrewSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.PeopleConversor.fromExternalPeopleToPeopleWithTraductionDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.PeopleConversor.toPeople;
import static es.udc.fic.diegovcandal.tfg.rest.util.PeopleConversor.toPeopleDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.PeopleConversor.toPeopleWithTraductionDTO;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PeopleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPIClientPeopleService;
import es.udc.fic.diegovcandal.tfg.model.service.ImageService;
import es.udc.fic.diegovcandal.tfg.model.service.PeopleService;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreatePersonDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.IdDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PeopleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PeopleWithTraductionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonCastSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonCreditsSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonCrewSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalPersonCreditsSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.exceptions.ApiNotEnabledException;
import es.udc.fic.diegovcandal.tfg.rest.util.Util;

@RestController
@RequestMapping("/people")
public class PeopleController {

	private static final String TITLE_NOT_FOUND_EXCEPTION_ID = "exceptions.PeopleNotFoundException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private PeopleService peopleService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private TmdbAPIClientPeopleService tmdbAPIClientPeopleService;

	@Value("${project.external.api.enabled}")
	private boolean apiEnabled;

	@ExceptionHandler(PeopleNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handlePeopleNotFoundException(PeopleNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(TITLE_NOT_FOUND_EXCEPTION_ID, null, TITLE_NOT_FOUND_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@PostMapping()
	public IdDTO createPerson(@RequestAttribute Long userId, @Validated @RequestBody CreatePersonDTO personDTO)
			throws UserNotFoundException, PermissionException, ExternalIdAlreadyLinkedException {

		People people = toPeople(personDTO);

		return new IdDTO(peopleService.createPerson(userId, people));
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void uptadePerson(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody CreatePersonDTO personDTO)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		peopleService.uptadePerson(userId, id, toPeople(personDTO));

	}

	@GetMapping("/internal/{id}")
	public PeopleDTO getInternalPerson(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		return toPeopleDTO(peopleService.getInternalPerson(id, userId), true);

	}

	@GetMapping("/{id}/image")
	public void findPersonImage(@PathVariable Long id, HttpServletResponse response) throws PeopleNotFoundException {

		String image = imageService.getPeopleImage(id);

		Util.sendImage(response, image);

	}

	@GetMapping("/{id}")
	public PeopleWithTraductionDTO getPerson(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws PeopleNotFoundException, UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		PeopleWithTraduction peopleWithTraduction = peopleService.getPersonDetails(userId, id, localeId);

		People people = peopleWithTraduction.getPeople();

		if (apiEnabled && people.getSourceType() != SourceType.INTERNAL)
			return fromExternalPeopleToPeopleWithTraductionDTO(
					tmdbAPIClientPeopleService.findCachedPerson(people.getExternalId(), people.getId(), acceptLanguage),
					people.isSetForDelete());

		return toPeopleWithTraductionDTO(peopleWithTraduction, SourceType.INTERNAL);

	}

	@GetMapping("/tmdb/{id}")
	public PeopleWithTraductionDTO getExternalPerson(@RequestAttribute(required = false) Long userId,
			@PathVariable Long id, @RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException {

		Long personId = peopleService.existsByExternalId(id);

		if (personId == -1L)
			return fromExternalPeopleToPeopleWithTraductionDTO(
					tmdbAPIClientPeopleService.findExternalPeople(id, acceptLanguage), false);

		try {

			return getPerson(userId, personId, acceptLanguage);

		} catch (PeopleNotFoundException e) {

			return fromExternalPeopleToPeopleWithTraductionDTO(
					tmdbAPIClientPeopleService.findExternalPeople(id, acceptLanguage), false);

		}

	}

	@PostMapping("/tmdb/{id}/create")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createExternalMovie(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws PeopleNotFoundException, UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException {

		tmdbAPIClientPeopleService.createExternalPerson(userId, id, acceptLanguage);

	}

	@PostMapping("/{id}/makePublic")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void makePersonPublic(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {
		peopleService.makePersonPublic(userId, id);
	}

	@PostMapping("/{id}/setForDeletion")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void setPersonForDeletion(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {
		peopleService.setPersonForDeletion(userId, id, true);
	}

	@PostMapping("/{id}/changeSourceType")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changeSourceType(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {
		peopleService.changeSourceType(userId, id);
	}

	@GetMapping("/{id}/externalImage")
	public void findExternalImage(@PathVariable Long id, HttpServletResponse response) throws PeopleNotFoundException {

		People people = null;

		try {
			people = peopleService.findByExternalId(id);
		} catch (PeopleNotFoundException e) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}

		String image = people.getExternalImage();

		if (image == null || image.equals("")) {

			if (people.getExternalId() == null) {
				response.setStatus(HttpStatus.NOT_FOUND.value());
				return;
			}

			image = tmdbAPIClientPeopleService
					.findExternalPeople(people.getExternalId(), LocaleType.DEFAULT_LOCALE.toString()).getProfile_path();

		}

		byte[] decoded = tmdbAPIClientPeopleService.getExternalImage(image);

		InputStream inputStream = new ByteArrayInputStream(decoded);

		try {
			IOUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@GetMapping("/{id}/cast")
	public List<PersonCastSummaryDTO> findCastHistory(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws PeopleNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toPersonCastSummaryDTOs(peopleService.findCastHistory(id, localeId));

	}

	@GetMapping("/{id}/crew")
	public List<PersonCrewSummaryDTO> findCrewHistory(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws PeopleNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toPersonCrewSummaryDTOs(peopleService.findCrewHistory(id, localeId));

	}

	@GetMapping("/tmdb/{id}/cast-crew")
	public PersonCreditsSummaryDTO findCastCrewExternalHistory(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws PeopleNotFoundException, ApiNotEnabledException {

		Util.checkApiEnabled(apiEnabled);

		ExternalPersonCreditsSummaryDTO credits = tmdbAPIClientPeopleService.findPersonCastAndCrewHistory(id,
				acceptLanguage);

		return new PersonCreditsSummaryDTO(toPersonCastSummaryDTOFromExternals(credits.getCast()),
				toPersonCrewSummaryDTOFromExternals(credits.getCrew()));

	}

}
