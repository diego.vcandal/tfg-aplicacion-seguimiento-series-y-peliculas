package es.udc.fic.diegovcandal.tfg.rest.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CastSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCastSummary;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCastEpisodeDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreatePersonCastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateSeasonCastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.InternalPersonCastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonCastSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.CastExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalCastRoleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalPersonCastSummaryDTO;

public class CastConversor {

	public static final Cast toCast(CreatePersonCastDTO createPersonCastDTO) {

		Cast cast = new Cast(null, null, (short) 0, createPersonCastDTO.getName(), createPersonCastDTO.isMainCast());

		if (createPersonCastDTO.getSeasons() != null && createPersonCastDTO.getSeasons().size() > 0) {

			Set<CastSeason> seasons = new HashSet<>();

			for (CreateSeasonCastDTO createSeasonCastDTO : createPersonCastDTO.getSeasons()) {

				if (createSeasonCastDTO.getEpisodes().size() >= 0) {

					Season season = new Season();
					season.setSeasonNumber(createSeasonCastDTO.getSeasonNumber());
					CastSeason castSeason = new CastSeason(cast, season, (short) 0);

					Set<CastEpisode> episodes = new HashSet<>();

					for (Long episodeNumber : createSeasonCastDTO.getEpisodes()) {

						Episode episode = new Episode();
						episode.setEpisodeNumber(episodeNumber.shortValue());

						episodes.add(new CastEpisode(castSeason, episode, createPersonCastDTO.getName()));
					}

					castSeason.setCastEpisodes(episodes);
					seasons.add(castSeason);
				}

			}

			cast.setCastSeasons(seasons);

		}

		return cast;
	}

	public static final CastEpisode toEpisodeCast(CreateCastEpisodeDTO createCastEpisodeDTO) {

		Episode episode = new Episode();
		episode.setEpisodeNumber(createCastEpisodeDTO.getEpisodeNumber());

		return new CastEpisode(null, episode, createCastEpisodeDTO.getCharName());
	}

	public static final List<CastDTO> toCastDTOs(List<Cast> casts) {
		return casts.stream().map(p -> toCastDTO(p)).collect(Collectors.toList());
	}

	public static final List<CastDTO> toCastEpisodeDTOs(List<CastEpisode> casts) {
		return casts.stream().map(p -> toCastDTO(p)).collect(Collectors.toList());
	}

	public static final List<CastDTO> toCastEpisodeDTOsfromExternal(List<CastExternalDTO> casts, boolean isMainCast) {
		return casts.stream().map(p -> toCastDTO(p, isMainCast)).collect(Collectors.toList());
	}

	public static final List<InternalPersonCastDTO> toInternalCastDTOs(List<Cast> casts) {
		return casts.stream().map(p -> toInternalCastDTO(p)).collect(Collectors.toList());
	}

	public static final List<PersonCastSummaryDTO> toPersonCastSummaryDTOs(List<PersonCastSummary> casts) {
		return casts.stream().map(p -> toPersonCastSummaryDTO(p)).collect(Collectors.toList());
	}

	public static final List<PersonCastSummaryDTO> toPersonCastSummaryDTOFromExternals(
			List<ExternalPersonCastSummaryDTO> casts) {
		return casts.stream().map(p -> toPersonCastSummaryDTO(p)).collect(Collectors.toList());
	}

	private static final CastDTO toCastDTO(Cast cast) {

		boolean hasImage = cast.getPeopleId().getImage() != null;

		return new CastDTO(cast.getId(), cast.getPeopleId().getId(), cast.getPeopleId().getExternalId(),
				cast.getPeopleId().getOriginalName(), cast.getCharacterName(), hasImage,
				cast.getPeopleId().getExternalImage(), cast.getPeopleId().getSourceType().getId(), cast.isMainCast(),
				cast.getEpisodeCount());
	}

	private static final CastDTO toCastDTO(CastEpisode cast) {

		People people = cast.getCastSeasonId().getCastId().getPeopleId();
		boolean hasImage = people.getImage() != null;

		return new CastDTO(cast.getId(), people.getId(), people.getExternalId(), people.getOriginalName(),
				cast.getCharacterName(), hasImage, people.getExternalImage(), people.getSourceType().getId(), false,
				(short) -1);
	}

	private static final CastDTO toCastDTO(CastExternalDTO cast, boolean isMainCast) {

		String charName = cast.getCharacter();
		short episodes = -1;

		if (charName == null || charName.equals("")) {
			ExternalCastRoleDTO role = cast.getRoles().stream().findFirst().get();
			charName = role.getCharacter();
			episodes = role.getEpisode_count();
		}

		return new CastDTO(-1L, cast.getId(), cast.getId(),
				cast.getName() != null ? cast.getName() : cast.getOriginal_name(), charName, false,
				cast.getProfile_path(), SourceType.EXTERNAL_TMDB.getId(), isMainCast, episodes);
	}

	private static final InternalPersonCastDTO toInternalCastDTO(Cast cast) {

		List<CreateSeasonCastDTO> seasons = new ArrayList<>();
		for (CastSeason castSeason : cast.getCastSeasons()) {
			List<Long> episodes = new ArrayList<>();
			for (CastEpisode castEpisode : castSeason.getCastEpisodes()) {
				episodes.add((long) castEpisode.getEpisodeId().getEpisodeNumber());
			}
			seasons.add(new CreateSeasonCastDTO(castSeason.getSeasonId().getSeasonNumber(), episodes));

		}

		boolean hasImage = cast.getPeopleId().getImage() != null;

		return new InternalPersonCastDTO(cast.getId(), cast.getTitleId().getId(), cast.getPeopleId().getId(),
				cast.getPeopleId().getExternalId(), cast.getPeopleId().getOriginalName(), hasImage,
				cast.getPeopleId().getExternalImage(), cast.getPeopleId().getSourceType().getId(),
				cast.getCharacterName(), cast.isMainCast(), seasons);

	}

	private static final PersonCastSummaryDTO toPersonCastSummaryDTO(PersonCastSummary cast) {
		return new PersonCastSummaryDTO(cast.getTitleId(), cast.getOriginalTitle(), cast.getTitleName(), cast.getYear(),
				cast.getCharacterName(), cast.getEpisodeCount());
	}

	private static final PersonCastSummaryDTO toPersonCastSummaryDTO(ExternalPersonCastSummaryDTO cast) {

		if (cast.getMedia_type().equals("movie")) {

			short year = cast.getRelease_date() == null || cast.getRelease_date().equals("") ? -1
					: Short.parseShort(cast.getRelease_date().substring(0, 4));

			return new PersonCastSummaryDTO(cast.getId(), cast.getOriginal_title(), cast.getTitle(), year,
					cast.getCharacter(), (short) 0);
		} else {

			short year = cast.getFirst_air_date() == null || cast.getFirst_air_date().equals("") ? -1
					: Short.parseShort(cast.getFirst_air_date().substring(0, 4));

			return new PersonCastSummaryDTO(cast.getId(), cast.getOriginal_name(), cast.getName(), year,
					cast.getCharacter(), cast.getEpisode_count());
		}
	}

}
