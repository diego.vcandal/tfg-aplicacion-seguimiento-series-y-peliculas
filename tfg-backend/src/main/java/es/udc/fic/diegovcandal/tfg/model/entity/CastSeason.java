package es.udc.fic.diegovcandal.tfg.model.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class CastSeason {

	private Long id;
	private Cast castId;
	private Season seasonId;
	private short episodeCount;

	private Set<CastEpisode> castEpisodes;

	public CastSeason() {
		this.castEpisodes = new HashSet<>();
	}

	public CastSeason(Cast castId, Season seasonId, short episodeCount) {
		this.castId = castId;
		this.seasonId = seasonId;
		this.episodeCount = episodeCount;

		this.castEpisodes = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "castId")
	public Cast getCastId() {
		return castId;
	}

	public void setCastId(Cast castId) {
		this.castId = castId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "seasonId")
	public Season getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(Season seasonId) {
		this.seasonId = seasonId;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	@OneToMany(mappedBy = "castSeasonId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CastEpisode> getCastEpisodes() {
		return castEpisodes;
	}

	public void setCastEpisodes(Set<CastEpisode> castEpisodes) {
		this.castEpisodes = castEpisodes;
	}

}
