package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class BlockDTO<T> {

	private List<T> items;
	private boolean existMoreItems;
	private int totalPages;
	private long totalElements;

	private String searchCondition;

	public BlockDTO() {
	}

	public BlockDTO(List<T> items, boolean existMoreItems, int totalPages, long totalElements) {
		this.items = items;
		this.existMoreItems = existMoreItems;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
	}

	public BlockDTO(List<T> items, boolean existMoreItems, int totalPages, long totalElements, String searchCondition) {
		this.items = items;
		this.existMoreItems = existMoreItems;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
		this.searchCondition = searchCondition;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public boolean getExistMoreItems() {
		return existMoreItems;
	}

	public void setExistMoreItems(boolean existMoreItems) {
		this.existMoreItems = existMoreItems;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public String getSearchCondition() {
		return searchCondition;
	}

	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

}
