package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class FollowingListDTO {

	private Long id;
	private Long userId;
	private String userName;
	private boolean visible;
	private Long lastUpdated;
	private short movieCount;
	private short tvshowCount;

	private List<FollowingListTitleDTO> all;
	private List<FollowingListTitleDTO> planToWatch;
	private List<FollowingListTitleDTO> watching;
	private List<FollowingListTitleDTO> completed;
	private List<FollowingListTitleDTO> dropped;

	public FollowingListDTO() {
	}

	public FollowingListDTO(Long id, Long userId, String userName, boolean visible, Long lastUpdated, short movieCount,
			short tvshowCount) {
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.visible = visible;
		this.lastUpdated = lastUpdated;
		this.movieCount = movieCount;
		this.tvshowCount = tvshowCount;
	}

	public FollowingListDTO(Long id, Long userId, String userName, boolean visible, Long lastUpdated, short movieCount,
			short tvshowCount, List<FollowingListTitleDTO> all, List<FollowingListTitleDTO> planToWatch,
			List<FollowingListTitleDTO> watching, List<FollowingListTitleDTO> completed,
			List<FollowingListTitleDTO> dropped) {
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.visible = visible;
		this.lastUpdated = lastUpdated;
		this.movieCount = movieCount;
		this.tvshowCount = tvshowCount;
		this.all = all;
		this.planToWatch = planToWatch;
		this.watching = watching;
		this.completed = completed;
		this.dropped = dropped;
	}

	public Long getId() {
		return id;
	}

	public Long getUserId() {
		return userId;
	}

	public boolean isVisible() {
		return visible;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public short getMovieCount() {
		return movieCount;
	}

	public short getTvshowCount() {
		return tvshowCount;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public void setMovieCount(short movieCount) {
		this.movieCount = movieCount;
	}

	public void setTvshowCount(short tvshowCount) {
		this.tvshowCount = tvshowCount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<FollowingListTitleDTO> getPlanToWatch() {
		return planToWatch;
	}

	public List<FollowingListTitleDTO> getWatching() {
		return watching;
	}

	public List<FollowingListTitleDTO> getCompleted() {
		return completed;
	}

	public List<FollowingListTitleDTO> getDropped() {
		return dropped;
	}

	public void setPlanToWatch(List<FollowingListTitleDTO> planToWatch) {
		this.planToWatch = planToWatch;
	}

	public void setWatching(List<FollowingListTitleDTO> watching) {
		this.watching = watching;
	}

	public void setCompleted(List<FollowingListTitleDTO> completed) {
		this.completed = completed;
	}

	public void setDropped(List<FollowingListTitleDTO> dropped) {
		this.dropped = dropped;
	}

	public List<FollowingListTitleDTO> getAll() {
		return all;
	}

	public void setAll(List<FollowingListTitleDTO> all) {
		this.all = all;
	}

}
