package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class PeopleNotFoundException extends InstanceNotFoundException {

	public PeopleNotFoundException(Object key) {
		super("entities.people", key);
	}
}
