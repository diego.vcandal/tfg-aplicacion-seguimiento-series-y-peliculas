package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class RelationshipInfoDTO {

	private boolean areFriends;
	private boolean sentPetition;
	private boolean receivedPetition;

	public RelationshipInfoDTO(boolean areFriends, boolean sentPetition, boolean receivedPetition) {
		super();
		this.areFriends = areFriends;
		this.sentPetition = sentPetition;
		this.receivedPetition = receivedPetition;
	}

	public boolean isAreFriends() {
		return areFriends;
	}

	public void setAreFriends(boolean areFriends) {
		this.areFriends = areFriends;
	}

	public boolean isSentPetition() {
		return sentPetition;
	}

	public void setSentPetition(boolean sentPetition) {
		this.sentPetition = sentPetition;
	}

	public boolean isReceivedPetition() {
		return receivedPetition;
	}

	public void setReceivedPetition(boolean receivedPetition) {
		this.receivedPetition = receivedPetition;
	}

}
