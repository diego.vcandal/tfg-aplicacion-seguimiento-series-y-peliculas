package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedCustomList;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCustomListDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCustomListTitleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CustomListDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CustomListSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CustomListTranslatedTitleDTO;

public class CustomListConversor {

	public static final CustomList toCustomList(CreateCustomListDTO createCustomListDTO) {

		CustomList list = new CustomList(null, createCustomListDTO.isVisible() ? Visibility.PUBLIC : Visibility.PRIVATE,
				null, createCustomListDTO.getTitle(), createCustomListDTO.getDescription(), 0, 0,
				createCustomListDTO.getImage());

		if (createCustomListDTO.getTitles() != null) {
			for (CreateCustomListTitleDTO customListTitleDTO : createCustomListDTO.getTitles()) {
				Title title = new Title();
				title.setId(customListTitleDTO.getId());
				list.getCustomListTitles().add(new CustomListTitle(null, title, customListTitleDTO.getPosition()));
			}
		}

		return list;
	}

	public static final List<CustomListSummaryDTO> toCustomListSummaryDTOs(List<CustomList> lists) {
		return lists.stream().map(c -> toCustomListSummaryDTO(c)).collect(Collectors.toList());
	}

	private static final CustomListSummaryDTO toCustomListSummaryDTO(CustomList list) {

		boolean hasImage = list.getImage() != null;

		return new CustomListSummaryDTO(list.getId(), list.getUserId().getId(), list.getUserId().getUserName(),
				list.getVisibility() == Visibility.PUBLIC ? true : false, toMillis(list.getLastUpdated()),
				list.getTitle(), list.getDescription(), list.getLikeCount(), list.getTitleCount(), hasImage);
	}

	public static final CustomListDTO toCustomListDTO(TranslatedCustomList translatedCustomList) {

		CustomList customList = translatedCustomList.getCustomList();
		boolean hasImage = customList.getImage() != null;

		List<CustomListTranslatedTitleDTO> titles = new ArrayList<>();

		translatedCustomList.getTitles().forEach(tt -> {
			Title title = tt.getCustomListTitle().getTitleId();
			TitleTraduction titleTraduction = tt.getTitleTraduction();

			if (titleTraduction != null)
				titles.add(new CustomListTranslatedTitleDTO(title.getId(), tt.getCustomListTitle().getPosition(),
						titleTraduction.getIdLocale(), title.getOriginalTitle(), titleTraduction.getTitleName(),
						title.getSourceType().getId(), title.getTitleType().getId(), title.getExternalId(),
						title.getExternalImage(), title.getAvgRating(), title.getTimesRated()));
			else
				titles.add(new CustomListTranslatedTitleDTO(title.getId(), tt.getCustomListTitle().getPosition(),
						title.getOriginalTitle(), title.getSourceType().getId(), title.getTitleType().getId(),
						title.getExternalId(), title.getExternalImage(), title.getAvgRating(), title.getTimesRated()));

		});

		return new CustomListDTO(customList.getId(), customList.getUserId().getId(),
				customList.getUserId().getUserName(), customList.getVisibility() == Visibility.PUBLIC ? true : false,
				toMillis(customList.getLastUpdated()), customList.getTitle(), customList.getDescription(),
				customList.getLikeCount(), customList.getTitleCount(), hasImage, titles, customList.isLiked());

	}

	public static final Set<CustomListTitle> toCustomListTitlesSet(List<CreateCustomListTitleDTO> titles) {

		Set<CustomListTitle> newList = new HashSet<>();

		for (CreateCustomListTitleDTO customListTitleDTO : titles) {
			Title title = new Title();
			title.setId(customListTitleDTO.getId());
			newList.add(new CustomListTitle(null, title, customListTitleDTO.getPosition()));
		}

		return newList;

	}

	private static final Long toMillis(LocalDateTime date) {

		if (date == null)
			return null;

		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
	}

}
