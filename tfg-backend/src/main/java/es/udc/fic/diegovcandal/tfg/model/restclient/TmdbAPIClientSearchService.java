package es.udc.fic.diegovcandal.tfg.model.restclient;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.BlockDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TitleInfoDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.BlockExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

public interface TmdbAPIClientSearchService {

	BlockExternalDTO<MovieExternalDTO> findExternalMovies(String titleName, int page, String acceptLanguage);

	BlockExternalDTO<TVShowExternalDTO> findExternalTVShows(String titleName, int page, String acceptLanguage);

	BlockDTO<TitleInfoDTO> mergeTitleSearch(String titleName, String acceptLanguage, List<TitleInfo> internalTitles);

}
