package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class UserSummaryDTO {

	private Long id;
	private String country;
	private String userName;
	private boolean image;

	public UserSummaryDTO() {
	}

	public UserSummaryDTO(Long id, String country, String userName, boolean image) {
		super();
		this.id = id;
		this.country = country;
		this.userName = userName;
		this.image = image;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean getImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

}
