package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingListTitle;

public interface FollowingListTitleDAO extends PagingAndSortingRepository<FollowingListTitle, Long> {

	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingListTitle(ftt, tt) "
			+ "FROM FollowingListTitle ftt " 
			+ "		JOIN Title t " 
			+ "			on t.id = ftt.titleId "
			+ "		LEFT JOIN TitleTraduction tt " 
			+ "			on tt.idTitle = t.id AND "
			+ "				tt.idLocale = ?2 " 
			+ "WHERE ftt.listId = ?1 "
			+ "ORDER BY tt.titleName, t.originalTitle")
	List<TranslatedFollowingListTitle> findFollowingListTranslatedTitles(FollowingList listId, Long localeId);

	Optional<FollowingListTitle> findByTitleIdAndListId(Title titleId, FollowingList listId);

}
