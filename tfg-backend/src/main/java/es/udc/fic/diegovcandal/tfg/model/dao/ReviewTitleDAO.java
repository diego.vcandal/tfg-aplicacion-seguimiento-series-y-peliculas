package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;

public interface ReviewTitleDAO extends PagingAndSortingRepository<ReviewTitle, Long> {

	Optional<ReviewTitle> findByTitleIdAndUserId(Title titleId, User userId);

	Page<ReviewTitle> findByTitleIdAndContainsReviewTrue(Title titleId, Pageable pageable);

}
