package es.udc.fic.diegovcandal.tfg.rest.util;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.UserFriendOf;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.AuthenticatedUserDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FriendPetitionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.RelationshipInfoDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserCreationParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserSummaryDTO;

public class UserConversor {

	public static final User toUser(UserCreationParamsDTO userDTO) {

		String image = userDTO.getImage() == null ? null : userDTO.getImage();

		return new User(userDTO.getId(), userDTO.getEmail(), userDTO.getCountry(), userDTO.getUserName(),
				userDTO.getPassword(), image);
	}

	public static final AuthenticatedUserDTO toAuthenticatedUserDTO(String serviceToken, User user,
			List<FriendPetitionDTO> petitions) {
		return new AuthenticatedUserDTO(serviceToken, toUserDTO(user, null), petitions);
	}

	public static final UserDTO toUserDTO(User user, RelationshipInfoDTO relationshipInfoDTO) {

		return new UserDTO(user.getId(), user.getEmail(), user.getCountry(), user.getUserName(),
				user.getRole().toString(), user.getImage() != null, relationshipInfoDTO);

	}

	public static final List<UserSummaryDTO> toFriendsSummaryDTOs(Long userId, List<UserFriendOf> userFriendOfs) {
		return userFriendOfs.stream().map(friendOf -> toFriendSummaryDTO(userId, friendOf))
				.collect(Collectors.toList());
	}

	private static final UserSummaryDTO toFriendSummaryDTO(Long userId, UserFriendOf userFriendOf) {

		if (userFriendOf.getFirstFriendId().getId() == userId)
			return toUserSummaryDTO(userFriendOf.getSecondFriendId());
		else
			return toUserSummaryDTO(userFriendOf.getFirstFriendId());

	}

	public static final List<UserSummaryDTO> toUserSummaryDTOs(List<User> users) {
		return users.stream().map(p -> toUserSummaryDTO(p)).collect(Collectors.toList());
	}

	private static final UserSummaryDTO toUserSummaryDTO(User user) {

		return new UserSummaryDTO(user.getId(), user.getCountry(), user.getUserName(), user.getImage() != null);
	}

}
