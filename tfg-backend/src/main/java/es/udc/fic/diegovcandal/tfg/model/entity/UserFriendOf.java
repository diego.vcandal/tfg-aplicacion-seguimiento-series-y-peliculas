package es.udc.fic.diegovcandal.tfg.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.UserFriendOfCompositeKey;

@Entity
@IdClass(UserFriendOfCompositeKey.class)
public class UserFriendOf implements Serializable {

	private static final long serialVersionUID = 2022245324474579795L;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "firstFriendId")
	private User firstFriendId;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "secondFriendId")
	private User secondFriendId;

	private LocalDateTime date;

	public UserFriendOf() {
	}

	public UserFriendOf(User firstFriendId, User secondFriendId, LocalDateTime date) {
		this.firstFriendId = firstFriendId;
		this.secondFriendId = secondFriendId;
		this.date = date;
	}

	public User getFirstFriendId() {
		return firstFriendId;
	}

	public void setFirstFriendId(User firstFriendId) {
		this.firstFriendId = firstFriendId;
	}

	public User getSecondFriendId() {
		return secondFriendId;
	}

	public void setSecondFriendId(User secondFriendId) {
		this.secondFriendId = secondFriendId;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

}
