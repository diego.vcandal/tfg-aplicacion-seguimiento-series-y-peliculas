package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

public class ExternalPersonCrewSummaryDTO {

	private Long id;
	private String original_title;
	private String title;
	private String original_name;
	private String name;
	private String first_air_date;
	private String release_date;
	private String department;
	private String media_type;
	private short episode_count;

	public Long getId() {
		return id;
	}

	public String getOriginal_name() {
		return original_name;
	}

	public String getName() {
		return name;
	}

	public String getFirst_air_date() {
		return first_air_date;
	}

	public String getRelease_date() {
		return release_date;
	}

	public String getDepartment() {
		return department;
	}

	public short getEpisode_count() {
		return episode_count;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOriginal_name(String original_name) {
		this.original_name = original_name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFirst_air_date(String first_air_date) {
		this.first_air_date = first_air_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setEpisode_count(short episode_count) {
		this.episode_count = episode_count;
	}

	public String getOriginal_title() {
		return original_title;
	}

	public String getTitle() {
		return title;
	}

	public void setOriginal_title(String original_title) {
		this.original_title = original_title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMedia_type() {
		return media_type;
	}

	public void setMedia_type(String media_type) {
		this.media_type = media_type;
	}

}
