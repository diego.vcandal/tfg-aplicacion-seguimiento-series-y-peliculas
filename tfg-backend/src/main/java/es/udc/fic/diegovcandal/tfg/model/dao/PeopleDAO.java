package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.People;

public interface PeopleDAO extends PagingAndSortingRepository<People, Long> {

	boolean existsByExternalId(Long externalId);

	Optional<People> findByExternalId(Long externalId);

	@Query("SELECT p "
			+ "FROM People p LEFT JOIN PeopleTraduction pt "
			+ "on p.id = pt.idPeople "
			+ "WHERE pt.name LIKE %?1% AND "
			+ "visibility = 0"
			+ "GROUP BY p.id")
	Page<People> findPublicPeopleByName(String name, Pageable pageable);
	
	@Query("SELECT p "
			+ "FROM People p LEFT JOIN PeopleTraduction pt "
			+ "on p.id = pt.idPeople "
			+ "WHERE pt.name LIKE %?1% "
			+ "GROUP BY p.id")
	Page<People> findAllPeopleByName(String name, Pageable pageable);
	
	
}
