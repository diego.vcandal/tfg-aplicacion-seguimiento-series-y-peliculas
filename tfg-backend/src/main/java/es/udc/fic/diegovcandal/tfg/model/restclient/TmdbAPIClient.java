package es.udc.fic.diegovcandal.tfg.model.restclient;

import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.SeasonSummaryExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

public interface TmdbAPIClient {

	MovieExternalDTO findCachedMovie(Long externalId, Long internalId, String acceptLanguage)
			throws TitleNotFoundException;

	MovieExternalDTO findExternalMovie(Long externalId, String acceptLanguage) throws TitleNotFoundException;

	TVShowExternalDTO findCachedTVShow(Long externalId, Long internalId, String acceptLanguage)
			throws TitleNotFoundException;

	TVShowExternalDTO findExternalTVShow(Long externalId, String acceptLanguage) throws TitleNotFoundException;

	SeasonSummaryExternalDTO findExternalSeason(Long externalId, byte seasonNumber, String acceptLanguage)
			throws TitleNotFoundException;

	byte[] getExternalPortraitImage(String image) throws TitleNotFoundException;

	Season createExternalSeason(Long userId, Long externalId, Long externalSeasonId, byte seasonNumber,
			String acceptLanguage)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException, PermissionException;

	void createExternalEpisodes(Long userId, Long externalId, Long externalSeasonId, byte seasonNumber,
			String acceptLanguage)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException, PermissionException;

	void createExternalEpisode(Long userId, Long externalId, Long externalSeasonId, byte seasonNumber,
			short episodeNumber, String acceptLanguage)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException, PermissionException;

	TVShow createExternalTVShow(Long userId, Long externalId, String acceptLanguage, boolean automaticCreation)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException, PermissionException;

	Movie createExternalMovie(Long userId, Long externalId, String acceptLanguage, boolean automaticCreation)
			throws TitleNotFoundException, ExternalIdAlreadyLinkedException, UserNotFoundException, PermissionException;

	void rateTitle(Long userId, Long titleId, byte rating, TitleType titleType, String acceptLanguage)
			throws IncorrectRatingException, TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException;

	ReviewTitle addNewReview(Long userId, Long titleId, String reviewTitle, String content, byte rating,
			TitleType titleType, String acceptLanguage) throws IncorrectRatingException, TitleNotFoundException,
			UserNotFoundException, ExternalIdAlreadyLinkedException, PermissionException, IncorrectOperationException;

	void addToFollowingList(Long userId, Long titleId, Long status, TitleType titleType, String acceptLanguage)
			throws IncorrectOperationException, UserNotFoundException, TitleNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException;

}
