package es.udc.fic.diegovcandal.tfg.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.CrewType;

public interface CrewTypeDAO extends PagingAndSortingRepository<CrewType, Long> {

}
