package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toCrew;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toCrewDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toCrewEpisodeDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toEpisodeCrew;
import static es.udc.fic.diegovcandal.tfg.rest.util.CrewConversor.toInternalCrewDTOs;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewTypeNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.CrewService;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCrewEpisodeDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreatePersonCrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.IdDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.InternalPersonCrewDTO;

@RestController
@RequestMapping("/credits/crews")
public class CrewController {

	private static final String CREW_NOT_FOUND_EXCEPTION_ID = "exceptions.CrewNotFoundException";
	private static final String CREW_ALREADY_EXISTS_EXCEPTION_ID = "exceptions.CrewAlreadyExistsException";
	private static final String CREW_TYPE_NOT_FOUND_EXCEPTION_ID = "exceptions.CrewTypeNotFoundException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private CrewService crewService;

	@Value("${project.external.api.enabled}")
	private boolean apiEnabled;

	@ExceptionHandler(CrewNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleCrewNotFoundException(CrewNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(CREW_NOT_FOUND_EXCEPTION_ID, null, CREW_NOT_FOUND_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(CrewAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleCrewAlreadyExistsException(CrewAlreadyExistsException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(CREW_ALREADY_EXISTS_EXCEPTION_ID, null,
				CREW_ALREADY_EXISTS_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(CrewTypeNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleCrewTypeNotFoundException(CrewTypeNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(CREW_TYPE_NOT_FOUND_EXCEPTION_ID, null,
				CREW_TYPE_NOT_FOUND_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@PostMapping("/titles")
	public IdDTO addPersonNewCrewParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreatePersonCrewDTO personCrewDTO)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewTypeNotFoundException {

		Crew crew = toCrew(personCrewDTO);

		return new IdDTO(crewService.addPersonNewCrewParticipation(userId, personCrewDTO.getPersonId(),
				personCrewDTO.getTitleId(), crew));
	}

	@PutMapping("/titles")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updatePersonCrewParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreatePersonCrewDTO personCrewDTO)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewNotFoundException {

		Crew crew = toCrew(personCrewDTO);

		crewService.updatePersonCrewParticipation(userId, personCrewDTO.getId(), crew);
	}

	@DeleteMapping("/titles/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removePersonCrewParticipation(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewNotFoundException {

		crewService.removePersonCrewParticipation(userId, id);
	}

	@PostMapping("/episodes")
	public IdDTO addPersonEpisodeCrewParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreateCrewEpisodeDTO createCrewEpisodeDTO)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewTypeNotFoundException {

		CrewEpisode crew = toEpisodeCrew(createCrewEpisodeDTO);

		return new IdDTO(crewService.addPersonEpisodeCrewParticipation(userId, createCrewEpisodeDTO.getPersonId(),
				createCrewEpisodeDTO.getTitleId(), createCrewEpisodeDTO.getSeasonNumber(), crew));
	}

	@DeleteMapping("/episodes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removePersonEpisodeCrewParticipation(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewNotFoundException {

		crewService.removePersonEpisodeCrewParticipation(userId, id);
	}

	@GetMapping("/titles/{id}/all")
	public List<CrewDTO> findAllCrew(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toCrewDTOs(crewService.findAllCrew(id, localeId));

	}

	@GetMapping("/titles/{id}/internal")
	public List<InternalPersonCrewDTO> findAllinternalCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toInternalCrewDTOs(crewService.findAllCrew(id, localeId));

	}

	@GetMapping("/episodes/{id}/all")
	public List<CrewDTO> findEpisodeCrew(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toCrewEpisodeDTOs(crewService.findEpisodeCrew(id, localeId));

	}

}
