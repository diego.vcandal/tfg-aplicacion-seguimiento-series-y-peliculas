package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

public class EpisodeSummaryExternalDTO {

	private Long id;
	private short episode_number;
	private String air_date;
	private String still_path;
	private String name;
	private String overview;

	public Long getId() {
		return id;
	}

	public short getEpisode_number() {
		return episode_number;
	}

	public String getAir_date() {
		return air_date;
	}

	public String getStill_path() {
		return still_path;
	}

	public String getName() {
		return name;
	}

	public String getOverview() {
		return overview;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEpisode_number(short episode_number) {
		this.episode_number = episode_number;
	}

	public void setAir_date(String air_date) {
		this.air_date = air_date;
	}

	public void setStill_path(String still_path) {
		this.still_path = still_path;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

}
