package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class ErrorDTO {

	private String globalError;
	private List<FieldErrorDTO> fieldErrors;

	public ErrorDTO(String globalError) {
		this.globalError = globalError;
	}

	public ErrorDTO(List<FieldErrorDTO> fieldErrors) {

		this.fieldErrors = fieldErrors;

	}

	public String getGlobalError() {
		return globalError;
	}

	public void setGlobalError(String globalError) {
		this.globalError = globalError;
	}

	public List<FieldErrorDTO> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldErrorDTO> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}
