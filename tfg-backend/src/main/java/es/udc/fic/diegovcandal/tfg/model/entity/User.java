package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;

@Entity
public class User {

	private Long id;
	private String email;
	private String country;
	private String userName;
	private String password;
	private UserRole role;
	private String image;

	private FollowingList followingList;

	public User() {
	}

	public User(Long id, String email, String country, String userName, String password, String image) {
		this.id = id;
		this.email = email;
		this.country = country;
		this.userName = userName;
		this.password = password;
		this.image = image;
	}

	public User(String email, String country, String userName, String password, UserRole role, String image) {
		this.email = email;
		this.country = country;
		this.userName = userName;
		this.password = password;
		this.role = role;
		this.image = image;
	}

	public User(String email, String country, String userName, String password, String image) {
		this.email = email;
		this.country = country;
		this.userName = userName;
		this.password = password;
		this.image = image;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "userId", cascade = CascadeType.ALL, orphanRemoval = true)
	public FollowingList getFollowingList() {
		return followingList;
	}

	public void setFollowingList(FollowingList followingList) {
		this.followingList = followingList;
	}

}
