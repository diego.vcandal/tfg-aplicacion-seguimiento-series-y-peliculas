package es.udc.fic.diegovcandal.tfg.model.dao;

import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;

public interface TVShowDAO extends TitleBaseDAO<TVShow> {

}
