package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

public class JobCrewExternalDTO {

	private String job;
	private short episode_count;

	public String getJob() {
		return job;
	}

	public short getEpisode_count() {
		return episode_count;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setEpisode_count(short episode_count) {
		this.episode_count = episode_count;
	}

}
