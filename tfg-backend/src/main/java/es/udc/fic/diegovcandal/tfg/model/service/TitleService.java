package es.udc.fic.diegovcandal.tfg.model.service;

import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithSeasons;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.GenreDoesntExistException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface TitleService {

	TitleWithTraduction<TVShow> getTVShowDetails(Long userId, Long id, Long localeId)
			throws TitleNotFoundException, UserNotFoundException;

	TitleWithTraduction<Movie> getMovieDetails(Long userId, Long id, Long localeId)
			throws TitleNotFoundException, UserNotFoundException;

	TVShowWithSeasons getSeasons(Long userId, Long id, Long localeId) throws UserNotFoundException;

	SeasonWithTraduction getFullSeason(Long userId, Long id, byte seasonNumber, Long localeId)
			throws TitleNotFoundException, UserNotFoundException;

	TVShowWithSeasons getLastSeasonPreview(Long userId, Long id, Long localeId)
			throws TitleNotFoundException, UserNotFoundException;

	Long createMovie(Long userId, Movie movie) throws ExternalIdAlreadyLinkedException, UserNotFoundException,
			PermissionException, GenreDoesntExistException;

	void makeTitlePublic(Long userId, Long titleId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	void deleteTitle(Long userId, Long titleId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	void setTitleForDeletion(Long userId, Long titleId, boolean setForDeletion)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	void updateMovie(Long userId, Long movieId, Movie updatedMovie)
			throws UserNotFoundException, PermissionException, GenreDoesntExistException, TitleNotFoundException;

	void updateTVShow(Long userId, Long tvShowId, TVShow updatedTVShow)
			throws UserNotFoundException, PermissionException, GenreDoesntExistException, TitleNotFoundException;

	Long createTVShow(Long userId, TVShow tvShow) throws ExternalIdAlreadyLinkedException, UserNotFoundException,
			PermissionException, GenreDoesntExistException;

	Long titleExistsByExternalId(Long externalId);

	Movie getInternalMovieDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	TVShow getInternalTVShowDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	void changeSourceType(Long userId, Long titleId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	Long createSeason(Long userId, Long titleId, Season season)
			throws UserNotFoundException, PermissionException, TitleNotFoundException;

	Long createEpisode(Long userId, Long titleId, byte seasonNumber, Episode episode)
			throws UserNotFoundException, PermissionException, TitleNotFoundException;

	Long seasonExistsByExternalId(Long externalId);

	Long episodeExistsByExternalId(Long externalId);

	void updateSeason(Long userId, Long seasonId, Season updatedSeason)
			throws UserNotFoundException, PermissionException, TitleNotFoundException;

	void updateEpisode(Long userId, Long episodeId, Episode updatedEpisode)
			throws UserNotFoundException, PermissionException, TitleNotFoundException;

	void setSeasonForDeletion(Long userId, Long seasonId, boolean setForDeletion)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	void setEpisodeForDeletion(Long userId, Long episodeId, boolean setForDeletion)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	TVShowWithInternalEpisode getInternalEpisodeDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	TVShowWithInternalSeason getInternalSeasonDetails(Long id, Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException;

	Title getTitleById(Long id);

}
