package es.udc.fic.diegovcandal.tfg.model.utils;

public enum Visibility {
	PUBLIC,
	PRIVATE
}
