package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class CustomListDTO {

	private Long id;
	private Long userId;
	private String userName;
	private boolean visible;
	private Long lastUpdated;
	private String title;
	private String description;
	private int likeCount;
	private int titleCount;
	private boolean hasImage;
	private boolean liked;

	private List<CustomListTranslatedTitleDTO> titles;

	public CustomListDTO(Long id, Long userId, String userName, boolean visible, Long lastUpdated, String title,
			String description, int likeCount, int titleCount, boolean hasImage,
			List<CustomListTranslatedTitleDTO> titles, boolean liked) {
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.visible = visible;
		this.lastUpdated = lastUpdated;
		this.title = title;
		this.description = description;
		this.likeCount = likeCount;
		this.titleCount = titleCount;
		this.hasImage = hasImage;
		this.titles = titles;
		this.liked = liked;
	}

	public Long getId() {
		return id;
	}

	public Long getUserId() {
		return userId;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public int getTitleCount() {
		return titleCount;
	}

	public boolean isHasImage() {
		return hasImage;
	}

	public List<CustomListTranslatedTitleDTO> getTitles() {
		return titles;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public void setTitleCount(int titleCount) {
		this.titleCount = titleCount;
	}

	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}

	public void setTitles(List<CustomListTranslatedTitleDTO> titles) {
		this.titles = titles;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

}
