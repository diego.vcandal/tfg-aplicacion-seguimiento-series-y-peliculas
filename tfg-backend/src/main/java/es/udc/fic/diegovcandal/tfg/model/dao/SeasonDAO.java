package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;

public interface SeasonDAO extends PagingAndSortingRepository<Season, Long> {

	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction(s, st) "
			+ "FROM Season s LEFT JOIN SeasonTraduction st "
			+ "on s.id = st.idSeason AND "
			+ "st.idLocale = ?3 "
			+ "WHERE s.tvShow = ?1 AND s.seasonNumber = ?2")
	Optional<SeasonWithTraduction> findSeasonWithTraduction(TVShow tvShow, byte seasonNumber, Long localeId);
	
	@Query("SELECT s "
			+ "FROM Season s "
			+ "WHERE s.tvShow = ?1 AND s.seasonNumber = ?2")
	Optional<Season> findSeason(TVShow tvShow, byte seasonNumber);
	
	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction(s, st) "
			+ "FROM Season s LEFT JOIN SeasonTraduction st "
			+ "on s.id = st.idSeason AND "
			+ "st.idLocale = ?2 "
			+ "WHERE s.tvShow = ?1")
	List<SeasonWithTraduction> findAllSeasonsWithTraduction(TVShow tvShow, Long localeId); 
	
	@Query("SELECT s.image "
			+ "FROM Season s "
			+ "WHERE s.tvShow = ?1 AND s.seasonNumber = ?2")
	String findSeasonImage(TVShow tvShow, byte seasonNumber);
	
	Optional<Season> findByExternalId(Long externalId);
}
