package es.udc.fic.diegovcandal.tfg.model.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectLoginException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectPasswordException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private BCryptPasswordEncoder passwordEncoder;

	private UserDao userDao;

	private PermissionManager permissionManager;

	@Autowired
	public UserServiceImpl(BCryptPasswordEncoder passwordEncoder, UserDao userDao,
			PermissionManager permissionManager) {
		this.passwordEncoder = passwordEncoder;
		this.userDao = userDao;
		this.permissionManager = permissionManager;
	}

	@Override
	public void signUp(User user) throws UserAlreadyExistsException {

		if (userDao.existsByUserName(user.getUserName())) {
			throw new UserAlreadyExistsException(user.getUserName());
		}

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRole(UserRole.USER);
		user.setFollowingList(new FollowingList(user, Visibility.PUBLIC, LocalDateTime.now(), (short) 0, (short) 0));

		userDao.save(user);

	}

	@Override
	@Transactional(readOnly = true)
	public User loginFromUserName(String userName, String password) throws IncorrectLoginException {

		Optional<User> user = userDao.findByUserName(userName);

		if (!user.isPresent()) {
			throw new IncorrectLoginException(userName, password);
		}

		if (!passwordEncoder.matches(password, user.get().getPassword())) {
			throw new IncorrectLoginException(userName, password);
		}

		return user.get();

	}

	@Override
	@Transactional(readOnly = true)
	public User loginFromId(Long id) throws UserNotFoundException {
		return permissionManager.findUser(id);
	}

	@Override
	@Transactional(readOnly = true)
	public User findUser(Long id) throws UserNotFoundException {
		return permissionManager.findUser(id);
	}

	@Override
	public User updateProfile(User user, boolean imageChanged)
			throws UserNotFoundException, UserAlreadyExistsException {

		User foundUser = permissionManager.findUser(user.getId());

		if (!user.getUserName().equals(foundUser.getUserName()) && userDao.existsByUserName(user.getUserName())) {
			throw new UserAlreadyExistsException(user.getUserName());
		}

		foundUser.setUserName(user.getUserName());
		foundUser.setCountry(user.getCountry());
		foundUser.setEmail(user.getEmail());

		if (imageChanged) {
			foundUser.setImage(user.getImage());
		}

		return foundUser;
	}

	@Override
	public void deleteProfile(Long id, String password) throws UserNotFoundException, IncorrectPasswordException {

		User user = permissionManager.findUser(id);

		if (!passwordEncoder.matches(password, user.getPassword())) {
			throw new IncorrectPasswordException();
		} else {
			userDao.delete(user);
		}

	}

	@Override
	public void changePassword(Long id, String oldPassword, String newPassword)
			throws UserNotFoundException, IncorrectPasswordException {

		User user = permissionManager.findUser(id);

		if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
			throw new IncorrectPasswordException();
		} else {
			user.setPassword(passwordEncoder.encode(newPassword));
		}

	}

}
