package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import java.util.ArrayList;
import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;

public class TranslatedCustomList {

	private CustomList customList;
	private List<TranslatedCustomListTitle> titles;

	public TranslatedCustomList() {
		this.titles = new ArrayList<>();
	}

	public TranslatedCustomList(CustomList customList, List<TranslatedCustomListTitle> titles) {
		this.customList = customList;
		this.titles = titles;
	}

	public CustomList getCustomList() {
		return customList;
	}

	public List<TranslatedCustomListTitle> getTitles() {
		return titles;
	}

	public void setCustomList(CustomList customList) {
		this.customList = customList;
	}

	public void setTitles(List<TranslatedCustomListTitle> titles) {
		this.titles = titles;
	}

}
