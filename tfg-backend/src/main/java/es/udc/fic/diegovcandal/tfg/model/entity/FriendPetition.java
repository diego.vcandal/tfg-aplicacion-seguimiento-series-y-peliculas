package es.udc.fic.diegovcandal.tfg.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.FriendPetitionCompositeKey;

@Entity
@IdClass(FriendPetitionCompositeKey.class)
public class FriendPetition implements Serializable {

	private static final long serialVersionUID = 2280204860242431394L;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "originUser")
	private User originUser;

	@Id
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "destinationUser")
	private User destinationUser;

	private LocalDateTime date;

	public FriendPetition() {
	}

	public FriendPetition(User originUser, User destinationUser, LocalDateTime date) {
		super();
		this.originUser = originUser;
		this.destinationUser = destinationUser;
		this.date = date;
	}

	public User getOriginUser() {
		return originUser;
	}

	public void setOriginUser(User originUser) {
		this.originUser = originUser;
	}

	public User getDestinationUser() {
		return destinationUser;
	}

	public void setDestinationUser(User destinationUser) {
		this.destinationUser = destinationUser;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

}
