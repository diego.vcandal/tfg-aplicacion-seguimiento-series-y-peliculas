package es.udc.fic.diegovcandal.tfg.model.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;

public interface TitleDAO extends TitleBaseDAO<Title>, TitleDAOCustom, JpaSpecificationExecutor<TitleInfo> {

}
