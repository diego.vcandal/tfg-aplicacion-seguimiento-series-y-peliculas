package es.udc.fic.diegovcandal.tfg.model.service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CustomListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CustomListLikeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CustomListTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListLike;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedCustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedCustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ListElementsLimitExceededException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Service
@Transactional
public class CustomListServiceImpl implements CustomListService {

	@Autowired
	private CustomListDAO customListDAO;

	@Autowired
	private CustomListTitleDAO customListTitleDAO;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private CustomListLikeDAO customListLikeDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Value("${project.lists.limit}")
	private int listLimit;

	@Override
	@Transactional(readOnly = true)
	public TranslatedCustomList getCustomList(Long userId, Long listId, Long localeId)
			throws CustomListNotFoundException, PrivateResourceException, UserNotFoundException {

		Optional<CustomList> customList = customListDAO.findById(listId);
		User user = null;

		if (!customList.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (customList.get().getVisibility() == Visibility.PRIVATE) {

			if (userId == null) {
				throw new PrivateResourceException();
			}

			user = permissionManager.findUser(userId);

			if (user.getId() != customList.get().getUserId().getId()) {
				throw new PrivateResourceException();
			}

		}

		if (userId != null) {

			user = user == null ? permissionManager.findUser(userId) : user;

			Optional<CustomListLike> customListLike = customListLikeDAO.findByListIdAndUserId(customList.get(), user);

			if (customListLike.isPresent()) {
				customList.get().setLiked(true);
			}
		}

		List<TranslatedCustomListTitle> titles = customListTitleDAO.findCustomListTranslatedTitles(customList.get(),
				localeId);

		return new TranslatedCustomList(customList.get(), titles);

	}

	@Override
	@Transactional(readOnly = true)
	public List<CustomList> getCustomLists(Long userId, Long listUserId) throws UserNotFoundException {

		User listUser = permissionManager.findUser(listUserId);

		List<CustomList> lists = customListDAO.findByUserIdAndVisibility(listUser, Visibility.PUBLIC);

		if (userId != null) {
			User user = permissionManager.findUser(userId);

			for (CustomList customList : lists) {
				Optional<CustomListLike> customListLike = customListLikeDAO.findByListIdAndUserId(customList, user);

				if (customListLike.isPresent()) {
					customList.setLiked(true);
				}
			}

		}

		return lists;

	}

	@Override
	@Transactional(readOnly = true)
	public Block<CustomList> getCustomLists(Long userId, Long listUserId, int pageNumber, int size)
			throws UserNotFoundException {

		User listUser = permissionManager.findUser(listUserId);

		Page<CustomList> page = customListDAO.findByUserIdAndVisibilityOrderByIdDesc(listUser, Visibility.PUBLIC,
				PageRequest.of(pageNumber, size));

		if (userId != null) {
			User user = permissionManager.findUser(userId);

			for (CustomList customList : page.getContent()) {
				Optional<CustomListLike> customListLike = customListLikeDAO.findByListIdAndUserId(customList, user);

				if (customListLike.isPresent()) {
					customList.setLiked(true);
				}
			}

		}

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());

	}

	@Override
	@Transactional(readOnly = true)
	public Block<CustomList> getMyCustomLists(Long userId, int pageNumber, int size) throws UserNotFoundException {

		User user = permissionManager.findUser(userId);

		Page<CustomList> page = customListDAO.findByUserIdOrderByIdDesc(user, PageRequest.of(pageNumber, size));

		return new Block<>(page.getContent(), page.hasNext(), page.getTotalPages(), page.getTotalElements());

	}

	@Override
	@Transactional(readOnly = true)
	public List<CustomList> getMyCustomLists(Long userId) throws UserNotFoundException {

		User user = permissionManager.findUser(userId);

		return customListDAO.findByUserId(user);

	}

	@Override
	@Transactional(readOnly = true)
	public List<CustomList> getMyCustomListsFromTitle(Long userId, Long titleId) throws UserNotFoundException {

		User user = permissionManager.findUser(userId);

		List<CustomList> lists = customListDAO.findByUserId(user);

		for (CustomList customList : lists) {
			Optional<Title> title = titleDAO.findById(titleId);

			if (title.isPresent()) {
				customList.setHasTitle(customListTitleDAO.existsByListIdAndTitleId(customList, title.get()));
			}
		}

		return customListDAO.findByUserId(user);

	}

	@Override
	public Long createCustomList(Long userId, CustomList list) throws UserNotFoundException, TitleNotFoundException {

		User user = permissionManager.findUser(userId);

		list.setUserId(user);
		list.setLastUpdated(LocalDateTime.now());
		list.setLikeCount(0);
		list.setTitleCount(0);

		return customListDAO.save(list).getId();

	}

	@Override
	public Long updateElementsOrder(Long userId, Long listId, Set<CustomListTitle> newList)
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		updateElements(list.get(), newList);
		list.get().setTitleCount(list.get().getCustomListTitles().size());

		return customListDAO.save(list.get()).getId();

	}

	@Override
	public void updateCustomList(Long userId, Long listId, CustomList updatedList)
			throws UserNotFoundException, TitleNotFoundException, PermissionException, CustomListNotFoundException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		list.get().setLastUpdated(LocalDateTime.now());
		list.get().setVisibility(updatedList.getVisibility());
		list.get().setTitle(updatedList.getTitle());
		list.get().setDescription(updatedList.getDescription());

		String image = updatedList.getImage() != null && updatedList.getImage().trim().equals("") ? null
				: updatedList.getImage();
		list.get().setImage(updatedList.getImage() == null ? list.get().getImage() : image);

		customListDAO.save(list.get());

	}

	@Override
	public void deleteCustomList(Long userId, Long listId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		customListDAO.delete(list.get());

	}

	@Override
	public void changeListVisbility(Long userId, Long listId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		list.get().setVisibility(
				list.get().getVisibility() == Visibility.PUBLIC ? Visibility.PRIVATE : Visibility.PUBLIC);

		customListDAO.save(list.get());

	}

	@Override
	public void addLike(Long userId, Long listId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (list.get().getVisibility() == Visibility.PRIVATE) {
			throw new PermissionException();
		}

		if (user.getId() == list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		Optional<CustomListLike> customListLike = customListLikeDAO.findByListIdAndUserId(list.get(), user);

		if (customListLike.isPresent()) {
			return;
		}

		CustomListLike newCustomListLike = new CustomListLike(list.get(), user);

		list.get().setLikeCount(list.get().getLikeCount() + 1);

		customListLikeDAO.save(newCustomListLike);
		customListDAO.save(list.get());
	}

	@Override
	public void removeLike(Long userId, Long listId)
			throws CustomListNotFoundException, UserNotFoundException, PermissionException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		if (list.get().getVisibility() == Visibility.PRIVATE) {
			throw new PermissionException();
		}

		if (user.getId() == list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		Optional<CustomListLike> customListLike = customListLikeDAO.findByListIdAndUserId(list.get(), user);

		if (!customListLike.isPresent()) {
			return;
		}

		list.get().setLikeCount(list.get().getLikeCount() - 1);

		customListLikeDAO.delete(customListLike.get());
		customListDAO.save(list.get());
	}

	@Override
	public void addToList(Long userId, Long listId, Long titleId) throws PermissionException, TitleNotFoundException,
			UserNotFoundException, CustomListNotFoundException, ListElementsLimitExceededException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		if (!customListTitleDAO.existsByListIdAndTitleId(list.get(), title.get())) {

			if (list.get().getTitleCount() >= listLimit) {
				throw new ListElementsLimitExceededException();
			}

			list.get().getCustomListTitles()
					.add(new CustomListTitle(list.get(), title.get(), list.get().getTitleCount() + 1));
			list.get().setTitleCount(list.get().getTitleCount() + 1);
			list.get().setLastUpdated(LocalDateTime.now());

		}

		customListDAO.save(list.get());

	}

	@Override
	public void deleteFromList(Long userId, Long listId, Long titleId)
			throws PermissionException, TitleNotFoundException, UserNotFoundException, CustomListNotFoundException {

		User user = permissionManager.findUser(userId);

		Optional<CustomList> list = customListDAO.findById(listId);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(listId);
		}

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		if (user.getId() != list.get().getUserId().getId()) {
			throw new PermissionException();
		}

		Optional<CustomListTitle> customListTitle = customListTitleDAO.findByListIdAndTitleId(list.get(), title.get());

		if (customListTitle.isPresent()) {

			list.get().getCustomListTitles().remove(customListTitle.get());

			Set<CustomListTitle> newSet = checkElementsPosition(list.get().getCustomListTitles());

			list.get().getCustomListTitles().removeAll(list.get().getCustomListTitles());
			list.get().getCustomListTitles().addAll(newSet);

			list.get().setTitleCount(list.get().getTitleCount() - 1);
			list.get().setLastUpdated(LocalDateTime.now());

		}

		customListDAO.save(list.get());

	}

	private void updateElements(CustomList list, Set<CustomListTitle> updatedElements) throws TitleNotFoundException {

		list.getCustomListTitles().removeAll(list.getCustomListTitles());

		Set<CustomListTitle> newList = new HashSet<>();
		for (CustomListTitle customListTitle : updatedElements) {

			Optional<Title> title = titleDAO.findById(customListTitle.getTitleId().getId());

			if (!title.isPresent())
				throw new TitleNotFoundException(customListTitle.getTitleId().getId());

			customListTitle.setListId(list);
			customListTitle.setTitleId(title.get());

			newList.add(customListTitle);

		}

		list.getCustomListTitles().addAll(checkElementsPosition(newList));

	}

	private Set<CustomListTitle> checkElementsPosition(Set<CustomListTitle> updatedElements)
			throws TitleNotFoundException {

		List<CustomListTitle> list = updatedElements.stream().collect(Collectors.toList());
		Collections.sort(list, new CustomListElementComparator());

		int pos = 1;
		for (CustomListTitle customListTitle : list) {
			customListTitle.setPosition(pos++);
		}

		return list.stream().collect(Collectors.toSet());

	}

	static class CustomListElementComparator implements Comparator<CustomListTitle> {
		@Override
		public int compare(CustomListTitle clt1, CustomListTitle clt2) {

			int result = Integer.valueOf(clt1.getPosition()).compareTo(Integer.valueOf(clt2.getPosition()));

			if (result != 0 && clt1.getPosition() <= 0)
				return 1;

			if (result != 0 && clt2.getPosition() <= 0)
				return -1;

			return result == 0 ? clt1.getTitleId().getOriginalTitle().compareTo(clt2.getTitleId().getOriginalTitle())
					: result;
		}
	}

}
