package es.udc.fic.diegovcandal.tfg.model.restclient;

import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ListElementsLimitExceededException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

public interface TmdbAPIClientListService {

	Long addToList(Long userId, Long listId, Long titleId, TitleType titleType, String acceptLanguage)
			throws UserNotFoundException, IncorrectOperationException, CustomListNotFoundException, PermissionException,
			TitleNotFoundException, ExternalIdAlreadyLinkedException, ListElementsLimitExceededException;

}
