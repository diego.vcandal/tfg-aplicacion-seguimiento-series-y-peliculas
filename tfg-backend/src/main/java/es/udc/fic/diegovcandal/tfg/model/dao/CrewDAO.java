package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewType;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCrewSummary;

public interface CrewDAO extends PagingAndSortingRepository<Crew, Long> {

	Optional<Crew> findByPeopleIdAndTitleIdAndCrewTypeId(People peopleId, Title titleId, CrewType crewTypeId);

	List<Crew> findByTitleId(Title titleId);
	
	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCrewSummary("
			+ "t.id, t.year, t.originalTitle, tt.titleName, ct.id, c.episodeCount) "
			+ "FROM Crew c JOIN Title t " 
			+ "on c.titleId = t.id "
			+ "LEFT JOIN TitleTraduction tt "
			+ "on t.id = tt.idTitle AND " 
			+ "tt.idLocale = ?2 "
			+ "JOIN CrewType ct "
			+ "on c.crewTypeId=ct.id "
			+ "WHERE c.peopleId = ?1")
	List<PersonCrewSummary> findCrewHistory(People people, Long localeId);

}
