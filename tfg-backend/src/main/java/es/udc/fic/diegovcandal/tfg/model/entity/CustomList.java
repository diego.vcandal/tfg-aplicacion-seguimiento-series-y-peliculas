package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Entity
public class CustomList {

	private Long id;
	private User userId;
	private Visibility visibility;
	private LocalDateTime lastUpdated;
	private String title;
	private String description;
	private int likeCount;
	private int titleCount;
	private String image;

	@Transient
	private boolean liked;

	@Transient
	private boolean hasTitle;

	private Set<CustomListTitle> customListTitles;

	public CustomList() {
		this.customListTitles = new HashSet<>();
	}

	public CustomList(User userId, Visibility visibility, LocalDateTime lastUpdated, String title, String description,
			int likeCount, int titleCount, String image) {
		this.userId = userId;
		this.visibility = visibility;
		this.lastUpdated = lastUpdated;
		this.title = title;
		this.description = description;
		this.likeCount = likeCount;
		this.titleCount = titleCount;
		this.image = image;

		this.liked = false;
		this.hasTitle = false;
		this.customListTitles = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public User getUserId() {
		return userId;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public int getTitleCount() {
		return titleCount;
	}

	public String getImage() {
		return image;
	}

	@Transient
	public boolean isLiked() {
		return liked;
	}

	@OneToMany(mappedBy = "listId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CustomListTitle> getCustomListTitles() {
		return customListTitles;
	}

	@Transient
	public boolean hasTitle() {
		return hasTitle;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public void setTitleCount(int titleCount) {
		this.titleCount = titleCount;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Transient
	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public void setCustomListTitles(Set<CustomListTitle> customListTitles) {
		this.customListTitles = customListTitles;
	}

	@Transient
	public void setHasTitle(boolean hasTitle) {
		this.hasTitle = hasTitle;
	}

}
