package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;

@Entity
public class Season {

	private Long id;
	private Long externalId;
	private TVShow tvShow;
	private byte seasonNumber;
	private String originalTitle;
	private String originalDescription;
	private LocalDate firstAirDate;
	private LocalDate lastAirDate;
	private String image;
	private boolean setForDelete;
	private SourceType sourceType;

	private short totalDuration;
	private short episodeCount;

	private Set<Episode> episodes;
	private Set<SeasonTraduction> seasonTraductions;

	private Set<CrewSeason> crewSeasons;
	private Set<CastSeason> castSeasons;

	public Season() {
		this.episodes = new HashSet<>();
		this.seasonTraductions = new HashSet<>();
		this.crewSeasons = new HashSet<>();
		this.castSeasons = new HashSet<>();
		this.setForDelete = false;
	}

	public Season(Long externalId, TVShow tvShow, byte seasonNumber, String originalTitle, String originalDescription,
			LocalDate firstAirDate, LocalDate lastAirDate, String image, short totalDuration, short episodeCount,
			Set<Episode> episodes) {
		this.externalId = externalId;
		this.tvShow = tvShow;
		this.seasonNumber = seasonNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.firstAirDate = firstAirDate;
		this.lastAirDate = lastAirDate;
		this.image = image;
		this.totalDuration = totalDuration;
		this.episodeCount = episodeCount;
		this.episodes = episodes;

		this.episodes = new HashSet<>();
		this.seasonTraductions = new HashSet<>();
		this.crewSeasons = new HashSet<>();
		this.castSeasons = new HashSet<>();
		this.setForDelete = false;
	}

	public Season(Long id, Long externalId, TVShow tvShow, byte seasonNumber, String originalTitle,
			String originalDescription, LocalDate firstAirDate, LocalDate lastAirDate, String image) {
		this.id = id;
		this.externalId = externalId;
		this.tvShow = tvShow;
		this.seasonNumber = seasonNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.firstAirDate = firstAirDate;
		this.lastAirDate = lastAirDate;
		this.totalDuration = 0;
		this.episodeCount = 0;
		this.image = image;

		this.episodes = new HashSet<>();
		this.seasonTraductions = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "tvShowId")
	public TVShow getTvShow() {
		return tvShow;
	}

	@OneToMany(mappedBy = "season", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Episode> getEpisodes() {
		return episodes;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public LocalDate getFirstAirDate() {
		return firstAirDate;
	}

	public LocalDate getLastAirDate() {
		return lastAirDate;
	}

	public short getTotalDuration() {
		return totalDuration;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTvShow(TVShow tvShow) {
		this.tvShow = tvShow;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public void setFirstAirDate(LocalDate firstAirDate) {
		this.firstAirDate = firstAirDate;
	}

	public void setLastAirDate(LocalDate lastAirDate) {
		this.lastAirDate = lastAirDate;
	}

	public void setTotalDuration(short totalDuration) {
		this.totalDuration = totalDuration;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	public void setEpisodes(Set<Episode> episodes) {
		this.episodes = episodes;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	@OneToMany(mappedBy = "idSeason", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<SeasonTraduction> getSeasonTraductions() {
		return seasonTraductions;
	}

	public void setSeasonTraductions(Set<SeasonTraduction> seasonTraductions) {
		this.seasonTraductions = seasonTraductions;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	@Transient
	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	@OneToMany(mappedBy = "seasonId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CrewSeason> getCrewSeasons() {
		return crewSeasons;
	}

	public void setCrewSeasons(Set<CrewSeason> crewSeasons) {
		this.crewSeasons = crewSeasons;
	}

	@OneToMany(mappedBy = "seasonId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CastSeason> getCastSeasons() {
		return castSeasons;
	}

	public void setCastSeasons(Set<CastSeason> castSeasons) {
		this.castSeasons = castSeasons;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + episodeCount;
		result = prime * result + ((episodes == null) ? 0 : episodes.hashCode());
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		result = prime * result + ((firstAirDate == null) ? 0 : firstAirDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((lastAirDate == null) ? 0 : lastAirDate.hashCode());
		result = prime * result + ((originalDescription == null) ? 0 : originalDescription.hashCode());
		result = prime * result + ((originalTitle == null) ? 0 : originalTitle.hashCode());
		result = prime * result + seasonNumber;
		result = prime * result + ((seasonTraductions == null) ? 0 : seasonTraductions.hashCode());
		result = prime * result + (setForDelete ? 1231 : 1237);
		result = prime * result + ((sourceType == null) ? 0 : sourceType.hashCode());
		result = prime * result + totalDuration;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Season other = (Season) obj;
		if (episodeCount != other.episodeCount)
			return false;
		if (episodes == null) {
			if (other.episodes != null)
				return false;
		} else if (!episodes.equals(other.episodes))
			return false;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		if (firstAirDate == null) {
			if (other.firstAirDate != null)
				return false;
		} else if (!firstAirDate.equals(other.firstAirDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (lastAirDate == null) {
			if (other.lastAirDate != null)
				return false;
		} else if (!lastAirDate.equals(other.lastAirDate))
			return false;
		if (originalDescription == null) {
			if (other.originalDescription != null)
				return false;
		} else if (!originalDescription.equals(other.originalDescription))
			return false;
		if (originalTitle == null) {
			if (other.originalTitle != null)
				return false;
		} else if (!originalTitle.equals(other.originalTitle))
			return false;
		if (seasonNumber != other.seasonNumber)
			return false;
		if (seasonTraductions == null) {
			if (other.seasonTraductions != null)
				return false;
		} else if (!seasonTraductions.equals(other.seasonTraductions))
			return false;
		if (setForDelete != other.setForDelete)
			return false;
		if (sourceType != other.sourceType)
			return false;
		if (totalDuration != other.totalDuration)
			return false;
		return true;
	}

}
