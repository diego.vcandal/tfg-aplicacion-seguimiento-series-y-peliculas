package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class TitleInfoDTO {

	private Long id;
	private Long externalId;
	private String originalTitle;
	private String titleName;
	private String description;
	private Long titleType;
	private Long sourceType;
	private String originalDescription;
	private boolean image;
	private String imageExternalPath;
	private short year;

	private float avgRating;
	private int timesRated;

	public TitleInfoDTO(Long id, Long externalId, String originalTitle, String titleName, String description,
			Long titleType, Long sourceType, String originalDescription, boolean image, short year, float avgRating,
			int timesRated) {

		this.id = id;
		this.externalId = externalId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.description = description;
		this.titleType = titleType;
		this.sourceType = sourceType;
		this.originalDescription = originalDescription;
		this.image = image;
		this.year = year;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public TitleInfoDTO(Long id, Long externalId, String originalTitle, String titleName, String description,
			Long titleType, Long sourceType, String originalDescription, boolean image, String imageExternalPath,
			short year, float avgRating, int timesRated) {

		this.id = id;
		this.externalId = externalId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.description = description;
		this.titleType = titleType;
		this.sourceType = sourceType;
		this.originalDescription = originalDescription;
		this.image = image;
		this.imageExternalPath = imageExternalPath;
		this.year = year;
		this.avgRating = avgRating;
		this.timesRated = timesRated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTitleType() {
		return titleType;
	}

	public void setTitleType(Long titleType) {
		this.titleType = titleType;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public boolean getImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public String getImageExternalPath() {
		return imageExternalPath;
	}

	public void setImageExternalPath(String imageExternalPath) {
		this.imageExternalPath = imageExternalPath;
	}

	public short getYear() {
		return year;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public float getAvgRating() {
		return avgRating;
	}

	public int getTimesRated() {
		return timesRated;
	}

	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}

	public void setTimesRated(int timesRated) {
		this.timesRated = timesRated;
	}

}
