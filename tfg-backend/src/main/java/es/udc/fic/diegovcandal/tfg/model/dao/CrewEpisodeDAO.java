package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;

public interface CrewEpisodeDAO extends PagingAndSortingRepository<CrewEpisode, Long> {

	CrewEpisode findFirstByOrderByIdAsc();

	List<CrewEpisode> findByEpisodeId(Episode episodeId);

}
