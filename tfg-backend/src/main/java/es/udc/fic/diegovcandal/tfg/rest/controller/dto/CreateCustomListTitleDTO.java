package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.NotNull;

public class CreateCustomListTitleDTO {

	private Long id;
	private int position;

	public CreateCustomListTitleDTO(Long id, int position) {
		this.id = id;
		this.position = position;
	}

	@NotNull()
	public Long getId() {
		return id;
	}

	public int getPosition() {
		return position;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
