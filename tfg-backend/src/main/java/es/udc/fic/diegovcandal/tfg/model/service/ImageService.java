package es.udc.fic.diegovcandal.tfg.model.service;

import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface ImageService {

	String getHeaderImage(Long id) throws TitleNotFoundException;

	String getPortraitImage(Long id) throws TitleNotFoundException;

	String getSeasonImage(Long id, byte seasonNumber) throws TitleNotFoundException;

	String getEpisodeImage(Long id, byte seasonNumber, short episodeNumber) throws TitleNotFoundException;

	String getProfileImage(Long id) throws UserNotFoundException;

	String getPeopleImage(Long id) throws PeopleNotFoundException;

	String getCustomListImage(Long id) throws CustomListNotFoundException;

}
