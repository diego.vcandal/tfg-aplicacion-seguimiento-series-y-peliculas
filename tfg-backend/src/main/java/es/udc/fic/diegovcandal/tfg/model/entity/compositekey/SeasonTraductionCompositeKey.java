package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class SeasonTraductionCompositeKey implements Serializable {

	private static final long serialVersionUID = -6189832395539196591L;

	private Long idSeason;
	private Long idLocale;

	public SeasonTraductionCompositeKey() {
	}

	public SeasonTraductionCompositeKey(Long idSeason, Long idLocale) {
		this.idSeason = idSeason;
		this.idLocale = idLocale;
	}

	public Long getIdSeason() {
		return idSeason;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdSeason(Long idSeason) {
		this.idSeason = idSeason;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

}
