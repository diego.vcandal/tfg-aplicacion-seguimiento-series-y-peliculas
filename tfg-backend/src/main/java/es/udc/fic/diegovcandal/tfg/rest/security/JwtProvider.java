package es.udc.fic.diegovcandal.tfg.rest.security;

public interface JwtProvider {

	String generate(JwtUser info);

	JwtUser getInfo(String token);
}
