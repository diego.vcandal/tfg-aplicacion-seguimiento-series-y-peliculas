package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class PeopleWithTraductionDTO {

	private Long id;
	private Long externalId;
	private Long sourceType;
	private String originalName;
	private String name;
	private String biography;
	private String birthPlace;
	private Long birthday;
	private Long deathday;
	private boolean image;
	private boolean visibility;
	private boolean setForDelete;
	private String imageExternalPath;

	public PeopleWithTraductionDTO() {
	}

	public PeopleWithTraductionDTO(Long id, Long externalId, Long sourceType, String originalName, String name,
			String biography, String birthPlace, Long birthday, Long deathday, boolean image, boolean visibility,
			boolean setForDelete, String imageExternalPath) {
		this.id = id;
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.originalName = originalName;
		this.name = name;
		this.biography = biography;
		this.birthPlace = birthPlace;
		this.birthday = birthday;
		this.deathday = deathday;
		this.image = image;
		this.visibility = visibility;
		this.setForDelete = setForDelete;
		this.imageExternalPath = imageExternalPath;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Long getBirthday() {
		return birthday;
	}

	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}

	public Long getDeathday() {
		return deathday;
	}

	public void setDeathday(Long deathday) {
		this.deathday = deathday;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public boolean isVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public String getImageExternalPath() {
		return imageExternalPath;
	}

	public void setImageExternalPath(String imageExternalPath) {
		this.imageExternalPath = imageExternalPath;
	}

}
