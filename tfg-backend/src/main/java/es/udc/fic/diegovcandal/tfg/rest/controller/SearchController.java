package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CustomListConversor.toCustomListSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.PeopleConversor.externalToPersonSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.PeopleConversor.toPersonSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.TitleInfoConversor.externalToMovieInfoDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.TitleInfoConversor.externalToTVShowInfoDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.TitleInfoConversor.toTitleInfoDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.UserConversor.toUserSummaryDTOs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPIClientPeopleService;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPIClientSearchService;
import es.udc.fic.diegovcandal.tfg.model.service.SearchService;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.BlockDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CustomListSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TitleInfoDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.BlockExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.PeopleExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.exceptions.ApiNotEnabledException;
import es.udc.fic.diegovcandal.tfg.rest.util.Util;

@RestController
@RequestMapping("/search")
public class SearchController {

	@Autowired
	private SearchService searchService;

	@Autowired
	private TmdbAPIClientSearchService tmdbAPIClientSearchService;

	@Autowired
	private TmdbAPIClientPeopleService tmdbAPIClientPeopleService;

	@Value("${project.external.api.enabled}")
	private boolean apiEnabled;

	@GetMapping("/users")
	public BlockDTO<UserSummaryDTO> findUsers(@RequestParam(required = false) String userName,
			@RequestParam(defaultValue = "0") int page) {

		Block<User> userBlock = searchService.findUsers(userName != null ? userName.trim() : "", page, 10);

		return new BlockDTO<>(toUserSummaryDTOs(userBlock.getItems()), userBlock.getExistMoreItems(),
				userBlock.getTotalPages(), userBlock.getTotalElements());

	}

	@GetMapping("/titles")
	public BlockDTO<TitleInfoDTO> findTitles(@RequestAttribute(required = false) Long userId,
			@RequestParam(required = false) String titleName, @RequestParam(defaultValue = "0") int page,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		String title = titleName == null || titleName.trim().equals("") ? "" : titleName;

		Block<TitleInfo> titleBlock = searchService.findTitles(userId, title, localeId, page, 10);

		return new BlockDTO<>(toTitleInfoDTOs(titleBlock.getItems(), apiEnabled ? null : SourceType.INTERNAL),
				titleBlock.getExistMoreItems(), titleBlock.getTotalPages(), titleBlock.getTotalElements());

	}

	@GetMapping("/externalMovies")
	public BlockExternalDTO<TitleInfoDTO> findExternalMovies(@RequestParam(required = false) String titleName,
			@RequestParam(defaultValue = "1") int page,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws ApiNotEnabledException {

		Util.checkApiEnabled(apiEnabled);

		BlockExternalDTO<MovieExternalDTO> block = tmdbAPIClientSearchService.findExternalMovies(titleName, page,
				acceptLanguage);

		return new BlockExternalDTO<>(externalToMovieInfoDTOs(block.getResults()), block.getPage(),
				block.getTotal_results(), block.getTotal_pages());

	}

	@GetMapping("/externalTVShows")
	public BlockExternalDTO<TitleInfoDTO> findExternalTVShows(@RequestParam(required = false) String titleName,
			@RequestParam(defaultValue = "1") int page,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws ApiNotEnabledException {

		Util.checkApiEnabled(apiEnabled);

		BlockExternalDTO<TVShowExternalDTO> block = tmdbAPIClientSearchService.findExternalTVShows(titleName, page,
				acceptLanguage);

		return new BlockExternalDTO<>(externalToTVShowInfoDTOs(block.getResults()), block.getPage(),
				block.getTotal_results(), block.getTotal_pages());

	}

	@GetMapping("/movies/last")
	public BlockDTO<TitleInfoDTO> findLastMovies(
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		Block<TitleInfo> titleBlock = searchService.findLastTitles(localeId, TitleType.MOVIE);

		return new BlockDTO<>(toTitleInfoDTOs(titleBlock.getItems(), apiEnabled ? null : SourceType.INTERNAL),
				titleBlock.getExistMoreItems(), titleBlock.getTotalPages(), titleBlock.getTotalElements());

	}

	@GetMapping("/tvshows/last")
	public BlockDTO<TitleInfoDTO> findLastTVShows(
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		Block<TitleInfo> titleBlock = searchService.findLastTitles(localeId, TitleType.TV_SHOW);

		return new BlockDTO<>(toTitleInfoDTOs(titleBlock.getItems(), apiEnabled ? null : SourceType.INTERNAL),
				titleBlock.getExistMoreItems(), titleBlock.getTotalPages(), titleBlock.getTotalElements());

	}

	@GetMapping("/people")
	public BlockDTO<PersonSummaryDTO> findPeople(@RequestAttribute(required = false) Long userId,
			@RequestParam(required = false) String name, @RequestParam(defaultValue = "0") int page)
			throws UserNotFoundException {

		String originalName = name == null || name.trim().equals("") ? "" : name;

		Block<People> titleBlock = searchService.findPeople(userId, originalName, page, 10);

		return new BlockDTO<>(toPersonSummaryDTOs(titleBlock.getItems()), titleBlock.getExistMoreItems(),
				titleBlock.getTotalPages(), titleBlock.getTotalElements());

	}

	@GetMapping("/externalPeople")
	public BlockExternalDTO<PersonSummaryDTO> findExternalPeople(@RequestAttribute(required = false) Long userId,
			@RequestParam(required = false) String name, @RequestParam(defaultValue = "0") int page)
			throws UserNotFoundException, ApiNotEnabledException {

		Util.checkApiEnabled(apiEnabled);

		BlockExternalDTO<PeopleExternalDTO> block = tmdbAPIClientPeopleService.findExternalPersons(name, page,
				LocaleType.DEFAULT_LOCALE.toString());

		return new BlockExternalDTO<>(externalToPersonSummaryDTOs(block.getResults()), block.getPage(),
				block.getTotal_results(), block.getTotal_pages());

	}

	@GetMapping("/merged-titles")
	public BlockDTO<TitleInfoDTO> findMergeTitle(@RequestParam(required = false) String titleName,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		String title = titleName == null || titleName.trim().equals("") ? "" : titleName;

		if (apiEnabled)
			return tmdbAPIClientSearchService.mergeTitleSearch(title, acceptLanguage,
					searchService.findTitles(null, title, localeId, 0, 10).getItems());
		else
			return findTitles(null, titleName, 0, acceptLanguage);

	}

	@GetMapping("/lists")
	public BlockDTO<CustomListSummaryDTO> getCustomLists(@RequestParam(required = false) String title,
			@RequestParam(defaultValue = "0") int page) {

		Block<CustomList> listBlock = searchService.findCustomLists(title != null ? title.trim() : "", page, 10);

		return new BlockDTO<>(toCustomListSummaryDTOs(listBlock.getItems()), listBlock.getExistMoreItems(),
				listBlock.getTotalPages(), listBlock.getTotalElements());

	}
}
