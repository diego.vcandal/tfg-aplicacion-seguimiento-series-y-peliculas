package es.udc.fic.diegovcandal.tfg.rest.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewType;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCrewSummary;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCrewEpisodeDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreatePersonCrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateSeasonCrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.InternalPersonCrewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.PersonCrewSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.CrewExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalPersonCrewSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.JobCrewExternalDTO;

public class CrewConversor {

	public static final Crew toCrew(CreatePersonCrewDTO createPersonCrewDTO) {

		CrewType crewType = new CrewType();
		crewType.setId(createPersonCrewDTO.getCrewTypeId());

		Crew crew = new Crew(null, null, crewType, (short) 0);

		if (createPersonCrewDTO.getSeasons() != null && createPersonCrewDTO.getSeasons().size() > 0) {

			Set<CrewSeason> seasons = new HashSet<>();

			for (CreateSeasonCrewDTO createSeasonCrewDTO : createPersonCrewDTO.getSeasons()) {

				if (createSeasonCrewDTO.getEpisodes().size() >= 0) {

					Season season = new Season();
					season.setSeasonNumber(createSeasonCrewDTO.getSeasonNumber());
					CrewSeason crewSeason = new CrewSeason(crew, season, (short) 0);

					Set<CrewEpisode> episodes = new HashSet<>();

					for (Long episodeNumber : createSeasonCrewDTO.getEpisodes()) {

						Episode episode = new Episode();
						episode.setEpisodeNumber(episodeNumber.shortValue());

						episodes.add(new CrewEpisode(crewSeason, episode, crewType));
					}

					crewSeason.setCrewEpisodes(episodes);
					seasons.add(crewSeason);
				}

			}

			crew.setCrewSeasons(seasons);

		}

		return crew;
	}

	public static final CrewEpisode toEpisodeCrew(CreateCrewEpisodeDTO createCrewEpisodeDTO) {

		Episode episode = new Episode();
		episode.setEpisodeNumber(createCrewEpisodeDTO.getEpisodeNumber());

		CrewType crewType = new CrewType();
		crewType.setId(createCrewEpisodeDTO.getCrewTypeId());

		return new CrewEpisode(null, episode, crewType);
	}

	public static final List<CrewDTO> toCrewDTOs(List<Crew> crews) {
		return crews.stream().map(p -> toCrewDTO(p)).collect(Collectors.toList());
	}

	public static final List<CrewDTO> toCrewEpisodeDTOs(List<CrewEpisode> crews) {
		return crews.stream().map(p -> toCrewDTO(p)).collect(Collectors.toList());
	}

	public static final List<CrewDTO> toCrewEpisodeDTOsfromExternal(List<CrewExternalDTO> crews) {
		return crews.stream().map(p -> toCrewDTO(p)).collect(Collectors.toList());
	}

	public static final List<InternalPersonCrewDTO> toInternalCrewDTOs(List<Crew> crews) {
		return crews.stream().map(p -> toInternalCrewDTO(p)).collect(Collectors.toList());
	}

	public static final List<PersonCrewSummaryDTO> toPersonCrewSummaryDTOs(List<PersonCrewSummary> crews) {
		return crews.stream().map(p -> toPersonCrewSummaryDTO(p)).collect(Collectors.toList());
	}

	public static final List<PersonCrewSummaryDTO> toPersonCrewSummaryDTOFromExternals(
			List<ExternalPersonCrewSummaryDTO> crews) {
		return crews.stream().map(p -> toPersonCrewSummaryDTO(p)).collect(Collectors.toList());
	}

	private static final CrewDTO toCrewDTO(Crew crew) {

		boolean hasImage = crew.getPeopleId().getImage() != null;

		return new CrewDTO(crew.getId(), crew.getPeopleId().getId(), crew.getPeopleId().getExternalId(),
				crew.getPeopleId().getOriginalName(), crew.getCrewTypeId().getId(), hasImage,
				crew.getPeopleId().getExternalImage(), crew.getPeopleId().getSourceType().getId(),
				crew.getEpisodeCount());
	}

	private static final CrewDTO toCrewDTO(CrewEpisode crew) {

		People people = crew.getCrewSeasonId().getCrewId().getPeopleId();
		boolean hasImage = people.getImage() != null;

		return new CrewDTO(crew.getId(), people.getId(), people.getExternalId(), people.getOriginalName(),
				crew.getCrewTypeId().getId(), hasImage, people.getExternalImage(), people.getSourceType().getId(),
				(short) -1);
	}

	private static final CrewDTO toCrewDTO(CrewExternalDTO crew) {

		String job = crew.getJob();
		short episodes = -1;

		if (job == null || job.equals("")) {
			JobCrewExternalDTO externalDTO = crew.getJobs().stream().findFirst().get();
			job = externalDTO.getJob();
			episodes = externalDTO.getEpisode_count();
		}

		return new CrewDTO(-1L, crew.getId(), crew.getId(),
				crew.getName() != null ? crew.getName() : crew.getOriginal_name(), false, crew.getProfile_path(),
				SourceType.EXTERNAL_TMDB.getId(), crew.getDepartment(), job, episodes);
	}

	private static final InternalPersonCrewDTO toInternalCrewDTO(Crew crew) {

		List<CreateSeasonCrewDTO> seasons = new ArrayList<>();
		for (CrewSeason crewSeason : crew.getCrewSeasons()) {
			List<Long> episodes = new ArrayList<>();
			for (CrewEpisode crewEpisode : crewSeason.getCrewEpisodes()) {
				episodes.add((long) crewEpisode.getEpisodeId().getEpisodeNumber());
			}
			seasons.add(new CreateSeasonCrewDTO(crewSeason.getSeasonId().getSeasonNumber(), episodes));

		}

		boolean hasImage = crew.getPeopleId().getImage() != null;

		return new InternalPersonCrewDTO(crew.getId(), crew.getTitleId().getId(), crew.getPeopleId().getId(),
				crew.getPeopleId().getExternalId(), crew.getPeopleId().getOriginalName(), crew.getCrewTypeId().getId(),
				hasImage, crew.getPeopleId().getExternalImage(), crew.getPeopleId().getSourceType().getId(), seasons);

	}

	private static final PersonCrewSummaryDTO toPersonCrewSummaryDTO(PersonCrewSummary crew) {
		return new PersonCrewSummaryDTO(crew.getTitleId(), crew.getOriginalTitle(), crew.getTitleName(), crew.getYear(),
				crew.getCrewTypeId(), crew.getEpisodeCount());
	}

	private static final PersonCrewSummaryDTO toPersonCrewSummaryDTO(ExternalPersonCrewSummaryDTO crew) {

		if (crew.getMedia_type().equals("movie")) {

			short year = crew.getRelease_date() == null || crew.getRelease_date().equals("") ? -1
					: Short.parseShort(crew.getRelease_date().substring(0, 4));

			return new PersonCrewSummaryDTO(crew.getId(), crew.getOriginal_title(), crew.getTitle(), year,
					crew.getDepartment(), (short) 0);
		} else {

			short year = crew.getFirst_air_date() == null || crew.getFirst_air_date().equals("") ? -1
					: Short.parseShort(crew.getFirst_air_date().substring(0, 4));

			return new PersonCrewSummaryDTO(crew.getId(), crew.getOriginal_name(), crew.getName(), year,
					crew.getDepartment(), crew.getEpisode_count());
		}
	}

}
