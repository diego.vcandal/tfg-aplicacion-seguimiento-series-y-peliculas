package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;

public class TitleWithTraduction<T extends Title> {

	private T title;
	private TitleTraduction titleTraduction;

	public TitleWithTraduction(T title, TitleTraduction titleTraduction) {
		this.title = title;
		this.titleTraduction = titleTraduction;
	}

	public T getTitle() {
		return title;
	}

	public TitleTraduction getTitleTraduction() {
		return titleTraduction;
	}

	public void setTitle(T title) {
		this.title = title;
	}

	public void setTitleTraduction(TitleTraduction titleTraduction) {
		this.titleTraduction = titleTraduction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((titleTraduction == null) ? 0 : titleTraduction.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitleWithTraduction<T> other = (TitleWithTraduction<T>) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (titleTraduction == null) {
			if (other.titleTraduction != null)
				return false;
		} else if (!titleTraduction.equals(other.titleTraduction))
			return false;
		return true;
	}

}
