package es.udc.fic.diegovcandal.tfg.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CustomListLike {

	private Long id;
	private CustomList listId;
	private User userId;

	public CustomListLike() {
	}

	public CustomListLike(CustomList listId, User userId) {
		this.listId = listId;
		this.userId = userId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "listId")
	public CustomList getListId() {
		return listId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public User getUserId() {
		return userId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setListId(CustomList listId) {
		this.listId = listId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

}
