package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class TVShow extends Title {

	private short totalDuration;
	private short episodeCount;
	private byte seasonNumber;
	private LocalDate firstAirDate;
	private LocalDate lastAirDate;

	private Set<Season> seasons = new HashSet<Season>();

	public TVShow() {

	}

	public TVShow(Title title, short totalDuration, short episodeCount, byte seasonNumber, LocalDate firstAirDate,
			LocalDate lastAirDate) {

		super(title.getExternalId(), title.getSourceType(), TitleType.TV_SHOW, title.getOriginalTitle(),
				title.getCompanyName(), title.getOriginalDescription(), title.getYear());

		this.totalDuration = totalDuration;
		this.episodeCount = episodeCount;
		this.seasonNumber = seasonNumber;
		this.firstAirDate = firstAirDate;
		this.lastAirDate = lastAirDate;
	}

	@OneToMany(mappedBy = "tvShow", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(Set<Season> seasons) {
		this.seasons = seasons;
	}

	public short getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(short totalDuration) {
		this.totalDuration = totalDuration;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public LocalDate getFirstAirDate() {
		return firstAirDate;
	}

	public void setFirstAirDate(LocalDate firstAirDate) {
		this.firstAirDate = firstAirDate;
	}

	public LocalDate getLastAirDate() {
		return lastAirDate;
	}

	public void setLastAirDate(LocalDate lastAirDate) {
		this.lastAirDate = lastAirDate;
	}

}
