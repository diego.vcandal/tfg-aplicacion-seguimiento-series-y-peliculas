package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TitleTraductionDTO {

	private Long idLocale;
	private String titleName;
	private String description;

	public TitleTraductionDTO(Long idLocale, String titleName, String description) {
		this.idLocale = idLocale;
		this.titleName = titleName;
		this.description = description;
	}

	@NotNull()
	@Min(0)
	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	@NotNull()
	@Size(min = 1, max = 50)
	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	@NotNull()
	@Size(min = 0, max = 500)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
