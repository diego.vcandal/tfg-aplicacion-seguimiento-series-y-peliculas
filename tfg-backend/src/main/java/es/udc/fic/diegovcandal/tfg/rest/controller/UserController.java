package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CustomListConversor.toCustomListSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.FollowingListConversor.toFollowingListDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.PetitionConversor.toPetitionsDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.UserConversor.toAuthenticatedUserDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.UserConversor.toFriendsSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.UserConversor.toUser;
import static es.udc.fic.diegovcandal.tfg.rest.util.UserConversor.toUserDTO;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.FriendPetition;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.UserFriendOf;
import es.udc.fic.diegovcandal.tfg.model.exceptions.AlreadyFriendsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionAlreadySentException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionNotFound;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ImageNotValidException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectLoginException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectPasswordException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.InstanceNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.CustomListService;
import es.udc.fic.diegovcandal.tfg.model.service.FollowingListService;
import es.udc.fic.diegovcandal.tfg.model.service.FriendService;
import es.udc.fic.diegovcandal.tfg.model.service.ImageService;
import es.udc.fic.diegovcandal.tfg.model.service.UserService;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.AuthenticatedUserDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.BlockDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ChangePasswordParamsDto;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CustomListSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.DeleteProfileParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FollowingListDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FriendOfParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FriendPetitionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FriendRequestParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.LoginParamsDto;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.RelationshipInfoDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserCreationParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.UserSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.security.JwtProvider;
import es.udc.fic.diegovcandal.tfg.rest.security.JwtUser;
import es.udc.fic.diegovcandal.tfg.rest.util.Util;

@RestController
@RequestMapping("/users")
public class UserController {

	private static final String USER_ALREADY_EXISTS_EXCEPTION_ID = "exceptions.UserAlreadyExistsException";
	private static final String INCORRECT_LOGIN_EXCEPTION_ID = "exceptions.IncorrectLoginException";

	private static final String IMAGE_NOT_VALID_EXCEPTION_ID = "exceptions.ImageNotValidException";
	private static final String INCORRECT_PASSWORD_EXCEPTION_ID = "exceptions.IncorrectPasswordException";

	private static final String ALREADY_FRIENDS_EXCEPTION_ID = "exceptions.AlreadyFriendsException";
	private static final String FRIEND_PETITION_ALREADY_SENT_EXCEPTION_ID = "exceptions.FriendPetitionAlreadySentException";
	private static final String FRIEND_PETITION_NOT_FOUND_EXCEPTION_ID = "exceptions.FriendPetitionNotFound";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private UserService userService;

	@Autowired
	private FriendService friendService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private FollowingListService followingListService;

	@Autowired
	private CustomListService customListService;

	@Value("${project.external.api.enabled}")
	private boolean apiEnabled;

	@ExceptionHandler(UserAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleUserAlreadyExistsException(UserAlreadyExistsException exception, Locale locale) {

		String nameMessage = messageSource.getMessage(exception.getUserName(), null, exception.getUserName(), locale);
		String errorMessage = messageSource.getMessage(USER_ALREADY_EXISTS_EXCEPTION_ID, new Object[] { nameMessage },
				USER_ALREADY_EXISTS_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(IncorrectLoginException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleIncorrectLoginException(IncorrectLoginException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(INCORRECT_LOGIN_EXCEPTION_ID, null, INCORRECT_LOGIN_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(ImageNotValidException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleImageNotValidException(ImageNotValidException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(IMAGE_NOT_VALID_EXCEPTION_ID, null, IMAGE_NOT_VALID_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(IncorrectPasswordException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleIncorrectPasswordException(IncorrectPasswordException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(INCORRECT_PASSWORD_EXCEPTION_ID, null,
				INCORRECT_PASSWORD_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(FriendPetitionAlreadySentException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleFriendPetitionAlreadySentException(FriendPetitionAlreadySentException exception,
			Locale locale) {

		String errorMessage = messageSource.getMessage(FRIEND_PETITION_ALREADY_SENT_EXCEPTION_ID, null,
				FRIEND_PETITION_ALREADY_SENT_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(FriendPetitionNotFound.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleFriendPetitionNotFound(FriendPetitionNotFound exception, Locale locale) {

		String errorMessage = messageSource.getMessage(FRIEND_PETITION_NOT_FOUND_EXCEPTION_ID, null,
				FRIEND_PETITION_NOT_FOUND_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(AlreadyFriendsException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleAlreadyFriendsException(AlreadyFriendsException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(ALREADY_FRIENDS_EXCEPTION_ID, null, ALREADY_FRIENDS_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@PostMapping("/signUp")
	public ResponseEntity<AuthenticatedUserDTO> signUp(
			@Validated({ UserDTO.AllValidations.class }) @RequestBody UserCreationParamsDTO userDTO)
			throws UserAlreadyExistsException {

		User user = toUser(userDTO);

		userService.signUp(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId())
				.toUri();

		return ResponseEntity.created(location)
				.body(toAuthenticatedUserDTO(generateServiceToken(user), user, new ArrayList<FriendPetitionDTO>()));

	}

	@PostMapping("/login")
	public AuthenticatedUserDTO login(@Validated @RequestBody LoginParamsDto params)
			throws IncorrectLoginException, UserNotFoundException {

		User user = userService.loginFromUserName(params.getUserName(), params.getPassword());
		List<FriendPetition> petitions = friendService.getSentFriendPetitions(user.getId());

		return toAuthenticatedUserDTO(generateServiceToken(user), user, toPetitionsDTOs(petitions));

	}

	@PostMapping("/loginFromServiceToken")
	public AuthenticatedUserDTO loginFromServiceToken(@RequestAttribute Long userId,
			@RequestAttribute String serviceToken) throws UserNotFoundException {

		User user = userService.loginFromId(userId);
		List<FriendPetition> petitions = friendService.getSentFriendPetitions(user.getId());

		return toAuthenticatedUserDTO(serviceToken, user, toPetitionsDTOs(petitions));

	}

	@GetMapping("/{id}")
	public UserDTO findUser(@RequestAttribute(required = false) Long userId, @PathVariable Long id)
			throws UserNotFoundException {

		RelationshipInfoDTO relationshipInfoDTO = new RelationshipInfoDTO(false, false, false);

		if (userId != null) {

			relationshipInfoDTO.setAreFriends(friendService.areFriends(id, userId));
			relationshipInfoDTO.setSentPetition(friendService.hasSentPetition(userId, id));
			relationshipInfoDTO.setReceivedPetition(friendService.hasSentPetition(id, userId));

		}

		return toUserDTO(userService.findUser(id), relationshipInfoDTO);

	}

	@GetMapping("/{id}/image")
	public void findUserProfileImage(@PathVariable Long id, HttpServletResponse response) throws UserNotFoundException {

		String image = imageService.getProfileImage(id);

		Util.sendImage(response, image);

	}

	@PutMapping("/{id}")
	public UserDTO updateProfile(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated({ UserDTO.UpdateValidations.class }) @RequestBody UserCreationParamsDTO userDTO)
			throws UserNotFoundException, PermissionException, UserAlreadyExistsException, ImageNotValidException {

		if (!id.equals(userId) || !userId.equals(userDTO.getId())) {
			throw new PermissionException();
		}

		if (userDTO.isImageChanged() && userDTO.getImage() != null
				&& (!userDTO.getImage().startsWith("data:image/") || userDTO.getImage().startsWith("data:image/gif"))) {
			throw new ImageNotValidException();
		}

		return toUserDTO(userService.updateProfile(toUser(userDTO), userDTO.isImageChanged()), null);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProfile(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody DeleteProfileParamsDTO params)
			throws UserNotFoundException, PermissionException, IncorrectPasswordException {

		if (!id.equals(userId)) {
			throw new PermissionException();
		}

		userService.deleteProfile(userId, params.getPassword());

	}

	@PostMapping("/{id}/changePassword")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changePassword(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody ChangePasswordParamsDto params)
			throws PermissionException, UserNotFoundException, IncorrectPasswordException {

		if (!id.equals(userId)) {
			throw new PermissionException();
		}

		userService.changePassword(id, params.getOldPassword(), params.getNewPassword());

	}

	@PostMapping("/{id}/send-friend-request")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void sendFriendRequest(@RequestAttribute Long userId, @PathVariable Long id)
			throws IncorrectOperationException, UserNotFoundException, FriendPetitionNotFound,
			FriendPetitionAlreadySentException, AlreadyFriendsException {

		if (id.equals(userId)) {
			throw new IncorrectOperationException();
		}

		friendService.sendFriendRequest(userId, id);

	}

	@PostMapping("/{id}/accept-friend-request")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void acceptFriendRequest(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody FriendRequestParamsDTO friendRequestParamsDTO)
			throws IncorrectOperationException, UserNotFoundException, FriendPetitionNotFound,
			FriendPetitionAlreadySentException, AlreadyFriendsException, PermissionException {

		if (id.equals(friendRequestParamsDTO.getOrigin())) {
			throw new IncorrectOperationException();
		}

		if (!id.equals(userId) || !userId.equals(friendRequestParamsDTO.getDestination())) {
			throw new PermissionException();
		}

		friendService.acceptFriendRequest(friendRequestParamsDTO.getOrigin(), friendRequestParamsDTO.getDestination());

	}

	@PostMapping("/{id}/reject-friend-request")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void rejectFriendRequest(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody FriendRequestParamsDTO friendRequestParamsDTO)
			throws IncorrectOperationException, UserNotFoundException, FriendPetitionNotFound,
			FriendPetitionAlreadySentException, AlreadyFriendsException, PermissionException {

		if (id.equals(friendRequestParamsDTO.getOrigin())) {
			throw new IncorrectOperationException();
		}

		if (!id.equals(userId) || !userId.equals(friendRequestParamsDTO.getDestination())) {
			throw new PermissionException();
		}

		friendService.rejectFriendRequest(friendRequestParamsDTO.getOrigin(), friendRequestParamsDTO.getDestination());

	}

	@GetMapping("/{id}/receivedRequests")
	public List<FriendPetitionDTO> getReceivedRequests(@RequestAttribute Long userId, @PathVariable Long id)
			throws UserNotFoundException, PermissionException {

		if (!id.equals(userId)) {
			throw new PermissionException();
		}

		return toPetitionsDTOs(friendService.getReceivedFriendPetitions(id));

	}

	@GetMapping("/{id}/getAllFriends")
	public BlockDTO<UserSummaryDTO> getAllFriends(@PathVariable Long id)
			throws UserNotFoundException, PermissionException {

		Block<UserFriendOf> userBlock = friendService.getFriends(id, 0, Integer.MAX_VALUE);

		return new BlockDTO<>(toFriendsSummaryDTOs(id, userBlock.getItems()), userBlock.getExistMoreItems(),
				userBlock.getTotalPages(), userBlock.getTotalElements());

	}

	@GetMapping("/{id}/getSummaryFriends")
	public BlockDTO<UserSummaryDTO> getSummaryFriends(@PathVariable Long id)
			throws UserNotFoundException, PermissionException {

		Block<UserFriendOf> userBlock = friendService.getFriends(id, 0, 5);

		return new BlockDTO<>(toFriendsSummaryDTOs(id, userBlock.getItems()), userBlock.getExistMoreItems(),
				userBlock.getTotalPages(), userBlock.getTotalElements());

	}

	@DeleteMapping("/{id}/deleteFriend")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteFriend(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody FriendOfParamsDTO friendOfParamsDTO)
			throws InstanceNotFoundException, PermissionException {

		if (!id.equals(userId)) {
			throw new PermissionException();
		}

		friendService.deleteFriend(friendOfParamsDTO.getUser(), friendOfParamsDTO.getFriend());

	}

	@GetMapping("/my-following-list")
	public FollowingListDTO getPersonalList(@RequestAttribute Long userId,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toFollowingListDTO(followingListService.getPersonalList(userId, localeId), apiEnabled);
	}

	@GetMapping("/{id}/following-list")
	public FollowingListDTO getPublicList(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PrivateResourceException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		if (userId != null && userId == id)
			return toFollowingListDTO(followingListService.getPersonalList(userId, localeId), apiEnabled);

		return toFollowingListDTO(followingListService.getPublicList(id, localeId), apiEnabled);
	}

	@PostMapping("/change-list-visibility")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changeListVisbility(@RequestAttribute Long userId) throws UserNotFoundException {
		followingListService.changeListVisbility(userId);
	}

	@GetMapping("/{id}/last-lists")
	public BlockDTO<CustomListSummaryDTO> getCustomLists(@RequestAttribute(required = false) Long userId,
			@PathVariable Long id) throws UserNotFoundException, PrivateResourceException {

		Block<CustomList> lists = null;

		if (userId != null && userId == id)
			lists = customListService.getMyCustomLists(userId, 0, 5);
		else
			lists = customListService.getCustomLists(userId, id, 0, 5);

		return new BlockDTO<CustomListSummaryDTO>(toCustomListSummaryDTOs(lists.getItems()), lists.getExistMoreItems(),
				lists.getTotalPages(), lists.getTotalElements());

	}

	@GetMapping("/{id}/all-lists")
	public List<CustomListSummaryDTO> getAllCustomLists(@RequestAttribute(required = false) Long userId,
			@PathVariable Long id) throws UserNotFoundException, PrivateResourceException {

		if (userId != null && userId == id)
			return toCustomListSummaryDTOs(customListService.getMyCustomLists(userId));

		return toCustomListSummaryDTOs(customListService.getCustomLists(userId, id));

	}

	private String generateServiceToken(User user) {

		return jwtProvider.generate(new JwtUser(user.getId(), user.getUserName(), user.getRole().toString()));

	}

}
