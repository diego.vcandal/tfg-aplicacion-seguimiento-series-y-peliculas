package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

public class ExternalCastRoleDTO {

	private String character;
	private short episode_count;

	public String getCharacter() {
		return character;
	}

	public short getEpisode_count() {
		return episode_count;
	}

	public void setCharacter(String character) {
		this.character = character;
	}

	public void setEpisode_count(short episode_count) {
		this.episode_count = episode_count;
	}

}
