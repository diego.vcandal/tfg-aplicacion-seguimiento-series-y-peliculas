package es.udc.fic.diegovcandal.tfg.model.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Title {

	private Long id;
	private Long externalId;
	private SourceType sourceType;
	private TitleType titleType;
	private String originalTitle;
	private String originalDescription;
	private String companyName;
	private String externalImage;
	private short year;
	private Visibility visibility;
	private boolean setForDelete;
	private User addedByUser;
	private LocalDateTime addedDate;
	private float avgRating;
	private int timesRated;

	private Set<TitleTraduction> titleTraductions;
	private Set<TitleImage> titleImages;
	private Set<Genre> genres;

	private Set<Crew> crews;
	private Set<Cast> casts;

	public Title() {
		this.titleTraductions = new HashSet<>();
		this.titleImages = new HashSet<>();
		this.genres = new HashSet<>();
		this.addedDate = LocalDateTime.now();
		this.crews = new HashSet<>();
		this.casts = new HashSet<>();
	}

	public Title(Long externalId, SourceType sourceType, TitleType titleType, String originalTitle, String companyName,
			String originalDescription, short year) {
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.titleType = titleType;
		this.originalTitle = originalTitle;
		this.companyName = companyName;
		this.originalDescription = originalDescription;
		this.year = year;
		this.visibility = Visibility.PRIVATE;
		this.setForDelete = false;

		this.titleTraductions = new HashSet<>();
		this.titleImages = new HashSet<>();
		this.genres = new HashSet<>();
		this.addedDate = LocalDateTime.now();
		this.crews = new HashSet<>();
		this.casts = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@OneToMany(mappedBy = "idTitle", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<TitleTraduction> getTitleTraductions() {
		return titleTraductions;
	}

	public void setTitleTraductions(Set<TitleTraduction> titleTraductions) {
		this.titleTraductions = titleTraductions;
	}

	public TitleType getTitleType() {
		return titleType;
	}

	public void setTitleType(TitleType titleType) {
		this.titleType = titleType;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	@OneToMany(mappedBy = "title", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<TitleImage> getTitleImages() {
		return titleImages;
	}

	public void setTitleImages(Set<TitleImage> titleImages) {
		this.titleImages = titleImages;
	}

	public short getYear() {
		return year;
	}

	public void setYear(short year) {
		this.year = year;
	}

	@JoinTable(name = "GenreTitle", joinColumns = @JoinColumn(name = "titleId"), inverseJoinColumns = @JoinColumn(name = "genreId"))
	@ManyToMany(fetch = FetchType.LAZY)
	public Set<Genre> getGenres() {
		return genres;
	}

	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "addedBy")
	public User getAddedByUser() {
		return addedByUser;
	}

	public void setAddedByUser(User addedByUser) {
		this.addedByUser = addedByUser;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public String getExternalImage() {
		return externalImage;
	}

	public void setExternalImage(String externalImage) {
		this.externalImage = externalImage;
	}

	public LocalDateTime getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(LocalDateTime addedDate) {
		this.addedDate = addedDate;
	}

	@OneToMany(mappedBy = "titleId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Crew> getCrews() {
		return crews;
	}

	public void setCrews(Set<Crew> crews) {
		this.crews = crews;
	}

	@OneToMany(mappedBy = "titleId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Cast> getCasts() {
		return casts;
	}

	public void setCasts(Set<Cast> casts) {
		this.casts = casts;
	}

	public float getAvgRating() {
		return avgRating;
	}

	public int getTimesRated() {
		return timesRated;
	}

	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}

	public void setTimesRated(int timesRated) {
		this.timesRated = timesRated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addedByUser == null) ? 0 : addedByUser.hashCode());
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		result = prime * result + ((externalImage == null) ? 0 : externalImage.hashCode());
		result = prime * result + ((genres == null) ? 0 : genres.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((originalDescription == null) ? 0 : originalDescription.hashCode());
		result = prime * result + ((originalTitle == null) ? 0 : originalTitle.hashCode());
		result = prime * result + (setForDelete ? 1231 : 1237);
		result = prime * result + ((sourceType == null) ? 0 : sourceType.hashCode());
		result = prime * result + ((titleImages == null) ? 0 : titleImages.hashCode());
		result = prime * result + ((titleTraductions == null) ? 0 : titleTraductions.hashCode());
		result = prime * result + ((titleType == null) ? 0 : titleType.hashCode());
		result = prime * result + ((visibility == null) ? 0 : visibility.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Title other = (Title) obj;
		if (addedByUser == null) {
			if (other.addedByUser != null)
				return false;
		} else if (!addedByUser.equals(other.addedByUser))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		if (externalImage == null) {
			if (other.externalImage != null)
				return false;
		} else if (!externalImage.equals(other.externalImage))
			return false;
		if (genres == null) {
			if (other.genres != null)
				return false;
		} else if (!genres.equals(other.genres))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (originalDescription == null) {
			if (other.originalDescription != null)
				return false;
		} else if (!originalDescription.equals(other.originalDescription))
			return false;
		if (originalTitle == null) {
			if (other.originalTitle != null)
				return false;
		} else if (!originalTitle.equals(other.originalTitle))
			return false;
		if (setForDelete != other.setForDelete)
			return false;
		if (sourceType != other.sourceType)
			return false;
		if (titleImages == null) {
			if (other.titleImages != null)
				return false;
		} else if (!titleImages.equals(other.titleImages))
			return false;
		if (titleTraductions == null) {
			if (other.titleTraductions != null)
				return false;
		} else if (!titleTraductions.equals(other.titleTraductions))
			return false;
		if (titleType != other.titleType)
			return false;
		if (visibility != other.visibility)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

}
