package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CreateSeasonParamsDTO {

	private Long externalId;
	private byte seasonNumber;
	private String originalTitle;
	private String originalDescription;
	private Long firstAirDate;
	private Long lastAirDate;
	private String imageContent;

	@Valid
	private List<SeasonTraductionDTO> traductions;

	public CreateSeasonParamsDTO(Long externalId, byte seasonNumber, String originalTitle, String originalDescription,
			Long firstAirDate, Long lastAirDate, String imageContent) {
		this.externalId = externalId;
		this.seasonNumber = seasonNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.firstAirDate = firstAirDate;
		this.lastAirDate = lastAirDate;
		this.imageContent = imageContent;
	}

	@NotNull()
	@Min(1)
	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	@NotNull()
	@Min(0)
	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	@NotNull()
	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public Long getFirstAirDate() {
		return firstAirDate;
	}

	public void setFirstAirDate(Long firstAirDate) {
		this.firstAirDate = firstAirDate;
	}

	public Long getLastAirDate() {
		return lastAirDate;
	}

	public void setLastAirDate(Long lastAirDate) {
		this.lastAirDate = lastAirDate;
	}

	public String getImageContent() {
		return imageContent;
	}

	public void setImageContent(String imageContent) {
		this.imageContent = imageContent;
	}

	public List<SeasonTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<SeasonTraductionDTO> traductions) {
		this.traductions = traductions;
	}

}
