package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.EpisodeTraduction;

public class EpisodeWithTraduction {

	private Episode episode;
	private EpisodeTraduction episodeTraduction;

	public EpisodeWithTraduction(Episode episode, EpisodeTraduction episodeTraduction) {
		this.episode = episode;
		this.episodeTraduction = episodeTraduction;
	}

	public Episode getEpisode() {
		return episode;
	}

	public EpisodeTraduction getEpisodeTraduction() {
		return episodeTraduction;
	}

	public void setEpisode(Episode episode) {
		this.episode = episode;
	}

	public void setEpisodeTraduction(EpisodeTraduction episodeTraduction) {
		this.episodeTraduction = episodeTraduction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((episode == null) ? 0 : episode.hashCode());
		result = prime * result + ((episodeTraduction == null) ? 0 : episodeTraduction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EpisodeWithTraduction other = (EpisodeWithTraduction) obj;
		if (episode == null) {
			if (other.episode != null)
				return false;
		} else if (!episode.equals(other.episode))
			return false;
		if (episodeTraduction == null) {
			if (other.episodeTraduction != null)
				return false;
		} else if (!episodeTraduction.equals(other.episodeTraduction))
			return false;
		return true;
	}

}
