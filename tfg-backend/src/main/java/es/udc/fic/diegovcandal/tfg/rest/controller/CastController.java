package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toCast;
import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toCastDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toCastEpisodeDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toEpisodeCast;
import static es.udc.fic.diegovcandal.tfg.rest.util.CastConversor.toInternalCastDTOs;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.CastService;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateCastEpisodeDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreatePersonCastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.IdDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.InternalPersonCastDTO;

@RestController
@RequestMapping("/credits/casts")
public class CastController {

	private static final String CAST_NOT_FOUND_EXCEPTION_ID = "exceptions.CastNotFoundException";
	private static final String CAST_ALREADY_EXISTS_EXCEPTION_ID = "exceptions.CastAlreadyExistsException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private CastService castService;

	@Value("${project.external.api.enabled}")
	private boolean apiEnabled;

	@ExceptionHandler(CastNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorDTO handleCastNotFoundException(CastNotFoundException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(CAST_NOT_FOUND_EXCEPTION_ID, null, CAST_NOT_FOUND_EXCEPTION_ID,
				locale);

		return new ErrorDTO(errorMessage);

	}

	@ExceptionHandler(CastAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleCastAlreadyExistsException(CastAlreadyExistsException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(CAST_ALREADY_EXISTS_EXCEPTION_ID, null,
				CAST_ALREADY_EXISTS_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@PostMapping("/titles")
	public IdDTO addPersonNewCastParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreatePersonCastDTO personCastDTO) throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		Cast cast = toCast(personCastDTO);

		return new IdDTO(castService.addPersonNewCastParticipation(userId, personCastDTO.getPersonId(),
				personCastDTO.getTitleId(), cast));
	}

	@PutMapping("/titles")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updatePersonCastParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreatePersonCastDTO personCastDTO)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CastAlreadyExistsException, CastNotFoundException {

		Cast cast = toCast(personCastDTO);

		castService.updatePersonCastParticipation(userId, personCastDTO.getId(), cast);
	}

	@DeleteMapping("/titles/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removePersonCastParticipation(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CastAlreadyExistsException, CastNotFoundException {

		castService.removePersonCastParticipation(userId, id);
	}

	@PostMapping("/episodes")
	public IdDTO addPersonEpisodeCastParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreateCastEpisodeDTO createCastEpisodeDTO) throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		CastEpisode cast = toEpisodeCast(createCastEpisodeDTO);

		return new IdDTO(castService.addPersonEpisodeCastParticipation(userId, createCastEpisodeDTO.getPersonId(),
				createCastEpisodeDTO.getTitleId(), createCastEpisodeDTO.getSeasonNumber(), cast));
	}

	@PutMapping("/episodes")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updatePersonEpisodeCastParticipation(@RequestAttribute Long userId,
			@Validated @RequestBody CreateCastEpisodeDTO createCastEpisodeDTO)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CastAlreadyExistsException, CastNotFoundException {

		castService.updatePersonEpisodeCastParticipation(userId, createCastEpisodeDTO.getId(),
				createCastEpisodeDTO.getCharName());
	}

	@DeleteMapping("/episodes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removePersonEpisodeCastParticipation(@PathVariable Long id, @RequestAttribute Long userId)
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CastAlreadyExistsException, CastNotFoundException {

		castService.removePersonEpisodeCastParticipation(userId, id);
	}

	@GetMapping("/titles/{id}/main")
	public List<CastDTO> findMainCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toCastDTOs(castService.findMainCast(id, localeId));

	}

	@GetMapping("/titles/{id}/all")
	public List<CastDTO> findAllCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toCastDTOs(castService.findAllCast(id, localeId));

	}

	@GetMapping("/titles/{id}/internal")
	public List<InternalPersonCastDTO> findAllinternalCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toInternalCastDTOs(castService.findAllCast(id, localeId));

	}

	@GetMapping("/episodes/{id}/all")
	public List<CastDTO> findEpisodeCast(@PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		return toCastEpisodeDTOs(castService.findEpisodeCast(id, localeId));

	}

}
