package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class SeasonSummaryDTO {

	private Long id;
	private Long externalId;
	private byte seasonNumber;
	private String originalTitle;
	private String originalDescription;
	private short year;
	private short episodeCount;
	private String title;
	private String description;
	private boolean image;
	private String imagePath;
	private Long sourceType;
	private boolean setForDelete;
	private String tvshowOriginalTitle;

	private Long firstAirDate;
	private Long lastAirDate;

	private List<EpisodeSummaryDTO> episodes;
	private List<SeasonTraductionDTO> traductions;

	public SeasonSummaryDTO(Long id, Long externalId, byte seasonNumber, String originalTitle,
			String originalDescription, short year, short episodeCount, String title, String description, boolean image,
			Long sourceType) {
		this.id = id;
		this.externalId = externalId;
		this.seasonNumber = seasonNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.year = year;
		this.episodeCount = episodeCount;
		this.title = title;
		this.description = description;
		this.image = image;
		this.sourceType = sourceType;
	}

	public SeasonSummaryDTO(Long id, Long externalId, byte seasonNumber, String originalTitle,
			String originalDescription, short year, short episodeCount, String title, String description,
			String imagePath, Long sourceType) {
		this.id = id;
		this.externalId = externalId;
		this.seasonNumber = seasonNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.year = year;
		this.episodeCount = episodeCount;
		this.title = title;
		this.description = description;
		this.imagePath = imagePath;
		this.sourceType = sourceType;
	}

	public SeasonSummaryDTO(Long id, Long externalId, byte seasonNumber, String originalTitle,
			String originalDescription, boolean image, boolean setForDelete, Long firstAirDate, Long lastAirDate,
			List<SeasonTraductionDTO> traductions) {
		this.id = id;
		this.externalId = externalId;
		this.seasonNumber = seasonNumber;
		this.originalTitle = originalTitle;
		this.originalDescription = originalDescription;
		this.image = image;
		this.setForDelete = setForDelete;
		this.firstAirDate = firstAirDate;
		this.lastAirDate = lastAirDate;
		this.traductions = traductions;
	}

	public Long getId() {
		return id;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getOriginalDescription() {
		return originalDescription;
	}

	public short getYear() {
		return year;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setOriginalDescription(String originalDescription) {
		this.originalDescription = originalDescription;
	}

	public void setYear(short year) {
		this.year = year;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public List<EpisodeSummaryDTO> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<EpisodeSummaryDTO> episodes) {
		this.episodes = episodes;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public List<SeasonTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<SeasonTraductionDTO> traductions) {
		this.traductions = traductions;
	}

	public Long getFirstAirDate() {
		return firstAirDate;
	}

	public Long getLastAirDate() {
		return lastAirDate;
	}

	public void setTvshowOriginalTitle(String tvshowOriginalTitle) {
		this.tvshowOriginalTitle = tvshowOriginalTitle;
	}

	public String getTvshowOriginalTitle() {
		return tvshowOriginalTitle;
	}

}
