package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SeasonTraductionDTO {

	private Long idLocale;
	private String title;
	private String description;

	public SeasonTraductionDTO(Long idLocale, String title, String description) {
		this.idLocale = idLocale;
		this.title = title;
		this.description = description;
	}

	@NotNull()
	@Min(0)
	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	@NotNull()
	@Size(min = 1, max = 50)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@NotNull()
	@Size(min = 0, max = 500)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
