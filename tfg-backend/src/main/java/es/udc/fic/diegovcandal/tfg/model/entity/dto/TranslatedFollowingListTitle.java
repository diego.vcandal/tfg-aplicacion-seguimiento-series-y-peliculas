package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;

public class TranslatedFollowingListTitle {

	private FollowingListTitle followingListTitle;
	private TitleTraduction titleTraduction;

	public TranslatedFollowingListTitle() {
	}

	public TranslatedFollowingListTitle(FollowingListTitle followingListTitle, TitleTraduction titleTraduction) {
		this.followingListTitle = followingListTitle;
		this.titleTraduction = titleTraduction;
	}

	public FollowingListTitle getFollowingListTitle() {
		return followingListTitle;
	}

	public TitleTraduction getTitleTraduction() {
		return titleTraduction;
	}

	public void setFollowingListTitle(FollowingListTitle followingListTitle) {
		this.followingListTitle = followingListTitle;
	}

	public void setTitleTraduction(TitleTraduction titleTraduction) {
		this.titleTraduction = titleTraduction;
	}

}
