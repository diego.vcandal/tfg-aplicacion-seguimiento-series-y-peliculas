package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.NotNull;

public class FriendRequestParamsDTO {

	private Long origin;
	private Long destination;

	public FriendRequestParamsDTO() {

	}

	@NotNull
	public Long getOrigin() {
		return origin;
	}

	public void setOrigin(Long origin) {
		this.origin = origin;
	}

	@NotNull
	public Long getDestination() {
		return destination;
	}

	public void setDestination(Long destination) {
		this.destination = destination;
	}

}
