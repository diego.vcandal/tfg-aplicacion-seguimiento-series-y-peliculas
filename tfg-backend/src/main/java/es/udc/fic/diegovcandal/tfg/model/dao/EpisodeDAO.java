package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.EpisodeWithTraduction;

public interface EpisodeDAO extends PagingAndSortingRepository<Episode, Long> {

	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.EpisodeWithTraduction(e, et) "
			+ "FROM Episode e LEFT JOIN EpisodeTraduction et " 
			+ "on e.id = et.idEpisode AND " 
			+ "et.idLocale = ?3 "
			+ "WHERE e.season = ?1 AND e.episodeNumber = ?2")
	Optional<EpisodeWithTraduction> findEpisodeWithTraduction(Season season, short episodeNumber, Long localeId);

	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.EpisodeWithTraduction(e, et) "
			+ "FROM Episode e LEFT JOIN EpisodeTraduction et " 
			+ "on e.id = et.idEpisode AND " 
			+ "et.idLocale = ?2 "
			+ "WHERE e.season = ?1")
	List<EpisodeWithTraduction> findAllEpisodeWithTraduction(Season season, Long localeId);

	@Query("SELECT e.image "
			+ "FROM Episode e "
			+ "WHERE e.season = ?1 AND e.episodeNumber = ?2")
	String findEpisodeImage(Season season, short episodeNumber);

	Optional<Episode> findByExternalId(Long externalId);
	
	@Query("SELECT e "
			+ "FROM Episode e " 
			+ "WHERE e.season = ?1 AND e.episodeNumber = ?2")
	Optional<Episode> findEpisode(Season season, short episodeNumber);

}
