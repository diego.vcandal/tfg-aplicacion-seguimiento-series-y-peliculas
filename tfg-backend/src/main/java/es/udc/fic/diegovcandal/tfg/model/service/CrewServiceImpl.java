package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CrewDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CrewEpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CrewTypeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewType;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewTypeNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;

@Service
@Transactional()
public class CrewServiceImpl implements CrewService {

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private CrewDAO crewDAO;

	@Autowired
	private CrewEpisodeDAO crewEpisodeDAO;

	@Autowired
	private CrewTypeDAO crewTypeDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	public Long addPersonNewCrewParticipation(Long userId, Long personId, Long titleId, Crew newCrew)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException, TitleNotFoundException,
			CrewAlreadyExistsException, CrewTypeNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<CrewType> crewType = crewTypeDAO.findById(newCrew.getCrewTypeId().getId());

		if (!crewType.isPresent()) {
			throw new CrewTypeNotFoundException();
		}

		Optional<Crew> crew = crewDAO.findByPeopleIdAndTitleIdAndCrewTypeId(people.get(), title.get(), crewType.get());

		if (crew.isPresent()) {
			throw new CrewAlreadyExistsException();
		}

		int totalEpisodes = 0;
		for (CrewSeason crewSeason : newCrew.getCrewSeasons()) {

			Optional<Season> season = seasonDAO.findSeason((TVShow) title.get(),
					crewSeason.getSeasonId().getSeasonNumber());

			if (!season.isPresent()) {
				throw new TitleNotFoundException(titleId);
			}

			crewSeason.setSeasonId(season.get());

			int episodes = addCrewEpisodes(crewSeason);
			totalEpisodes += episodes;

			crewSeason.setEpisodeCount((short) episodes);

		}

		newCrew.setEpisodeCount((short) totalEpisodes);
		newCrew.setTitleId(title.get());
		newCrew.setPeopleId(people.get());

		return crewDAO.save(newCrew).getId();

	}

	@Override
	public void updatePersonCrewParticipation(Long userId, Long crewId, Crew newCrew) throws PeopleNotFoundException,
			UserNotFoundException, PermissionException, TitleNotFoundException, CrewNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<Crew> crew = crewDAO.findById(crewId);

		if (!crew.isPresent()) {
			throw new CrewNotFoundException();
		}

		int totalEpisodes = 0;
		Set<CrewSeason> crewSeasons = new HashSet<>();
		for (CrewSeason newCrewSeason : newCrew.getCrewSeasons()) {

			byte seasonNumber = newCrewSeason.getSeasonId().getSeasonNumber();
			int episodes = 0;

			Optional<CrewSeason> crewSeason = crew.get().getCrewSeasons().stream()
					.filter(cs -> cs.getSeasonId().getSeasonNumber() == seasonNumber).findFirst();

			if (!crewSeason.isPresent()) {

				Optional<Season> season = seasonDAO.findSeason(
						tvShowDAO.findById(crew.get().getTitleId().getId()).get(),
						newCrewSeason.getSeasonId().getSeasonNumber());

				if (!season.isPresent()) {
					throw new TitleNotFoundException(crew.get().getTitleId());
				}

				newCrewSeason.setSeasonId(season.get());
				episodes = addCrewEpisodes(newCrewSeason);
				newCrewSeason.setEpisodeCount((short) episodes);
				newCrewSeason.setCrewId(crew.get());

				totalEpisodes += episodes;

				crewSeasons.add(newCrewSeason);

			} else {

				Set<CrewEpisode> crewEpisodes = new HashSet<>();

				for (CrewEpisode newCrewEpisode : newCrewSeason.getCrewEpisodes()) {

					short episodeNumber = newCrewEpisode.getEpisodeId().getEpisodeNumber();

					Optional<CrewEpisode> crewEpisode = crewSeason.get().getCrewEpisodes().stream()
							.filter(ce -> ce.getEpisodeId().getEpisodeNumber() == episodeNumber).findFirst();

					if (!crewEpisode.isPresent()) {

						Optional<Episode> episode = episodeDAO.findEpisode(crewSeason.get().getSeasonId(),
								newCrewEpisode.getEpisodeId().getEpisodeNumber());

						if (!episode.isPresent()) {
							throw new TitleNotFoundException(crew.get().getTitleId());
						}

						newCrewEpisode.setEpisodeId(episode.get());
						newCrewEpisode.setCrewSeasonId(crewSeason.get());

						episodes++;

						crewEpisodes.add(newCrewEpisode);

					} else {

						crewEpisodes.add(crewEpisode.get());
					}

					totalEpisodes++;

				}

				crewSeason.get().setEpisodeCount((short) episodes);
				crewSeason.get().getCrewEpisodes().removeAll(crewSeason.get().getCrewEpisodes());
				crewSeason.get().getCrewEpisodes().addAll(crewEpisodes);

				crewSeasons.add(crewSeason.get());

			}

		}

		if (crew.get().getTitleId().getTitleType() == TitleType.TV_SHOW) {
			crew.get().setEpisodeCount((short) totalEpisodes);
			crew.get().getCrewSeasons().removeAll(crew.get().getCrewSeasons());
			crew.get().getCrewSeasons().addAll(crewSeasons);

		}

		crewDAO.save(crew.get());

	}

	@Override
	public void removePersonCrewParticipation(Long userId, Long crewId)
			throws UserNotFoundException, PermissionException, CrewNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<Crew> crew = crewDAO.findById(crewId);

		if (!crew.isPresent()) {
			throw new CrewNotFoundException();
		}

		crewDAO.delete(crew.get());

	}

	@Override
	public Long addPersonEpisodeCrewParticipation(Long userId, Long peopleId, Long titleId, byte seasonNumber,
			CrewEpisode newCrewEpisode) throws UserNotFoundException, PermissionException, PeopleNotFoundException,
			TitleNotFoundException, CrewTypeNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<People> people = peopleDAO.findById(peopleId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(peopleId);
		}

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<CrewType> crewType = crewTypeDAO.findById(newCrewEpisode.getCrewTypeId().getId());

		if (!crewType.isPresent()) {
			throw new CrewTypeNotFoundException();
		}

		Optional<Crew> crew = crewDAO.findByPeopleIdAndTitleIdAndCrewTypeId(people.get(), title.get(), crewType.get());

		if (crew.isPresent()) {

			Optional<CrewSeason> crewSeason = crew.get().getCrewSeasons().stream()
					.filter(cs -> cs.getSeasonId().getSeasonNumber() == seasonNumber).findFirst();

			if (crewSeason.isPresent()) {

				Optional<CrewEpisode> crewEpisode = crewSeason.get().getCrewEpisodes().stream().filter(
						ce -> ce.getEpisodeId().getEpisodeNumber() == newCrewEpisode.getEpisodeId().getEpisodeNumber())
						.findFirst();

				if (crewEpisode.isPresent()) {
					return crewEpisode.get().getId();
				}

				addEpisodeToCrewSeason(crewSeason.get(), newCrewEpisode);

				crewSeason.get().setEpisodeCount((short) (crewSeason.get().getEpisodeCount() + 1));

			} else {

				addCrewSeasonWithCrewEpisode(crew.get(), seasonNumber, newCrewEpisode);

			}

			crew.get().setEpisodeCount((short) (crew.get().getEpisodeCount() + 1));

		} else {

			Crew newCrew = new Crew(title.get(), people.get(), crewType.get(), (short) 1);

			addCrewSeasonWithCrewEpisode(newCrew, seasonNumber, newCrewEpisode);

			return crewDAO.save(newCrew).getCrewSeasons().stream().findFirst().get().getCrewEpisodes().stream()
					.findFirst().get().getId();

		}

		return crewDAO.save(crew.get()).getCrewSeasons().stream()
				.filter(cs -> cs.getSeasonId().getSeasonNumber() == seasonNumber).findFirst().get().getCrewEpisodes()
				.stream()
				.filter(ce -> ce.getEpisodeId().getEpisodeNumber() == newCrewEpisode.getEpisodeId().getEpisodeNumber())
				.findFirst().get().getId();

	}

	@Override
	public void removePersonEpisodeCrewParticipation(Long userId, Long episodeCrewId) throws UserNotFoundException,
			PermissionException, PeopleNotFoundException, TitleNotFoundException, CrewNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<CrewEpisode> crewEpisode = crewEpisodeDAO.findById(episodeCrewId);

		if (!crewEpisode.isPresent()) {
			throw new CrewNotFoundException();
		}

		CrewSeason crewSeason = crewEpisode.get().getCrewSeasonId();
		Crew crew = crewSeason.getCrewId();

		crewSeason.getCrewEpisodes().remove(crewEpisode.get());
		crewSeason.setEpisodeCount((short) (crewSeason.getEpisodeCount() - 1));
		crew.setEpisodeCount((short) (crew.getEpisodeCount() - 1));

		crewDAO.save(crew);

	}

	@Override
	@Transactional(readOnly = true)
	public List<Crew> findAllCrew(Long titleId, Long localeId) {

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			return new ArrayList<>();
		}

		List<Crew> list = crewDAO.findByTitleId(title.get());

		for (Crew crew : list) {
			translatePerson(crew.getPeopleId(), localeId);
		}

		return list;

	}

	@Override
	@Transactional(readOnly = true)
	public List<CrewEpisode> findEpisodeCrew(Long episodeId, Long localeId) {

		Optional<Episode> episode = episodeDAO.findById(episodeId);

		if (!episode.isPresent()) {
			return new ArrayList<>();
		}

		List<CrewEpisode> list = crewEpisodeDAO.findByEpisodeId(episode.get());

		for (CrewEpisode crew : list) {
			translatePerson(crew.getCrewSeasonId().getCrewId().getPeopleId(), localeId);
		}

		return list;

	}

	private void translatePerson(People person, Long localeId) {

		Optional<PeopleTraduction> peopleTraduction = person.getPeopleTraductions().stream()
				.filter(t -> t.getIdLocale() == localeId).findFirst();

		if (!peopleTraduction.isPresent()) {
			return;
		}

		person.setOriginalName(peopleTraduction.get().getName());

	}

	private int addCrewEpisodes(CrewSeason crewSeason) throws TitleNotFoundException {

		int episodes = 0;

		for (CrewEpisode crewEpisode : crewSeason.getCrewEpisodes()) {

			addEpisodeToCrewSeason(crewSeason, crewEpisode);

			episodes++;

		}

		return episodes;
	}

	private void addEpisodeToCrewSeason(CrewSeason crewSeason, CrewEpisode crewEpisode) throws TitleNotFoundException {

		Optional<Episode> episode = episodeDAO.findEpisode(crewSeason.getSeasonId(),
				crewEpisode.getEpisodeId().getEpisodeNumber());

		if (!episode.isPresent()) {
			throw new TitleNotFoundException(crewSeason.getSeasonId().getId());
		}

		crewEpisode.setEpisodeId(episode.get());
		crewEpisode.setCrewSeasonId(crewSeason);
		crewSeason.getCrewEpisodes().add(crewEpisode);

	}

	private void addCrewSeasonWithCrewEpisode(Crew crew, byte seasonNumber, CrewEpisode crewEpisode)
			throws TitleNotFoundException {

		Optional<Season> season = seasonDAO.findSeason((TVShow) crew.getTitleId(), seasonNumber);

		if (!season.isPresent()) {
			throw new TitleNotFoundException(crew.getTitleId().getId());
		}

		CrewSeason newCrewSeason = new CrewSeason(crew, season.get(), (short) 1);

		addEpisodeToCrewSeason(newCrewSeason, crewEpisode);

		newCrewSeason.getCrewEpisodes().add(crewEpisode);
		crewEpisode.setCrewSeasonId(newCrewSeason);
		crew.getCrewSeasons().add(newCrewSeason);

	}

}
