package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class TitleTraductionCompositeKey implements Serializable {

	private static final long serialVersionUID = 85922670431318940L;

	private Long idTitle;
	private Long idLocale;

	public TitleTraductionCompositeKey() {

	}

	public TitleTraductionCompositeKey(Long idTitle, Long idLocale) {

		this.idTitle = idTitle;
		this.idLocale = idLocale;
	}

	public Long getIdTitle() {
		return idTitle;
	}

	public void setIdTitle(Long idTitle) {
		this.idTitle = idTitle;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLocale == null) ? 0 : idLocale.hashCode());
		result = prime * result + ((idTitle == null) ? 0 : idTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitleTraductionCompositeKey other = (TitleTraductionCompositeKey) obj;
		if (idLocale == null) {
			if (other.idLocale != null)
				return false;
		} else if (!idLocale.equals(other.idLocale))
			return false;
		if (idTitle == null) {
			if (other.idTitle != null)
				return false;
		} else if (!idTitle.equals(other.idTitle))
			return false;
		return true;
	}

}
