package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class UserNotFoundException extends InstanceNotFoundException {

	public UserNotFoundException(Object key) {
		super("entities.user", key);
	}

}
