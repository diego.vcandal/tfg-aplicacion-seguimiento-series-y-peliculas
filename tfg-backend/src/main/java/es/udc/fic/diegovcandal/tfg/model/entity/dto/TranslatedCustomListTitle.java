package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;

public class TranslatedCustomListTitle {

	private CustomListTitle customListTitle;
	private TitleTraduction titleTraduction;

	public TranslatedCustomListTitle() {
	}

	public TranslatedCustomListTitle(CustomListTitle customListTitle, TitleTraduction titleTraduction) {
		this.customListTitle = customListTitle;
		this.titleTraduction = titleTraduction;
	}

	public CustomListTitle getCustomListTitle() {
		return customListTitle;
	}

	public TitleTraduction getTitleTraduction() {
		return titleTraduction;
	}

	public void setCustomListTitle(CustomListTitle customListTitle) {
		this.customListTitle = customListTitle;
	}

	public void setTitleTraduction(TitleTraduction titleTraduction) {
		this.titleTraduction = titleTraduction;
	}

}
