package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.CustomListTitleCompositeKey;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedCustomListTitle;

public interface CustomListTitleDAO extends PagingAndSortingRepository<CustomListTitle, CustomListTitleCompositeKey> {
	
	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedCustomListTitle(ctt, tt) "
			+ "FROM CustomListTitle ctt " 
			+ "		JOIN Title t " 
			+ "			on t.id = ctt.titleId "
			+ "		LEFT JOIN TitleTraduction tt " 
			+ "			on tt.idTitle = t.id AND "
			+ "				tt.idLocale = ?2 " 
			+ "WHERE ctt.listId = ?1 "
			+ "ORDER BY tt.titleName, t.originalTitle")
	List<TranslatedCustomListTitle> findCustomListTranslatedTitles(CustomList listId, Long localeId);

	boolean existsByListIdAndTitleId(CustomList listId, Title titleId);
	
	Optional<CustomListTitle> findByListIdAndTitleId(CustomList listId, Title titleId);
	
}
