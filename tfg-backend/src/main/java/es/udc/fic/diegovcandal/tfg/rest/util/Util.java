package es.udc.fic.diegovcandal.tfg.rest.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;

import es.udc.fic.diegovcandal.tfg.rest.exceptions.ApiNotEnabledException;

public class Util {

	public static void checkApiEnabled(boolean apiEnabled) throws ApiNotEnabledException {
		if (!apiEnabled)
			throw new ApiNotEnabledException();
	}

	public static void sendImage(HttpServletResponse response, String image) {

		if (image == null) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}

		byte[] decoded = Base64.getDecoder().decode(image.split(",")[1]);

		response.setContentType("image/png");
		response.setContentLength(decoded.length);

		InputStream inputStream = new ByteArrayInputStream(decoded);

		try {
			IOUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
