package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.List;
import java.util.Set;

import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedCustomList;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ListElementsLimitExceededException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;

public interface CustomListService {

	TranslatedCustomList getCustomList(Long userId, Long listId, Long localeId)
			throws CustomListNotFoundException, PrivateResourceException, UserNotFoundException;

	Long createCustomList(Long userId, CustomList list) throws UserNotFoundException, TitleNotFoundException;

	void updateCustomList(Long userId, Long listId, CustomList updatedList)
			throws UserNotFoundException, TitleNotFoundException, PermissionException, CustomListNotFoundException;

	void deleteCustomList(Long userId, Long listId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException;

	void changeListVisbility(Long userId, Long listId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException;

	void removeLike(Long userId, Long listId)
			throws CustomListNotFoundException, UserNotFoundException, PermissionException;

	void addLike(Long userId, Long listId)
			throws UserNotFoundException, CustomListNotFoundException, PermissionException;

	List<CustomList> getCustomLists(Long userId, Long listUserId) throws UserNotFoundException;

	List<CustomList> getMyCustomListsFromTitle(Long userId, Long titleId) throws UserNotFoundException;

	void addToList(Long userId, Long listId, Long titleId) throws PermissionException, TitleNotFoundException,
			UserNotFoundException, CustomListNotFoundException, ListElementsLimitExceededException;

	List<CustomList> getMyCustomLists(Long userId) throws UserNotFoundException;

	Long updateElementsOrder(Long userId, Long listId, Set<CustomListTitle> newList)
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException;

	Block<CustomList> getMyCustomLists(Long userId, int pageNumber, int size) throws UserNotFoundException;

	Block<CustomList> getCustomLists(Long userId, Long listUserId, int pageNumber, int size)
			throws UserNotFoundException;

	void deleteFromList(Long userId, Long listId, Long titleId)
			throws PermissionException, TitleNotFoundException, UserNotFoundException, CustomListNotFoundException;

}
