package es.udc.fic.diegovcandal.tfg.model.restclient;

import static es.udc.fic.diegovcandal.tfg.rest.util.TitleInfoConversor.externalToMovieInfoDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.TitleInfoConversor.externalToTVShowInfoDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.BlockDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TitleInfoDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.BlockExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

@Service
@Transactional
public class TmdbAPIClientSearchServiceImpl implements TmdbAPIClientSearchService {

	@Value("${project.external.api.apiKey}")
	private String apiKey;

	@Value("${project.external.api.apiEndpoint}")
	private String apiEndpoint;

	@Value("${project.external.api.apiImageEndpoint}")
	private String apiImageEndpoint;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	@Transactional(readOnly = true)
	public BlockExternalDTO<MovieExternalDTO> findExternalMovies(String titleName, int page, String acceptLanguage) {

		if (titleName == null || titleName.trim().equals("")) {
			return new BlockExternalDTO<>(new ArrayList<MovieExternalDTO>(), 0, 0, 0);
		}

		BlockExternalDTO<MovieExternalDTO> block = restTemplate.exchange(
				apiEndpoint + "/search/movie" + "?api_key=" + apiKey + "&language=" + acceptLanguage + "&query="
						+ titleName + "&page=" + page + "&include_adult=false",
				HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<BlockExternalDTO<MovieExternalDTO>>() {
				}).getBody();

		return new BlockExternalDTO<>(block.getResults(), block.getPage(), block.getTotal_results(),
				block.getTotal_pages());

	}

	@Override
	@Transactional(readOnly = true)
	public BlockExternalDTO<TVShowExternalDTO> findExternalTVShows(String titleName, int page, String acceptLanguage) {

		if (titleName == null || titleName.trim().equals("")) {
			return new BlockExternalDTO<>(new ArrayList<TVShowExternalDTO>(), 0, 0, 0);
		}

		BlockExternalDTO<TVShowExternalDTO> block = restTemplate.exchange(
				apiEndpoint + "/search/tv" + "?api_key=" + apiKey + "&language=" + acceptLanguage + "&query="
						+ titleName + "&page=" + page + "&include_adult=false",
				HttpMethod.GET, HttpEntity.EMPTY,
				new ParameterizedTypeReference<BlockExternalDTO<TVShowExternalDTO>>() {
				}).getBody();

		return new BlockExternalDTO<>(block.getResults(), block.getPage(), block.getTotal_results(),
				block.getTotal_pages());
	}

	@Override
	@Transactional(readOnly = true)
	public BlockDTO<TitleInfoDTO> mergeTitleSearch(String titleName, String acceptLanguage,
			List<TitleInfo> internalTitles) {

		List<MovieExternalDTO> movies = findExternalMovies(titleName, 1, acceptLanguage).getResults();
		List<TVShowExternalDTO> tvshows = findExternalTVShows(titleName, 1, acceptLanguage).getResults();

		if (movies.size() > 10) {
			movies = movies.subList(0, 9);
		}

		if (tvshows.size() > 10) {
			tvshows = tvshows.subList(0, 9);
		}

		List<TitleInfoDTO> finalList = new ArrayList<>();
		for (TitleInfo titleInfo : internalTitles) {

			String extImage = null;

			Optional<MovieExternalDTO> movieExternalDTO = movies.stream()
					.filter(m -> m.getId().equals(titleInfo.getExternalId())).findFirst();

			if (movieExternalDTO.isPresent()) {
				movies.remove(movieExternalDTO.get());
			}

			Optional<TVShowExternalDTO> tvshowExternalDTO = tvshows.stream()
					.filter(t -> t.getId().equals(titleInfo.getExternalId())).findFirst();

			if (tvshowExternalDTO.isPresent()) {
				tvshows.remove(tvshowExternalDTO.get());
				extImage = tvshowExternalDTO.get().getPoster_path();
			}

			finalList.add(new TitleInfoDTO(titleInfo.getId(), titleInfo.getExternalId(), titleInfo.getOriginalTitle(),
					titleInfo.getTitleName(), titleInfo.getDescription(), titleInfo.getTitleType().getId(),
					titleInfo.getSourceType().getId(), titleInfo.getOriginalDescription(), titleInfo.getImage() != null,
					extImage, titleInfo.getYear(), titleInfo.getAvgRating(), titleInfo.getTimesRated()));
		}

		int moviePop = 0;
		if (movies.size() > 0)
			moviePop = movies.get(0).getPopularity();

		int tvshowPop = 0;
		if (tvshows.size() > 0)
			tvshowPop = tvshows.get(0).getPopularity();

		if (moviePop > tvshowPop) {
			movies.stream().forEach(m -> finalList.add(externalToMovieInfoDTO(m)));
			tvshows.stream().forEach(t -> finalList.add(externalToTVShowInfoDTO(t)));
		} else {
			tvshows.stream().forEach(t -> finalList.add(externalToTVShowInfoDTO(t)));
			movies.stream().forEach(m -> finalList.add(externalToMovieInfoDTO(m)));
		}

		return new BlockDTO<>(finalList, false, 1, finalList.size(), titleName);
	}

}
