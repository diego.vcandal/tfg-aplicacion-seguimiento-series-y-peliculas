package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class InstanceNotFoundException extends Exception {

	private String name;
	private Object key;

	protected InstanceNotFoundException(String message) {
		super(message);
	}

	public InstanceNotFoundException(String name, Object key) {
		this.name = name;
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public Object getKey() {
		return key;
	}

}
