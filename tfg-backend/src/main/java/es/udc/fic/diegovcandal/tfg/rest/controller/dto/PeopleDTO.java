package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class PeopleDTO {

	private Long id;
	private Long externalId;
	private Long sourceType;
	private String originalName;
	private Long birthday;
	private Long deathday;
	private String image;
	private String externalImage;
	private boolean visibility;
	private boolean setForDelete;
	private boolean hasImage;
	private boolean internal;

	private List<PersonTraductionDTO> traductions;

	public PeopleDTO(Long id, Long externalId, Long sourceType, String originalName, Long birthday, Long deathday,
			String image, String externalImage, boolean setForDelete, List<PersonTraductionDTO> traductions) {
		this.id = id;
		this.externalId = externalId;
		this.sourceType = sourceType;
		this.originalName = originalName;
		this.birthday = birthday;
		this.deathday = deathday;
		this.image = image;
		this.externalImage = externalImage;
		this.setForDelete = setForDelete;
		this.traductions = traductions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public Long getBirthday() {
		return birthday;
	}

	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}

	public Long getDeathday() {
		return deathday;
	}

	public void setDeathday(Long deathday) {
		this.deathday = deathday;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getExternalImage() {
		return externalImage;
	}

	public void setExternalImage(String externalImage) {
		this.externalImage = externalImage;
	}

	public boolean getVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public boolean isSetForDelete() {
		return setForDelete;
	}

	public void setSetForDelete(boolean setForDelete) {
		this.setForDelete = setForDelete;
	}

	public List<PersonTraductionDTO> getTraductions() {
		return traductions;
	}

	public void setTraductions(List<PersonTraductionDTO> traductions) {
		this.traductions = traductions;
	}

	public boolean isHasImage() {
		return hasImage;
	}

	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setIsInternal(boolean internal) {
		this.internal = internal;
	}

}
