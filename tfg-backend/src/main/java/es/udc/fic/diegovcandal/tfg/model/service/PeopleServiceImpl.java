package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CastDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CrewDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PeopleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCastSummary;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCrewSummary;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Service
@Transactional()
public class PeopleServiceImpl implements PeopleService {

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private CastDAO castDAO;

	@Autowired
	private CrewDAO crewDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	public Long existsByExternalId(Long externalId) {

		Optional<People> people = peopleDAO.findByExternalId(externalId);

		if (!people.isPresent()) {
			return -1L;
		}

		return people.get().getId();

	}

	@Override
	public People findByExternalId(Long externalId) throws PeopleNotFoundException {

		Optional<People> people = peopleDAO.findByExternalId(externalId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(externalId);
		}

		return people.get();

	}

	@Override
	public Long createPerson(Long userId, People people)
			throws UserNotFoundException, PermissionException, ExternalIdAlreadyLinkedException {

		if (peopleDAO.existsByExternalId(people.getExternalId())) {
			throw new ExternalIdAlreadyLinkedException();
		}

		User user = permissionManager.findAdminUser(userId);

		people.setSourceType(SourceType.INTERNAL);
		people.setAddedByUser(user);

		if (people.getPeopleTraductions().size() != 0) {
			PeopleTraduction traduction = people.getPeopleTraductions().stream().findFirst().get();
			traduction.setOriginalLanguage(true);
			people.setOriginalName(traduction.getName());
		}

		return peopleDAO.save(people).getId();
	}

	@Override
	public void uptadePerson(Long userId, Long peopleId, People updatedPeople)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException {

		Optional<People> optional = peopleDAO.findById(peopleId);

		if (!optional.isPresent()) {
			throw new PeopleNotFoundException(peopleId);
		}

		permissionManager.findAdminUser(userId);

		People people = optional.get();

		updateTraductions(people, updatedPeople);

		people.setBirthday(updatedPeople.getBirthday());
		people.setDeathday(updatedPeople.getDeathday());
		people.setImage(updatedPeople.getImage() == null ? people.getImage()
				: (updatedPeople.getImage().trim().equals("") ? null : updatedPeople.getImage()));

		peopleDAO.save(people);

	}

	@Override
	@Transactional(readOnly = true)
	public People getInternalPerson(Long id, Long userId)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException {

		permissionManager.findAdminUser(userId);

		Optional<People> people = peopleDAO.findById(id);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(id);
		}

		return people.get();
	}

	@Override
	public PeopleWithTraduction getPersonDetails(Long userId, Long id, Long localeId)
			throws PeopleNotFoundException, UserNotFoundException {

		Optional<People> people = peopleDAO.findById(id);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(id);
		}

		boolean isAdmin = false;

		if (userId != null)
			isAdmin = permissionManager.findUser(userId).getRole() == UserRole.ADMIN;

		if (!isAdmin && people.get().getVisibility() == Visibility.PRIVATE)
			throw new PeopleNotFoundException(id);

		Optional<PeopleTraduction> peopleTraduction = people.get().getPeopleTraductions().stream()
				.filter(t -> t.getIdLocale() == localeId).findFirst();

		if (!peopleTraduction.isPresent()) {
			peopleTraduction = people.get().getPeopleTraductions().stream().filter(t -> t.isOriginalLanguage())
					.findFirst();
		}

		return new PeopleWithTraduction(people.get(), peopleTraduction.get());

	}

	@Override
	public void makePersonPublic(Long userId, Long personId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		permissionManager.findAdminUser(userId);

		people.get().setVisibility(Visibility.PUBLIC);

		peopleDAO.save(people.get());
	}

	@Override
	public void setPersonForDeletion(Long userId, Long personId, boolean setForDeletion)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		permissionManager.findAdminUser(userId);

		people.get().setSetForDelete(setForDeletion);
		people.get().setSourceType(setForDeletion ? SourceType.EXTERNAL_TMDB : SourceType.INTERNAL);

		peopleDAO.save(people.get());
	}

	@Override
	public void changeSourceType(Long userId, Long personId)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		permissionManager.findAdminUser(userId);

		if (people.get().isSetForDelete())
			return;

		if (people.get().getVisibility() == Visibility.PRIVATE)
			return;

		people.get().setSourceType(
				people.get().getSourceType() == SourceType.INTERNAL ? SourceType.EXTERNAL_TMDB : SourceType.INTERNAL);

		peopleDAO.save(people.get());
	}

	@Override
	@Transactional(readOnly = true)
	public List<PersonCastSummary> findCastHistory(Long personId, Long localeId) throws PeopleNotFoundException {

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		return castDAO.findCastHistory(people.get(), localeId);

	}

	@Override
	@Transactional(readOnly = true)
	public List<PersonCrewSummary> findCrewHistory(Long personId, Long localeId) throws PeopleNotFoundException {

		Optional<People> people = peopleDAO.findById(personId);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(personId);
		}

		return crewDAO.findCrewHistory(people.get(), localeId);

	}

	private void updateTraductions(People people, People updatedPeople) {
		people.getPeopleTraductions().removeAll(people.getPeopleTraductions());
		for (PeopleTraduction traduction : updatedPeople.getPeopleTraductions()) {
			traduction.setIdPeople(people);

			if (traduction.isOriginalLanguage())
				people.setOriginalName(traduction.getName());

			people.getPeopleTraductions().add(traduction);
		}
	}

}
