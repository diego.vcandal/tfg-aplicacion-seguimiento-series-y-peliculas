package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class PersonCreditsSummaryDTO {

	private List<PersonCastSummaryDTO> cast;
	private List<PersonCrewSummaryDTO> crew;

	public PersonCreditsSummaryDTO(List<PersonCastSummaryDTO> cast, List<PersonCrewSummaryDTO> crew) {
		this.cast = cast;
		this.crew = crew;
	}

	public List<PersonCastSummaryDTO> getCast() {
		return cast;
	}

	public List<PersonCrewSummaryDTO> getCrew() {
		return crew;
	}

	public void setCast(List<PersonCastSummaryDTO> cast) {
		this.cast = cast;
	}

	public void setCrew(List<PersonCrewSummaryDTO> crew) {
		this.crew = crew;
	}

}
