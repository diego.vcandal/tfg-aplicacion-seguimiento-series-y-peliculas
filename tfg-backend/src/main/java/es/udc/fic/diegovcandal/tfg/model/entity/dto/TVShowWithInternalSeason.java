package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.Season;

public class TVShowWithInternalSeason {

	private String originalTitle;
	private Season season;

	public TVShowWithInternalSeason(String originalTitle, Season season) {
		this.originalTitle = originalTitle;
		this.season = season;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public Season getSeason() {
		return season;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((originalTitle == null) ? 0 : originalTitle.hashCode());
		result = prime * result + ((season == null) ? 0 : season.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TVShowWithInternalSeason other = (TVShowWithInternalSeason) obj;
		if (originalTitle == null) {
			if (other.originalTitle != null)
				return false;
		} else if (!originalTitle.equals(other.originalTitle))
			return false;
		if (season == null) {
			if (other.season != null)
				return false;
		} else if (!season.equals(other.season))
			return false;
		return true;
	}

}
