package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class PersonCastSummaryDTO {

	private Long titleId;
	private String originalTitle;
	private String titleName;
	private short year;
	private String characterName;
	private short episodeCount;

	public PersonCastSummaryDTO(Long titleId, String originalTitle, String titleName, short year, String characterName,
			short episodeCount) {
		this.titleId = titleId;
		this.originalTitle = originalTitle;
		this.titleName = titleName;
		this.year = year;
		this.characterName = characterName;
		this.episodeCount = episodeCount;
	}

	public Long getTitleId() {
		return titleId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getTitleName() {
		return titleName;
	}

	public short getYear() {
		return year;
	}

	public String getCharacterName() {
		return characterName;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

}
