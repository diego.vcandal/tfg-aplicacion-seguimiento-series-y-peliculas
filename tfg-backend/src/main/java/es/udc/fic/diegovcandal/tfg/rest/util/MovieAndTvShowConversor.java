package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.Genre;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleImage;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateMovieParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateSeasonCastDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateTVShowParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.GenreDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.MovieDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TVShowDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TitleTraductionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.MovieExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;

public class MovieAndTvShowConversor {

	public static final MovieDTO toMovieDTO(TitleWithTraduction<Movie> titleWithTraduction, SourceType sourceType) {

		Movie movie = titleWithTraduction.getTitle();
		TitleTraduction traduction = titleWithTraduction.getTitleTraduction();

		boolean header = getHeaderImage(movie.getTitleImages());
		boolean portrait = getPortraitImage(movie.getTitleImages());

		List<GenreDTO> genres = movie.getGenres().stream().map(g -> toGenreDTO(g)).collect(Collectors.toList());

		MovieDTO movieDTO = new MovieDTO(movie.getId(), movie.getExternalId(), sourceType.getId(),
				movie.getOriginalTitle(), movie.getCompanyName(), movie.getOriginalDescription(), movie.getYear(),
				movie.getDuration(), null, null, header, portrait, movie.getAvgRating(), movie.getTimesRated());

		if (traduction != null) {
			movieDTO.setDescription(traduction.getDescription());
			movieDTO.setTitle(traduction.getTitleName());
		}

		movieDTO.setVisible(
				movie.getSourceType() == SourceType.INTERNAL ? movie.getVisibility() == Visibility.PUBLIC : true);

		movieDTO.setSetForDelete(movie.isSetForDelete());

		movieDTO.setGenres(genres);

		return movieDTO;
	}

	public static final MovieDTO toMovieDTO(Movie title) {

		boolean header = getHeaderImage(title.getTitleImages());
		boolean portrait = getPortraitImage(title.getTitleImages());

		List<GenreDTO> genres = title.getGenres().stream().map(g -> toGenreDTO(g)).collect(Collectors.toList());
		List<TitleTraductionDTO> traductions = title.getTitleTraductions().stream().map(tt -> toTitleTraductionDTO(tt))
				.collect(Collectors.toList());

		MovieDTO movieDTO = new MovieDTO(title.getId(), title.getExternalId(), title.getSourceType().getId(),
				title.getOriginalTitle(), title.getCompanyName(), title.getOriginalDescription(), title.getYear(),
				title.getDuration(), null, null, header, portrait, title.getAvgRating(), title.getTimesRated());

		movieDTO.setGenres(genres);
		movieDTO.setTraductions(traductions);

		movieDTO.setVisible(
				title.getSourceType() == SourceType.INTERNAL ? title.getVisibility() == Visibility.PUBLIC : true);

		movieDTO.setSetForDelete(title.isSetForDelete());

		return movieDTO;
	}

	public static final TVShowDTO toTVShowDTO(TitleWithTraduction<TVShow> titleWithTraduction, SourceType sourceType) {

		TVShow tvShow = titleWithTraduction.getTitle();
		TitleTraduction traduction = titleWithTraduction.getTitleTraduction();

		boolean header = getHeaderImage(tvShow.getTitleImages());
		boolean portrait = getPortraitImage(tvShow.getTitleImages());

		List<GenreDTO> genres = tvShow.getGenres().stream().map(g -> toGenreDTO(g)).collect(Collectors.toList());

		TVShowDTO tvShowDTO = new TVShowDTO(tvShow.getId(), tvShow.getExternalId(), sourceType.getId(),
				tvShow.getOriginalTitle(), tvShow.getCompanyName(), tvShow.getOriginalDescription(), tvShow.getYear(),
				tvShow.getTotalDuration(), tvShow.getEpisodeCount(), tvShow.getSeasonNumber(),
				toMillis(tvShow.getFirstAirDate()), toMillis(tvShow.getLastAirDate()), null, null, header, portrait,
				tvShow.getAvgRating(), tvShow.getTimesRated());

		tvShowDTO.setGenres(genres);

		tvShowDTO.setVisible(
				tvShow.getSourceType() == SourceType.INTERNAL ? tvShow.getVisibility() == Visibility.PUBLIC : true);

		tvShowDTO.setSetForDelete(tvShow.isSetForDelete());

		if (traduction != null) {
			tvShowDTO.setDescription(traduction.getDescription());
			tvShowDTO.setTitle(traduction.getTitleName());
		}

		return tvShowDTO;
	}

	public static final TVShowDTO toTVShowDTO(TVShow title) {

		boolean header = getHeaderImage(title.getTitleImages());
		boolean portrait = getPortraitImage(title.getTitleImages());

		List<GenreDTO> genres = title.getGenres().stream().map(g -> toGenreDTO(g)).collect(Collectors.toList());
		List<TitleTraductionDTO> traductions = title.getTitleTraductions().stream().map(tt -> toTitleTraductionDTO(tt))
				.collect(Collectors.toList());

		TVShowDTO tvShowDTO = new TVShowDTO(title.getId(), title.getExternalId(), title.getSourceType().getId(),
				title.getOriginalTitle(), title.getCompanyName(), title.getOriginalDescription(), title.getYear(),
				title.getTotalDuration(), title.getEpisodeCount(), title.getSeasonNumber(),
				toMillis(title.getFirstAirDate()), toMillis(title.getLastAirDate()), null, null, header, portrait,
				title.getAvgRating(), title.getTimesRated());

		tvShowDTO.setGenres(genres);
		tvShowDTO.setTraductions(traductions);

		tvShowDTO.setVisible(
				title.getSourceType() == SourceType.INTERNAL ? title.getVisibility() == Visibility.PUBLIC : true);

		tvShowDTO.setSetForDelete(title.isSetForDelete());

		List<CreateSeasonCastDTO> seasonList = new ArrayList<>();
		for (Season season : title.getSeasons()) {

			List<Long> episodes = new ArrayList<>();
			for (Episode episode : season.getEpisodes()) {
				episodes.add((long) episode.getEpisodeNumber());
			}

			seasonList.add(new CreateSeasonCastDTO(season.getSeasonNumber(), season.getEpisodeCount(), episodes));
		}

		tvShowDTO.setSeasons(seasonList);

		return tvShowDTO;
	}

	public static final MovieDTO fromExternalMovieToMovieDTO(MovieExternalDTO movieExternalDTO, boolean setForDelete,
			float avgRating, int ratingCount) {

		String header = movieExternalDTO.getBackdrop_path();
		String portrait = movieExternalDTO.getPoster_path();

		List<GenreDTO> genres = movieExternalDTO.getGenres().stream().map(g -> new GenreDTO(-1L, g.getId()))
				.collect(Collectors.toList());

		short year = movieExternalDTO.getRelease_date() == null || movieExternalDTO.getRelease_date().equals("") ? -1
				: Short.parseShort(movieExternalDTO.getRelease_date().substring(0, 4));

		String company = movieExternalDTO.getProduction_companies().size() > 0
				? movieExternalDTO.getProduction_companies().get(0).getName()
				: "";

		MovieDTO movieDTO = new MovieDTO(movieExternalDTO.getId(), movieExternalDTO.getExternalId(),
				SourceType.EXTERNAL_TMDB.getId(), movieExternalDTO.getOriginal_title(), company,
				movieExternalDTO.getOriginal_description(), year, (short) movieExternalDTO.getRuntime(), header,
				portrait, true, true, avgRating, ratingCount);

		movieDTO.setDescription(movieExternalDTO.getOverview());
		movieDTO.setTitle(movieExternalDTO.getTitle());

		movieDTO.setGenres(genres);

		movieDTO.setVisible(true);
		movieDTO.setSetForDelete(setForDelete);

		return movieDTO;
	}

	public static final TVShowDTO fromExternalTVShowToTVShowDTO(TVShowExternalDTO tvShowExternalDTO,
			boolean setForDelete, float avgRating, int ratingCount) {

		String header = tvShowExternalDTO.getBackdrop_path();
		String portrait = tvShowExternalDTO.getPoster_path();

		List<GenreDTO> genres = tvShowExternalDTO.getGenres().stream().map(g -> new GenreDTO(-1L, g.getId()))
				.collect(Collectors.toList());

		short year = tvShowExternalDTO.getFirst_air_date() == null || tvShowExternalDTO.getFirst_air_date().equals("")
				? -1
				: Short.parseShort(tvShowExternalDTO.getFirst_air_date().substring(0, 4));

		String company = tvShowExternalDTO.getProduction_companies().size() > 0
				? tvShowExternalDTO.getProduction_companies().get(0).getName()
				: "";

		Long runtime = tvShowExternalDTO.getEpisode_run_time().size() > 0
				? tvShowExternalDTO.getEpisode_run_time().get(0)
				: -1;

		TVShowDTO tvShowDTO = new TVShowDTO(tvShowExternalDTO.getId(), tvShowExternalDTO.getExternalId(),
				SourceType.EXTERNAL_TMDB.getId(), tvShowExternalDTO.getOriginal_name(), company,
				tvShowExternalDTO.getOriginal_description(), year, runtime.shortValue(),
				tvShowExternalDTO.getNumber_of_episodes(), (byte) tvShowExternalDTO.getNumber_of_seasons(),
				toMillisFromString(tvShowExternalDTO.getFirst_air_date()),
				toMillisFromString(tvShowExternalDTO.getLast_air_date()), header, portrait, true, true, avgRating,
				ratingCount);

		tvShowDTO.setTitle(tvShowExternalDTO.getName());
		tvShowDTO.setDescription(tvShowExternalDTO.getOverview());

		tvShowDTO.setVisible(true);
		tvShowDTO.setSetForDelete(setForDelete);

		tvShowDTO.setGenres(genres);

		return tvShowDTO;
	}

	public static final Movie toMovie(CreateMovieParamsDTO createMovieDTO) {

		Movie movie = new Movie(createMovieDTO.getExternalId(), SourceType.INTERNAL, createMovieDTO.getOriginalTitle(),
				createMovieDTO.getCompanyName(), createMovieDTO.getOriginalDescription(), createMovieDTO.getYear(),
				createMovieDTO.getDuration());

		if (createMovieDTO.getTraductions() != null) {
			movie.setTitleTraductions(toTitleTraductions(movie, createMovieDTO.getTraductions()));
		}

		Set<TitleImage> images = new HashSet<>();

		if (createMovieDTO.getHeaderImg() != null)
			images.add(toTitleImage(movie, createMovieDTO.getHeaderImg(), true, false));

		if (createMovieDTO.getPortraitImg() != null)
			images.add(toTitleImage(movie, createMovieDTO.getPortraitImg(), false, true));

		movie.setTitleImages(images);

		if (createMovieDTO.getGenres() != null) {
			movie.setGenres(toGenres(createMovieDTO.getGenres()));
		}

		return movie;
	}

	public static final TVShow toTVShow(CreateTVShowParamsDTO createTVShowDTO) {

		Title title = new Title(createTVShowDTO.getExternalId(), SourceType.INTERNAL, TitleType.TV_SHOW,
				createTVShowDTO.getOriginalTitle(), createTVShowDTO.getCompanyName(),
				createTVShowDTO.getOriginalDescription(), createTVShowDTO.getYear());

		TVShow tvShow = new TVShow(title, (short) 0, (short) 0, (byte) 0,
				toLocalDate(createTVShowDTO.getFirstAirDate()), toLocalDate(createTVShowDTO.getLastAirDate()));

		if (createTVShowDTO.getTraductions() != null) {
			tvShow.setTitleTraductions(toTitleTraductions(tvShow, createTVShowDTO.getTraductions()));
		}

		Set<TitleImage> images = new HashSet<>();

		if (createTVShowDTO.getHeaderImg() != null)
			images.add(toTitleImage(tvShow, createTVShowDTO.getHeaderImg(), true, false));

		if (createTVShowDTO.getPortraitImg() != null)
			images.add(toTitleImage(tvShow, createTVShowDTO.getPortraitImg(), false, true));

		tvShow.setTitleImages(images);

		if (createTVShowDTO.getGenres() != null) {
			tvShow.setGenres(toGenres(createTVShowDTO.getGenres()));
		}

		return tvShow;
	}

	private static final Set<TitleTraduction> toTitleTraductions(Title title, List<TitleTraductionDTO> traductions) {
		return traductions.stream().map(tt -> toTitleTraduction(title, tt)).collect(Collectors.toSet());
	}

	private static final Set<Genre> toGenres(List<GenreDTO> genres) {
		return genres.stream().map(g -> toGenre(g)).collect(Collectors.toSet());
	}

	private static final TitleImage toTitleImage(Title title, String image, boolean header, boolean portrait) {
		return new TitleImage(title, image.trim().equals("") ? null : image, portrait, header);
	}

	private static final TitleTraduction toTitleTraduction(Title title, TitleTraductionDTO titleTraductionDTO) {
		return new TitleTraduction(title, titleTraductionDTO.getIdLocale(), titleTraductionDTO.getTitleName(),
				titleTraductionDTO.getDescription());
	}

	private static final TitleTraductionDTO toTitleTraductionDTO(TitleTraduction titleTraduction) {
		return new TitleTraductionDTO(titleTraduction.getIdLocale(), titleTraduction.getTitleName(),
				titleTraduction.getDescription());
	}

	private static final Genre toGenre(GenreDTO genreDTO) {
		return new Genre(genreDTO.getId(), genreDTO.getExternalId(), null);
	}

	private static final GenreDTO toGenreDTO(Genre genre) {

		return new GenreDTO(genre.getId(), genre.getExternalId());
	}

	private static final boolean getHeaderImage(Set<TitleImage> set) {

		boolean hasImage = false;

		for (TitleImage titleImage : set) {
			if (titleImage.isHeader() && titleImage.getImage() != null) {
				hasImage = true;
			}
		}

		return hasImage;
	}

	private static final boolean getPortraitImage(Set<TitleImage> list) {

		boolean hasImage = false;

		for (TitleImage titleImage : list) {
			if (titleImage.isPortrait() && titleImage.getImage() != null) {
				hasImage = true;
			}
		}

		return hasImage;
	}

	private static final long toMillisFromString(String date) {

		if (date == null || date.trim().equals("")) {
			return 0;
		}

		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		return toMillis(LocalDate.parse(date, dtf));
	}

	private static final Long toMillis(LocalDate date) {

		if (date == null)
			return null;

		return date.atStartOfDay().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant()
				.toEpochMilli();
	}

	private static final LocalDate toLocalDate(Long date) {

		if (date == null)
			return null;

		return Instant.ofEpochMilli(date).atZone(ZoneOffset.systemDefault()).toLocalDate();
	}

}
