package es.udc.fic.diegovcandal.tfg.rest.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.EpisodeTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.SeasonTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.EpisodeWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalSeason;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateEpisodeParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateSeasonParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.EpisodeSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.EpisodeTraductionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.SeasonSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.SeasonTraductionDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.EpisodeSummaryExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.SeasonSummaryExternalDTO;

public class SeasonConversor {

	public static final List<SeasonSummaryDTO> toSeasonSummaryDTOsFromExternal(List<SeasonSummaryExternalDTO> seasons,
			List<SeasonWithTraduction> internalSessions) {

		List<SeasonSummaryDTO> list = new ArrayList<>();

		for (SeasonSummaryExternalDTO season : seasons) {

			Long internalId = -1L;

			Optional<SeasonWithTraduction> internal = internalSessions.stream()
					.filter(s -> s.getSeason().getExternalId().equals(season.getId())).findFirst();

			if (internal.isPresent())
				internalId = internal.get().getSeason().getId();

			short year = season.getAir_date() == null || season.getAir_date().equals("") ? -1
					: Short.parseShort(season.getAir_date().substring(0, 4));

			list.add(new SeasonSummaryDTO(internalId, season.getId(), (byte) season.getSeason_number(),
					season.getName(), season.getOverview(), year, season.getEpisode_count(), season.getName(),
					season.getOverview(), season.getPoster_path(), SourceType.EXTERNAL_TMDB.getId()));
		}

		return list;
	}

	public static final List<SeasonSummaryDTO> toSeasonSummaryDTOs(List<SeasonWithTraduction> seasonWithTraductions,
			SourceType sourceType) {

		List<SeasonSummaryDTO> list = new ArrayList<>();

		for (SeasonWithTraduction seasonWithTraduction : seasonWithTraductions) {

			list.add(toSeasonSummaryDTO(seasonWithTraduction, sourceType));

		}

		return list;
	}

	public static final SeasonSummaryDTO toSeasonSummaryDTOWithEpisodes(SeasonWithTraduction seasonWithTraduction,
			SourceType sourceType) {

		SeasonSummaryDTO summaryDTO = toSeasonSummaryDTO(seasonWithTraduction, sourceType);

		List<EpisodeSummaryDTO> list = new ArrayList<>();
		for (EpisodeWithTraduction ep : seasonWithTraduction.getEpisodes()) {
			list.add(toEpisodeSummaryDTO(ep));
		}
		summaryDTO.setEpisodes(list);

		return summaryDTO;
	}

	public static final SeasonSummaryDTO toSeasonSummaryDTOWithEpisodesFromExternal(SeasonSummaryExternalDTO season,
			SeasonWithTraduction seasonWithTraduction) {

		Season seasonInternal = seasonWithTraduction.getSeason();

		short year = season.getAir_date() == null || season.getAir_date().equals("") ? -1
				: Short.parseShort(season.getAir_date().substring(0, 4));

		SeasonSummaryDTO seasonSummaryDTO = new SeasonSummaryDTO(seasonInternal.getId(), season.getId(),
				(byte) season.getSeason_number(), season.getName(), season.getOverview(), year,
				season.getEpisode_count(), season.getName(), season.getOverview(), season.getPoster_path(),
				SourceType.EXTERNAL_TMDB.getId());

		List<EpisodeSummaryDTO> list = new ArrayList<>();

		for (EpisodeSummaryExternalDTO episode : season.getEpisodes()) {

			Long internalId = -1L;

			Optional<EpisodeWithTraduction> internal = seasonWithTraduction.getEpisodes().stream()
					.filter(s -> s.getEpisode().getExternalId().equals(episode.getId())).findFirst();

			if (internal.isPresent()) {
				internalId = internal.get().getEpisode().getId();
			}

			list.add(new EpisodeSummaryDTO(internalId, episode.getId(), episode.getEpisode_number(), episode.getName(),
					episode.getOverview(), toMillisFromString(episode.getAir_date()), (short) 0,
					episode.getStill_path(), seasonSummaryDTO.getSourceType()));

		}

		seasonSummaryDTO.setEpisodes(list);

		return seasonSummaryDTO;

	}

	public static final SeasonSummaryDTO toSeasonSummaryDTOWithEpisodesFromExternal(SeasonSummaryExternalDTO season) {

		short year = season.getAir_date() == null || season.getAir_date().equals("") ? -1
				: Short.parseShort(season.getAir_date().substring(0, 4));

		SeasonSummaryDTO seasonSummaryDTO = new SeasonSummaryDTO(-1L, season.getId(), (byte) season.getSeason_number(),
				season.getName(), season.getOverview(), year, season.getEpisode_count(), season.getName(),
				season.getOverview(), season.getPoster_path(), SourceType.EXTERNAL_TMDB.getId());

		List<EpisodeSummaryDTO> list = new ArrayList<>();

		for (EpisodeSummaryExternalDTO episode : season.getEpisodes()) {

			Long internalId = -1L;

			list.add(new EpisodeSummaryDTO(internalId, episode.getId(), episode.getEpisode_number(), episode.getName(),
					episode.getOverview(), toMillisFromString(episode.getAir_date()), (short) 0,
					episode.getStill_path(), seasonSummaryDTO.getSourceType()));

		}

		seasonSummaryDTO.setEpisodes(list);

		return seasonSummaryDTO;

	}

	public static final Season toSeason(CreateSeasonParamsDTO season) {

		Season newSeason = new Season(season.getExternalId(), null, season.getSeasonNumber(), season.getOriginalTitle(),
				season.getOriginalDescription(), toLocalDate(season.getFirstAirDate()),
				toLocalDate(season.getLastAirDate()), season.getImageContent(), (short) 0, (short) 0, null);

		Set<SeasonTraduction> seasonTraductions = new HashSet<>();

		for (SeasonTraductionDTO seasonTraduction : season.getTraductions()) {
			seasonTraductions.add(new SeasonTraduction(newSeason, seasonTraduction.getIdLocale(),
					seasonTraduction.getTitle(), seasonTraduction.getDescription()));
		}

		newSeason.setSeasonTraductions(seasonTraductions);

		return newSeason;
	}

	public static final Episode toEpisode(CreateEpisodeParamsDTO episode) {

		Episode newEpisode = new Episode(episode.getExternalId(), null, episode.getEpisodeNumber(),
				episode.getOriginalTitle(), episode.getOriginalDescription(), toLocalDate(episode.getAirDate()),
				episode.getTotalDuration(), episode.getImageContent());

		Set<EpisodeTraduction> episodeTraductions = new HashSet<>();

		for (EpisodeTraductionDTO episodeTraduction : episode.getTraductions()) {
			episodeTraductions.add(new EpisodeTraduction(newEpisode, episodeTraduction.getIdLocale(),
					episodeTraduction.getTitle(), episodeTraduction.getDescription()));
		}

		newEpisode.setEpisodeTraductions(episodeTraductions);

		return newEpisode;
	}

	public static final SeasonSummaryDTO toUpdateSeasonDTO(TVShowWithInternalSeason tvShowWithInternalSeason) {

		Season season = tvShowWithInternalSeason.getSeason();

		boolean image = season.getImage() != null;

		List<SeasonTraductionDTO> traductions = season.getSeasonTraductions().stream()
				.map(tt -> toSeasonTraductionDTO(tt)).collect(Collectors.toList());

		SeasonSummaryDTO seasonSummaryDTO = new SeasonSummaryDTO(season.getId(), season.getExternalId(),
				season.getSeasonNumber(), season.getOriginalTitle(), season.getOriginalDescription(), image,
				season.isSetForDelete(), toMillis(season.getFirstAirDate()), toMillis(season.getLastAirDate()),
				traductions);

		seasonSummaryDTO.setSourceType(season.getSourceType().getId());
		seasonSummaryDTO.setTvshowOriginalTitle(tvShowWithInternalSeason.getOriginalTitle());

		return seasonSummaryDTO;
	}

	public static final EpisodeSummaryDTO toUpdateEpisodeDTO(TVShowWithInternalEpisode tvShowWithInternalEpisode) {

		Episode episode = tvShowWithInternalEpisode.getEpisode();

		boolean image = episode.getImage() != null;

		List<EpisodeTraductionDTO> traductions = episode.getEpisodeTraductions().stream()
				.map(tt -> toEpisodeTraductionDTO(tt)).collect(Collectors.toList());

		EpisodeSummaryDTO episodeSummaryDTO = new EpisodeSummaryDTO(episode.getId(), episode.getExternalId(),
				episode.getEpisodeNumber(), episode.getOriginalTitle(), episode.getOriginalDescription(),
				toMillis(episode.getAirDate()), image, episode.isSetForDelete(), traductions);

		episodeSummaryDTO.setSourceType(episode.getSourceType().getId());
		episodeSummaryDTO.setSeasonOriginalTitle(tvShowWithInternalEpisode.getSeasonOriginalTitle());
		episodeSummaryDTO.setTvshowOriginalTitle(tvShowWithInternalEpisode.getOriginalTitle());

		return episodeSummaryDTO;
	}

	private static final SeasonTraductionDTO toSeasonTraductionDTO(SeasonTraduction seasonTraduction) {
		return new SeasonTraductionDTO(seasonTraduction.getIdLocale(), seasonTraduction.getTitle(),
				seasonTraduction.getDescription());
	}

	private static final EpisodeTraductionDTO toEpisodeTraductionDTO(EpisodeTraduction episodeTraduction) {
		return new EpisodeTraductionDTO(episodeTraduction.getIdLocale(), episodeTraduction.getTitle(),
				episodeTraduction.getDescription());
	}

	private static final SeasonSummaryDTO toSeasonSummaryDTO(SeasonWithTraduction seasonWithTraduction,
			SourceType sourceType) {

		Season season = seasonWithTraduction.getSeason();
		SeasonTraduction traduction = seasonWithTraduction.getSeasonTraduction();

		SeasonSummaryDTO summaryDTO = new SeasonSummaryDTO(season.getId(), season.getExternalId(),
				season.getSeasonNumber(), season.getOriginalTitle(), season.getOriginalDescription(),
				season.getFirstAirDate() != null ? (short) season.getFirstAirDate().getYear() : (short) 0,
				season.getEpisodeCount(), null, null, season.getImage() != null, sourceType.getId());

		if (traduction != null) {
			summaryDTO.setDescription(traduction.getDescription());
			summaryDTO.setTitle(traduction.getTitle());
		}

		summaryDTO.setSetForDelete(season.isSetForDelete());

		return summaryDTO;

	}

	private static final EpisodeSummaryDTO toEpisodeSummaryDTO(EpisodeWithTraduction episode) {

		Episode ep = episode.getEpisode();
		EpisodeTraduction et = episode.getEpisodeTraduction();

		EpisodeSummaryDTO episodeSummaryDTO = new EpisodeSummaryDTO(ep.getId(), ep.getExternalId(),
				ep.getEpisodeNumber(), ep.getOriginalTitle(), ep.getOriginalDescription(), toMillis(ep.getAirDate()),
				ep.getTotalDuration(), ep.getImage() != null, SourceType.INTERNAL.getId());

		if (et != null) {
			episodeSummaryDTO.setDescription(et.getDescription());
			episodeSummaryDTO.setTitle(et.getTitle());
		}

		episodeSummaryDTO.setSetForDelete(ep.isSetForDelete());

		return episodeSummaryDTO;

	}

	private static final long toMillisFromString(String date) {

		if (date == null || date.trim().equals("")) {
			return 0;
		}

		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		return toMillis(LocalDate.parse(date, dtf));
	}

	private static final Long toMillis(LocalDate date) {

		if (date == null)
			return null;

		return date.atStartOfDay().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant()
				.toEpochMilli();
	}

	private static final LocalDate toLocalDate(Long date) {

		if (date == null)
			return null;

		return Instant.ofEpochMilli(date).atZone(ZoneOffset.systemDefault()).toLocalDate();
	}

}
