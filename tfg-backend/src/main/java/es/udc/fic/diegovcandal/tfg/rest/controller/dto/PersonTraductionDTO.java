package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PersonTraductionDTO {

	private Long idLocale;
	private String name;
	private String biography;
	private String birthPlace;
	private boolean originalLanguage;

	public PersonTraductionDTO(Long idLocale, String name, String biography, String birthPlace,
			boolean originalLanguage) {
		this.idLocale = idLocale;
		this.name = name;
		this.biography = biography;
		this.birthPlace = birthPlace;
		this.originalLanguage = originalLanguage;
	}

	@NotNull()
	@Min(0)
	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	@NotNull()
	@Size(min = 1, max = 70)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Size(min = 0, max = 500)
	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	@NotNull()
	public boolean isOriginalLanguage() {
		return originalLanguage;
	}

	public void setOriginalLanguage(boolean originalLanguage) {
		this.originalLanguage = originalLanguage;
	}

}
