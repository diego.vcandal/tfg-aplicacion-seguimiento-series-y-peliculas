package es.udc.fic.diegovcandal.tfg.model.service;

import java.time.LocalDate;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingList;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface FollowingListService {

	TranslatedFollowingList getPersonalList(Long userId, Long localeId) throws UserNotFoundException;

	TranslatedFollowingList getPublicList(Long ownerId, Long localeId)
			throws UserNotFoundException, PrivateResourceException;

	void changeListVisbility(Long userId) throws UserNotFoundException;

	void addToList(Long userId, Long titleId, Long status)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException;

	void editTitleFromList(Long userId, Long titleId, LocalDate startDate, LocalDate endDate, Long status, Byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException;

	void deleteTitleFromList(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException;

	FollowingListTitle getFollowingListTitleInfo(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException;

}
