package es.udc.fic.diegovcandal.tfg.rest.controller;

import static es.udc.fic.diegovcandal.tfg.rest.util.FollowingListConversor.toFollowingListTitleDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.fromExternalMovieToMovieDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.fromExternalTVShowToTVShowDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.toMovie;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.toMovieDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.toTVShow;
import static es.udc.fic.diegovcandal.tfg.rest.util.MovieAndTvShowConversor.toTVShowDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.ReviewTitleConversor.toReviewTitleDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.ReviewTitleConversor.toReviewTitleDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toEpisode;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeason;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeasonSummaryDTOWithEpisodes;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeasonSummaryDTOWithEpisodesFromExternal;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeasonSummaryDTOs;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toSeasonSummaryDTOsFromExternal;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toUpdateEpisodeDTO;
import static es.udc.fic.diegovcandal.tfg.rest.util.SeasonConversor.toUpdateSeasonDTO;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithSeasons;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.GenreDoesntExistException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ReviewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.restclient.TmdbAPIClient;
import es.udc.fic.diegovcandal.tfg.model.service.FollowingListService;
import es.udc.fic.diegovcandal.tfg.model.service.ImageService;
import es.udc.fic.diegovcandal.tfg.model.service.RatingService;
import es.udc.fic.diegovcandal.tfg.model.service.TitleService;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.BlockDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateEpisodeParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateMovieParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateSeasonParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.CreateTVShowParamsDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.EditFollowingListItemDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.EpisodeSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ErrorDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.FollowingListTitleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.IdDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.MovieDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.RatingDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ReviewDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.ReviewTitleDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.SeasonSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.dto.TVShowDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.TVShowExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.util.Util;

@RestController
@RequestMapping("/titles")
public class TitleController {

	private static final String GENRE_NOT_EXISTS_EXCEPTION_ID = "exceptions.GenreDoesntExistException";

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private TitleService titleService;

	@Autowired
	private TmdbAPIClient tmdbAPIClient;

	@Autowired
	private ImageService imageService;

	@Autowired
	private RatingService ratingService;

	@Autowired
	private FollowingListService followingListService;

	@Value("${project.external.api.enabled}")
	private boolean apiEnabled;

	@ExceptionHandler(GenreDoesntExistException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorDTO handleGenreDoesntExistException(GenreDoesntExistException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(GENRE_NOT_EXISTS_EXCEPTION_ID, null,
				GENRE_NOT_EXISTS_EXCEPTION_ID, locale);

		return new ErrorDTO(errorMessage);

	}

	@GetMapping("/movies/{id}")
	public MovieDTO getMovieDetails(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		TitleWithTraduction<Movie> titleWithTraduction = titleService.getMovieDetails(userId, id, localeId);

		Movie movie = titleWithTraduction.getTitle();

		if (apiEnabled && movie.getSourceType() != SourceType.INTERNAL)
			return fromExternalMovieToMovieDTO(
					tmdbAPIClient.findCachedMovie(movie.getExternalId(), movie.getId(), acceptLanguage),
					movie.isSetForDelete(), movie.getAvgRating(), movie.getTimesRated());

		return toMovieDTO(titleWithTraduction, SourceType.INTERNAL);

	}

	@GetMapping("/tvshows/{id}")
	public TVShowDTO getTVShowDetails(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		TitleWithTraduction<TVShow> titleWithTraduction = titleService.getTVShowDetails(userId, id, localeId);

		TVShow tvShow = titleWithTraduction.getTitle();

		if (apiEnabled && tvShow.getSourceType() != SourceType.INTERNAL)
			return fromExternalTVShowToTVShowDTO(
					tmdbAPIClient.findCachedTVShow(tvShow.getExternalId(), tvShow.getId(), acceptLanguage),
					tvShow.isSetForDelete(), tvShow.getAvgRating(), tvShow.getTimesRated());

		return toTVShowDTO(titleWithTraduction, SourceType.INTERNAL);

	}

	@GetMapping("/tvshows/{id}/seasons")
	public List<SeasonSummaryDTO> getTVShowSeasons(@RequestAttribute(required = false) Long userId,
			@PathVariable Long id, @RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		TVShowWithSeasons tvShowWithSeasons = titleService.getSeasons(userId, id, localeId);

		if (apiEnabled && tvShowWithSeasons.getTvShow().getSourceType() != SourceType.INTERNAL) {

			TVShowExternalDTO tvShowExternalDTO = tmdbAPIClient.findCachedTVShow(
					tvShowWithSeasons.getTvShow().getExternalId(), tvShowWithSeasons.getTvShow().getId(),
					acceptLanguage);

			return toSeasonSummaryDTOsFromExternal(tvShowExternalDTO.getSeasons(), tvShowWithSeasons.getSeasons());
		}

		return toSeasonSummaryDTOs(tvShowWithSeasons.getSeasons(), SourceType.INTERNAL);

	}

	@GetMapping("/tvshows/{id}/seasons/{seasonNumber}")
	public SeasonSummaryDTO getSeasonAndEpisodesDetails(@RequestAttribute(required = false) Long userId,
			@PathVariable Long id, @PathVariable byte seasonNumber,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		SeasonWithTraduction seasonWithTraduction = titleService.getFullSeason(userId, id, seasonNumber, localeId);

		if (seasonWithTraduction.isNoContent())
			return toSeasonSummaryDTOWithEpisodesFromExternal(
					tmdbAPIClient.findExternalSeason(id, seasonNumber, acceptLanguage));

		TVShow tvShow = seasonWithTraduction.getSeason().getTvShow();

		if (apiEnabled && tvShow.getSourceType() != SourceType.INTERNAL)
			return toSeasonSummaryDTOWithEpisodesFromExternal(
					tmdbAPIClient.findExternalSeason(tvShow.getExternalId(), seasonNumber, acceptLanguage),
					seasonWithTraduction);

		return toSeasonSummaryDTOWithEpisodes(seasonWithTraduction, SourceType.INTERNAL);

	}

	@GetMapping("/tvshows/{id}/lastSeason")
	public List<SeasonSummaryDTO> getTVShowLastSeason(@RequestAttribute(required = false) Long userId,
			@PathVariable Long id, @RequestHeader(value = "Accept-Language", required = false) String acceptLanguage)
			throws TitleNotFoundException, UserNotFoundException {

		Long localeId = LocaleType.valueOfLanguageOrDefault(acceptLanguage);

		TVShowWithSeasons tvShowWithSeasons = titleService.getLastSeasonPreview(userId, id, localeId);

		if (apiEnabled && tvShowWithSeasons.getTvShow().getSourceType() != SourceType.INTERNAL) {

			TVShowExternalDTO tvShowExternalDTO = tmdbAPIClient
					.findExternalTVShow(tvShowWithSeasons.getTvShow().getExternalId(), acceptLanguage);

			return toSeasonSummaryDTOsFromExternal(tvShowExternalDTO.getSeasons().stream()
					.filter(s -> s.getSeason_number() == tvShowExternalDTO.getNumber_of_seasons())
					.collect(Collectors.toList()), tvShowWithSeasons.getSeasons());

		}

		return toSeasonSummaryDTOs(tvShowWithSeasons.getSeasons(), SourceType.INTERNAL);

	}

	@GetMapping("/{id}/images/portrait")
	public void findPortraitTitleImage(@PathVariable Long id, HttpServletResponse response)
			throws TitleNotFoundException {

		String image = imageService.getPortraitImage(id);

		Util.sendImage(response, image);

	}

	@GetMapping("/{id}/images/header")
	public void findHeaderTitleImage(@PathVariable Long id, HttpServletResponse response)
			throws TitleNotFoundException {

		String image = imageService.getHeaderImage(id);

		Util.sendImage(response, image);

	}

	@GetMapping("/{id}/externalImage")
	public void findExternalImage(@PathVariable Long id, HttpServletResponse response) throws TitleNotFoundException {

		Title title = titleService.getTitleById(id);

		if (title == null) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}

		String image = title.getExternalImage();

		if (image == null || image.equals("")) {

			if (title.getExternalId() == null) {
				response.setStatus(HttpStatus.NOT_FOUND.value());
				return;
			}

			image = title.getTitleType() == TitleType.MOVIE
					? tmdbAPIClient.findExternalMovie(title.getExternalId(), LocaleType.DEFAULT_LOCALE.toString())
							.getPoster_path()
					: tmdbAPIClient.findExternalTVShow(title.getExternalId(), LocaleType.DEFAULT_LOCALE.toString())
							.getPoster_path();
		}

		byte[] decoded = tmdbAPIClient.getExternalPortraitImage(image);

		InputStream inputStream = new ByteArrayInputStream(decoded);

		try {
			IOUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@GetMapping("/{id}/seasons/{seasonNumber}/image")
	public void findSeasonImage(@PathVariable Long id, @PathVariable byte seasonNumber, HttpServletResponse response)
			throws TitleNotFoundException {

		String image = imageService.getSeasonImage(id, seasonNumber);

		Util.sendImage(response, image);

	}

	@GetMapping("/{id}/seasons/{seasonNumber}/episodes/{episodeNumber}/image")
	public void findEpisodeImage(@PathVariable Long id, @PathVariable byte seasonNumber,
			@PathVariable short episodeNumber, HttpServletResponse response) throws TitleNotFoundException {

		String image = imageService.getEpisodeImage(id, seasonNumber, episodeNumber);

		Util.sendImage(response, image);

	}

	@PostMapping("/movies/create")
	public IdDTO createMovie(@RequestAttribute Long userId, @Validated @RequestBody CreateMovieParamsDTO movieDTO)
			throws UserNotFoundException, ExternalIdAlreadyLinkedException, PermissionException,
			GenreDoesntExistException {

		Movie movie = toMovie(movieDTO);

		return new IdDTO(titleService.createMovie(userId, movie));
	}

	@PostMapping("/tvshows/create")
	public IdDTO createTVShow(@RequestAttribute Long userId, @Validated @RequestBody CreateTVShowParamsDTO tvShowDTO)
			throws UserNotFoundException, ExternalIdAlreadyLinkedException, PermissionException,
			GenreDoesntExistException {

		TVShow tvShow = toTVShow(tvShowDTO);

		return new IdDTO(titleService.createTVShow(userId, tvShow));

	}

	@PutMapping("/movies/{id}/update")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void uptadeMovie(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody CreateMovieParamsDTO movieDTO)
			throws UserNotFoundException, TitleNotFoundException, PermissionException, GenreDoesntExistException {

		Movie movie = toMovie(movieDTO);
		titleService.updateMovie(userId, id, movie);

	}

	@PutMapping("/tvshows/{id}/update")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void uptadeTVShow(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody CreateTVShowParamsDTO tvShowDTO)
			throws UserNotFoundException, TitleNotFoundException, PermissionException, GenreDoesntExistException {

		TVShow tvShow = toTVShow(tvShowDTO);
		titleService.updateTVShow(userId, id, tvShow);

	}

	@PostMapping("/{id}/makePublic")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void makeTitlePublic(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {
		titleService.makeTitlePublic(userId, id);
	}

	@PostMapping("/{id}/setForDeletion")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void setTitleForDeletion(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {
		titleService.setTitleForDeletion(userId, id, true);
	}

	@DeleteMapping("/{id}/delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTitle(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {
		titleService.deleteTitle(userId, id);
	}

	@PostMapping("/{id}/changeSourceType")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changeSourceType(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {
		titleService.changeSourceType(userId, id);
	}

	@GetMapping("/internal/movies/{id}")
	public MovieDTO getInternalMovieDetails(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		Movie title = titleService.getInternalMovieDetails(id, userId);

		return toMovieDTO(title);

	}

	@GetMapping("/internal/tvshows/{id}")
	public TVShowDTO getInternalTVShow(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		TVShow title = titleService.getInternalTVShowDetails(id, userId);

		return toTVShowDTO(title);

	}

	@PostMapping("/tvshows/{id}/seasons")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createSeason(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody CreateSeasonParamsDTO season)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		titleService.createSeason(userId, id, toSeason(season));
	}

	@PostMapping("/tvshows/{id}/seasons/{seasonNumber}/episodes")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createEpisode(@RequestAttribute(required = false) Long userId, @PathVariable Long id,
			@PathVariable byte seasonNumber,
			@RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
			@Validated @RequestBody CreateEpisodeParamsDTO episode)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		titleService.createEpisode(userId, id, seasonNumber, toEpisode(episode));

	}

	@PutMapping("/seasons/{id}/update")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void uptadeSeason(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody CreateSeasonParamsDTO createSeasonParamsDTO)
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		titleService.updateSeason(userId, id, toSeason(createSeasonParamsDTO));

	}

	@PutMapping("/episodes/{id}/update")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void uptadeEpisode(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody CreateEpisodeParamsDTO createEpisodeParamsDTO)
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		titleService.updateEpisode(userId, id, toEpisode(createEpisodeParamsDTO));

	}

	@PostMapping("/seasons/{id}/setForDeletion")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void setSeasonForDeletion(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {
		titleService.setSeasonForDeletion(userId, id, true);
	}

	@PostMapping("/episodes/{id}/setForDeletion")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void setEpisodeForDeletion(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {
		titleService.setEpisodeForDeletion(userId, id, true);
	}

	@GetMapping("/internal/seasons/{id}")
	public SeasonSummaryDTO getInternalSeason(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		return toUpdateSeasonDTO(titleService.getInternalSeasonDetails(id, userId));

	}

	@GetMapping("/internal/episodes/{id}")
	public EpisodeSummaryDTO getInternalEpisode(@PathVariable Long id, @RequestAttribute Long userId)
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		return toUpdateEpisodeDTO(titleService.getInternalEpisodeDetails(id, userId));

	}

	@PostMapping("/{id}/rate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void rateTitle(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody RatingDTO ratingDTO) throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {
		ratingService.rateTitle(userId, id, ratingDTO.getRating());
	}

	@PutMapping("/{id}/rate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void editTitleRating(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody RatingDTO ratingDTO) throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException, ReviewNotFoundException {
		ratingService.editTitleRating(userId, id, ratingDTO.getRating());
	}

	@DeleteMapping("/{id}/rate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTitleRating(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException {
		ratingService.deleteTitleRating(userId, id);
	}

	@PostMapping("/{id}/likes")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addLike(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, IncorrectOperationException, ReviewNotFoundException {
		ratingService.addLike(userId, id);
	}

	@DeleteMapping("/{id}/likes")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeLike(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, IncorrectOperationException, ReviewNotFoundException {
		ratingService.removeLike(userId, id);
	}

	@PostMapping("/{id}/reviews")
	public ReviewTitleDTO addNewReview(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody ReviewDTO reviewDTO)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		return toReviewTitleDTO(ratingService.addNewReview(userId, id, reviewDTO.getReviewTitle(),
				reviewDTO.getContent(), reviewDTO.getRating()));
	}

	@PutMapping("/{id}/reviews")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void editReview(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody ReviewDTO reviewDTO)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		ratingService.editReview(userId, id, reviewDTO.getReviewTitle(), reviewDTO.getContent(), reviewDTO.getRating());
	}

	@DeleteMapping("/{id}/reviews")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteReview(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {
		ratingService.deleteReview(userId, id);
	}

	@GetMapping("/{id}/reviews")
	public BlockDTO<ReviewTitleDTO> getReviews(@PathVariable Long id, @RequestAttribute(required = false) Long userId,
			@RequestParam(defaultValue = "0") int page) throws TitleNotFoundException, UserNotFoundException {

		Block<ReviewTitle> block = ratingService.getReviews(id, userId, page, 10);

		return new BlockDTO<>(toReviewTitleDTOs(block.getItems()), block.getExistMoreItems(), block.getTotalPages(),
				block.getTotalElements());
	}

	@GetMapping("/{id}/my-review")
	public ReviewTitleDTO getReview(@PathVariable Long id, @RequestAttribute(required = false) Long userId,
			HttpServletResponse response) throws TitleNotFoundException, UserNotFoundException {

		if (userId == null) {
			return null;
		}

		ReviewTitle review = ratingService.getReviewTitle(userId, id);

		return review == null ? null : toReviewTitleDTO(review);
	}

	@PostMapping("/{id}/following-add")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addToList(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody EditFollowingListItemDTO editDTO)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		followingListService.addToList(userId, id, editDTO.getStatus());
	}

	@PutMapping("/{id}/following-edit")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void editTitleFromList(@PathVariable Long id, @RequestAttribute Long userId,
			@Validated @RequestBody EditFollowingListItemDTO editDTO) throws UserNotFoundException,
			TitleNotFoundException, IncorrectOperationException, IncorrectRatingException {

		followingListService.editTitleFromList(userId, id, toLocalDate(editDTO.getDateStarted()),
				toLocalDate(editDTO.getDateFinalized()), editDTO.getStatus(), editDTO.getRating());
	}

	@DeleteMapping("/{id}/following-delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTitleFromList(@PathVariable Long id, @RequestAttribute Long userId)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {
		followingListService.deleteTitleFromList(userId, id);
	}

	@GetMapping("/{id}/following-list-info")
	public FollowingListTitleDTO getFollowingListTitleInfo(@RequestAttribute Long userId, @PathVariable Long id)
			throws UserNotFoundException, TitleNotFoundException {

		FollowingListTitle followingListTitle = followingListService.getFollowingListTitleInfo(userId, id);

		return followingListTitle == null ? null : toFollowingListTitleDTO(followingListTitle);
	}

	private static final LocalDate toLocalDate(Long date) {

		if (date == null)
			return null;

		return Instant.ofEpochMilli(date).atZone(ZoneOffset.systemDefault()).toLocalDate();
	}
}
