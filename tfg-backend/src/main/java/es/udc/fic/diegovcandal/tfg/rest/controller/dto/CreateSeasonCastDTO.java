package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class CreateSeasonCastDTO {

	private byte seasonNumber;
	private short episodeNumber;
	private List<Long> episodes;

	public CreateSeasonCastDTO() {
	}

	public CreateSeasonCastDTO(byte seasonNumber, List<Long> episodes) {
		this.seasonNumber = seasonNumber;
		this.episodes = episodes;
	}

	public CreateSeasonCastDTO(byte seasonNumber, short episodeNumber, List<Long> episodes) {
		this.seasonNumber = seasonNumber;
		this.episodeNumber = episodeNumber;
		this.episodes = episodes;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public List<Long> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<Long> episodes) {
		this.episodes = episodes;
	}

	public short getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(short episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

}
