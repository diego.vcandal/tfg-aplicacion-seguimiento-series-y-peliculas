package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

public class ReviewTitleDTO {

	private Long id;
	private Long titleId;
	private Long userId;
	private String userName;
	private byte rating;
	private boolean containsReview;
	private String title;
	private String content;
	private int likeCount;
	private boolean liked;
	private Long addedDate;

	public ReviewTitleDTO(Long id, Long titleId, Long userId, String userName, byte rating, boolean containsReview,
			String title, String content, int likeCount, Long addedDate, boolean liked) {
		this.id = id;
		this.titleId = titleId;
		this.userId = userId;
		this.rating = rating;
		this.userName = userName;
		this.containsReview = containsReview;
		this.title = title;
		this.content = content;
		this.likeCount = likeCount;
		this.addedDate = addedDate;
		this.liked = liked;
	}

	public Long getId() {
		return id;
	}

	public Long getTitleId() {
		return titleId;
	}

	public Long getUserId() {
		return userId;
	}

	public byte getRating() {
		return rating;
	}

	public boolean isContainsReview() {
		return containsReview;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public Long getAddedDate() {
		return addedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setRating(byte rating) {
		this.rating = rating;
	}

	public void setContainsReview(boolean containsReview) {
		this.containsReview = containsReview;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public void setAddedDate(Long addedDate) {
		this.addedDate = addedDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

}
