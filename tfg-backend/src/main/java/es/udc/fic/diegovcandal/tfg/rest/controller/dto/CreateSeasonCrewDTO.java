package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class CreateSeasonCrewDTO {

	private byte seasonNumber;
	private List<Long> episodes;

	public CreateSeasonCrewDTO(byte seasonNumber, List<Long> episodes) {
		this.seasonNumber = seasonNumber;
		this.episodes = episodes;
	}

	public byte getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(byte seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public List<Long> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<Long> episodes) {
		this.episodes = episodes;
	}

}
