package es.udc.fic.diegovcandal.tfg.model.service;

import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectLoginException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectPasswordException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface UserService {

	void signUp(User user) throws UserAlreadyExistsException;

	User loginFromUserName(String userName, String password) throws IncorrectLoginException;

	User loginFromId(Long id) throws UserNotFoundException;

	User findUser(Long id) throws UserNotFoundException;

	User updateProfile(User user, boolean imageChanged) throws UserNotFoundException, UserAlreadyExistsException;

	void deleteProfile(Long id, String password) throws UserNotFoundException, IncorrectPasswordException;

	void changePassword(Long id, String oldPassword, String newPassword)
			throws UserNotFoundException, IncorrectPasswordException;

}
