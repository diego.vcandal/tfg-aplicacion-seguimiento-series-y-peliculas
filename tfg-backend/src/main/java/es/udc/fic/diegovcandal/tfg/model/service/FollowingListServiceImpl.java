package es.udc.fic.diegovcandal.tfg.model.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.FollowingListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.FollowingListTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.ReviewTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TranslatedFollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.FollowingStatus;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@Service
@Transactional
public class FollowingListServiceImpl implements FollowingListService {

	@Autowired
	private FollowingListDAO followingListDAO;

	@Autowired
	private FollowingListTitleDAO followingListTitleDAO;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private ReviewTitleDAO reviewTitleDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	@Transactional(readOnly = true)
	public TranslatedFollowingList getPersonalList(Long userId, Long localeId) throws UserNotFoundException {

		User user = permissionManager.findUser(userId);

		FollowingList list = user.getFollowingList();
		List<TranslatedFollowingListTitle> titles = followingListTitleDAO.findFollowingListTranslatedTitles(list,
				localeId);

		return toTranslatedFollowingList(list, titles);

	}

	@Override
	@Transactional(readOnly = true)
	public TranslatedFollowingList getPublicList(Long ownerId, Long localeId)
			throws UserNotFoundException, PrivateResourceException {

		User user = permissionManager.findUser(ownerId);

		FollowingList list = user.getFollowingList();

		if (list.getVisibility() == Visibility.PRIVATE)
			throw new PrivateResourceException();

		List<TranslatedFollowingListTitle> titles = followingListTitleDAO.findFollowingListTranslatedTitles(list,
				localeId);

		return toTranslatedFollowingList(list, titles);

	}

	@Override
	public void changeListVisbility(Long userId) throws UserNotFoundException {

		User user = permissionManager.findUser(userId);

		FollowingList list = user.getFollowingList();
		list.setVisibility(list.getVisibility() == Visibility.PUBLIC ? Visibility.PRIVATE : Visibility.PUBLIC);

		followingListDAO.save(list);

	}

	@Override
	public void addToList(Long userId, Long titleId, Long status)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = permissionManager.findUser(userId);

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		FollowingList list = user.getFollowingList();

		Optional<FollowingListTitle> listTitleOptional = followingListTitleDAO.findByTitleIdAndListId(title.get(),
				list);

		if (listTitleOptional.isPresent()) {
			throw new IncorrectOperationException();
		}

		Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user);

		list.getTitles().add(new FollowingListTitle(title.get(), list, FollowingStatus.valueOfStatusOrDefault(status),
				LocalDateTime.now(), null, null, reviewTitle.isPresent() ? reviewTitle.get().getRating() : (byte) -1,
				reviewTitle.isPresent() ? reviewTitle.get().isContainsReview() : false));

		if (title.get().getTitleType() == TitleType.MOVIE)
			list.setMovieCount((short) (list.getMovieCount() + 1));
		else
			list.setTvshowCount((short) (list.getTvshowCount() + 1));

		list.setLastUpdated(LocalDateTime.now());

		followingListDAO.save(list);

	}

	@Override
	public void editTitleFromList(Long userId, Long titleId, LocalDate startDate, LocalDate endDate, Long status,
			Byte rating) throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = permissionManager.findUser(userId);

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		FollowingList list = user.getFollowingList();

		Optional<FollowingListTitle> listTitleOptional = followingListTitleDAO.findByTitleIdAndListId(title.get(),
				list);

		if (!listTitleOptional.isPresent()) {
			throw new IncorrectOperationException();
		}

		if (rating != null && rating >= 1 && rating <= 10) {

			Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user);

			if (reviewTitle.isPresent()) {

				byte oldRating = reviewTitle.get().getRating();
				float oldAvgRating = title.get().getAvgRating();
				int timesRated = title.get().getTimesRated();

				title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating + rating) / timesRated);
				reviewTitle.get().setRating(rating);
				reviewTitleDAO.save(reviewTitle.get());

			} else {

				ReviewTitle newRating = new ReviewTitle(title.get(), user, rating, false, null, null, 0,
						LocalDate.now());

				float oldAvgRating = title.get().getAvgRating();
				int timesRated = title.get().getTimesRated();

				title.get().setAvgRating(((oldAvgRating * timesRated) + rating) / (timesRated + 1));
				title.get().setTimesRated(timesRated + 1);
				reviewTitleDAO.save(newRating);

			}

			listTitleOptional.get().setRating(rating);

		} else {

			Optional<ReviewTitle> reviewTitle = reviewTitleDAO.findByTitleIdAndUserId(title.get(), user);

			if (reviewTitle.isPresent()) {

				if (reviewTitle.get().isContainsReview()) {
					throw new IncorrectOperationException();
				}

				byte oldRating = reviewTitle.get().getRating();
				float oldAvgRating = title.get().getAvgRating();
				int timesRated = title.get().getTimesRated();

				if (timesRated <= 1) {
					title.get().setAvgRating(0);
					title.get().setTimesRated(0);
				} else {
					title.get().setAvgRating(((oldAvgRating * timesRated) - oldRating) / (timesRated - 1));
					title.get().setTimesRated(timesRated - 1);
				}

				reviewTitleDAO.delete(reviewTitle.get());

			}

			listTitleOptional.get().setRating((byte) -1);

		}

		listTitleOptional.get().setDateFinalized(endDate);
		listTitleOptional.get().setDateStarted(startDate);
		listTitleOptional.get().setStatus(FollowingStatus.valueOfStatusOrDefault(status));

		list.setLastUpdated(LocalDateTime.now());

		titleDAO.save(title.get());
		followingListDAO.save(list);
		followingListTitleDAO.save(listTitleOptional.get());
	}

	@Override
	public void deleteTitleFromList(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = permissionManager.findUser(userId);

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		FollowingList list = user.getFollowingList();

		Optional<FollowingListTitle> listTitleOptional = followingListTitleDAO.findByTitleIdAndListId(title.get(),
				list);

		if (!listTitleOptional.isPresent()) {
			throw new IncorrectOperationException();
		}

		list.getTitles().remove(listTitleOptional.get());
		list.setLastUpdated(LocalDateTime.now());

		if (title.get().getTitleType() == TitleType.MOVIE)
			list.setMovieCount((short) (list.getMovieCount() - 1));
		else
			list.setTvshowCount((short) (list.getTvshowCount() - 1));

		followingListDAO.save(list);
	}

	private TranslatedFollowingList toTranslatedFollowingList(FollowingList followingList,
			List<TranslatedFollowingListTitle> titles) {
		TranslatedFollowingList translatedFollowingList = new TranslatedFollowingList(followingList);
		for (TranslatedFollowingListTitle listTitle : titles) {

			switch (listTitle.getFollowingListTitle().getStatus()) {

			case PLAN_TO_WATCH:
				translatedFollowingList.getPlanToWatch().add(listTitle);
				break;

			case WATCHING:
				translatedFollowingList.getWatching().add(listTitle);
				break;

			case COMPLETED:
				translatedFollowingList.getCompleted().add(listTitle);
				break;

			case DROPPED:
				translatedFollowingList.getDropped().add(listTitle);
				break;

			default:
				break;

			}

		}

		return translatedFollowingList;
	}

	@Override
	public FollowingListTitle getFollowingListTitleInfo(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException {

		User user = permissionManager.findUser(userId);

		Optional<Title> title = titleDAO.findById(titleId);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(titleId);
		}

		Optional<FollowingListTitle> listTitleOptional = followingListTitleDAO.findByTitleIdAndListId(title.get(),
				user.getFollowingList());

		if (!listTitleOptional.isPresent()) {
			return null;
		}

		return listTitleOptional.get();

	}

}
