package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class CreatePersonCrewDTO {

	private Long id;
	private Long titleId;
	private Long personId;
	private String name;
	private Long crewTypeId;

	private List<CreateSeasonCrewDTO> seasons;

	public CreatePersonCrewDTO(Long id, Long titleId, Long personId, String name, Long crewTypeId,
			List<CreateSeasonCrewDTO> seasons) {
		this.id = id;
		this.titleId = titleId;
		this.personId = personId;
		this.name = name;
		this.crewTypeId = crewTypeId;
		this.seasons = seasons;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCrewTypeId() {
		return crewTypeId;
	}

	public void setCrewTypeId(Long crewTypeId) {
		this.crewTypeId = crewTypeId;
	}

	public List<CreateSeasonCrewDTO> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<CreateSeasonCrewDTO> seasons) {
		this.seasons = seasons;
	}

}
