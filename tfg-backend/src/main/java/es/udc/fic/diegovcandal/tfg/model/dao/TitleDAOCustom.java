package es.udc.fic.diegovcandal.tfg.model.dao;

import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

public interface TitleDAOCustom {

	Block<TitleInfo> getTitles(String titleName, Long idLocale, Visibility visibility, boolean recentFirst,
			TitleType titleType, int page, int size);

}
