package es.udc.fic.diegovcandal.tfg.model.restclient;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.PeopleService;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.BlockExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalPersonCreditsSummaryDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.PeopleExternalDTO;

@Service
@Transactional
public class TmdbAPIClientPeopleServiceImpl implements TmdbAPIClientPeopleService {

	@Value("${project.external.api.apiKey}")
	private String apiKey;

	@Value("${project.external.api.apiEndpoint}")
	private String apiEndpoint;

	@Value("${project.external.api.apiImageEndpoint}")
	private String apiImageEndpoint;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private PermissionManager permissionManager;

	@Autowired
	private PeopleService peopleService;

	@Autowired
	private PeopleDAO peopleDAO;

	@Override
	@Transactional(readOnly = true)
	public PeopleExternalDTO findCachedPerson(Long externalId, Long internalId, String acceptLanguage)
			throws PeopleNotFoundException {

		PeopleExternalDTO peopleExternalDTO = null;

		try {
			peopleExternalDTO = getExternalPeople(externalId, acceptLanguage);
		} catch (HttpClientErrorException e) {
			throw new PeopleNotFoundException(internalId);
		}

		if (peopleExternalDTO.getBiography() == null || peopleExternalDTO.getBiography().equals("")) {
			peopleExternalDTO.setBiography(getExternalPeople(externalId, LocaleType.en.toString()).getBiography());
		}

		peopleExternalDTO.setExternalId(peopleExternalDTO.getId());
		peopleExternalDTO.setId(internalId);

		return peopleExternalDTO;
	}

	@Override
	@Transactional(readOnly = true)
	public PeopleExternalDTO findExternalPeople(Long externalId, String acceptLanguage) throws PeopleNotFoundException {

		return findCachedPerson(externalId, -1L, acceptLanguage);

	}

	@Override
	@Transactional(readOnly = true)
	public byte[] getExternalImage(String image) {

		try {
			return restTemplate
					.exchange(apiImageEndpoint + "/w500" + image, HttpMethod.GET, HttpEntity.EMPTY, byte[].class)
					.getBody();

		} catch (HttpClientErrorException e) {
			return new byte[0];
		}

	}

	@Override
	public Long createExternalPerson(Long userId, Long externalId, String acceptLanguage)
			throws ExternalIdAlreadyLinkedException, PeopleNotFoundException, UserNotFoundException,
			PermissionException {

		Long peopleId = peopleService.existsByExternalId(externalId);

		if (peopleId != -1L)
			throw new ExternalIdAlreadyLinkedException();

		PeopleExternalDTO peopleExternalDTO = null;

		try {
			peopleExternalDTO = getExternalPeople(externalId, acceptLanguage);
		} catch (HttpClientErrorException e) {
			throw new PeopleNotFoundException(externalId);
		}

		People people = new People(externalId, SourceType.EXTERNAL_TMDB, peopleExternalDTO.getName(), null, null, null);

		Set<PeopleTraduction> personTraductions = new HashSet<>();
		personTraductions.add(new PeopleTraduction(people, LocaleType.valueOfLanguageOrDefault(acceptLanguage),
				peopleExternalDTO.getName(), null, null, true));

		people.setExternalImage(peopleExternalDTO.getProfile_path());
		people.setAddedByUser(permissionManager.findAdminUser(userId));
		people.setVisibility(Visibility.PUBLIC);
		people.setPeopleTraductions(personTraductions);

		return peopleDAO.save(people).getId();

	}

	@Override
	@Transactional(readOnly = true)
	public BlockExternalDTO<PeopleExternalDTO> findExternalPersons(String name, int page, String acceptLanguage) {

		if (name == null || name.trim().equals("")) {
			return new BlockExternalDTO<>(new ArrayList<PeopleExternalDTO>(), 0, 0, 0);
		}

		BlockExternalDTO<PeopleExternalDTO> block = restTemplate.exchange(
				apiEndpoint + "/search/person" + "?api_key=" + apiKey + "&language=" + acceptLanguage + "&query=" + name
						+ "&page=" + page + "&include_adult=false",
				HttpMethod.GET, HttpEntity.EMPTY,
				new ParameterizedTypeReference<BlockExternalDTO<PeopleExternalDTO>>() {
				}).getBody();

		return new BlockExternalDTO<>(block.getResults(), block.getPage(), block.getTotal_results(),
				block.getTotal_pages());

	}

	@Override
	public ExternalPersonCreditsSummaryDTO findPersonCastAndCrewHistory(Long externalId, String acceptLanguage) {

		return getExternalPeopleCastAndCrew(externalId, acceptLanguage);

	}

	private PeopleExternalDTO getExternalPeople(Long externalId, String acceptLanguage) {

		return restTemplate
				.exchange(apiEndpoint + "/person/" + externalId + "?api_key=" + apiKey + "&language=" + acceptLanguage,
						HttpMethod.GET, HttpEntity.EMPTY, PeopleExternalDTO.class)
				.getBody();

	}

	private ExternalPersonCreditsSummaryDTO getExternalPeopleCastAndCrew(Long externalId, String acceptLanguage) {

		return restTemplate
				.exchange(
						apiEndpoint + "/person/" + externalId + "/combined_credits?api_key=" + apiKey + "&language="
								+ acceptLanguage,
						HttpMethod.GET, HttpEntity.EMPTY, ExternalPersonCreditsSummaryDTO.class)
				.getBody();

	}

}
