package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateCustomListDTO {

	private boolean visible;
	private String title;
	private String description;
	private String image;

	@Valid
	private List<CreateCustomListTitleDTO> titles;

	public CreateCustomListDTO(boolean visible, String title, String description, String image,
			List<CreateCustomListTitleDTO> titles) {
		this.visible = visible;
		this.title = title;
		this.description = description;
		this.image = image;
		this.titles = titles;
	}

	@NotNull()
	public boolean isVisible() {
		return visible;
	}

	@NotNull()
	@Size(min = 1, max = 100)
	public String getTitle() {
		return title;
	}

	@Size(min = 0, max = 300)
	public String getDescription() {
		return description;
	}

	public String getImage() {
		return image;
	}

	public List<CreateCustomListTitleDTO> getTitles() {
		return titles;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setTitles(List<CreateCustomListTitleDTO> titles) {
		this.titles = titles;
	}

}
