package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class PeopleTraductionCompositeKey implements Serializable {

	private static final long serialVersionUID = 1827398521515195017L;

	private Long idPeople;
	private Long idLocale;

	public PeopleTraductionCompositeKey() {
	}

	public PeopleTraductionCompositeKey(Long idPeople, Long idLocale) {
		this.idPeople = idPeople;
		this.idLocale = idLocale;
	}

	public Long getIdPeople() {
		return idPeople;
	}

	public void setIdPeople(Long idPeople) {
		this.idPeople = idPeople;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLocale == null) ? 0 : idLocale.hashCode());
		result = prime * result + ((idPeople == null) ? 0 : idPeople.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeopleTraductionCompositeKey other = (PeopleTraductionCompositeKey) obj;
		if (idLocale == null) {
			if (other.idLocale != null)
				return false;
		} else if (!idLocale.equals(other.idLocale))
			return false;
		if (idPeople == null) {
			if (other.idPeople != null)
				return false;
		} else if (!idPeople.equals(other.idPeople))
			return false;
		return true;
	}

}
