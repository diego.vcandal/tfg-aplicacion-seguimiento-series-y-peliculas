package es.udc.fic.diegovcandal.tfg.model.service;

import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ReviewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;

public interface RatingService {

	void rateTitle(Long userId, Long titleId, byte rating)
			throws IncorrectOperationException, UserNotFoundException, TitleNotFoundException, IncorrectRatingException;

	void editTitleRating(Long userId, Long titleId, byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException;

	void deleteTitleRating(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException;

	void removeLike(Long userId, Long reviewId)
			throws IncorrectOperationException, UserNotFoundException, ReviewNotFoundException;

	void editReview(Long userId, Long titleId, String reviewTitle, String content, byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException;

	ReviewTitle getReviewTitle(Long userId, Long titleId) throws UserNotFoundException, TitleNotFoundException;

	ReviewTitle addNewReview(Long userId, Long titleId, String reviewTitle, String content, byte rating)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException;

	void addLike(Long userId, Long reviewId)
			throws IncorrectOperationException, UserNotFoundException, ReviewNotFoundException;

	void deleteReview(Long userId, Long titleId)
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException;

	Block<ReviewTitle> getReviews(Long titleId, Long userId, int pageNumber, int size)
			throws TitleNotFoundException, UserNotFoundException;

}
