package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class FriendPetitionCompositeKey implements Serializable {

	private static final long serialVersionUID = 4684496725609765745L;

	private Long originUser;
	private Long destinationUser;

	public FriendPetitionCompositeKey() {
	}

	public FriendPetitionCompositeKey(Long originUser, Long destinationUser) {
		this.originUser = originUser;
		this.destinationUser = destinationUser;
	}

	public Long getOriginUser() {
		return originUser;
	}

	public void setOriginUser(Long originUser) {
		this.originUser = originUser;
	}

	public Long getDestinationUser() {
		return destinationUser;
	}

	public void setDestinationUser(Long destinationUser) {
		this.destinationUser = destinationUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destinationUser == null) ? 0 : destinationUser.hashCode());
		result = prime * result + ((originUser == null) ? 0 : originUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendPetitionCompositeKey other = (FriendPetitionCompositeKey) obj;
		if (destinationUser == null) {
			if (other.destinationUser != null)
				return false;
		} else if (!destinationUser.equals(other.destinationUser))
			return false;
		if (originUser == null) {
			if (other.originUser != null)
				return false;
		} else if (!originUser.equals(other.originUser))
			return false;
		return true;
	}

}
