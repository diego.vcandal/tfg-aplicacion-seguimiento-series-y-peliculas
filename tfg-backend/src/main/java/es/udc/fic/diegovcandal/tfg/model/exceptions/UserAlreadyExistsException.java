package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class UserAlreadyExistsException extends Exception {

	private String userName;

	public UserAlreadyExistsException(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
