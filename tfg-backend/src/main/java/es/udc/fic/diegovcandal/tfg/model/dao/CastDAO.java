package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCastSummary;

public interface CastDAO extends PagingAndSortingRepository<Cast, Long> {

	Optional<Cast> findByPeopleIdAndTitleId(People peopleId, Title titleId);

	List<Cast> findByTitleIdAndMainCastTrue(Title titleId);

	List<Cast> findByTitleId(Title titleId);
	
	@Query("SELECT new es.udc.fic.diegovcandal.tfg.model.entity.dto.PersonCastSummary("
			+ "t.id, t.year, t.originalTitle, tt.titleName, c.characterName, c.episodeCount) "
			+ "FROM Cast c JOIN Title t " 
			+ "on c.titleId = t.id "
			+ "LEFT JOIN TitleTraduction tt "
			+ "on t.id = tt.idTitle AND " 
			+ "tt.idLocale = ?2 "
			+ "WHERE c.peopleId = ?1")
	List<PersonCastSummary> findCastHistory(People people, Long localeId);

}
