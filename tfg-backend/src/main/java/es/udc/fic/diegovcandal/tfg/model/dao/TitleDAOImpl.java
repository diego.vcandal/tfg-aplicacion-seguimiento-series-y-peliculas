package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleImage;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

public class TitleDAOImpl implements TitleDAOCustom {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public Block<TitleInfo> getTitles(String titleName, Long idLocale, Visibility visibility, boolean recentFirst,
			TitleType titleType, int page, int size) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		/* MAIN QUERY */
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<Title> from = criteriaQuery.from(Title.class);
		from.join("titleTraductions", JoinType.LEFT);

		criteriaQuery.select(from.get("id"))
				.where(TitleSpecifications.getFindTitleSpecificationIds(titleName, visibility, titleType)
						.toPredicate(from, criteriaQuery, criteriaBuilder))
				.groupBy(from.get("id"));

		if (recentFirst) {
			criteriaQuery.orderBy(criteriaBuilder.desc(from.get("addedDate")));
		}

		/* COUNT QUERY */
		CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
		Root<Title> fromCount = countQuery.from(Title.class);

		countQuery.select(criteriaBuilder.countDistinct(fromCount.get("id")))
				.where(TitleSpecifications.getFindTitleSpecificationIds(titleName, visibility, titleType)
						.toPredicate(fromCount, countQuery, criteriaBuilder));

		/* RESULTS */
		Long total = entityManager.createQuery(countQuery).getSingleResult();

		List<Long> list = entityManager.createQuery(criteriaQuery).setFirstResult(page * size).setMaxResults(size)
				.getResultList();

		int pages = (int) Math.ceil((double) total / size);
		boolean hasNext = page < (pages - 1);

		/* FULL RESULTS */
		CriteriaQuery<TitleInfo> criteriaFullQuery = criteriaBuilder.createQuery(TitleInfo.class);
		Root<Title> fromFull = criteriaFullQuery.from(Title.class);
		Join<Title, TitleTraduction> joinFull = fromFull.join("titleTraductions", JoinType.LEFT);
		joinFull.on(criteriaBuilder.equal(joinFull.get("idLocale"), idLocale));
		Join<Title, TitleImage> joinImage = fromFull.join("titleImages", JoinType.LEFT);
		joinImage.on(criteriaBuilder.equal(joinImage.get("portrait"), 1));

		criteriaFullQuery
				.multiselect(fromFull.get("id"), fromFull.get("externalId"), fromFull.get("originalTitle"),
						joinFull.get("titleName"), joinFull.get("description"), fromFull.get("titleType"),
						fromFull.get("sourceType"), fromFull.get("originalDescription"), joinImage.get("image"),
						fromFull.get("year"), fromFull.get("avgRating"), fromFull.get("timesRated"))
				.where(fromFull.get("id").in(list));

		if (recentFirst) {
			criteriaFullQuery.orderBy(criteriaBuilder.desc(fromFull.get("addedDate")));
		}

		List<TitleInfo> finalList = entityManager.createQuery(criteriaFullQuery).getResultList();

		return new Block<>(finalList, hasNext, pages, total);

	}

}
