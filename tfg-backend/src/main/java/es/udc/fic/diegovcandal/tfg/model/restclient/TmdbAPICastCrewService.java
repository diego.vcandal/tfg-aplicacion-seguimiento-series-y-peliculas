package es.udc.fic.diegovcandal.tfg.model.restclient;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.CastExternalDTO;
import es.udc.fic.diegovcandal.tfg.rest.controller.external.dto.ExternalCreditsDTO;

public interface TmdbAPICastCrewService {

	List<CastExternalDTO> findMainCast(Long externalId, String acceptLanguage, TitleType titleType);

	ExternalCreditsDTO findAllCastAndCrew(Long externalId, String acceptLanguage, TitleType titleType);

	ExternalCreditsDTO findAllEpisodeCastAndCrew(Long externalId, byte season, short episode, String acceptLanguage);

}
