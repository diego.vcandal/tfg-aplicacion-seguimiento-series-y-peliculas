package es.udc.fic.diegovcandal.tfg.model.entity.compositekey;

import java.io.Serializable;

public class EpisodeTraductionCompositeKey implements Serializable {

	private static final long serialVersionUID = -8628048430838688926L;

	private Long idEpisode;
	private Long idLocale;

	public EpisodeTraductionCompositeKey() {
	}

	public EpisodeTraductionCompositeKey(Long idEpisode, Long idLocale) {
		this.idEpisode = idEpisode;
		this.idLocale = idLocale;
	}

	public Long getIdEpisode() {
		return idEpisode;
	}

	public Long getIdLocale() {
		return idLocale;
	}

	public void setIdEpisode(Long idEpisode) {
		this.idEpisode = idEpisode;
	}

	public void setIdLocale(Long idLocale) {
		this.idLocale = idLocale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEpisode == null) ? 0 : idEpisode.hashCode());
		result = prime * result + ((idLocale == null) ? 0 : idLocale.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EpisodeTraductionCompositeKey other = (EpisodeTraductionCompositeKey) obj;
		if (idEpisode == null) {
			if (other.idEpisode != null)
				return false;
		} else if (!idEpisode.equals(other.idEpisode))
			return false;
		if (idLocale == null) {
			if (other.idLocale != null)
				return false;
		} else if (!idLocale.equals(other.idLocale))
			return false;
		return true;
	}

}
