package es.udc.fic.diegovcandal.tfg.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.User;

public interface FollowingListDAO extends PagingAndSortingRepository<FollowingList, Long> {

	FollowingList findByUserId(User userId);

}
