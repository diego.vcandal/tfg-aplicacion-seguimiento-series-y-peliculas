package es.udc.fic.diegovcandal.tfg.rest.controller.dto;

import java.util.List;

public class InternalPersonCastDTO {

	private Long id;
	private Long titleId;
	private Long personId;
	private Long externalId;
	private String originalName;
	private String name;
	private boolean image;
	private String externalImage;
	private Long sourceType;
	private boolean isMainCast;

	private List<CreateSeasonCastDTO> seasons;

	public InternalPersonCastDTO(Long id, Long titleId, Long personId, Long externalId, String originalName,
			boolean image, String externalImage, Long sourceType, String name, boolean isMainCast,
			List<CreateSeasonCastDTO> seasons) {
		this.id = id;
		this.titleId = titleId;
		this.personId = personId;
		this.externalId = externalId;
		this.originalName = originalName;
		this.image = image;
		this.externalImage = externalImage;
		this.sourceType = sourceType;
		this.name = name;
		this.isMainCast = isMainCast;
		this.seasons = seasons;
	}

	public Long getId() {
		return id;
	}

	public Long getTitleId() {
		return titleId;
	}

	public Long getPersonId() {
		return personId;
	}

	public boolean isImage() {
		return image;
	}

	public String getExternalImage() {
		return externalImage;
	}

	public Long getSourceType() {
		return sourceType;
	}

	public boolean isMainCast() {
		return isMainCast;
	}

	public List<CreateSeasonCastDTO> getSeasons() {
		return seasons;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public void setExternalImage(String externalImage) {
		this.externalImage = externalImage;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public void setMainCast(boolean isMainCast) {
		this.isMainCast = isMainCast;
	}

	public void setSeasons(List<CreateSeasonCastDTO> seasons) {
		this.seasons = seasons;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public String getOriginalName() {
		return originalName;
	}

	public String getName() {
		return name;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public void setName(String name) {
		this.name = name;
	}

}
