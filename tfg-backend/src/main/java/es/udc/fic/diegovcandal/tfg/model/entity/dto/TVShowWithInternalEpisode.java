package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import es.udc.fic.diegovcandal.tfg.model.entity.Episode;

public class TVShowWithInternalEpisode {

	private String originalTitle;
	private String seasonOriginalTitle;
	private Episode episode;

	public TVShowWithInternalEpisode(String originalTitle, String seasonOriginalTitle, Episode episode) {
		this.originalTitle = originalTitle;
		this.seasonOriginalTitle = seasonOriginalTitle;
		this.episode = episode;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getSeasonOriginalTitle() {
		return seasonOriginalTitle;
	}

	public Episode getEpisode() {
		return episode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((episode == null) ? 0 : episode.hashCode());
		result = prime * result + ((originalTitle == null) ? 0 : originalTitle.hashCode());
		result = prime * result + ((seasonOriginalTitle == null) ? 0 : seasonOriginalTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TVShowWithInternalEpisode other = (TVShowWithInternalEpisode) obj;
		if (episode == null) {
			if (other.episode != null)
				return false;
		} else if (!episode.equals(other.episode))
			return false;
		if (originalTitle == null) {
			if (other.originalTitle != null)
				return false;
		} else if (!originalTitle.equals(other.originalTitle))
			return false;
		if (seasonOriginalTitle == null) {
			if (other.seasonOriginalTitle != null)
				return false;
		} else if (!seasonOriginalTitle.equals(other.seasonOriginalTitle))
			return false;
		return true;
	}

}
