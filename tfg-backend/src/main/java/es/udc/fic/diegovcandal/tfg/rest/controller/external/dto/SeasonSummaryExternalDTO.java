package es.udc.fic.diegovcandal.tfg.rest.controller.external.dto;

import java.util.List;

public class SeasonSummaryExternalDTO {

	private Long id;
	private short episode_count;
	private String air_date;
	private String poster_path;
	private String name;
	private String overview;
	private short season_number;

	List<EpisodeSummaryExternalDTO> episodes;

	public Long getId() {
		return id;
	}

	public short getEpisode_count() {
		return episode_count;
	}

	public String getAir_date() {
		return air_date;
	}

	public String getPoster_path() {
		return poster_path;
	}

	public String getName() {
		return name;
	}

	public String getOverview() {
		return overview;
	}

	public short getSeason_number() {
		return season_number;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEpisode_count(short episode_count) {
		this.episode_count = episode_count;
	}

	public void setAir_date(String air_date) {
		this.air_date = air_date;
	}

	public void setPoster_path(String poster_path) {
		this.poster_path = poster_path;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setSeason_number(short season_number) {
		this.season_number = season_number;
	}

	public List<EpisodeSummaryExternalDTO> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<EpisodeSummaryExternalDTO> episodes) {
		this.episodes = episodes;
	}

}
