package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.UserFriendOf;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.UserFriendOfCompositeKey;

public interface UserFriendOfDao extends PagingAndSortingRepository<UserFriendOf, UserFriendOfCompositeKey> {

	@Query("SELECT u FROM UserFriendOf u WHERE u.firstFriendId = ?1 OR u.secondFriendId = ?1")
	Page<UserFriendOf> findFriendsByUser(User user, Pageable pageable);

	@Query("SELECT u FROM UserFriendOf u WHERE (u.firstFriendId = ?1 AND u.secondFriendId = ?2) OR (u.firstFriendId = ?2 AND u.secondFriendId = ?1)")
	Optional<UserFriendOf> findFriendByUsers(User user, User friend);

}
