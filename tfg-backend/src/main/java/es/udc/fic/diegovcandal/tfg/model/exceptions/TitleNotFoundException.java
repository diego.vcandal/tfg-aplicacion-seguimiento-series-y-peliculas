package es.udc.fic.diegovcandal.tfg.model.exceptions;

@SuppressWarnings("serial")
public class TitleNotFoundException extends InstanceNotFoundException {

	public TitleNotFoundException(Object key) {
		super("entities.title", key);
	}
}
