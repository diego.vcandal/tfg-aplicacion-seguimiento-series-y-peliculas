package es.udc.fic.diegovcandal.tfg.model.entity.dto;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.SeasonTraduction;

public class SeasonWithTraduction {

	private Season season;
	private SeasonTraduction seasonTraduction;
	private boolean noContent;

	private List<EpisodeWithTraduction> episodes;

	public SeasonWithTraduction() {
		this.noContent = false;
	}

	public SeasonWithTraduction(Season season, SeasonTraduction seasonTraduction) {
		this.season = season;
		this.seasonTraduction = seasonTraduction;
		this.noContent = false;
	}

	public SeasonWithTraduction(Season season, SeasonTraduction seasonTraduction,
			List<EpisodeWithTraduction> episodes) {
		this.season = season;
		this.seasonTraduction = seasonTraduction;
		this.episodes = episodes;
		this.noContent = false;
	}

	public Season getSeason() {
		return season;
	}

	public SeasonTraduction getSeasonTraduction() {
		return seasonTraduction;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public void setSeasonTraduction(SeasonTraduction seasonTraduction) {
		this.seasonTraduction = seasonTraduction;
	}

	public List<EpisodeWithTraduction> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<EpisodeWithTraduction> episodes) {
		this.episodes = episodes;
	}

	public boolean isNoContent() {
		return noContent;
	}

	public void setNoContent(boolean noContent) {
		this.noContent = noContent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((episodes == null) ? 0 : episodes.hashCode());
		result = prime * result + (noContent ? 1231 : 1237);
		result = prime * result + ((season == null) ? 0 : season.hashCode());
		result = prime * result + ((seasonTraduction == null) ? 0 : seasonTraduction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeasonWithTraduction other = (SeasonWithTraduction) obj;
		if (episodes == null) {
			if (other.episodes != null)
				return false;
		} else if (!episodes.equals(other.episodes))
			return false;
		if (noContent != other.noContent)
			return false;
		if (season == null) {
			if (other.season != null)
				return false;
		} else if (!season.equals(other.season))
			return false;
		if (seasonTraduction == null) {
			if (other.seasonTraduction != null)
				return false;
		} else if (!seasonTraduction.equals(other.seasonTraduction))
			return false;
		return true;
	}

}
