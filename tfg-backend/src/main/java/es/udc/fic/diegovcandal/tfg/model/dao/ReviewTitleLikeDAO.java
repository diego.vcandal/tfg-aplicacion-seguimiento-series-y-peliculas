package es.udc.fic.diegovcandal.tfg.model.dao;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitleLike;
import es.udc.fic.diegovcandal.tfg.model.entity.User;

public interface ReviewTitleLikeDAO extends PagingAndSortingRepository<ReviewTitleLike, Long> {

	Optional<ReviewTitleLike> findByReviewIdAndUserId(ReviewTitle reviewId, User userId);

}
