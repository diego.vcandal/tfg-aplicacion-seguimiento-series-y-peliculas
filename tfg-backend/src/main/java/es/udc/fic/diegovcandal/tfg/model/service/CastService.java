package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.List;

import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;

public interface CastService {

	Long addPersonNewCastParticipation(Long userId, Long personId, Long titleId, Cast newCast)
			throws PeopleNotFoundException, UserNotFoundException, PermissionException, TitleNotFoundException,
			CastAlreadyExistsException;

	void updatePersonCastParticipation(Long userId, Long castId, Cast newCast) throws PeopleNotFoundException,
			UserNotFoundException, PermissionException, TitleNotFoundException, CastNotFoundException;

	void removePersonCastParticipation(Long userId, Long castId)
			throws UserNotFoundException, PermissionException, CastNotFoundException;

	Long addPersonEpisodeCastParticipation(Long userId, Long peopleId, Long titleId, byte seasonNumber,
			CastEpisode newCastEpisode)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException, TitleNotFoundException;

	void removePersonEpisodeCastParticipation(Long userId, Long episodeCastId) throws UserNotFoundException,
			PermissionException, PeopleNotFoundException, TitleNotFoundException, CastNotFoundException;

	void updatePersonEpisodeCastParticipation(Long userId, Long episodeCastId, String characterName)
			throws UserNotFoundException, PermissionException, PeopleNotFoundException, TitleNotFoundException,
			CastNotFoundException;

	List<Cast> findMainCast(Long titleId, Long localeId);

	List<Cast> findAllCast(Long titleId, Long localeId);

	List<CastEpisode> findEpisodeCast(Long episodeId, Long localeId);

}
