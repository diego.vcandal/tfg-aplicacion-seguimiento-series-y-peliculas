package es.udc.fic.diegovcandal.tfg.model.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CustomListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.PermissionManager;

@Service
@Transactional(readOnly = true)
public class ImageServiceImpl implements ImageService {

	@Autowired
	private TitleDAO titleDao;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private CustomListDAO customListDAO;

	@Autowired
	private PermissionManager permissionManager;

	@Override
	public String getProfileImage(Long id) throws UserNotFoundException {
		return permissionManager.findUser(id).getImage();
	}

	@Override
	public String getHeaderImage(Long id) throws TitleNotFoundException {

		Optional<Title> title = titleDao.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		return titleDao.findHeaderImage(title.get());
	}

	@Override
	public String getPortraitImage(Long id) throws TitleNotFoundException {
		Optional<Title> title = titleDao.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		return titleDao.findPortraitImage(title.get());
	}

	@Override
	public String getSeasonImage(Long id, byte seasonNumber) throws TitleNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		return seasonDAO.findSeasonImage(title.get(), seasonNumber);
	}

	@Override
	public String getEpisodeImage(Long id, byte seasonNumber, short episodeNumber) throws TitleNotFoundException {

		Optional<TVShow> title = tvShowDAO.findById(id);

		if (!title.isPresent()) {
			throw new TitleNotFoundException(id);
		}

		Optional<SeasonWithTraduction> season = seasonDAO.findSeasonWithTraduction(title.get(), seasonNumber,
				LocaleType.en.getId());

		if (!season.isPresent()) {

			throw new TitleNotFoundException(id);

		}

		return episodeDAO.findEpisodeImage(season.get().getSeason(), episodeNumber);
	}

	@Override
	public String getPeopleImage(Long id) throws PeopleNotFoundException {

		Optional<People> people = peopleDAO.findById(id);

		if (!people.isPresent()) {
			throw new PeopleNotFoundException(id);
		}

		return people.get().getImage();
	}

	@Override
	public String getCustomListImage(Long id) throws CustomListNotFoundException {

		Optional<CustomList> list = customListDAO.findById(id);

		if (!list.isPresent()) {
			throw new CustomListNotFoundException(id);
		}

		return list.get().getImage();
	}

}
