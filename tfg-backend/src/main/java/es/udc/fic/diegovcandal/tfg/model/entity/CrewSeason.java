package es.udc.fic.diegovcandal.tfg.model.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class CrewSeason {

	private Long id;
	private Crew crewId;
	private Season seasonId;
	private short episodeCount;

	private Set<CrewEpisode> crewEpisodes;

	public CrewSeason() {
		this.crewEpisodes = new HashSet<>();
	}

	public CrewSeason(Crew crewId, Season seasonId, short episodeCount) {
		this.crewId = crewId;
		this.seasonId = seasonId;
		this.episodeCount = episodeCount;

		this.crewEpisodes = new HashSet<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "crewId")
	public Crew getCrewId() {
		return crewId;
	}

	public void setCrewId(Crew crewId) {
		this.crewId = crewId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "seasonId")
	public Season getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(Season seasonId) {
		this.seasonId = seasonId;
	}

	public short getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(short episodeCount) {
		this.episodeCount = episodeCount;
	}

	@OneToMany(mappedBy = "crewSeasonId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<CrewEpisode> getCrewEpisodes() {
		return crewEpisodes;
	}

	public void setCrewEpisodes(Set<CrewEpisode> crewEpisodes) {
		this.crewEpisodes = crewEpisodes;
	}

}
