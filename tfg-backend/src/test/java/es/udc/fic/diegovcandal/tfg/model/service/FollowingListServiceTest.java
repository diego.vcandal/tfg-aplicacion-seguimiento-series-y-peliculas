package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.FollowingListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.FollowingListTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.ReviewTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.FollowingStatus;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class FollowingListServiceTest {

	@Autowired
	private FollowingListDAO followingListDAO;

	@Autowired
	private FollowingListTitleDAO followingListTitleDAO;

	@Autowired
	private FollowingListService followingListService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private ReviewTitleDAO reviewTitleDAO;

	@Test
	public void getPersonalListTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		assertEquals(user.getFollowingList(), followingListService
				.getPersonalList(user.getId(), LocaleType.DEFAULT_LOCALE.getId()).getFollowingList());

	}

	@Test
	public void getPersonalListNonExistantUserTest() throws UserNotFoundException {

		assertThrows(UserNotFoundException.class,
				() -> followingListService.getPersonalList(-1L, LocaleType.DEFAULT_LOCALE.getId()));

	}

	@Test
	public void getPublicListTest() throws UserNotFoundException, PrivateResourceException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		assertEquals(user.getFollowingList(),
				followingListService.getPublicList(user.getId(), LocaleType.DEFAULT_LOCALE.getId()).getFollowingList());

	}

	@Test
	public void getPublicListNonExistantUserTest() {

		assertThrows(UserNotFoundException.class,
				() -> followingListService.getPublicList(-1L, LocaleType.DEFAULT_LOCALE.getId()));

	}

	@Test
	public void getPrivateListTest() {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PRIVATE));
		assertThrows(PrivateResourceException.class, () -> followingListService
				.getPublicList(user.getId(), LocaleType.DEFAULT_LOCALE.getId()).getFollowingList());

	}

	@Test
	public void makeListPublicTest() throws UserNotFoundException, PrivateResourceException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PRIVATE));

		followingListService.changeListVisbility(user.getId());

		assertEquals(Visibility.PUBLIC, followingListDAO.findByUserId(user).getVisibility());

	}

	@Test
	public void makeListPrivateTest() throws UserNotFoundException, PrivateResourceException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		followingListService.changeListVisbility(user.getId());

		assertEquals(Visibility.PRIVATE, followingListDAO.findByUserId(user).getVisibility());

	}

	@Test
	public void makeListPublicNonExistantUserTest() throws UserNotFoundException {

		assertThrows(UserNotFoundException.class, () -> followingListService.changeListVisbility(-1L));

	}

	@Test
	public void getFollowingListTraductedPlanToWatchTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		Title title = Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC);
		Set<TitleTraduction> titleTraductions = Utils.createTitleTraductionSet(title);
		title.setTitleTraductions(titleTraductions);
		title = titleDAO.save(title);

		followingListTitleDAO.save(new FollowingListTitle(title, user.getFollowingList(), FollowingStatus.PLAN_TO_WATCH,
				LocalDateTime.now(), null, null, (byte) 1, false));

		assertEquals(1, followingListService.getPersonalList(user.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getPlanToWatch().size());

	}

	@Test
	public void getFollowingListTraductedCompletedTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		Title title = Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC);
		Set<TitleTraduction> titleTraductions = Utils.createTitleTraductionSet(title);
		title.setTitleTraductions(titleTraductions);
		title = titleDAO.save(title);

		followingListTitleDAO.save(new FollowingListTitle(title, user.getFollowingList(), FollowingStatus.COMPLETED,
				LocalDateTime.now(), null, null, (byte) 1, false));

		assertEquals(1, followingListService.getPersonalList(user.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCompleted().size());

	}

	@Test
	public void getFollowingListTraductedDroppedTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		Title title = Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC);
		Set<TitleTraduction> titleTraductions = Utils.createTitleTraductionSet(title);
		title.setTitleTraductions(titleTraductions);
		title = titleDAO.save(title);

		followingListTitleDAO.save(new FollowingListTitle(title, user.getFollowingList(), FollowingStatus.DROPPED,
				LocalDateTime.now(), null, null, (byte) 1, false));

		assertEquals(1, followingListService.getPersonalList(user.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getDropped().size());

	}

	@Test
	public void getFollowingListTraductedWatchingTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		Title title = Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC);
		Set<TitleTraduction> titleTraductions = Utils.createTitleTraductionSet(title);
		title.setTitleTraductions(titleTraductions);
		title = titleDAO.save(title);

		followingListTitleDAO.save(new FollowingListTitle(title, user.getFollowingList(), FollowingStatus.WATCHING,
				LocalDateTime.now(), null, null, (byte) 1, false));

		assertEquals(1, followingListService.getPersonalList(user.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getWatching().size());

	}

	@Test
	public void addToListTest() throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		followingListService.addToList(user.getId(), title.getId(), 0L);

		assertEquals(1, followingListDAO.findById(user.getFollowingList().getId()).get().getTitles().size());

	}

	@Test
	public void addMovieToListTest() throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		followingListService.addToList(user.getId(), title.getId(), 0L);

		assertEquals(1, followingListDAO.findById(user.getFollowingList().getId()).get().getMovieCount());

	}

	@Test
	public void addTVShowToListTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		followingListService.addToList(user.getId(), title.getId(), 0L);

		assertEquals(1, followingListDAO.findById(user.getFollowingList().getId()).get().getTvshowCount());

	}

	@Test
	public void addToListNonExistantTitleTest() {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class, () -> followingListService.addToList(user.getId(), -1L, 0L));

	}

	@Test
	public void addToListNonExistantUserTest() {
		assertThrows(UserNotFoundException.class, () -> followingListService.addToList(-1L, -1L, 0L));

	}

	@Test
	public void addToListAlreadyExistsTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		followingListService.addToList(user.getId(), title.getId(), 0L);

		assertThrows(IncorrectOperationException.class,
				() -> followingListService.addToList(user.getId(), title.getId(), 0L));

	}

	@Test
	public void editTitleFromListTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));
		followingListService.addToList(user.getId(), title.getId(), 0L);

		followingListService.editTitleFromList(user.getId(), title.getId(), null, null,
				FollowingStatus.COMPLETED.getId(), (byte) 5);

		assertEquals(FollowingStatus.COMPLETED, followingListDAO.findById(user.getFollowingList().getId()).get()
				.getTitles().stream().findFirst().get().getStatus());

	}

	@Test
	public void editTitleFromListNonExistantTitleTest() {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class, () -> followingListService.editTitleFromList(user.getId(), -1L, null,
				null, FollowingStatus.WATCHING.getId(), (byte) 5));

	}

	@Test
	public void editTitleFromListNonExistantUserTest() {
		assertThrows(UserNotFoundException.class, () -> followingListService.editTitleFromList(-1L, -1L, null, null,
				FollowingStatus.WATCHING.getId(), (byte) 5));

	}

	@Test
	public void editTitleFromListNotAddedTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		assertThrows(IncorrectOperationException.class, () -> followingListService.editTitleFromList(user.getId(),
				title.getId(), null, null, FollowingStatus.WATCHING.getId(), (byte) 5));

	}

	@Test
	public void deleteTitleFromListTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));
		followingListService.addToList(user.getId(), title.getId(), 0L);

		followingListService.deleteTitleFromList(user.getId(), title.getId());

		assertEquals(0, followingListDAO.findById(user.getFollowingList().getId()).get().getTitles().size());

	}

	@Test
	public void deleteTitleFromListNonExistantTitleTest() {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class, () -> followingListService.deleteTitleFromList(user.getId(), -1L));

	}

	@Test
	public void deleteTitleFromListNonExistantUserTest() {
		assertThrows(UserNotFoundException.class, () -> followingListService.deleteTitleFromList(-1L, -1L));

	}

	@Test
	public void deleteTitleFromListNotAddedTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		assertThrows(IncorrectOperationException.class,
				() -> followingListService.deleteTitleFromList(user.getId(), title.getId()));

	}

	@Test
	public void editTitleFromListWithReviewTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = reviewTitleDAO
				.save(new ReviewTitle(title, user, (byte) 5, false, null, null, (short) 0, LocalDate.now()));

		followingListService.addToList(user.getId(), title.getId(), 0L);

		followingListService.editTitleFromList(user.getId(), title.getId(), null, null,
				FollowingStatus.COMPLETED.getId(), (byte) 10);

		assertEquals(10, reviewTitleDAO.findById(reviewTitle.getId()).get().getRating());

	}

	@Test
	public void getFollowingListTitleInfoTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		followingListService.addToList(user.getId(), title.getId(), 0L);

		assertEquals(FollowingStatus.PLAN_TO_WATCH,
				followingListService.getFollowingListTitleInfo(user.getId(), title.getId()).getStatus());

	}

	@Test
	public void getFollowingListTitleInfoNonExistantUserTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class,
				() -> followingListService.getFollowingListTitleInfo(-1L, title.getId()));

	}

	@Test
	public void getFollowingListTitleInfoNonExistantTitleTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class,
				() -> followingListService.getFollowingListTitleInfo(user.getId(), -1L));

	}

	@Test
	public void getFollowingListTitleInfoNonExistantEntryInListTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));

		assertNull(followingListService.getFollowingListTitleInfo(user.getId(), title.getId()));

	}

	@Test
	public void editTitleFromListDeleteRatingTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));
		followingListService.addToList(user.getId(), title.getId(), 0L);

		followingListService.editTitleFromList(user.getId(), title.getId(), null, null,
				FollowingStatus.COMPLETED.getId(), (byte) 11);

		assertEquals(-1, followingListDAO.findById(user.getFollowingList().getId()).get().getTitles().stream()
				.findFirst().get().getRating());

	}

	@Test
	public void editTitleFromListDeleteRatingWithReviewTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));
		followingListService.addToList(user.getId(), title.getId(), 0L);

		ReviewTitle reviewTitle = reviewTitleDAO
				.save(new ReviewTitle(title, user, (byte) 5, false, null, null, (short) 0, LocalDate.now()));

		followingListService.editTitleFromList(user.getId(), title.getId(), null, null,
				FollowingStatus.COMPLETED.getId(), (byte) 0);

		assertFalse(reviewTitleDAO.findById(reviewTitle.getId()).isPresent());

	}

	@Test
	public void editTitleFromListDeleteRatingWithReviewAndContentTest() throws UserNotFoundException,
			TitleNotFoundException, IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUserWithFollowingList(Visibility.PUBLIC));
		Title title = titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC));
		followingListService.addToList(user.getId(), title.getId(), 0L);

		reviewTitleDAO.save(new ReviewTitle(title, user, (byte) 5, true, "test", "test", (short) 10, LocalDate.now()));

		assertThrows(IncorrectOperationException.class, () -> followingListService.editTitleFromList(user.getId(),
				title.getId(), null, null, FollowingStatus.COMPLETED.getId(), null));

	}
}
