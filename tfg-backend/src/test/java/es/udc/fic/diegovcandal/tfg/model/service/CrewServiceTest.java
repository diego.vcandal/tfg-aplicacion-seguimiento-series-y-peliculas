package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CrewDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CrewEpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CrewTypeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewType;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CrewTypeNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CrewServiceTest {

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private CrewService crewService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private CrewDAO crewDAO;

	@Autowired
	private CrewEpisodeDAO crewEpisodeDAO;

	@Autowired
	private CrewTypeDAO crewTypeDAO;

	@Test
	public void addPersonCrewParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s1));
		episodeDAO.save(Utils.createEpisode(3L, s2));

		Crew crew = Utils.createCrew(tvShow, people, crewType);

		Set<CrewSeason> crewSeasons = new HashSet<>();
		Set<CrewEpisode> crewEpisodes = new HashSet<>();

		CrewSeason crewSeason = Utils.createCrewSeason(crew, s1);
		crewEpisodes.add(Utils.createCrewEpisode(crewSeason, e1, crewType));
		crewEpisodes.add(Utils.createCrewEpisode(crewSeason, e2, crewType));
		crewSeasons.add(crewSeason);

		crewSeason.setCrewEpisodes(crewEpisodes);
		crew.setCrewSeasons(crewSeasons);

		Long id = crewService.addPersonNewCrewParticipation(user.getId(), people.getId(), tvShow.getId(), crew);

		assertEquals((short) 2, crewDAO.findById(id).get().getEpisodeCount());

	}

	@Test
	public void addPersonCrewParticipationWithNonExistantPersonTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CrewAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(PeopleNotFoundException.class,
				() -> crewService.addPersonNewCrewParticipation(user.getId(), -1L, -1L, null));

	}

	@Test
	public void addPersonCrewParticipationWithNonExistantTitleTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CrewAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class,
				() -> crewService.addPersonNewCrewParticipation(user.getId(), people.getId(), -1L, null));

	}

	@Test
	public void addPersonCrewParticipationWithAlreadyExistantCrewTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CrewAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		crewDAO.save(Utils.createCrew(tvShow, people, crewType));

		Crew crew = Utils.createCrew(tvShow, people, crewType);

		assertThrows(CrewAlreadyExistsException.class,
				() -> crewService.addPersonNewCrewParticipation(user.getId(), people.getId(), tvShow.getId(), crew));

	}

	@Test
	public void updatePersonCrewParticipationTest()
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewNotFoundException, CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s1));
		Episode e3 = episodeDAO.save(Utils.createEpisode(3L, s2));
		Episode e4 = episodeDAO.save(Utils.createEpisode(4L, s1));

		Crew crew = Utils.createCrew(tvShow, people, crewType);

		Set<CrewSeason> crewSeasons = new HashSet<>();
		Set<CrewEpisode> crewEpisodes = new HashSet<>();

		CrewSeason crewSeason = Utils.createCrewSeason(crew, s1);
		crewEpisodes.add(Utils.createCrewEpisode(crewSeason, e1, crewType));
		crewEpisodes.add(Utils.createCrewEpisode(crewSeason, e2, crewType));
		crewSeasons.add(crewSeason);

		crewSeason.setCrewEpisodes(crewEpisodes);
		crew.setCrewSeasons(crewSeasons);

		Long id = crewService.addPersonNewCrewParticipation(user.getId(), people.getId(), tvShow.getId(), crew);

		crew = Utils.createCrew(tvShow, people, crewType);

		Set<CrewSeason> crewSeasons1 = new HashSet<>();
		Set<CrewEpisode> crewEpisodes1 = new HashSet<>();
		Set<CrewEpisode> crewEpisodes2 = new HashSet<>();

		CrewSeason crewSeason1 = Utils.createCrewSeason(crew, s1);
		crewEpisodes1.add(Utils.createCrewEpisode(crewSeason1, e1, crewType));
		crewEpisodes1.add(Utils.createCrewEpisode(crewSeason1, e2, crewType));
		crewEpisodes1.add(Utils.createCrewEpisode(crewSeason1, e4, crewType));
		crewSeasons1.add(crewSeason1);

		CrewSeason crewSeason2 = Utils.createCrewSeason(crew, s2);
		crewEpisodes2.add(Utils.createCrewEpisode(crewSeason2, e3, crewType));
		crewSeasons1.add(crewSeason2);

		crewSeason1.setCrewEpisodes(crewEpisodes1);
		crewSeason2.setCrewEpisodes(crewEpisodes2);
		crew.setCrewSeasons(crewSeasons1);

		crewService.updatePersonCrewParticipation(user.getId(), id, crew);

		assertEquals((short) 4, crewDAO.findById(id).get().getEpisodeCount());

	}

	@Test
	public void updateNonExistantPersonCrewParticipationTest() {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CrewNotFoundException.class,
				() -> crewService.updatePersonCrewParticipation(user.getId(), -1L, null));

	}

	@Test
	public void removePersonCrewParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CrewNotFoundException, CrewAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Crew crew = crewDAO.save(Utils.createCrew(tvShow, people, crewType));

		crewService.removePersonCrewParticipation(user.getId(), crew.getId());

		assertFalse(crewDAO.findById(crew.getId()).isPresent());

	}

	@Test
	public void removeNonExistantPersonCrewParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CrewNotFoundException, CrewAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CrewNotFoundException.class, () -> crewService.removePersonCrewParticipation(user.getId(), -1L));

	}

	@Test
	public void addPersonToEpisodeCrewParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s1));

		Crew crew = Utils.createCrew(tvShow, people, crewType);

		Set<CrewSeason> crewSeasons = new HashSet<>();
		Set<CrewEpisode> crewEpisodes = new HashSet<>();

		CrewSeason crewSeason = Utils.createCrewSeason(crew, s1);
		crewEpisodes.add(Utils.createCrewEpisode(crewSeason, e1, crewType));
		crewSeasons.add(crewSeason);

		crewSeason.setCrewEpisodes(crewEpisodes);
		crew.setCrewSeasons(crewSeasons);

		crewService.addPersonNewCrewParticipation(user.getId(), people.getId(), tvShow.getId(), crew);

		CrewEpisode crewEpisode = new CrewEpisode(null, e2, crewType);
		Long id = crewService.addPersonEpisodeCrewParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), crewEpisode);

		assertEquals((short) 2, crewEpisodeDAO.findById(id).get().getCrewSeasonId().getCrewId().getEpisodeCount());

	}

	@Test
	public void addPersonToEpisodeCrewParticipationFromNewSeasonTest()
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s2));

		Crew crew = Utils.createCrew(tvShow, people, crewType);

		Set<CrewSeason> crewSeasons = new HashSet<>();
		Set<CrewEpisode> crewEpisodes = new HashSet<>();

		CrewSeason crewSeason = Utils.createCrewSeason(crew, s1);
		crewEpisodes.add(Utils.createCrewEpisode(crewSeason, e1, crewType));
		crewSeasons.add(crewSeason);

		crewSeason.setCrewEpisodes(crewEpisodes);
		crew.setCrewSeasons(crewSeasons);

		crewService.addPersonNewCrewParticipation(user.getId(), people.getId(), tvShow.getId(), crew);

		CrewEpisode crewEpisode = new CrewEpisode(null, e2, crewType);
		Long id = crewService.addPersonEpisodeCrewParticipation(user.getId(), people.getId(), tvShow.getId(),
				s2.getSeasonNumber(), crewEpisode);

		assertEquals((short) 2, crewEpisodeDAO.findById(id).get().getCrewSeasonId().getCrewId().getEpisodeCount());

	}

	@Test
	public void addPersonToEpisodeCrewParticipationFromNewTitleTest()
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		CrewEpisode crewEpisode = new CrewEpisode(null, e1, crewType);

		Long id = crewService.addPersonEpisodeCrewParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), crewEpisode);

		assertEquals((short) 1, crewEpisodeDAO.findById(id).get().getCrewSeasonId().getCrewId().getEpisodeCount());

	}

	@Test
	public void deletePersonToEpisodeCrewParticipationTest()
			throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException, PermissionException,
			CrewAlreadyExistsException, CrewNotFoundException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		CrewEpisode crewEpisode = new CrewEpisode(null, e1, crewType);

		Long id = crewService.addPersonEpisodeCrewParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), crewEpisode);

		Long episodeCrewId = crewEpisodeDAO.findFirstByOrderByIdAsc().getId();
		crewService.removePersonEpisodeCrewParticipation(user.getId(), episodeCrewId);

		assertEquals((short) 0, crewEpisodeDAO.findById(id).get().getCrewSeasonId().getCrewId().getEpisodeCount());

	}

	@Test
	public void removePersonToEpisodeCrewParticipationFromNonExistantCrewTest() {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CrewNotFoundException.class,
				() -> crewService.removePersonEpisodeCrewParticipation(user.getId(), -1L));

	}

	@Test
	public void findAllCrewTest() throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException,
			PermissionException, CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		People people2 = peopleDAO.save(Utils.createPeople(2L, Visibility.PUBLIC));
		People people3 = peopleDAO.save(Utils.createPeople(3L, Visibility.PUBLIC));

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));

		Crew crew1 = new Crew(tvShow, people1, crewType, (short) 1);
		Crew crew2 = new Crew(tvShow, people2, crewType, (short) 1);
		Crew crew3 = new Crew(tvShow, people3, crewType, (short) 1);

		crewService.addPersonNewCrewParticipation(user.getId(), people1.getId(), tvShow.getId(), crew1);
		crewService.addPersonNewCrewParticipation(user.getId(), people2.getId(), tvShow.getId(), crew2);
		crewService.addPersonNewCrewParticipation(user.getId(), people3.getId(), tvShow.getId(), crew3);

		assertEquals(3, crewService.findAllCrew(tvShow.getId(), LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findAllMainCrewNonExistantTitleTest() {

		assertEquals(0, crewService.findAllCrew(-1L, LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findEpisodeCrewTest() throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException,
			PermissionException, CrewAlreadyExistsException, CrewTypeNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		People people2 = peopleDAO.save(Utils.createPeople(2L, Visibility.PUBLIC));
		People people3 = peopleDAO.save(Utils.createPeople(3L, Visibility.PUBLIC));

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		CrewType crewType = crewTypeDAO.save(new CrewType(1L, "TEST"));
		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		crewService.addPersonEpisodeCrewParticipation(user.getId(), people1.getId(), tvShow.getId(),
				s1.getSeasonNumber(), new CrewEpisode(null, e1, crewType));
		crewService.addPersonEpisodeCrewParticipation(user.getId(), people2.getId(), tvShow.getId(),
				s1.getSeasonNumber(), new CrewEpisode(null, e1, crewType));
		crewService.addPersonEpisodeCrewParticipation(user.getId(), people3.getId(), tvShow.getId(),
				s1.getSeasonNumber(), new CrewEpisode(null, e1, crewType));

		assertEquals(3, crewService.findEpisodeCrew(e1.getId(), LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findEpisodeCrewNonExistantTitleTest() {

		assertEquals(0, crewService.findEpisodeCrew(-1L, LocaleType.DEFAULT_LOCALE.getId()).size());

	}

}
