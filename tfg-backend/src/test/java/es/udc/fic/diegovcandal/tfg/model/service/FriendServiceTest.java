package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.FriendPetitionDao;
import es.udc.fic.diegovcandal.tfg.model.dao.UserFriendOfDao;
import es.udc.fic.diegovcandal.tfg.model.entity.FriendPetition;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.UserFriendOf;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.FriendPetitionCompositeKey;
import es.udc.fic.diegovcandal.tfg.model.entity.compositekey.UserFriendOfCompositeKey;
import es.udc.fic.diegovcandal.tfg.model.exceptions.AlreadyFriendsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionAlreadySentException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.FriendPetitionNotFound;
import es.udc.fic.diegovcandal.tfg.model.exceptions.InstanceNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class FriendServiceTest {

	@Autowired
	private FriendService friendService;

	@Autowired
	private FriendPetitionDao friendPetitionDao;

	@Autowired
	private UserFriendOfDao userFriendOfDao;

	@Autowired
	private UserService userService;

	@Test
	public void sendFriendRequestTest() throws UserAlreadyExistsException, UserNotFoundException,
			FriendPetitionNotFound, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());

		assertNotNull(friendPetitionDao.findById(new FriendPetitionCompositeKey(user1.getId(), user2.getId())));

	}

	@Test
	public void sendFriendRequestThatAlreadyExistsTest() throws UserAlreadyExistsException, UserNotFoundException,
			FriendPetitionNotFound, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());

		assertThrows(FriendPetitionAlreadySentException.class,
				() -> friendService.sendFriendRequest(user1.getId(), user2.getId()));

	}

	@Test
	public void sendFriendRequestFromNonExistentOriginTest() throws UserAlreadyExistsException, UserNotFoundException,
			FriendPetitionNotFound, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user = createAndSignUpUser("user");

		assertThrows(UserNotFoundException.class, () -> friendService.sendFriendRequest(-1L, user.getId()));

	}

	@Test
	public void sendFriendRequestFromNonExistentDestinationTest() throws UserAlreadyExistsException,
			UserNotFoundException, FriendPetitionNotFound, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user = createAndSignUpUser("user");

		assertThrows(UserNotFoundException.class, () -> friendService.sendFriendRequest(user.getId(), -1L));

	}

	@Test
	public void sendFriendRequestAlreadyFriendsTest() throws UserAlreadyExistsException, UserNotFoundException,
			FriendPetitionNotFound, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		userFriendOfDao.save(new UserFriendOf(user1, user2, LocalDateTime.now()));
		userFriendOfDao.save(new UserFriendOf(user2, user1, LocalDateTime.now()));

		assertThrows(AlreadyFriendsException.class,
				() -> friendService.sendFriendRequest(user1.getId(), user2.getId()));
		assertThrows(AlreadyFriendsException.class,
				() -> friendService.sendFriendRequest(user2.getId(), user1.getId()));
	}

	@Test
	public void sendFriendRequestAlreadySentFromTheDestinationTest() throws UserAlreadyExistsException,
			UserNotFoundException, FriendPetitionNotFound, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());
		friendService.sendFriendRequest(user2.getId(), user1.getId());

		assertNotNull(userFriendOfDao.findById(new UserFriendOfCompositeKey(user1.getId(), user2.getId())));
	}

	@Test
	public void acceptFriendRequestTest() throws FriendPetitionNotFound, UserNotFoundException,
			FriendPetitionAlreadySentException, AlreadyFriendsException, UserAlreadyExistsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());

		friendService.acceptFriendRequest(user1.getId(), user2.getId());

		assertNotNull(userFriendOfDao.findById(new UserFriendOfCompositeKey(user1.getId(), user2.getId())));
	}

	@Test
	public void acceptFriendRequestDeletesPetitionTest() throws FriendPetitionNotFound, UserNotFoundException,
			FriendPetitionAlreadySentException, AlreadyFriendsException, UserAlreadyExistsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());

		friendService.acceptFriendRequest(user1.getId(), user2.getId());

		Optional<FriendPetition> petition = friendPetitionDao
				.findById(new FriendPetitionCompositeKey(user1.getId(), user2.getId()));

		assertTrue(!petition.isPresent());
	}

	@Test
	public void acceptNonExistentFriendRequestTest() throws FriendPetitionNotFound, UserNotFoundException,
			FriendPetitionAlreadySentException, AlreadyFriendsException, UserAlreadyExistsException {

		assertThrows(FriendPetitionNotFound.class, () -> friendService.acceptFriendRequest(-1L, -1L));
	}

	@Test
	public void rejectFriendRequestTest() throws FriendPetitionNotFound, UserAlreadyExistsException,
			UserNotFoundException, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());

		friendService.rejectFriendRequest(user1.getId(), user2.getId());

		Optional<UserFriendOf> friendOf = userFriendOfDao
				.findById(new UserFriendOfCompositeKey(user1.getId(), user2.getId()));

		assertTrue(!friendOf.isPresent());
	}

	@Test
	public void rejectFriendRequestDeletesPetitionTest() throws FriendPetitionNotFound, UserAlreadyExistsException,
			UserNotFoundException, FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());

		friendService.rejectFriendRequest(user1.getId(), user2.getId());

		Optional<FriendPetition> petition = friendPetitionDao
				.findById(new FriendPetitionCompositeKey(user1.getId(), user2.getId()));

		assertTrue(!petition.isPresent());
	}

	@Test
	public void getFriendsTest() throws UserAlreadyExistsException, UserNotFoundException, FriendPetitionNotFound,
			FriendPetitionAlreadySentException, AlreadyFriendsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");
		User user3 = createAndSignUpUser("user3");
		User user4 = createAndSignUpUser("user4");

		friendService.sendFriendRequest(user1.getId(), user2.getId());
		friendService.sendFriendRequest(user3.getId(), user1.getId());
		friendService.sendFriendRequest(user4.getId(), user1.getId());

		friendService.acceptFriendRequest(user1.getId(), user2.getId());
		friendService.acceptFriendRequest(user3.getId(), user1.getId());

		assertEquals(2, friendService.getFriends(user1.getId(), 0, Integer.MAX_VALUE).getTotalElements());
	}

	@Test
	public void getFindNoFriendsTest() throws UserAlreadyExistsException, UserNotFoundException {

		User user1 = createAndSignUpUser("user1");
		assertEquals(0, friendService.getFriends(user1.getId(), 0, Integer.MAX_VALUE).getTotalElements());

	}

	@Test
	public void getFriendsWithNonExistantIdTest() throws UserNotFoundException {

		assertThrows(UserNotFoundException.class, () -> friendService.getFriends(-1L, 0, Integer.MAX_VALUE));

	}

	@Test
	public void deleteFriendTest() throws UserAlreadyExistsException, FriendPetitionAlreadySentException,
			AlreadyFriendsException, InstanceNotFoundException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		friendService.sendFriendRequest(user1.getId(), user2.getId());
		friendService.acceptFriendRequest(user1.getId(), user2.getId());
		friendService.deleteFriend(user1.getId(), user2.getId());

		Optional<UserFriendOf> userFriendOf = userFriendOfDao.findFriendByUsers(user1, user2);

		assertTrue(!userFriendOf.isPresent());
	}

	@Test
	public void deleteFriendWithNonExistantUserTest() throws UserAlreadyExistsException {

		User user = createAndSignUpUser("user");

		assertThrows(UserNotFoundException.class, () -> friendService.deleteFriend(-1L, user.getId()));

	}

	@Test
	public void deleteFriendWithNonExistantFriendTest() throws UserAlreadyExistsException {

		User user = createAndSignUpUser("user");

		assertThrows(UserNotFoundException.class, () -> friendService.deleteFriend(user.getId(), -1L));

	}

	@Test
	public void deleteFriendWithNonExistantRelationshipTest() throws UserAlreadyExistsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");

		assertThrows(InstanceNotFoundException.class, () -> friendService.deleteFriend(user1.getId(), user2.getId()));

	}

	private User createAndSignUpUser(String userName) throws UserAlreadyExistsException {
		User u = Utils.createUser(userName, UserRole.USER);
		userService.signUp(u);
		return u;
	}
}
