package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.DuplicateInstanceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectLoginException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectPasswordException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.InstanceNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserDao userDao;

	@Test
	public void testSignUp() throws UserAlreadyExistsException {

		User user = Utils.createUser();

		userService.signUp(user);

		Optional<User> signedUpUser = userDao.findByUserName("test");

		assertEquals(user, signedUpUser.get());

	}

	@Test
	public void testSignUpDuplicatedUser() throws UserAlreadyExistsException {

		User user = Utils.createUser();

		userService.signUp(user);
		assertThrows(UserAlreadyExistsException.class, () -> userService.signUp(user));

	}

	@Test
	public void testLoginFromUserName() throws UserAlreadyExistsException, IncorrectLoginException {

		User user = Utils.createUser();
		String password = user.getPassword();
		userService.signUp(user);

		User loggedUser = userService.loginFromUserName(user.getUserName(), password);

		assertEquals(user, loggedUser);

	}

	@Test
	public void testLoginFromId() throws UserAlreadyExistsException, InstanceNotFoundException {

		User user = Utils.createUser();
		userService.signUp(user);

		User loggedUser = userService.loginFromId(user.getId());

		assertEquals(user, loggedUser);

	}

	@Test
	public void testLoginFromNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () -> userService.loginFromId(-1L));
	}

	@Test
	public void testLoginWithNonExistentUserName() {
		assertThrows(IncorrectLoginException.class, () -> userService.loginFromUserName("test", "test"));
	}

	@Test
	public void testLoginWithIncorrectPassword() throws DuplicateInstanceException, UserAlreadyExistsException {

		User user = Utils.createUser();
		String password = user.getPassword();
		userService.signUp(user);

		assertThrows(IncorrectLoginException.class,
				() -> userService.loginFromUserName(user.getUserName(), "test" + password));

	}

	@Test
	public void findUserTest() throws UserNotFoundException, UserAlreadyExistsException {

		User user = Utils.createUser();
		userService.signUp(user);

		User foundUser = userService.findUser(user.getId());

		assertEquals(user, foundUser);

	}

	@Test
	public void userNotFoundTest() throws UserNotFoundException, UserAlreadyExistsException {

		User user = Utils.createUser();
		userService.signUp(user);

		assertThrows(UserNotFoundException.class, () -> userService.findUser(-1L));

	}

	@Test
	public void testUpdateProfile()
			throws DuplicateInstanceException, UserAlreadyExistsException, UserNotFoundException {

		User user = Utils.createUser();

		userService.signUp(user);

		user.setCountry('X' + user.getCountry());
		user.setUserName('X' + user.getUserName());
		user.setEmail('X' + user.getEmail());

		userService.updateProfile(user, true);

		User updatedUser = userService.findUser(user.getId());

		assertEquals(user, updatedUser);

	}

	@Test
	public void testUpdateProfileWithNonExistentId() {
		User user = Utils.createUser();
		user.setId(-1L);

		assertThrows(UserNotFoundException.class, () -> userService.updateProfile(user, false));
	}

	@Test
	public void testDeleteProfile()
			throws UserAlreadyExistsException, UserNotFoundException, IncorrectPasswordException {

		User user = Utils.createUser();
		String password = user.getPassword();

		userService.signUp(user);

		userService.deleteProfile(user.getId(), password);

		assertThrows(UserNotFoundException.class, () -> userService.findUser(user.getId()));

	}

	@Test
	public void testDeleteProfileWithNonExistentId() {
		assertThrows(UserNotFoundException.class, () -> userService.deleteProfile(-1L, ""));
	}

	@Test
	public void testDeleteProfileWithIncorrectPassword()
			throws UserAlreadyExistsException, UserNotFoundException, IncorrectPasswordException {

		User user = Utils.createUser();
		String password = user.getPassword();

		userService.signUp(user);

		assertThrows(IncorrectPasswordException.class,
				() -> userService.deleteProfile(user.getId(), "test" + password));

	}

	@Test
	public void testChangePassword()
			throws UserAlreadyExistsException, UserNotFoundException, IncorrectPasswordException {

		User user = Utils.createUser();
		String oldPassword = user.getPassword();
		String newPassword = 'X' + oldPassword;

		userService.signUp(user);

		userService.changePassword(user.getId(), oldPassword, newPassword);

		User updatedUser = userService.findUser(user.getId());

		assertTrue(passwordEncoder.matches(newPassword, updatedUser.getPassword()));

	}

	@Test
	public void testChangePasswordWithNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () -> userService.changePassword(-1L, "X", "Y"));
	}

	@Test
	public void testChangePasswordWithIncorrectPassword() throws UserAlreadyExistsException {

		User user = Utils.createUser();
		String oldPassword = user.getPassword();
		String newPassword = 'X' + oldPassword;

		userService.signUp(user);

		assertThrows(IncorrectPasswordException.class,
				() -> userService.changePassword(user.getId(), 'Y' + oldPassword, newPassword));

	}

}
