package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.PeopleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class PeopleServiceTest {

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private PeopleService peopleService;

	@Autowired
	private UserDao userDao;

	@Test
	public void createPersonTest() throws UserNotFoundException, PermissionException, ExternalIdAlreadyLinkedException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = Utils.createPeople(1L);

		Long id = peopleService.createPerson(user.getId(), people);

		people.setId(id);
		people.setAddedByUser(user);

		assertEquals(people, peopleDAO.findById(id).get());

	}

	@Test
	public void createPersonWithAlreadyExistingExternalIdTest()
			throws UserNotFoundException, PermissionException, ExternalIdAlreadyLinkedException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		peopleService.createPerson(user.getId(), Utils.createPeople(1L));

		assertThrows(ExternalIdAlreadyLinkedException.class,
				() -> peopleService.createPerson(user.getId(), Utils.createPeople(1L)));

	}

	@Test
	public void uptadePersonTest() throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = peopleDAO.save(Utils.createPeople(1L));

		People updatedPeople = Utils.createPeople(1L);

		updatedPeople.setBirthday(updatedPeople.getBirthday().plusDays(1));
		updatedPeople.setDeathday(updatedPeople.getDeathday().plusDays(1));
		updatedPeople.setImage(updatedPeople.getImage() + "test");

		peopleService.uptadePerson(user.getId(), people.getId(), updatedPeople);

		updatedPeople.setId(people.getId());

		assertEquals(updatedPeople, peopleDAO.findById(people.getId()).get());

	}

	@Test
	public void updateNonExistantEpisodeTest()
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		assertThrows(PeopleNotFoundException.class, () -> peopleService.uptadePerson(-1L, -1L, null));

	}

	@Test
	public void updateEpisodeWithTraductionsTest()
			throws UserNotFoundException, PeopleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = peopleDAO.save(Utils.createPeople(1L));

		People updatedPeople = Utils.createPeople(1L);

		updatedPeople.setPeopleTraductions(Utils.createPeopleTraductionSet(people));

		peopleService.uptadePerson(user.getId(), people.getId(), updatedPeople);

		updatedPeople.setId(people.getId());

		assertEquals(updatedPeople.getPeopleTraductions().size(),
				peopleDAO.findById(people.getId()).get().getPeopleTraductions().size());

	}

	@Test
	public void getPersonDetailsTest() throws PeopleNotFoundException, UserNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = Utils.createPeople(1L);
		people.setPeopleTraductions(Utils.createPeopleTraductionSet(people));
		people = peopleDAO.save(people);

		PeopleWithTraduction expected = new PeopleWithTraduction(people,
				people.getPeopleTraductions().stream().findFirst().get());

		assertEquals(expected,
				peopleService.getPersonDetails(user.getId(), people.getId(), LocaleType.DEFAULT_LOCALE.getId()));

	}

	@Test
	public void getNonExistantPersonDetailsTest() throws PeopleNotFoundException {

		assertThrows(PeopleNotFoundException.class,
				() -> peopleService.getPersonDetails(-1L, -1L, LocaleType.DEFAULT_LOCALE.getId()));

	}

	@Test
	public void getPrivatePersonDetailsTest() throws PeopleNotFoundException, UserNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.USER));

		People people = Utils.createPeople(1L);
		people.setPeopleTraductions(Utils.createPeopleTraductionSet(people));
		people.setVisibility(Visibility.PRIVATE);
		Long id = peopleDAO.save(people).getId();

		assertThrows(PeopleNotFoundException.class,
				() -> peopleService.getPersonDetails(user.getId(), id, LocaleType.DEFAULT_LOCALE.getId()));

	}

	@Test
	public void getPrivatePersonDetailsFromUnregisteredUserTest()
			throws PeopleNotFoundException, UserNotFoundException {

		People people = Utils.createPeople(1L);
		people.setPeopleTraductions(Utils.createPeopleTraductionSet(people));
		people.setVisibility(Visibility.PRIVATE);
		Long id = peopleDAO.save(people).getId();

		assertThrows(PeopleNotFoundException.class,
				() -> peopleService.getPersonDetails(null, id, LocaleType.DEFAULT_LOCALE.getId()));

	}

	@Test
	public void getPersonDetailsWithNoTraductionTest() throws PeopleNotFoundException, UserNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = Utils.createPeople(1L);
		people.setPeopleTraductions(Utils.createPeopleTraductionSet(people));
		people = peopleDAO.save(people);

		PeopleWithTraduction expected = new PeopleWithTraduction(people,
				people.getPeopleTraductions().stream().findFirst().get());

		assertEquals(expected, peopleService.getPersonDetails(user.getId(), people.getId(), LocaleType.gl.getId()));

	}

	@Test
	public void changePersonSourceTypetoExternalTest()
			throws UserNotFoundException, PermissionException, PeopleNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));

		peopleService.changeSourceType(user.getId(), people.getId());

		assertEquals(SourceType.EXTERNAL_TMDB, peopleDAO.findById(people.getId()).get().getSourceType());
	}

	@Test
	public void changePersonSourceTypetoInternalTest()
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));

		peopleService.changeSourceType(user.getId(), people.getId());
		peopleService.changeSourceType(user.getId(), people.getId());

		assertEquals(SourceType.INTERNAL, peopleDAO.findById(people.getId()).get().getSourceType());
	}

	@Test
	public void changeSetForDeletePersonSourceTypeTest()
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));

		peopleService.setPersonForDeletion(user.getId(), people.getId(), true);

		people.setSourceType(SourceType.INTERNAL);
		people = peopleDAO.save(people);

		peopleService.changeSourceType(user.getId(), people.getId());

		assertEquals(SourceType.INTERNAL, peopleDAO.findById(people.getId()).get().getSourceType());
	}

	@Test
	public void changePrivatePersonSourceTypeTest()
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PRIVATE));

		peopleService.changeSourceType(user.getId(), people.getId());

		assertEquals(SourceType.INTERNAL, peopleDAO.findById(people.getId()).get().getSourceType());
	}

	@Test
	public void changeNonExistantPersonSourceTypeTest()
			throws PeopleNotFoundException, UserNotFoundException, PermissionException {

		assertThrows(PeopleNotFoundException.class, () -> peopleService.changeSourceType(-1L, -1L));
	}

}
