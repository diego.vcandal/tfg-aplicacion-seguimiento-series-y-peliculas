package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleImage;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class ImageServiceTest {

	@Autowired
	private ImageService imageService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Test
	public void getProfileImageTest() throws UserNotFoundException {

		User user = Utils.createUser();
		String image = "testImage";
		user.setImage(image);

		user = userDao.save(user);

		assertEquals(image, imageService.getProfileImage(user.getId()));

	}

	@Test
	public void getHeaderImageTest() throws TitleNotFoundException {

		Title title = Utils.createTitle();
		String image = "testImage";

		Set<TitleImage> images = new HashSet<>(Arrays.asList(new TitleImage(title, image, false, true)));
		title.setTitleImages(images);

		title.setTitleType(TitleType.MOVIE);
		title = titleDAO.save(title);

		assertEquals(image, imageService.getHeaderImage(title.getId()));

	}

	@Test
	public void getHeaderImageFromNonExistantTitleTest() throws TitleNotFoundException {

		assertThrows(TitleNotFoundException.class, () -> imageService.getHeaderImage(-1L));

	}

	@Test
	public void getPortraitImageTest() throws TitleNotFoundException {

		Title title = Utils.createTitle();
		String image = "testImage";

		Set<TitleImage> images = new HashSet<>(Arrays.asList(new TitleImage(title, image, true, false)));
		title.setTitleImages(images);

		title.setTitleType(TitleType.MOVIE);
		title = titleDAO.save(title);

		assertEquals(image, imageService.getPortraitImage(title.getId()));

	}

	@Test
	public void getPortraitImageFromNonExistantTitleTest() throws TitleNotFoundException {

		assertThrows(TitleNotFoundException.class, () -> imageService.getPortraitImage(-1L));

	}

	@Test
	public void getSeasonImageTest() throws TitleNotFoundException {

		TVShow tvShow = titleDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = Utils.createSeason(1L, tvShow);

		String image = "testImage";
		season.setImage(image);

		season = seasonDAO.save(season);

		assertEquals(image, imageService.getSeasonImage(tvShow.getId(), season.getSeasonNumber()));

	}

	@Test
	public void getSeasonImageFromNonExistantTitleTest() throws TitleNotFoundException {

		assertThrows(TitleNotFoundException.class, () -> imageService.getSeasonImage(-1L, (byte) 1));

	}

	@Test
	public void getSeasonImageFromNonExistantSeasonTest() throws TitleNotFoundException {

		TVShow tvShow = titleDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		assertNull(imageService.getSeasonImage(tvShow.getId(), (byte) 1));

	}

	@Test
	public void getEpisodeImageTest() throws TitleNotFoundException {

		TVShow tvShow = titleDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Episode episode = Utils.createEpisode(1L, season);

		String image = "testImage";
		episode.setImage(image);

		episode = episodeDAO.save(episode);

		assertEquals(image,
				imageService.getEpisodeImage(tvShow.getId(), season.getSeasonNumber(), episode.getEpisodeNumber()));

	}

	@Test
	public void getEpisodeImageFromNonExistantTitleTest() throws TitleNotFoundException {

		assertThrows(TitleNotFoundException.class, () -> imageService.getEpisodeImage(-1L, (byte) 1, (short) 1));

	}

	@Test
	public void getEpisodeImageFromNonExistantSeasonTest() throws TitleNotFoundException {

		TVShow tvShow = titleDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class,
				() -> imageService.getEpisodeImage(tvShow.getId(), (byte) 1, (short) 1));
	}

	@Test
	public void getEpisodeImageFromNonExistantEpisodeTest() throws TitleNotFoundException {

		TVShow tvShow = titleDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));

		assertNull(imageService.getEpisodeImage(tvShow.getId(), season.getSeasonNumber(), (short) 1));

	}

}
