package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleTraductionDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class SearchServiceTest {

	@Autowired
	private SearchService searchService;

	@Autowired
	private UserService userService;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private TitleTraductionDAO titleTraductionDAO;

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private UserDao userDao;

	@Test
	public void findUsers() throws UserAlreadyExistsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");
		User user3 = createAndSignUpUser("user3");
		createAndSignUpUser("testuser1");

		Block<User> expectedBlock = new Block<>(Arrays.asList(user1, user2, user3), false, 1, 3);
		assertEquals(expectedBlock, searchService.findUsers("user", 0, 3));

	}

	@Test
	public void findUsersWithPagination() throws UserAlreadyExistsException {

		User user1 = createAndSignUpUser("user1");
		User user2 = createAndSignUpUser("user2");
		createAndSignUpUser("user3");

		Block<User> expectedBlock = new Block<>(Arrays.asList(user1, user2), true, 2, 3);
		assertEquals(expectedBlock, searchService.findUsers("user", 0, 2));

	}

	@Test
	public void testFindNoUsers() {

		Block<User> expectedUsers = new Block<>(new ArrayList<>(), false, 0, 0);

		assertEquals(expectedUsers, searchService.findUsers("test", 0, 1));

	}

	@Test
	public void findTitlesTest() throws UserNotFoundException {

		Title title1 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "Film", Visibility.PUBLIC));
		Title title2 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "Film 2", Visibility.PUBLIC));
		titleDAO.save(Utils.createTitle(TitleType.MOVIE, "Film 3", Visibility.PRIVATE));
		titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "TV Show 1", Visibility.PUBLIC));

		TitleTraduction tt = titleTraductionDAO
				.save(Utils.createTitleTraduction(title1, "Pelicula", LocaleType.esES.getId()));
		titleTraductionDAO.save(Utils.createTitleTraduction(title1, "Film", LocaleType.en.getId()));
		titleTraductionDAO.save(Utils.createTitleTraduction(title1, "Pelicula", LocaleType.gl.getId()));
		titleTraductionDAO.save(Utils.createTitleTraduction(title2, "Film 2", LocaleType.en.getId()));

		Block<TitleInfo> titles = searchService.findTitles(null, "Film", LocaleType.esES.getId(), 0, 10);

		Block<TitleInfo> expectedBlock = new Block<>(
				Arrays.asList(Utils.createTitleInfo(title1, tt), Utils.createTitleInfo(title2, new TitleTraduction())),
				false, 1, 2);

		assertEquals(expectedBlock, titles);

	}

	@Test
	public void findNoTitlesTest() throws UserNotFoundException {

		Block<TitleInfo> expectedBlock = new Block<>(new ArrayList<>(), false, 0, 0);

		assertEquals(expectedBlock, searchService.findTitles(null, "Test", LocaleType.esES.getId(), 0, 10));

	}

	@Test
	public void findTitlesWithPagination() throws UserNotFoundException {

		Title title1 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "Film", Visibility.PUBLIC));
		titleDAO.save(Utils.createTitle(TitleType.MOVIE, "Film 2", Visibility.PUBLIC));
		titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "TV Show 1", Visibility.PUBLIC));
		titleDAO.save(Utils.createTitle(TitleType.TV_SHOW, "Film 3", Visibility.PRIVATE));

		Block<TitleInfo> expectedBlock = new Block<>(
				Arrays.asList(Utils.createTitleInfo(title1, new TitleTraduction())), true, 2, 2);

		assertEquals(expectedBlock, searchService.findTitles(null, "Film", LocaleType.esES.getId(), 0, 1));

	}

	@Test
	public void findPeople() throws UserAlreadyExistsException, UserNotFoundException {

		User user1 = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = Utils.createPeople(1L);
		People people2 = Utils.createPeople(2L);
		People people3 = Utils.createPeople(3L);

		Set<PeopleTraduction> traductions1 = new HashSet<>();
		traductions1.add(new PeopleTraduction(people1, 0L, "test1", "test", "test", true));
		traductions1.add(new PeopleTraduction(people1, 1L, "test1", "test", "test", false));
		people1.setPeopleTraductions(traductions1);

		Set<PeopleTraduction> traductions2 = new HashSet<>();
		traductions2.add(new PeopleTraduction(people2, 3L, "test2", "test", "test", true));
		people2.setPeopleTraductions(traductions2);

		people3.setOriginalName("name");

		people1 = peopleDAO.save(people1);
		people2 = peopleDAO.save(people2);
		people3 = peopleDAO.save(people3);

		Block<People> expectedBlock = new Block<>(Arrays.asList(people1, people2), false, 1, 2);

		assertEquals(expectedBlock, searchService.findPeople(user1.getId(), "test", 0, 2));

	}

	@Test
	public void findPeopleWithPagination() throws UserAlreadyExistsException, UserNotFoundException {

		User user1 = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = Utils.createPeople(1L);
		People people2 = Utils.createPeople(2L);

		Set<PeopleTraduction> traductions1 = new HashSet<>();
		traductions1.add(new PeopleTraduction(people1, 0L, "test 1", "test", "test", true));
		traductions1.add(new PeopleTraduction(people1, 1L, "test 1", "test", "test", false));
		people1.setPeopleTraductions(traductions1);

		Set<PeopleTraduction> traductions2 = new HashSet<>();
		traductions2.add(new PeopleTraduction(people2, 3L, "test 2", "test", "test", true));
		people2.setPeopleTraductions(traductions2);

		people1 = peopleDAO.save(people1);
		people2 = peopleDAO.save(people2);

		Block<People> expectedBlock = new Block<>(Arrays.asList(people1), true, 2, 2);
		assertEquals(expectedBlock, searchService.findPeople(user1.getId(), "test", 0, 1));

	}

	@Test
	public void testFindNoPeople() throws UserNotFoundException {

		Block<People> expectedUsers = new Block<>(new ArrayList<>(), false, 0, 0);

		assertEquals(expectedUsers, searchService.findPeople(null, "test", 0, 1));

	}

	private User createAndSignUpUser(String userName) throws UserAlreadyExistsException {
		User u = Utils.createUser(userName, UserRole.USER);
		userService.signUp(u);
		return u;
	}

}
