package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CustomListDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CustomListLikeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CustomListTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListLike;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomListTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CustomListNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ListElementsLimitExceededException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PrivateResourceException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CustomListServiceTest {

	@Autowired
	private CustomListService customListService;

	@Autowired
	private CustomListDAO customListDAO;

	@Autowired
	private CustomListTitleDAO customListTitleDAO;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private CustomListLikeDAO customListLikeDAO;

	@Autowired
	private UserDao userDao;

	@Test
	public void getPublicCustomListTest()
			throws UserNotFoundException, PrivateResourceException, CustomListNotFoundException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		assertEquals(customList, customListService
				.getCustomList(user.getId(), customList.getId(), LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void getPublicCustomListFromAnonymousUserTest()
			throws UserNotFoundException, PrivateResourceException, CustomListNotFoundException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		assertEquals(customList, customListService
				.getCustomList(null, customList.getId(), LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void getPublicCustomListWithLikeTest()
			throws UserNotFoundException, PrivateResourceException, CustomListNotFoundException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListLikeDAO.save(new CustomListLike(customList, user));

		assertTrue(customListService.getCustomList(user.getId(), customList.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().isLiked());

	}

	@Test
	public void getPublicCustomListTraductedTest()
			throws UserNotFoundException, PrivateResourceException, CustomListNotFoundException {

		User user = userDao.save(Utils.createUser());

		Title title = Utils.createTitle(TitleType.TV_SHOW, "test", Visibility.PUBLIC);
		Set<TitleTraduction> titleTraductions = Utils.createTitleTraductionSet(title);
		title.setTitleTraductions(titleTraductions);
		title = titleDAO.save(title);

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		customListTitleDAO.save(new CustomListTitle(customList, title, 1));

		assertNotNull(
				customListService.getCustomList(user.getId(), customList.getId(), LocaleType.DEFAULT_LOCALE.getId())
						.getTitles().get(0).getTitleTraduction());

	}

	@Test
	public void getUnexistantCustomListTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(CustomListNotFoundException.class, () -> customListService
				.getCustomList(user.getId(), -1L, LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void getPrivateCustomListTest()
			throws UserNotFoundException, PrivateResourceException, CustomListNotFoundException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		assertEquals(customList, customListService
				.getCustomList(user.getId(), customList.getId(), LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void getPrivateCustomListWithNonregisteredUserTest() {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		assertThrows(PrivateResourceException.class, () -> customListService
				.getCustomList(null, customList.getId(), LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void getPrivateCustomListNonValidPermissionTest() {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		CustomList customList = customListDAO.save(Utils.createCustomList(user1));

		assertThrows(PrivateResourceException.class, () -> customListService
				.getCustomList(user2.getId(), customList.getId(), LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void getPrivateCustomListNonExistantUserTest() {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		assertThrows(UserNotFoundException.class, () -> customListService
				.getCustomList(-1L, customList.getId(), LocaleType.DEFAULT_LOCALE.getId()).getCustomList());

	}

	@Test
	public void createCustomListTest() throws UserNotFoundException, TitleNotFoundException {

		User user = userDao.save(Utils.createUser());

		Long id = customListService.createCustomList(user.getId(), Utils.createCustomList(user));

		assertNotNull(customListDAO.findById(id));

	}

	@Test
	public void updateCustomListOrderTest()
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());

		Title title1 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		Title title2 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		Title title3 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		Set<CustomListTitle> customListTitles = new HashSet<>();
		customListTitles.add(new CustomListTitle(customList, title1, 1));
		customListTitles.add(new CustomListTitle(customList, title2, 3));
		customListTitles.add(new CustomListTitle(customList, title3, 2));

		Long id = customListService.updateElementsOrder(user.getId(), customList.getId(), customListTitles);

		assertEquals(3, customListDAO.findById(id).get().getCustomListTitles().size());

	}

	@Test
	public void updateCustomListOrderWithNonCorrectPositionsTest()
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());

		Title title1 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test1", Visibility.PUBLIC));
		Title title2 = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test2", Visibility.PUBLIC));

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		Set<CustomListTitle> customListTitles = new HashSet<>();
		customListTitles.add(new CustomListTitle(customList, title1, 10));
		customListTitles.add(new CustomListTitle(customList, title2, 10));

		Long id = customListService.updateElementsOrder(user.getId(), customList.getId(), customListTitles);

		assertEquals(title2, customListDAO.findById(id).get().getCustomListTitles().stream()
				.filter(e -> e.getPosition() == 2).findFirst().get().getTitleId());

	}

	@Test
	public void updateCustomListOrderWithNonExistantTitleTest() throws UserNotFoundException, TitleNotFoundException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user));

		Title title = new Title();
		title.setId(-1L);

		Set<CustomListTitle> customListTitles = new HashSet<>();
		customListTitles.add(new CustomListTitle(customList, title, 10));

		assertThrows(TitleNotFoundException.class,
				() -> customListService.updateElementsOrder(user.getId(), customList.getId(), customListTitles));

	}

	@Test
	public void updateCustomListOrderListWithNonExistantListTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(CustomListNotFoundException.class,
				() -> customListService.updateElementsOrder(user.getId(), -1L, null));

	}

	@Test
	public void updateCustomListOrderNonValidPermissionTest() {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		CustomList customList = customListDAO.save(Utils.createCustomList(user1));

		assertThrows(PermissionException.class,
				() -> customListService.updateElementsOrder(user2.getId(), customList.getId(), null));

	}

	@Test
	public void updateCustomListTest()
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		customList.setVisibility(Visibility.PRIVATE);
		customList.setTitle(customList.getTitle() + "test");
		customList.setDescription(customList.getDescription() + "test");

		customListService.updateCustomList(user.getId(), customList.getId(), customList);

		assertEquals(customList, customListDAO.findById(customList.getId()).get());

	}

	@Test
	public void updateCustomListWithNullImageTest()
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = Utils.createCustomList(user, Visibility.PUBLIC);
		customList.setImage("test");
		customList = customListDAO.save(customList);

		CustomList newList = Utils.createCustomList(user, Visibility.PUBLIC);
		newList.setImage(null);

		customListService.updateCustomList(user.getId(), customList.getId(), newList);

		assertEquals("test", customListDAO.findById(customList.getId()).get().getImage());

	}

	@Test
	public void updateCustomListUpdateImageTest()
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = Utils.createCustomList(user, Visibility.PUBLIC);
		customList.setImage("test");
		customList = customListDAO.save(customList);

		CustomList newList = Utils.createCustomList(user, Visibility.PUBLIC);
		newList.setImage("newTest");

		customListService.updateCustomList(user.getId(), customList.getId(), newList);

		assertEquals("newTest", customListDAO.findById(customList.getId()).get().getImage());

	}

	@Test
	public void updateCustomListResetImageTest()
			throws UserNotFoundException, TitleNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());

		CustomList customList = Utils.createCustomList(user, Visibility.PUBLIC);
		customList.setImage("test");
		customList = customListDAO.save(customList);

		CustomList newList = Utils.createCustomList(user, Visibility.PUBLIC);
		newList.setImage("");

		customListService.updateCustomList(user.getId(), customList.getId(), newList);

		assertNull(customListDAO.findById(customList.getId()).get().getImage());

	}

	@Test
	public void updateCustomListNonExistantListTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(CustomListNotFoundException.class,
				() -> customListService.updateCustomList(user.getId(), -1L, null));

	}

	@Test
	public void updateCustomListNoPermissionTest() {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		CustomList customList = customListDAO.save(Utils.createCustomList(user1, Visibility.PUBLIC));

		assertThrows(PermissionException.class,
				() -> customListService.updateCustomList(user2.getId(), customList.getId(), customList));

	}

	@Test
	public void makeListPublicTest() throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());
		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		customListService.changeListVisbility(user.getId(), customList.getId());

		assertEquals(Visibility.PUBLIC, customListDAO.findById(customList.getId()).get().getVisibility());

	}

	@Test
	public void makeListPrivateTest() throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());
		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		customListService.changeListVisbility(user.getId(), customList.getId());

		assertEquals(Visibility.PRIVATE, customListDAO.findById(customList.getId()).get().getVisibility());
	}

	@Test
	public void makeListPrivateNonExistantListTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(CustomListNotFoundException.class, () -> customListService.changeListVisbility(user.getId(), -1L));
	}

	@Test
	public void changeListVisbilityNoPermissionTest() {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		CustomList customList = customListDAO.save(Utils.createCustomList(user1, Visibility.PUBLIC));

		assertThrows(PermissionException.class,
				() -> customListService.changeListVisbility(user2.getId(), customList.getId()));

	}

	@Test
	public void deleteCustomListTest() throws UserNotFoundException, CustomListNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser());
		CustomList customList = customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		customListService.deleteCustomList(user.getId(), customList.getId());

		assertFalse(customListDAO.findById(customList.getId()).isPresent());

	}

	@Test
	public void deleteCustomListNonExistantListTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(CustomListNotFoundException.class, () -> customListService.deleteCustomList(user.getId(), -1L));
	}

	@Test
	public void deleteCustomListNoPermissionTest() {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		CustomList customList = customListDAO.save(Utils.createCustomList(user1, Visibility.PUBLIC));

		assertThrows(PermissionException.class,
				() -> customListService.deleteCustomList(user2.getId(), customList.getId()));

	}

	@Test
	public void getCustomListsTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUser());
		customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		assertEquals(2, customListService.getCustomLists(user.getId(), user.getId()).size());

	}

	@Test
	public void getCustomListsFromAnonymousUserTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUser());
		customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		assertEquals(2, customListService.getCustomLists(null, user.getId()).size());

	}

	@Test
	public void getCustomListsWithLikesTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));
		customListLikeDAO.save(new CustomListLike(list, user2));

		assertTrue(customListService.getCustomLists(user2.getId(), user.getId()).stream().findFirst().get().isLiked());

	}

	@Test
	public void getMyCustomListsTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUser());
		customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));
		customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		assertEquals(3, customListService.getMyCustomLists(user.getId()).size());

	}

	@Test
	public void getMyCustomListsFromTitleTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test1", Visibility.PUBLIC));

		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));
		customListTitleDAO.save(new CustomListTitle(list, title, 1));

		assertTrue(customListService.getMyCustomListsFromTitle(user.getId(), title.getId()).stream().findFirst().get()
				.hasTitle());

	}

	@Test
	public void getMyCustomListsFromTitleWithNonExistantTitleTest() throws UserNotFoundException {

		User user = userDao.save(Utils.createUser());

		customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		assertFalse(
				customListService.getMyCustomListsFromTitle(user.getId(), -1L).stream().findFirst().get().hasTitle());

	}

	@Test
	public void addLikeTest()
			throws UserNotFoundException, CustomListNotFoundException, PermissionException, PrivateResourceException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		customListService.addLike(user2.getId(), list.getId());

		assertTrue(customListService.getCustomList(user2.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().isLiked());

	}

	@Test
	public void addLikeAlreadyLikedTest()
			throws UserNotFoundException, CustomListNotFoundException, PermissionException, PrivateResourceException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		customListService.addLike(user2.getId(), list.getId());
		customListService.addLike(user2.getId(), list.getId());

		assertEquals(1, customListService.getCustomList(user2.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().getLikeCount());

	}

	@Test
	public void addLikeNonExistantListTest() {

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		assertThrows(CustomListNotFoundException.class, () -> customListService.addLike(user2.getId(), -1L));

	}

	@Test
	public void addLikePrivateListTestt() {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		assertThrows(PermissionException.class, () -> customListService.addLike(user2.getId(), list.getId()));

	}

	@Test
	public void addLikeSameUserTest() {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		assertThrows(PermissionException.class, () -> customListService.addLike(user.getId(), list.getId()));

	}

	@Test
	public void removeLikeTest()
			throws UserNotFoundException, CustomListNotFoundException, PermissionException, PrivateResourceException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		customListService.addLike(user2.getId(), list.getId());
		customListService.removeLike(user2.getId(), list.getId());

		assertFalse(customListService.getCustomList(user2.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().isLiked());

	}

	@Test
	public void removeLikeUpdatesCountTest()
			throws UserNotFoundException, CustomListNotFoundException, PermissionException, PrivateResourceException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		customListService.addLike(user2.getId(), list.getId());
		customListService.removeLike(user2.getId(), list.getId());

		assertEquals(0, customListService.getCustomList(user2.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().getLikeCount());

	}

	@Test
	public void removeLikeFromNonLikedTest()
			throws UserNotFoundException, CustomListNotFoundException, PermissionException, PrivateResourceException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		customListService.removeLike(user2.getId(), list.getId());

		assertEquals(0, customListService.getCustomList(user2.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().getLikeCount());

	}

	@Test
	public void removeLikeNonExistantListTest() {

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		assertThrows(CustomListNotFoundException.class, () -> customListService.removeLike(user2.getId(), -1L));

	}

	@Test
	public void removeLikePrivateListTest() {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		assertThrows(PermissionException.class, () -> customListService.removeLike(user2.getId(), list.getId()));

	}

	@Test
	public void removeLikeSameUserTest() {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		assertThrows(PermissionException.class, () -> customListService.removeLike(user.getId(), list.getId()));

	}

	@Test
	public void addToListTest() throws TitleNotFoundException, UserNotFoundException, CustomListNotFoundException,
			PermissionException, PrivateResourceException, ListElementsLimitExceededException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test1", Visibility.PUBLIC));

		customListService.addToList(user.getId(), list.getId(), title.getId());

		assertEquals(1, customListService.getCustomList(user.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().getTitleCount());

	}

	@Test
	public void addToListAlreadyAddedTest()
			throws TitleNotFoundException, UserNotFoundException, CustomListNotFoundException, PermissionException,
			PrivateResourceException, ListElementsLimitExceededException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test1", Visibility.PUBLIC));

		customListService.addToList(user.getId(), list.getId(), title.getId());
		customListService.addToList(user.getId(), list.getId(), title.getId());

		assertEquals(1, customListService.getCustomList(user.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().getTitleCount());

	}

	@Test
	public void addToListCorrectPositionTest()
			throws TitleNotFoundException, UserNotFoundException, CustomListNotFoundException, PermissionException,
			PrivateResourceException, ListElementsLimitExceededException {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test1", Visibility.PUBLIC));

		customListService.addToList(user.getId(), list.getId(), title.getId());

		assertEquals(1, customListService.getCustomList(user.getId(), list.getId(), LocaleType.DEFAULT_LOCALE.getId())
				.getCustomList().getCustomListTitles().stream().findFirst().get().getPosition());

	}

	@Test
	public void addToListNonExistantListTest() {

		User user = userDao.save(Utils.createUser("test", UserRole.USER));

		assertThrows(CustomListNotFoundException.class, () -> customListService.addToList(user.getId(), -1L, -1L));

	}

	@Test
	public void addToListNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class, () -> customListService.addToList(user.getId(), list.getId(), -1L));

	}

	@Test
	public void addToListNonValidPermissionTest() {

		User user = userDao.save(Utils.createUser());
		CustomList list = customListDAO.save(Utils.createCustomList(user, Visibility.PRIVATE));

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test1", Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));

		assertThrows(PermissionException.class,
				() -> customListService.addToList(user2.getId(), list.getId(), title.getId()));

	}

}
