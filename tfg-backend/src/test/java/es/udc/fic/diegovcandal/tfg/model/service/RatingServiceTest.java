package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.ReviewTitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectOperationException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.IncorrectRatingException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ReviewNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.Block;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class RatingServiceTest {

	@Autowired
	private UserDao userDao;

	@Autowired
	private TitleDAO titleDAO;

	@Autowired
	private ReviewTitleDAO reviewTitleDAO;

	@Autowired
	private RatingService ratingService;

	@Test
	public void rateTitleTest() throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException,
			IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user.getId(), title.getId(), (byte) 5);

		assertEquals(5, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void rateTitleNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class, () -> ratingService.rateTitle(user.getId(), -1L, (byte) 5));

	}

	@Test
	public void rateTitleNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class, () -> ratingService.rateTitle(-1L, title.getId(), (byte) 5));

	}

	@Test
	public void rateTitleInvalidMaxRatingTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.rateTitle(user.getId(), title.getId(), (byte) -1));

	}

	@Test
	public void rateTitleInvalidMinRatingTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.rateTitle(user.getId(), title.getId(), (byte) 11));

	}

	@Test
	public void rateAlreadyRatedTitleTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user1.getId(), title.getId(), (byte) 5);
		ratingService.rateTitle(user2.getId(), title.getId(), (byte) 10);

		assertEquals(7.5, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void editTitleRatingTest() throws UserNotFoundException, TitleNotFoundException, IncorrectOperationException,
			IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user.getId(), title.getId(), (byte) 5);
		ratingService.editTitleRating(user.getId(), title.getId(), (byte) 7);

		assertEquals(7, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void editTitleRatingNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class, () -> ratingService.editTitleRating(user.getId(), -1L, (byte) 5));

	}

	@Test
	public void editTitleRatingNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class, () -> ratingService.editTitleRating(-1L, title.getId(), (byte) 5));

	}

	@Test
	public void editTitleRatingInvalidMaxRatingTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		ratingService.rateTitle(user.getId(), title.getId(), (byte) 5);

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.editTitleRating(user.getId(), title.getId(), (byte) -1));

	}

	@Test
	public void editTitleRatingInvalidMinRatingTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		ratingService.rateTitle(user.getId(), title.getId(), (byte) 5);

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.editTitleRating(user.getId(), title.getId(), (byte) 11));

	}

	@Test
	public void editUnexistantTitleRatingTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(ReviewNotFoundException.class,
				() -> ratingService.editTitleRating(user.getId(), title.getId(), (byte) 11));

	}

	@Test
	public void editTitleRatingAlreadyRatedTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException, ReviewNotFoundException {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user1.getId(), title.getId(), (byte) 5);
		ratingService.rateTitle(user2.getId(), title.getId(), (byte) 10);
		ratingService.editTitleRating(user2.getId(), title.getId(), (byte) 5);

		assertEquals(5, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void deleteTitleRatingTest() throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user.getId(), title.getId(), (byte) 5);
		ratingService.deleteTitleRating(user.getId(), title.getId());

		assertEquals(0, titleDAO.findById(title.getId()).get().getTimesRated());

	}

	@Test
	public void deleteTitleRatingNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class, () -> ratingService.deleteTitleRating(user.getId(), -1L));

	}

	@Test
	public void deleteTitleRatingNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class, () -> ratingService.deleteTitleRating(-1L, title.getId()));

	}

	@Test
	public void deleteUnexistantTitleRatingTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(ReviewNotFoundException.class, () -> ratingService.deleteTitleRating(user.getId(), title.getId()));

	}

	@Test
	public void deleteTitleRatingAlreadyRatedTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectOperationException, IncorrectRatingException, ReviewNotFoundException {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user1.getId(), title.getId(), (byte) 5);
		ratingService.rateTitle(user2.getId(), title.getId(), (byte) 10);
		ratingService.deleteTitleRating(user2.getId(), title.getId());

		assertEquals(5, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void addLikeTest() throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = Utils.createReviewtitle(title, user, (byte) 5);
		reviewTitle.setContainsReview(true);
		reviewTitleDAO.save(reviewTitle);

		ratingService.addLike(user.getId(), reviewTitle.getId());

		assertEquals(1, reviewTitleDAO.findById(reviewTitle.getId()).get().getLikeCount());

	}

	@Test
	public void addLikeNonExistantUserTest() {

		assertThrows(UserNotFoundException.class, () -> ratingService.addLike(-1L, -1L));

	}

	@Test
	public void addLikeNonExistantRatingTest() {

		User user = userDao.save(Utils.createUser());
		assertThrows(ReviewNotFoundException.class, () -> ratingService.addLike(user.getId(), -1L));

	}

	@Test
	public void addLikeContainsReviewTest() throws UserNotFoundException, TitleNotFoundException,
			ReviewNotFoundException, IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = reviewTitleDAO.save(Utils.createReviewtitle(title, user, (byte) 5));

		assertThrows(IncorrectOperationException.class, () -> ratingService.addLike(user.getId(), reviewTitle.getId()));

	}

	@Test
	public void addLikeAlreadyLikedTest() throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = Utils.createReviewtitle(title, user, (byte) 5);
		reviewTitle.setContainsReview(true);
		reviewTitleDAO.save(reviewTitle);

		ratingService.addLike(user.getId(), reviewTitle.getId());

		assertThrows(IncorrectOperationException.class, () -> ratingService.addLike(user.getId(), reviewTitle.getId()));

	}

	@Test
	public void removeLikeTest() throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = Utils.createReviewtitle(title, user, (byte) 5);
		reviewTitle.setContainsReview(true);
		reviewTitleDAO.save(reviewTitle);

		ratingService.addLike(user.getId(), reviewTitle.getId());
		ratingService.removeLike(user.getId(), reviewTitle.getId());

		assertEquals(0, reviewTitleDAO.findById(reviewTitle.getId()).get().getLikeCount());

	}

	@Test
	public void removeLikeNonExistantUserTest() {

		assertThrows(UserNotFoundException.class, () -> ratingService.removeLike(-1L, -1L));

	}

	@Test
	public void removeLikeNonExistantRatingTest() {

		User user = userDao.save(Utils.createUser());
		assertThrows(ReviewNotFoundException.class, () -> ratingService.removeLike(user.getId(), -1L));

	}

	@Test
	public void removeNoLikeTest() throws UserNotFoundException, TitleNotFoundException, ReviewNotFoundException,
			IncorrectOperationException, IncorrectRatingException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		ReviewTitle reviewTitle = reviewTitleDAO.save(Utils.createReviewtitle(title, user, (byte) 5));

		assertThrows(ReviewNotFoundException.class, () -> ratingService.removeLike(user.getId(), reviewTitle.getId()));

	}

	@Test
	public void addNewReviewTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);

		assertEquals(5, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void addNewReviewAlreadyRatedTest() throws UserNotFoundException, TitleNotFoundException,
			IncorrectRatingException, ReviewNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.rateTitle(user.getId(), title.getId(), (byte) 5);
		ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 10);

		assertEquals(10, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void addNewReviewNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class,
				() -> ratingService.addNewReview(user.getId(), -1L, "test", "test", (byte) 10));

	}

	@Test
	public void addNewReviewNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class,
				() -> ratingService.addNewReview(-1L, title.getId(), "test", "test", (byte) 10));

	}

	@Test
	public void addNewReviewInvalidMaxRatingTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) -1));

	}

	@Test
	public void addNewReviewInvalidMinRatingTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 11));

	}

	@Test
	public void editReviewTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);
		ratingService.editReview(user.getId(), title.getId(), "test", "test", (byte) 10);

		assertEquals(10, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void editReviewNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class,
				() -> ratingService.editReview(user.getId(), -1L, "test", "test", (byte) 10));

	}

	@Test
	public void editReviewNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class,
				() -> ratingService.editReview(-1L, title.getId(), "test", "test", (byte) 10));

	}

	@Test
	public void editReviewNonExistantReviewTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(ReviewNotFoundException.class,
				() -> ratingService.editReview(user.getId(), title.getId(), "test", "test", (byte) 10));

	}

	@Test
	public void editReviewInvalidMaxRatingTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.editReview(user.getId(), title.getId(), "test", "test", (byte) -1));

	}

	@Test
	public void editReviewInvalidMinRatingTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));
		ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);

		assertThrows(IncorrectRatingException.class,
				() -> ratingService.editReview(user.getId(), title.getId(), "test", "test", (byte) 11));

	}

	@Test
	public void deleteReviewTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);
		ratingService.deleteReview(user.getId(), title.getId());

		assertEquals(0, titleDAO.findById(title.getId()).get().getTimesRated());

	}

	@Test
	public void deleteReviewAlreadyRatedTitleTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ratingService.addNewReview(user1.getId(), title.getId(), "test", "test", (byte) 5);
		ratingService.addNewReview(user2.getId(), title.getId(), "test", "test", (byte) 10);

		ratingService.deleteReview(user1.getId(), title.getId());

		assertEquals(10, titleDAO.findById(title.getId()).get().getAvgRating());

	}

	@Test
	public void deleteReviewNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class, () -> ratingService.deleteReview(user.getId(), -1L));

	}

	@Test
	public void deleteReviewNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class, () -> ratingService.deleteReview(-1L, title.getId()));

	}

	@Test
	public void deleteReviewNonExistantReviewTest() {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(ReviewNotFoundException.class, () -> ratingService.deleteReview(user.getId(), title.getId()));

	}

	@Test
	public void getReviewTitleTest()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);

		assertEquals(reviewTitle, ratingService.getReviewTitle(user.getId(), title.getId()));

	}

	@Test
	public void getReviewTitleNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class, () -> ratingService.getReviewTitle(user.getId(), -1L));

	}

	@Test
	public void getReviewTitleNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class, () -> ratingService.getReviewTitle(-1L, title.getId()));

	}

	@Test
	public void getReviewTitleNonExistantReviewTest() throws UserNotFoundException, TitleNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertNull(ratingService.getReviewTitle(user.getId(), title.getId()));

	}

	@Test
	public void getReviews()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);

		Block<ReviewTitle> expectedBlock = new Block<>(Arrays.asList(reviewTitle), false, 1, 1);
		assertEquals(expectedBlock, ratingService.getReviews(title.getId(), null, 0, 1));

	}

	@Test
	public void getLikedReviews() throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException,
			ReviewNotFoundException, IncorrectOperationException {

		User user = userDao.save(Utils.createUser());
		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle = ratingService.addNewReview(user.getId(), title.getId(), "test", "test", (byte) 5);
		ratingService.addLike(user.getId(), reviewTitle.getId());

		reviewTitle.setLiked(true);
		Block<ReviewTitle> expectedBlock = new Block<>(Arrays.asList(reviewTitle), false, 1, 1);
		assertEquals(expectedBlock, ratingService.getReviews(title.getId(), user.getId(), 0, 1));

	}

	@Test
	public void getReviewsWithPagination()
			throws UserNotFoundException, TitleNotFoundException, IncorrectRatingException, ReviewNotFoundException {

		User user1 = userDao.save(Utils.createUser("test1", UserRole.USER));
		User user2 = userDao.save(Utils.createUser("test2", UserRole.USER));
		User user3 = userDao.save(Utils.createUser("test3", UserRole.USER));
		User user4 = userDao.save(Utils.createUser("test4", UserRole.USER));

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		ReviewTitle reviewTitle1 = ratingService.addNewReview(user1.getId(), title.getId(), "test", "test", (byte) 5);
		ReviewTitle reviewTitle2 = ratingService.addNewReview(user2.getId(), title.getId(), "test", "test", (byte) 5);
		ratingService.addNewReview(user3.getId(), title.getId(), "test", "test", (byte) 5);
		ratingService.addNewReview(user4.getId(), title.getId(), "test", "test", (byte) 5);

		Block<ReviewTitle> expectedBlock = new Block<>(Arrays.asList(reviewTitle1, reviewTitle2), true, 2, 4);
		assertEquals(expectedBlock, ratingService.getReviews(title.getId(), null, 0, 2));

	}

	@Test
	public void getReviewsTitleNonExistantTitleTest() {

		User user = userDao.save(Utils.createUser());

		assertThrows(TitleNotFoundException.class, () -> ratingService.getReviews(-1L, user.getId(), 0, 1));

	}

	@Test
	public void getReviewsTitleNonExistantUserTest() {

		Title title = titleDAO.save(Utils.createTitle(TitleType.MOVIE, "test", Visibility.PUBLIC));

		assertThrows(UserNotFoundException.class, () -> ratingService.getReviews(title.getId(), -1L, 0, 1));

	}

}
