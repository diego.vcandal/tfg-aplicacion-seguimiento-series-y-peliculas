package es.udc.fic.diegovcandal.tfg.model.service.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CastSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.Crew;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.CrewType;
import es.udc.fic.diegovcandal.tfg.model.entity.CustomList;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.EpisodeTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.FollowingList;
import es.udc.fic.diegovcandal.tfg.model.entity.Genre;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.PeopleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.ReviewTitle;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.SeasonTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.Title;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleImage;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleInfo;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

public class Utils {

	public static Title createTitle() {
		return new Title(1L, SourceType.INTERNAL, null, "titleTest", "1", "test", (short) 2020);
	}

	public static Title createTitle(TitleType titleType, String titleName, Visibility visibility) {
		Title title = new Title(1L, SourceType.INTERNAL, titleType, titleName, "1", "test", (short) 2020);
		title.setVisibility(visibility);
		return title;
	}

	public static Movie createMovie(Visibility visibility) {
		Movie movie = new Movie(1L, SourceType.INTERNAL, "titleTest", "test", "test", (short) 2021, (short) 100);
		movie.setVisibility(visibility);
		return movie;
	}

	public static TVShow createTVShow(Visibility visibility) {
		TVShow tvShow = new TVShow(createTitle(), (short) 1000, (short) 10, (byte) 10, LocalDate.now(),
				LocalDate.now());
		tvShow.setVisibility(visibility);
		return tvShow;
	}

	public static Season createSeason(Long id, TVShow tvShow) {
		return new Season(id, tvShow, id.byteValue(), "test", "test", LocalDate.now(), LocalDate.now(), null, (short) 1,
				(short) 1, null);
	}

	public static Episode createEpisode(Long id, Season season) {
		return new Episode(id, season, id.shortValue(), "test", "test", LocalDate.now(), (short) 1, null);
	}

	public static User createUser(UserRole role) {
		return createUser("test", role);
	}

	public static User createUser(String userName, UserRole role) {
		return new User("test", "test", userName, "test", role, "test");
	}

	public static User createUser() {
		return new User("test@test.com", "test", "test", "test", UserRole.USER, "test");
	}

	public static User createUserWithFollowingList(Visibility visibility) {
		User user = createUser();
		user.setFollowingList(new FollowingList(user, visibility, LocalDateTime.now(), (short) 0, (short) 0));

		return user;
	}

	public static Genre createGenre() {
		return new Genre(1L, 1L, "test");
	}

	public static TitleTraduction createTitleTraduction(Title title, LocaleType locale) {
		return new TitleTraduction(title, locale.getId(), "test traduction", "test", "test traduction", "test");
	}

	public static TitleTraduction createTitleTraduction(Title title, String titleName, Long localeId) {
		return new TitleTraduction(title, localeId, titleName, "test", "test", "test");
	}

	public static Set<TitleTraduction> createTitleTraductionSet(Title title) {
		Set<TitleTraduction> titleTraductions = new HashSet<>();
		titleTraductions.add(createTitleTraduction(title, LocaleType.DEFAULT_LOCALE));
		return titleTraductions;
	}

	public static Set<TitleImage> createTitleImageSet(Title title) {
		Set<TitleImage> titleImages = new HashSet<>();
		titleImages.add(new TitleImage(title, "test", true, false));
		return titleImages;
	}

	public static TitleInfo createTitleInfo(Title title, TitleTraduction titleTraduction) {
		return new TitleInfo(title.getId(), title.getExternalId(), title.getOriginalTitle(),
				titleTraduction.getTitleName(), titleTraduction.getDescription(), title.getTitleType(),
				title.getSourceType(), title.getOriginalDescription(), title.getYear(), title.getAvgRating(),
				title.getTimesRated());
	}

	public static Set<SeasonTraduction> createSeasonTraductionSet(Season season) {
		Set<SeasonTraduction> seasonTraductions = new HashSet<>();
		seasonTraductions.add(
				new SeasonTraduction(season, LocaleType.DEFAULT_LOCALE.getId(), "title traduction", "desc traduction"));
		return seasonTraductions;
	}

	public static Set<EpisodeTraduction> createEpisodeTraductionSet(Episode episode) {
		Set<EpisodeTraduction> episodeTraductions = new HashSet<>();
		episodeTraductions.add(new EpisodeTraduction(episode, LocaleType.DEFAULT_LOCALE.getId(), "title traduction",
				"desc traduction"));
		return episodeTraductions;
	}

	public static Set<PeopleTraduction> createPeopleTraductionSet(People people) {
		Set<PeopleTraduction> peopleTraductions = new HashSet<>();
		peopleTraductions.add(new PeopleTraduction(people, LocaleType.DEFAULT_LOCALE.getId(), "title traduction",
				"desc traduction", "test", true));
		return peopleTraductions;
	}

	public static People createPeople(Long externalId) {
		return new People(externalId, SourceType.INTERNAL, "test", LocalDate.now(), LocalDate.now(), "test");
	}

	public static People createPeople(Long externalId, Visibility visibility) {
		People p = new People(externalId, SourceType.INTERNAL, "test", LocalDate.now(), LocalDate.now(), "test");
		p.setVisibility(visibility);
		return p;
	}

	public static Cast createCast(Title title, People people) {
		return new Cast(title, people, (short) 0, "test", true);
	}

	public static CastSeason createCastSeason(Cast cast, Season season) {
		return new CastSeason(cast, season, (short) 0);
	}

	public static CastEpisode createCastEpisode(CastSeason castSeason, Episode episode) {
		return new CastEpisode(castSeason, episode, "test");
	}

	public static Crew createCrew(Title title, People people, CrewType crewType) {
		return new Crew(title, people, crewType, (short) 0);
	}

	public static CrewSeason createCrewSeason(Crew crew, Season season) {
		return new CrewSeason(crew, season, (short) 0);
	}

	public static CrewEpisode createCrewEpisode(CrewSeason crewSeason, Episode episode, CrewType crewType) {
		return new CrewEpisode(crewSeason, episode, crewType);
	}

	public static ReviewTitle createReviewtitle(Title title, User user, byte rating) {
		return new ReviewTitle(title, user, rating, false, null, null, (short) 0, LocalDate.now());
	}

	public static CustomList createCustomList(User user, Visibility visibility) {
		return new CustomList(user, visibility, LocalDateTime.now(), "test", "test", 0, 0, null);
	}

	public static CustomList createCustomList(User user) {
		return createCustomList(user, Visibility.PRIVATE);
	}

}
