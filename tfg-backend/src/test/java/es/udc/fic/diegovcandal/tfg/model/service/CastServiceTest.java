package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.CastDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.CastEpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.PeopleDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.Cast;
import es.udc.fic.diegovcandal.tfg.model.entity.CastEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.CastSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.People;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastAlreadyExistsException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.CastNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PeopleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CastServiceTest {

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private CastService castService;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private CastDAO castDAO;

	@Autowired
	private CastEpisodeDAO castEpisodeDAO;

	@Test
	public void addPersonCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s1));
		episodeDAO.save(Utils.createEpisode(3L, s2));

		Cast cast = Utils.createCast(tvShow, people);

		Set<CastSeason> castSeasons = new HashSet<>();
		Set<CastEpisode> castEpisodes = new HashSet<>();

		CastSeason castSeason = Utils.createCastSeason(cast, s1);
		castEpisodes.add(Utils.createCastEpisode(castSeason, e1));
		castEpisodes.add(Utils.createCastEpisode(castSeason, e2));
		castSeasons.add(castSeason);

		castSeason.setCastEpisodes(castEpisodes);
		cast.setCastSeasons(castSeasons);

		Long id = castService.addPersonNewCastParticipation(user.getId(), people.getId(), tvShow.getId(), cast);

		assertEquals((short) 2, castDAO.findById(id).get().getEpisodeCount());

	}

	@Test
	public void addPersonCastParticipationWithNonExistantPersonTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(PeopleNotFoundException.class,
				() -> castService.addPersonNewCastParticipation(user.getId(), -1L, -1L, null));

	}

	@Test
	public void addPersonCastParticipationWithNonExistantTitleTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));

		assertThrows(TitleNotFoundException.class,
				() -> castService.addPersonNewCastParticipation(user.getId(), people.getId(), -1L, null));

	}

	@Test
	public void addPersonCastParticipationWithAlreadyExistantCastTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		castDAO.save(Utils.createCast(tvShow, people));

		assertThrows(CastAlreadyExistsException.class,
				() -> castService.addPersonNewCastParticipation(user.getId(), people.getId(), tvShow.getId(), null));

	}

	@Test
	public void updatePersonCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastNotFoundException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s1));
		Episode e3 = episodeDAO.save(Utils.createEpisode(3L, s2));
		Episode e4 = episodeDAO.save(Utils.createEpisode(4L, s1));

		Cast cast = Utils.createCast(tvShow, people);

		Set<CastSeason> castSeasons = new HashSet<>();
		Set<CastEpisode> castEpisodes = new HashSet<>();

		CastSeason castSeason = Utils.createCastSeason(cast, s1);
		castEpisodes.add(Utils.createCastEpisode(castSeason, e1));
		castEpisodes.add(Utils.createCastEpisode(castSeason, e2));
		castSeasons.add(castSeason);

		castSeason.setCastEpisodes(castEpisodes);
		cast.setCastSeasons(castSeasons);

		Long id = castService.addPersonNewCastParticipation(user.getId(), people.getId(), tvShow.getId(), cast);

		cast = Utils.createCast(tvShow, people);

		Set<CastSeason> castSeasons1 = new HashSet<>();
		Set<CastEpisode> castEpisodes1 = new HashSet<>();
		Set<CastEpisode> castEpisodes2 = new HashSet<>();

		CastSeason castSeason1 = Utils.createCastSeason(cast, s1);
		castEpisodes1.add(Utils.createCastEpisode(castSeason1, e1));
		castEpisodes1.add(Utils.createCastEpisode(castSeason1, e2));
		castEpisodes1.add(Utils.createCastEpisode(castSeason1, e4));
		castSeasons1.add(castSeason1);

		CastSeason castSeason2 = Utils.createCastSeason(cast, s2);
		castEpisodes2.add(Utils.createCastEpisode(castSeason2, e3));
		castSeasons1.add(castSeason2);

		castSeason1.setCastEpisodes(castEpisodes1);
		castSeason2.setCastEpisodes(castEpisodes2);
		cast.setCastSeasons(castSeasons1);

		castService.updatePersonCastParticipation(user.getId(), id, cast);

		assertEquals((short) 4, castDAO.findById(id).get().getEpisodeCount());

	}

	@Test
	public void updateNonExistantPersonCastParticipationTest() {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CastNotFoundException.class,
				() -> castService.updatePersonCastParticipation(user.getId(), -1L, null));

	}

	@Test
	public void removePersonCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastNotFoundException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Cast cast = castDAO.save(Utils.createCast(tvShow, people));

		castService.removePersonCastParticipation(user.getId(), cast.getId());

		assertFalse(castDAO.findById(cast.getId()).isPresent());

	}

	@Test
	public void removeNonExistantPersonCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastNotFoundException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CastNotFoundException.class, () -> castService.removePersonCastParticipation(user.getId(), -1L));

	}

	@Test
	public void addPersonToEpisodeCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s1));

		Cast cast = Utils.createCast(tvShow, people);

		Set<CastSeason> castSeasons = new HashSet<>();
		Set<CastEpisode> castEpisodes = new HashSet<>();

		CastSeason castSeason = Utils.createCastSeason(cast, s1);
		castEpisodes.add(Utils.createCastEpisode(castSeason, e1));
		castSeasons.add(castSeason);

		castSeason.setCastEpisodes(castEpisodes);
		cast.setCastSeasons(castSeasons);

		castService.addPersonNewCastParticipation(user.getId(), people.getId(), tvShow.getId(), cast);

		CastEpisode castEpisode = new CastEpisode(null, e2, "test");
		Long id = castService.addPersonEpisodeCastParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), castEpisode);

		assertEquals((short) 2, castEpisodeDAO.findById(id).get().getCastSeasonId().getCastId().getEpisodeCount());

	}

	@Test
	public void addPersonToEpisodeCastParticipationFromNewSeasonTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));
		Episode e2 = episodeDAO.save(Utils.createEpisode(2L, s2));

		Cast cast = Utils.createCast(tvShow, people);

		Set<CastSeason> castSeasons = new HashSet<>();
		Set<CastEpisode> castEpisodes = new HashSet<>();

		CastSeason castSeason = Utils.createCastSeason(cast, s1);
		castEpisodes.add(Utils.createCastEpisode(castSeason, e1));
		castSeasons.add(castSeason);

		castSeason.setCastEpisodes(castEpisodes);
		cast.setCastSeasons(castSeasons);

		castService.addPersonNewCastParticipation(user.getId(), people.getId(), tvShow.getId(), cast);

		CastEpisode castEpisode = new CastEpisode(null, e2, "test");
		Long id = castService.addPersonEpisodeCastParticipation(user.getId(), people.getId(), tvShow.getId(),
				s2.getSeasonNumber(), castEpisode);

		assertEquals((short) 2, castEpisodeDAO.findById(id).get().getCastSeasonId().getCastId().getEpisodeCount());

	}

	@Test
	public void addPersonToEpisodeCastParticipationFromNewTitleTest() throws PeopleNotFoundException,
			UserNotFoundException, TitleNotFoundException, PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		CastEpisode castEpisode = new CastEpisode(null, e1, "test");

		Long id = castService.addPersonEpisodeCastParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), castEpisode);

		assertEquals((short) 1, castEpisodeDAO.findById(id).get().getCastSeasonId().getCastId().getEpisodeCount());

	}

	@Test
	public void updatePersonToEpisodeCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastAlreadyExistsException, CastNotFoundException {

		String charName = "test";
		String newCharName = charName + "test";

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		CastEpisode castEpisode = new CastEpisode(null, e1, charName);

		castService.addPersonEpisodeCastParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), castEpisode);

		Long episodeCastId = castEpisodeDAO.findFirstByOrderByIdAsc().getId();

		castService.updatePersonEpisodeCastParticipation(user.getId(), episodeCastId, newCharName);

		assertEquals(newCharName, castEpisodeDAO.findById(episodeCastId).get().getCharacterName());

	}

	@Test
	public void updatePersonToEpisodeCastParticipationFromNonExistantCastTest() {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CastNotFoundException.class,
				() -> castService.updatePersonEpisodeCastParticipation(user.getId(), -1L, "test"));

	}

	@Test
	public void deletePersonToEpisodeCastParticipationTest() throws PeopleNotFoundException, UserNotFoundException,
			TitleNotFoundException, PermissionException, CastAlreadyExistsException, CastNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		CastEpisode castEpisode = new CastEpisode(null, e1, "test");

		Long id = castService.addPersonEpisodeCastParticipation(user.getId(), people.getId(), tvShow.getId(),
				s1.getSeasonNumber(), castEpisode);

		Long episodeCastId = castEpisodeDAO.findFirstByOrderByIdAsc().getId();
		castService.removePersonEpisodeCastParticipation(user.getId(), episodeCastId);

		assertEquals((short) 0, castEpisodeDAO.findById(id).get().getCastSeasonId().getCastId().getEpisodeCount());

	}

	@Test
	public void removePersonToEpisodeCastParticipationFromNonExistantCastTest() {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(CastNotFoundException.class,
				() -> castService.removePersonEpisodeCastParticipation(user.getId(), -1L));

	}

	@Test
	public void findMainCastTest() throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException,
			PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		People people2 = peopleDAO.save(Utils.createPeople(2L, Visibility.PUBLIC));
		People people3 = peopleDAO.save(Utils.createPeople(3L, Visibility.PUBLIC));

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Cast cast1 = new Cast(tvShow, people1, (short) 1, "test", true);
		Cast cast2 = new Cast(tvShow, people2, (short) 1, "test", true);
		Cast cast3 = new Cast(tvShow, people3, (short) 1, "test", false);

		castService.addPersonNewCastParticipation(user.getId(), people1.getId(), tvShow.getId(), cast1);
		castService.addPersonNewCastParticipation(user.getId(), people2.getId(), tvShow.getId(), cast2);
		castService.addPersonNewCastParticipation(user.getId(), people3.getId(), tvShow.getId(), cast3);

		assertEquals(2, castService.findMainCast(tvShow.getId(), LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findMainCastNonExistantTitleTest() {

		assertEquals(0, castService.findMainCast(-1L, LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findAllCastTest() throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException,
			PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		People people2 = peopleDAO.save(Utils.createPeople(2L, Visibility.PUBLIC));
		People people3 = peopleDAO.save(Utils.createPeople(3L, Visibility.PUBLIC));

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Cast cast1 = new Cast(tvShow, people1, (short) 1, "test", true);
		Cast cast2 = new Cast(tvShow, people2, (short) 1, "test", true);
		Cast cast3 = new Cast(tvShow, people3, (short) 1, "test", false);

		castService.addPersonNewCastParticipation(user.getId(), people1.getId(), tvShow.getId(), cast1);
		castService.addPersonNewCastParticipation(user.getId(), people2.getId(), tvShow.getId(), cast2);
		castService.addPersonNewCastParticipation(user.getId(), people3.getId(), tvShow.getId(), cast3);

		assertEquals(3, castService.findAllCast(tvShow.getId(), LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findAllMainCastNonExistantTitleTest() {

		assertEquals(0, castService.findAllCast(-1L, LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findEpisodeCastTest() throws PeopleNotFoundException, UserNotFoundException, TitleNotFoundException,
			PermissionException, CastAlreadyExistsException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		People people1 = peopleDAO.save(Utils.createPeople(1L, Visibility.PUBLIC));
		People people2 = peopleDAO.save(Utils.createPeople(2L, Visibility.PUBLIC));
		People people3 = peopleDAO.save(Utils.createPeople(3L, Visibility.PUBLIC));

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Episode e1 = episodeDAO.save(Utils.createEpisode(1L, s1));

		castService.addPersonEpisodeCastParticipation(user.getId(), people1.getId(), tvShow.getId(),
				s1.getSeasonNumber(), new CastEpisode(null, e1, "test"));
		castService.addPersonEpisodeCastParticipation(user.getId(), people2.getId(), tvShow.getId(),
				s1.getSeasonNumber(), new CastEpisode(null, e1, "test"));
		castService.addPersonEpisodeCastParticipation(user.getId(), people3.getId(), tvShow.getId(),
				s1.getSeasonNumber(), new CastEpisode(null, e1, "test"));

		assertEquals(3, castService.findEpisodeCast(e1.getId(), LocaleType.DEFAULT_LOCALE.getId()).size());

	}

	@Test
	public void findEpisodeCastNonExistantTitleTest() {

		assertEquals(0, castService.findEpisodeCast(-1L, LocaleType.DEFAULT_LOCALE.getId()).size());

	}

}
