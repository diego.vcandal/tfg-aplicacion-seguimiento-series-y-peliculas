package es.udc.fic.diegovcandal.tfg.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.fic.diegovcandal.tfg.model.dao.EpisodeDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.GenreDao;
import es.udc.fic.diegovcandal.tfg.model.dao.MovieDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.SeasonDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TVShowDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.TitleTraductionDAO;
import es.udc.fic.diegovcandal.tfg.model.dao.UserDao;
import es.udc.fic.diegovcandal.tfg.model.entity.Episode;
import es.udc.fic.diegovcandal.tfg.model.entity.Genre;
import es.udc.fic.diegovcandal.tfg.model.entity.Movie;
import es.udc.fic.diegovcandal.tfg.model.entity.Season;
import es.udc.fic.diegovcandal.tfg.model.entity.TVShow;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleImage;
import es.udc.fic.diegovcandal.tfg.model.entity.TitleTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.User;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.EpisodeWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.SeasonWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalEpisode;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithInternalSeason;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TVShowWithSeasons;
import es.udc.fic.diegovcandal.tfg.model.entity.dto.TitleWithTraduction;
import es.udc.fic.diegovcandal.tfg.model.exceptions.ExternalIdAlreadyLinkedException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.GenreDoesntExistException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.PermissionException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.TitleNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.exceptions.UserNotFoundException;
import es.udc.fic.diegovcandal.tfg.model.service.utils.Utils;
import es.udc.fic.diegovcandal.tfg.model.utils.LocaleType;
import es.udc.fic.diegovcandal.tfg.model.utils.SourceType;
import es.udc.fic.diegovcandal.tfg.model.utils.TitleType;
import es.udc.fic.diegovcandal.tfg.model.utils.UserRole;
import es.udc.fic.diegovcandal.tfg.model.utils.Visibility;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TitleServiceTest {

	@Autowired
	private TitleService titleService;

	@Autowired
	private MovieDAO movieDAO;

	@Autowired
	private TVShowDAO tvShowDAO;

	@Autowired
	private TitleTraductionDAO titleTraductionDAO;

	@Autowired
	private SeasonDAO seasonDAO;

	@Autowired
	private EpisodeDAO episodeDAO;

	@Autowired
	private UserDao userDao;

	@Autowired
	private GenreDao genreDao;

	@Test
	public void findMovieDetailsTest() throws TitleNotFoundException {

		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));
		TitleTraduction titleTraduction = titleTraductionDAO.save(Utils.createTitleTraduction(movie, LocaleType.esES));

		TitleWithTraduction<Movie> expected = new TitleWithTraduction<>(movie, titleTraduction);

		TitleWithTraduction<Movie> movieInfo = movieDAO.findTitleWithTraduction(movie.getId(), LocaleType.esES.getId())
				.get();

		assertEquals(expected, movieInfo);

	}

	@Test
	public void findNonExistantMovieDetailsTest() {

		assertThrows(TitleNotFoundException.class,
				() -> titleService.getMovieDetails(null, -1L, LocaleType.esES.getId()));

	}

	@Test
	public void findPrivateMovieDetailsTest() {

		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PRIVATE));
		assertThrows(TitleNotFoundException.class,
				() -> titleService.getMovieDetails(null, movie.getId(), LocaleType.esES.getId()));

	}

	@Test
	public void findTVShowDetailsTest() throws TitleNotFoundException {

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		TitleTraduction titleTraduction = titleTraductionDAO.save(Utils.createTitleTraduction(tvShow, LocaleType.esES));

		TitleWithTraduction<TVShow> expected = new TitleWithTraduction<>(tvShow, titleTraduction);

		TitleWithTraduction<TVShow> tvShowInfo = tvShowDAO
				.findTitleWithTraduction(tvShow.getId(), LocaleType.esES.getId()).get();

		assertEquals(expected, tvShowInfo);

	}

	@Test
	public void findNonExistantTVShowDetailsTest() {

		assertThrows(TitleNotFoundException.class,
				() -> titleService.getTVShowDetails(null, -1L, LocaleType.esES.getId()));

	}

	@Test
	public void findPrivateTVShowDetailsTest() {
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PRIVATE));
		assertThrows(TitleNotFoundException.class,
				() -> titleService.getTVShowDetails(null, tvShow.getId(), LocaleType.esES.getId()));

	}

	@Test
	public void foundMovieInsteadOfTVShowTest() {
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));
		assertThrows(TitleNotFoundException.class,
				() -> titleService.getTVShowDetails(null, movie.getId(), LocaleType.esES.getId()));

	}

	@Test
	public void foundTVShowInsteadOfMovieTest() {
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		assertThrows(TitleNotFoundException.class,
				() -> titleService.getMovieDetails(null, tvShow.getId(), LocaleType.esES.getId()));

	}

	@Test
	public void findSeasonsTest() throws TitleNotFoundException, UserNotFoundException {

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season s2 = seasonDAO.save(Utils.createSeason(2L, tvShow));

		List<SeasonWithTraduction> expected = Arrays.asList(
				seasonDAO.findSeasonWithTraduction(tvShow, s1.getSeasonNumber(), LocaleType.esES.getId()).get(),
				seasonDAO.findSeasonWithTraduction(tvShow, s2.getSeasonNumber(), LocaleType.esES.getId()).get());

		assertEquals(new TVShowWithSeasons(tvShow, expected),
				titleService.getSeasons(null, tvShow.getId(), LocaleType.esES.getId()));

	}

	@Test
	public void findSeasonsOfNonExistantTvShowTest() throws TitleNotFoundException, UserNotFoundException {
		assertEquals(new TVShowWithSeasons(null, new ArrayList<>()),
				titleService.getSeasons(null, -1L, LocaleType.esES.getId()));
	}

	@Test
	public void findSeasonsOfPrivateTvShowTest() throws TitleNotFoundException, UserNotFoundException {

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PRIVATE));
		assertEquals(new TVShowWithSeasons(null, new ArrayList<>()),
				titleService.getSeasons(null, tvShow.getId(), LocaleType.esES.getId()));
	}

	@Test
	public void getFullSeasonTest() throws TitleNotFoundException, UserNotFoundException {

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season s1 = seasonDAO.save(Utils.createSeason(1L, tvShow));

		episodeDAO.save(Utils.createEpisode(1L, s1));
		episodeDAO.save(Utils.createEpisode(2L, s1));
		episodeDAO.save(Utils.createEpisode(3L, s1));

		List<EpisodeWithTraduction> expectedList = Arrays.asList(
				episodeDAO.findEpisodeWithTraduction(s1, (short) 1, LocaleType.esES.getId()).get(),
				episodeDAO.findEpisodeWithTraduction(s1, (short) 2, LocaleType.esES.getId()).get(),
				episodeDAO.findEpisodeWithTraduction(s1, (short) 3, LocaleType.esES.getId()).get());

		SeasonWithTraduction expected = new SeasonWithTraduction(s1, null, expectedList);

		assertEquals(expected, titleService.getFullSeason(null, tvShow.getId(), (byte) 1, LocaleType.esES.getId()));

	}

	@Test
	public void getFullSeasonExternalTest() throws TitleNotFoundException, UserNotFoundException {

		TVShow tvShow = Utils.createTVShow(Visibility.PUBLIC);
		tvShow.setSourceType(SourceType.EXTERNAL_TMDB);
		tvShow = tvShowDAO.save(tvShow);

		assertTrue(titleService.getFullSeason(null, tvShow.getId(), (byte) 1, LocaleType.esES.getId()).isNoContent());

	}

	@Test
	public void getFullNonExistantSeasonTest() throws TitleNotFoundException {

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		assertThrows(TitleNotFoundException.class,
				() -> titleService.getFullSeason(null, tvShow.getId(), (byte) 1, LocaleType.esES.getId()));

	}

	@Test
	public void getFullSeasonOfNonExistantTVShowTest() throws TitleNotFoundException {

		assertThrows(TitleNotFoundException.class,
				() -> titleService.getFullSeason(null, -1L, (byte) 1, LocaleType.esES.getId()));

	}

	@Test
	public void getFullSeasonOfPrivateTVShowTest() throws TitleNotFoundException {

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PRIVATE));
		assertThrows(TitleNotFoundException.class,
				() -> titleService.getFullSeason(null, tvShow.getId(), (byte) 1, LocaleType.esES.getId()));

	}

	@Test
	public void createMovieTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Genre genre = genreDao.save(Utils.createGenre());

		Movie movie = new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1);

		movie.setTitleTraductions(Utils.createTitleTraductionSet(movie));
		movie.setTitleImages(Utils.createTitleImageSet(movie));
		Set<Genre> genres = new HashSet<>();
		genres.add(genre);
		movie.setGenres(genres);

		Long id = titleService.createMovie(user.getId(), movie);

		movie.setId(id);
		movie.setAddedByUser(user);
		movie.setTitleType(TitleType.MOVIE);

		assertEquals(movie, movieDAO.findById(id).get());

	}

	@Test
	public void createMovieWithNonExistantGenreTest() throws TitleNotFoundException {

		Movie movie = new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1);
		Set<Genre> genres = new HashSet<>();
		genres.add(new Genre(-1L, -1L, "test"));
		movie.setGenres(genres);

		assertThrows(GenreDoesntExistException.class, () -> titleService.createMovie(-1L, movie));

	}

	@Test
	public void createMovieWithNonAdminUserTest() throws TitleNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.USER));
		Movie movie = new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1);

		assertThrows(PermissionException.class, () -> titleService.createMovie(user.getId(), movie));

	}

	@Test
	public void createMovieWithNonExistantUserTest() throws TitleNotFoundException {

		Movie movie = new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1);

		assertThrows(UserNotFoundException.class, () -> titleService.createMovie(-1L, movie));

	}

	@Test
	public void createMovieWithExistantExternalIdTest() throws TitleNotFoundException {

		movieDAO.save(new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1));

		assertThrows(ExternalIdAlreadyLinkedException.class, () -> titleService.createMovie(-1L,
				new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1)));

	}

	@Test
	public void updateMovieTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Genre genre = genreDao.save(Utils.createGenre());

		Movie movie = new Movie(1L, SourceType.INTERNAL, "test", "test", "test", (short) 2021, (short) 1);
		Set<Genre> genres = new HashSet<>();
		genres.add(genre);
		movie.setTitleTraductions(Utils.createTitleTraductionSet(movie));
		movie.setTitleImages(Utils.createTitleImageSet(movie));
		movie.setGenres(genres);

		Long id = titleService.createMovie(user.getId(), movie);

		Movie updatedMovie = new Movie(1L, SourceType.INTERNAL, "test 2", "test 2", "test 2", (short) 2022, (short) 2);
		updatedMovie.setGenres(new HashSet<>());
		updatedMovie.setTitleImages(movie.getTitleImages());
		updatedMovie.setTitleTraductions(new HashSet<>());
		updatedMovie.setId(id);
		updatedMovie.setAddedByUser(user);
		updatedMovie.setTitleType(TitleType.MOVIE);

		titleService.updateMovie(user.getId(), movie.getId(), updatedMovie);

		assertEquals(updatedMovie, movieDAO.findById(id).get());

	}

	@Test
	public void updateNonExistantMovieTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		assertThrows(TitleNotFoundException.class, () -> titleService.updateMovie(1L, -1L, new Movie()));

	}

	@Test
	public void updateMovieWithNonAdminUserTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser("userAdmin", UserRole.ADMIN));
		Long id = titleService.createMovie(user.getId(), Utils.createMovie(Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("user", UserRole.USER));
		assertThrows(PermissionException.class, () -> titleService.updateMovie(user2.getId(), id, new Movie()));

	}

	@Test
	public void updateMovieWithNonExistantGenreTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Long id = titleService.createMovie(user.getId(), Utils.createMovie(Visibility.PUBLIC));

		Movie updatedMovie = new Movie(1L, SourceType.INTERNAL, "test 2", "test 2", "test 2", (short) 2022, (short) 2);

		Set<Genre> genres = new HashSet<>();
		genres.add(new Genre(-1L, -1L, "test"));
		updatedMovie.setGenres(genres);

		assertThrows(GenreDoesntExistException.class, () -> titleService.updateMovie(user.getId(), id, updatedMovie));
	}

	@Test
	public void makeTitlePublicTest() throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		titleService.makeTitlePublic(user.getId(), movie.getId());

		assertEquals(Visibility.PUBLIC, movieDAO.findById(movie.getId()).get().getVisibility());
	}

	@Test
	public void makeUnexistantTitlePublicTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		assertThrows(TitleNotFoundException.class, () -> titleService.makeTitlePublic(-1L, -1L));
	}

	@Test
	public void makeTitlePublicWithNonAdminUserTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.USER));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		assertThrows(PermissionException.class, () -> titleService.makeTitlePublic(user.getId(), movie.getId()));
	}

	@Test
	public void makeAlreadyPublicTitlePublicTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = Utils.createMovie(Visibility.PUBLIC);
		movie.setVisibility(Visibility.PUBLIC);
		movie = movieDAO.save(movie);

		titleService.makeTitlePublic(user.getId(), movie.getId());

		assertEquals(Visibility.PUBLIC, movieDAO.findById(movie.getId()).get().getVisibility());
	}

	@Test
	public void setTitleForDeletionTest() throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		titleService.setTitleForDeletion(user.getId(), movie.getId(), true);

		assertTrue(movieDAO.findById(movie.getId()).get().isSetForDelete());
	}

	@Test
	public void removeTitleForDeletionTest() throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = Utils.createMovie(Visibility.PUBLIC);
		movie.setSetForDelete(true);
		movie = movieDAO.save(movie);

		titleService.setTitleForDeletion(user.getId(), movie.getId(), false);

		assertFalse(movieDAO.findById(movie.getId()).get().isSetForDelete());
	}

	@Test
	public void setUnexistantTitleForDeletionTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		assertThrows(TitleNotFoundException.class, () -> titleService.setTitleForDeletion(-1L, -1L, true));
	}

	@Test
	public void setTitleForDeletionNonAdminUserTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.USER));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		assertThrows(PermissionException.class,
				() -> titleService.setTitleForDeletion(user.getId(), movie.getId(), true));
	}

	@Test
	public void deleteTitleTest() throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		titleService.setTitleForDeletion(user.getId(), movie.getId(), true);
		titleService.deleteTitle(user.getId(), movie.getId());

		assertTrue(movieDAO.findById(movie.getId()).isEmpty());
	}

	@Test
	public void deleteTitleWithoutBeingSetForDeletionTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		assertThrows(PermissionException.class, () -> titleService.deleteTitle(user.getId(), movie.getId()));
	}

	@Test
	public void deleteUnexistantTitleTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		assertThrows(TitleNotFoundException.class, () -> titleService.deleteTitle(-1L, -1L));
	}

	@Test
	public void deleteTitleWithNonAdminUserTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.USER));

		Movie movie = Utils.createMovie(Visibility.PUBLIC);
		movie.setSetForDelete(true);
		Long id = movieDAO.save(movie).getId();

		assertThrows(PermissionException.class, () -> titleService.deleteTitle(user.getId(), id));
	}

	@Test
	public void createTVShowTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Genre genre = genreDao.save(Utils.createGenre());

		TVShow tvShow = new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(),
				LocalDate.now());

		tvShow.setTitleTraductions(Utils.createTitleTraductionSet(tvShow));
		tvShow.setTitleImages(Utils.createTitleImageSet(tvShow));
		Set<Genre> genres = new HashSet<>();
		genres.add(genre);
		tvShow.setGenres(genres);

		Long id = titleService.createTVShow(user.getId(), tvShow);

		tvShow.setId(id);
		tvShow.setAddedByUser(user);
		tvShow.setTitleType(TitleType.TV_SHOW);

		assertEquals(tvShow, tvShowDAO.findById(id).get());

	}

	@Test
	public void createTVShowWithNonExistantGenreTest() throws TitleNotFoundException {

		TVShow tvShow = new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(),
				LocalDate.now());
		Set<Genre> genres = new HashSet<>();
		genres.add(new Genre(-1L, -1L, "test"));
		tvShow.setGenres(genres);

		assertThrows(GenreDoesntExistException.class, () -> titleService.createTVShow(-1L, tvShow));

	}

	@Test
	public void createTVShowWithNonAdminUserTest() throws TitleNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.USER));
		TVShow tvShow = new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(),
				LocalDate.now());

		assertThrows(PermissionException.class, () -> titleService.createTVShow(user.getId(), tvShow));

	}

	@Test
	public void createTVShowWithNonExistantUserTest() throws TitleNotFoundException {

		TVShow tvShow = new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(),
				LocalDate.now());

		assertThrows(UserNotFoundException.class, () -> titleService.createTVShow(-1L, tvShow));

	}

	@Test
	public void createTVShowWithExistantExternalIdTest() throws TitleNotFoundException {

		tvShowDAO.save(
				new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(), LocalDate.now()));

		assertThrows(ExternalIdAlreadyLinkedException.class, () -> titleService.createTVShow(-1L,
				new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(), LocalDate.now())));

	}

	@Test
	public void updateTVShowTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Genre genre = genreDao.save(Utils.createGenre());

		TVShow tvShow = new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 10, LocalDate.now(),
				LocalDate.now());
		Set<Genre> genres = new HashSet<>();
		genres.add(genre);
		tvShow.setTitleTraductions(Utils.createTitleTraductionSet(tvShow));
		tvShow.setTitleImages(Utils.createTitleImageSet(tvShow));
		tvShow.setGenres(genres);

		Long id = titleService.createTVShow(user.getId(), tvShow);

		TVShow updatedTVShow = new TVShow(Utils.createTitle(), (short) 13, (short) 13, (byte) 10,
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(1));
		updatedTVShow.setGenres(new HashSet<>());
		updatedTVShow.setTitleImages(tvShow.getTitleImages());
		updatedTVShow.setTitleTraductions(new HashSet<>());
		updatedTVShow.setId(id);
		updatedTVShow.setAddedByUser(user);
		updatedTVShow.setTitleType(TitleType.TV_SHOW);

		titleService.updateTVShow(user.getId(), tvShow.getId(), updatedTVShow);

		updatedTVShow.setTotalDuration((short) 10);
		updatedTVShow.setEpisodeCount((short) 10);

		assertEquals(updatedTVShow, tvShowDAO.findById(id).get());

	}

	@Test
	public void updateNonExistantTVShowTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		assertThrows(TitleNotFoundException.class, () -> titleService.updateTVShow(1L, -1L, new TVShow()));

	}

	@Test
	public void updateTVShowWithNonAdminUserTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser("userAdmin", UserRole.ADMIN));
		Long id = titleService.createTVShow(user.getId(), Utils.createTVShow(Visibility.PUBLIC));

		User user2 = userDao.save(Utils.createUser("user", UserRole.USER));
		assertThrows(PermissionException.class, () -> titleService.updateTVShow(user2.getId(), id, new TVShow()));

	}

	@Test
	public void updateTVShowWithNonExistantGenreTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Long id = titleService.createTVShow(user.getId(), Utils.createTVShow(Visibility.PUBLIC));

		TVShow updatedTvShow = new TVShow(Utils.createTitle(), (short) 13, (short) 13, (byte) 10,
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(1));

		Set<Genre> genres = new HashSet<>();
		genres.add(new Genre(-1L, -1L, "test"));
		updatedTvShow.setGenres(genres);

		assertThrows(GenreDoesntExistException.class, () -> titleService.updateTVShow(user.getId(), id, updatedTvShow));
	}

	@Test
	public void getLastSeasonPreviewTest() throws UserNotFoundException, ExternalIdAlreadyLinkedException,
			PermissionException, GenreDoesntExistException, TitleNotFoundException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		TVShow tvShow = new TVShow(Utils.createTitle(), (short) 10, (short) 10, (byte) 1, LocalDate.now(),
				LocalDate.now());
		Long id = titleService.createTVShow(user.getId(), tvShow);
		titleService.makeTitlePublic(user.getId(), tvShow.getId());

		Season season = new Season(1L, tvShow, (byte) 1, "test", "test", LocalDate.now(), LocalDate.now(), "test",
				(short) 10, (short) 10, null);

		season = seasonDAO.save(season);

		TVShowWithSeasons tvShowWithSeasons = new TVShowWithSeasons(tvShow,
				Arrays.asList(new SeasonWithTraduction(season, null)));

		assertEquals(tvShowWithSeasons, titleService.getLastSeasonPreview(null, id, LocaleType.DEFAULT_LOCALE.getId()));
	}

	@Test
	public void changeTitleSourceTypetoExternalTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		titleService.changeSourceType(user.getId(), movie.getId());

		assertEquals(SourceType.EXTERNAL_TMDB, movieDAO.findById(movie.getId()).get().getSourceType());
	}

	@Test
	public void changeTitleSourceTypetoInternalTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		titleService.changeSourceType(user.getId(), movie.getId());
		titleService.changeSourceType(user.getId(), movie.getId());

		assertEquals(SourceType.INTERNAL, movieDAO.findById(movie.getId()).get().getSourceType());
	}

	@Test
	public void changeSetForDeleteTitleSourceTypeTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		titleService.setTitleForDeletion(user.getId(), movie.getId(), true);

		movie.setSourceType(SourceType.INTERNAL);
		movie = movieDAO.save(movie);

		titleService.changeSourceType(user.getId(), movie.getId());

		assertEquals(SourceType.INTERNAL, movieDAO.findById(movie.getId()).get().getSourceType());
	}

	@Test
	public void changePrivateTitleSourceTypeTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PRIVATE));

		titleService.changeSourceType(user.getId(), tvShow.getId());

		assertEquals(SourceType.INTERNAL, tvShowDAO.findById(tvShow.getId()).get().getSourceType());
	}

	@Test
	public void changeNonExistantTitleSourceTypeTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		assertThrows(TitleNotFoundException.class, () -> titleService.changeSourceType(-1L, -1L));
	}

	@Test
	public void changeTVShowSourceTypeWithSeasonsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = Utils.createTVShow(Visibility.PUBLIC);

		tvShow.setSeasons(new HashSet<>(Arrays.asList(Utils.createSeason(1L, tvShow), Utils.createSeason(2L, tvShow))));

		tvShow = tvShowDAO.save(tvShow);

		titleService.changeSourceType(user.getId(), tvShow.getId());

		assertEquals((byte) 2, tvShowDAO.findById(tvShow.getId()).get().getSeasonNumber());
	}

	@Test
	public void changeTVShowSourceTypeWithSeasonsAndEpisodesTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = Utils.createTVShow(Visibility.PUBLIC);

		Season s1 = Utils.createSeason(1L, tvShow);
		Season s2 = Utils.createSeason(2L, tvShow);

		s1.setEpisodes(new HashSet<>(
				Arrays.asList(Utils.createEpisode(1L, s1), Utils.createEpisode(2L, s1), Utils.createEpisode(3L, s1))));
		s2.setEpisodes(new HashSet<>(Arrays.asList(Utils.createEpisode(4L, s2), Utils.createEpisode(5L, s2))));

		tvShow.setSeasons(new HashSet<>(Arrays.asList(s1, s2)));

		tvShow = tvShowDAO.save(tvShow);

		titleService.changeSourceType(user.getId(), tvShow.getId());

		assertEquals((short) 5, tvShowDAO.findById(tvShow.getId()).get().getEpisodeCount());
	}

	@Test
	public void updateSeasonTest() throws UserNotFoundException, TitleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season updatedSeason = Utils.createSeason(1L, tvShow);

		updatedSeason.setImage(updatedSeason.getImage() + "test");
		updatedSeason.setOriginalTitle(updatedSeason.getOriginalTitle() + "test");
		updatedSeason.setOriginalDescription(updatedSeason.getOriginalDescription() + "test");
		updatedSeason.setFirstAirDate(updatedSeason.getFirstAirDate().plusDays(1));
		updatedSeason.setLastAirDate(updatedSeason.getLastAirDate().plusDays(1));
		updatedSeason.setSeasonNumber((byte) (updatedSeason.getSeasonNumber() + 1));

		titleService.updateSeason(user.getId(), season.getId(), updatedSeason);

		updatedSeason.setId(season.getId());

		assertEquals(updatedSeason, seasonDAO.findById(season.getId()).get());

	}

	@Test
	public void updateNonExistantSeasonTest()
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		assertThrows(TitleNotFoundException.class, () -> titleService.updateSeason(-1L, -1L, null));

	}

	@Test
	public void updateSeasonWithTraductionsTest()
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Season updatedSeason = Utils.createSeason(1L, tvShow);

		updatedSeason.setSeasonTraductions(Utils.createSeasonTraductionSet(season));

		titleService.updateSeason(user.getId(), season.getId(), updatedSeason);

		assertEquals(updatedSeason.getSeasonTraductions().size(),
				seasonDAO.findById(season.getId()).get().getSeasonTraductions().size());

	}

	@Test
	public void createSeasonTest() throws UserNotFoundException, TitleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		Season season = Utils.createSeason(1L, tvShow);

		Long id = titleService.createSeason(user.getId(), tvShow.getId(), season);

		season.setId(id);

		assertEquals(season, seasonDAO.findById(id).get());

	}

	@Test
	public void createSeasonNonExistantTitleTest()
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		assertThrows(TitleNotFoundException.class, () -> titleService.createSeason(-1L, -1L, null));

	}

	@Test
	public void updateEpisodeTest() throws UserNotFoundException, TitleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode episode = episodeDAO.save(Utils.createEpisode(1L, season));
		Episode updatedEpisode = Utils.createEpisode(1L, season);

		updatedEpisode.setImage(updatedEpisode.getImage() + "test");
		updatedEpisode.setOriginalTitle(updatedEpisode.getOriginalTitle() + "test");
		updatedEpisode.setOriginalDescription(updatedEpisode.getOriginalDescription() + "test");
		updatedEpisode.setAirDate(updatedEpisode.getAirDate().plusDays(1));
		updatedEpisode.setEpisodeNumber((short) (updatedEpisode.getEpisodeNumber() + 1));

		titleService.updateEpisode(user.getId(), episode.getId(), updatedEpisode);

		updatedEpisode.setId(episode.getId());

		assertEquals(updatedEpisode, episodeDAO.findById(episode.getId()).get());

	}

	@Test
	public void updateNonExistantEpisodeTest()
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		assertThrows(TitleNotFoundException.class, () -> titleService.updateEpisode(-1L, -1L, null));

	}

	@Test
	public void updateEpisodeWithTraductionsTest()
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode episode = episodeDAO.save(Utils.createEpisode(1L, season));
		Episode updatedEpisode = Utils.createEpisode(1L, season);

		updatedEpisode.setEpisodeTraductions(Utils.createEpisodeTraductionSet(episode));

		titleService.updateEpisode(user.getId(), episode.getId(), updatedEpisode);

		assertEquals(updatedEpisode.getEpisodeTraductions().size(),
				episodeDAO.findById(episode.getId()).get().getEpisodeTraductions().size());

	}

	@Test
	public void createEpisodeTest() throws UserNotFoundException, TitleNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));

		Episode episode = Utils.createEpisode(1L, season);

		Long id = titleService.createEpisode(user.getId(), tvShow.getId(), season.getSeasonNumber(), episode);

		episode.setId(id);

		assertEquals(episode, episodeDAO.findById(id).get());

	}

	@Test
	public void createEpisodeWithNonExistantTitleTest()
			throws UserNotFoundException, TitleNotFoundException, PermissionException {

		assertThrows(TitleNotFoundException.class, () -> titleService.createEpisode(-1L, -1L, (byte) 1, null));

	}

	@Test
	public void getInternalSeasonDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));

		season.setSourceType(season.getTvShow().getSourceType());
		TVShowWithInternalSeason expected = new TVShowWithInternalSeason(tvShow.getOriginalTitle(), season);

		assertEquals(expected, titleService.getInternalSeasonDetails(season.getId(), user.getId()));

	}

	@Test
	public void getNonExistantInternalSeasonDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(TitleNotFoundException.class, () -> titleService.getInternalSeasonDetails(-1L, user.getId()));

	}

	@Test
	public void getInternalEpisodeDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Episode episode = episodeDAO.save(Utils.createEpisode(1L, season));

		episode.setSourceType(season.getTvShow().getSourceType());
		TVShowWithInternalEpisode expected = new TVShowWithInternalEpisode(tvShow.getOriginalTitle(),
				season.getOriginalTitle(), episode);

		assertEquals(expected, titleService.getInternalEpisodeDetails(episode.getId(), user.getId()));

	}

	@Test
	public void getNonExistantInternalEpisodeDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(TitleNotFoundException.class, () -> titleService.getInternalEpisodeDetails(-1L, user.getId()));

	}

	@Test
	public void setSeasonForDeleteTest() throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));

		titleService.setSeasonForDeletion(user.getId(), season.getId(), true);

		assertTrue(seasonDAO.findById(season.getId()).get().isSetForDelete());
	}

	@Test
	public void setEpisodeForDeleteTest() throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));
		Season season = seasonDAO.save(Utils.createSeason(1L, tvShow));
		Episode episode = episodeDAO.save(Utils.createEpisode(1L, season));

		titleService.setEpisodeForDeletion(user.getId(), episode.getId(), true);

		assertTrue(episodeDAO.findById(episode.getId()).get().isSetForDelete());
	}

	@Test
	public void updateMovieImagesTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		Movie movie = Utils.createMovie(Visibility.PUBLIC);
		Set<TitleImage> titleImages = new HashSet<>();
		titleImages.add(new TitleImage(movie, "test", true, false));
		titleImages.add(new TitleImage(movie, "test", false, true));
		movie.setTitleImages(titleImages);

		Long id = titleService.createMovie(user.getId(), movie);

		Movie updatedMovie = Utils.createMovie(Visibility.PUBLIC);

		for (TitleImage titleImage : titleImages) {
			titleImage.setImage(null);
		}

		updatedMovie.setTitleImages(titleImages);

		titleService.updateMovie(user.getId(), movie.getId(), updatedMovie);

		assertEquals(updatedMovie.getTitleImages(), movieDAO.findById(id).get().getTitleImages());

	}

	@Test
	public void updateMovieWithNoHeaderAndPortraitImagesTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		Movie movie = Utils.createMovie(Visibility.PUBLIC);

		Long id = titleService.createMovie(user.getId(), movie);

		Movie updatedMovie = Utils.createMovie(Visibility.PUBLIC);

		Set<TitleImage> newTitleImages = new HashSet<>();
		newTitleImages.add(new TitleImage(movie, "test", true, false));
		newTitleImages.add(new TitleImage(movie, "test", false, true));
		updatedMovie.setTitleImages(newTitleImages);

		titleService.updateMovie(user.getId(), movie.getId(), updatedMovie);

		assertEquals(updatedMovie.getTitleImages().size(), movieDAO.findById(id).get().getTitleImages().size());

	}

	@Test
	public void updateMovieTraductionsTest() throws TitleNotFoundException, UserNotFoundException,
			ExternalIdAlreadyLinkedException, PermissionException, GenreDoesntExistException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		Movie movie = Utils.createMovie(Visibility.PUBLIC);
		movie.setTitleTraductions(new HashSet<>());

		Long id = titleService.createMovie(user.getId(), movie);

		Movie updatedMovie = Utils.createMovie(Visibility.PUBLIC);

		updatedMovie.setTitleTraductions(Utils.createTitleTraductionSet(movie));

		titleService.updateMovie(user.getId(), movie.getId(), updatedMovie);

		assertEquals(updatedMovie.getTitleTraductions().size(),
				movieDAO.findById(id).get().getTitleTraductions().size());

	}

	@Test
	public void getInternalMovieDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		Movie movie = movieDAO.save(Utils.createMovie(Visibility.PUBLIC));

		assertEquals(movie, titleService.getInternalMovieDetails(movie.getId(), user.getId()));

	}

	@Test
	public void getNonExistantInternalMovieDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(TitleNotFoundException.class, () -> titleService.getInternalMovieDetails(-1L, user.getId()));

	}

	@Test
	public void getInternalTVShowDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));
		TVShow tvShow = tvShowDAO.save(Utils.createTVShow(Visibility.PUBLIC));

		assertEquals(tvShow, titleService.getInternalTVShowDetails(tvShow.getId(), user.getId()));

	}

	@Test
	public void getNonExistantInternalTVShowDetailsTest()
			throws TitleNotFoundException, UserNotFoundException, PermissionException {

		User user = userDao.save(Utils.createUser(UserRole.ADMIN));

		assertThrows(TitleNotFoundException.class, () -> titleService.getInternalTVShowDetails(-1L, user.getId()));

	}

}
