DROP TABLE IF EXISTS CustomListLike;
DROP TABLE IF EXISTS CustomListTitle;
DROP TABLE IF EXISTS CustomList;

DROP TABLE IF EXISTS FollowingListTitle;
DROP TABLE IF EXISTS FollowingList;

DROP TABLE IF EXISTS ReviewTitleLike;
DROP TABLE IF EXISTS ReviewTitle;

DROP TABLE IF EXISTS CastEpisode;
DROP TABLE IF EXISTS CastSeason;
DROP TABLE IF EXISTS Cast;

DROP TABLE IF EXISTS CrewEpisode;
DROP TABLE IF EXISTS CrewSeason;
DROP TABLE IF EXISTS Crew;
DROP TABLE IF EXISTS CrewType;

DROP TABLE IF EXISTS GenreTitle;
DROP TABLE IF EXISTS Genre;
DROP TABLE IF EXISTS EpisodeTraduction;
DROP TABLE IF EXISTS SeasonTraduction;
DROP TABLE IF EXISTS TitleTraduction;
DROP TABLE IF EXISTS Episode;
DROP TABLE IF EXISTS Season;
DROP TABLE IF EXISTS Movie;
DROP TABLE IF EXISTS TVShow;
DROP TABLE IF EXISTS TitleImage;
DROP TABLE IF EXISTS Title;

DROP TABLE IF EXISTS PeopleTraduction;
DROP TABLE IF EXISTS People;

DROP TABLE IF EXISTS FriendPetition;
DROP TABLE IF EXISTS UserFriendOf;
DROP TABLE IF EXISTS User;

CREATE TABLE User (
    id BIGINT NOT NULL AUTO_INCREMENT,
    email VARCHAR(40) NOT NULL,
    country VARCHAR(20),
    userName VARCHAR(60) NOT NULL,
    password VARCHAR(60) NOT NULL,
    role TINYINT NOT NULL,
    image MEDIUMBLOB,
    
    CONSTRAINT UserPK PRIMARY KEY (id),
    CONSTRAINT UserNameUniqueKey UNIQUE (userName)
) ENGINE = InnoDB;

CREATE INDEX UserIndexByUserName ON User (userName);


CREATE TABLE FriendPetition (
    originUser BIGINT NOT NULL,
    destinationUser BIGINT NOT NULL,
	date DATETIME NOT NULL,
	
    CONSTRAINT FriendPetitionPK PRIMARY KEY (originUser, destinationUser),
    CONSTRAINT OriginUserKey FOREIGN KEY (originUser)
    	REFERENCES User (id) ON DELETE CASCADE,
    CONSTRAINT DestinationUserKey FOREIGN KEY (destinationUser)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE UserFriendOf (
    firstFriendId BIGINT NOT NULL,
    secondFriendId BIGINT NOT NULL,
	date DATETIME NOT NULL,
	
    CONSTRAINT UserFriendOfPK PRIMARY KEY (firstFriendId, secondFriendId),
    CONSTRAINT FirstFriendIdKey FOREIGN KEY (firstFriendId)
    	REFERENCES User (id) ON DELETE CASCADE,
    CONSTRAINT SecondFriendIdKey FOREIGN KEY (secondFriendId)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE Title (
    id BIGINT NOT NULL AUTO_INCREMENT,
    externalId BIGINT,
    sourceType TINYINT NOT NULL,
    titleType TINYINT NOT NULL,
    originalTitle VARCHAR(100) NOT NULL,
    originalDescription VARCHAR(500),
    companyName VARCHAR(70),
    year SMALLINT NOT NULL,
    addedBy BIGINT,
    visibility TINYINT NOT NULL,
    setForDelete BOOLEAN NOT NULL,
    addedDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    externalImage VARCHAR(50),
    avgRating FLOAT NOT NULL DEFAULT 0,
    timesRated INT NOT NULL DEFAULT 0,
    
    CONSTRAINT TitlePK PRIMARY KEY (id),
    CONSTRAINT AddedByIdKey FOREIGN KEY (addedBy)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX TitleIndexByExternalId ON Title (externalId);


CREATE TABLE TitleImage (
    id BIGINT NOT NULL AUTO_INCREMENT,
    idTitle BIGINT NOT NULL,
    image MEDIUMBLOB,
    portrait BOOLEAN NOT NULL,
    header BOOLEAN NOT NULL,
    
    CONSTRAINT TitleImagePK PRIMARY KEY (id),
    CONSTRAINT TitleImageTitleIdKey FOREIGN KEY (idTitle)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX TitleImageIndexByTitleId ON TitleImage (idTitle);


CREATE TABLE TitleTraduction (
    idTitle BIGINT NOT NULL,
    idLocale BIGINT NOT NULL,
    titleName VARCHAR(100) NOT NULL,
    country VARCHAR(30),
    description VARCHAR(500) NOT NULL,
    originalLanguage VARCHAR(15),
    
    CONSTRAINT TitleTraductionPK PRIMARY KEY (idTitle, idLocale),
    CONSTRAINT TitleTraductionIdTitleKey FOREIGN KEY (idTitle)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE TVShow (
    id BIGINT NOT NULL,
    totalDuration SMALLINT NOT NULL,
    episodeCount SMALLINT NOT NULL,
    seasonNumber SMALLINT NOT NULL,
    firstAirDate DATETIME,
    lastAirDate DATETIME,
    
    CONSTRAINT TVShowPK PRIMARY KEY (id),
    CONSTRAINT TVShowTitleKey FOREIGN KEY (id)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE Movie (
    id BIGINT NOT NULL,
    duration SMALLINT NOT NULL,
    
    CONSTRAINT MoviePK PRIMARY KEY (id),
    CONSTRAINT MovieTitleKey FOREIGN KEY (id)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE Season (
    id BIGINT NOT NULL AUTO_INCREMENT,
    externalId BIGINT,
    tvShowId BIGINT NOT NULL,
    totalDuration SMALLINT NOT NULL,
    episodeCount SMALLINT NOT NULL,
    seasonNumber TINYINT NOT NULL,
    firstAirDate DATETIME,
    lastAirDate DATETIME,
    originalTitle VARCHAR(100),
    originalDescription VARCHAR(400),
    image MEDIUMBLOB,
    setForDelete BOOLEAN NOT NULL DEFAULT 0,
    
    CONSTRAINT SeasonPK PRIMARY KEY (id),
    CONSTRAINT TVShowSeasonKey FOREIGN KEY (tvShowId)
    	REFERENCES TVShow (id) ON DELETE CASCADE,
    CONSTRAINT SeasonExternalIdUniqueKey UNIQUE (externalId)
) ENGINE = InnoDB;

CREATE TABLE SeasonTraduction (
    idSeason BIGINT NOT NULL,
    idLocale BIGINT NOT NULL,
    title VARCHAR(50),
    description VARCHAR(400) NOT NULL,
    
    CONSTRAINT  SeasonTraductionPK PRIMARY KEY (idSeason, idLocale),
    CONSTRAINT  SeasonTraductionIdSeasonKey FOREIGN KEY (idSeason)
    	REFERENCES  Season (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE Episode (
    id BIGINT NOT NULL AUTO_INCREMENT,
    externalId BIGINT,
    seasonId BIGINT NOT NULL,
    totalDuration SMALLINT NOT NULL,
    episodeNumber SMALLINT NOT NULL,
    airDate DATETIME,
    originalTitle VARCHAR(100),
    originalDescription VARCHAR(400),
    image MEDIUMBLOB,
    setForDelete BOOLEAN NOT NULL DEFAULT 0,
    
    CONSTRAINT EpisodePK PRIMARY KEY (id),
    CONSTRAINT TVShowEpisodeKey FOREIGN KEY (seasonId)
    	REFERENCES Season (id) ON DELETE CASCADE,
    CONSTRAINT EpisodeExternalIdUniqueKey UNIQUE (externalId)
) ENGINE = InnoDB;

CREATE TABLE EpisodeTraduction (
    idEpisode BIGINT NOT NULL,
    idLocale BIGINT NOT NULL,
    title VARCHAR(50) NOT NULL,
    description VARCHAR(400) NOT NULL,
    
    CONSTRAINT  EpisodeTraductionPK PRIMARY KEY (idEpisode, idLocale),
    CONSTRAINT  EpisodeTraductionIdEpisodeKey FOREIGN KEY (idEpisode)
    	REFERENCES  Episode (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE Genre (
    id BIGINT NOT NULL AUTO_INCREMENT,
    externalId BIGINT NOT NULL,
    name VARCHAR(30) NOT NULL,
    
    CONSTRAINT GenrePK PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE GenreTitle (
    genreId BIGINT NOT NULL,
    titleId BIGINT NOT NULL,
    
    CONSTRAINT GenreTitlePK PRIMARY KEY (genreId, titleId),
    CONSTRAINT GenreTitleGenreKey FOREIGN KEY (genreId)
    	REFERENCES Genre (id) ON DELETE CASCADE,
    CONSTRAINT GenreTitleTitleKey FOREIGN KEY (titleId)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE People (
    id BIGINT NOT NULL AUTO_INCREMENT,
    externalId BIGINT,
    sourceType TINYINT NOT NULL,
    originalName VARCHAR(50) NOT NULL,
    birthday DATETIME,
    deathday DATETIME,
    image MEDIUMBLOB,
    addedBy BIGINT,
    visibility TINYINT NOT NULL,
    setForDelete BOOLEAN NOT NULL,
    externalImage VARCHAR(50),
    
    CONSTRAINT PeoplePK PRIMARY KEY (id),
    CONSTRAINT PeopleAddedByIdKey FOREIGN KEY (addedBy)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX PeopleIndexByExternalId ON People (externalId);

CREATE TABLE PeopleTraduction (
    idPeople BIGINT NOT NULL,
    idLocale BIGINT NOT NULL,
    name VARCHAR(70) NOT NULL,
    biography VARCHAR(1000) NULL,
    birthPlace VARCHAR(70) NULL,
    originalLanguage BOOLEAN NOT NULL,
    
    CONSTRAINT PeopleTraductionPK PRIMARY KEY (idPeople, idLocale),
    CONSTRAINT PeopleTraductionIdTitleKey FOREIGN KEY (idPeople)
    	REFERENCES People (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE Cast (
    id BIGINT NOT NULL AUTO_INCREMENT,
    titleId BIGINT NOT NULL,
    peopleId BIGINT NOT NULL,
    episodeCount SMALLINT NOT NULL DEFAULT 0,
    characterName VARCHAR(100) NOT NULL,
    mainCast BOOLEAN NOT NULL,
    
    CONSTRAINT CastPK PRIMARY KEY (id),
    CONSTRAINT CastTitleIdKey FOREIGN KEY (titleId)
    	REFERENCES Title (id) ON DELETE CASCADE,
    CONSTRAINT CastPeopleIdKey FOREIGN KEY (peopleId)
    	REFERENCES People (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX CastIndexByTitleId ON Cast (titleId);
CREATE INDEX CastIndexByPeopleId ON Cast (peopleId);

CREATE TABLE CastSeason (
 	id BIGINT NOT NULL AUTO_INCREMENT,
    castId BIGINT NOT NULL,
    seasonId BIGINT NOT NULL,
    episodeCount SMALLINT NOT NULL DEFAULT 0,
    
    CONSTRAINT CastSeasonPK PRIMARY KEY (id),
    CONSTRAINT CastSeasonCastIdKey FOREIGN KEY (castId)
    	REFERENCES Cast (id) ON DELETE CASCADE,
    CONSTRAINT CastSeasonSeasonIdKey FOREIGN KEY (seasonId)
    	REFERENCES Season (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE CastEpisode (
    id BIGINT NOT NULL AUTO_INCREMENT,
    castSeasonId BIGINT NOT NULL,
    episodeId BIGINT NOT NULL,
    characterName VARCHAR(100) NOT NULL,
    
    CONSTRAINT CastSeasonPK PRIMARY KEY (id),
    CONSTRAINT CastEpisodeCastIdKey FOREIGN KEY (castSeasonId)
    	REFERENCES CastSeason (id) ON DELETE CASCADE,
    CONSTRAINT CastEpisodeEpisodeIdKey FOREIGN KEY (episodeId)
    	REFERENCES Episode (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE CrewType (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    
    CONSTRAINT CrewTypePK PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE Crew (
    id BIGINT NOT NULL AUTO_INCREMENT,
    titleId BIGINT NOT NULL,
    peopleId BIGINT NOT NULL,
    episodeCount SMALLINT NOT NULL DEFAULT 0,
    crewTypeId BIGINT NOT NULL DEFAULT 1,
    
    CONSTRAINT CrewPK PRIMARY KEY (id),
    CONSTRAINT CrewTitleIdKey FOREIGN KEY (titleId)
    	REFERENCES Title (id) ON DELETE CASCADE,
    CONSTRAINT CrewPeopleIdKey FOREIGN KEY (peopleId)
    	REFERENCES People (id) ON DELETE CASCADE,
    CONSTRAINT CrewCrewTypeIdKey FOREIGN KEY (crewTypeId)
    	REFERENCES CrewType (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX CrewIndexByTitleId ON Crew (titleId);
CREATE INDEX CrewIndexByPeopleId ON Crew (peopleId);

CREATE TABLE CrewSeason (
    id BIGINT NOT NULL AUTO_INCREMENT,
    crewId BIGINT NOT NULL,
    seasonId BIGINT NOT NULL,
    episodeCount SMALLINT NOT NULL DEFAULT 0,
    
    CONSTRAINT CrewSeasonPK PRIMARY KEY (id),
    CONSTRAINT CrewSeasonCastIdKey FOREIGN KEY (crewId)
    	REFERENCES Crew (id) ON DELETE CASCADE,
    CONSTRAINT CrewSeasonSeasonIdKey FOREIGN KEY (seasonId)
    	REFERENCES Season (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE CrewEpisode (
    id BIGINT NOT NULL AUTO_INCREMENT,
    crewSeasonId BIGINT NOT NULL,
    episodeId BIGINT NOT NULL,
    crewTypeId BIGINT NOT NULL DEFAULT 1,
    
    CONSTRAINT CrewEpisodePK PRIMARY KEY (id),
    CONSTRAINT CrewEpisodeIdKey FOREIGN KEY (crewSeasonId)
    	REFERENCES CrewSeason (id) ON DELETE CASCADE,
    CONSTRAINT CrewEpisodeEpisodeIdKey FOREIGN KEY (episodeId)
    	REFERENCES Episode (id) ON DELETE CASCADE,
    CONSTRAINT CrewEpisodeCrewTypeIdKey FOREIGN KEY (crewTypeId)
    	REFERENCES CrewType (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE ReviewTitle (
    id BIGINT NOT NULL AUTO_INCREMENT,
    titleId BIGINT NOT NULL,
    userId BIGINT NOT NULL,
    rating TINYINT NOT NULL,
    containsReview BOOLEAN NOT NULL,
    title VARCHAR(100),
    content VARCHAR(2500),
    likeCount INT NOT NULL DEFAULT 0,
    addedDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    
    CONSTRAINT ReviewTitleId PRIMARY KEY (id),
    CONSTRAINT ReviewTitleTitleIdKey FOREIGN KEY (titleId)
    	REFERENCES Title (id) ON DELETE CASCADE,
    CONSTRAINT ReviewTitleUserIdKey FOREIGN KEY (userId)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX IndexReviewTitleByTitleId ON ReviewTitle (titleId);

CREATE TABLE ReviewTitleLike (
    id BIGINT NOT NULL AUTO_INCREMENT,
    reviewId BIGINT NOT NULL,
    userId BIGINT NOT NULL,
    
    CONSTRAINT ReviewTitleLikeId PRIMARY KEY (id),
    CONSTRAINT ReviewTitleLikeReviewIdKey FOREIGN KEY (reviewId)
    	REFERENCES ReviewTitle (id) ON DELETE CASCADE,
    CONSTRAINT ReviewTitleLikeUserIdKey FOREIGN KEY (userId)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;


CREATE TABLE FollowingList (
    id BIGINT NOT NULL AUTO_INCREMENT,
    visibility TINYINT NOT NULL,
    lastUpdated DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    userId BIGINT NOT NULL,
    movieCount SMALLINT NOT NULL,
    tvshowCount SMALLINT NOT NULL,
    
    CONSTRAINT FollowingListId PRIMARY KEY (id),
    CONSTRAINT FollowingListUserIdKey FOREIGN KEY (userId)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX IndexFollowingListByUserId ON FollowingList (userId);

CREATE TABLE FollowingListTitle (
	id BIGINT NOT NULL AUTO_INCREMENT,
    listId BIGINT NOT NULL,
    titleId BIGINT NOT NULL,
    status TINYINT NOT NULL,
    dateAdded DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dateStarted DATETIME,
    dateFinalized DATETIME,
    rating TINYINT,
    hasReview BOOLEAN NOT NULL DEFAULT FALSE,
    
    CONSTRAINT FollowingListTitleId PRIMARY KEY (id),
    CONSTRAINT FollowingListTitleListIdKey FOREIGN KEY (listId)
    	REFERENCES FollowingList (id) ON DELETE CASCADE,
    CONSTRAINT FollowingListTitleTitleIdKey FOREIGN KEY (titleId)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;


CREATE TABLE CustomList (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userId BIGINT NOT NULL,
    visibility TINYINT NOT NULL,
    lastUpdated DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title VARCHAR(100),
    description VARCHAR(300),
    likeCount INT NOT NULL DEFAULT 0,
    titleCount INT NOT NULL DEFAULT 0,
    image MEDIUMBLOB,
    
    CONSTRAINT CustomListId PRIMARY KEY (id),
    CONSTRAINT CustomListUserIdKey FOREIGN KEY (userId)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE INDEX IndexCustomListByUserId ON CustomList (userId);

CREATE TABLE CustomListTitle (
    listId BIGINT NOT NULL,
    titleId BIGINT NOT NULL,
    position INT NOT NULL DEFAULT 0,
    
    CONSTRAINT CustomListTitlePK PRIMARY KEY (listId, titleId),
    CONSTRAINT CustomListTitleListKey FOREIGN KEY (listId)
    	REFERENCES CustomList (id) ON DELETE CASCADE,
    CONSTRAINT CustomListTitleTitleKey FOREIGN KEY (titleId)
    	REFERENCES Title (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE CustomListLike (
    id BIGINT NOT NULL AUTO_INCREMENT,
    listId BIGINT NOT NULL,
    userId BIGINT NOT NULL,
    
    CONSTRAINT CustomListLikeId PRIMARY KEY (id),
    CONSTRAINT CustomListLikeListIdKey FOREIGN KEY (listId)
    	REFERENCES CustomList (id) ON DELETE CASCADE,
    CONSTRAINT CustomListLikeUserIdKey FOREIGN KEY (userId)
    	REFERENCES User (id) ON DELETE CASCADE
) ENGINE = InnoDB;
