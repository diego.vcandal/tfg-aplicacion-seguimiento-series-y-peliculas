INSERT INTO User (id, email, country, userName, password, role, image)
	VALUES (1, 'admin1@tfg.es', 'España', 'admin1', '$2a$10$rUqYew3bJKuEWS4VjdWx8Os/J5FGjz.9zx2SevmPlb6V.C.CiPs.e', 1, null);
INSERT INTO FollowingList (id, visibility, userId, movieCount, tvshowCount) VALUES (1, 0, 1, 0, 0);
	
INSERT INTO User (id, email, country, userName, password, role, image)
	VALUES (2, 'admin2@tfg.es', 'España', 'admin2', '$2a$10$rUqYew3bJKuEWS4VjdWx8Os/J5FGjz.9zx2SevmPlb6V.C.CiPs.e', 1, null);
INSERT INTO FollowingList (id, visibility, userId, movieCount, tvshowCount) VALUES (2, 0, 2, 0, 0);
	
INSERT INTO User (id, email, country, userName, password, role, image)
	VALUES (3, 'admin3@tfg.es', 'España', 'admin3', '$2a$10$rUqYew3bJKuEWS4VjdWx8Os/J5FGjz.9zx2SevmPlb6V.C.CiPs.e', 1, null);
INSERT INTO FollowingList (id, visibility, userId, movieCount, tvshowCount) VALUES (3, 0, 3, 0, 0);

INSERT INTO Genre (id, externalId, name) VALUES (1, 28, 'Action');
INSERT INTO Genre (id, externalId, name) VALUES (2, 12, 'Adventure');
INSERT INTO Genre (id, externalId, name) VALUES (3, 16, 'Animation');
INSERT INTO Genre (id, externalId, name) VALUES (4, 35, 'Comedy');
INSERT INTO Genre (id, externalId, name) VALUES (5, 80, 'Crime');
INSERT INTO Genre (id, externalId, name) VALUES (6, 99, 'Documentary');
INSERT INTO Genre (id, externalId, name) VALUES (7, 18, 'Drama');
INSERT INTO Genre (id, externalId, name) VALUES (8, 10751, 'Family');
INSERT INTO Genre (id, externalId, name) VALUES (9, 14, 'Fantasy');
INSERT INTO Genre (id, externalId, name) VALUES (10, 37, 'History');
INSERT INTO Genre (id, externalId, name) VALUES (11, 27, 'Horror');
INSERT INTO Genre (id, externalId, name) VALUES (12, 10402, 'Music');
INSERT INTO Genre (id, externalId, name) VALUES (13, 9648, 'Mistery');
INSERT INTO Genre (id, externalId, name) VALUES (14, 10749, 'Romance');
INSERT INTO Genre (id, externalId, name) VALUES (15, 878, 'ScienceFiction');
INSERT INTO Genre (id, externalId, name) VALUES (16, 10770, 'TV Movie');
INSERT INTO Genre (id, externalId, name) VALUES (17, 53, 'Thriller');
INSERT INTO Genre (id, externalId, name) VALUES (18, 10752, 'War');
INSERT INTO Genre (id, externalId, name) VALUES (19, 37, 'Western');
INSERT INTO Genre (id, externalId, name) VALUES (20, 10759, 'Action & Adventure');
INSERT INTO Genre (id, externalId, name) VALUES (21, 10762, 'Kids');
INSERT INTO Genre (id, externalId, name) VALUES (22, 10763, 'News');
INSERT INTO Genre (id, externalId, name) VALUES (23, 10764, 'Reality');
INSERT INTO Genre (id, externalId, name) VALUES (24, 10765, 'Sci-Fi & Fantasy');
INSERT INTO Genre (id, externalId, name) VALUES (25, 10766, 'Soap');
INSERT INTO Genre (id, externalId, name) VALUES (26, 10767, 'Talk');
INSERT INTO Genre (id, externalId, name) VALUES (27, 10768, 'War & Politics');

INSERT INTO CrewType (id, name) VALUES (1, 'Other');
INSERT INTO CrewType (id, name) VALUES (2, 'Directing'); 
INSERT INTO CrewType (id, name) VALUES (3, 'Writing'); 
INSERT INTO CrewType (id, name) VALUES (4, 'Production');
INSERT INTO CrewType (id, name) VALUES (5, 'Music');
INSERT INTO CrewType (id, name) VALUES (6, 'Cinematography');
INSERT INTO CrewType (id, name) VALUES (7, 'Editing');
INSERT INTO CrewType (id, name) VALUES (8, 'Casting');
INSERT INTO CrewType (id, name) VALUES (9, 'Art');
INSERT INTO CrewType (id, name) VALUES (10, 'Decoration');
INSERT INTO CrewType (id, name) VALUES (11, 'Camera');
INSERT INTO CrewType (id, name) VALUES (12, 'Costume & Makeup');
INSERT INTO CrewType (id, name) VALUES (13, 'Lighting');
INSERT INTO CrewType (id, name) VALUES (14, 'Visual effects');
INSERT INTO CrewType (id, name) VALUES (15, 'Special effects');
INSERT INTO CrewType (id, name) VALUES (16, 'Stunts');
INSERT INTO CrewType (id, name) VALUES (17, 'Animation');

