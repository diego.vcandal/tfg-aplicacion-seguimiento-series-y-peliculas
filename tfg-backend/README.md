# Backend

## Creación de la Base de Datos

Creación de las bases de datos:
```shell
mysqladmin -u root create tfg -p
mysqladmin -u root create tfgtest -p
```

Creación del usuario:
```shell
mysql -u root -p
    CREATE USER 'tfg-admin'@'localhost' IDENTIFIED BY 'tfg-admin';
    GRANT ALL PRIVILEGES ON tfg.* to 'tfg-admin'@'localhost' WITH GRANT OPTION;
    GRANT ALL PRIVILEGES ON tfgtest.* to 'tfg-admin'@'localhost' WITH GRANT OPTION;
    exit
```

## Ejecución

Únicamente la primera vez para la creación de tablas:
```shell
mvn sql:execute
```

Ejecución del backend:
```shell
mvn spring-boot:run
```