# TFG - Aplicación para el seguimiento de TV y Películas

## Introducción
En este proyecto se desarolla una aplicación para el seguimiento y valoración de series y películas. Además de funcionalidades para visualizar toda la información disponible de estas, creación de listas personalizadas, y funcionalidades propias de una red social integrada.

La aplicación se desarrolla como una aplicación web SPA (Single Page Application), con un frontend desarrollado con [React](https://es.reactjs.org/) y [Redux](https://es.redux.js.org/), y un backend desarrollado con [Spring Boot](https://spring.io/projects/spring-boot) que ofrecerá un API Rest.

Además la aplicación se integra un un API pública externa, [The Movie Database (TMDB) ](https://developers.themoviedb.org/3/getting-started/introduction), que facilitará la recopilación de información.

## Requisitos

- Node 14.16.0+.
- Yarn 1.21.5+.
- Java SE 11+.
- Maven 3+.
- MySQL 8+.

## Instrucciones para la instalación y ejecución

Se encuentran en las respectivas carpetas:
- [Backend](tfg-backend)
- [Frontend](tfg-frontend)


## Planificación
### Sprint 0 - Creación y configuración de la aplicación base.
Consiste en la preparación y configuración de todas las herramientas necesarias, junto a la creación del esqueleto del proyecto hasta el punto en que se pueda empezar a trabajar en las funcionalidades.
### Sprint 1 - Funcionalidades de Perfiles y de Amigos
Historia de Usuario (JIRA #) | Título | Descripcón 
--- | --- | --- |
HU-02 | Registro de Usuario | Como usuario anónimo, quiero poder registrarme en la aplicación para poder tener una cuenta.
HU-03 | Inicio de Sesión | Como usuario registrado, quiero iniciar sesión en la aplicación para poder acceder a mi cuenta.
HU-04 | Cerrar Sesión | Como usuario registrado, quiero poder cerrar mi sesión actual.
HU-05 | Ver Perfil | Como usuario registrado, quiero poder ver mi perfil.
HU-06 | Actualizar Perfil | Como usuario registrado, quiero poder actualizar la información de mi perfil.
HU-07 | Eliminar Perfil | Como usuario registrado, quiero poder eliminar mi perfil y borrar todos mis datos.
HU-09 | Buscar Usuarios | Como usuario general, puedo buscar otros usuarios por su nombre.
HU-10 | Ver Amigos | Como usuario general, puedo ver los amigos de un usuario.
HU-11 | Enviar Petición de Amistad | Como usuario registrado, le puedo mandar una petición de amistad a otro usuario.
HU-12 | Rechazar Petición de Amistad | Como usuario registrado, puedo rechazar la petición de amistad de otro usuario.
HU-13 | Aceptar Petición de Amistad | Como usuario registrado, puedo aceptar la petición de amistad de otro usuario.
HU-14 | Eliminar Amigo | Como usuario registrado, puedo eliminar un amigo que añadiera previamente.

### Sprint 2 - Funcionalidades de Películas y Series
Historia de Usuario (JIRA #) | Título | Descripcón 
--- | --- | --- |
HU-08 | Buscar Obras | Como usuario general, puedo buscar obras por su título.
HU-15 | Ver Información de Obra | Como usuario general, puedo ver toda la información asociada a una obra.
HU-16 | Eliminar Obra | Como administrador, puede borrar una obra y sus datos asociados.
HU-17 | Añadir una Obra | Como administrador, puede añadir una nueva obra al sistema.
HU-18 | Editar Obra | Como administrador, puedo actualizar la información de una obra.
HU-19 | Añadir Temporada | Como administrador, puedo añadir una temporada a una serie.
HU-20 | Añadir Episodio | Como administrador, puedo añadir un nuevo episodio a una serie.
HU-21 | Editar Temporada | Como administrador, puedo actualizar la información de una temporada.
HU-22 | Editar Episodio | Como administrador, puedo actualizar la información de un episodio.
HU-23 | Borrar temporada | Como administrador, puede borrar una temporada y sus datos asociados.
HU-24 | Borrar episodio | Como administrador, puedo borrar un episodio y sus datos asociados.


### Sprint 3 - Funcionalidades de Personas y Valoraciones.
Historia de Usuario (JIRA #) | Título | Descripcón 
--- | --- | --- |
HU-25 | Buscar Personas | Como usuario general, puedo buscar personas por su nombre.
HU-26 | Ver Informacion de Persona | Como usuario general, puedo ver toda la información asociada a una persona.
HU-27 | Eliminar Persona | Como administrador, puede borrar una persona y sus datos asociados.
HU-28 | Añadir una Persona | Como administrador, puede añadir una nueva persona al sistema.
HU-29 | Editar Persona | omo administrador, puedo actualizar la información de una persona .
HU-30 | Añadir Participaciones | Como administrador, puedo asociar una persona a una obra.
HU-31 | Editar Participaciones | Como administrador, puedo editar las asociaciones de una persona en obras.
HU-32 | Eliminar Participaciones | Como administrador, puedo eliminar una participación de una persona en una obra
HU-33 | Ver Reseñas | Como usuario general, debería poder ver las reseñas asociadas a una obra.
HU-34 | Añadir Valoración | Como usuario registrado, podrá añadir una valoración a una obra.
HU-35 | Editar Valoración | Como usuario registrado, podrá modifica la valoración que le hice a una obra.
HU-36 | Añadir Reseña | Como usuario registrado, podrá añadir una reseña a una obra.
HU-37 | Editar Reseña | Como usuario registrado, podrá eliminar la reseña que le hice a una obra.
HU-38 | Eliminar Reseña | Como usuario registrado, podrá modifica la reseña que le hice a una obra.
HU-39 | Dar 'Like' a Reseña | Como usuario registrado, podrá dar like a una reseña de otro usuario.

### Sprint 4 - Funcionalidades de Listas de Seguimiento y Personalizadas
Historia de Usuario (JIRA #) | Título | Descripcón 
--- | --- | --- |
HU-40 | Buscar Listas | Como usuario general, puedo buscar lista por su título.
HU-41 | Ver Detalles de una Lista | Como usuario general, puedo ver los detalles de una lista pública.
HU-42 | Crear Lista Personalizada | Como usuario registrado puede crear una lista personalizada.
HU-43 | Eliminar Lista Personalizada | Como usuario registrado, debe poder eliminar una lista personalizada que creara previamente.
HU-44 | Editar Lista Personalizada | Como usuario registrado, debo poder modificar una lista personalizada creada previamente.
HU-45 | Gestión de Elementos de Lista  Personalizada | Como usuario registrado, puedo añadir una obra a una lista que haya creado.
HU-54 | Crear Lista de Seguimiento | Como usuario registrado, debería de tener una lista de seguimiento personal.
HU-55 | Gestión de Elementos de la Lista de Seguimiento | Como usuario registrado, podré añadir y eliminar elementos a mi lista de seguimiento.
HU-56 | Ver Listas de Seguimiento | Como usuario general, podré ver las distintas listas de seguimiento marcadas como públicas.
