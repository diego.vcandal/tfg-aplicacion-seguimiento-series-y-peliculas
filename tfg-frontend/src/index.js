import React from 'react';
import ReactDOM from 'react-dom';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import {Provider} from 'react-redux';

import * as serviceWorker from './serviceWorker';
import configureStore from './store';
import {Initializer} from './modules/root';
import backend from './backend';
import {NetworkError} from './backend';
import root from './modules/root';
import './styles.css';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

/* Configure store. */
const store = configureStore();

/*  Configure backend proxy with a callback that dispatchs an action in case of a
    network error.
*/
backend.init(error => store.dispatch(root.actions.error(new NetworkError())));

/* Render application. */
ReactDOM.render(
    <Provider store={store}>
            <Initializer/>
    </Provider>, 
    document.getElementById('root'));

serviceWorker.register();
