import messages from './messages';
import common from '../modules/common';

export const initReactIntl = (lang) => {

    /*
        Browser language or english as default.
    */
    let locale = (navigator.languages && navigator.languages[0]) ||
        navigator.language || navigator.userLanguage || 'en';

    if(lang)
        locale = lang;

    const localeWithoutRegionCode = locale.toLowerCase().split(/[_-]+/)[0];

    /*
        Get the messages for the locale, if there are no messages it returns english.
    */
    const localeMessages = messages[locale] || 
        messages[localeWithoutRegionCode] || messages['en'];

    locale = localeMessages === messages['en'] ? 'en' : locale;

    return {locale, messages: localeMessages};

}

export const getInitialLocale = () => {

    const object = common.localStorageUtils.getObject('TFG_language')

    if(object && object.language)
        return object.language;
    
    let locale = (navigator.languages && navigator.languages[0]) ||
        navigator.language || navigator.userLanguage || 'en';

    return locale;

}

