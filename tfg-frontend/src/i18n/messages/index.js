/* eslint-disable import/no-anonymous-default-export */
import {global_es, search_es, users_es, languages_es, genres_es, titles_es,
    people_es, lists_es} from './es';

import {global_en, search_en, users_en, languages_en, genres_en, titles_en,
    people_en, lists_en} from './en';

import {global_gl, search_gl, users_gl, languages_gl, genres_gl, titles_gl,
    people_gl, lists_gl} from './gl';


export default {

    'es': {...global_es, 
        ...search_es,
        ...users_es,
        ...languages_es,
        ...genres_es,
        ...titles_es,
        ...people_es,
        ...lists_es},

    'en': {...global_en, 
        ...search_en,
        ...users_en,
        ...languages_en,
        ...genres_en,
        ...titles_en,
        ...people_en,
        ...lists_en},

    'gl': {...global_gl, 
        ...search_gl,
        ...users_gl,
        ...languages_gl,
        ...genres_gl,
        ...titles_gl,
        ...people_gl,
        ...lists_gl},

};
