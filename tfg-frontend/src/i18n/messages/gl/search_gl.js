export const search_gl = {

    'project.search.option.user': 'Usuarios',
    'project.search.option.movie&tv': 'Películas e Series',
    'project.search.option.people': 'Persoas',
    'project.search.option.lists': 'Listas',
    'project.search.FindUsers.noUsersFound': 'Non se encontraron usuarios.',

    'project.search.results': 'Resultados',
    'project.search.findTitles.noTitlesFound': 'Non se encontraron resultados',
    'project.search.findTitles.TMDBresults': 'Resultados deTMDB',
    'project.search.findTitles.TVShows': 'Series',
    'project.search.findTitles.Movies': 'Peliculas',
    'project.search.findTitles.noDescription': 'Non se encontrou ningunha descripción neste idioma.',

    'project.search.lastTitles.tvshows': 'Series engadidas recentemente',
    'project.search.lastTitles.movies': 'Películas engadidas recentemente',

    'project.search.people.internal': 'Persoas',
    'project.search.people.external': 'Resultados de TMDb',

    'project.search.titles.internal': 'Películas e Series',
    'project.search.titles.external.movies': 'Películas de TMDb',
    'project.search.titles.external.tvshows': 'Series de TMDb',

}
