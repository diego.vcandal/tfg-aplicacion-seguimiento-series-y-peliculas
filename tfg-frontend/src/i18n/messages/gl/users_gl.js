export const users_gl =  {

    'project.users.SignUp.title': 'Rexistro',
    'project.users.Login.title': 'Login',
    'project.users.Logout.title': 'Pechar Sesión',
    'project.users.Profile.title': 'Meu Perfil',
    'project.users.UpdateProfile.title': 'Editar perfil',
    'project.users.Friends.title': 'Amigos',
    'project.users.MyRequests.title': 'Miñas Peticións',
    'project.users.FriendRequest.title': 'Peticions de Amizade',
    'project.users.Titles.create.title': 'Nova Obra',
    'project.users.People.create.title': 'Nova Persona',
    'project.users.lists.title': 'Listas',

    'project.users.lists.create': 'Nova lista',

    'project.users.profileEdit.selectImage': 'Seleccionar unha imaxe',
    'project.users.profileEdit.deleteImage': 'Resetear imaxe',

    'project.users.ChangePassword.fields.confirmNewPassword': 'Confirmar contrasinal nova',
    'project.users.ChangePassword.fields.newPassword': 'Contrasinal nova',
    'project.users.ChangePassword.fields.oldPassword': 'Contrasinal antiga',
    'project.users.ChangePassword.title': 'Cambiar contrasinal',

    'project.users.friends.list.title': 'Amigos',

    'project.users.buttons.followingList': 'Lista de Seguemento',

    'project.users.info.lastUpdate': 'Última actualización',
    'project.users.info.private': 'Privada',

    'project.users.info.allLists': 'Tódalas listas',
    'project.users.info.returnToProfile': 'Volver ó perfil',
    'project.users.info.allRecentLists': 'Listas máis recentes',
    'project.users.info.seeAll': 'Ver todas',
    
    'project.users.followingList.all': 'Todas',
    'project.users.followingList.watching': 'Vendo agora',
    'project.users.followingList.planToWatch': 'Planea Ver',
    'project.users.followingList.completed': 'Completadas',
    'project.users.followingList.dropped': 'Abandoadas',

    'project.users.followingList.added': 'Engadida o:',
    'project.users.followingList.order.label': 'Ordear por:',
    'project.users.followingList.order.name': 'Título',
    'project.users.followingList.order.rating': 'Puntuación',

    'project.users.followingList.status.planToWatch': 'A Planea Ver',
    'project.users.followingList.status.watching': 'Vendo ahora',
    'project.users.followingList.status.completed': 'Completada',
    'project.users.followingList.status.dropped': 'Abandoada',

}
