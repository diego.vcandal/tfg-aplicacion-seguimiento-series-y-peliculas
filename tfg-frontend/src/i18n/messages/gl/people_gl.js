export const people_gl = {

    'project.people.create.title': 'Introduce ó nome ...',
    'project.people.create.title.label': 'Nome',
    'project.people.create.placeOfBirth': 'Lugar de nacemento (Formato: Ciudad, Rexión, País)',
    'project.people.create.dateOfBirth': 'Naceu no',
    'project.people.create.dateOfDeath': 'Faleceu no',
    'project.people.create.biography': 'Biografía',
    'project.people.create.originalLanguage': 'Idioma orixinal',
    'project.people.create.originalLanguage.info': 'Atención: Introducir os datos neste idioma',

    'project.people.create.update.images.portrait': 'Imaxe principal',
    'project.people.create.update.images.portrait.instructions': 'Para subir unha imaxe de forma ' + 
        'correcta use as seguintes indicacións sobre as dimensións da imaxe. ',
    'project.people.create.update.images.portrait.minRes': 'Dimensións mínimas recomendadas: 300x450.',
    'project.people.create.update.images.portrait.ratio': 'Relación de aspecto recomendada: 2:3.',

    'project.people.label.name.alternative': 'Nome alternativo',
}
