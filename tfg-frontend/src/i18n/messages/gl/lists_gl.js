export const lists_gl = {

    'project.list.create.title': 'Introduce o título ...',
    'project.list.create.description': 'Descripción',
    'project.list.create.visible': 'Marcar como pública',

    'project.list.create.elements': 'Todos os elementos',
    'project.list.create.updateOrder': 'Actualizar orde',

    'project.list.update.menu.details': 'Datos principais',
    'project.list.update.menu.elements': 'Elementos',

    'project.list.update.updateToast': 'A lista foi actualizada correctamente.',
    'project.list.create.updateToast': 'O orde dos elementos foi actualizado.',

    'project.list.create.update.image': 'Imaxe principal',
    'project.list.create.update.image.instructions': 'Para subir unha imaxe de forma ' + 
        'correcta use as seguintes indicacións sobre as dimensións da imaxe. ',
    'project.list.create.update.image.minRes': 'Dimensións mínimas recomendadas: 300x450.',
    'project.list.create.update.image.ratio': 'Relación de aspecto recomendada: 2:3.',

}
