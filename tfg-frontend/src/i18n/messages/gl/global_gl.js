export const global_gl = {

    'project.root.Footer.text': 'TFG - TV and Movie tracking app - Universidade da Coruña',
    'project.root.Header.home': 'Inicio',
    'project.root.Home.welcome': '¡Benvido!',
    
    'project.common.ErrorDialog.title': 'Erro',
    'project.common.return': 'Volver',
    
    'project.common.title.update': 'Actualizar',

    'project.error.NotFound.title': 'Non podemos encontrar a páxina que busca.',
    'project.error.NotFound.description': 'A páxina requerida non existe.',

    'project.global.buttons.cancel': 'Cancelar',
    'project.global.buttons.close': 'Cerrar',
    'project.global.buttons.next': 'Seguinte',
    'project.global.buttons.ok': 'OK',
    'project.global.buttons.back': 'Anterior',
    'project.global.buttons.save': 'Gardar',
    'project.global.buttons.search': 'Buscar',
    'project.global.buttons.add': 'Engadir',
    'project.global.buttons.updateProfile': 'Editar Perfil',
    'project.global.buttons.deleteProfile': 'Borrar perfil',
    'project.global.buttons.uptadePassword': 'Actualizar contrasinal',
    'project.global.buttons.sendRequest': 'Enviar solicitude de amizade',
    'project.global.buttons.requestSent': 'Solicitude enviada',
    'project.global.buttons.seeMore': 'Ver mais',
    'project.global.buttons.acceptRequest': 'Aceptar petición de amizade',
    'project.global.buttons.addReview': 'Engadir análise',
    'project.global.buttons.editReview': 'Editar análise',

    'project.global.exceptions.NetworkError': 'Erro de comunicación',

    'project.global.dialog.deleteUser.Title': 'Borra o teu perfil',
    'project.global.dialog.deleteUser.Body': '¿Está seguro de que quere continuar',
    'project.global.dialog.looseChanges.Title': 'Os cambios actuais non se gardarán',
    'project.global.dialog.looseChanges.Body': 'Seguro que quere continuar?',
    'project.global.dialog.adminOptions.delete.Title': 'Marcar título para borrado',
    'project.global.dialog.adminOptions.delete.Body': 'Queres marcar este título para o seu borrado?',
    'project.global.dialog.adminOptions.changeSource.Title': 'Cambiar a fonte de datos',
    'project.global.dialog.adminOptions.changeSource.internal.Body': '¿Queres definir a fonte de datos deste título como interno?',
    'project.global.dialog.adminOptions.changeSource.external.Body': '¿Queres definir a fonte de datos deste título como externo?',
    'project.global.dialog.adminOptions.makePublic.Title': 'Facer público',
    'project.global.dialog.adminOptions.makePublic.Body': '¿Queres marcar este título como público (isto é irreversible) ?',
    'project.global.dialog.review.delete.Title': 'Borrar a túa análise',
    'project.global.dialog.review.delete.Body': 'Coidado, a túa análise será borrada, queres continuar?',

    'project.global.dialog.adminOptions.list.makePublic.Title': 'Facer pública',
    'project.global.dialog.adminOptions.list.makePublic.Body': 'Queres marcar esta lista como pública?',
    'project.global.dialog.adminOptions.list.makePrivate.Title': 'Facer privada',
    'project.global.dialog.adminOptions.list.makePrivate.Body': 'Queres marcar esta lista como privada?',

    'project.global.fields.email': 'Email',
    'project.global.fields.country': 'País',
    'project.global.fields.password': 'Contrasinal',
    'project.global.fields.confirmPassword': 'Confirme o contrasinal',
    'project.global.fields.userName': 'Nome de usuario',
    
    'project.global.validator.email': 'Introduzca unha dirección de correo electrónico correcta',
    'project.global.validator.passwordsDoNotMatch': 'As contrasinais non coinciden',
    'project.global.validator.required': 'Campo obrigatorio',

    'project.global.type.movie': 'Película',
    'project.global.type.tv': 'Serie',
    'project.global.type.TMDB': 'TMDB',

    'project.global.buttons.likes.add': 'Dar \'like\'?',
    'project.global.buttons.likes.liked': 'Lle destes \'like\'',
    'project.global.buttons.likes.remove': 'Eliminar o teu \'like\'?',
    
}