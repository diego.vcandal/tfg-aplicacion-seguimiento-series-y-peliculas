export const people_es = {

    'project.people.create.title': 'Introduce el nombre ...',
    'project.people.create.title.label': 'Nombre',
    'project.people.create.placeOfBirth': 'Lugar de nacimento (Formato: Ciudad, Región, País)',
    'project.people.create.dateOfBirth': 'Nació en el',
    'project.people.create.dateOfDeath': 'Falleció en el',
    'project.people.create.biography': 'Biografía',
    'project.people.create.originalLanguage': 'Idioma original',
    'project.people.create.originalLanguage.info': 'Atención: Introducir los datos en este idioma',

    'project.people.create.update.images.portrait': 'Imagen principal',
    'project.people.create.update.images.portrait.instructions': 'Para subir una imagen de forma ' + 
        'correcta use las siguintes indicaciones sobre las dimensiones de la imagen. ',
    'project.people.create.update.images.portrait.minRes': 'Dimensiones mínimas recomendadas: 300x450.',
    'project.people.create.update.images.portrait.ratio': 'Relación de aspecto recomendada: 2:3.',

    'project.people.label.name.alternative': 'Nombre alternativo',

}
