export const global_es= {

    'project.root.Footer.text': 'TFG - TV and Movie tracking app - Universidad de A Coruña',
    'project.root.Header.home': 'Inicio',
    'project.root.Home.welcome': '¡Bienvenido!',
    
    'project.common.ErrorDialog.title': 'Error',
    'project.common.return': 'Volver',

    'project.common.title.update': 'Actualizar',

    'project.error.NotFound.title': 'No podemos encontrar la pagina que quieres.',
    'project.error.NotFound.description': 'La página solicitada no existe.',

    'project.global.buttons.cancel': 'Cancelar',
    'project.global.buttons.close': 'Cerrar',
    'project.global.buttons.next': 'Siguiente',
    'project.global.buttons.ok': 'OK',
    'project.global.buttons.back': 'Anterior',
    'project.global.buttons.save': 'Guardar',
    'project.global.buttons.search': 'Buscar',
    'project.global.buttons.add': 'Añadir',
    'project.global.buttons.updateProfile': 'Editar Perfil',
    'project.global.buttons.deleteProfile': 'Borrar perfil',
    'project.global.buttons.uptadePassword': 'Actualizar contraseña',
    'project.global.buttons.sendRequest': 'Enviar petición de amistad',
    'project.global.buttons.requestSent': 'Petición enviada',
    'project.global.buttons.seeMore': 'Ver más',
    'project.global.buttons.acceptRequest': 'Aceptar petición de amistad',
    'project.global.buttons.addReview': 'Añadir reseña',
    'project.global.buttons.editReview': 'Editar reseña',

    'project.global.exceptions.NetworkError': 'Fallo de comunicación',

    'project.global.dialog.deleteUser.Title': 'Borrar tu perfil',
    'project.global.dialog.deleteUser.Body': '¿Está seguro que quiere continuar?',
    'project.global.dialog.looseChanges.Title': 'Los cambios actuales se perderán',
    'project.global.dialog.looseChanges.Body': 'Seguro que quiere continuar?',
    'project.global.dialog.adminOptions.delete.Title': 'Marcar título para borrado',
    'project.global.dialog.adminOptions.delete.Body': 'Quieres marcar este título para su borrado?',
    'project.global.dialog.adminOptions.changeSource.Title': 'Cambiar fuente de datos',
    'project.global.dialog.adminOptions.changeSource.internal.Body': '¿Quieres definir la fuente de datos de este título como interno?',
    'project.global.dialog.adminOptions.changeSource.external.Body': '¿Quieres definir la fuente de datos de este título como externo?',
    'project.global.dialog.adminOptions.makePublic.Title': 'Hacer público',
    'project.global.dialog.adminOptions.makePublic.Body': '¿Quieres marcar este título como público (esto es irreversible) ?',
    'project.global.dialog.review.delete.Title': 'Borrar tu reseña',
    'project.global.dialog.review.delete.Body': 'Cuidado, tu reseña se borrará, ¿quieres continuar?',

    'project.global.dialog.adminOptions.list.makePublic.Title': 'Hacer pública',
    'project.global.dialog.adminOptions.list.makePublic.Body': '¿Quieres marcar esta lista como pública?',
    'project.global.dialog.adminOptions.list.makePrivate.Title': 'Hacer privada',
    'project.global.dialog.adminOptions.list.makePrivate.Body': '¿Quieres marcar esta lista como privada?',

    'project.global.fields.email': 'Email',
    'project.global.fields.country': 'País',
    'project.global.fields.password': 'Contraseña',
    'project.global.fields.confirmPassword': 'Confirmar Contraseña',
    'project.global.fields.userName': 'Nombre de usuario',

    'project.global.validator.email': 'Introduzca una dirección de correo electrónico correcta',
    'project.global.validator.passwordsDoNotMatch': 'Las contraseñas no coinciden',
    'project.global.validator.required': 'Campo obligatorio',

    'project.global.type.movie': 'Película',
    'project.global.type.tv': 'Serie',
    'project.global.type.TMDB': 'TMDB',

    'project.global.buttons.likes.add': '¿Dar \'like\'?',
    'project.global.buttons.likes.liked': 'Le has dado \'like\'',
    'project.global.buttons.likes.remove': '¿Eliminar tu \'like\'?',
    
}