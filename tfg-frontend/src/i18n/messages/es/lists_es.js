export const lists_es = {

    'project.list.create.title': 'Introduce el título ...',
    'project.list.create.description': 'Descripción',
    'project.list.create.visible': 'Marcar como pública',

    'project.list.create.elements': 'Todos los elementos',
    'project.list.create.updateOrder': 'Actualizar orden',

    'project.list.update.menu.details': 'Datos principales',
    'project.list.update.menu.elements': 'Elementos',

    'project.list.update.updateToast': 'La lista fue actualizada correctamente.',
    'project.list.create.updateToast': 'El orden de los elementos fue actualizado.',

    'project.list.create.update.image': 'Imagen principal',
    'project.list.create.update.image.instructions': 'Para subir una imagen de forma ' + 
        'correcta use las siguintes indicaciones sobre las dimensiones de la imagen. ',
    'project.list.create.update.image.minRes': 'Dimensiones mínimas recomendadas: 300x450.',
    'project.list.create.update.image.ratio': 'Relación de aspecto recomendada: 2:3.',

}
