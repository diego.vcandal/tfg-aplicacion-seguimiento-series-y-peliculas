export const search_es = {

    'project.search.option.user': 'Usuarios',
    'project.search.option.movie&tv': 'Películas y Series',
    'project.search.option.people': 'Personas',
    'project.search.option.lists': 'Listas',
    'project.search.FindUsers.noUsersFound': 'No se han encontrado usuarios.',

    'project.search.results': 'Resultados',
    'project.search.findTitles.noTitlesFound': 'No se encontraron resultados',
    'project.search.findTitles.TMDBresults': 'Resultados de TMDB',
    'project.search.findTitles.TVShows': 'Series',
    'project.search.findTitles.Movies': 'Peliculas',
    'project.search.findTitles.noDescription': 'No se ha encontrado ninguna descripción en este idioma.',
    
    'project.search.lastTitles.tvshows': 'Series añadidas recientemente',
    'project.search.lastTitles.movies': 'Películas añadidas recientemente',

    'project.search.people.internal': 'Personas',
    'project.search.people.external': 'Resultados de TMDb',

    'project.search.titles.internal': 'Películas y Series',
    'project.search.titles.external.movies': 'Películas de TMDb',
    'project.search.titles.external.tvshows': 'Series de TMDb',
    
}
