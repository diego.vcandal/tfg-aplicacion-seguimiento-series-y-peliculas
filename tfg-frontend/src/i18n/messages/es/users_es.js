export const users_es = {

    'project.users.SignUp.title': 'Registro',
    'project.users.Login.title': 'Login',
    'project.users.Logout.title': 'Cerrar Sesión',
    'project.users.Profile.title': 'Mi Perfil',
    'project.users.UpdateProfile.title': 'Editar perfil',
    'project.users.Friends.title': 'Amigos',
    'project.users.MyRequests.title': 'Mis Peticiones',
    'project.users.FriendRequest.title': 'Peticiones de Amistad',
    'project.users.Titles.create.title': 'Nueva Obra',
    'project.users.People.create.title': 'Nueva Persona',
    'project.users.lists.title': 'Listas',

    'project.users.lists.create': 'Nueva lista',

    'project.users.profileEdit.selectImage': 'Seleccionar una imagen',
    'project.users.profileEdit.deleteImage': 'Resetear imagen',

    'project.users.ChangePassword.fields.confirmNewPassword': 'Confirmar contraseña nueva',
    'project.users.ChangePassword.fields.newPassword': 'Contraseña nueva',
    'project.users.ChangePassword.fields.oldPassword': 'Contraseña antigua',
    'project.users.ChangePassword.title': 'Cambiar contraseña',

    'project.users.friends.list.title': 'Amigos',

    'project.users.buttons.followingList': 'Lista de Seguimiento',

    'project.users.info.lastUpdate': 'Última actualización',
    'project.users.info.private': 'Privada',

    'project.users.info.allLists': 'Todas las listas',
    'project.users.info.returnToProfile': 'Volver al perfil',
    'project.users.info.allRecentLists': 'Listas más recientes',
    'project.users.info.seeAll': 'Ver todas',

    'project.users.followingList.all': 'Todas',
    'project.users.followingList.watching': 'Viendo ahora',
    'project.users.followingList.planToWatch': 'Planea Ver',
    'project.users.followingList.completed': 'Completadas',
    'project.users.followingList.dropped': 'Abandonadas',

    'project.users.followingList.added': 'Añadida el:',
    'project.users.followingList.order.label': 'Ordenar por:',
    'project.users.followingList.order.name': 'Título',
    'project.users.followingList.order.rating': 'Puntuación',

    'project.users.followingList.status.planToWatch': 'Planea Verla',
    'project.users.followingList.status.watching': 'Viendo ahora',
    'project.users.followingList.status.completed': 'Completada',
    'project.users.followingList.status.dropped': 'Abandonada',
    
}
