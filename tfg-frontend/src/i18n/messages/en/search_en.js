export const search_en = {

    'project.search.option.user': 'Users',
    'project.search.option.movie&tv': 'Movies & Tv Shows',
    'project.search.option.people': 'People',
    'project.search.option.lists': 'Lists',
    'project.search.FindUsers.noUsersFound': 'No Users found.',
    
    'project.search.results': 'Results',
    'project.search.findTitles.noTitlesFound': 'No results found',
    'project.search.findTitles.TMDBresults': 'TMDB results',
    'project.search.findTitles.TVShows': 'TV Shows',
    'project.search.findTitles.Movies': 'Movies',
    'project.search.findTitles.noDescription': 'No overview found for this language.',

    'project.search.lastTitles.tvshows': 'Recently added TV Shows',
    'project.search.lastTitles.movies': 'Recently added Movies',

    'project.search.people.internal': 'People',
    'project.search.people.external': 'TMDb results',

    'project.search.titles.internal': 'Movies & Shows',
    'project.search.titles.external.movies': 'TMDb Movies',
    'project.search.titles.external.tvshows': 'TMDb TV Shows',

}
