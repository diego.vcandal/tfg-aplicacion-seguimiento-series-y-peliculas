export const people_en = {

    'project.people.create.title': 'Introduce the name ...',
    'project.people.create.title.label': 'Name',
    'project.people.create.placeOfBirth': 'Place of birth (Format: City, Region, Country)',
    'project.people.create.dateOfBirth': 'Born',
    'project.people.create.dateOfDeath': 'Died',
    'project.people.create.biography': 'Biography',
    'project.people.create.originalLanguage': 'Original language',
    'project.people.create.originalLanguage.info': 'Warning: Introduce the data in this language',

    'project.people.create.images.portrait': 'Portrait image',
    'project.people.create.images.portrait.instructions': 'To upload a portrait image correctly ' + 
        'please read the following instructions of the recommended size. ',
    'project.people.create.update.images.portrait.minRes': 'Recommended minimum dimensions: 300x450.',
    'project.people.create.update.images.portrait.ratio': 'Recommended aspect ratio: 2:3.',

    'project.people.label.name.alternative': 'Alternative name',

}
