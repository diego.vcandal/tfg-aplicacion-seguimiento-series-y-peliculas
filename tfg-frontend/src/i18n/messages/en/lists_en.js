export const lists_en = {

    'project.list.create.title': 'Introduce the title ...',
    'project.list.create.description': 'Description',
    'project.list.create.visible': 'Mark as Public',

    'project.list.create.elements': 'All elements',
    'project.list.create.updateOrder': 'Update Order',

    'project.list.update.menu.details': 'Main details',
    'project.list.update.menu.elements': 'Elements',

    'project.list.update.updateToast': 'The list was updated correctly.',
    'project.list.create.updateToast': 'The elements order was updated.',

    'project.list.create.image': 'Portrait image',
    'project.list.create.image.instructions': 'To upload a portrait image correctly ' + 
        'please read the following instructions of the recommended size. ',
    'project.list.create.update.image.minRes': 'Recommended minimum dimensions: 300x450.',
    'project.list.create.update.image.ratio': 'Recommended aspect ratio: 2:3.',

}
