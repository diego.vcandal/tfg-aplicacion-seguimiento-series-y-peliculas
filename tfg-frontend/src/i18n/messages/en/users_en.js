export const users_en = {

    'project.users.SignUp.title': 'Register',
    'project.users.Login.title': 'Login',
    'project.users.Logout.title': 'Logout',
    'project.users.Profile.title': 'My Profile',
    'project.users.UpdateProfile.title': 'Edit Profile',
    'project.users.Friends.title': 'Friends',
    'project.users.MyRequests.title': 'My Requests',
    'project.users.FriendRequest.title': 'Friend Request',
    'project.users.Titles.create.title': 'New Title',
    'project.users.People.create.title': 'New Person',
    'project.users.lists.title': 'Lists',

    'project.users.lists.create': 'New List',

    'project.users.profileEdit.selectImage': 'Select a image',
    'project.users.profileEdit.deleteImage': 'Reset image',

    'project.users.ChangePassword.fields.confirmNewPassword': 'Confirm new password',
    'project.users.ChangePassword.fields.newPassword': 'New password',
    'project.users.ChangePassword.fields.oldPassword': 'Old password',
    'project.users.ChangePassword.title': 'Change password',

    'project.users.friends.list.title': 'Friends',

    'project.users.buttons.followingList': 'Following List',

    'project.users.info.lastUpdate': 'Last Update',
    'project.users.info.private': 'Private',

    'project.users.info.allLists': 'All lists',
    'project.users.info.returnToProfile': 'Return to profile',
    'project.users.info.allRecentLists': 'Latest lists',
    'project.users.info.seeAll': 'See all',
    
    'project.users.followingList.all': 'All',
    'project.users.followingList.watching': 'Watching',
    'project.users.followingList.planToWatch': 'Plan to Watch',
    'project.users.followingList.completed': 'Completed',
    'project.users.followingList.dropped': 'Dropped',

    'project.users.followingList.added': 'Added on:',
    'project.users.followingList.order.label': 'Order by:',
    'project.users.followingList.order.name': 'Title',
    'project.users.followingList.order.rating': 'Rating',

    'project.users.followingList.status.planToWatch': 'Plan to Watch',
    'project.users.followingList.status.watching': 'Watching',
    'project.users.followingList.status.completed': 'Completed',
    'project.users.followingList.status.dropped': 'Dropped',
}
