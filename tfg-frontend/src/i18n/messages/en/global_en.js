export const global_en = {

    'project.root.Footer.text': 'TFG - TV and Movie tracking app - University of A Coruña',
    'project.root.Header.home': 'Home',
    'project.root.Home.welcome': 'Welcome!',

    'project.common.ErrorDialog.title': 'Error',
    'project.common.return': 'Return',

    'project.common.title.update': 'Update',

    'project.error.NotFound.title': 'We can\'t find the page you\re looking for.',
    'project.error.NotFound.description': 'The page requested doesn\'t exist',

    'project.global.buttons.cancel': 'Cancel',
    'project.global.buttons.close': 'Close',
    'project.global.buttons.next': 'Next',
    'project.global.buttons.ok': 'OK',
    'project.global.buttons.back': 'Back',
    'project.global.buttons.save': 'Save',
    'project.global.buttons.search': 'Search',
    'project.global.buttons.add': 'Add',
    'project.global.buttons.updateProfile': 'Edit Profile',
    'project.global.buttons.deleteProfile': 'Delete profile',
    'project.global.buttons.uptadePassword': 'Update Password',
    'project.global.buttons.sendRequest': 'Send Friend Request',
    'project.global.buttons.requestSent': 'Request sent',
    'project.global.buttons.seeMore': 'See more',
    'project.global.buttons.acceptRequest': 'Accept friend request',
    'project.global.buttons.addReview': 'Add review',
    'project.global.buttons.editReview': 'Edit review',

    'project.global.exceptions.NetworkError': 'Network error',

    'project.global.dialog.deleteUser.Title': 'Delete your profile',
    'project.global.dialog.deleteUser.Body': 'Are you sure you want to delete your profile?',
    'project.global.dialog.looseChanges.Title': 'Current changes will be lost',
    'project.global.dialog.looseChanges.Body': 'You want to proceed?',
    'project.global.dialog.adminOptions.delete.Title': 'Set title for delete',
    'project.global.dialog.adminOptions.delete.Body': 'You want to mark this title for deletion?',
    'project.global.dialog.adminOptions.changeSource.Title': 'Change source type',
    'project.global.dialog.adminOptions.changeSource.internal.Body': 'You want to set the source of this title as internal?',
    'project.global.dialog.adminOptions.changeSource.external.Body': 'You want to set the source of this title as external?',
    'project.global.dialog.adminOptions.makePublic.Title': 'Make title public',
    'project.global.dialog.adminOptions.makePublic.Body': 'You want to set this title as public (this is irreversible) ?',
    'project.global.dialog.review.delete.Title': 'Remove the review',
    'project.global.dialog.review.delete.Body': 'Warning, your review will be deleted, do you want to continue?',

    'project.global.dialog.adminOptions.list.makePublic.Title': 'Make title public',
    'project.global.dialog.adminOptions.list.makePublic.Body': 'You want to set this title as public?',
    'project.global.dialog.adminOptions.list.makePrivate.Title': 'Make title private',
    'project.global.dialog.adminOptions.list.makePrivate.Body': 'You want to set this title as private?',

    'project.global.fields.email': 'Email',
    'project.global.fields.country': 'Country',
    'project.global.fields.password': 'Password',
    'project.global.fields.confirmPassword': 'Confirm password',
    'project.global.fields.userName': 'Username',

    'project.global.validator.email': 'Provide a correct e-mail address',
    'project.global.validator.passwordsDoNotMatch': 'Passwords do not match',
    'project.global.validator.required': 'Required field',
    
    'project.global.type.movie': 'Movie',
    'project.global.type.tv': 'Tv Show',
    'project.global.type.TMDB': 'TMDB',

    'project.global.buttons.likes.add': 'Like this?',
    'project.global.buttons.likes.liked': 'You liked this',
    'project.global.buttons.likes.remove': 'Remove like?',

}