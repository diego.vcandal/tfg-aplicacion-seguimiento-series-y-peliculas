import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import rootReducer from './rootReducer';

const configureStore = () => {

    // redux-thunk allows to change the default synchronous behaviour of 
    // actions to one that allows side effects and asynchronous actions.
    const middlewares = [thunk];

    if (process.env.NODE_ENV !== 'production') {    
        middlewares.push(logger);
    }

    // creates the store that holds the state tree of the app, with 
    // middlewares to extends the default functionality
    return createStore(rootReducer, 
       applyMiddleware(...middlewares));

}

export default configureStore;