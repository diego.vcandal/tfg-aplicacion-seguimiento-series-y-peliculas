import {combineReducers} from 'redux';

import root from '../modules/root';
import users from '../modules/user';
import search from '../modules/search';
import language from '../modules/language';
import titles from '../modules/titles';
import people from '../modules/people';
import lists from '../modules/lists';

// root reducer combining the reducers of all modules
const rootReducer = combineReducers({
    root: root.reducer,
    users: users.reducer,
    search: search.reducer,
    language: language.reducer,
    titles: titles.reducer,
    people: people.reducer,
    lists: lists.reducer
});

export default rootReducer;
