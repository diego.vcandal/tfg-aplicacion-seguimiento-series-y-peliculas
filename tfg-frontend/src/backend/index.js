/* eslint-disable import/no-anonymous-default-export */
import {init} from './appFetch';
import * as userService from './userService';
import * as searchService from './searchService';
import * as friendService from './friendService';
import * as titleService from './titleService';
import * as peopleService from './peopleService';
import * as castAndCrewService from './castAndCrewService';
import * as ratingService from './ratingService';
import * as followingListService from './followingListService';
import * as customListService from './customListService';

export {default as NetworkError} from "./NetworkError";

export default {init, userService, searchService, friendService, titleService, peopleService, 
    castAndCrewService, ratingService, followingListService, customListService};
