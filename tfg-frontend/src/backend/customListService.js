import {config, appFetch} from './appFetch';

export const createCustomList = (params, onSuccess, onErrors) => {
    appFetch(`/lists`, config('POST', params), onSuccess, onErrors);
}

export const getCustomList = (id, onSuccess, onErrors) => {
    appFetch(`/lists/${id}`, config('GET'), onSuccess, onErrors);
}

export const searchMergedTitles = (titleName, onSuccess) => {

    let path = `/search/merged-titles?`;
    path += titleName ? `&titleName=${titleName}` : "";

    appFetch(path, config('GET'), onSuccess); 
}

export const addToList = (id, titleId, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/add`, config('POST', {id: titleId}), onSuccess, onErrors);
}

export const addExternalMovieToList = (id, titleId, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/add-movie-tmdb`, config('POST', {id: titleId}), onSuccess, onErrors);
}

export const addExternalTVShowToList = (id, titleId, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/add-tvshow-tmdb`, config('POST', {id: titleId}), onSuccess, onErrors);
}

export const updateCustomListOrder = (id, params, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/update-order`, config('PUT', params), onSuccess, onErrors);
}

export const updateCustomList = (params, onSuccess, onErrors) => {
    appFetch(`/lists/${params.id}`, config('PUT', params), onSuccess, onErrors);
}

export const deleteCustomList = (id, onSuccess, onErrors) => {
    appFetch(`/lists/${id}`, config('DELETE'), onSuccess, onErrors);
}

export const deleteFromList = (id, titleId, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/delete`, config('POST', {id: titleId}), onSuccess, onErrors);
}

export const addLike = (id, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/likes`, config('POST'), onSuccess, onErrors);
}

export const removeLike = (id, onSuccess, onErrors) => {
    appFetch(`/lists/${id}/likes`, config('DELETE'), onSuccess, onErrors);
}