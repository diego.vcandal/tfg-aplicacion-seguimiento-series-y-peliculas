import {config, appFetch} from './appFetch';

export const getFollowingListTitleInfo = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/following-list-info`, config('GET'), onSuccess, onErrors);
}

export const addToList = (id, params, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/following-add`, config('POST', params), onSuccess, onErrors);
}

export const editTitleFromList = (id, params, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/following-edit`, config('PUT', params), onSuccess, onErrors);
}

export const deleteTitleFromList = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/following-delete`, config('DELETE'), onSuccess, onErrors);
}

export const addExternalMovieToList = (id, params, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/movies/${id}/following-add`, config('POST', params), onSuccess, onErrors);
}

export const addExternalTVShowToList = (id, params, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/following-add`, config('POST', params), onSuccess, onErrors);
}