import {config, appFetch} from './appFetch';


export const findUsers = ({userName, page}, onSuccess) => {

    let path = `/search/users?page=${page}`;

    path += userName ? `&userName=${userName}` : "";

    appFetch(path, config('GET'), onSuccess);

}

export const findTitles = ({titleName, page}, onSuccess) => {

    let path = `/search/titles?page=${page}`;

    path += titleName ? `&titleName=${titleName}` : "";

    appFetch(path, config('GET'), onSuccess); 

}

export const findExternalMovies = ({titleName, page}, onSuccess) => {

    let path = `/search/externalMovies?page=${page}`;

    path += titleName ? `&titleName=${titleName}` : "";

    appFetch(path, config('GET'), onSuccess); 

}

export const findExternalTVShows = ({titleName, page}, onSuccess) => {

    let path = `/search/externalTVShows?page=${page}`;

    path += titleName ? `&titleName=${titleName}` : "";

    appFetch(path, config('GET'), onSuccess); 

}

export const findLastMovies = (onSuccess) => {

    appFetch(`/search/movies/last`, config('GET'), onSuccess); 

}

export const findLastTVShows = (onSuccess) => {

    appFetch(`/search/tvshows/last`, config('GET'), onSuccess); 

}

export const findPeople = ({name, page}, onSuccess) => {

    let path = `/search/people?page=${page}`;

    path += name ? `&name=${name}` : "";

    appFetch(path, config('GET'), onSuccess); 

}

export const findExternalPeople = ({name, page}, onSuccess) => {

    let path = `/search/externalPeople?page=${page}`;

    path += name ? `&name=${name}` : "";

    appFetch(path, config('GET'), onSuccess); 

}

export const findLists = ({title, page}, onSuccess) => {

    let path = `/search/lists?page=${page}`;

    path += title ? `&title=${title}` : "";

    appFetch(path, config('GET'), onSuccess);

}
