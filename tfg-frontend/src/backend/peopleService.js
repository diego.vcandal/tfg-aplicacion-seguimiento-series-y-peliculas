import {config, appFetch} from './appFetch';


export const createPerson = (person, onSuccess) => {
    appFetch(`/people`, config('POST', person), onSuccess);
}

export const updatePerson = (person, onSuccess) => {
    appFetch(`/people/${person.id}`, config('PUT', person), onSuccess);
}

export const findInternalPerson = (id, onSuccess) => {
    let path = `/people/internal/${id}`;
    appFetch(path, config('GET'), onSuccess);
}

export const findExternalPerson = (id, onSuccess) => {
    let path = `/people/tmdb/${id}`;
    appFetch(path, config('GET'), onSuccess);
}

export const findPerson = (id, onSuccess) => {
    let path = `/people/${id}`;
    appFetch(path, config('GET'), onSuccess);
}

export const makePublic = (id, onSuccess, onErrors) => {
    appFetch(`/people/${id}/makePublic`, config('POST'), onSuccess, onErrors);
}

export const setForDeletion = (id, onSuccess, onErrors) => {
    appFetch(`/people/${id}/setForDeletion`, config('POST'), onSuccess, onErrors);
}

export const changeSourceType = (id, onSuccess, onErrors) => {
    appFetch(`/people/${id}/changeSourceType`, config('POST'), onSuccess, onErrors);
}

export const createCachePerson = (id, onSuccess, onErrors) => {
    appFetch(`/people/tmdb/${id}/create`, config('POST'), onSuccess, onErrors);
}

export const findPeople = (name, onSuccess) => {

    let path = `/search/people?page=` + 0;

    path += name ? `&name=${name}` : "";

    appFetch(path, config('GET'), onSuccess); 

}

export const findCastCrewExternalHistory = (id, onSuccess) => {
    let path = `/people/tmdb/${id}/cast-crew`;
    appFetch(path, config('GET'), onSuccess);
}

export const findCastHistory = (id, onSuccess) => {
    let path = `/people/${id}/cast`;
    appFetch(path, config('GET'), onSuccess);
}

export const findCrewHistory = (id, onSuccess) => {
    let path = `/people/${id}/crew`;
    appFetch(path, config('GET'), onSuccess);
}