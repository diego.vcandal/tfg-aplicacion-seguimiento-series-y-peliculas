import {config, appFetch} from './appFetch';


export const findMovie = (id, onSuccess, onErrors) => {

    let path = `/titles/movies/${id}`;

    appFetch(path, config('GET'), onSuccess, onErrors);

}

export const findTVShow = (id, onSuccess) => {

    let path = `/titles/tvshows/${id}`;

    appFetch(path, config('GET'), onSuccess);

}

export const findSeasons = (id, onSuccess) => {

    let path = `/titles/tvshows/${id}/seasons`;

    appFetch(path, config('GET'), onSuccess);

}

export const findLastSeason = (id, onSuccess) => {

    let path = `/titles/tvshows/${id}/lastSeason`;

    appFetch(path, config('GET'), onSuccess);

}

export const findSeasonByNumber = (id, seasonNumber, onSuccess) => {

    let path = `/titles/tvshows/${id}/seasons/${seasonNumber}`;

    appFetch(path, config('GET'), onSuccess);

}


export const findExternalMovie = (id, onSuccess) => {

    let path = `/titles/tmdb/movies/${id}`;

    appFetch(path, config('GET'), onSuccess);

}

export const findExternalTVShow = (id, onSuccess) => {

    let path = `/titles/tmdb/tvshow/${id}`;

    appFetch(path, config('GET'), onSuccess);

}

export const findLastExternalSeason = (id, onSuccess) => {

    let path = `/titles/tmdb/tvshow/${id}/lastSeason`;

    appFetch(path, config('GET'), onSuccess);

}

export const findExternalSeasons = (id, onSuccess) => {

    let path = `/titles/tmdb/tvshow/${id}/seasons`;

    appFetch(path, config('GET'), onSuccess);

}

export const findExternalSeasonByNumber = (id, seasonNumber, onSuccess) => {

    let path = `/titles/tmdb/tvshow/${id}/seasons/${seasonNumber}`;

    appFetch(path, config('GET'), onSuccess);

}

export const createMovie = (movie, onSuccess, onErrors) => {

    appFetch('/titles/movies/create', config('POST', movie), onSuccess, onErrors);

}

export const createTVShow = (tvshow, onSuccess, onErrors) => {

    appFetch('/titles/tvshows/create', config('POST', tvshow), onSuccess, onErrors);

}

export const findFullInternalMovie = (id, onSuccess) => {

    let path = `/titles/internal/movies/${id}`;

    appFetch(path, config('GET'), onSuccess);

}

export const findFullInternalTVShow = (id, onSuccess) => {

    let path = `/titles/internal/tvshows/${id}`;

    appFetch(path, config('GET'), onSuccess);

}

export const updateMovie = (movie, onSuccess, onErrors) => {
    appFetch(`/titles/movies/${movie.id}/update`, config('PUT', movie), onSuccess, onErrors);
}

export const updateTVShow = (tvshow, onSuccess, onErrors) => {
    appFetch(`/titles/tvshows/${tvshow.id}/update`, config('PUT', tvshow), onSuccess, onErrors);
}

export const makePublic = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/makePublic`, config('POST'), onSuccess, onErrors);
}

export const setForDeletion = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/setForDeletion`, config('POST'), onSuccess, onErrors);
}

export const changeSourceType = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/changeSourceType`, config('POST'), onSuccess, onErrors);
}

export const createCacheMovie = (id, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/movies/${id}/create`, config('POST'), onSuccess, onErrors);
}

export const createCacheTVShow = (id, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/create`, config('POST'), onSuccess, onErrors);
}

export const createCacheSeason = (id, seasonNumber, params, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/seasons/${seasonNumber}`, config('POST', params), onSuccess, onErrors);
}

export const createCacheEpisode = (id, seasonNumber, episodeNumber, params, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/seasons/${seasonNumber}/episodes/${episodeNumber}`, config('POST', params), onSuccess, onErrors);
}

export const createCacheAllEpisodes = (id, seasonNumber, params, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/seasons/${seasonNumber}/episodes/addAll`, config('POST', params), onSuccess, onErrors);
}

export const createSeason = (id, params, onSuccess, onErrors) => {
    appFetch(`/titles/tvshows/${id}/seasons`, config('POST', params), onSuccess, onErrors);
}

export const createEpisode = (id, seasonNumber, params, onSuccess, onErrors) => {
    appFetch(`/titles/tvshows/${id}/seasons/${seasonNumber}/episodes`, config('POST', params), onSuccess, onErrors);
}

export const findFullInternalSeason = (id, onSuccess) => {
    let path = `/titles/internal/seasons/${id}`;
    appFetch(path, config('GET'), onSuccess);
}

export const findFullInternalEpisode = (id, onSuccess) => {
    let path = `/titles/internal/episodes/${id}`;
    appFetch(path, config('GET'), onSuccess);
}

export const updateSeason = (season, onSuccess, onErrors) => {
    appFetch(`/titles/seasons/${season.id}/update`, config('PUT', season), onSuccess, onErrors);
}

export const updateEpisode= (episode, onSuccess, onErrors) => {
    appFetch(`/titles/episodes/${episode.id}/update`, config('PUT', episode), onSuccess, onErrors);
}

export const setSeasonForDeletion = (id, onSuccess, onErrors) => {
    appFetch(`/titles/seasons/${id}/setForDeletion`, config('POST'), onSuccess, onErrors);
}

export const setEpisodeForDeletion = (id, onSuccess, onErrors) => {
    appFetch(`/titles/episodes/${id}/setForDeletion`, config('POST'), onSuccess, onErrors);
}