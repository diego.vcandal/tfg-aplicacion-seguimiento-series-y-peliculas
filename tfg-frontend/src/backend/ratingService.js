import {config, appFetch} from './appFetch';

export const getMyReview = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/my-review`, config('GET'), onSuccess, onErrors);
}

export const rateTitle = (id, rating,onSuccess, onErrors) => {
    appFetch(`/titles/${id}/rate`, config('POST', rating), onSuccess, onErrors);
}

export const updateRating = (id, rating, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/rate`, config('PUT', rating), onSuccess, onErrors);
}

export const deleteRating = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/rate`, config('DELETE'), onSuccess, onErrors);
}


export const rateExternalMovie = (id, rating,onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/movies/${id}/rate`, config('POST', rating), onSuccess, onErrors);
}

export const rateExternalTVShow = (id, rating, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/rate`, config('POST', rating), onSuccess, onErrors);
}



export const addNewReview = (id, review, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/reviews`, config('POST', review), onSuccess, onErrors);
}

export const editReview = (id, review, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/reviews`, config('PUT', review), onSuccess, onErrors);
}

export const deleteReview = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/reviews`, config('DELETE'), onSuccess, onErrors);
}


export const addNewMoviewReview = (id, review, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/movies/${id}/reviews`, config('POST', review), onSuccess, onErrors);
}

export const addNewTVShowReview = (id, review, onSuccess, onErrors) => {
    appFetch(`/titles/tmdb/tvshow/${id}/reviews`, config('POST', review), onSuccess, onErrors);
}

export const findReviews = ({id, page}, onSuccess) => {

    appFetch(`/titles/${id}/reviews?page=${page}`, config('GET'), onSuccess); 

}

export const addLike = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/likes`, config('POST'), onSuccess, onErrors);
}

export const removeLike = (id, onSuccess, onErrors) => {
    appFetch(`/titles/${id}/likes`, config('DELETE'), onSuccess, onErrors);
}