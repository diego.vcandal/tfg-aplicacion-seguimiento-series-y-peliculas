import {config, appFetch, setServiceToken, setReauthenticationCallback, getServiceToken, removeServiceToken} from './appFetch';

export const signUp = (user, onSuccess, onErrors, reauthenticationCallback) => {

    appFetch('/users/signUp', config('POST', user), 
        authenticatedUser => {
            setServiceToken(authenticatedUser.serviceToken);
            setReauthenticationCallback(reauthenticationCallback);
            onSuccess(authenticatedUser);
        }, 
        onErrors);

}

export const login = (userName, password, onSuccess, onErrors, reauthenticationCallback) =>
    appFetch('/users/login', config('POST', {userName, password}),
        authenticatedUser => {
            setServiceToken(authenticatedUser.serviceToken);
            setReauthenticationCallback(reauthenticationCallback);
            onSuccess(authenticatedUser);
        }, 
        onErrors);

export const tryLoginFromServiceToken = (onSuccess, reauthenticationCallback) => {

    const serviceToken = getServiceToken();

    if (!serviceToken) {
        onSuccess();
        return;
    }

    setReauthenticationCallback(reauthenticationCallback);

    appFetch('/users/loginFromServiceToken', config('POST'),
        authenticatedUser => onSuccess(authenticatedUser),
        () => removeServiceToken()
    );

}

export const findUserById = (id, onSuccess, onErrors) => 
    appFetch(`/users/${id}`, config('GET'), onSuccess, onErrors);


export const logout = () => removeServiceToken();

export const updateProfile = (user, onSuccess, onErrors) =>
    appFetch(`/users/${user.id}`, config('PUT', user),
        onSuccess, onErrors);

export const deleteUser = (id, password, onSuccess, onErrors) =>
    appFetch(`/users/${id}`, config('DELETE', {password: password}),
        onSuccess, onErrors);
    
export const changePassword = (id, oldPassword, newPassword, onSuccess,
    onErrors) =>
    appFetch(`/users/${id}/changePassword`, 
        config('POST', {oldPassword, newPassword}),
        onSuccess, onErrors);


export const getPublicList = (id, onSuccess, onErrors) => 
    appFetch(`/users/${id}/following-list`, config('GET'), onSuccess, onErrors);

export const getPersonalList = (onSuccess, onErrors) => 
    appFetch(`/users/my-following-list`, config('GET'), onSuccess, onErrors);

export const changeListVisibility = (onSuccess, onErrors) => 
    appFetch(`/users/change-list-visibility`, config('POST'), onSuccess, onErrors);

export const getLastLists = (id, onSuccess, onErrors) => 
    appFetch(`/users/${id}/last-lists`, config('GET'), onSuccess, onErrors);

export const getAllLists = (id, onSuccess, onErrors) => 
    appFetch(`/users/${id}/all-lists`, config('GET'), onSuccess, onErrors);