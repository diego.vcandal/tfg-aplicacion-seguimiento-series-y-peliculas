import {config, appFetch} from './appFetch';

export const sendRequest = (destination, onSuccess, onErrors) => {

    appFetch(`/users/${destination}/send-friend-request`, config('POST'), onSuccess, onErrors);

}

export const acceptRequest = (request, onSuccess, onErrors) => {

    appFetch(`/users/${request.destination}/accept-friend-request`, config('POST', request), 
        onSuccess, onErrors);

}

export const rejectRequest = (request, onSuccess, onErrors) => {

    appFetch(`/users/${request.destination}/reject-friend-request`, config('POST', request), 
        onSuccess, onErrors);

}

export const getReceivedRequests = (id, onSuccess, onErrors) => {

    appFetch(`/users/${id}/receivedRequests`, config('GET'), 
        onSuccess, onErrors);

}

export const findAllFriendsById = (id, onSuccess, onErrors) => 
    appFetch(`/users/${id}/getAllFriends`, config('GET'), onSuccess, onErrors);

export const findSummaryFriendsById = (id, onSuccess, onErrors) => 
    appFetch(`/users/${id}/getSummaryFriends`, config('GET'), onSuccess, onErrors);


export const deleteFriend = (request, onSuccess, onErrors) => {

    appFetch(`/users/${request.user}/deleteFriend`, config('DELETE', request), 
        onSuccess, onErrors);

}