import {config, appFetch} from './appFetch';

export const createCast = (params, onSuccess, onErrors) => {
    appFetch(`/credits/casts/titles`, config('POST', params), onSuccess, onErrors);
}

export const updateCast = (params, onSuccess, onErrors) => {
    appFetch(`/credits/casts/titles`, config('PUT', params), onSuccess, onErrors);
}

export const deleteCast = (id, onSuccess, onErrors) => {
    appFetch(`/credits/casts/titles/${id}`, config('DELETE'), onSuccess, onErrors);
}

export const createEpisodeCast = (params, onSuccess, onErrors) => {
    appFetch(`/credits/casts/episodes`, config('POST', params), onSuccess, onErrors);
}

export const deleteEpisodeCast = (id, onSuccess, onErrors) => {
    appFetch(`/credits/casts/episodes/${id}`, config('DELETE'), onSuccess, onErrors);
}

export const findMainCast = (id, onSuccess) => {
    appFetch(`/credits/casts/titles/${id}/main`, config('GET'), onSuccess);
}

export const findAllCast = (id, onSuccess) => {
    appFetch(`/credits/casts/titles/${id}/all`, config('GET'), onSuccess);
}

export const findEpisodeCast = (id, onSuccess) => {
    appFetch(`/credits/casts/episodes/${id}/all`, config('GET'), onSuccess);
}

export const findInternalCast = (id, onSuccess) => {
    appFetch(`/credits/casts/titles/${id}/internal`, config('GET'), onSuccess);
}



export const createCrew = (params, onSuccess, onErrors) => {
    appFetch(`/credits/crews/titles`, config('POST', params), onSuccess, onErrors);
}

export const updateCrew = (params, onSuccess, onErrors) => {
    appFetch(`/credits/crews/titles`, config('PUT', params), onSuccess, onErrors);
}

export const deleteCrew = (id, onSuccess, onErrors) => {
    appFetch(`/credits/crews/titles/${id}`, config('DELETE'), onSuccess, onErrors);
}

export const createEpisodeCrew = (params, onSuccess, onErrors) => {
    appFetch(`/credits/crews/episodes`, config('POST', params), onSuccess, onErrors);
}

export const deleteEpisodeCrew = (id, onSuccess, onErrors) => {
    appFetch(`/credits/crews/episodes/${id}`, config('DELETE'), onSuccess, onErrors);
}

export const findAllCrew = (id, onSuccess) => {
    appFetch(`/credits/crews/titles/${id}/all`, config('GET'), onSuccess);
}

export const findEpisodeCrew = (id, onSuccess) => {
    appFetch(`/credits/crews/episodes/${id}/all`, config('GET'), onSuccess);
}

export const findInternalCrew = (id, onSuccess) => {
    appFetch(`/credits/crews/titles/${id}/internal`, config('GET'), onSuccess);
}



export const findMainExternalMovieCast = (id, onSuccess) => {
    appFetch(`/titles/tmdb/movies/${id}/main-cast`, config('GET'), onSuccess);
}

export const findMainExternalTVShowCast = (id, onSuccess) => {
    appFetch(`/titles/tmdb/tvshow/${id}/main-cast`, config('GET'), onSuccess);
}

export const findAllExternalMovieCast = (id, onSuccess) => {
    appFetch(`/titles/tmdb/movies/${id}/all-cast`, config('GET'), onSuccess);
}

export const findAllExternalTVShowCast = (id, onSuccess) => {
    appFetch(`/titles/tmdb/tvshow/${id}/all-cast`, config('GET'), onSuccess);
}
