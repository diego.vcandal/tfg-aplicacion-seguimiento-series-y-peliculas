import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    userSearch: null,
    titles: {
        titleSearch: null,
        externalMovieSearch: null,
        externalTVShowSearch: null
    },
    lastMovies: null,
    lastTVShows: null,
    people: {
        peopleSearch: null,
        externalPeopleSearch: null,
    },
    listSearch: null,
};

const userSearch = (state = initialState.userSearch, action) => {

    switch (action.type) {

        case actionTypes.FIND_USERS_COMPLETED:
            return action.userSearch;

        case actionTypes.CLEAR_USER_SEARCH:
            return initialState.userSearch;

        default:
            return state;

    }
}

const titles = (state = initialState.titles, action) => {

    switch (action.type) {

        case actionTypes.FIND_TITLES_COMPLETED:
            return {...state, titleSearch: action.titleSearch};

        case actionTypes.CLEAR_TITLE_SEARCH:
            return {...state, titleSearch: null};

        /* EXTERNAL API ACTIONS */
        case actionTypes.FIND_EXTERNAL_MOVIES_COMPLETED:
            return {...state, externalMovieSearch: action.externalMovieSearch};

        case actionTypes.CLEAR_EXTERNAL_MOVIE_SEARCH:
            return {...state, externalMovieSearch: null};

        case actionTypes.FIND_EXTERNAL_TVSHOWS_COMPLETED:
            return {...state, externalTVShowSearch: action.externalTVShowSearch};

        case actionTypes.CLEAR_EXTERNAL_TVSHOW_SEARCH:
            return {...state, externalTVShowSearch: null};

        default:
            return state;

    }
}

const lastMovies = (state = initialState.lastMovies, action) => {

    switch (action.type) {

        case actionTypes.FIND_LAST_MOVIES_COMPLETED:
            return action.lastMovies;

        case actionTypes.CLEAR_LAST_MOVIES_SEARCH:
            return initialState.lastMovies;

        default:
            return state;

    }
}

const lastTVShows = (state = initialState.lastTVShows, action) => {

    switch (action.type) {

        case actionTypes.FIND_LAST_TVSHOWS_COMPLETED:
            return action.lastTVShows;

        case actionTypes.CLEAR_LAST_TVSHOWS_SEARCH:
            return initialState.lastTVShows;

        default:
            return state;

    }
}

const people = (state = initialState.people, action) => {

    switch (action.type) {

        case actionTypes.FIND_PEOPLE_COMPLETED:
            return {...state, peopleSearch: action.peopleSearch};

        case actionTypes.CLEAR_PEOPLE_SEARCH:
            return {...state, peopleSearch: null};

        case actionTypes.FIND_EXTERNAL_PEOPLE_COMPLETED:
            return {...state, externalPeopleSearch: action.externalPeopleSearch};

        case actionTypes.CLEAR_EXTERNAL_PEOPLE_SEARCH:
            return {...state, externalPeopleSearch: null};

        default:
            return state;

    }
}

const listSearch = (state = initialState.listSearch, action) => {

    switch (action.type) {

        case actionTypes.FIND_LISTS_COMPLETED:
            return action.listSearch;

        case actionTypes.CLEAR_LIST_SEARCH:
            return initialState.listSearch;

        default:
            return state;

    }
}

const reducer = combineReducers({
    userSearch,
    titles,
    lastMovies,
    lastTVShows,
    people,
    listSearch
});

export default reducer;
