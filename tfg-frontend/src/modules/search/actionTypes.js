export const FIND_USERS_COMPLETED = "project/search/findUsersCompleted";
export const CLEAR_USER_SEARCH = "project/search/clearUserSearch";

export const FIND_TITLES_COMPLETED = "project/search/findTitlesCompleted";
export const CLEAR_TITLE_SEARCH = "project/search/clearTitlesSearch";

export const FIND_LAST_MOVIES_COMPLETED = "project/search/findLastMoviesCompleted";
export const CLEAR_LAST_MOVIES_SEARCH = "project/search/clearLastMoviesSearch";

export const FIND_LAST_TVSHOWS_COMPLETED = "project/search/findLastTVShowsCompleted";
export const CLEAR_LAST_TVSHOWS_SEARCH = "project/search/clearLastTVShowsSearch";

export const FIND_LISTS_COMPLETED = "project/search/findListsCompleted";
export const CLEAR_LIST_SEARCH = "project/search/clearListSearch";

/* EXTERNAL API ACTIONS */
export const FIND_EXTERNAL_MOVIES_COMPLETED = "project/search/findExternalMoviesCompleted";
export const CLEAR_EXTERNAL_MOVIE_SEARCH = "project/search/clearExternalMoviesSearch";
export const FIND_EXTERNAL_TVSHOWS_COMPLETED = "project/search/findExternalTVShowsCompleted";
export const CLEAR_EXTERNAL_TVSHOW_SEARCH = "project/search/clearExternalTVShowsSearch";


export const FIND_PEOPLE_COMPLETED = "project/search/findPeopleCompleted";
export const CLEAR_PEOPLE_SEARCH = "project/search/clearPeopleSearch";

export const FIND_EXTERNAL_PEOPLE_COMPLETED = "project/search/findExternalPeopleCompleted";
export const CLEAR_EXTERNAL_PEOPLE_SEARCH = "project/search/clearExternalPeopleSearch";