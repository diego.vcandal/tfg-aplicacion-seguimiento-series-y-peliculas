import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';


import * as selectors from '../selectors';
import * as actions from '../actions';
import HorizontalTitles from './Entity/HorizontalTitles';


const LastTitles = () => {

    const dispatch = useDispatch();
    const lastMovies = useSelector(selectors.getLastMoviesSearch);
    const lastTVShows = useSelector(selectors.getLastTVShowsSearch);

    useEffect(() => {

        dispatch(actions.findLastMovies());

        return () => dispatch(actions.clearLastMoviesSearch());

    }, [dispatch]);

    useEffect(() => {

        dispatch(actions.findLastTVShows());

        return () => dispatch(actions.clearLasTVShowsSearch());

    }, [dispatch]);

    if (!lastMovies || !lastTVShows) {
        return null;
    }

    return (

        <div>
            {lastMovies.items.length !== 0 &&
                <div className="center-element">
                    <div className="row">
                        <h3 className="mb-2 recently-added-title"><FormattedMessage id='project.search.lastTitles.movies'/></h3>
                    </div>
                    <HorizontalTitles titles={lastMovies.items} />
                </div>
            }
            
            {lastTVShows.items.length !== 0 &&
                <div className="center-element mt-3">
                    <div className="row">
                        <h3 className="mb-2 recently-added-title"><FormattedMessage id='project.search.lastTitles.tvshows'/></h3>
                    </div>
                    <HorizontalTitles titles={lastTVShows.items} />
                </div>
            }
            
        </div>

    );

}

export default LastTitles;