import React from 'react';
import {useSelector} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

import Titles from './Entity/Titles';
import { Pager} from '../../common';
import * as selectors from '../selectors';

const SearchInternalTitles = () => {

    const titleSearch = useSelector(selectors.getTitleSearch);

    const query = new URLSearchParams(useLocation().search);
    let titleName = query.get("titleName");
    let page = query.get("page");

    const history = useHistory();

    const handleNextPrevius = type => {

        let path = "/search/titles?";
        path += titleName ? "titleName=" + titleName.trim() + "&" : "";
        path += "page=" + (Number(page) + type);

        history.push(path);

    };
    
    return (

        <div className="center-element col-8 p-0">

            <Titles titles={titleSearch.result.items} />
            
            <Pager 
                back={{
                    enabled: titleSearch.criteria.page >= 1,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: titleSearch.result.existMoreItems,
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default SearchInternalTitles;