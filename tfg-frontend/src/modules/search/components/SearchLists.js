import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

import {PageNotFound, Pager} from '../../common';
import * as selectors from '../selectors';
import * as actions from '../actions';
import Lists from './Entity/Lists';


const SearchLists = () => {

    const dispatch = useDispatch();
    const listSearch = useSelector(selectors.getListSearch);
    const query = new URLSearchParams(useLocation().search);
    let page = query.get("page");
    let title = query.get("title");
    const history = useHistory();

    useEffect(() => {

        let t = title != null ? title.trim() : "";
        let p = !Number.isNaN(Number(page)) ? Number(page) : 0;

        dispatch(actions.findLists({title: t, page: p}));;

        return () => dispatch(actions.clearListSearch());

    }, [title, page, dispatch]);

    if (!listSearch) {
        return <PageNotFound />;
    }

    const handleNextPrevius = type => {

        let path = "/search/lists?";
        path += title ? "title=" + title.trim() + "&" : "";
        path += "page=" + (Number(page) + type);

        dispatch(actions.clearListSearch());

        history.push(path);

    };
    
    return (

        <div className="center-element w-80 mt-4">
            <Lists lists={listSearch.result.items} />
            
            <Pager 
                back={{
                    enabled: listSearch.criteria.page >= 1,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: listSearch.result.existMoreItems,
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default SearchLists;