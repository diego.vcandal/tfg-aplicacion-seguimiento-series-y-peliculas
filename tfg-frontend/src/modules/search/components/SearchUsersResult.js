import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import Users from './Entity/Users';
import {PageNotFound, Pager} from '../../common';
import * as selectors from '../selectors';
import * as actions from '../actions';


const SearchUsersResult = () => {

    const dispatch = useDispatch();
    const userSearch = useSelector(selectors.getUserSearch);
    const query = new URLSearchParams(useLocation().search);
    let page = query.get("page");
    let userName = query.get("userName");
    const history = useHistory();

    useEffect(() => {

        let name = userName != null ? userName.trim() : "";
        let p = !Number.isNaN(Number(page)) ? Number(page) : 0;

        dispatch(actions.findUsers({userName: name, page: p}));;

        return () => dispatch(actions.clearUserSearch());

    }, [userName, page, dispatch]);

    if (!userSearch) {
        return <PageNotFound />;
    }

    if (userSearch.result.items.length === 0) {
        return (
            <div className="alert alert-danger" role="alert">
                <FormattedMessage id='project.search.FindUsers.noUsersFound'/>
            </div>
        );
    }

    const handleNextPrevius = type => {

        let path = "/search/users?";
        path += userName ? "userName=" + userName.trim() + "&" : "";
        path += "page=" + (Number(page) + type);

        dispatch(actions.clearUserSearch());

        history.push(path);

    };
    
    return (

        <div className="center-element w-80 mt-4">
            <Users users={userSearch.result.items} />

            <Pager 
                back={{
                    enabled: userSearch.criteria.page >= 1,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: userSearch.result.existMoreItems,
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default SearchUsersResult;