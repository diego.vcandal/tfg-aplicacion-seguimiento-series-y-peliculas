/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import ReactStars from 'react-rating-stars-component';


const HorizontalTitles = ({titles}) => {

    const history = useHistory();
    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;

    var arrow_left_image = `${process.env.PUBLIC_URL}/images/left_arrow.png`;
    var arrow_right_image = `${process.env.PUBLIC_URL}/images/right_arrow.png`;

    const arrow_style = "align-self-center p-2 ";

    const [arrow_left_active, setArrowLeftActive]  = useState(false);
    const [arrow_right_active, setArrowRightActive]  = useState(titles.length > 6);

    const text_max_len = 30;
    const scrollWidth = 1188;
    let scroll;

    const parseText = str => {
        if(str.length <= text_max_len) 
            return str;
        else
            return str.substring(0, text_max_len) + "...";
    }

    const handleDisplacement = direction => {

        if (direction === 0 && arrow_left_active){
            scroll.scrollLeft -= scrollWidth;
            setArrowRightActive(true);

            if ((scroll.scrollLeft -= scrollWidth) > 0)
                setArrowLeftActive(true);
            else 
                setArrowLeftActive(false);

        } else if(direction === 1 && arrow_right_active){
            scroll.scrollLeft += scrollWidth;
            setArrowLeftActive(true);

            if ((scroll.scrollLeft += scrollWidth) < (scroll.scrollWidth - scrollWidth))
                setArrowRightActive(true);
            else 
                setArrowRightActive(false);

        }

    };


    return(
        <div className="row horizontal-elements-container">

            <div className="image-arrow-container p-0">
                <img className={arrow_style + (arrow_left_active ? "image-arrow link-pointer" : "image-arrow-fade")} src={arrow_left_image} 
                    onClick={() => handleDisplacement(0)} alt="arrow left" />
            </div>
        
            <div className="mt-3 horizontal-row-scroll">

                <div className="row horizontal-row-container m-0" ref={node => scroll = node} >

                    {titles.map(title =>

                        <div key={title.id} className="horizontal-title border-radius-15" >

                            <div className="mb-2">
                                <img className="image-large link-pointer" 
                                    alt="title poster image" src={
                                        title.sourceType !== 0 ?
                                            title_image_path + title.id + "/externalImage"
                                        :
                                            title.image ? 
                                                title_image_path + title.id + "/images/portrait"
                                            : 
                                                title_default_image
                                    } 
                                    onClick={() => title.titleType === 0 ? 
                                        history.push('/movies/' + title.id) : history.push('/tvshows/' + title.id)}
                                    />
                            </div>
                            
                            {title.timesRated > 0 &&
                                <span className="row align-rating-fullreview-end center-element mr-1">
                                    <ReactStars size={17} count={10} activeColor="gold" color="grey" value={title.avgRating}
                                        isHalf={false} edit={false} classNames="noselect"/>
                                </span>
                            }

                            <div>
                                <p className="ml-1 mr-2 link-pointer-text" 
                                    onClick={() => title.titleType === 0 ? 
                                        history.push('/movies/' + title.id) : history.push('/tvshows/' + title.id)}>
                                    {!title.titleName ?
                                        parseText(title.originalTitle)
                                    : 
                                        parseText(title.titleName)
                                    }
                                </p>
                            </div>
                            
                        </div>

                    )}

                </div>
                
            </div>

            <div className="image-arrow-container p-0">
                <img className={arrow_style + (arrow_right_active ? "image-arrow link-pointer" : "image-arrow-fade")} src={arrow_right_image} 
                    onClick={() => handleDisplacement(1)} alt="arrow rigth" />
            </div>

        </div>
    );
    
    
};

HorizontalTitles.propTypes = {
    titles: PropTypes.array.isRequired
};

export default HorizontalTitles;