/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';


const Users = ({users}) => {

    const history = useHistory();
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/`;

    return(
        <div className="center-element w-80">

            {users.map(user =>

                <div key={user.id} className="row user-search-details mb-3" onClick={() => history.push('/users/' + user.id)}>
                    <div className="col-1 image-user-details-container">
                        <img className="image-small" alt="user profile image" src={user.image ? title_image_path + user.id + "/image" 
                        : user_default_image} />
                    </div>
                    <div className="col ml-5">
                        <p className="search-name">{user.userName}</p>
                        <p className="light-gray-text">{user.country}</p>
                    </div>
                </div>

            )}

        </div>
    );
    

};

Users.propTypes = {
    users: PropTypes.array.isRequired
};

export default Users;