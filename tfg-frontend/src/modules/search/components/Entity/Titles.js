/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';
import ReactStars from 'react-rating-stars-component';


const Titles = ({titles}) => {

    const history = useHistory();
    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;
    var external_api_imgage_path = "https://image.tmdb.org/t/p/w500/";

    const text_max_len = 250;

    const parseText = str => {
        if(str.length <= text_max_len) 
            return str;
        else
            return str.substring(0, text_max_len) + "...";
    }

    const titleType = (title) => (
        <div className="row">
            {title.sourceType === 2 &&
                <span className="TMDB-type-tag font-weight-600 ml-3">
                    <FormattedMessage id='project.global.type.TMDB'/>
                </span>
            }
            <span className="title-type-tag font-weight-600 ml-2">{title.titleType === 0 ?
                <FormattedMessage id='project.global.type.movie'/>
            :
                <FormattedMessage id='project.global.type.tv'/>
            }</span>
        </div>
    );

    const titleData = (title) => (
        <span className="ml-4">
            <span className="search-title-name ">{!title.titleName ?
                title.originalTitle
            :
                title.titleName
            }</span>
            {title.year != null && title.year > 0 &&
                <span className="search-title-year-TMDB ml-1"> ({title.year})</span>
            }
        </span>
    );

    const getImage = (title) => {
        
        switch (Number(title.sourceType)){
            case 0:
                if (title.image)  
                    return title_image_path + title.id + "/images/portrait";
                else 
                    return title_default_image;
            
            case 1:
                return title_image_path + title.id + "/externalImage";

            case 2:
                if (title.imageExternalPath != null)
                    return external_api_imgage_path + title.imageExternalPath;
                else
                    return title_default_image;

            default:
                return title_default_image;
        }

    };

    const getLink = (title) => {
        
        switch (Number(title.sourceType)){
            default:
            case 0:
            case 1:
                if (title.titleType === 0)  
                    return '/movies/' + title.id;
                else 
                    return '/tvshows/' + title.id;

            case 2:
                if (title.titleType === 0)  
                    return '/tmdb/movies/' + title.externalId;
                else 
                    return '/tmdb/tvshows/' + title.externalId;
        }

    };

    return(
        <div className="center-element w-100 mt-3">

            {titles.map(title =>

                <div key={title.id} className="row title-search-details mb-5 ml-1" onClick={() => history.push(getLink(title))}>

                    <div className="col-1 image-title-search-container">
                        <img className="image-medium border-radius-15 title-search-image-shadow" 
                            alt="title poster image" src={getImage(title)} />
                    </div>

                    <div className="col title-search-details-content">
                        <div className="row ml-5">

                            {titleType(title)}
                            {titleData(title)}

                            {title.timesRated > 0 &&
                                <span className="row align-rating-fullreview-end m-float-right mr-3">
                                    <ReactStars size={23} count={10} activeColor="gold" color="grey" value={title.avgRating}
                                        isHalf={false} edit={false} classNames="noselect"/>
                                </span>
                            }

                        </div>
                        <div className="row search-title-description ml-5 mt-2">
                            <span className="ml-5 mr-5">{
                            !title.description ?
                                !title.originalDescription ?
                                    <FormattedMessage id='project.search.findTitles.noDescription'/>
                                :
                                    parseText(title.originalDescription)
                            :
                                parseText(title.description)
                            }</span>
                        </div>
                        
                        
                    </div>
                </div>

            )}

        </div>
    );
    
    
};

Titles.propTypes = {
    titles: PropTypes.array.isRequired
};

export default Titles;