/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import users from '../../../user';

const Lists = ({lists}) => {

    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const actualUser = useSelector(users.selectors.getUser);

    const history = useHistory();
    const list_default_image = `${process.env.PUBLIC_URL}/images/list_default_image.png`;
    var list_image_path = `${process.env.REACT_APP_BACKEND_URL}/lists/`;
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var user_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/`;

    return(
        <div className="center-element w-100 mt-3">

            {lists.map(list =>

                <div key={list.id} className="row list-search-details mb-5 ml-1" >

                    <div className="col-2 p-0">
                        <img className="image-list-search-view border-radius-15-left link-pointer " onClick={() => history.push(`/lists/${list.id}`)}
                            alt="title poster image" src={list.hasImage ? list_image_path + list.id + "/image" : list_default_image} />
                    </div>

                    <div className="col title-search-details-content p-0">
                        <div className="row">

                            <div className="col-9 p-0 ml-1">
                                <span>
                                    <span className="search-title-name link-pointer-text"
                                        onClick={() => history.push(`/lists/${list.id}`)}>
                                            {list.title + (list.titleCount > 0 ? "," : "")}
                                        </span>
                                    {list.titleCount > 0 && <span className="list-search-title-count">{" "}({list.titleCount})</span>}
                                </span>
                            </div>

                            <div className="col p-0 list-search-like-container">
                                <div className="row w-95 justify-content-end">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart-fill mt-1" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                    </svg>
                                    <span className="font-weight-600 ml-2"> {"(" + list.likeCount + ")"}</span>
                                </div>
                            </div>

                        </div>
                        <div className="row search-title-description ml-4 mt-2">
                            <span className="mr-5">{
                            !list.description ?
                                    <FormattedMessage id='project.search.findTitles.noDescription'/>
                            :
                                list.description
                            }</span>
                        </div>
                        
                        <div className="row search-title-description ml-5 mt-2 user-list-container">
                            <div className="col text-right ">
                                <img className="image-user-list border-radius-15 link-pointer " alt="person portrait image"
                                    onClick={() => history.push('/users/' + list.userId)} 
                                    src={user_image_path + list.userId + "/image"}
                                    onError={e => {e.target.src = user_default_image}}/>
                                <span className="link-bold link-pointer-text ml-2 mt-1"
                                    onClick={() => history.push('/users/' + list.userId)}>
                                        {list.userName}
                                        {isLoggedIn && list.userId === actualUser.id && 
                                            <span>{" "}(<FormattedMessage id="project.titles.info.avgRating.you"/>)</span>
                                        }
                                </span> 
                            </div>
                        </div>
                    </div>
                </div>

            )}

        </div>
    );
    
    
};

Lists.propTypes = {
    lists: PropTypes.array.isRequired
};

export default Lists;