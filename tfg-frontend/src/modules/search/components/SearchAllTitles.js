import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useLocation, useHistory} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';

import * as selectors from '../selectors';
import * as actions from '../actions';
import SearchInternalTitles from './SearchInternalTitles';
import SearchExternalMovies from './SearchExternalMovies';
import SearchExternalTVShows from './SearchExternalTVShows';


const SearchAllTitles = ({type}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const titleSearch = useSelector(selectors.getTitleSearch);
    const externalMovieSearch = useSelector(selectors.getEternalMovieSearch);
    const externalTVShowSearch = useSelector(selectors.getExternalTVShowSearch);

    const query = new URLSearchParams(useLocation().search);
    let titleName = query.get("titleName");
    let page = query.get("page");

    let list = [{id: 0, ref: null}, 
        {id: 1, ref: null},
        {id: 2, ref: null}];

    useEffect(() => {

        let title = titleName != null ? titleName.trim() : "";
        let p = !page ? 0 : Number(page);

        dispatch(actions.findTitles({titleName: title, page: p}));;

        return () => dispatch(actions.clearTitleSearch());

    }, [titleName, page, dispatch]);

    /* EXTERNAL API MOVIE SEARCH */
    useEffect(() => {

        let title = titleName != null ? titleName.trim() : "";
        let p = !page || Number(page) === 0 ? 1 : Number(page);

        dispatch(actions.findExternalMovies({titleName: title, page: p}));;

        return () => dispatch(actions.clearExternalMoviesSearch());

    }, [titleName, page, dispatch]);

    /* EXTERNAL API TV SHOW SEARCH */
    useEffect(() => {

        let title = titleName != null ? titleName.trim() : "";
        let p = !page || Number(page) === 0 ? 1 : Number(page);

        dispatch(actions.findExternalTVShows({titleName: title, page: p}));;

        return () => dispatch(actions.clearExternalTVShowsSearch());

    }, [titleName, page, dispatch]);

    const baseClass = "list-group-item list-group-item-action col";
    const baseClassActive = "list-group-item list-group-item-action col search-button-active";

    return (

        <div className="row m-0 mt-4">
            <div className="side-search-menu-container shadow-container col-3 list-group">

                <div className="list-group-item list-group-item-action col active">
                    <div className="col">
                        <FormattedMessage id='project.search.results'/>
                    </div>
                </div>

                <button type="button" className={type === 0 ? baseClassActive : baseClass} ref={node => list[0].ref = node}
                    onClick={e => history.push("/search/titles?titleName=" + (!titleName || titleName === null ? "" : titleName))}>
                    <div className="col-10">
                        <FormattedMessage id='project.search.titles.internal'/>
                        {titleSearch && titleSearch.result && (" (" +  titleSearch.result.totalElements + ")")}
                    </div>
                    <div className="col-2 center-element-v">➤</div>
                </button>

                <div className="list-group-divider"></div>

                <button type="button" className={type === 1 ? baseClassActive : baseClass} ref={node => list[1].ref = node}
                    onClick={e => history.push("/search/tmdb-movies?titleName=" + (!titleName || titleName === null ? "" : titleName))}>
                    <div className="col-10">
                        <FormattedMessage id='project.search.titles.external.movies'/>
                        {externalMovieSearch && externalMovieSearch.result && (" (" +  externalMovieSearch.result.total_results + ")")}
                    </div>
                    <div className="col-2 center-element-v">➤</div>
                </button>

                <button type="button" className={type === 2 ? baseClassActive : baseClass} ref={node => list[2].ref = node}
                    onClick={e => history.push("/search/tmdb-tvshows?titleName=" + (!titleName || titleName === null ? "" : titleName))}>
                    <div className="col-10">
                        <FormattedMessage id='project.search.titles.external.tvshows'/>
                        {externalTVShowSearch && externalTVShowSearch.result && (" (" +  externalTVShowSearch.result.total_results + ")")}
                    </div>
                    <div className="col-2 center-element-v">➤</div>
                </button>

            </div>

            {type === 0 &&
                titleSearch && <SearchInternalTitles />
            }

            {type === 1 &&
                externalMovieSearch && <SearchExternalMovies />
            }

            {type === 2 &&
                externalTVShowSearch && <SearchExternalTVShows />
            }

        </div>

    );

}

export default SearchAllTitles;