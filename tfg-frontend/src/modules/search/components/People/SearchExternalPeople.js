import React from 'react';
import {useSelector} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

import {Pager} from '../../../common';
import * as selectors from '../../selectors';
import Person from './Entity/Person';


const SearchExternalPeople = () => {

    const externalPeopleSearch = useSelector(selectors.getExternalPeopleSearch);

    const query = new URLSearchParams(useLocation().search);
    let name = query.get("name");
    let page = query.get("page");

    const history = useHistory();

    const handleNextPrevius = type => {

        page = Number(page) === 0 ? 1 : Number(page);

        let path = "/search/externalPeople?";
        path += name ? "name=" + name.trim() + "&" : "";
        path += "page=" + (Number(page) + type);

        history.push(path);

    };

    return (

        <div className="center-element col-8 p-0">

            <Person persons={externalPeopleSearch.result.results} type={1} />
            
            <Pager 
                back={{
                    enabled: externalPeopleSearch.criteria.page >= 2,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: (externalPeopleSearch.result.page < externalPeopleSearch.result.total_pages),
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default SearchExternalPeople;