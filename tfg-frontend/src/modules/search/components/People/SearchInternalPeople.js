import React from 'react';
import {useSelector} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

import {Pager} from '../../../common';
import * as selectors from '../../selectors';
import Person from './Entity/Person';


const SearchInternalPeople = () => {

    const peopleSearch = useSelector(selectors.getPeopleSearch);

    const query = new URLSearchParams(useLocation().search);
    let name = query.get("name");
    let page = query.get("page");

    const history = useHistory();

    const handleNextPrevius = type => {

        let path = "/search/people?";
        path += name ? "name=" + name.trim() + "&" : "";
        path += "page=" + (Number(page) + type);

        history.push(path);

    };

    return (

        <div className="center-element col-8 p-0">

            <Person persons={peopleSearch.result.items} type={0}/>
            
            <Pager 
                back={{
                    enabled: peopleSearch.criteria.page >= 1,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: peopleSearch.result.existMoreItems,
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default SearchInternalPeople;