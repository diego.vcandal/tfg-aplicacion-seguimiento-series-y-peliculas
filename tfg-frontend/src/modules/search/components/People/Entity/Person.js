/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';


const Person = ({persons, type}) => {

    const history = useHistory();
    var person_default_image = `${process.env.PUBLIC_URL}/images/person_default_image.png`;
    var person_image_path = `${process.env.REACT_APP_BACKEND_URL}/people/`;
    var external_api_image_path = "https://image.tmdb.org/t/p/w500";

    return(
        <div>

            {persons.map(person =>
                
                <div key={person.id} className="row mb-3 person-search-details" 
                    onClick={() => type === 0 ? history.push('/people/' + person.id) : history.push('/tmdb/people/' + person.id)}>

                    <div className="col-1 image-user-details-container">
                        <img className="image-small border-radius-15" alt="person portrait image"
                        src={person.sourceType !== 0 ?
                                person.externalId ?
                                    person_image_path + person.externalId + "/externalImage"
                                :
                                    external_api_image_path + person.imageExternalPath
                            :
                                person.image ? 
                                    person_image_path + person.id + "/image"
                                : 
                                    person_default_image}
                        onError={e => {e.target.src = person_default_image}}/>
                    </div>
                    <div className="col ml-2">
                        <p className="search-name">{person.originalName}</p>
                    </div>
                </div>

            )}

        </div>
    );
    

};

Person.propTypes = {
    persons: PropTypes.array.isRequired
};

export default Person;