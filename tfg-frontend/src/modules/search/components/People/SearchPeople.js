import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useLocation, useHistory} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';

import * as actions from '../../actions';
import * as selectors from '../../selectors';
import SearchInternalPeople from './SearchInternalPeople';
import SearchExternalPeople from './SearchExternalPeople';

const SearchPeople = ({type}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    let fullQuery = useLocation().search;
    const query = new URLSearchParams(fullQuery);
    let name = query.get("name");
    let page = query.get("page");

    const peopleSearch = useSelector(selectors.getPeopleSearch);
    const externalPeopleSearch = useSelector(selectors.getExternalPeopleSearch);

    useEffect(() => {

        let query = name != null ? name.trim() : "";
        let p = !page ? 0 : Number(page);

        dispatch(actions.findPeople({name: query, page: p}));;

        return () => dispatch(actions.clearPeopleSearch());

    }, [name, page, dispatch]);

    useEffect(() => {

        let query = name != null ? name.trim() : "";
        let p = !page || Number(page) === 0 ? 1 : Number(page);

        dispatch(actions.findExternalPeople({name: query, page: p}));;

        return () => dispatch(actions.clearExternalPeopleSearch());

    }, [name, page, dispatch]);

    let list = [{id: 0, ref: null}, 
        {id: 1, ref: null}];

    const baseClass = "list-group-item list-group-item-action col";
    const baseClassActive = "list-group-item list-group-item-action col search-button-active";

    return (
        <div className="row m-0 mt-4">
            <div className="side-search-menu-container shadow-container col-3 list-group">

                <div className="list-group-item list-group-item-action col active">
                    <div className="col">
                        <FormattedMessage id='project.search.results'/>
                    </div>
                </div>

                <button type="button" className={type === 0 ? baseClassActive : baseClass} ref={node => list[0].ref = node}
                    onClick={e => history.push("/search/people?name=" + (!name || name === null ? "" : name))}>
                    <div className="col-10">
                        <FormattedMessage id='project.search.people.internal'/>
                        {peopleSearch && peopleSearch.result && (" (" +  peopleSearch.result.totalElements + ")")}
                    </div>
                    <div className="col-2 center-element-v">➤</div>
                </button>

                <button type="button" className={type === 1 ? baseClassActive : baseClass} ref={node => list[1].ref = node}
                    onClick={e => history.push("/search/externalPeople?name=" + (!name || name === null ? "" : name))}>
                    <div className="col-10">
                        <FormattedMessage id='project.search.people.external'/>
                        {externalPeopleSearch && externalPeopleSearch.result && (" (" +  externalPeopleSearch.result.total_results + ")")}
                    </div>
                    <div className="col-2 center-element-v">➤</div>
                </button>

            </div>

            {type === 0 &&
                peopleSearch && <SearchInternalPeople />
            }

            {type === 1 &&
                externalPeopleSearch && <SearchExternalPeople />
            }

        </div>
    );

}

export default SearchPeople;