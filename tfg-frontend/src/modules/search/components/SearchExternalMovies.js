import React from 'react';
import {useSelector} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

import {Pager} from '../../common';
import * as selectors from '../selectors';
import Titles from './Entity/Titles';


const SearchExternalMovies = () => {

    const query = new URLSearchParams(useLocation().search);
    let titleName = query.get("titleName");
    let page = query.get("page");

    const history = useHistory();

    let externalTitles = useSelector(selectors.getEternalMovieSearch);

    const handleNextPrevius = type => {

        page = Number(page) === 0 ? 1 : Number(page);

        let path = "/search/tmdb-movies?";
        path += titleName ? "titleName=" + titleName.trim() + "&" : "";
        path += "page=" + (Number(page) + type);

        history.push(path);

    };
    
    return (

        <div className="center-element col-8 p-0 mt-4">

            <Titles titles={externalTitles.result.results} preview={false} />
            
            <Pager 
                back={{
                    enabled: externalTitles.criteria.page >= 2,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: (externalTitles.result.page < externalTitles.result.total_pages),
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default SearchExternalMovies;