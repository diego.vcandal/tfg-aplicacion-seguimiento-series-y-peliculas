import React from 'react';
import {FormattedMessage} from 'react-intl';
import PropTypes from 'prop-types';

import {SearchType} from '../util/SearchType';

const SearchTypeSelector = (selectProps) => {

    return (

        <select {...selectProps}>

            {SearchType.map(type => 
                <FormattedMessage id={type.name}>
                    {
                        (msg) => <option key={type.name} value={type.id}>{msg}</option>
                    }
                </FormattedMessage>
            )}

        </select>

    );

}

SearchTypeSelector.propTypes = {
    selectProps: PropTypes.object
};

export default SearchTypeSelector;