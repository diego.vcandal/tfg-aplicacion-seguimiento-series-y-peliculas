import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';

import SearchTypeSelector from './SearchTypeSelector';

const SearchElements = () => {

    const history = useHistory();
    const [searchTypeId, setSearchTypeId] = useState('');
    const [keywords, setKeywords] = useState('');
   
    const handleSubmit = event => {
        event.preventDefault();
        let path;

        switch(searchTypeId){
            case "3":
                path = "/search/lists?";
                path += keywords ? "title=" + keywords.trim() + "&" : "";
                path += "page=0";

                history.push(path);
                break;

            case "2":
                path = "/search/people?";
                path += keywords ? "name=" + keywords.trim() + "&" : "";
                path += "page=0";

                history.push(path);
                break;

            case "1":
                path = "/search/users?";
                path += keywords ? "userName=" + keywords.trim() + "&" : "";
                path += "page=0";

                history.push(path);
                break;

            case "0":
            default:
                path = "/search/titles?";
                path += keywords ? "titleName=" + keywords.trim(): "";

                history.push(path);
                break;

        }

    }

    return (

        <form className="form-inline mt-2 ml-3 mt-md-0 row w-100" onSubmit={e => handleSubmit(e)}>

            <SearchTypeSelector id="dropSearch" className="custom-select my-1 mr-sm-2 col-3"
                value={searchTypeId} onChange={e => setSearchTypeId(e.target.value)}/>

            <input id="keywords" type="text" className="form-control mr-sm-2 col-4"
                value={keywords} onChange={e => setKeywords(e.target.value)}/>
            
            <button type="submit" className="btn btn-primary my-2 my-sm-0">
                <FormattedMessage id='project.global.buttons.search'/>
            </button>

        </form>

    );

}

export default SearchElements;
