const getModuleState = state => state.search;

export const getUserSearch = state =>
    getModuleState(state).userSearch;

export const getTitles = state =>
    getModuleState(state).titles;

export const getTitleSearch = state =>
    getTitles(state).titleSearch;

export const getEternalMovieSearch = state =>
    getTitles(state).externalMovieSearch;

export const getExternalTVShowSearch = state =>
    getTitles(state).externalTVShowSearch;

export const getLastMoviesSearch = state =>
    getModuleState(state).lastMovies;

export const getLastTVShowsSearch = state =>
    getModuleState(state).lastTVShows;

export const getPeople = state =>
    getModuleState(state).people;

export const getPeopleSearch = state =>
    getPeople(state).peopleSearch;

export const getExternalPeopleSearch = state =>
    getPeople(state).externalPeopleSearch;

export const getListSearch = state =>
    getModuleState(state).listSearch;