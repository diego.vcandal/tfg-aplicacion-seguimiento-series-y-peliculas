export const SearchType = [
    {id: 0, type: "movie&tv", name: "project.search.option.movie&tv"},
    {id: 1, type: "user", name: "project.search.option.user"},
    {id: 2, type: "people", name: "project.search.option.people"},
    {id: 3, type: "lists", name: "project.search.option.lists"}
]