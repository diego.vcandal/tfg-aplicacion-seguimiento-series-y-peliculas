import * as actionTypes from './actionTypes';
import backend from '../../backend';


export const clearUserSearch = () => ({
    type: actionTypes.CLEAR_USER_SEARCH
});

const findUsersCompleted = userSearch =>({
    type: actionTypes.FIND_USERS_COMPLETED,
    userSearch
})

export const findUsers = criteria => dispatch => {

    backend.searchService.findUsers(criteria,
        result => dispatch(findUsersCompleted({criteria, result})));

}

export const previousFindUsersResultPage = criteria =>
    findUsers({...criteria, page: criteria.page-1});

export const nextFindUsersResultPage = criteria =>
    findUsers({...criteria, page: criteria.page+1});



/******************************************************/
/*********************** TITLES ***********************/
/******************************************************/    

export const clearTitleSearch = () => ({
    type: actionTypes.CLEAR_TITLE_SEARCH
});

const findTitlesCompleted = titleSearch =>({
    type: actionTypes.FIND_TITLES_COMPLETED,
    titleSearch
})

export const findTitles = criteria => dispatch => {
    backend.searchService.findTitles(criteria,
        result => dispatch(findTitlesCompleted({criteria, result})));
}


/* EXTERNAL API ACTIONS */

export const clearExternalMoviesSearch = () => ({
    type: actionTypes.CLEAR_EXTERNAL_MOVIE_SEARCH
});

const findExternalMoviesCompleted = externalMovieSearch =>({
    type: actionTypes.FIND_EXTERNAL_MOVIES_COMPLETED,
    externalMovieSearch
})

export const findExternalMovies = criteria => dispatch => {
    backend.searchService.findExternalMovies(criteria,
        result => dispatch(findExternalMoviesCompleted({criteria, result})));
}


export const clearExternalTVShowsSearch = () => ({
    type: actionTypes.CLEAR_EXTERNAL_TVSHOW_SEARCH
});

const findExternalTVShowsCompleted = externalTVShowSearch =>({
    type: actionTypes.FIND_EXTERNAL_TVSHOWS_COMPLETED,
    externalTVShowSearch
})

export const findExternalTVShows = criteria => dispatch => {
    backend.searchService.findExternalTVShows(criteria,
        result => dispatch(findExternalTVShowsCompleted({criteria, result})))
}


export const clearLastMoviesSearch = () => ({
    type: actionTypes.CLEAR_LAST_MOVIES_SEARCH
});

const findLastMoviesCompleted = lastMovies =>({
    type: actionTypes.FIND_LAST_MOVIES_COMPLETED,
    lastMovies
})

export const findLastMovies = () => dispatch => {
    backend.searchService.findLastMovies(result => dispatch(findLastMoviesCompleted(result)));
}

export const clearLasTVShowsSearch = () => ({
    type: actionTypes.CLEAR_LAST_TVSHOWS_SEARCH
});

const findLastTVShowsCompleted = lastTVShows =>({
    type: actionTypes.FIND_LAST_TVSHOWS_COMPLETED,
    lastTVShows
})

export const findLastTVShows = () => dispatch => {
    backend.searchService.findLastTVShows(result => dispatch(findLastTVShowsCompleted(result)));
}



/******************************************************/
/*********************** PEOPLE ***********************/
/******************************************************/


export const clearPeopleSearch = () => ({
    type: actionTypes.CLEAR_PEOPLE_SEARCH
});

const findPeopleCompleted = peopleSearch =>({
    type: actionTypes.FIND_PEOPLE_COMPLETED,
    peopleSearch
})

export const findPeople = criteria => dispatch => {
    backend.searchService.findPeople(criteria,
        result => dispatch(findPeopleCompleted({criteria, result})));
}


export const clearExternalPeopleSearch = () => ({
    type: actionTypes.CLEAR_EXTERNAL_PEOPLE_SEARCH
});

const findExternalPeopleCompleted = externalPeopleSearch =>({
    type: actionTypes.FIND_EXTERNAL_PEOPLE_COMPLETED,
    externalPeopleSearch
})

export const findExternalPeople = criteria => dispatch => {
    backend.searchService.findExternalPeople(criteria,
        result => dispatch(findExternalPeopleCompleted({criteria, result})));
}


/******************************************************/
/*********************** LISTS ************************/
/******************************************************/

export const clearListSearch = () => ({
    type: actionTypes.CLEAR_LIST_SEARCH
});

const findListsCompleted = listSearch =>({
    type: actionTypes.FIND_LISTS_COMPLETED,
    listSearch
})

export const findLists = criteria => dispatch => {

    backend.searchService.findLists(criteria,
        result => dispatch(findListsCompleted({criteria, result})));

}

export const previousFindListsResultPage = criteria =>
    findLists({...criteria, page: criteria.page-1});

export const nextFindListsResultPage = criteria =>
    findLists({...criteria, page: criteria.page+1});

