import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialLanguage = null;

const language = (state = initialLanguage, action) => {

    switch (action.type) {

        case actionTypes.CHANGE_LANGUAGE:
            return action.language;

        default:
            return state;

    }

}

const reducer = combineReducers({
    language
});

export default reducer;