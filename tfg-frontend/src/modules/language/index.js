/* eslint-disable import/no-anonymous-default-export */
import * as actions from './actions';
import reducer from './reducer'
import * as selectors from './selectors';

export default {actions, reducer, selectors};