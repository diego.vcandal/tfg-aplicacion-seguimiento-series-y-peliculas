export const AvailableLanguages = [
    {id: 0, type: "gl", name: "project.languages.galician"},
    {id: 1, type: "es-ES", name: "project.languages.spanish"},
    {id: 2, type: "en", name: "project.languages.english"}
]