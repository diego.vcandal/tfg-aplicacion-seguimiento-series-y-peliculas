const getModuleState = state => state.people;

export const getFoundPerson = state =>
    getModuleState(state).foundPerson;

export const getFoundCast = state => 
    getModuleState(state).foundCastHistory;

export const getFoundCrew = state => 
    getModuleState(state).foundCrewHistory;
