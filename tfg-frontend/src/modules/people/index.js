/* eslint-disable import/no-anonymous-default-export */
import * as actions from './actions';
import * as actionTypes from './actionTypes';
import reducer from './reducer';
import * as selectors from './selectors';

export {default as CreatePerson} from './components/CreatePerson';
export {default as UpdatePerson} from './components/UpdatePerson';
export {default as PersonDetails} from './components/PersonDetails';

export default {actions, actionTypes, reducer, selectors};