import * as actionTypes from './actionTypes';
import backend from '../../backend';


const findPersonByIdCompleted = foundPerson => ({
    type: actionTypes.FIND_PERSON_COMPLETED,
    foundPerson
});

export const clearFoundPerson = () => ({
    type: actionTypes.CLEAR_FOUND_PERSON
});

export const findInternalPersonById = (id, onErrors) => dispatch => {

    backend.peopleService.findInternalPerson(id,
        foundPerson => dispatch(findPersonByIdCompleted(foundPerson)),
        onErrors);

}

export const updatePersonCompleted = () => ({
    type: actionTypes.UPDATE_PERSON_COMPLETED,
});

export const updatePerson = (person, onSuccess, onErrors) => dispatch => {
    backend.peopleService.updatePerson(person, 
        () => {
            dispatch(updatePersonCompleted());
            onSuccess();
        }, onErrors);
}

export const createPersonCompleted = () => ({
    type: actionTypes.CREATED_PERSON_COMPLETED,
});

export const createPerson = (person, onSuccess, onErrors) => dispatch => {
    backend.peopleService.createPerson(person, 
        ({id}) => {
            dispatch(createPersonCompleted());
            onSuccess(id);
        }, onErrors);
}


export const findPersonById = (id, onErrors) => dispatch => {

    backend.peopleService.findPerson(id,
        foundPerson => dispatch(findPersonByIdCompleted(foundPerson)),
        onErrors);

}

export const findExternalPersonById = (id, onErrors) => dispatch => {

    backend.peopleService.findExternalPerson(id,
        foundPerson => dispatch(findPersonByIdCompleted(foundPerson)),
        onErrors);

}


export const makePublic = (id, onSuccess, onErrors) => dispatch => {
    backend.peopleService.makePublic(id, 
        () => {
            dispatch(updatePersonCompleted());
            onSuccess();
        }, onErrors);
}

export const changeSourceType = (id, onSuccess, onErrors) => dispatch => {
    backend.peopleService.changeSourceType(id, 
        () => {
            dispatch(updatePersonCompleted());
            onSuccess();
        }, onErrors);
}

export const setForDeletion = (id, onSuccess, onErrors) => dispatch => {
    backend.peopleService.setForDeletion(id, 
        () => {
            dispatch(updatePersonCompleted());
            onSuccess();
        }, onErrors);
}


export const createCachePerson = (id, onSuccess, onErrors) => dispatch => {
    backend.peopleService.createCachePerson(id, 
        () => {
            dispatch(createPersonCompleted());
            onSuccess();
        }, onErrors);
}



export const clearFoundCastAndCrewHistory = () => ({
    type: actionTypes.CLEAR_FOUND_CAST_AND_CREW_HISTORY
});

export const findCrewHistoryCompleted = foundCrewHistory => ({
    type: actionTypes.FIND_CREW_HISTORY_COMPLETED,
    foundCrewHistory
});

export const findAllCrewHistory = (id, onErrors) => dispatch => {
    backend.peopleService.findCrewHistory(id, 
        (foundCrewHistory) => {
            dispatch(findCrewHistoryCompleted(foundCrewHistory));
        }, onErrors);
}

export const findCastHistoryCompleted = foundCastHistory => ({
    type: actionTypes.FIND_CAST_HISTORY_COMPLETED,
    foundCastHistory
});

export const findAllCastHistory = (id, onErrors) => dispatch => {
    backend.peopleService.findCastHistory(id, 
        (foundCastHistory) => {
            dispatch(findCastHistoryCompleted(foundCastHistory));
        }, onErrors);
}

export const findExternalCastAndCrewHistoryCompleted = foundCastAndCrewHistory => ({
    type: actionTypes.FIND_EXTERNAL_CAST_AND_CREW_HISTORY_COMPLETED,
    foundCastAndCrewHistory
});

export const findAllExternalCastAndCrewHistory = (id, onErrors) => dispatch => {
    backend.peopleService.findCastCrewExternalHistory(id, 
        (foundCastAndCrewHistory) => {
            dispatch(findExternalCastAndCrewHistoryCompleted(foundCastAndCrewHistory));
        }, onErrors);
}
