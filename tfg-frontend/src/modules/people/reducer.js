import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    foundPerson: null,
    foundCastHistory: null,
    foundCrewHistory: null,
};

const foundPerson = (state = initialState.foundPerson, action) => {

    switch (action.type) {

        case actionTypes.FIND_PERSON_COMPLETED:
            return action.foundPerson;
            
        case actionTypes.CLEAR_FOUND_PERSON:
            return initialState.foundPerson;

        default:
            return state;

    }

}

const foundCastHistory = (state = initialState.foundCastHistory, action) => {

    switch (action.type) {

        case actionTypes.FIND_CAST_HISTORY_COMPLETED:
            return action.foundCastHistory;
            
        case actionTypes.CLEAR_FOUND_CAST_AND_CREW_HISTORY:
            return initialState.foundCastHistory;

        case actionTypes.FIND_EXTERNAL_CAST_AND_CREW_HISTORY_COMPLETED:
            return action.foundCastAndCrewHistory.cast;

        default:
            return state;

    }

}

const foundCrewHistory = (state = initialState.foundCrewHistory, action) => {

    switch (action.type) {

        case actionTypes.FIND_CREW_HISTORY_COMPLETED:
            return action.foundCrewHistory;
            
        case actionTypes.CLEAR_FOUND_CAST_AND_CREW_HISTORY:
            return initialState.foundCrewHistory;

        case actionTypes.FIND_EXTERNAL_CAST_AND_CREW_HISTORY_COMPLETED:
            return action.foundCastAndCrewHistory.crew;

        default:
            return state;

    }

}

const reducer = combineReducers({
    foundPerson,
    foundCastHistory,
    foundCrewHistory
});

export default reducer;
