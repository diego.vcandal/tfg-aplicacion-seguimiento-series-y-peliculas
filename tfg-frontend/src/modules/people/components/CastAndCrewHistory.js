import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import * as selectors from '../selectors';
import * as actions from '../actions';
import { CrewTypes } from '../../titles/util/CrewTypes';

const CastAndCrewHistory = () => {

    const dispatch = useDispatch();

    const foundCast = useSelector(selectors.getFoundCast);
    const foundCrew = useSelector(selectors.getFoundCrew);

    const foundPerson = useSelector(selectors.getFoundPerson);

    useEffect(() => {

        if (foundPerson.sourceType === 0){
            dispatch(actions.findAllCastHistory(foundPerson.id));
            dispatch(actions.findAllCrewHistory(foundPerson.id));
        } else {
            dispatch(actions.findAllExternalCastAndCrewHistory(foundPerson.externalId));
        } 

        return () => dispatch(actions.clearFoundCastAndCrewHistory());

    }, [foundPerson, dispatch]);

    foundCast && foundCast.sort(function(a,b) {
        if (a.year === b.year) return 0;
        if (a.year <= 0 ) return -1;
        if (b.year <= 0 ) return 1;
        if (a.year < b.year) return 1;
        if (a.year > b.year) return -1;
        return 0;
    });

    foundCrew && foundCrew.sort(function(a,b) {
        if (a.year === b.year) return 0;
        if (a.year <= 0 ) return -1;
        if (b.year <= 0 ) return 1;
        if (a.year < b.year) return 1;
        if (a.year > b.year) return -1;
        return 0;
    });

    return (

        <div className="col-12 center-element p-0">

            {foundCast &&
                <h3 className="m-0 mt-4 main-cast-title">
                    <FormattedMessage id='project.titles.update.menu.cast'/>
                </h3>
            }

            <div className="row w-90 shadow-container p-2 m-0 mt-3">
                {foundCast && foundCast.map(cast =>
                    <div key={cast.id} className="row w-100 mt-2 mb-2">
                        
                        <div className="col-2 text-center">
                            <span className="cast-title-year">
                                {cast.year > 0 ? cast.year : "---"}
                            </span>
                        </div>

                        <div className="col-10 pl-0">
                            <span className="cast-title-name"
                                onClick={null}>
                                {cast.titleName ? cast.titleName : cast.originalTitle }
                            </span>
                            {cast.episodeCount > 0 && <span className="cast-title-episodes">{" (" + cast.episodeCount} <FormattedMessage id='project.titles.info.episodes'/>{")"}</span>}
                            {cast.characterName && <span className="cast-title-character">{", " + cast.characterName}</span>}
                        </div>

                    </div>  
                )}
            </div>

            {foundCast &&
                <h3 className="m-0 mt-5 main-cast-title">
                    <FormattedMessage id='project.titles.update.menu.crew'/>
                </h3>
            }

            <div className="row w-90 shadow-container p-2 m-0 mt-3">
                {foundCrew && foundCrew.map(crew => 
                    <div key={crew.id} className="row w-100 mt-2 mb-2">
                        
                        <div className="col-2 text-center">
                        <span className="cast-title-year">
                                {crew.year > 0 ? crew.year : "---"}
                            </span>
                        </div>

                        <div className="col-10 pl-0">
                            <span className="cast-title-name"
                                onClick={null}>
                                {crew.titleName ? crew.titleName : crew.originalTitle }
                            </span>
                            {crew.episodeCount > 0 && <span className="cast-title-episodes">{" (" + crew.episodeCount} <FormattedMessage id='project.titles.info.episodes'/>{")"}</span>}
                            {(crew.crewTypeId || crew.crewTypeId !== null) ? 
                                <span className="cast-title-character">{", "} <FormattedMessage id={CrewTypes.find(ct => ct.id === crew.crewTypeId).name}/></span>
                            :
                                <span className="cast-title-character">{", " + crew.department}</span>
                            }
                        </div>

                    </div>  
                )}
            </div>

        </div>
    );

}

export default CastAndCrewHistory;