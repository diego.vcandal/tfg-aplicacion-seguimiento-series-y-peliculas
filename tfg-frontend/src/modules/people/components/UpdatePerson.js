/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams, useHistory} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

import CreatePerson from './CreatePerson';
import LooseChanges from './Modals/LooseChanges';
import EditTraductions from './EditTraductions';

const EditPerson = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    
    const [actualPanel, setActualPanel] = useState(0);
    const {personId} = useParams();
    const [backendErrors, setBackendErrors] = useState(null);
    const [madeChanges, setMadeChanges] = useState(false);   

    const foundPerson = useSelector(selectors.getFoundPerson);

    let dialog, selectedCode;
    let dialogId = "looseAllChanges";
    let list = [{id: 0, ref: null}, 
        {id: 1, ref: null}];

    const baseClass = "list-group-item list-group-item-action col";

    useEffect(() => {

        const foundPerson = Number(personId);

        if (!Number.isNaN(foundPerson)) {
            dispatch(actions.findInternalPersonById(foundPerson));
        }

        return () => dispatch(actions.clearFoundPerson());

    }, [personId, dispatch]);

    
    const handleUpdatePanel = (code) => {
        selectedCode = code;

        if (madeChanges) 
            dialog.click();
        else
            onDialogConfirm();

    };

    if (!foundPerson || !foundPerson.internal) {
        return null;
    }

    const onDialogConfirm = () => {
        setActualPanel(selectedCode);
        list.map(e => e.id === selectedCode ? e.ref.className = baseClass + " active" : e.ref.className = baseClass)
        setMadeChanges(false);
    };

    return (
        <div className="row mt-4">

            <div className="row w-100 mb-4 ml-0 ">

                {foundPerson.sourceType !== 0 ?
                    <span className="TMDB-type-tag font-weight-600 mr-3 font-size-update-title-tag">
                        <FormattedMessage id='project.global.type.TMDB'/>
                    </span>
                :
                    null
                }

                <span className="font-size-update-title">
                    <FormattedMessage id='project.common.title.update'/>
                    {" - "}
                    <a className="link-primary" onClick={() => history.push("/people/" + foundPerson.id)}>
                        {foundPerson.originalName}
                    </a>
                </span>
            </div>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="side-update-menu-container shadow-container col-2 list-group">

                <button type="button" className={baseClass + " active"} ref={node => list[0].ref = node}
                    onClick={e => handleUpdatePanel(0)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.details'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[1].ref = node}
                    onClick={e => handleUpdatePanel(1)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.traductions'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

            </div>

            {actualPanel === 0 &&
                <CreatePerson update={true} setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 1 &&
                <EditTraductions madeChanges={madeChanges} setMadeChanges={setMadeChanges}/>
            }

            <button className="btn btn-danger button-align-right button-width-50" 
                ref={node => dialog = node} hidden={true}
                data-toggle="modal" data-target={"#" + dialogId}>
            </button >
            
            <LooseChanges id={dialogId} onConfirm={onDialogConfirm}></LooseChanges>

        </div>

    );

}

export default EditPerson;