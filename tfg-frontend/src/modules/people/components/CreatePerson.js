import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import PropTypes from 'prop-types';

import {AvailableLanguages} from '../../language/AvailableLanguages';

const CreatePerson = ({update, setMadeChanges}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const foundPerson = useSelector(selectors.getFoundPerson);
    const canUpdate = update && foundPerson;

    const person_default_image = `${process.env.PUBLIC_URL}/images/person_default_image.png`;
    let person_image;

    if(foundPerson)
        person_image = `${process.env.REACT_APP_BACKEND_URL}/people/${foundPerson.id}/image`;

    const [place, setPlace]  = useState('');
    const [externalId, setExternalId]  = useState(canUpdate ? foundPerson.externalId : '');
    const [name, setName]  = useState(canUpdate ? foundPerson.originalName : '');
    const [backendErrors, setBackendErrors] = useState(null);
    const [description, setDescription]  = useState(canUpdate ? foundPerson.originalDescription : '');
    const [language, setLanguage]  = useState("0");

    let form;

    let classCreate="center-element w-80 mt-4";
    let classUpdate="center-element col-9 mt-4mt-4";

    let foundDateOfBirth = new Date().getTime();
    let foundDateOfDeath;

    if (canUpdate) {
        foundDateOfBirth = foundPerson.birthday && foundPerson.birthday !== null ? 
            foundPerson.birthday : '';
        foundDateOfDeath = foundPerson.deathday && foundPerson.deathday !== null ? 
            foundPerson.deathday : '';
    }

    const [dateOfBirth, setDateOfBirth] = useState(foundDateOfBirth);
    const [dateOfDeath, setDateOfDeath] = useState(foundDateOfDeath);
    const [portrait, setPortrait]  = useState(!foundPerson || !foundPerson.hasImage ? person_default_image : person_image);

    const handleSubmit = event => {
        
        event && event.preventDefault();
        
        if (form.checkValidity()) {

            if (!update) {

                var img = portrait === person_image || portrait === person_default_image ? null : portrait;

                let person = {
                    externalId: externalId,
                    originalName: name,
                    birthday: Number(dateOfBirth),
                    deathday: Number(dateOfDeath),
                    image: img,
                    traductions: [
                        {
                            idLocale: language,
                            name: name,
                            biography: description,
                            birthPlace: place,
                            originalLanguage: true
                        }
                    ]
                }
    
                dispatch(actions.createPerson(person, (id) => history.push('/people/' + id), errors => setBackendErrors(errors)))
                
            } else {

                foundPerson.birthday = dateOfBirth !== "" ? Number(dateOfBirth): null;
                foundPerson.deathday = dateOfDeath !== "" ? Number(dateOfDeath): null;
                foundPerson.image = portrait === person_image ? null : portrait === person_default_image ? "" : portrait;
    
                dispatch(actions.updatePerson(foundPerson, () => history.go(0), errors => setBackendErrors(errors)))

            }
            
        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const handleParseDate = (date) => {
        let day = date.getDate();
        let month = date.getMonth() + 1;
        return date.getFullYear() + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;
    }

    const handlePortraitChange = img => {
        var fileReader= new FileReader();
    
        fileReader.addEventListener("load", function(e) {
            setPortrait(e.target.result);
        }); 
        
        fileReader.readAsDataURL(img);

        canUpdate && setMadeChanges(true);
    }

    return (
        <div className={update ? classUpdate : classCreate}>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col shadow-container" noValidate onSubmit={e => handleSubmit(e)}>

                    <div className="row p-3">

                        {!update &&
                            <div className="col-7">
                                <FormattedMessage id="project.people.create.title">
                                    {
                                        (msg) => <input type="text" id="userName" className="col form-control name-input ml-0 mb-3"
                                        onChange={e => setName(e.target.value)}
                                        autoFocus
                                        value={name}
                                        placeholder={msg}
                                        required
                                        disabled={update} />
                                    }
                                </FormattedMessage>
                            </div>
                        }

                        {!update &&
                            <div className="col display-i-f">
                                <div className="col-12 mr-1 float-right text-right center-element-v">
                                    <label htmlFor="language" className="col-6 title-label-color p-0">
                                        <FormattedMessage id="project.people.create.originalLanguage"/>
                                    </label>
                                    <select id="language" className="custom-select col-5 ml-3"
                                        value={language}
                                        onChange={e => setLanguage(e.target.value)} >
                                        {AvailableLanguages.map(type => 
                                            <FormattedMessage id={type.name}>
                                                {
                                                    (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                                }
                                            </FormattedMessage>
                                        )}
                                    </select>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-circle mb-1 mr-2" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                                    </svg>
                                    <span className="light-gray-text"><FormattedMessage id="project.people.create.originalLanguage.info"/></span>
                                </div>
                            </div>
                        }

                        <div className="col-7 mt-2">

                            {update &&
                                <div className="row">
                                    <FormattedMessage id="project.people.create.title">
                                        {
                                            (msg) => <input type="text" id="userName" className="col form-control name-input ml-0 mb-3"
                                            onChange={e => setName(e.target.value)}
                                            autoFocus
                                            value={name}
                                            placeholder={msg}
                                            required
                                            disabled={update} />
                                        }
                                    </FormattedMessage>
                                </div>
                            }

                            <div className="row ml-2 mb-4">
                                <label htmlFor="externalId" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.externalId"/>
                                </label>
                                <div className="col-4 align-input-center">
                                    <input type="number" id="externalId" className="form-control"
                                        value={externalId}
                                        onChange={e => setExternalId(e.target.value)}
                                        required min="1"
                                        disabled={update} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                            

                            {!update &&
                                <div className="row ml-2 mb-4">
                                    <label htmlFor="placeOfBirth" className="col-12 title-label-color">
                                        <FormattedMessage id="project.people.create.placeOfBirth"/>
                                    </label>
                                    <div className="col-8 align-input-center">
                                        <input  id="placeOfBirth" className="form-control"
                                            value={place}
                                            onChange={e => {setPlace(e.target.value); canUpdate && setMadeChanges(true)}}
                                            required />
                                        <div className="invalid-feedback">
                                            <FormattedMessage id='project.global.validator.required'/>
                                        </div>
                                    </div>
                                </div>
                            }

                            <div className="row ml-2 mb-4">
                                <div className="col w-25">
                                    <label htmlFor="dateOfBirth" className="title-label-color">
                                        <FormattedMessage id="project.people.create.dateOfBirth"/>
                                    </label>
                                    <div className="align-input-center">
                                        <input className="form-control" type="date" 
                                            value={handleParseDate(new Date(dateOfBirth))} 
                                            onChange={e => {setDateOfBirth(new Date(e.target.value).getTime()); canUpdate && setMadeChanges(true)}}
                                            id="dateOfBirth" />
                                    </div>
                                </div>

                                <div className="col w-25">
                                    <label htmlFor="dateOfDeath" className="title-label-color">
                                        <FormattedMessage id="project.people.create.dateOfDeath"/>
                                    </label>
                                    <div className="align-input-center">
                                        <input className="form-control" type="date" 
                                            value={handleParseDate(new Date(dateOfDeath))} 
                                            onChange={e => {setDateOfDeath(new Date(e.target.value).getTime()); canUpdate && setMadeChanges(true)}}
                                            id="dateOfDeath" />
                                    </div>
                                </div>
                            </div>

                            {!update &&
                                <div className="w-100 row mr-3 ml-2">
                                    <label htmlFor="biography" className="col-12 title-label-color">
                                        <FormattedMessage id="project.people.create.biography"/>
                                    </label>
                                    <div className="col align-input-center">
                                        <textarea className="form-control" id="biography" rows="7" 
                                            value={description}
                                            maxLength="1000" 
                                            onChange={e => {setDescription(e.target.value); canUpdate && setMadeChanges(true)}}></textarea>
                                    </div>
                                    
                                </div>
                            }
                        </div>

                        <div className="col mt-3">
                            <div className="col update-portrait-container">
                                <img id="portrait-img" src={portrait} className="person-update-image border-radius-15 center-element" alt="portrait img"/>
                                <input id="portraitFile" type="file" name="image" className="inputfile" 
                                    onChange={e => handlePortraitChange(e.target.files[0])} 
                                    accept="image/*"
                                    multiple={false} />
                                <div className="row mt-3 w-90 center-element">
                                    <div className="col-12 ">
                                        <label htmlFor="portraitFile" className="label-image" >
                                            <FormattedMessage id="project.users.profileEdit.selectImage"/>
                                        </label>
                                    </div>
                                    <div className="col-12">
                                        <span className="label-image red-label" onClick={() => {setPortrait(person_default_image); canUpdate && setMadeChanges(true);}}>
                                            <span><FormattedMessage id="project.users.profileEdit.deleteImage"/></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className="col ml-4 mt-4">
                                <span className="row w-100 title-img-type-color p-0 mb-3"><FormattedMessage id="project.people.create.update.images.portrait"/></span>
                                <span className="row w-100 mb-2"><FormattedMessage id="project.people.create.update.images.portrait.instructions"/></span>
                                <ul className="row w-100">
                                    <li><FormattedMessage id="project.people.create.update.images.portrait.minRes"/></li>
                                    <li><FormattedMessage id="project.people.create.update.images.portrait.ratio"/></li>
                                </ul>
                            </div>
                        </div>
                        <div className="w-100 row mr-3 mt-3 mb-2">
                            <div className="col title-submit-btn-container p-0">
                                {!update &&
                                    <button type="button" className="btn btn-secondary ml-3 mr-3 float-right" 
                                            onClick={() => {history.goBack()} }>
                                        <FormattedMessage id="project.global.buttons.cancel"/>
                                    </button>
                                }
                                <button type="submit" className="btn btn-success float-right">
                                    <FormattedMessage id="project.titles.create.buttons.add"/>
                                </button>
                            </div>   
                        </div>
                    </div>
            </form>
        </div>
    );

}

CreatePerson.propTypes = {
    update: PropTypes.bool.isRequired,
    setMadeChanges: PropTypes.func.isRequired
}

export default CreatePerson;