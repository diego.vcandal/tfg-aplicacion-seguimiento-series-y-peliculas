import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

import {AvailableLanguages} from '../../language/AvailableLanguages';
import LooseChanges from './Modals/LooseChanges';

const EditTraductions = ({madeChanges, setMadeChanges}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const foundPerson = useSelector(selectors.getFoundPerson);
    const [backendErrors, setBackendErrors] = useState(null);

    let firstLanguage = foundPerson && foundPerson.traductions && foundPerson.traductions.length > 0 ? 
        foundPerson.traductions[0] : null;

    const [language, setLanguage] = useState(firstLanguage ? firstLanguage.idLocale : 0);
    const [name, setName] = useState(firstLanguage ? firstLanguage.name : '');
    const [birthPlace, setBirthPlace] = useState(firstLanguage ? firstLanguage.birthPlace : '');
    const [biography, setBiography] = useState(firstLanguage ? firstLanguage.biography : '');   

    let form, dialog;
    let dialogId = "looseTraductionChanges";
    let dialogOpened = false;
    let selectedLanguage = firstLanguage ? firstLanguage.idLocale : -1;

    const handleSubmit = event => {
       
        if (dialogOpened) {
            return;
        }

        event && event.preventDefault();
        
        if (form.checkValidity()) {

            let trad = foundPerson.traductions.find(t => t.idLocale === Number(language));

            if (trad) {
                trad.name = name;
                trad.birthPlace = birthPlace;
                trad.biography = biography;
            } else {
                foundPerson.traductions.push({
                    idLocale: Number(language),
                    name: name,
                    biography: biography,
                    birthPlace: birthPlace,
                });
            }

            dispatch(actions.updatePerson(foundPerson, () => history.go(0), errors => setBackendErrors(errors)))

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    if (!foundPerson)
        return null;

    let locales = [];
    foundPerson.traductions.forEach(t => {
        locales.push(AvailableLanguages.find(l => t.idLocale === l.id));
    });

    const handleOnChangedTraduction = (id) => {

        selectedLanguage = id;
        dialogOpened = true;

        if (madeChanges) 
            dialog.click();
        else
            onDialogConfirm();

    };

    const onDialogConfirm = () => {

        if (selectedLanguage < 0)
           return; 
        
        let traductions = foundPerson.traductions;
        let actualTraduction = traductions.find(t => t.idLocale === Number(selectedLanguage));
        
        if (actualTraduction) {
            setName(actualTraduction.name);
            setBiography(actualTraduction.biography);
            setBirthPlace(actualTraduction.birthPlace);
        } else {
            setName('');
            setBiography('');
            setBirthPlace('');
        }
        
        setLanguage(selectedLanguage);
        setMadeChanges(false);
        
    };

    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="col pr-0 mb-3">
                <div className="col-3 m-float-right pr-0 text-right">
                    <select id="language" className="custom-select col-11 "
                        value={language}
                        onChange={e => handleOnChangedTraduction(e.target.value)} >
                        {AvailableLanguages.map(type =>
                            <FormattedMessage id={type.name}>
                                {
                                    (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                }
                            </FormattedMessage>
                        )}
                    </select>
                </div>
            </div>

            <form ref={node => form = node}
                className="needs-validation col shadow-container" noValidate onSubmit={e => handleSubmit(e)}>

                    <div className="row p-3">

                        <div className="w-100 row mb-3">
                            <div className="col-10 mr-3">
                                <div className="col-12 align-input-center">
                                    <input type="text" id="title" className="col form-control name-input ml-0 mb-3"
                                        value={foundPerson.originalName}
                                        required disabled/>
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-6 mr-3">
                                <label htmlFor="name" className="col-12 title-label-color" >
                                    <FormattedMessage id="project.people.create.title.label"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input type="text" id="name" className="form-control "
                                        value={name}
                                        onChange={e => {setName(e.target.value); setMadeChanges(true)}}
                                        required />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-6 mr-3">
                                <label htmlFor="birthPlace" className="col-12 title-label-color" >
                                    <FormattedMessage id="project.people.create.placeOfBirth"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input type="text" id="birthPlace" className="form-control "
                                        value={birthPlace}
                                        onChange={e => {setBirthPlace(e.target.value); setMadeChanges(true)}}
                                        required />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-9 mr-3 mt-3">
                                <label htmlFor="biography" className="col-12 title-label-color">
                                    <FormattedMessage id="project.people.create.biography"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <textarea className="form-control" id="biography" rows="7" 
                                        value={biography}
                                        maxLength="500"
                                        onChange={e => {setBiography(e.target.value); setMadeChanges(true)}}>
                                    </textarea>
                                </div>
                            </div>
                        

                            <div className="col title-submit-btn-container p-0">
                                <div>
                                    <button type="submit" className="btn btn-success float-right">
                                        <FormattedMessage id="project.titles.create.buttons.add"/>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <button className="btn btn-danger button-align-right button-width-50" 
                        ref={node => dialog = node} hidden={true}
                        data-toggle="modal" data-target={"#" + dialogId}
                        onClick={e => e.preventDefault()}>
                    </button>

                    <LooseChanges id={dialogId} onConfirm={onDialogConfirm} onCancel={() => dialogOpened = false}></LooseChanges>
            </form>
        </div>
    );

}

export default EditTraductions;