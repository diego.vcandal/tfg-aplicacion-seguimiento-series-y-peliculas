/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage, FormattedDate} from 'react-intl';
import {useParams} from 'react-router-dom';
import {useHistory, useLocation, Redirect} from 'react-router-dom';

import {PageNotFound, Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import users from '../../user';
import AdminOptionsModal from './Modals/AdminOptionsModal';
import CastAndCrewHistory from './CastAndCrewHistory';

const PersonDetails = () => {

    const dispatch = useDispatch();
    const {id, externalId} = useParams();
    const history = useHistory();
    const location = useLocation().pathname;

    const foundPerson = useSelector(selectors.getFoundPerson);
    const isAdmin = useSelector(users.selectors.isAdmin);

    const [backendErrors, setBackendErrors] = useState(null);

    const deleteModal = "delete-modal";
    const makePublicModal = "make-public-modal";
    const changeSourceInternalModal = "change-source-internal-modal";
    const changeSourceExternalModal = "change-source-external-modal";

    useEffect(() => {

        if (id) {
            const foundPersonId = Number(id);

            if (!Number.isNaN(foundPersonId)) {
                dispatch(actions.findPersonById(foundPersonId));
            }

        } else if (externalId) {
            const foundPersonId = Number(externalId);

            if (!Number.isNaN(foundPersonId)) {
                dispatch(actions.findExternalPersonById(foundPersonId));
            }

        }
        
        return () => dispatch(actions.clearFoundPerson());

    }, [id, externalId, dispatch]);

    if (!foundPerson) {
        return <PageNotFound />;
    }

    if (foundPerson && (foundPerson.externalId === Number(externalId)) && foundPerson.id > 0) {
        return <Redirect to={"/people/" + foundPerson.id} />;
    }
    
    let person_default_image = `${process.env.PUBLIC_URL}/images/person_default_image.png`;
    let person_image = person_default_image;
    
    if(foundPerson.sourceType !== 0){
        person_image = "https://image.tmdb.org/t/p/w500" + foundPerson.imageExternalPath;
    } else {
        if(foundPerson.image)
            person_image = `${process.env.REACT_APP_BACKEND_URL}/people/${foundPerson.id}/image`;
    }

    return (

        <div className="center-element w-80 mt-4">
            <div className="row-12">

                <div className="row-12">
                    <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>
                </div>

                <div className="row">
                    <div className="col-3 user-img-column">
                        <img id="img-user" alt="user profile image" src={person_image} className="border-radius-15 mb-3"
                            onError={e => {e.target.src = person_default_image}}/>

                        {isAdmin && foundPerson && foundPerson.id && foundPerson.id === -1 &&
                            <div className="row center-element">
                                <button className="btn btn-success btn-cache-title mb-4 w-100" 
                                    type="button" title="Create"
                                    onClick={() => dispatch(actions.createCachePerson(foundPerson.externalId, 
                                        () => history.go(0), 
                                        errors => setBackendErrors(errors)))
                                    }>
                                    <FormattedMessage id='project.titles.button.create'/>
                                </button>
                            </div>
                        }
                        
                        {foundPerson.originalName && foundPerson.originalName !== foundPerson.name &&
                            <div className="row center-element w-90">
                                <span className="title-sub-info-title w-100 row">
                                    <FormattedMessage id='project.people.label.name.alternative'/>
                                </span>
                                <p className="title-sub-info ml-4">{foundPerson.originalName}</p>
                            </div>
                        }

                        {(foundPerson.birthday || foundPerson.birthday !== 0) &&
                            <div className="row center-element w-90">
                                <span className="title-sub-info-title w-100 row">
                                    <FormattedMessage id='project.people.create.dateOfBirth'/>
                                </span>
                                <p className="title-sub-info ml-4">
                                    <FormattedDate value={new Date(foundPerson.birthday)}/>
                                </p>
                            </div>
                        }

                        {(foundPerson.deathday || foundPerson.deathday !== null) &&
                            <div className="row center-element w-90">
                                <span className="title-sub-info-title w-100 row">
                                    <FormattedMessage id='project.people.create.dateOfDeath'/>
                                </span>
                                <p className="title-sub-info ml-4">
                                    <FormattedDate value={new Date(foundPerson.deathday)}/>
                                </p>
                            </div>
                        }

                    </div>
                    <div className="col ml-3">
                        <div>
                            <div className={"row pt-1 pb-2 pr-1 " + (isAdmin && foundPerson.setForDelete ? "set-for-delete-container" : "")}>
                                <div className="col-8 p-0 d-flex">
                                    {foundPerson.sourceType !== 0 &&
                                        <span className="TMDB-type-tag-person font-weight-600 mr-2 center-element-v">
                                            <FormattedMessage id='project.global.type.TMDB'/>
                                        </span>
                                    }
                                    <h4 className="person-username-title center-element-v">{foundPerson.name}</h4>
                                </div>

                                {isAdmin && foundPerson && foundPerson.id && foundPerson.id !== -1 &&
                                    <div className="col-4 admin-panel p-0">
                                            {isAdmin && foundPerson.setForDelete && 
                                                <span className="deletion-tag mb-1 d-block m-float-right"><FormattedMessage id='project.titles.setForDeletion'/></span>
                                            }
                                            {foundPerson.visibility &&
                                                <button className="btn btn-primary btn-sm" type="button" title="ChangeSource"
                                                        data-toggle="modal" data-target={"#" + (foundPerson.sourceType === 0 ? changeSourceExternalModal : changeSourceInternalModal)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-gear-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                                                    </svg>
                                                </button>
                                            }
                                            {foundPerson.sourceType === 0 && !foundPerson.setForDelete &&
                                                <button className="btn btn-danger btn-sm ml-2" type="button" title="Delete"
                                                    data-toggle="modal" data-target={"#" + deleteModal}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                    </svg>
                                                </button>
                                            }
                                            <button className="btn btn-success btn-sm ml-2" type="button" title="Edit"
                                                    onClick={() => history.push(location + '/update')}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                </svg>
                                            </button>
                                            {!foundPerson.visibility &&
                                                <button className="btn btn-secondary btn-sm ml-2" type="button" title="MakePublic"
                                                    data-toggle="modal" data-target={"#" + makePublicModal}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-eye-slash-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="m10.79 12.912-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                                        <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829zm3.171 6-12-12 .708-.708 12 12-.708.708z"/>
                                                    </svg>
                                                </button>
                                            }
                                        </div> 
                                    }
                            </div>
                            <div className="row">
                                <p className="font-weight-600 light-gray-text">{foundPerson.birthPlace}</p>
                            </div>
                        </div>

                        <div className="row mt-1">
                            <span className="title-pre-title-box">
                            </span>
                            <span className="title-overview-title">
                                <FormattedMessage id='project.people.create.biography'/>
                            </span>
                            <span className="mt-2 w-100 align-text-justify">
                                {foundPerson.biography ? 
                                    foundPerson.biography
                                :
                                    <FormattedMessage id='project.titles.info.noData'/>}
                            </span>
                        </div>
                   
                        <CastAndCrewHistory />

                    </div>
                </div>

                        

            </div>
            
            <AdminOptionsModal id={deleteModal} type={0} 
                onConfirm={() => dispatch(actions.setForDeletion(foundPerson.id, () => history.go(0), null))} />

            <AdminOptionsModal id={makePublicModal} type={1} 
                onConfirm={() => dispatch(actions.makePublic(foundPerson.id, () => history.go(0), null))} />

            <AdminOptionsModal id={changeSourceInternalModal} type={2}
                onConfirm={() => dispatch(actions.changeSourceType(foundPerson.id, () => history.go(0), null))} />

            <AdminOptionsModal id={changeSourceExternalModal} type={3} 
                onConfirm={() => dispatch(actions.changeSourceType(foundPerson.id, () => history.go(0), null))} />

        </div>
    );

}

export default PersonDetails;