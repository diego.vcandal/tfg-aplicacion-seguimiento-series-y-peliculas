export const CREATED_PERSON_COMPLETED = "project/people/createPersonCompleted";
export const UPDATE_PERSON_COMPLETED = "project/people/updatePersonCompleted";
export const FIND_PERSON_COMPLETED = "project/people/findPersonByNumberCompleted";
export const CLEAR_FOUND_PERSON = "project/people/clearFoundPerson";

export const CLEAR_FOUND_CAST_AND_CREW_HISTORY = "project/people/clearFoundCastAndCrewHistory";
export const FIND_CREW_HISTORY_COMPLETED = "project/people/findCrewHistoryCompleted";
export const FIND_CAST_HISTORY_COMPLETED = "project/people/findCastHistoryCompleted";
export const FIND_EXTERNAL_CAST_AND_CREW_HISTORY_COMPLETED = "project/people/findExternalCastAndCrewHistoryCompleted";