export const FollowingStatus = [
    {id: 0, name: "project.users.followingList.status.planToWatch"},
    {id: 1, name: "project.users.followingList.status.watching"},
    {id: 2, name: "project.users.followingList.status.completed"},
    {id: 3, name: "project.users.followingList.status.dropped"}
]

export const getStatusNameCode = (id) => {
    return FollowingStatus.find(s => s.id === id).name;
}