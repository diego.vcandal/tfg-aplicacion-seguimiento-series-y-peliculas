/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams} from 'react-router-dom';
import {useHistory, useLocation, Link} from 'react-router-dom';

import {PageNotFound, Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import ProfileFriendsSummaryView from './ProfileFriendsSummaryView';

const AllLists = () => {

    const user = useSelector(selectors.getUser);
    const foundUser = useSelector(selectors.getFoundUser);
    const customLists = useSelector(selectors.getCustomLists);
    const dispatch = useDispatch();
    const {id} = useParams();

    const history = useHistory();
    const location = useLocation().pathname;

    const [password, setPassword] = useState("");
    const [disable, setDisable] = useState(true);
    const [backendErrors, setBackendErrors] = useState(null);

    const list_default_image = `${process.env.PUBLIC_URL}/images/list_default_image.png`;
    var list_image_path = `${process.env.REACT_APP_BACKEND_URL}/lists/`;
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var user_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/`;

    useEffect(() => {

        const foundUserId = Number(id);

        if (!Number.isNaN(foundUserId)) {
            dispatch(actions.findAllCustomListsById(foundUserId));
        }

        return () => dispatch(actions.clearFoundCustomLists());

    }, [id, dispatch]);

    if (!Array.isArray(customLists)) {
        return null;
    }

    customLists.length > 0 && customLists.sort(function(a,b) {
        if (a.likeCount < b.likeCount) return 1;
        if (a.likeCount > b.likeCount) return -1;
        return 0;
    })

    return (

        <div className="center-element w-80 mt-4">

            <div className="col-12 p-0">
                <div className="row ml-2 mb-4">
                    <span className="title-pre-title-box"></span>
                    <h3 className="m-0 main-cast-title"><FormattedMessage id='project.users.info.allLists'/></h3>
                        <Link className="link-bold m-float-right label-v-class mr-2" to={"/users/" + id}>
                            <FormattedMessage id='project.users.info.returnToProfile'/>
                        </Link>
                </div>
            </div>

            {customLists.map(list =>

                <div key={list.id} className="row list-search-details mb-5 ml-1" >

                    <div className="col-2 p-0">
                        <img className="image-list-search-view border-radius-15-left link-pointer " onClick={() => history.push(`/lists/${list.id}`)}
                            alt="title poster image" src={list.hasImage ? list_image_path + list.id + "/image" : list_default_image} />
                    </div>

                    <div className="col title-search-details-content p-0">
                        <div className="row">

                            <div className="col-9 p-0 ml-1">

                                {!list.visible &&
                                    <span className="custom-list-private-type-tag font-weight-600 mr-2">
                                        <FormattedMessage id='project.users.info.private'/>
                                    </span>
                                }

                                <span>
                                    <span className="search-title-name link-pointer-text"
                                        onClick={() => history.push(`/lists/${list.id}`)}>
                                            {list.title + (list.titleCount > 0 ? "," : "")}
                                        </span>
                                    {list.titleCount > 0 && <span className="list-search-title-count">{" "}({list.titleCount})</span>}
                                </span>
                            </div>

                            <div className="col p-0 list-search-like-container">
                                <div className="row w-95 justify-content-end">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart-fill mt-1" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                    </svg>
                                    <span className="font-weight-600 ml-2"> {"(" + list.likeCount + ")"}</span>
                                </div>
                            </div>

                        </div>
                        <div className="row search-title-description ml-4 mt-2">
                            <span className="mr-5">{
                            !list.description ?
                                    <FormattedMessage id='project.search.findTitles.noDescription'/>
                            :
                                list.description
                            }</span>
                        </div>
                        
                    </div>
                </div>

            )}
        </div>
    );

}

export default AllLists;