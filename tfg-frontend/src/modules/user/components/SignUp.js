import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';

const SignUp = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [country, setCountry] = useState('');
    const [email, setEmail]  = useState('');
    const [backendErrors, setBackendErrors] = useState(null);
    const [passwordsDoNotMatch, setPasswordsDoNotMatch] = useState(false);
    let form;
    let confirmPasswordInput;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity() && checkConfirmPassword()) {
            
            dispatch(actions.signUp(
                {userName: userName.trim(),
                password: password,
                country: country.trim(),
                email: email.trim()},
                () => history.push('/'),
                errors => setBackendErrors(errors),
                () => {
                    history.push('/users/login');
                    dispatch(actions.logout());
                }
            ));

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }
    }

    const checkConfirmPassword = () => {

        if (password !== confirmPassword) {
            confirmPasswordInput.setCustomValidity('error');
            setPasswordsDoNotMatch(true);

            return false;
        } else {
            return true;
        }

    }

    const handleConfirmPasswordChange = value => {
        confirmPasswordInput.setCustomValidity('');
        setConfirmPassword(value);
        setPasswordsDoNotMatch(false);
    }

    return (
        <div className="center-element w-50 mt-4">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>
            <div className="center-form">
                <h5 className="center-form-header">
                    <FormattedMessage id="project.users.SignUp.title"/>
                </h5>
                <div className="center-element">
                
                    <form ref={node => form = node}
                        className="needs-validation" noValidate 
                        onSubmit={e => handleSubmit(e)}>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" 
                                    fill="currentColor" className="bi bi-person-fill" viewBox="0 0 16 16">
                                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.global.fields.userName">
                                {
                                    (msg) => <input type="text" id="userName" className="form-control"
                                    value={userName}
                                    onChange={e => setUserName(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" 
                                    className="bi bi-key-fill" viewBox="0 0 16 16">
                                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.global.fields.password">
                                {
                                    (msg) => <input type="password" id="password" className="form-control"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" 
                                    className="bi bi-key-fill" viewBox="0 0 16 16">
                                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.global.fields.confirmPassword">
                                {
                                    (msg) => <input ref={node => confirmPasswordInput = node}
                                    type="password" id="confirmPassword" className="form-control"
                                    value={confirmPassword}
                                    onChange={e => handleConfirmPasswordChange(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                {passwordsDoNotMatch ?
                                    <FormattedMessage id='project.global.validator.passwordsDoNotMatch'/> :
                                    <FormattedMessage id='project.global.validator.required'/>}
                            </div>
                        </div>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" 
                                    fill="currentColor" className="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                                        <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.global.fields.country">
                                {
                                    (msg) => <input type="text" id="country" className="form-control"
                                    value={country}
                                    onChange={e => setCountry(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" 
                                    fill="currentColor" className="bi bi-envelope-fill" viewBox="0 0 16 16">
                                        <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 
                                        4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 
                                        0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 
                                        11.801V4.697l-5.803 3.546z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.global.fields.email">
                                {
                                    (msg) => <input type="email" id="email" className="form-control"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                                <FormattedMessage id='project.global.validator.email'/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-md-2 center-element">
                                <button type="submit" className="btn btn-primary">
                                    <FormattedMessage id="project.users.SignUp.title"/>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default SignUp;