import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

const UpdateProfile = () => {
    
    const user = useSelector(selectors.getUser);
    const user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    const user_image = `${process.env.REACT_APP_BACKEND_URL}/users/${user.id}/image`;
    const dispatch = useDispatch();
    const history = useHistory();

    const [image, setImage]  = useState( !user.image ? user_default_image : user_image);
    const [image_changed, setImage_Changed]  = useState(false);
    const [userName, setUserName] = useState(user.userName);
    const [country, setCountry] = useState(user.country);
    const [email, setEmail]  = useState(user.email);
    const [backendErrors, setBackendErrors] = useState(null);

    let form;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity()) {

            var img = image === user_image || image === user_default_image ? null : image;
            
            dispatch(actions.updateProfile(
                {id: user.id,
                userName: userName.trim(),
                country: country.trim(),
                email: email.trim(),
                image: img,
                imageChanged: image_changed},
                () => history.push('/users/' + user.id),
                errors => setBackendErrors(errors)));

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const handleImageChange = img => {
        var fileReader= new FileReader();
    
        fileReader.addEventListener("load", function(e) {
            setImage(e.target.result);
            setImage_Changed(true);
        }); 
        
        fileReader.readAsDataURL(img);
    }

    return (
        <div className="center-element w-80 mt-4">

            <h5 className="center-form-header">
                <FormattedMessage id="project.users.UpdateProfile.title"/>
            </h5>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation row user-info" noValidate onSubmit={e => handleSubmit(e)}>
                <div className="col-3 user-img-column ">
                    <img id="img-user" src={image} className="img-thumbnail" alt="User img"/>
                    <input id="selectedFile" type="file" name="image" className="inputfile" 
                        onChange={e => handleImageChange(e.target.files[0])} 
                        accept="image/*"/>
                    <label htmlFor="selectedFile" className="label-image" tabIndex="-1">
                        <FormattedMessage id="project.users.profileEdit.selectImage"/>
                    </label>
                    <span className="label-image red-label" tabIndex="-2" onClick={e => {
                        setImage(user_default_image);
                        setImage_Changed(true);
                        }}>
                        <FormattedMessage id="project.users.profileEdit.deleteImage"/>
                    </span>
                </div>

                <div className="col-6">
                    <div className="row mb-3">
                        <label htmlFor="userName" className="col-form-label col-3 align-right-input-label-text">
                            <FormattedMessage id="project.global.fields.userName"/>
                        </label>
                        <div className="col-9 align-input-center">
                            <input type="userName" id="userName" className="form-control"
                                value={userName}
                                onChange={e => setUserName(e.target.value)}
                                required/>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>
                    </div>
                    <div className="row mb-3">
                        <label htmlFor="country" className="col-form-label col-3 align-right-input-label-text">
                            <FormattedMessage id="project.global.fields.country"/>
                        </label>
                        <div className="col-9 align-input-center">
                            <input type="country" id="country" className="form-control "
                                value={country}
                                onChange={e => setCountry(e.target.value)}
                                required/>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>
                    </div>
                    <div className="row mb-3">
                        <label htmlFor="email" className="col-form-label col-3 align-right-input-label-text">
                            <FormattedMessage id="project.global.fields.email"/>
                        </label>
                        <div className="col-9 align-input-center">
                            <input type="email" id="email" className="form-control"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required/>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.email'/>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="col ">
                    <div className="row">
                        <button type="submit" className="btn btn-success button-align-right button-width-50">
                            <FormattedMessage id="project.global.buttons.save"/>
                        </button>
                    </div>
                    <div className="row"> 
                        <button type="button" className="btn btn-secondary button-align-right button-width-50" 
                                    onClick={() => {history.goBack()} }>
                            <FormattedMessage id="project.global.buttons.cancel"/>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );

}

export default UpdateProfile;