/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams} from 'react-router-dom';
import {useHistory, useLocation, Link} from 'react-router-dom';

import {PageNotFound, Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import ProfileFriendsSummaryView from './ProfileFriendsSummaryView';

const LastCustomLists = () => {

    const user = useSelector(selectors.getUser);
    const foundUser = useSelector(selectors.getFoundUser);
    const customLists = useSelector(selectors.getCustomLists);
    const dispatch = useDispatch();
    const {id} = useParams();

    const history = useHistory();
    const location = useLocation().pathname;

    const [password, setPassword] = useState("");
    const [disable, setDisable] = useState(true);
    const [backendErrors, setBackendErrors] = useState(null);

    const list_default_image = `${process.env.PUBLIC_URL}/images/list_default_image.png`;
    var list_image_path = `${process.env.REACT_APP_BACKEND_URL}/lists/`;

    useEffect(() => {

        const foundUserId = Number(id);

        if (!Number.isNaN(foundUserId)) {
            dispatch(actions.findLastCustomListsById(foundUserId));
        }

        return () => dispatch(actions.clearFoundCustomLists());

    }, [id, dispatch]);

    if (!customLists) {
        return null;
    }

    return (

        <div className="row w-100 p-0 center-element">

            <div className="col-12 p-0">
                <div className="row ml-2 mb-4">
                    <span className="title-pre-title-box"></span>
                    <h3 className="m-0 main-cast-title"><FormattedMessage id='project.users.info.allRecentLists'/></h3>
                        <Link className="link-bold m-float-right label-v-class mr-2" to={location + "/lists"}>
                            <FormattedMessage id='project.users.info.seeAll'/>
                        </Link>
                </div>
            </div>

            {customLists.items.length > 0 && customLists.items.map(list => 
                <div className="col-20-100 user-custom-list-last-item">
                    <div className="row p-0 custom-list-shadow-container center-element">
                        <div>
                            {!list.visible &&
                                <span className="private-lock-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-shield-lock-fill label-v-class" viewBox="0 0 16 16">
                                        <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
                                    </svg>
                                </span>
                            }

                            <img className="link-pointer w-100" 
                                alt="title poster image" src={list.hasImage ? list_image_path + list.id + "/image" : list_default_image}
                                onClick={() => history.push("/lists/" + list.id)} />
                        </div>
                    </div>
                    <p className="link-pointer-text user-custom-list-last-title cut-text-dots mt-1 mb-0"
                        onClick={() => history.push("/lists/" + list.id)}>
                        {list.title}
                    </p>
                    <div className="col pr-2">
                        <div className="w-100 list-search-like-container label-v-class justify-content-end">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart-fill mt-1" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                            </svg>
                            <span className="font-weight-600 ml-2"> {"(" + list.likeCount + ")"}</span>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );

}

export default LastCustomLists;