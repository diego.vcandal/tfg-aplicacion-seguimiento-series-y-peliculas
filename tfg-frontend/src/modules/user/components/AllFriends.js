import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useParams} from 'react-router-dom';
import * as selectors from '../selectors';
import * as actions from '../actions';
import Friends from './Entity/Friends';


const AllFriends = () => {

    const dispatch = useDispatch();
    const {id} = useParams();
    const friends = useSelector(selectors.getFriends);

    useEffect(() => {

        const foundUserId = Number(id);

        if (!Number.isNaN(foundUserId)) {
            dispatch(actions.findAllFriends(foundUserId));
        }

        return () => dispatch(actions.clearFindFriends());

    }, [id, dispatch]);

    if (!friends) {
        return null;
    }

    return (

        <div className="mt-4">
            <Friends friends={friends.items} />
        </div>

    );

}

export default AllFriends;