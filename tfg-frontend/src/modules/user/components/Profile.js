/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams} from 'react-router-dom';
import {useHistory, useLocation} from 'react-router-dom';

import {PageNotFound, Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import ProfileFriendsSummaryView from './ProfileFriendsSummaryView';
import LastCustomLists from './LastCustomLists';

const Profile = () => {

    const user = useSelector(selectors.getUser);
    const foundUser = useSelector(selectors.getFoundUser);
    const dispatch = useDispatch();
    const {id} = useParams();

    const history = useHistory();
    const location = useLocation().pathname;

    const [password, setPassword] = useState("");
    const [disable, setDisable] = useState(true);
    const [backendErrors, setBackendErrors] = useState(null);

    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;

    useEffect(() => {

        const foundUserId = Number(id);

        if (!Number.isNaN(foundUserId)) {
            dispatch(actions.findUserById(foundUserId));
        }

        return () => dispatch(actions.clearFoundUser());

    }, [id, dispatch]);

    if (!foundUser) {
        return <PageNotFound />;
    }

    if(foundUser.image){
        user_default_image = `${process.env.REACT_APP_BACKEND_URL}/users/${foundUser.id}/image`;
    }

    const handleDelete = () => {

        dispatch(actions.deleteUser(user.id, password,
            () => history.push('/'),
            errors => {setBackendErrors(errors);
                        setPassword('')
            }));
            
    }

    const handleConfirmPassword = value => {
        setPassword(value);

        if(value.length === 0)
            setDisable(true);
        else
            setDisable(false);
    
    }

    let relationshipInfo = foundUser.relationshipInfoDTO;

    return (

        <div className="user-info center-element w-90 mt-4">
            <div className="row-12">

                <div className="row-12">
                    <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>
                </div>
                
                <div className="row">
                    <div className="col-3 user-img-column">
                        <img id="img-user" alt="user profile image" src={user_default_image} className="img-thumbnail"/>
                        
                        <button className="btn btn-primary w-100 mt-3" onClick={() => history.push(location + "/following-list")}>
                            <FormattedMessage id="project.users.buttons.followingList" />
                        </button>

                        <div className="row">
                            <div className="col mt-4">
                                <ProfileFriendsSummaryView />
                            </div>
                        </div>
                    </div>
                    
                    <div className="col">
                        <div className="row">
                            <div className="col ml-3">
                                <div className="row">
                                    <h4 className="user-username-title">{foundUser.userName}</h4>
                                    <p className="ml-2 mt-1 light-gray-text">{foundUser.country}</p>
                                </div>
                                <div className="row">
                                    <p className="font-weight-600 light-gray-text">{foundUser.email}</p>
                                </div>

                            </div>

                            {user && user.id === foundUser.id &&
                                <div className="col">
                                    <div className="row">
                                        <button  className="btn btn-success button-align-right button-width-50" 
                                            onClick={() => history.push("/lists/create")}>
                                            <FormattedMessage id="project.users.lists.create"/>
                                        </button >
                                    </div>
                                    <div className="row">
                                        <button className="btn btn-primary button-align-right button-width-50"
                                            onClick={() => history.push('/users/update-profile')}>
                                            <FormattedMessage id="project.global.buttons.updateProfile"/>
                                        </button>
                                    </div>
                                    <div className="row">
                                        <button className="btn btn-primary button-align-right button-width-50" 
                                            onClick={() => history.push('/users/change-password')}>
                                            <FormattedMessage id="project.global.buttons.uptadePassword"/>
                                        </button>
                                    </div>
                                    <div className="row">
                                        <button  className="btn btn-danger button-align-right button-width-50" 
                                            data-toggle="modal" data-target={"#delete-confirmation"}>
                                            <FormattedMessage id="project.global.buttons.deleteProfile"/>
                                        </button >
                                    </div>
                                </div>
                            }

                            {user && user.id !== foundUser.id && !relationshipInfo.areFriends ?

                                    !relationshipInfo.sentPetition ?

                                        relationshipInfo.receivedPetition ?

                                            <div className="col">
                                                <div className="row">
                                                    <button className="btn btn-success button-align-right button-width-50 mr-3"
                                                        onClick={() => {dispatch(actions.acceptRequest({originUser: foundUser.id, destinationUser: user.id}, user,
                                                            errors => setBackendErrors(errors)))}}>
                                                        <FormattedMessage id="project.global.buttons.acceptRequest" />
                                                    </button >
                                                </div>
                                            </div>
                                        :

                                        <div className="col">
                                            <div className="row">
                                                <button className="btn btn-primary button-align-right button-width-50 mr-3"
                                                    onClick={() => {dispatch(actions.sendRequest(user.id, foundUser.id,
                                                        errors => setBackendErrors(errors)))}}>
                                                    <FormattedMessage id="project.global.buttons.sendRequest"/>
                                                </button>
                                            </div>
                                        </div>
                                    :

                                    <div className="col">
                                        <div className="row">
                                            <button className="btn btn-secondary button-align-right button-width-50 mr-3" disabled>
                                                <FormattedMessage id="project.global.buttons.requestSent" />
                                            </button >
                                        </div>
                                    </div>
                                :
                                    null
                                
                            }
                        </div>
                        
                        <br/><br/><br/>
                        <div className="row">
                            <LastCustomLists />
                        </div>
                    </div>

                    <div id="delete-confirmation" className="modal fade" tabIndex="-1" role="dialog">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title"><FormattedMessage id="project.global.dialog.deleteUser.Title"/></h5>
                                </div>
                                <div className="modal-body">
                                    <p><FormattedMessage id="project.global.dialog.deleteUser.Body"/></p>
                                </div>
                                
                                <div className="form-group input-group w-75 mb-3 ml-auto mr-auto">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" 
                                            className="bi bi-key-fill" viewBox="0 0 16 16">
                                                <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                            </svg>
                                        </span>
                                    </div>
                                    <FormattedMessage id="project.global.fields.password">
                                        {
                                            (msg) => <input type="password" id="password" className="form-control"
                                            value={password}
                                            onChange={e => handleConfirmPassword(e.target.value)}
                                            autoFocus
                                            placeholder={msg}
                                            required />
                                        }
                                    </FormattedMessage>
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" 
                                        data-dismiss="modal"
                                        onClick={() => {setPassword(''); setDisable(true)}}>
                                        <FormattedMessage id="project.global.buttons.cancel"/>
                                    </button>
                                    <button type="button" className="btn btn-danger" 
                                        data-dismiss="modal"
                                        onClick={() => handleDelete()}
                                        disabled={disable}>
                                        <FormattedMessage id="project.global.buttons.ok"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default Profile;