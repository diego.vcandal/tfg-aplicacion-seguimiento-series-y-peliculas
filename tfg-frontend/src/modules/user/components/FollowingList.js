/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage, FormattedDate, FormattedTime} from 'react-intl';
import {useParams} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

import {PageNotFound} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

import AdminOptionsModal from './Modals/AdminOptionsModal';
import FollowingListItems from './Entity/FollowingListItems';

const FollowingList = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const user = useSelector(selectors.getUser);
    const isLoggedIn = useSelector(selectors.isLoggedIn);
    const foundFollowingList = useSelector(selectors.getFollowingList);

    const {id} = useParams();
    
    const makePublicModal = "make-public-modal";
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;

    useEffect(() => {

        const foundListId = Number(id);

        if (!Number.isNaN(foundListId)) {
            if (isLoggedIn && user.id === foundListId)
                dispatch(actions.findMyFollowingListById());
            else
                dispatch(actions.findFollowingListById(foundListId));
        }

        return () => dispatch(actions.clearFoundFollowingList());

    }, [id, user, isLoggedIn, dispatch]);

    if (!foundFollowingList) {
        return <PageNotFound />;
    }

    const orderByName = (a,b) => {
        if ((a.title ? a.title : a.originalTitle) < (b.title ? b.title : b.originalTitle)) return -1;
        if ((a.title ? a.title : a.originalTitle) > (b.title ? b.title : b.originalTitle)) return 1;
        return 0;
    }

    const orderByRating = (a,b) => {
        if (a.rating === b.rating) return orderByName(a,b);
        if (a.rating <= 0 ) return 1;
        if (b.rating <= 0 ) return -1;
        if (a.rating < b.rating) return 1;
        if (a.rating > b.rating) return -1;
        return 0;
    }

    const changeOrderType = (id, list, type) => {

        if (Number(id) === 1){
            list.sort((a,b) => orderByRating(a,b));
        } else {
            list.sort((a,b) => orderByName(a,b));
        }    

        switch (type) {

            case 4:
                dispatch(actions.reorderPlanToWatchFollowingListTitlesCompleted(list));
                break;

            case 3:
                dispatch(actions.reorderDroppedFollowingListTitlesCompleted(list));
                break;

            case 2:
                dispatch(actions.reorderCompletedFollowingListTitlesCompleted(list));
                break;

            case 1:
                dispatch(actions.reorderWatchingFollowingListTitlesCompleted(list));
                break;

            case 0:
                dispatch(actions.reorderAllFollowingListTitlesCompleted(list));
                break;

            default:
                break;

        }

    }

    const createOrderElementsController = (list, type) => {
        return (
            <div className="form-group custom-select-order-by-container">
                <label for="orderBy" className="custom-select-order-by-label"><FormattedMessage id="project.users.followingList.order.label"/></label>
                <select className="ml-3 custom-select custom-select-order-by" id="orderBy"
                    onChange={e => changeOrderType(e.target.value, list, type)}>
                        {type === 0 && <option key={-1} value={-1} disabled selected hidden>---</option>}
                    <FormattedMessage id="project.users.followingList.order.name">
                        {
                            (msg) => <option key={0} value={0}>{msg}</option>
                        }
                    </FormattedMessage>
                    <FormattedMessage id="project.users.followingList.order.rating">
                        {
                            (msg) => <option key={1} value={1}>{msg}</option>
                        }
                    </FormattedMessage>
                </select>
            </div>
        );
    };

    return (
        <div className="center-element w-90">
            <div className="row sticky-top following-list-header-sticky ">

                <div className="col mt-3">
                    <div className="row w-95 center-element">
                        <img className="image-square-medium border-radius-20" alt="user profile image" 
                            src={`${process.env.REACT_APP_BACKEND_URL}/users/${foundFollowingList.userId}/image`}
                            onError={e => {e.target.src = user_default_image}}/>
                        
                        <div className="ml-4 col">
                            <span className="person-username-title d-flex">

                                {!foundFollowingList.visible &&
                                    <span className="list-private-type-tag font-weight-600 mr-2 mb-1">
                                        <FormattedMessage id='project.users.info.private'/>
                                    </span>
                                }

                                <FormattedMessage id="project.users.buttons.followingList"/> 
                                {" - "}
                                <span className="link-pointer-text ml-2"
                                    onClick={() => history.push("/users/" + foundFollowingList.userId)}>
                                        {foundFollowingList.userName}
                                </span>
                            </span>
                            <span className="ml-1 row">
                                <FormattedMessage id="project.users.info.lastUpdate"/>
                                {": "}
                                <FormattedTime value={new Date(foundFollowingList.lastUpdated)}/>
                                {" - "}
                                <FormattedDate value={new Date(foundFollowingList.lastUpdated)}/>
                            </span>
                        </div>

                        <div className="col-1 p-0 text-right">
                            {isLoggedIn && user.id === foundFollowingList.userId &&
                                <button className={"btn btn-sm ml-2" + (foundFollowingList.visible ? " btn-success" : " btn-secondary")} type="button" title="MakePublic"
                                    data-toggle="modal" data-target={"#" + makePublicModal}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-eye-slash-fill btn-icon" viewBox="0 0 16 16">
                                        <path d="m10.79 12.912-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                        <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829zm3.171 6-12-12 .708-.708 12 12-.708.708z"/>
                                    </svg>
                                </button>
                            }
                        </div>
    
                    </div>

                    <ul className="row nav nav-tabs mt-2 following-list-header-elements" id="myTab" role="tablist" data-offset-top="50">
                        <li className="nav-item" role="presentation">
                            <a className="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true"
                                onClick={() => window.scrollTo(0, 0)}><FormattedMessage id="project.users.followingList.all"/></a>
                        </li>
                        <li className="nav-item" role="presentation">
                            <a className="nav-link" id="watching-tab" data-toggle="tab" href="#watching" role="tab" aria-controls="watching" aria-selected="false"
                                onClick={() => window.scrollTo(0, 0)}><FormattedMessage id="project.users.followingList.watching"/></a>
                        </li>
                        <li className="nav-item" role="presentation">
                            <a className="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false"
                                onClick={() => window.scrollTo(0, 0)}><FormattedMessage id="project.users.followingList.completed"/></a>
                        </li>
                        <li className="nav-item" role="presentation">
                            <a className="nav-link" id="dropped-tab" data-toggle="tab" href="#dropped" role="tab" aria-controls="dropped" aria-selected="false"
                                onClick={() => window.scrollTo(0, 0)}><FormattedMessage id="project.users.followingList.dropped"/></a>
                        </li>
                        <li className="nav-item" role="presentation">
                            <a className="nav-link" id="planToWatch-tab" data-toggle="tab" href="#planToWatch" role="tab" aria-controls="planToWatch" aria-selected="false"
                                onClick={() => window.scrollTo(0, 0)}><FormattedMessage id="project.users.followingList.planToWatch"/></a>
                        </li>
                    </ul>

                </div>
            </div>

            <div className="tab-content z-index-1 mt-2" id="myTabContent">
                <div className="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                    {createOrderElementsController(foundFollowingList.all, 0)}
                    <FollowingListItems titles={foundFollowingList.all} type={0} />
                </div>
                <div className="tab-pane fade" id="watching" role="tabpanel" aria-labelledby="watching-tab">
                    {createOrderElementsController(foundFollowingList.watching, 1)}
                    <FollowingListItems titles={foundFollowingList.watching} type={1} />
                </div>
                <div className="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
                    {createOrderElementsController(foundFollowingList.completed, 2)}
                    <FollowingListItems titles={foundFollowingList.completed} type={2} />
                </div>
                <div className="tab-pane fade" id="dropped" role="tabpanel" aria-labelledby="dropped-tab">
                    {createOrderElementsController(foundFollowingList.dropped, 3)}
                    <FollowingListItems titles={foundFollowingList.dropped} type={3} />
                </div>
                <div className="tab-pane fade" id="planToWatch" role="tabpanel" aria-labelledby="planToWatch-tab">
                    {createOrderElementsController(foundFollowingList.planToWatch, 4)}
                    <FollowingListItems titles={foundFollowingList.planToWatch} type={4} />
                </div>
            </div>

            <AdminOptionsModal id={makePublicModal} visible={foundFollowingList.visible} onConfirm={() => dispatch(actions.changeListVisibility())} />

        </div>
    );

}

export default FollowingList;