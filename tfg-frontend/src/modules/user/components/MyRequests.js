import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {PageNotFound} from '../../common';
import * as selectors from '../selectors';
import * as actions from '../actions';
import Requests from './Requests';


const MyRequests = () => {

    const dispatch = useDispatch();
    const user = useSelector(selectors.getUser);
    const requests = useSelector(selectors.getReceivedRequests);

    useEffect(() => {

        dispatch(actions.findRequests(user.id));

    }, [user, dispatch]);

    if (!requests) {
        return <PageNotFound />;
    }

    return (

        <div className="center-element w-80 mt-4">
            <Requests requests={requests} />
        </div>

    );

}

export default MyRequests;