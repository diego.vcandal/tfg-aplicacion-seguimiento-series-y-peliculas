/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {FormattedDate, FormattedMessage} from 'react-intl';
import { useHistory } from 'react-router-dom';

import * as actions from '../../actions';
import * as selectors from '../../selectors';

import {FollowingStatus, getStatusNameCode} from '../../utils/FollowingStatus';

const FollowingListItems = ({titles, type}) => {

    const history = useHistory();
    const dispatch = useDispatch();

    const isLoggedIn = useSelector(selectors.isLoggedIn);
    const user = useSelector(selectors.getUser);
    const foundFollowingList = useSelector(selectors.getFollowingList);
    
    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;
    var external_api_image_path = "https://image.tmdb.org/t/p/w500/";

    const [, setBackendErrors] = useState(null);
    const [titleName, setTitleName]  = useState('');
    const [selectedTitle, setSelectedTitle]  = useState(null);
    const [status, setStatus]  = useState(-1);
    const [rating, setRating]  = useState(-1);
    const [hasReview, setHasReview]  = useState(false);

    let form, modal_open, modal_close, delete_modal_open;

    const getImage = (title) => {
        
        switch (Number(title.sourceType)){
            case 0:
                return title_image_path + title.id + "/images/portrait";

            case 1:
            case 2:
                return external_api_image_path + title.externalImagePath;

            default:
                return title_default_image;
        }

    };

    const getStatusColor = (id) => {

        switch(id) {
            case 3:
                return "dropped-mark";
            case 2:
                return "completed-mark";
            case 1:
                return "watching-mark";
            case 0:
            default:
                return "plan-to-watch-mark";
        }

    };

    const updateTitleData = (title) => {
        setStatus(title.status);
        setRating((title.rating < 1 || title.rating > 10) ? '' : title.rating);
        setSelectedTitle(title);
        setTitleName(title.title ? title.title : title.originalTitle);
        setHasReview(title.hasReview);
    }

    const handleOpen = (title) => {
        setBackendErrors(null);
        form.classList = "";

        updateTitleData(title);
        modal_open.click();
    }

    const handleDeleteOpen = (title) => {
        updateTitleData(title);
        delete_modal_open.click();
    }

    const handleClose = () => {
        setBackendErrors(null);
        form.classList = "";
    }

    const handleSubmit = (event) => {

        event.preventDefault();
        
        if (form.checkValidity()) {

            dispatch(actions.editEntryFromList(selectedTitle.titleId, {status: Number(status), rating: (rating === '' ? null : rating)},
                {...selectedTitle, status: Number(status), rating: (rating === '' ? null: rating)},
                selectedTitle.status), 
                errors => setBackendErrors(errors));

            modal_close.click();
            
        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    const handleDoDelete = () => {
        dispatch(actions.deleteEntryFromList(selectedTitle.titleId, Number(status), errors => setBackendErrors(errors)));
    }

    if (!titles) {
        return null;
    }

    let index = 1;
    return(
        <div className="row p-0">

            {titles.map(title => 
                <div className="col-6 mb-4 pl-3 pr-3" >
                    <div className="row following-list-shadow-container center-element">
                        <div >
                            <img className="following-list-title-image border-radius-15-left" 
                                alt="title poster image" src={getImage(title)} 
                                onError={e => {e.target.src = title_default_image}}/>
                        </div>
                        <div className="col">
                            <div className="row">
                                <span className="col following-list-title-name ml-2 mt-1 p-0 link-pointer-text"
                                    onClick={() => history.push(title.titleType === 0 ? ('/movies/' + title.titleId) : ('/tvshows/' + title.titleId))}>
                                    {title.title ? title.title : title.originalTitle}
                                </span>
                                <span className="following-list-title-number p-0 mr-2 ml-1 text-right">#{index++}</span>
                            </div>

                            <div className="row">
                                <span className="following-list-title-divider center-element"></span>
                            </div>

                            <div className="row ml-1 mt-1">
                                <div className="col following-list-title-body">
                                    
                                    <div className="row">
                                        <span className="col p-0">
                                            <FormattedMessage id='project.users.followingList.added'/>
                                            {" "}
                                            <FormattedDate value={new Date(title.dateAdded)} />
                                        </span>
                                        <span className="text-right following-list-status-row mr-4">
                                            <FormattedMessage id={getStatusNameCode(title.status)}/>
                                        </span>
                                    </div>

                                </div>
                            </div>

                            <div className="row ml-1 mt-1 following-list-rating-row mb-1 w-95">
                                {title.rating > 0 &&
                                    <div className="col p-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#eaba2b" className="bi bi-star middle-v-button-icon" viewBox="0 0 16 16">
                                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                        </svg>
                                        <span className="ml-2 middle-v-button-icon">{title.rating}</span>
                                    </div>
                                }
                                {isLoggedIn && user.id === foundFollowingList.userId &&
                                    <div className="mr-2 m-float-right mb-1">
                                        <button className="btn btn-success btn-sm" type="button" title="Edit" 
                                            onClick={() => handleOpen(title)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                            </svg>
                                        </button>

                                        <button className="btn btn-danger btn-sm ml-2" type="button" title="Edit" 
                                            onClick={() => handleDeleteOpen(title)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20"  fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                </svg>
                                        </button>

                                    </div>
                                }
                            </div>

                        </div>
                        <span className={"border-radius-15-right " + getStatusColor(title.status)}></span>
                    </div>
                </div>

            )}

            <button
                ref={node => modal_open = node} 
                type="button" title="Create"
                data-toggle="modal" data-target={"#add-panel" + type}
                hidden>
            </button>

            <div id={"add-panel" + type } className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                <FormattedMessage id="project.titles.followingList.edit.modal.title"/>
                            </h5>
                        </div>
                        
                        <div className="modal-body">
                            <form ref={node => form = node}>

                                <div className="row w-90 center-element">
                                    <label htmlFor="status" className="col-5 title-label-color label-v-class">
                                        <FormattedMessage id="project.titles.followingList.add.modal.title"/>
                                    </label>
                                    <input type="text" id="originalTitle" className="col-7 center-element form-control "
                                        value={titleName}
                                        disabled />
                                </div>

                                <div className="row w-90 center-element mt-3">
                                    <label htmlFor="status" className="col-5 title-label-color label-v-class">
                                        <FormattedMessage id="project.titles.followingList.add.modal.status"/>
                                    </label>
                                    <select value={status} className="col-6 center-element custom-select "
                                        onChange={e => setStatus(e.target.value)} >
                                        {FollowingStatus.map(type => 
                                            <FormattedMessage id={type.name}>
                                                {
                                                    (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                                }
                                            </FormattedMessage>
                                        )}
                                    </select>
                                </div>

                                <div className="row w-90 center-element mt-3">
                                    <label htmlFor="rating" className="col-5 title-label-color label-v-class">
                                        <FormattedMessage id="project.titles.followingList.add.modal.rating"/>
                                    </label>
                                    <input type="number" id="rating" className="col-6 center-element form-control"
                                        value={rating}
                                        onChange={e => setRating(e.target.value)}
                                        min="1" max="10" required={hasReview} />
                                </div>
                                
                                {hasReview && 
                                    <div className="w-90 center-element mt-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-circle mb-1 mr-2" viewBox="0 0 16 16">
                                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                            <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                                        </svg>
                                        <span className="light-gray-text"><FormattedMessage id="project.titles.followingList.add.modal.reviewWarning"/></span>
                                    </div>
                                }

                            </form>
                        </div>
                                        
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal"
                                onClick={() => handleClose()}>
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="submit" className="btn btn-success"
                                onClick={(e) => handleSubmit(e)}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                            <button type="button" hidden
                                ref={node => modal_close = node} 
                                data-dismiss="modal">
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>

            <button
                ref={node => delete_modal_open = node} 
                type="button" title="Delete"
                data-toggle="modal" data-target={"#delete-panel" + type}
                hidden>
            </button>

            <div id={"delete-panel" + type } className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                <FormattedMessage id="project.titles.followingList.delete.modal.title"/>
                            </h5>
                        </div>
                        <div className="modal-body text-center">
                            <p>
                                <FormattedMessage id="project.titles.followingList.delete.modal.body"/>
                            </p>

                            {rating > 0 && 
                                <div className="mt-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-circle mb-1 mr-2" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                                    </svg>
                                    <span className="light-gray-text"><FormattedMessage id="project.titles.followingList.delete.modal.reviewWarning"/></span>
                                </div>
                            }

                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal">
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="button" className="btn btn-danger" 
                                data-dismiss="modal"
                                onClick={() => handleDoDelete()}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );

};

FollowingListItems.propTypes = {
    titles: PropTypes.array.isRequired,
    type: PropTypes.number.isRequired
};

export default FollowingListItems;