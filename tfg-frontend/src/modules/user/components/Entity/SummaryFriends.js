/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {useHistory, useParams} from 'react-router-dom';

import {PageNotFound} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';

const SummaryFriends = ({friends}) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(selectors.getUser);
    const {id} = useParams();
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/`;
    const [, setBackendErrors] = useState(null);

    if(Number.isNaN(Number(id))){
        return <PageNotFound/>;
    }

    if (!friends) {
        return null;
    }

    return(
        <div className="mt-2">

            {friends.map(friend =>

                <div className="row mb-2 friend-summary-name-container" >

                    <div className="col-3">
                        <img className="image-x-small link-pointer" src={friend.image ? title_image_path + friend.id + "/image" : user_default_image} 
                            alt="user profile image" onClick={() => history.push('/users/' + friend.id)}/>
                    </div>

                    <div className="col ml-2">
                        <div className="row">
                            <p className="friend-summary-name link-pointer" 
                                onClick={() => history.push('/users/' + friend.id)}>{friend.userName}</p>
                        </div>
                    </div>
                    
                    {user && user.id === Number(id) &&
                        <div className="friend-delete-button">
                            <button className="btn btn-danger btn-sm" type="button" title="Delete"
                                onClick={() => {dispatch(actions.deleteFriend(id, friend.id,
                                                errors => setBackendErrors(errors)))}}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x" viewBox="0 0 16 16">
                                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </button>
                        </div>
                    }

                </div>

            )}

        </div>
    );

};

SummaryFriends.propTypes = {
    friends: PropTypes.array.isRequired
};

export default SummaryFriends;