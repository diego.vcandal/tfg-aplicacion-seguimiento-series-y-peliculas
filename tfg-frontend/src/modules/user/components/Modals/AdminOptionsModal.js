import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';

const AdminOptionsModal = ({id, visible, onConfirm}) => (

    <div id={id} className="modal fade" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">
                        {visible ? 
                            <FormattedMessage id="project.global.dialog.adminOptions.list.makePrivate.Title"/>
                        :
                            <FormattedMessage id="project.global.dialog.adminOptions.list.makePublic.Title"/>
                        }
                        
                    </h5>
                </div>
                <div className="modal-body">
                    <p>
                        {visible ? 
                            <FormattedMessage id="project.global.dialog.adminOptions.list.makePrivate.Body"/>
                        :
                            <FormattedMessage id="project.global.dialog.adminOptions.list.makePublic.Body"/>
                        }
                    </p>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" 
                        data-dismiss="modal">
                        <FormattedMessage id="project.global.buttons.cancel"/>
                    </button>
                    <button type="button" className="btn btn-danger" 
                        data-dismiss="modal"
                        onClick={onConfirm}>
                        <FormattedMessage id="project.global.buttons.ok"/>
                    </button>
                </div>
            </div>
        </div>
    </div>

);

AdminOptionsModal.propTypes = {
    id: PropTypes.string.isRequired,
    visible: PropTypes.bool.isRequired,
    onConfirm: PropTypes.func.isRequired,
}

export default AdminOptionsModal;
