import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {Link} from 'react-router-dom';

import SummaryFriends from './Entity/SummaryFriends';
import * as actions from '../actions';
import * as selectors from '../selectors';

const ProfileFriendsSummaryView = () => {

    const dispatch = useDispatch();
    const foundUser = useSelector(selectors.getFoundUser);
    let id = foundUser.id;
    const friends = useSelector(selectors.getFriends);

    useEffect(() => {

        const foundUserId = Number(id);

        if (!Number.isNaN(foundUserId)) {
            dispatch(actions.findSummaryFriends(foundUserId));
        }

        return () => dispatch(actions.clearFindFriends());

    }, [id, dispatch]);

    if (!friends) {
        return null;
    }

    return(

        <div className="friend-list-summary">
            <div className="row friend-list-summary-header">
                <div className="col">
                    <span className="friends-number-summary"><FormattedMessage id="project.users.friends.list.title"/> ({friends.totalElements})</span>
                </div>
                <div className="col text-right">
                    <Link to={`/users/` + foundUser.id + `/friends`}><FormattedMessage id="project.global.buttons.seeMore"/></Link>
                </div>
            </div>
            <hr className="nopadding hr-friends"></hr>

            <SummaryFriends friends={friends.items} />

        </div>
    );

};

export default ProfileFriendsSummaryView;