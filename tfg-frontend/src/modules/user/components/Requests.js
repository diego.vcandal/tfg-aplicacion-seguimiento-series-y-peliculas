/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import PropTypes from 'prop-types';

import {Errors} from '../../common';
import * as actions from '../actions';

const Requests = ({requests}) => {

    const dispatch = useDispatch();
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/`;
    const [backendErrors, setBackendErrors] = useState(null);

    return(
        <div className="center-element w-80">

            <h4 className="center-form-header">
                <FormattedMessage id="project.users.FriendRequest.title"/>
            </h4>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            {requests.map(request =>

                <div className="row user-search-details-request mb-3">
                    <div className="col-1 image-user-details-container">
                        <img className="image-x-small" alt="user profile image" 
                            src={request.originUserImage ? title_image_path + request.originUser + "/image" : user_default_image} />
                    </div>
                    <div className="col ml-2">
                        <div className="row">
                            <p className="search-name-request">{request.originUserName}</p>
                        </div>
                    </div>
                    
                    <div className="request-icons">
                        <button className="btn btn-success btn-sm mr-3" type="button" title="Accept"
                            onClick={() => {dispatch(actions.acceptRequest(request,
                                errors => setBackendErrors(errors)))}}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check2" viewBox="0 0 16 16">
                                <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
                            </svg>
                        </button>
                        <button className="btn btn-danger btn-sm" type="button" title="Delete"
                            onClick={() => {dispatch(actions.rejectRequest(request,
                                errors => setBackendErrors(errors)))}}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x" viewBox="0 0 16 16">
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </button>
                    </div>
                </div>

            )}

        </div>
    );

};

Requests.propTypes = {
    requests: PropTypes.array.isRequired
};

export default Requests;