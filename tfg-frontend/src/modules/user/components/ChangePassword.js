import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

const ChangePassword = () => {

    const user = useSelector(selectors.getUser);
    const dispatch = useDispatch();
    const history = useHistory();
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [backendErrors, setBackendErrors] = useState(null);
    const [passwordsDoNotMatch, setPasswordsDoNotMatch] = useState(false);
    let form;
    let confirmNewPasswordInput;

    const handleSubmit = event => {

        event.preventDefault();

        if (form.checkValidity() && checkConfirmNewPassword()) {

            dispatch(actions.changePassword(user.id, oldPassword, newPassword,
                () => history.push('/'),
                errors => setBackendErrors(errors)));

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');
            
        }

    }

    const checkConfirmNewPassword = () => {

        if (newPassword !== confirmNewPassword) {

            confirmNewPasswordInput.setCustomValidity('error');
            setPasswordsDoNotMatch(true);

            return false;

        } else {
            return true;
        }

    }

    const handleConfirmNewPasswordChange = event => {

        confirmNewPasswordInput.setCustomValidity('');
        setConfirmNewPassword(event.target.value);
        setPasswordsDoNotMatch(false);

    }

    return (
        <div className="center-element w-50 mt-4">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="center-form">

                <h5 className="center-form-header">
                    <FormattedMessage id="project.users.ChangePassword.title"/>
                </h5>

                <div className="center-element">

                    <form ref={node => form = node} 
                        className="needs-validation" noValidate 
                        onSubmit={e => handleSubmit(e)}>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" 
                                    className="bi bi-key-fill" viewBox="0 0 16 16">
                                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.users.ChangePassword.fields.oldPassword">
                                {
                                    (msg) => <input type="password" id="oldPassword" className="form-control"
                                    value={oldPassword}
                                    onChange={e => setOldPassword(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" 
                                    className="bi bi-key-fill" viewBox="0 0 16 16">
                                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.users.ChangePassword.fields.newPassword">
                                {
                                    (msg) => <input type="password" id="password" className="form-control"
                                    value={newPassword}
                                    onChange={e => setNewPassword(e.target.value)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                <FormattedMessage id='project.global.validator.required'/>
                            </div>
                        </div>

                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" 
                                    className="bi bi-key-fill" viewBox="0 0 16 16">
                                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                    </svg>
                                </span>
                            </div>
                            <FormattedMessage id="project.users.ChangePassword.fields.confirmNewPassword">
                                {
                                    (msg) => <input ref={node => confirmNewPasswordInput = node}
                                    type="password" id="confirmNewPassword" className="form-control"
                                    value={confirmNewPassword}
                                    onChange={e => handleConfirmNewPasswordChange(e)}
                                    autoFocus
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>
                            <div className="invalid-feedback">
                                {passwordsDoNotMatch ?
                                    <FormattedMessage id='project.global.validator.passwordsDoNotMatch'/> :
                                    <FormattedMessage id='project.global.validator.required'/>}
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="center-element">
                                <button type="submit" className="btn btn-success">
                                    <FormattedMessage id="project.global.buttons.save"/>
                                </button>
                                <button type="button" className="btn btn-secondary ml-4" 
                                            onClick={() => {history.goBack()} }>
                                    <FormattedMessage id="project.global.buttons.cancel"/>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default ChangePassword;
