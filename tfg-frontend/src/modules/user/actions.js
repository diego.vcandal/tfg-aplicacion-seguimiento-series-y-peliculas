import * as actionTypes from './actionTypes';
import backend from '../../backend';

const signUpCompleted = authenticatedUser => ({
    type: actionTypes.SIGNUP_COMPLETED,
    authenticatedUser
});

export const signUp = (user, onSuccess, onErrors, reauthenticationCallback) => dispatch =>
    backend.userService.signUp(user,
        authenticatedUser => {
            dispatch(signUpCompleted(authenticatedUser));
            onSuccess();
        },
        onErrors,
        reauthenticationCallback);

const loginCompleted = authenticatedUser => ({
    type: actionTypes.LOGIN_COMPLETED,
    authenticatedUser
});

export const login = (userName, password, onSuccess, onErrors, reauthenticationCallback) => dispatch =>
    backend.userService.login(userName, password,
        authenticatedUser => {
            dispatch(loginCompleted(authenticatedUser));
            onSuccess();
        },
        onErrors,
        reauthenticationCallback
    );

export const tryLoginFromServiceToken = reauthenticationCallback => dispatch =>
    backend.userService.tryLoginFromServiceToken(
        authenticatedUser => {
            if (authenticatedUser) {
                dispatch(loginCompleted(authenticatedUser));
            }
        },
        reauthenticationCallback
    );

export const logout = () => {

    backend.userService.logout();

    return {type: actionTypes.LOGOUT};

};

const findUserByIdCompleted = foundUser => ({
    type: actionTypes.GET_USER_INFO_COMPLETED,
    foundUser
});

export const findUserById = (id, onErrors) => dispatch => {

    backend.userService.findUserById(id,
        foundUser => dispatch(findUserByIdCompleted(foundUser)),
        onErrors);

}

export const clearFoundUser = () => ({
    type: actionTypes.CLEAR_FOUND_USER
});

export const updateProfileCompleted = user => ({
    type: actionTypes.UPDATE_PROFILE_COMPLETED,
    user
})

export const updateProfile = (user, onSuccess, onErrors) => dispatch =>
    backend.userService.updateProfile(user, 
        user => {
            dispatch(updateProfileCompleted(user));
            onSuccess();
        },
        onErrors);


export const deleteUserCompleted = () => ({
    type: actionTypes.DELETE_USER_COMPLETED
})

export const deleteUser = (id, password, onSuccess, onErrors) => dispatch =>
    backend.userService.deleteUser(id, password, 
        () => {
            dispatch(deleteUserCompleted());
            dispatch(logout());
            onSuccess();
        },
        onErrors);

export const changePassword = (id, oldPassword, newPassword, onSuccess, onErrors) => dispatch =>
    backend.userService.changePassword(id, oldPassword, newPassword, onSuccess, onErrors);

export const sentRequestCompleted = (sentRequest, sentPetition) => ({
    type: actionTypes.SENT_REQUEST_COMPLETED,
    sentRequest,
    sentPetition
})

export const sendRequest = (id, destination, onErrors) => dispatch =>
    backend.friendService.sendRequest(destination, 
        () => {
            dispatch(sentRequestCompleted({originUser: id, destinationUser: destination, date: null}, true))
        }, onErrors);

export const findRequestsCompleted = receivedRequests => ({
    type: actionTypes.RECEIVED_REQUESTS_COMPLETED,
    receivedRequests
})

export const findRequests = (id) => dispatch =>
    backend.friendService.getReceivedRequests(id, result => dispatch(findRequestsCompleted(result)));

export const acceptRequestCompleted = (request, areFriends, friend) => ({
    type: actionTypes.ACCEPT_REQUEST_COMPLETED,
    request,
    areFriends,
    friend
})

export const acceptRequest = (request, friend, onErrors) => dispatch =>
    backend.friendService.acceptRequest({origin: request.originUser, destination: request.destinationUser}, 
        () => {
            dispatch(acceptRequestCompleted(request, true, 
                {id: friend.id, userName: friend.userName, image: friend.image}))
        }, onErrors);

export const rejectRequestCompleted = (request, receivedPetition) => ({
    type: actionTypes.REJECT_REQUEST_COMPLETED,
    request,
    receivedPetition
})

export const rejectRequest = (request, onErrors) => dispatch =>
    backend.friendService.rejectRequest({origin: request.originUser, destination: request.destinationUser}, 
        () => dispatch(rejectRequestCompleted(request, false)), onErrors);



export const clearFindFriends = () => ({
    type: actionTypes.CLEAR_FRIENDS_SEARCH
});

const findFriendsCompleted = friendsFound =>({
    type: actionTypes.FIND_FRIENDS_COMPLETED,
    friendsFound
})

export const findAllFriends = id => dispatch => {

    backend.friendService.findAllFriendsById(id,
        result => dispatch(findFriendsCompleted(result)));

}

export const findSummaryFriends = id => dispatch => {

    backend.friendService.findSummaryFriendsById(id,
        result => dispatch(findFriendsCompleted(result)));

}

export const deleteFriendCompleted = friend => ({
    type: actionTypes.DELETE_FRIEND_COMPLETED,
    friend
})

export const deleteFriend = (user, friend, onErrors) => dispatch =>
    backend.friendService.deleteFriend({user: user, friend: friend}, 
        () => dispatch(deleteFriendCompleted(friend, false)), onErrors);



export const clearFoundFollowingList = () => ({
    type: actionTypes.CLEAR_FOUND_FOLLOWING_LIST
});

const findFollowingListByIdCompleted = foundFollowingList => ({
    type: actionTypes.GET_FOLLOWING_LIST_COMPLETED,
    foundFollowingList
});

export const findFollowingListById = (id, onErrors) => dispatch => {
    backend.userService.getPublicList(id,
        foundFollowingList => dispatch(findFollowingListByIdCompleted(foundFollowingList)),
        onErrors);
}

export const findMyFollowingListById = (onErrors) => dispatch => {
    backend.userService.getPersonalList(foundFollowingList => 
            dispatch(findFollowingListByIdCompleted(foundFollowingList)),
        onErrors);
}

const changeListVisibilityCompleted = () => ({
    type: actionTypes.CHANGE_LIST_VISIBILITY_COMPLETED
});

export const changeListVisibility = (onErrors) => dispatch => {
    backend.userService.changeListVisibility(() => 
            dispatch(changeListVisibilityCompleted()),
        onErrors);
}



export const reorderAllFollowingListTitlesCompleted = (newList) => ({
    type: actionTypes.REORDER_All_FOLLOWING_TITLES_COMPLETED,
    newList
});

export const reorderWatchingFollowingListTitlesCompleted = (newList) => ({
    type: actionTypes.REORDER_WATCHING_FOLLOWING_TITLES_COMPLETED,
    newList
});

export const reorderCompletedFollowingListTitlesCompleted = (newList) => ({
    type: actionTypes.REORDER_COMPLETED_FOLLOWING_TITLES_COMPLETED,
    newList
});

export const reorderPlanToWatchFollowingListTitlesCompleted = (newList) => ({
    type: actionTypes.REORDER_PLAN_TO_WATCH_FOLLOWING_TITLES_COMPLETED,
    newList
});

export const reorderDroppedFollowingListTitlesCompleted = (newList) => ({
    type: actionTypes.REORDER_DROPPED_FOLLOWING_TITLES_COMPLETED,
    newList
});



export const editEntryFromListCompleted = (newEntry, lastState) => ({
    type: actionTypes.EDIT_FOLLOWING_LIST_ENTRY_COMPLETED,
    newEntry,
    lastState
});

export const editEntryFromList = (id, params, newEntry, lastState, onErrors) => dispatch => {
    backend.followingListService.editTitleFromList(id, params,
        () => {
            dispatch(editEntryFromListCompleted(newEntry, lastState));
        }, onErrors);
}

export const deleteEntryFromListCompleted = (titleId, lastState) => ({
    type: actionTypes.DELETE_FOLLOWING_LIST_ENTRY_COMPLETED,
    titleId,
    lastState
});

export const deleteEntryFromList = (id, lastState, onErrors) => dispatch => {
    backend.followingListService.deleteTitleFromList(id,
        () => {
            dispatch(deleteEntryFromListCompleted(id, lastState));
        }, onErrors);
}




export const clearFoundCustomLists = () => ({
    type: actionTypes.CLEAR_FOUND_CUSTOM_LISTS
});

const findCustomListsByIdCompleted = customLists => ({
    type: actionTypes.GET_CUSTOM_LISTS_COMPLETED,
    customLists
});

export const findLastCustomListsById = (id, onErrors) => dispatch => {
    backend.userService.getLastLists(id,
        customLists => dispatch(findCustomListsByIdCompleted(customLists)),
        onErrors);
}

export const findAllCustomListsById = (id, onErrors) => dispatch => {
    backend.userService.getAllLists(id,
        customLists => dispatch(findCustomListsByIdCompleted(customLists)),
        onErrors);
}
