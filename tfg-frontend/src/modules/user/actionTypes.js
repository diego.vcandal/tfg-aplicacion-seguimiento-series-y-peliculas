export const SIGNUP_COMPLETED = "users/signUpCompleted";
export const LOGIN_COMPLETED = "project/users/loginCompleted";
export const LOGOUT = "project/users/logout";

export const GET_USER_INFO_COMPLETED = "project/users/userFound";
export const CLEAR_FOUND_USER = "project/users/clearFoundUser";

export const UPDATE_PROFILE_COMPLETED = "project/users/updateProfileCompleted";
export const DELETE_USER_COMPLETED = "project/users/deleteUserCompleted";

export const SENT_REQUEST_COMPLETED = "project/users/sentRequestCompleted";
export const RECEIVED_REQUESTS_COMPLETED = "project/users/receivedRequestsCompleted";
export const ACCEPT_REQUEST_COMPLETED = "project/users/acceptRequestCompleted";
export const REJECT_REQUEST_COMPLETED = "project/users/rejectRequestCompleted";

export const CLEAR_FRIENDS_SEARCH = "project/users/clearFriendsSearch";
export const FIND_FRIENDS_COMPLETED = "project/users/findFriendsCompleted";
export const DELETE_FRIEND_COMPLETED = "project/users/deleteFriendCompleted";

export const CLEAR_FOUND_FOLLOWING_LIST = "project/users/clearFoundFollowingList";
export const GET_FOLLOWING_LIST_COMPLETED = "project/users/findFollowingListByIdCompleted";
export const CHANGE_LIST_VISIBILITY_COMPLETED = "project/users/changeListVisbilityCompleted";

export const REORDER_All_FOLLOWING_TITLES_COMPLETED = "project/users/reorderAllFollowingListTitlesCompleted";
export const REORDER_WATCHING_FOLLOWING_TITLES_COMPLETED = "project/users/reorderWatchingFollowingListTitlesCompleted";
export const REORDER_COMPLETED_FOLLOWING_TITLES_COMPLETED = "project/users/reorderCompletedFollowingListTitlesCompleted";
export const REORDER_PLAN_TO_WATCH_FOLLOWING_TITLES_COMPLETED = "project/users/reorderPlanToWatchFollowingListTitlesCompleted";
export const REORDER_DROPPED_FOLLOWING_TITLES_COMPLETED = "project/users/reorderDroppedFollowingListTitlesCompleted";

export const EDIT_FOLLOWING_LIST_ENTRY_COMPLETED = "project/users/editEntryFromListCompleted";
export const DELETE_FOLLOWING_LIST_ENTRY_COMPLETED = "project/users/deleteEntryFromList";

export const GET_CUSTOM_LISTS_COMPLETED = "project/users/findCustomListsByIdCompleted";
export const CLEAR_FOUND_CUSTOM_LISTS = "project/users/clearFoundCustomLists";