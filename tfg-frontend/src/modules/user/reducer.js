import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    user: null,
    sentRequests: [],
    receivedRequests: [],
    friendsFound: null,
    foundUser: null,
    foundFollowingList: null,
    customLists: null,
};

const user = (state = initialState.user, action) => {

    switch (action.type) {

        case actionTypes.SIGNUP_COMPLETED:
            return action.authenticatedUser.user;

        case actionTypes.LOGIN_COMPLETED:
            return action.authenticatedUser.user;
            
        case actionTypes.LOGOUT:
            return initialState.user;

        case actionTypes.UPDATE_PROFILE_COMPLETED:
            return action.user;

        case actionTypes.DELETE_USER_COMPLETED:
            return initialState.user;

        default:
            return state;

    }

}

const sentRequests = (state = initialState.sentRequests, action) => {

    switch (action.type) {

        case actionTypes.SIGNUP_COMPLETED:
            return action.authenticatedUser.petitions;

        case actionTypes.LOGIN_COMPLETED:
            return action.authenticatedUser.petitions;

        case actionTypes.LOGOUT:
            return initialState.sentRequests;

        case actionTypes.DELETE_USER_COMPLETED:
            return initialState.sentRequests;

        case actionTypes.SENT_REQUEST_COMPLETED:
            return [...state, action.sentRequest];

        default:
            return state;

    }

}

const receivedRequests = (state = initialState.receivedRequests, action) => {

    switch (action.type) {

        case actionTypes.RECEIVED_REQUESTS_COMPLETED:
            return action.receivedRequests;

        case actionTypes.LOGOUT:
            return initialState.receivedRequests;

        case actionTypes.ACCEPT_REQUEST_COMPLETED:
            return state.filter(r => r !== action.request);
            
        case actionTypes.REJECT_REQUEST_COMPLETED:
            return state.filter(r => r !== action.request);

        default:
            return state;

    }

}

const friendsFound = (state = initialState.friendsFound, action) => {

    let elements;

    if(state != null)
        elements = state.totalElements;

    switch (action.type) {

        case actionTypes.FIND_FRIENDS_COMPLETED:
            return action.friendsFound;

        case actionTypes.CLEAR_FRIENDS_SEARCH:
            return initialState.friendsFound;

        case actionTypes.DELETE_FRIEND_COMPLETED:
            return {...state, totalElements: (elements - 1), 
                items: state.items.filter(f => f.id !== action.friend)};

        case actionTypes.ACCEPT_REQUEST_COMPLETED:
            return state == null ? null :
                {...state, totalElements: (elements + 1), items: (elements >= 5 ? state.items : [...state.items, action.friend])};


        default:
            return state;

    }

}

const foundUser = (state = initialState.foundUser, action) => {

    switch (action.type) {

        case actionTypes.GET_USER_INFO_COMPLETED:
            return action.foundUser;

        case actionTypes.CLEAR_FOUND_USER:
            return initialState.foundUser;

        case actionTypes.SENT_REQUEST_COMPLETED:
            return state == null ? null : 
                {...state, relationshipInfoDTO: {...state.relationshipInfoDTO, sentPetition: action.sentPetition}};

        case actionTypes.ACCEPT_REQUEST_COMPLETED:
            return state == null ? null : 
                {...state, relationshipInfoDTO: {...state.relationshipInfoDTO, areFriends: action.areFriends}};

        case actionTypes.REJECT_REQUEST_COMPLETED:
            return state == null ? null : 
                {...state, relationshipInfoDTO: {...state.relationshipInfoDTO, receivedPetition: action.receivedPetition}};
            
        default:
            return state;

    }

}


const foundFollowingList = (state = initialState.foundFollowingList, action) => {

    let new_state;

    switch (action.type) {

        case actionTypes.GET_FOLLOWING_LIST_COMPLETED:
            return action.foundFollowingList;

        case actionTypes.CLEAR_FOUND_FOLLOWING_LIST:
            return initialState.foundFollowingList;

        case actionTypes.CHANGE_LIST_VISIBILITY_COMPLETED:
            return {...state, visible: !state.visible};

        case actionTypes.REORDER_All_FOLLOWING_TITLES_COMPLETED:
            return {...state, all: action.newList}

        case actionTypes.REORDER_WATCHING_FOLLOWING_TITLES_COMPLETED:
            return {...state, watching: action.newList}

        case actionTypes.REORDER_COMPLETED_FOLLOWING_TITLES_COMPLETED:
            return {...state, completed: action.newList}

        case actionTypes.REORDER_PLAN_TO_WATCH_FOLLOWING_TITLES_COMPLETED:
            return {...state, planToWatch: action.newList}

        case actionTypes.REORDER_DROPPED_FOLLOWING_TITLES_COMPLETED:
            return {...state, dropped: action.newList}

        case actionTypes.EDIT_FOLLOWING_LIST_ENTRY_COMPLETED:

            new_state = {...state, all: state.all.map(title => 
                                title.titleId === action.newEntry.titleId ? action.newEntry : title)};

            removeFromStatusList(new_state, action.newEntry.titleId, action.lastState);
            addToStatusList(new_state, action.newEntry);

            return new_state;

        case actionTypes.DELETE_FOLLOWING_LIST_ENTRY_COMPLETED:

            new_state = {...state, all: removeFromList(state.all, action.titleId)};

            removeFromStatusList(new_state, action.titleId, action.lastState);

            return new_state;

        default:
            return state;

    }
}


const removeFromStatusList = (new_state, titleId, lastState) => {

    switch (lastState) {
        case 0:
            new_state.planToWatch = removeFromList(new_state.planToWatch, titleId);
            break;

        case 1:
            new_state.watching = removeFromList(new_state.watching, titleId);
            break;

        case 2:
            new_state.completed = removeFromList(new_state.completed, titleId);
            break;

        case 3:
            new_state.dropped = removeFromList(new_state.dropped, titleId);
            break;

        default:
            break;
    }

}

const addToStatusList = (new_state, newEntry) => {

    switch (newEntry.status) {
        case 0:
            new_state.planToWatch = addToList(new_state.planToWatch, newEntry);
            break;

        case 1:
            new_state.watching = addToList(new_state.watching, newEntry);
            break;

        case 2:
            new_state.completed = addToList(new_state.completed, newEntry);
            break;

        case 3:
            new_state.dropped = addToList(new_state.dropped, newEntry);
            break;

        default:
            break;
    }

}

const removeFromList = (list, titleId) => list.filter(title => title.titleId !== titleId);
const addToList = (list, newTitle) => [...list, newTitle];

const customLists = (state = initialState.customLists, action) => {

    switch (action.type) {

        case actionTypes.GET_CUSTOM_LISTS_COMPLETED:
            return action.customLists;

        case actionTypes.CLEAR_FOUND_CUSTOM_LISTS:
            return initialState.customLists;  
        default:
            return state;

    }

}

const reducer = combineReducers({
    user,
    sentRequests,
    receivedRequests,
    friendsFound,
    foundUser,
    foundFollowingList,
    customLists
});

export default reducer;
