import * as Roles from './utils/Roles';

const getModuleState = state => state.users;

export const getUser = state => 
    getModuleState(state).user;

export const isLoggedIn = state =>
    getUser(state) !== null

export const getUserName = state => 
    isLoggedIn(state) ? getUser(state).userName : null;

export const getFoundUser = state => 
    getModuleState(state).foundUser;

export const getSentRequests = state => 
    getModuleState(state).sentRequests;

export const getReceivedRequests = state => 
    getModuleState(state).receivedRequests;  

export const getFriends = state => 
    getModuleState(state).friendsFound;

export const isAdmin = state => 
    isLoggedIn(state) && getUser(state).role === Roles.ADMIN_ROLE

export const getFollowingList = state => 
    getModuleState(state).foundFollowingList;

export const getCustomLists = state => 
    getModuleState(state).customLists;