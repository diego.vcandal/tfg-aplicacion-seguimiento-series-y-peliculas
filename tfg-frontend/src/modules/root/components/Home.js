import React from 'react';
import { LastTitles } from '../../search';

const Home = () => (
    <div className="text-center mt-4">
        <LastTitles></LastTitles>
    </div>
);

export default Home;
