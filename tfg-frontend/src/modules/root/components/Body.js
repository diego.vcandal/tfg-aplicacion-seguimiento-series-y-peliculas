import React from 'react';
import {useSelector} from 'react-redux';
import {Route, Switch} from 'react-router-dom';

import AppGlobalComponents from './AppGlobalComponents';
import Home from './Home';
import {PageNotFound} from '../../common';
import users from '../../user';

import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {SignUp, Login, Logout, Profile, UpdateProfile, ChangePassword, MyRequests, 
    AllFriends, FollowingList, AllLists} from '../../user';

import {SearchUsersResult, SearchAllTitles, SearchExternalMovies,
    SearchExternalTVShows, SearchPeople, SearchLists} from '../../search';

import {Movie, TVShow, AllSeasons, SeasonDetails,
    CreateTitle, EditTitle, CreateSeason, CreateEpisode,
    EditEpisode, EditSeason, AllCastAndCrew, AllReviews} from '../../titles';

import {CreatePerson, UpdatePerson, PersonDetails} from '../../people';

import {UpdateList, CustomList} from '../../lists';

import ScrollToTop from './ScrollToTop';


const Body = () => {

    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const isAdmin = useSelector(users.selectors.isAdmin);
    const apiEnabled = process.env.REACT_APP_API_ENDPOINT_ENABLED === 'true';

    return (
       
        <div className="container-xl">
            
            <AppGlobalComponents/>
            <ScrollToTop />
            <ToastContainer />

            <Switch>
                <Route exact path="/"><Home/></Route>
                <Route exact path="/search/users"><SearchUsersResult/></Route>
                <Route exact path="/search/titles"><SearchAllTitles type={0}/></Route>
                <Route exact path="/search/tmdb-movies"><SearchAllTitles type={1}/></Route>
                <Route exact path="/search/tmdb-tvshows"><SearchAllTitles type={2}/></Route>
                <Route exact path="/search/people"><SearchPeople type={0} /></Route>
                <Route exact path="/search/externalPeople"><SearchPeople type={1}/></Route>
                <Route exact path="/search/lists"><SearchLists/></Route>
                {apiEnabled && <Route exact path="/search/titles/tmdb-movies"><SearchExternalMovies/></Route>}
                {apiEnabled && <Route exact path="/search/titles/tmdb-tvshows"><SearchExternalTVShows/></Route>}
                {!isLoggedIn && <Route exact path="/users/signup"><SignUp/></Route>}
                {!isLoggedIn && <Route exact path="/users/login"><Login/></Route>}
                {isLoggedIn && <Route exact path="/users/logout"><Logout/></Route>}
                {isLoggedIn && <Route exact path="/users/change-password"><ChangePassword/></Route>}
                {isLoggedIn && <Route exact path="/users/update-profile"><UpdateProfile/></Route>}
                {isLoggedIn && <Route exact path="/users/my-requests"><MyRequests/></Route>}
                <Route exact path="/users/:id"><Profile/></Route>
                <Route exact path="/users/:id/friends"><AllFriends/></Route>
                <Route exact path="/users/:id/following-list"><FollowingList/></Route>
                <Route exact path="/users/:id/lists"><AllLists/></Route>

                <Route exact path="/movies/:id"><Movie/></Route>
                <Route exact path="/tvshows/:id"><TVShow/></Route>
                <Route exact path="/movies/:movieId/reviews"><AllReviews/></Route>
                <Route exact path="/tvshows/:tvshowId/reviews"><AllReviews/></Route>
                <Route exact path="/movies/:id/cast-crew"><AllCastAndCrew type={0} /></Route>
                <Route exact path="/tvshows/:id/cast-crew"><AllCastAndCrew type={1}/></Route>
                <Route exact path="/tvshows/:id/seasons"><AllSeasons/></Route>
                <Route exact path="/tvshows/:id/seasons/:seasonNumber"><SeasonDetails/></Route>

                {apiEnabled && <Route exact path="/tmdb/movies/:externalId"><Movie/></Route>}
                {apiEnabled && <Route exact path="/tmdb/tvshows/:externalId"><TVShow/></Route>}
                {apiEnabled && <Route exact path="/tmdb/movies/:externalId/cast-crew"><AllCastAndCrew type={0}/></Route>}
                {apiEnabled && <Route exact path="/tmdb/tvshows/:externalId/cast-crew"><AllCastAndCrew type={1}/></Route>}
                {apiEnabled && <Route exact path="/tmdb/tvshows/:externalId/seasons"><AllSeasons/></Route>}
                {apiEnabled && <Route exact path="/tmdb/tvshows/:externalId/seasons/:seasonNumber"><SeasonDetails/></Route>}

                {isAdmin && <Route exact path="/titles/create"><CreateTitle update={false} type={-1} /></Route>}
                {isAdmin && <Route exact path="/movies/:movieId/update"><EditTitle type={0} /></Route>}
                {isAdmin && <Route exact path="/tvshows/:tvshowId/update"><EditTitle type={1} /></Route>}
                {isAdmin && <Route exact path="/tvshows/:tvshowId/create-season"><CreateSeason update={false} /></Route>}
                {isAdmin && <Route exact path="/tvshows/:tvshowId/seasons/:seasonNumber/create-episode"><CreateEpisode update={false} /></Route>}
                {isAdmin && <Route exact path="/tvshows/:tvshowId/seasons/:seasonId/update"><EditSeason/></Route>}
                {isAdmin && <Route exact path="/tvshows/:tvshowId/seasons/:seasonNumber/episodes/:episodeId/update"><EditEpisode/></Route>}

                {isAdmin && <Route exact path="/people/create"><CreatePerson update={false} /></Route>}
                {isAdmin && <Route exact path="/people/:personId/update"><UpdatePerson /></Route>}

                {isLoggedIn && <Route exact path="/lists/create"><UpdateList create={true}/></Route>}
                {isLoggedIn && <Route exact path="/lists/:listId/update"><UpdateList create={false} /></Route>}
                <Route exact path="/lists/:id"><CustomList/></Route>

                <Route exact path="/people/:id"><PersonDetails/></Route>
                <Route exact path="/tmdb/people/:externalId"><PersonDetails/></Route>

                <Route exact path="/404"><PageNotFound/></Route>
                <Route><PageNotFound/></Route>
            </Switch>
        </div>

    );

};

export default Body;
