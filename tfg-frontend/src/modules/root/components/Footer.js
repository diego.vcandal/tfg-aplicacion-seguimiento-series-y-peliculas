import React from 'react';
import {FormattedMessage} from 'react-intl';

const Footer = () => {

    var logo = `${process.env.PUBLIC_URL}/images/TMDB_logo.png`;

    return (
    <div id="footer">
        <br/>
        <hr/>
        <footer>
            <p className="text-center">
                <FormattedMessage id="project.root.Footer.text"/>
            </p>
            <div className="row w-100 mb-3 mt-5 center-element">
                <img src={logo} alt="tmdb api logo" className="api-logo-footer center-element"/>
            </div>
            <p className="text-center">
                This product uses the TMDb API but is not endorsed or certified by TMDb.
            </p>
            <p className="text-center">
                Any use of data from the TMDb API is explicitly indicated.
            </p>
        </footer>
    </div>
    );

};

export default Footer;
