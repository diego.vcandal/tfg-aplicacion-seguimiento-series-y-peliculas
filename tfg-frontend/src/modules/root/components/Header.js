/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import {Link} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {useSelector} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import users from '../../user';
import language from '../../language';
import common from '../../common';

import {SearchElements} from '../../search';
import {AvailableLanguages} from '../../language/AvailableLanguages';

const Header = () => {

    const dispatch = useDispatch();
    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const isAdmin = useSelector(users.selectors.isAdmin);
    const user = useSelector(users.selectors.getUser);
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;

    var lang  = useSelector(language.selectors.getLanguage).language;    

    if(user && user.image)
        user_default_image = `${process.env.REACT_APP_BACKEND_URL}/users/${user.id}/image`;

    if(lang == null)
        lang = '';

    return (

        <nav className="navbar navbar-expand-lg navbar-light border navbar-shadow nav-bg-color sticky-top">
            <Link className="navbar-brand" to="/">TFG - TV and Movie tracking app</Link>

            <ul className="navbar-nav mr-auto col-5">
                <li className="row w-100">
                    <SearchElements/>
                </li>
            </ul>

            <button className="navbar-toggler" type="button" 
                data-toggle="collapse" data-target="#navbarSupportedContent" 
                aria-controls="navbarSupportedContent" aria-expanded="false" 
                aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">

                <ul className="navbar-nav ml-auto">

                    <select value={lang} className="custom-select my-1 mr-sm-2 language-select"
                        onChange={e => {dispatch(language.actions.changeLanguage(e.target.value));
                                        common.localStorageUtils.addObject('TFG_language', {language: e.target.value})}} >
                        {AvailableLanguages.map(type => 
                            <FormattedMessage id={type.name}>
                                {
                                    (msg) => <option key={type.id} value={type.type}>{msg}</option>
                                }
                            </FormattedMessage>
                        )}
                    </select>

                    {!isLoggedIn &&
                        <li className="nav-item">
                            <Link className="nav-link" to="/users/signUp">
                                <FormattedMessage id="project.users.SignUp.title"/>
                            </Link>
                        </li>
                    }

                    {!isLoggedIn &&
                        <li className="nav-item">
                        <Link className="nav-link" to="/users/login">
                            <FormattedMessage id="project.users.Login.title"/>
                        </Link>
                        </li>
                    }

                    {isLoggedIn &&

                        <li className="nav-item dropdown mr-1">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
                                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span className="userName-menu">{user.userName}</span>
                                <img className="image-xx-small link-pointer ml-2" alt="user profile image" 
                                    src={user_default_image}/>
                            </a>

                            <div className="dropdown-menu dropdown-menu-right text-center" aria-labelledby="navbarDropdown">
                                <Link className="dropdown-item" to={`/users/` + user.id}>
                                    <FormattedMessage id="project.users.Profile.title"/>
                                </Link>
                                <Link className="dropdown-item" to="/users/my-requests">
                                    <FormattedMessage id="project.users.MyRequests.title"/>
                                </Link>

                                <Link className="dropdown-item" to={`/users/${user.id}/lists`}>
                                    <FormattedMessage id="project.users.lists.title"/>
                                </Link>

                                {isAdmin &&
                                    <div>
                                        <div className="dropdown-divider" />
                                        <Link className="dropdown-item" to="/titles/create">
                                            <FormattedMessage id="project.users.Titles.create.title"/>
                                        </Link>
                                        <Link className="dropdown-item" to="/people/create">
                                            <FormattedMessage id="project.users.People.create.title"/>
                                        </Link>
                                    </div>
                                }
                                    
                                <div>
                                    <div className="dropdown-divider" />
                                    <Link className="dropdown-item" to={`/users/` + user.id + `/following-list`}>
                                        <FormattedMessage id="project.users.buttons.followingList"/>
                                    </Link>
                                </div>

                                <div className="dropdown-divider"></div>
                                <Link className="dropdown-item" to="/users/logout">
                                    <FormattedMessage id="project.users.Logout.title"/>
                                </Link>
                            </div>

                        </li>

                    }
                </ul>
            </div>
        </nav>

    );

};

export default Header;
