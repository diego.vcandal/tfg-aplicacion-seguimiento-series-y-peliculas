import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';

import users from '../../user';
import Root from './Root';

import language from '../../language';
import {getInitialLocale} from '../../../i18n';

const Initializer = () => {

    const dispatch = useDispatch();

    const initialLocale = getInitialLocale();

    useEffect(() => {
        
        dispatch(users.actions.tryLoginFromServiceToken(
            () => dispatch(users.actions.logout())));

        dispatch(language.actions.changeLanguage(initialLocale));

    });

    return (
        <Root />
    );

}
    
export default Initializer;