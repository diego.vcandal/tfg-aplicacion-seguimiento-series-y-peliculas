import React from 'react';
import {useSelector} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';

import Header from './Header';
import Body from './Body';
import Footer from './Footer';
import language from '../../language';

import {IntlProvider} from 'react-intl';
import {initReactIntl} from '../../../i18n';

const Root = () => {

    const lang = useSelector(language.selectors.getLanguage).language;

    /* Configure i18n. */
    const {locale, messages} = initReactIntl(lang);

    return (
        <div>
            <IntlProvider locale={locale} messages={messages}>
                <Router>
                    <div>
                        <Header/>
                        <div>
                        <Body/>
                        <Footer/>

                        </div>
                        
                        
                    </div>
                </Router>
                
            </IntlProvider>
        </div>
    );

}
    
export default Root;