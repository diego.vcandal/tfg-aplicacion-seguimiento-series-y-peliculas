const getModuleState = state => state.root;

export const getError = state => getModuleState(state).error;

export const isLoading = state => getModuleState(state).loading;