/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import users from '../../user';
import * as followingListActions from '../followingListActions';
import * as selectors from '../selectors';
import { FollowingStatus } from '../../user/utils/FollowingStatus';

const AddToFollowingList = ({titleId, edit, external, type}) => {

    const history = useHistory();
    const dispatch = useDispatch();

    const foundFollowingListTitle = useSelector(selectors.getFoundFollowingListTitle);
    const isAdmin = useSelector(users.selectors.isAdmin);

    const [, setBackendErrors] = useState(null);

    const [status, setStatus]  = useState(edit ? foundFollowingListTitle.status : 0);
    const [rating, setRating]  = useState(edit ? foundFollowingListTitle.rating && (foundFollowingListTitle.rating < 1 || 
                                    foundFollowingListTitle.rating > 10) ? '' : foundFollowingListTitle.rating : '');

    let form;

    const handleClose = () => {
        setBackendErrors(null);
        form.classList = "";
    }

    const handleSubmit = (event) => {

        event.preventDefault();
        
        if (form.checkValidity()) {

            if (!edit) {
                if (external) {

                    if (type === 0) {
                        dispatch(followingListActions.addExternalMovieToList(titleId, {status: status},
                            () => history.go(0), 
                            errors => setBackendErrors(errors)));
                    } else {
                        dispatch(followingListActions.addExternalTVShowToList(titleId, {status: status},
                            () => history.go(0), 
                            errors => setBackendErrors(errors)));
                    }
        
                } else {
                    dispatch(followingListActions.addToList(titleId, {status: status},
                        () => history.go(0), 
                        errors => setBackendErrors(errors)));
                }

            } else {

                dispatch(followingListActions.editTitleFromList(titleId, {status: status, rating: (rating === '' ? null : rating)},
                    () => history.go(0), 
                    errors => setBackendErrors(errors)));

            }
            
        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    const handleDoDelete = () => {
        dispatch(followingListActions.deleteTitleFromList(titleId,
                    () => history.go(0), 
                    errors => setBackendErrors(errors)));
    }

    return(
        <div className="row center-element w-90">

            {isAdmin && external && <span className="following-list-title-divider w-50 center-element mt-2"></span>}

            {!edit &&
                <div className="row center-element w-100">
                    <button className="btn btn-primary btn-cache-title ml-2 mr-2 mt-2 w-100" 
                        type="button" title="Create"
                        data-toggle="modal" data-target="#add-entry-panel">
                        <FormattedMessage id='project.titles.buttons.followingList.add'/>
                    </button>
                </div>
            }

            {edit &&
                <div className="row center-element w-100">
                    <button className="btn btn-primary btn-cache-title ml-2 mr-2 mt-2 w-100" 
                        type="button" title="Create"
                        data-toggle="modal" data-target="#add-entry-panel">
                        <FormattedMessage id='project.titles.buttons.followingList.edit'/>
                    </button>
                </div>
            }

            {edit &&
                <div className="row center-element w-100">
                    <button className="btn btn-danger btn-cache-title ml-2 mr-2 mt-2 w-100" 
                        type="button" title="Create"
                        data-toggle="modal" data-target="#delete-entry-panel">
                        <FormattedMessage id='project.titles.buttons.followingList.delete'/>
                    </button>
                </div>
            }
                
            <div id={"add-entry-panel"} className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                {edit ? 
                                    <FormattedMessage id="project.titles.followingList.edit.modal.title"/>
                                :
                                    <FormattedMessage id="project.titles.followingList.add.modal.title"/>
                                }
                            </h5>
                        </div>
                        
                        <div className="modal-body">
                            <form ref={node => form = node}>
                                <div className="row w-90 center-element">
                                        <label htmlFor="status" className="col-5 title-label-color label-v-class">
                                            <FormattedMessage id="project.titles.followingList.add.modal.status"/>
                                        </label>
                                        <select value={status} className="col-6 center-element custom-select "
                                            onChange={e => setStatus(e.target.value)} >
                                            {FollowingStatus.map(type => 
                                                <FormattedMessage id={type.name}>
                                                    {
                                                        (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                                    }
                                                </FormattedMessage>
                                            )}
                                        </select>
                                </div>

                                {edit &&
                                    <div className="row w-90 center-element mt-3">
                                        <label htmlFor="rating" className="col-5 title-label-color label-v-class">
                                            <FormattedMessage id="project.titles.followingList.add.modal.rating"/>
                                        </label>
                                        <input type="number" id="rating" className="col-6 center-element form-control"
                                            value={rating}
                                            onChange={e => setRating(e.target.value)}
                                            min="1" max="10" required={foundFollowingListTitle.hasReview} />
                                    </div>
                                }

                                {edit && foundFollowingListTitle.hasReview && 
                                    <div className="w-90 center-element mt-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-circle mb-1 mr-2" viewBox="0 0 16 16">
                                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                            <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                                        </svg>
                                        <span className="light-gray-text"><FormattedMessage id="project.titles.followingList.add.modal.reviewWarning"/></span>
                                    </div>
                                }
                                
                            </form>
                        </div>
                                        
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal"
                                onClick={() => handleClose()}>
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="submit" className="btn btn-success"
                                onClick={(e) => handleSubmit(e)}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div id={"delete-entry-panel"} className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                <FormattedMessage id="project.titles.followingList.delete.modal.title"/>
                            </h5>
                        </div>
                        <div className="modal-body text-center">
                            <p>
                                <FormattedMessage id="project.titles.followingList.delete.modal.body"/>
                            </p>

                            {foundFollowingListTitle && foundFollowingListTitle.rating > 0 && 
                                <div className="mt-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-circle mb-1 mr-2" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                                    </svg>
                                    <span className="light-gray-text"><FormattedMessage id="project.titles.followingList.delete.modal.reviewWarning"/></span>
                                </div>
                            }

                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal">
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="button" className="btn btn-danger" 
                                data-dismiss="modal"
                                onClick={() => handleDoDelete()}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
    

};

AddToFollowingList.propTypes = {
    titleId: PropTypes.number.isRequired,
    edit: PropTypes.bool.isRequired,
    external: PropTypes.bool.isRequired,
    type: PropTypes.number.isRequired,
};

export default AddToFollowingList;