import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import * as selectors from '../selectors';
import * as castAndCrewActions from '../castAndCrewActions';
import Casts from './Entity/Casts';
import Crews from './Entity/Crews';

const AllCasts = ({type}) => {

    const dispatch = useDispatch();

    const foundCast = useSelector(selectors.getFoundCast);
    const foundCrew = useSelector(selectors.getFoundCrew);
    const foundTitle = useSelector(selectors.getFoundTitle);
    const [loadCrew, setLoadCrew]  = useState(false);
    const [loadCast, setLoadCast]  = useState(false);
    const [loadExternal, setLoadExternal]  = useState(false);

    useEffect(() => {

        if (foundTitle.sourceType === 0){
            dispatch(castAndCrewActions.findAllCast(foundTitle.id, () => setLoadCast(true)));
            dispatch(castAndCrewActions.findAllCrew(foundTitle.id, () => setLoadCrew(true)));
        } else {

            if (type === 0)
                dispatch(castAndCrewActions.findAllExternalMovieCast(foundTitle.externalId, () => setLoadExternal(true)));
            else
                dispatch(castAndCrewActions.findAllExternalTVShowCast(foundTitle.externalId, () => setLoadExternal(true)));
        } 

        return () => dispatch(castAndCrewActions.clearFoundCast());

    }, [foundTitle, type, dispatch]);

    if (!foundCast || !foundTitle) {
        return null;
    }

    return (

        <div className="row mt-4 center-element">
            <div className="col-4 center-element">
                <div className="row mb-2">
                    <span className="title-pre-title-box"></span>
                    <h3 className="m-0 main-cast-title">
                        <FormattedMessage id='project.titles.update.menu.cast'/>
                        {(loadCast || loadExternal) && (" - (" + foundCast.length + ")")}
                    </h3>
                </div>
                <div className="row">
                {(loadCast || loadExternal) && <Casts casts={foundCast} />}
                </div>
            </div>
            <div className="col-4 center-element">
                <div className="row mb-2">
                    <span className="title-pre-title-box"></span>
                    <h3 className="m-0 main-cast-title">
                        <FormattedMessage id='project.titles.update.menu.crew'/>
                        {(loadCrew || loadExternal) && (" - (" + foundCrew.length + ")")}
                    </h3>
                </div>
                <div className="row">
                    {(loadCrew || loadExternal) && <Crews crews={foundCrew} />}
                </div>
            </div>
        </div>
    );

}

export default AllCasts;