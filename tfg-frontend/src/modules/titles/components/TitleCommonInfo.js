/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {FormattedMessage, FormattedDate} from 'react-intl';
import PropTypes from 'prop-types';
import {Link, useHistory, useLocation } from 'react-router-dom';

import {Errors} from '../../common';
import {getGenreNameCode} from '../util/Genres';
import Seasons from './Seasons';
import AdminOptionsModal from './CreateAndEdit/Modals/AdminOptionsModal';
import * as actions from '../actions';
import * as ratingActions from '../ratingActions';
import * as selectors from '../selectors';
import users from '../../user';

import ReactStars from "react-rating-stars-component";
import DeleteReviewModal from './CreateAndEdit/Modals/DeleteReviewModal';
import AddToFollowingList from './AddToFollowingList';

const TitleCommonInfo = ({title, type, seasons, external}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation().pathname;

    const isAdmin = useSelector(users.selectors.isAdmin);
    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const myReview = useSelector(selectors.getFoundReview);
    const foundFollowingListTitle = useSelector(selectors.getFoundFollowingListTitle);

    const [backendErrors, setBackendErrors] = useState(null);
    const [rating, setRating] = useState(myReview !== null ? myReview.rating : 0);
    const [avgRating, setAvgRating] = useState(title.avgRating);
    const [ratingCount, setRatingCount] = useState(title.timesRated);

    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_default_image_wide = `${process.env.PUBLIC_URL}/images/title_default_image_wide_large.png`;
    var external_api_imgage_path = "https://image.tmdb.org/t/p/";
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;

    let headerPath = title_default_image_wide;
    let portraitPath = title_default_image;
    let ratingInput, ratingInputOpen = false;

    let modal;

    const deleteModal = "delete-modal";
    const makePublicModal = "make-public-modal";
    const changeSourceInternalModal = "change-source-internal-modal";
    const changeSourceExternalModal = "change-source-external-modal";
    const deleteReviewModal = "delete-review-modal";

    if(title.sourceType !== 0){

        headerPath = external_api_imgage_path + "original" + title.imageHeaderPath;
        portraitPath = external_api_imgage_path + "w500" + title.imagePortraitPath;

    } else {

        if(title.imagePortrait)
            portraitPath = title_image_path + title.id + "/images/portrait"

        if(title.imageHeader)
            headerPath = title_image_path + title.id + "/images/header"
    }

    const getGenre = (genreId, genreExternalId) => {
        let g = getGenreNameCode(genreId, genreExternalId);
        if(g)
            return g.name;
    };

    const handleCacheTitle = () => {
        if (type === 0) {
            dispatch(actions.createCacheMovie(title.externalId, 
                () => history.go(0), 
                errors => setBackendErrors(errors)));
        } else {
            dispatch(actions.createCacheTVShow(title.externalId, 
                () => history.go(0), 
                errors => setBackendErrors(errors)));
        }
    };


    const handleRatingInputOpenClose = (update) => {
        if(ratingInputOpen) {
            ratingInputOpen = false;
            ratingInput.className="rating-input";
        } else {
            ratingInputOpen = true;
            ratingInput.className = (update ? "rating-input active-update" : "rating-input active-new");
        }
    };

    const doRemoveDispatch = () => {
        dispatch(ratingActions.deleteRating(title.id,
            () => {
                setAvgRating(((avgRating * ratingCount) - rating) / (ratingCount - 1));
                setRatingCount(ratingCount - 1)
                setRating(0);
                history.go(0)
            }));
        
    }
    
    const handleRemoveRating = () => {
        
        if(myReview !== null) {

            if (!myReview.containsReview)
                doRemoveDispatch();
            else
                modal.click();
        }

        ratingInputOpen = false;
        ratingInput.className="rating-input";
    };

    const handleNewRating = (newRating) => {

        if (external) {

            if (type === 0) {
                dispatch(ratingActions.rateExternalMovie(title.externalId, {rating: newRating},
                    () => history.go(0), 
                    errors => setBackendErrors(errors)));
            } else {
                dispatch(ratingActions.rateExternalTVShow(title.externalId, {rating: newRating},
                    () => history.go(0), 
                    errors => setBackendErrors(errors)));
            }

        } else {
        
            if (myReview === null) {
                dispatch(ratingActions.rateTitle(title.id, {rating: newRating},
                    () => {
                        setAvgRating(((avgRating * ratingCount) + newRating) / (ratingCount + 1));
                        setRatingCount(ratingCount + 1)
                        setRating(newRating)
                    }));
            }else{
                dispatch(ratingActions.updateRating(title.id, {rating: newRating},
                    () => {
                        setAvgRating(((avgRating * ratingCount) - rating + newRating) / ratingCount);
                        setRating(newRating)
                    }));
            }

        }

        
        ratingInputOpen = false;
        ratingInput.className="rating-input";
    };
    
    return (
        <div className="col center-element container-title-common-info">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="row">
                <img className="header-img center-element" alt="title poster image" 
                    src={headerPath} />
            </div>

            <div className="row">

                <div className="col-3 text-center img-portrait-container">
                    <img className="portrait-img" alt="title poster image" 
                        src={portraitPath} />

                    <div className="row mt-3">

                        {isAdmin && title && title.id && title.id === -1 &&
                            <div className="row center-element w-90">
                                <button className="btn btn-success btn-cache-title ml-2 mr-2 w-100" 
                                    type="button" title="Create"
                                    onClick={() => handleCacheTitle()}>
                                    <FormattedMessage id='project.titles.button.create'/>
                                </button>
                            </div>
                        }

                        {isAdmin && title && title.id !== -1 && title.sourceType === 0 &&
                            <div className="row center-element w-90">
                                <button className="btn btn-success btn-cache-title ml-2 mr-2 mt-2 w-100" 
                                    type="button" title="Create"
                                    onClick={() => history.push(`/tvshows/${title.id}/create-season`)}>
                                    <FormattedMessage id='project.titles.buttons.add.Season'/>
                                </button>
                            </div>
                        }

                        {isLoggedIn  &&
                            <AddToFollowingList titleId={external ? title.externalId : title.id} 
                                edit={foundFollowingListTitle === null ? false : true} external={external} type={type} />
                        }

                        <div className="mt-3">
                            {(title.duration || title.duration >= 0) &&
                                <div className="row center-element w-90">
                                    <span className="title-sub-info-title w-100 row">
                                        <FormattedMessage id='project.titles.info.duration'/>
                                    </span>
                                    <p className="title-sub-info ml-4">{title.duration}</p>
                                    <p className="title-sub-info ml-1">
                                        <FormattedMessage id='project.titles.info.minutes'/>
                                    </p>
                                </div>
                            }

                            {title.companyName && 
                                <div className="row center-element w-90">
                                    <span className="title-sub-info-title w-100 row">
                                        <FormattedMessage id='project.titles.info.producer'/>
                                    </span>
                                    <p className="title-sub-info ml-4">{title.companyName}</p>
                                </div>
                            }

                            {(title.totalDuration || title.totalDuration >= 0) &&
                                <div className="row center-element w-90">
                                    <span className="title-sub-info-title w-100 row">
                                        <FormattedMessage id='project.titles.info.duration'/>
                                    </span>
                                    <p className="title-sub-info ml-4">{title.totalDuration}</p>
                                    <p className="title-sub-info ml-1">
                                        <FormattedMessage id='project.titles.info.episodeDuration'/>
                                    </p>
                                </div>
                            }

                            {(title.episodeCount || title.episodeCount >= 0) &&
                                <div className="row center-element w-90 mb-2">
                                    <span className="title-sub-info-title row">
                                        <FormattedMessage id='project.titles.info.episodeCount'/>:
                                    </span>
                                    <span className="title-sub-info-inline ml-4">{title.episodeCount}</span>
                                </div>
                            }

                            {(title.seasonNumber || title.seasonNumber >= 0) &&
                                <div className="row center-element w-90 mb-2">
                                    <span className="title-sub-info-title row">
                                        <FormattedMessage id='project.titles.info.seasonCount'/>:
                                    </span>
                                    <span className="title-sub-info-inline ml-4">{title.seasonNumber}</span>
                                </div>
                            }

                            {title.firstAirDate && title.firstAirDate !== 0 &&
                                <div className="row center-element w-90">
                                    <span className="title-sub-info-title  row">
                                        <FormattedMessage id='project.titles.info.firstAirDate'/>
                                    </span>
                                    <p className="title-sub-info ml-4">
                                        <FormattedDate value={new Date(title.firstAirDate)}/>
                                    </p>
                                </div>
                            }

                            {title.lastAirDate && title.lastAirDate !== 0 &&
                                <div className="row center-element w-90">
                                    <span className="title-sub-info-title row">
                                        <FormattedMessage id='project.titles.info.lastAirDate'/>
                                    </span>
                                    <p className="title-sub-info ml-4">
                                        <FormattedDate value={new Date(title.lastAirDate)}/>
                                    </p>
                                </div>
                            }
                        </div>

                    </div>
                </div>

                <div className="col common-title-info mt-2 ml-3">
                    <div className={"pb-2 pt-2 " + (isAdmin && title.setForDelete ? "set-for-delete-container" : "")}>
                        <div className="ml-4">
                            <div className="row">
                                <div className="col">

                                    <div className="row d-block">
                                        <span className="title-name">
                                            {title.title ? title.title : title.originalTitle }
                                        </span>
                                        <span className="title-year ml-3">({title.year})</span>
                                    </div>

                                    {title.title &&
                                        <div className="row mt-2">
                                            <span className="alternative-title light-gray-text ">
                                                {title.title ? title.originalTitle : "" }
                                            </span>
                                        </div>
                                    }

                                    <div className="row mt-2">
                                        
                                        {title.sourceType !== 0 &&
                                            <span className="TMDB-type-tag-details font-weight-600 mr-2">
                                                <FormattedMessage id='project.global.type.TMDB'/>
                                            </span>
                                        }
                                        
                                        <span className="title-type-tag-details font-weight-600 mr-2">
                                            {type === 0 ?
                                                <FormattedMessage id='project.global.type.movie'/>
                                            :
                                                <FormattedMessage id='project.global.type.tv'/>
                                            }
                                        </span>
                                        
                                        {title.genres.map(genre => 
                                            <span className="title-genre-tag mr-2">
                                                <FormattedMessage id={getGenre(genre.id, genre.externalId)}/>
                                            </span>
                                        )}

                                    </div>

                                </div>

                                <div className="col-4 p-0">
                                    {isAdmin && title && title.id && title.id !== -1 &&
                                        <div className="row admin-panel w-100 mb-2 pr-3">
                                            {isAdmin && title.setForDelete && 
                                                <span className="deletion-tag mb-1 d-block m-float-right"><FormattedMessage id='project.titles.setForDeletion'/></span>
                                            }
                                            {title.visible &&
                                                <button className="btn btn-primary btn-sm" type="button" title="ChangeSource"
                                                        data-toggle="modal" data-target={"#" + (title.sourceType === 0 ? changeSourceExternalModal : changeSourceInternalModal)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-gear-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                                                    </svg>
                                                </button>
                                            }
                                            {title.sourceType === 0 && !title.setForDelete &&
                                                <button className="btn btn-danger btn-sm ml-2" type="button" title="Delete"
                                                    data-toggle="modal" data-target={"#" + deleteModal}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                    </svg>
                                                </button>
                                            }
                                            <button className="btn btn-success btn-sm ml-2" type="button" title="Edit"
                                                    onClick={() => history.push(location + '/update')}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                </svg>
                                            </button>
                                            {!title.visible &&
                                                <button className="btn btn-secondary btn-sm ml-2" type="button" title="MakePublic"
                                                    data-toggle="modal" data-target={"#" + makePublicModal}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-eye-slash-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="m10.79 12.912-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                                        <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829zm3.171 6-12-12 .708-.708 12 12-.708.708z"/>
                                                    </svg>
                                                </button>
                                            }
                                        </div>
                                    }
                                    <div className="row admin-panel w-100 stars-general-rating">
                                        <div className="col-7 p-0 mr-1">
                                            <div className="row align-rating-right">
                                                <ReactStars size={50} count={1} activeColor="gold" value={1}
                                                    isHalf={false} edit={false} />
                                                <span className="ml-1 stars-general-rating-text">{Math.round(avgRating * 10) / 10}</span>
                                                <span className="stars-general-rating-outof light-gray-dark font-weight-600">/ 10</span>
                                                
                                            </div>
                                            <div className="row p-0 align-rating-right m-0">
                                                <span className="stars-general-rating-number light-gray-text font-weight-600">({ratingCount +" "} <FormattedMessage id='project.titles.info.avgRating.ratings'/>)</span>
                                            </div>
                                        </div>
                                        {isLoggedIn && 
                                            <span className="noselect stars-general-rating-text">|</span>
                                        }
                                        {isLoggedIn  && myReview !== null &&
                                            <div className="noselect col-4 rating-input-rate-container p-0 pl-1 link-pointer"
                                                onClick={() => handleRatingInputOpenClose(true)}>
                                                <div className="react-start">
                                                    <ReactStars size={50} count={1} activeColor="#632DB6" value={1}
                                                        isHalf={false} edit={false} />
                                                </div>
                                                <div className="row rating-input-rate-col ml-1 mt-1">
                                                    <span className="rating-input-rate-col-rating">{rating}</span>
                                                    <span className="rating-input-rate-col-text stars-general-rating-outof light-gray-dark font-weight-600">
                                                        <FormattedMessage id="project.titles.info.avgRating.you"/>
                                                    </span>
                                                </div>
                                            </div>
                                        }
                                        {isLoggedIn && myReview === null &&
                                            <div className="noselect col-2 new-review-button p-0 link-pointer">
                                                <button className="btn btn-sm p-0 ml-2 no-btn-outline" type="button" title="MakePublic"
                                                    onClick={() => handleRatingInputOpenClose(false)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-plus-circle-fill middle-v-button-icon" viewBox="0 0 16 16">
                                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                                </svg>
                                                </button>
                                            </div>
                                        }
                                    </div>
                                    <div className="rating-input" ref={node => ratingInput = node}>
                                        <span>
                                            <ReactStars size={24} count={10} activeColor="gold" color="white" value={rating}
                                            isHalf={false} edit={true} classNames="noselect" onChange={(newRating) => {handleNewRating(newRating)}}/>
                                        </span>
                                        <span className="rating-input-line pl-2">|</span>
                                        <button className="btn p-0 pl-2 no-btn-outline" type="button" 
                                            onClick={() => handleRemoveRating()}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle-fill rating-input-button" viewBox="0 0 16 16">
                                                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div className="ml-4">
                        <div className="row mt-4">
                            <span className="title-pre-title-box">
                            </span>
                            <span className="title-overview-title">
                                <FormattedMessage id='project.titles.info.overview'/>
                            </span>
                            <span className="mt-2 w-100 align-text-justify mr-5">
                                {title.description ? title.description : 
                                    title.originalDescription ? title.originalDescription : 
                                        <FormattedMessage id='project.titles.info.noData'/>}
                            </span>
                        </div>

                        {seasons && seasons.length !== 0 &&
                            <div >
                                <div className="row mt-5">
                                    <span className="title-pre-title-box"> </span>
                                    <span className="title-overview-title">
                                        <FormattedMessage id='project.titles.info.lastSeason'/>
                                    </span>
                                </div>

                                <div className="row mt-3">
                                    <Seasons seasons={seasons} titleId={external ? title.externalId : title.id} preview={true} external={external} lastSeason={true}/>
                                </div>

                                <div className="row">
                                    <Link className="link-bold" to={external ? 
                                            "/tmdb/tvshows/" + title.externalId + "/seasons"
                                        :
                                            "/tvshows/" + title.id + "/seasons"
                                        }>
                                        <FormattedMessage id='project.titles.buttons.seeAllSeasons'/>
                                    </Link>
                                </div>

                            </div>
                        }
                    </div>
                </div>
                
            </div>

            <button ref={node => modal = node} className="btn btn-primary btn-sm" hidden={true} type="button" title="ChangeSource" 
                data-toggle="modal" data-target={"#" + deleteReviewModal}> </button>
            
            <AdminOptionsModal id={deleteModal} type={0} 
                onConfirm={() => dispatch(actions.setForDeletion(title.id, () => history.go(0), null))} />

            <AdminOptionsModal id={makePublicModal} type={1} 
                onConfirm={() => dispatch(actions.makePublic(title.id, () => history.go(0), null))} />

            <AdminOptionsModal id={changeSourceInternalModal} type={2}
                onConfirm={() => dispatch(actions.changeSourceType(title.id, () => history.go(0), null))} />

            <AdminOptionsModal id={changeSourceExternalModal} type={3} 
                onConfirm={() => dispatch(actions.changeSourceType(title.id, () => history.go(0), null))} />

            <DeleteReviewModal id={deleteReviewModal} onConfirm={() => doRemoveDispatch()} />

        </div>
    );

}

TitleCommonInfo.propTypes = {
    title: PropTypes.object.isRequired,
    type: PropTypes.number.isRequired,
    seasons: PropTypes.array.isRequired,
    external: PropTypes.bool.isRequired,
}

export default TitleCommonInfo;