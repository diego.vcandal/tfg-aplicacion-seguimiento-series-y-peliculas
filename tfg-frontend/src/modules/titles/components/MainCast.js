import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {Link, useLocation} from 'react-router-dom';

import * as selectors from '../selectors';
import * as castAndCrewActions from '../castAndCrewActions';
import HorizontalCast from './Entity/HorizontalCast';

const MainCast = ({type}) => {

    const dispatch = useDispatch();
    const location = useLocation().pathname;

    const foundMainCast = useSelector(selectors.getFoundCast);
    const foundTitle = useSelector(selectors.getFoundTitle);
    const [load, setLoad]  = useState(false);

    useEffect(() => {

        if (foundTitle.sourceType === 0){
            dispatch(castAndCrewActions.findMainCast(foundTitle.id, () => setLoad(true)));
        } else {

            if (type === 0)
                dispatch(castAndCrewActions.findMainExternalMovieCast(foundTitle.externalId, () => setLoad(true)));
            else
                dispatch(castAndCrewActions.findMainExternalTVShowCast(foundTitle.externalId, () => setLoad(true)));
        } 

        return () => dispatch(castAndCrewActions.clearFoundCast());

    }, [foundTitle, type, dispatch]);

    if (!foundMainCast) {
        return null;
    }

    return (

        <div className=" mt-4 w-90 center-element">
            {foundMainCast.length > 0 &&
                <div className="center-element">
                    <div className="row ml-5 mb-2">
                        <span className="title-pre-title-box"></span>
                        <h3 className="m-0 main-cast-title"><FormattedMessage id='project.titles.info.cast.mainCast'/></h3>
                            <Link className="link-bold m-float-right label-v-class mr-5" to={location + "/cast-crew"}>
                                <FormattedMessage id='project.titles.info.cast.seeAll'/>
                            </Link>
                    </div>

                    {load && <HorizontalCast cast={foundMainCast} />}
                </div>
            }
        </div>
    );

}

export default MainCast;