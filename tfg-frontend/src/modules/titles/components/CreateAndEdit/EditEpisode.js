/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams, useHistory} from 'react-router-dom';

import {Errors} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';

import LooseChanges from './Modals/LooseChanges';
import EditSeasonEpisodeTraductions from './EditSeasonEpisodeTraductions';
import CreateEpisode from './CreateEpisode';
import EditSeasonEpisodeImage from './EditSeasonEpisodeImage';
import EditEpisodeCast from './EditEpisodeCast';
import EditEpisodeCrew from './EditEpisodeCrew';

const EditEpisode = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    
    const [actualPanel, setActualPanel] = useState(0);
    const {episodeId} = useParams();
    const [backendErrors, setBackendErrors] = useState(null);
    const [madeChanges, setMadeChanges] = useState(false);   

    const foundEpisode = useSelector(selectors.getFoundEpisode);

    let dialog, selectedCode;
    let dialogId = "looseAllChanges";
    let list = [{id: 0, ref: null}, 
        {id: 1, ref: null}, 
        {id: 2, ref: null},
        {id: 3, ref: null},
        {id: 4, ref: null}];

    const baseClass = "list-group-item list-group-item-action col";

    useEffect(() => {

        const foundEpisode = Number(episodeId);

        if (!Number.isNaN(foundEpisode)) {
            dispatch(actions.findFullInternalEpisodeById(foundEpisode));
        }

        return () => dispatch(actions.clearFoundEpisode());

    }, [episodeId, dispatch]);

    
    const handleUpdatePanel = (code) => {
        selectedCode = code;

        if (madeChanges) 
            dialog.click();
        else
            onDialogConfirm();

    };

    if (!foundEpisode) {
        return null;
    }

    const onDialogConfirm = () => {
        setActualPanel(selectedCode);
        list.map(e => e.id === selectedCode ? e.ref.className = baseClass + " active" : e.ref.className = baseClass)
        setMadeChanges(false);
    };

    return (
        <div className="row mt-4">

            <div className="row w-100 mb-2 ml-0 ">

                {foundEpisode.sourceType !== 0 ?
                    <span className="TMDB-type-tag font-weight-600 mr-3 font-size-update-title-tag">
                        <FormattedMessage id='project.global.type.TMDB'/>
                    </span>
                :
                    null
                }

                <span className="font-size-update-title">
                    <FormattedMessage id='project.common.title.update'/>
                    {" - " + foundEpisode.tvshowOriginalTitle}
                    {" - " + foundEpisode.seasonOriginalTitle}
                    {!foundEpisode.title ? " - " + foundEpisode.originalTitle: " - " + foundEpisode.title}
                </span>

            </div>

            <div className="row w-100 mb-4 ml-0">
                <a className="link-primary link-bold font-size-update-title-tag" onClick={() => history.goBack()}>
                    ← <FormattedMessage id='project.common.return'/>
                </a>
            </div>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="side-update-menu-container shadow-container col-2 list-group">

                <button type="button" className={baseClass + " active"} ref={node => list[0].ref = node}
                    onClick={e => handleUpdatePanel(0)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.details'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[1].ref = node}
                    onClick={e => handleUpdatePanel(1)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.traductions'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[2].ref = node}
                    onClick={e => handleUpdatePanel(2)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.images'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[3].ref = node}
                    onClick={e => handleUpdatePanel(3)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.cast'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[4].ref = node}
                    onClick={e => handleUpdatePanel(4)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.crew'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

            </div>

            {actualPanel === 0 &&
                <CreateEpisode update={true} setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 1 &&
                <EditSeasonEpisodeTraductions madeChanges={madeChanges} setMadeChanges={setMadeChanges} type={1}/>
            }

            {actualPanel === 2 &&
                <EditSeasonEpisodeImage setMadeChanges={setMadeChanges} type={1}/>
            }

            {actualPanel === 3 &&
                <EditEpisodeCast />
            }

            {actualPanel === 4 &&
                <EditEpisodeCrew />
            }

            <button className="btn btn-danger button-align-right button-width-50" 
                ref={node => dialog = node} hidden={true}
                data-toggle="modal" data-target={"#" + dialogId}>
            </button >
            
            <LooseChanges id={dialogId} onConfirm={onDialogConfirm}></LooseChanges>

        </div>

    );

}

export default EditEpisode;