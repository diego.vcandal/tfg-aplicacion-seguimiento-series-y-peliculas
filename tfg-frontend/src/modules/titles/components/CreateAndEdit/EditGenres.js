import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory, useParams} from 'react-router-dom';

import {Errors} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';

import {Genres, getGenreNameCode, getFullGenre} from '../../util/Genres';

const EditGenres = ({setMadeChanges}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {movieId, tvshowId} = useParams();

    const foundTitle = useSelector(selectors.getFoundTitle);
    const [backendErrors, setBackendErrors] = useState(null);
    const [selectedGenre, setSelectedGenre] = useState(1); 
    const [genreList, setGenreList] = useState(foundTitle.genres); 
    
    let form;

    const handleSubmit = event => {

        event && event.preventDefault();
        
        if (form.checkValidity()) {

            foundTitle.genres = genreList;
            
            if (movieId) 
                dispatch(actions.updateMovie(foundTitle, () => history.go(0), errors => setBackendErrors(errors)))
            else if (tvshowId) 
                dispatch(actions.updateTVShow(foundTitle, () => history.go(0), errors => setBackendErrors(errors)))
            
        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    if (!foundTitle)
        return null;

    const handleAdd = () => {
        setMadeChanges(true);
        
        let foundGenre = genreList.find(g => g.id === Number(selectedGenre));
        if (!foundGenre) {
            let g = getFullGenre(Number(selectedGenre));
            
            setGenreList([...genreList, {
                id: g.id,
                externalId: g.externalId
            }])
        }

    };

    const getGenre = (genreId, genreExternalId) => {
        let g = getGenreNameCode(genreId, genreExternalId);
        if(g)
            return g.name;
    };

    const removeGenre = (genreId) => {
        setMadeChanges(true);
        setGenreList(genreList.filter(genre => genre.id !== genreId));
    };

    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col user-info" noValidate onSubmit={e => handleSubmit(e)}>


                    <div className="row shadow-container p-3 mb-3">
                        <div className="w-100 row mb-3 ml-0">
                            <div className="col-6 mr-3">
                                <label htmlFor="genres" className="col-12 title-label-color p-0" >
                                    <FormattedMessage id="project.titles.update.menu.genres"/>
                                </label>
                                <select id="genres" className="custom-select col-9 "
                                    value={null}
                                    onChange={e => setSelectedGenre(e.target.value)} >
                                    {Genres.map(type =>
                                        <FormattedMessage id={type.name}>
                                            {
                                                (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                            }
                                        </FormattedMessage>
                                    )}
                                </select>
                                <button type="button" className="btn btn-secondary float-right" 
                                    onClick={e => handleAdd(e)}>
                                    <FormattedMessage id="project.titles.create.buttons.add"/>
                                </button>
                            </div>
                        </div>

                        <div className="w-100 row mb-3 center-element ">
                            <div className="col">
                                <table className="table table-striped genre-table">
                                    <thead className="thead-light">
                                        <tr>
                                        <th className="col-2"><FormattedMessage id="project.titles.update.genres.id"/></th>
                                        <th className="col-2"><FormattedMessage id="project.titles.update.genres.idExternal"/></th>
                                        <th className="col-6"><FormattedMessage id="project.titles.update.genres.name"/></th>
                                        <th className="col-3"><FormattedMessage id="project.titles.update.genres.operations"/></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {genreList.map(genre =>
                                            <tr>
                                                <th scope="row">{genre.id}</th>
                                                <td>{genre.externalId}</td>
                                                <td><FormattedMessage id={getGenre(genre.id, genre.externalId)}/></td>
                                                <td>
                                                    <button className="btn btn-danger btn-sm" type="button" title="Delete"
                                                        onClick={() => removeGenre(genre.id)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x" viewBox="0 0 16 16">
                                                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                        </svg>
                                                    </button>
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col title-submit-btn-container p-0">
                                <div>
                                    <button type="submit" className="btn btn-success float-right">
                                        <FormattedMessage id="project.titles.create.buttons.add"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    );

}

export default EditGenres;