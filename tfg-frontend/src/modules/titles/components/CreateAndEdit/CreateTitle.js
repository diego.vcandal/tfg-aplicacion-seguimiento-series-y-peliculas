import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';
import PropTypes from 'prop-types';

import {TitleTypes} from '../../util/TitleTypes';
import {AvailableLanguages} from '../../../language/AvailableLanguages';
import CreateMovieDetails from './CreateMovieDetails';
import CreateTVShowDetails from './CreateTVShowDetails';

const CreateTitle = ({update, type, setMadeChanges}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const foundTitle = useSelector(selectors.getFoundTitle);
    const canUpdate = update && foundTitle;

    const [title, setTitle]  = useState('');
    const [originalTitle, setOriginalTitle]  = useState(canUpdate ? foundTitle.originalTitle : '');
    const [language, setLanguage]  = useState("0");
    const [isOriginalLanguage, setIsOriginalLanguage]  = useState(false);
    const [description, setDescription]  = useState(canUpdate ? foundTitle.originalDescription : '');
    const [companyName, setCompanyName]  = useState(canUpdate ? foundTitle.companyName : '');
    const [externalId, setExternalId]  = useState(canUpdate ? foundTitle.externalId : '');
    const [year, setYear]  = useState(canUpdate ? foundTitle.year : '');
    const [backendErrors, setBackendErrors] = useState(null);

    let form;

    let foundFirstAirDate = new Date().getTime();
    let foundLastAirDate = new Date().getTime();

    if (canUpdate) {
        foundFirstAirDate = foundTitle.firstAirDate && foundTitle.firstAirDate !== null ? 
            foundTitle.firstAirDate : '';
        foundLastAirDate = foundTitle.lastAirDate && foundTitle.lastAirDate !== null ? 
            foundTitle.lastAirDate : '';
    }

    const [isDisabled, setDisabled]  = useState(false);
    const [isMovie, setIsMovie]  = useState(canUpdate ? (type === 0 ? true : false) : true);

    const [movieDuration, setMovieDuration] = useState(canUpdate ? foundTitle.duration : 0);
    const [firstAirDate, setFirstAirDate] = useState(foundFirstAirDate);
    const [lastAirDate, setLastAirDate] = useState(foundLastAirDate);

    let classCreate="center-element w-80 mt-4";
    let classUpdate="center-element col-9 mt-4";

    const handleSubmit = event => {
        
        event && event.preventDefault();
        
        if (form.checkValidity()) {

            if (!update) {

                let titleResult = {
                    externalId: externalId,
                    originalTitle: isOriginalLanguage ? title : originalTitle,
                    originalDescription: isOriginalLanguage ? description : "",
                    companyName: companyName,
                    year: year,
                    traductions: [
                        {
                            idLocale: language,
                            titleName: title,
                            description: description
                        }
                    ]
                }
    
                if (isMovie) {
                    titleResult = {...titleResult, 
                        duration: Number(movieDuration)}
    
                    dispatch(actions.createMovie(titleResult, (id) => history.push('/movies/' + id), errors => setBackendErrors(errors)))
                } else {
                    titleResult = {...titleResult, 
                        firstAirDate: Number(firstAirDate),
                        lastAirDate: Number(lastAirDate)}
    
                    dispatch(actions.createTVShow(titleResult, (id) => history.push('/tvshows/' + id), errors => setBackendErrors(errors)))
                }

            } else {

                foundTitle.originalTitle = originalTitle;
                foundTitle.companyName = companyName;
                foundTitle.year = year;
                foundTitle.originalDescription = description;
    
                if (isMovie) {
                    foundTitle.duration = Number(movieDuration);
    
                    dispatch(actions.updateMovie(foundTitle, () => history.go(0), errors => setBackendErrors(errors)))
                } else {
                    foundTitle.firstAirDate = firstAirDate !== "" ? Number(firstAirDate): null;
                    foundTitle.lastAirDate = lastAirDate !== "" ? Number(lastAirDate): null;

                    dispatch(actions.updateTVShow(foundTitle, () => history.go(0), errors => setBackendErrors(errors)))
                }
            }
            
        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const titleInputOnChange = title => {
        setTitle(title);

        if (isDisabled) 
            setOriginalTitle(title);

    }


    const handleMarkOriginal = event => {
        setIsOriginalLanguage(event);
        setDisabled(event);

        if (event) 
            setOriginalTitle(title);

    }

    return (
        <div className={update ? classUpdate : classCreate}>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col user-info" noValidate onSubmit={e => handleSubmit(e)}>
 

                    {!update &&
                        <div className="row shadow-container mb-3">
                            <FormattedMessage id="project.titles.create.title">
                                {
                                    (msg) => <input type="text" id="userName" className="col form-control title-input"
                                    onChange={e => titleInputOnChange(e.target.value)}
                                    autoFocus
                                    value={title}
                                    placeholder={msg}
                                    required />
                                }
                            </FormattedMessage>

                            <div>
                                <div className="vertical-hr"/>
                            </div>

                            <select className="col-2 custom-select no-border-select title-input "
                                onChange={e => setIsMovie(Number(e.target.value) === 0)} >
                                {TitleTypes.map(type => 
                                    <FormattedMessage id={type.name}>
                                        {
                                            (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                        }
                                    </FormattedMessage>
                                )}
                            </select>
                        </div>
                    }

                    <div className="row shadow-container p-3 mb-3">
                        <div className="w-100 row mb-3">
                            <div className="col-6 mr-3">
                                <label htmlFor="originalTitle" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.originalTitle"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input type="text" id="originalTitle" className="form-control "
                                        value={originalTitle}
                                        onChange={e => {setOriginalTitle(e.target.value); canUpdate && setMadeChanges(true)}}
                                        required disabled={isDisabled} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>

                            {!update &&
                                <div className="col-3 mr-1">
                                    <label htmlFor="language" className="col-12 title-label-color">
                                        <FormattedMessage id="project.titles.create.language"/>
                                    </label>
                                    <select id="language" className="custom-select col-11 ml-3"
                                        value={language}
                                        onChange={e => setLanguage(e.target.value)} >
                                        {AvailableLanguages.map(type => 
                                            <FormattedMessage id={type.name}>
                                                {
                                                    (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                                }
                                            </FormattedMessage>
                                        )}
                                    </select>
                                </div>
                            }

                            {!update &&
                                <div className="col-2 ml-5 form-check">
                                    <div className="row title-checkbox custom-checkbox">
                                        <input type="checkbox" id="checkbox-language" className="custom-control-input" 
                                            checked={isOriginalLanguage}
                                            onChange={e => handleMarkOriginal(e.target.checked)} />

                                        <label htmlFor="checkbox-language" className="custom-control-label title-label-color">
                                            <FormattedMessage id="project.titles.create.isOriginalLanguage"/>
                                        </label>
                                    </div>
                                </div>
                            }
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-3 mb-3">
                                <label htmlFor="externalId" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.externalId"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <input type="number" id="externalId" className="form-control"
                                        value={externalId}
                                        onChange={e => setExternalId(e.target.value)}
                                        required min="1"
                                        disabled={update} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>

                            <div className="col-3 mb-3 mr-3">
                                <label htmlFor="companyName" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.companyName"/>
                                </label>
                                <div className="col align-input-center">
                                    <input  id="companyName" className="form-control"
                                        value={companyName}
                                        onChange={e => {setCompanyName(e.target.value); canUpdate && setMadeChanges(true)}}
                                        required />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>

                            <div className="col-3 mb-3">
                                <label htmlFor="year" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.year"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <input type="number" id="year" className="form-control"
                                        value={year}
                                        onChange={e => {setYear(e.target.value); canUpdate && setMadeChanges(true)}}
                                        required min="1895"/>
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-9 mr-3 mt-3">
                                <label htmlFor="titleDescription" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.description"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <textarea className="form-control" id="titleDescription" rows="5" 
                                        value={description}
                                        maxLength="500"
                                        onChange={e => {setDescription(e.target.value); canUpdate && setMadeChanges(true)}}></textarea>
                                </div>
                            </div>
                        

                            <div className="col title-submit-btn-container p-0">

                                    {!update &&
                                        <button type="button" className="btn btn-secondary ml-4" 
                                                onClick={() => {history.goBack()} }>
                                            <FormattedMessage id="project.global.buttons.cancel"/>
                                        </button>
                                    }
                                    <button type="submit" className="btn btn-success float-right">
                                        <FormattedMessage id="project.titles.create.buttons.add"/>
                                    </button>
                            </div>
                        </div>
                    </div>

                    {isMovie && 
                        <CreateMovieDetails movieDuration={movieDuration} setMovieDuration={setMovieDuration} 
                            setMadeChanges={setMadeChanges} canUpdate={canUpdate}/>
                    }

                    {!isMovie && 
                        <CreateTVShowDetails firstAirDate={firstAirDate} setFirstAirDate={setFirstAirDate} 
                            lastAirDate={lastAirDate} setLastAirDate={setLastAirDate} setMadeChanges={setMadeChanges}
                            canUpdate={canUpdate}/>
                    }

            </form>
        </div>
    );

}

CreateTitle.propTypes = {
    update: PropTypes.bool.isRequired,
    type: PropTypes.number.isRequired,
    setMadeChanges: PropTypes.func.isRequired
}

export default CreateTitle;