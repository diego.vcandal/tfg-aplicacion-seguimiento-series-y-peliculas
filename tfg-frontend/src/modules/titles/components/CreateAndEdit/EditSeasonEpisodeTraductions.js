import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {Errors} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';

import {AvailableLanguages} from '../../../language/AvailableLanguages';
import LooseChanges from './Modals/LooseChanges';

const EditSeasonEpisodeTraductions = ({madeChanges, setMadeChanges, type}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const season = useSelector(selectors.getFoundSeason);
    const episode = useSelector(selectors.getFoundEpisode);
    const [backendErrors, setBackendErrors] = useState(null);

    let firstLanguage;

    if (type === 0) {
        firstLanguage = season && season.traductions && season.traductions.length > 0 ? season.traductions[0] : null;
    }

    if (type === 1) {
        firstLanguage = episode && episode.traductions && episode.traductions.length > 0 ? episode.traductions[0] : null;
    }

    const [language, setLanguage] = useState(firstLanguage ? firstLanguage.idLocale : -1);
    const [originalTitle, setOriginalTitle] = useState(type === 0 ? season.originalTitle : episode.originalTitle);
    const [externalId, setExternalId] = useState(type === 0 ? season.externalId : episode.externalId);
    const [title, setTitle] = useState(firstLanguage ? firstLanguage.title : '');
    const [description, setDescription] = useState(firstLanguage ? firstLanguage.description : '');   

    let form, dialog;
    let dialogId = "looseTraductionChanges";
    let dialogOpened = false;
    let selectedLanguage = firstLanguage ? firstLanguage.idLocale : -1;

    const handleSubmit = event => {
       
        if (dialogOpened) {
            return;
        }

        event && event.preventDefault();
        
        if (form.checkValidity()) {

            if (type === 0) {

                let trad = season.traductions.find(t => t.idLocale === Number(language));

                if (trad) {
                    trad.title = title;
                    trad.description = description;
                } else {
                    season.traductions.push({
                        idLocale: Number(language),
                        title: title,
                        description: description
                    });
                }

                dispatch(actions.updateSeason(season, () => history.go(0), errors => setBackendErrors(errors)))
            }
        
            if (type === 1) {

                let trad = episode.traductions.find(t => t.idLocale === Number(language));

                if (trad) {
                    trad.titleName = title;
                    trad.description = description;
                } else {
                    episode.traductions.push({
                        idLocale: Number(language),
                        title: title,
                        description: description
                    });
                }

                dispatch(actions.updateEpisode(episode, () => history.go(0), errors => setBackendErrors(errors)))
            }
            
           
        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    if (!season && !episode)
        return null;

    let locales = [];
    
    if (type === 0) {
        season.traductions.forEach(t => {
            locales.push(AvailableLanguages.find(l => t.idLocale === l.id));
        });
    }

    if (type === 1) {
        episode.traductions.forEach(t => {
            locales.push(AvailableLanguages.find(l => t.idLocale === l.id));
        });
    }

    const handleOnChangedTraduction = (id) => {

        selectedLanguage = id;
        dialogOpened = true;

        if (madeChanges) 
            dialog.click();
        else
            onDialogConfirm();

    };

    const onDialogConfirm = () => {

        if (selectedLanguage < 0)
           return; 
        
        let traductions = type === 0 ? season.traductions : episode.traductions;
        let actualTraduction = traductions.find(t => t.idLocale === Number(selectedLanguage));
        
        if (actualTraduction) {
            setTitle(actualTraduction.title);
            setDescription(actualTraduction.description);
        } else {
            setTitle('');
            setDescription('');
        }
        
        setLanguage(selectedLanguage);
        setMadeChanges(false);
        
    };

    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="w-100 row mb-3">
                <div className="col-3 m-float-right pr-0">
                    <select id="language" className="custom-select col-11 "
                        value={language}
                        onChange={e => handleOnChangedTraduction(e.target.value)} >
                        {AvailableLanguages.map(type =>
                            <FormattedMessage id={type.name}>
                                {
                                    (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                }
                            </FormattedMessage>
                        )}
                    </select>
                </div>
            </div>

            <form ref={node => form = node}
                className="needs-validation col user-info" noValidate onSubmit={e => handleSubmit(e)}>


                    <div className="row shadow-container p-3 mb-3">
                        <div className="w-100 row mb-3">
                            <div className="col-6 mr-3">
                                <label htmlFor="originalTitle" className="col-12 title-label-color" >
                                    <FormattedMessage id="project.titles.create.originalTitle"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input type="text" id="originalTitle" className="form-control "
                                        value={originalTitle}
                                        onChange={e => setOriginalTitle(e.target.value)}
                                        required disabled={true} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>

                            <div className="col-3 mb-3">
                                <label htmlFor="externalId" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.externalId"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <input type="number" id="externalId" className="form-control"
                                        value={externalId}
                                        onChange={e => setExternalId(e.target.value)}
                                        required min="1"
                                        disabled={true} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-6 mr-3">
                                <label htmlFor="title" className="col-12 title-label-color" >
                                    <FormattedMessage id="project.titles.create.titletraduction"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input type="text" id="title" className="form-control "
                                        value={title}
                                        onChange={e => {setTitle(e.target.value); setMadeChanges(true)}}
                                        required />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-9 mr-3 mt-3">
                                <label htmlFor="titleDescription" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.descriptiontraduction"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <textarea className="form-control" id="titleDescription" rows="5" 
                                        value={description}
                                        maxLength="500"
                                        onChange={e => {setDescription(e.target.value); setMadeChanges(true)}}>
                                    </textarea>
                                </div>
                            </div>
                        

                            <div className="col title-submit-btn-container p-0">
                                <div>
                                    <button type="submit" className="btn btn-success float-right">
                                        <FormattedMessage id="project.titles.create.buttons.add"/>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <button className="btn btn-danger button-align-right button-width-50" 
                        ref={node => dialog = node} hidden={true}
                        data-toggle="modal" data-target={"#" + dialogId}
                        onClick={e => e.preventDefault()}>
                    </button>

                    <LooseChanges id={dialogId} onConfirm={onDialogConfirm} onCancel={() => dialogOpened = false}></LooseChanges>
            </form>
        </div>
    );

}

export default EditSeasonEpisodeTraductions;