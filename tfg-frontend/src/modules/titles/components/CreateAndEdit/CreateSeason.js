import React, {useState,useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory, useParams} from 'react-router-dom';

import {Errors, PageNotFound} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';
import PropTypes from 'prop-types';

import {AvailableLanguages} from '../../../language/AvailableLanguages';

const CreateSeason = ({update, setMadeChanges}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const {tvshowId} = useParams();

    const foundTitle = useSelector(selectors.getFoundTitle);
    const season = useSelector(selectors.getFoundSeason);
    const canUpdate = update && season;

    const [title, setTitle]  = useState('');
    const [originalTitle, setOriginalTitle]  = useState(canUpdate ? season.originalTitle : '');
    const [language, setLanguage]  = useState("0");
    const [isOriginalLanguage, setIsOriginalLanguage]  = useState(false);
    const [description, setDescription]  = useState(canUpdate ? season.originalDescription : '');
    const [externalId, setExternalId]  = useState(canUpdate ? season.externalId : '');
    const [seasonNumber, setSeasonNumber]  = useState(0);
    const [seasonNumberInitial, setSeasonNumberInitial]  = useState(true);
    const [backendErrors, setBackendErrors] = useState(null);

    let form;

    let foundFirstAirDate = new Date().getTime();
    let foundLastAirDate = new Date().getTime();

    if (canUpdate) {
        foundFirstAirDate = season.firstAirDate && season.firstAirDate !== null ? 
            season.firstAirDate : '';
        foundLastAirDate = season.lastAirDate && season.lastAirDate !== null ? 
            season.lastAirDate : '';
    }

    const [isDisabled, setDisabled]  = useState(false);

    const [firstAirDate, setFirstAirDate] = useState(foundFirstAirDate);
    const [lastAirDate, setLastAirDate] = useState(foundLastAirDate);

    let classCreate="center-element w-80 mt-4";
    let classUpdate="center-element col-9 mt-4";

    useEffect(() => {
        
        const foundId = Number(tvshowId);
        if (!Number.isNaN(foundId)) {
            dispatch(actions.findTVShowById(foundId));
        }
        
        return () => dispatch(actions.clearFoundTitle());

    }, [tvshowId, dispatch]);


    if (!foundTitle) {
        return <PageNotFound/>
    }

    const handleSubmit = event => {
        
        event && event.preventDefault();
        
        if (form.checkValidity()) {

            if (!update) {

                let titleResult = {
                    externalId: externalId,
                    seasonNumber: seasonNumber === 0 && seasonNumberInitial ?  tempInicial : seasonNumber,
                    originalTitle: isOriginalLanguage ? title : originalTitle,
                    originalDescription: isOriginalLanguage ? description : "",
                    firstAirDate: Number(firstAirDate),
                    lastAirDate: Number(lastAirDate),
                    traductions: [
                        {
                            idLocale: language,
                            title: title,
                            description: description
                        }
                    ]
                }
    
                dispatch(actions.createSeason(foundTitle.id, titleResult, () => history.push(`/tvshows/${foundTitle.id}/seasons`), errors => setBackendErrors(errors)))

            } else {
                
                season.originalTitle = originalTitle;
                season.originalDescription = description;
                season.firstAirDate= firstAirDate !== "" ? Number(firstAirDate): null;
                season.lastAirDate= lastAirDate !== "" ? Number(lastAirDate): null;
                
                dispatch(actions.updateSeason(season, () => history.go(0), errors => setBackendErrors(errors)))
                
            }
            
        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const titleInputOnChange = title => {
        setTitle(title);

        if (isDisabled) 
            setOriginalTitle(title);

    }

    const handleMarkOriginal = event => {
        setIsOriginalLanguage(event);
        setDisabled(event);

        if (event) 
            setOriginalTitle(title);

    }

    const handleSeasonNumber = value =>{
        setSeasonNumberInitial(false);
        setSeasonNumber(value);
    }

    const handleParseDate = (date) => {
        let day = date.getDate();
        let month = date.getMonth() + 1;
        return date.getFullYear() + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;
    }

    let tempInicial = foundTitle.seasonNumber + 1;

    return (
        <div className={update ? classUpdate : classCreate}>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col user-info" noValidate onSubmit={e => handleSubmit(e)}>
 

                    {!update &&
                        <div className="row shadow-container mb-3">
                            <FormattedMessage id="project.titles.create.season">
                                {
                                    (msg) => <input type="text" id="userName" className="col form-control title-input"
                                    onChange={e => titleInputOnChange(e.target.value)}
                                    autoFocus
                                    value={title}
                                    placeholder={msg + " - " + tempInicial}
                                    required />
                                }
                            </FormattedMessage>

                            
                        </div>
                    }

                    <div className="row shadow-container p-3 mb-3">
                        <div className="w-100 row mb-3">
                            <div className="col-6 mr-3">
                                <label htmlFor="originalTitle" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.originalTitle"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input type="text" id="originalTitle" className="form-control "
                                        value={originalTitle}
                                        onChange={e => {setOriginalTitle(e.target.value); canUpdate && setMadeChanges(true)}}
                                        required disabled={isDisabled} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>

                            {!update &&
                                <div className="col-3 mr-1">
                                    <label htmlFor="language" className="col-12 title-label-color">
                                        <FormattedMessage id="project.titles.create.language"/>
                                    </label>
                                    <select id="language" className="custom-select col-11 ml-3"
                                        value={language}
                                        onChange={e => setLanguage(e.target.value)} >
                                        {AvailableLanguages.map(type => 
                                            <FormattedMessage id={type.name}>
                                                {
                                                    (msg) => <option key={type.id} value={type.id}>{msg}</option>
                                                }
                                            </FormattedMessage>
                                        )}
                                    </select>
                                </div>
                            }

                            {!update &&
                                <div className="col-2 ml-5 form-check">
                                    <div className="row title-checkbox custom-checkbox">
                                        <input type="checkbox" id="checkbox-language" className="custom-control-input" 
                                            checked={isOriginalLanguage}
                                            onChange={e => handleMarkOriginal(e.target.checked)} />

                                        <label htmlFor="checkbox-language" className="custom-control-label title-label-color">
                                            <FormattedMessage id="project.titles.create.isOriginalLanguage"/>
                                        </label>
                                    </div>
                                </div>
                            }
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-3 mb-3">
                                <label htmlFor="externalId" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.externalId"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <input type="number" id="externalId" className="form-control"
                                        value={externalId}
                                        onChange={e => setExternalId(e.target.value)}
                                        required min="1"
                                        disabled={update} />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>

                            <div className="col-3 mb-3 mr-3">
                                <label htmlFor="seasonNumber" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.seasonNumber"/>
                                </label>
                                <div className="col align-input-center">
                                    <input id="seasonNumber" className="form-control"
                                        value={seasonNumber === 0 && seasonNumberInitial ? foundTitle ? tempInicial : 0 : seasonNumber} type="number"
                                        onChange={e => {handleSeasonNumber(e.target.value); canUpdate && setMadeChanges(true)}}
                                        required min={0} disabled={update}  />
                                    <div className="invalid-feedback">
                                        <FormattedMessage id='project.global.validator.required'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-3 mr-3">
                                <label htmlFor="firstAirDate" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.tvshow.firstAirDate"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input className="form-control" type="date" 
                                        value={handleParseDate(new Date(firstAirDate))} 
                                        onChange={e => {setFirstAirDate(new Date(e.target.value).getTime()); canUpdate && setMadeChanges(true)}}
                                        id="firstAirDate" />
                                </div>
                            </div>

                            <div className="col-3 mr-3">
                                <label htmlFor="lastAirDate" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.tvshow.lastAirDate"/>
                                </label>
                                <div className="col-12 align-input-center">
                                    <input className="form-control" type="date" 
                                        value={handleParseDate(new Date(lastAirDate))} 
                                        onChange={e => {setLastAirDate(new Date(e.target.value).getTime()); canUpdate && setMadeChanges(true)}}
                                        id="lastAirDate" />
                                </div>
                            </div>
                        </div>

                        <div className="w-100 row mb-3">
                            <div className="col-9 mr-3 mt-3">
                                <label htmlFor="titleDescription" className="col-12 title-label-color">
                                    <FormattedMessage id="project.titles.create.description"/>
                                </label>
                                <div className="col-10 align-input-center">
                                    <textarea className="form-control" id="titleDescription" rows="5" 
                                        value={description}
                                        maxLength="500"
                                        onChange={e => {setDescription(e.target.value); canUpdate && setMadeChanges(true)}}></textarea>
                                </div>
                            </div>
                        

                            <div className="col title-submit-btn-container p-0">

                                    {!update &&
                                        <button type="button" className="btn btn-secondary ml-4" 
                                                onClick={() => {history.goBack()} }>
                                            <FormattedMessage id="project.global.buttons.cancel"/>
                                        </button>
                                    }
                                    <button type="submit" className="btn btn-success float-right">
                                        <FormattedMessage id="project.titles.create.buttons.add"/>
                                    </button>
                            </div>
                        </div>
                    </div>

            </form>
        </div>
    );

}

CreateSeason.propTypes = {
    update: PropTypes.bool.isRequired,
    setMadeChanges: PropTypes.func.isRequired
}

export default CreateSeason;