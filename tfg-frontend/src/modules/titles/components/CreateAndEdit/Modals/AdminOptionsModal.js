import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';

const AdminOptionsModal = ({id, type, onConfirm}) => (

    <div id={id} className="modal fade" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">
                        {type === 0 && <FormattedMessage id="project.global.dialog.adminOptions.delete.Title"/>}
                        {type === 1 && <FormattedMessage id="project.global.dialog.adminOptions.makePublic.Title"/>}
                        {(type === 2 || type === 3) && <FormattedMessage id="project.global.dialog.adminOptions.changeSource.Title"/>}
                    </h5>
                </div>
                <div className="modal-body">
                    <p>
                        {type === 0 && <FormattedMessage id="project.global.dialog.adminOptions.delete.Body"/>}
                        {type === 1 && <FormattedMessage id="project.global.dialog.adminOptions.makePublic.Body"/>}
                        {type === 2 && <FormattedMessage id="project.global.dialog.adminOptions.changeSource.internal.Body"/>}
                        {type === 3 && <FormattedMessage id="project.global.dialog.adminOptions.changeSource.external.Body"/>}
                    </p>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" 
                        data-dismiss="modal">
                        <FormattedMessage id="project.global.buttons.cancel"/>
                    </button>
                    <button type="button" className="btn btn-danger" 
                        data-dismiss="modal"
                        onClick={onConfirm}>
                        <FormattedMessage id="project.global.buttons.ok"/>
                    </button>
                </div>
            </div>
        </div>
    </div>

);

AdminOptionsModal.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.number.isRequired,
    onConfirm: PropTypes.func.isRequired,
}

export default AdminOptionsModal;
