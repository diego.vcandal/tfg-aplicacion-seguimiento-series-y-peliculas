import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';

const DeleteReviewModal = ({id, onConfirm}) => (

    <div id={id} className="modal fade" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">
                        <FormattedMessage id="project.global.dialog.review.delete.Title"/>
                    </h5>
                </div>
                <div className="modal-body">
                    <p>
                        <FormattedMessage id="project.global.dialog.review.delete.Body"/>
                    </p>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" 
                        data-dismiss="modal">
                        <FormattedMessage id="project.global.buttons.cancel"/>
                    </button>
                    <button type="button" className="btn btn-danger" 
                        data-dismiss="modal"
                        onClick={onConfirm}>
                        <FormattedMessage id="project.global.buttons.ok"/>
                    </button>
                </div>
            </div>
        </div>
    </div>

);

DeleteReviewModal.propTypes = {
    id: PropTypes.string.isRequired,
    onConfirm: PropTypes.func.isRequired,
}

export default DeleteReviewModal;
