import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';

const LooseChanges = ({id, onConfirm, onCancel}) => (

    <div id={id} className="modal fade" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title"><FormattedMessage id="project.global.dialog.looseChanges.Title"/></h5>
                </div>
                <div className="modal-body">
                    <p><FormattedMessage id="project.global.dialog.looseChanges.Body"/></p>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" 
                        data-dismiss="modal"
                        onClick={onCancel}>
                        <FormattedMessage id="project.global.buttons.cancel"/>
                    </button>
                    <button type="button" className="btn btn-danger" 
                        data-dismiss="modal"
                        onClick={onConfirm}>
                        <FormattedMessage id="project.global.buttons.ok"/>
                    </button>
                </div>
            </div>
        </div>
    </div>

);

LooseChanges.propTypes = {
    onConfirm: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
}

export default LooseChanges;
