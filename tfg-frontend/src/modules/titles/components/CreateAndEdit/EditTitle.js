/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams, useHistory} from 'react-router-dom';

import {Errors} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';

import PropTypes from 'prop-types';

import CreateTitle from './CreateTitle';
import EditTraductions from './EditTraductions';
import LooseChanges from './Modals/LooseChanges';
import EditGenres from './EditGenres';
import EditImages from './EditImages';
import EditCast from './EditCast';
import EditCrew from './EditCrew';

const EditTitle = ({type}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    
    const [actualPanel, setActualPanel] = useState(0);
    const {movieId, tvshowId} = useParams();
    const [backendErrors, setBackendErrors] = useState(null);
    const [madeChanges, setMadeChanges] = useState(false);   

    const foundTitle = useSelector(selectors.getFoundTitle);

    let dialog, selectedCode;
    let dialogId = "looseAllChanges";
    let list = [{id: 0, ref: null}, 
        {id: 1, ref: null}, 
        {id: 2, ref: null}, 
        {id: 3, ref: null},
        {id: 4, ref: null},
        {id: 5, ref: null}];

    const baseClass = "list-group-item list-group-item-action col";

    useEffect(() => {

        if (movieId) {
            const foundTitle = Number(movieId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findFullInternalMovieById(foundTitle));
            }

        } else if (tvshowId) {
            const foundTitle = Number(tvshowId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findFullInternalTVShowById(foundTitle));
            }

        }

        return () => dispatch(actions.clearFoundTitle());

    }, [movieId, tvshowId, dispatch]);

    
    const handleUpdatePanel = (code) => {
        selectedCode = code;

        if (madeChanges) 
            dialog.click();
        else
            onDialogConfirm();

    };

    if (!foundTitle) {
        return null;
    }

    const onDialogConfirm = () => {
        setActualPanel(selectedCode);
        list.map(e => e.id === selectedCode ? e.ref.className = baseClass + " active" : e.ref.className = baseClass)
        setMadeChanges(false);
    };

    return (
        <div className="row mt-4">

            <div className="row w-100 mb-2 ml-0 ">

                {foundTitle.sourceType !== 0 ?
                    <span className="TMDB-type-tag font-weight-600 mr-3 font-size-update-title-tag">
                        <FormattedMessage id='project.global.type.TMDB'/>
                    </span>
                :
                    null
                }

                <span className="font-size-update-title">
                    <FormattedMessage id='project.common.title.update'/>
                    {!foundTitle.title ? " - " + foundTitle.originalTitle: " - " + foundTitle.title}
                </span>

            </div>

            <div className="row w-100 mb-4 ml-0">
                <a className="link-primary link-bold font-size-update-title-tag" onClick={() => history.goBack()}>
                    ← <FormattedMessage id='project.common.return'/>
                </a>
            </div>

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="side-update-menu-container shadow-container col-2 list-group">

                <button type="button" className={baseClass + " active"} ref={node => list[0].ref = node}
                    onClick={e => handleUpdatePanel(0)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.details'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[1].ref = node}
                    onClick={e => handleUpdatePanel(1)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.traductions'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[2].ref = node}
                    onClick={e => handleUpdatePanel(2)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.genres'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[3].ref = node}
                    onClick={e => handleUpdatePanel(3)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.images'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[4].ref = node}
                    onClick={e => handleUpdatePanel(4)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.cast'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass} ref={node => list[5].ref = node}
                    onClick={e => handleUpdatePanel(5)}>
                    <div className="col-10">
                        <FormattedMessage id='project.titles.update.menu.crew'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

            </div>

            {actualPanel === 0 &&
                <CreateTitle update={true} type={type} setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 1 &&
                <EditTraductions madeChanges={madeChanges} setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 2 &&
                <EditGenres setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 3 &&
                <EditImages setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 4 &&
                <EditCast setMadeChanges={setMadeChanges}/>
            }

            {actualPanel === 5 &&
                <EditCrew setMadeChanges={setMadeChanges}/>
            }

            <button className="btn btn-danger button-align-right button-width-50" 
                ref={node => dialog = node} hidden={true}
                data-toggle="modal" data-target={"#" + dialogId}>
            </button >
            
            <LooseChanges id={dialogId} onConfirm={onDialogConfirm}></LooseChanges>

        </div>

    );

}

EditTitle.propTypes = {
    type: PropTypes.number.isRequired
}

export default EditTitle;