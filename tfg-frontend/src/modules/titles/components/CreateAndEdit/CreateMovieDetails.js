import React from 'react';
import {FormattedMessage} from 'react-intl';
import PropTypes from 'prop-types';;

const CreateMovieDetails = ({movieDuration, setMovieDuration, setMadeChanges, canUpdate}) => {

    return(
        <div className="row shadow-container p-3 mb-3">
            <div className="row w-25 mb-3 mr-3">
                <label htmlFor="movieDuration" className="col-12 title-label-color">
                    <FormattedMessage id="project.titles.create.movie.duration"/>
                </label>
                <div className="col-10 align-input-center">
                    <input type="number" id="movieDuration" className="form-control"
                        value={movieDuration}
                        onChange={e => {setMovieDuration(e.target.value); canUpdate && setMadeChanges(true)}}
                        required min="1"/>
                    <div className="invalid-feedback">
                        <FormattedMessage id='project.global.validator.required'/>
                    </div>
                </div>
            </div>
        </div>
    );

};

CreateMovieDetails.propTypes = {
    movieDuration: PropTypes.string.isRequired,
    setMovieDuration: PropTypes.func.isRequired,
    setMadeChanges: PropTypes.func.isRequired,
    canUpdate: PropTypes.bool.isRequired,
};

export default CreateMovieDetails;