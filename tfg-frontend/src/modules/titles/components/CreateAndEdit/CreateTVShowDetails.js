import React from 'react';
import {FormattedMessage} from 'react-intl';
import PropTypes from 'prop-types';;

const CreateTVShowDetails = (
    {firstAirDate, setFirstAirDate,
    lastAirDate, setLastAirDate,
    setMadeChanges, canUpdate}
    ) => {

    const handleParseDate = (date) => {
        let day = date.getDate();
        let month = date.getMonth() + 1;
        return date.getFullYear() + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;
    }

    return(
        <div className="row shadow-container p-3 mb-3">
            <div className="row w-25 mb-3 mr-3">
                <label htmlFor="firstAirDate" className="col-12 title-label-color">
                    <FormattedMessage id="project.titles.create.tvshow.firstAirDate"/>
                </label>
                <div className="col-12 align-input-center">
                    <input className="form-control" type="date" 
                        value={handleParseDate(new Date(firstAirDate))} 
                        onChange={e => {setFirstAirDate(new Date(e.target.value).getTime()); canUpdate && setMadeChanges(true)}}
                        id="firstAirDate" />
                </div>
            </div>

            <div className="row w-25 mb-3 mr-3">
                <label htmlFor="lastAirDate" className="col-12 title-label-color">
                    <FormattedMessage id="project.titles.create.tvshow.lastAirDate"/>
                </label>
                <div className="col-12 align-input-center">
                    <input className="form-control" type="date" 
                        value={handleParseDate(new Date(lastAirDate))} 
                        onChange={e => {setLastAirDate(new Date(e.target.value).getTime()); canUpdate && setMadeChanges(true)}}
                        id="lastAirDate" />
                </div>
            </div>
        </div>
    );

};

CreateTVShowDetails.propTypes = {
    firstAirDate: PropTypes.number.isRequired,
    setFirstAirDate: PropTypes.func.isRequired,
    lastAirDate: PropTypes.number.isRequired,
    setLastAirDate: PropTypes.func.isRequired,
    setMadeChanges: PropTypes.func.isRequired,
    canUpdate: PropTypes.bool.isRequired,
};

export default CreateTVShowDetails;