import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory, useParams} from 'react-router-dom';

import {Errors} from '../../../common';
import * as castAndCrewActions from '../../castAndCrewActions';
import * as selectors from '../../selectors';

const EditEpisodeCast = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {tvshowId, seasonNumber} = useParams();

    const foundEpisode = useSelector(selectors.getFoundEpisode);
    const foundCast = useSelector(selectors.getFoundCast);

    const [keywords, setKeywords] = useState('');
    const [searched, setSearched] = useState(false);
    const [persons, setPersons] = useState(null);
    const [actualPerson, setActualPerson]  = useState(null);
    const [id, setId]  = useState('');
    const [title, setTitle]  = useState('');

    const [actualCast, setActualCast]  = useState(false);

    const [backendErrors, setBackendErrors] = useState(null);

    var person_default_image = `${process.env.PUBLIC_URL}/images/person_default_image.png`;
    var person_image_path = `${process.env.REACT_APP_BACKEND_URL}/people/`;
    var external_api_image_path = "https://image.tmdb.org/t/p/w500";
    
    let form, modal;

    useEffect(() => {

        dispatch(castAndCrewActions.findEpisodeCast(foundEpisode.id));
        return () => dispatch(castAndCrewActions.clearFoundCast());

    }, [foundEpisode, dispatch]);


    const cleanState = () => {
        setActualPerson(null);
        setKeywords('');
        setId('');
        setSearched(false);
        setTitle('');
    }

    const handleSubmit = event => {

        event.preventDefault();
        
        if (form.checkValidity() && id) {

            let newCast = {
                titleId: tvshowId,
                personId: id,
                charName: title,
                seasonNumber: seasonNumber,
                episodeNumber: foundEpisode.episodeNumber
            };

            newCast.externalId = actualPerson.externalId;
            newCast.externalImage = actualPerson.externalImage;
            newCast.sourceType = actualPerson.sourceType;
            newCast.image = actualPerson.image;
            newCast.name = actualPerson.originalName;

            dispatch(castAndCrewActions.createEpisodeCast(newCast, () => null, errors => setBackendErrors(errors)));

            modal.click();
            cleanState();

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }
    
    const handleClose = () => {
        setBackendErrors(null);
        form.classList = "";
        cleanState();
    }

    const handleDelete = (cast) => {
        setActualCast(cast);
    }

    const handleSearch = () => {
        if (keywords !== '')

        dispatch(castAndCrewActions.findPeople(keywords,
            foundPeople => {
                setSearched(true);
                setPersons(foundPeople.items);
            }));
    }

    const selectPerson = (person) => {
        setActualPerson(person);
        setKeywords(person.originalName);
        setId(person.id);
        setSearched(false);
    }

    const removePerson = () => {
        setActualPerson(null);
        setKeywords('');
        setId('');
        setSearched(false);
    }

    if (!foundCast) {
        return null;
    }

    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="row shadow-container p-3 mb-3">
                <h4 className="font-weight-600 light-gray-dark col-12"><FormattedMessage id="project.titles.update.cast.charName.info.title"/></h4>
                <ul className="mt-2 mb-1 ml-4">
                    <li><FormattedMessage id="project.titles.update.cast.charName.info.language"/></li>
                    <li><FormattedMessage id="project.titles.update.cast.charName.info.multiple"/></li>
                    <li><FormattedMessage id="project.titles.update.cast.charName.info.multiple"/></li>
                    <li><FormattedMessage id="project.titles.update.cast.charName.info.dub"/></li>
                </ul>
            </div>
         
            <div className="row shadow-container p-3 mb-3">
                <div className="w-100 row mb-3 ml-0">
                    <div className="col mr-3">
                        <button type="button" className="btn btn-secondary"
                            data-toggle="modal" data-target={"#add-panel"}>
                            <FormattedMessage id="project.global.buttons.add"/>
                        </button>
                    </div>
                </div>
                
                <div className="w-100 row mb-3 center-element ">
                    <div className="col">
                        <table className="table table-striped">
                            <thead className="thead-light text-center">
                                <tr>
                                <th className="col-5"><FormattedMessage id="project.titles.update.cast.table.person"/></th>
                                <th className="col-5"><FormattedMessage id="project.titles.update.cast.table.charName"/></th>
                                <th className="col-2"><FormattedMessage id="project.titles.update.cast.table.options"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                {foundCast.map(cast =>
                                    <tr>
                                        <td className="d-flex ">
                                            <div className="col-3 image-user-details-container">
                                                <img className="image-cast-add-search border-radius-15 link-pointer" alt="person portrait"
                                                onClick={() => history.push("/people/" + cast.peopleId)}
                                                src={cast.sourceType !== 0 ?
                                                        cast.externalId ?
                                                            person_image_path + cast.externalId + "/externalImage"
                                                        :
                                                            external_api_image_path + cast.imageExternalPath
                                                    :
                                                        cast.image ? 
                                                            person_image_path + cast.id + "/image"
                                                        : 
                                                            person_default_image}
                                                onError={e => {e.target.src = person_default_image}}/>
                                            </div>
                                            
                                            <div className="col-8 ml-2">
                                                <p className="search-person link-pointer" onClick={() => history.push("/people/" + cast.peopleId)}>
                                                    {cast.name}
                                                </p>
                                            </div>    
                                        
                                        </td>
                                        <td className="text-center cast-table-td"><p className="col m-0">{cast.charName}</p></td>
                                        <td className="text-center cast-table-td">
                                            <button className="btn btn-danger btn-sm" type="button" title="Delete"
                                                data-toggle="modal" data-target={"#delete"}
                                                onClick={() => handleDelete(cast)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x" viewBox="0 0 16 16">
                                                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="add-panel" className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog modal-max-width-600" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title"><FormattedMessage id="project.titles.update.cast.title"/></h5>
                        </div>
                        
                        <div className="modal-body">
                            <form ref={node => form = node}>
                                <div className="row mr-2 ml-2 input-group">
                                    <FormattedMessage id="project.people.create.title">
                                        {
                                            (msg) => <input type="text" id="keywords" className="form-control w-50"
                                            onChange={e => setKeywords(e.target.value)}
                                            autoFocus
                                            value={keywords}
                                            placeholder={msg}
                                            required />
                                        }
                                    </FormattedMessage>
                                    <div className="input-group-append">
                                        <button className="btn btn-outline-secondary input-right-button" type="button" 
                                            onClick={() => removePerson()} >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle center-v-button-icon" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                            </svg>
                                        </button>
                                    </div>
                                    
                                    <button type="button" className="btn btn-primary col mr-2 ml-1" 
                                        onClick={(e) => handleSearch()}>
                                        <FormattedMessage id='project.global.buttons.search'/>
                                    </button>

                                    {searched && persons && persons.length > 0 &&
                                        <div className="col-12 container-border mt-2 add-cast-search-container">
                                            {persons.map(person =>
                    
                                                <div key={person.id} className="row mb-1 mt-1">
                                                    <div className="col-1 image-user-details-container">
                                                        <img className="image-cast-add-search border-radius-15" alt="person portrait"
                                                        onClick={() => selectPerson(person)}
                                                        src={person.sourceType !== 0 ?
                                                                person.externalId ?
                                                                    person_image_path + person.externalId + "/externalImage"
                                                                :
                                                                    external_api_image_path + person.imageExternalPath
                                                            :
                                                                person.image ? 
                                                                    person_image_path + person.id + "/image"
                                                                : 
                                                                    person_default_image}
                                                        onError={e => {e.target.src = person_default_image}}/>
                                                    </div>
                                                    <div className="col ml-2">
                                                        <p className="search-person" onClick={() => selectPerson(person)}>
                                                            {person.originalName}
                                                        </p>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    }
                                </div>
                                <div className="row mt-3 mr-2 ml-2">
                                    <label htmlFor="id" className="col-4 title-label-color m-0 pr-3 p-0 label-v-class text-right flex-direction-right">
                                        <FormattedMessage id="project.titles.update.cast.id"/>
                                    </label>
                                    <div className="col-4 align-input-center">
                                        <input type="number" id="id" className="form-control"
                                            value={id}
                                            onChange={() => null}
                                            required
                                            disabled
                                            min={0}/>
                                        <div className="invalid-feedback">
                                            <FormattedMessage id='project.global.validator.required'/>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-3 mr-2 ml-2">
                                    <label htmlFor="charName" className="col-4 title-label-color m-0 pr-3 p-0 label-v-class flex-direction-right">
                                        <FormattedMessage id="project.titles.update.cast.charName"/>
                                    </label>
                                    <div className="col-8 align-input-center">
                                        <input type="text" id="charName" className="form-control"
                                            value={title}
                                            onChange={e => setTitle(e.target.value)}
                                            required />
                                        <div className="invalid-feedback">
                                            <FormattedMessage id='project.global.validator.required'/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                                        
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal"
                                onClick={() => handleClose()}>
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="submit" className="btn btn-success"
                                onClick={(e) => handleSubmit(e)}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                            <button type="button" hidden={true}
                                ref={node => modal = node} 
                                data-dismiss="modal">
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>
        
        
            <div id="delete" className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                <FormattedMessage id="project.titles.update.cast.dialog.delete.title"/>
                            </h5>
                        </div>
                        <div className="modal-body">
                            <p>
                                <FormattedMessage id="project.titles.update.cast.dialog.delete.body"/>
                            </p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal">
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="button" className="btn btn-danger" 
                                data-dismiss="modal"
                                onClick={() => dispatch(castAndCrewActions.deleteEpisodeCast(actualCast.id, () => null, errors => setBackendErrors(errors)))}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default EditEpisodeCast;