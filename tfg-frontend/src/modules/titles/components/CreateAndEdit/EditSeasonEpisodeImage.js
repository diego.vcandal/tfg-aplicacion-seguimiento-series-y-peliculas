import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory, useParams} from 'react-router-dom';

import {Errors} from '../../../common';
import * as actions from '../../actions';
import * as selectors from '../../selectors';

const EditSeasonEpisodeImage = ({setMadeChanges, type}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {tvshowId, seasonNumber} = useParams();

    const foundSeason = useSelector(selectors.getFoundSeason);
    const foundEpisode = useSelector(selectors.getFoundEpisode);
    const [backendErrors, setBackendErrors] = useState(null);

    const default_header_image = `${process.env.PUBLIC_URL}/images/title_default_image_wide_large.png`;
    const default_portrait_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    const title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;

    let seasonImagePath = default_portrait_image;
    let episodeImagePath = default_header_image;

    if(type === 0 && foundSeason.image) {
        seasonImagePath = title_image_path + tvshowId + "/seasons/" + foundSeason.seasonNumber + "/image";
    }

    if(type === 1 && foundEpisode.image) {
        episodeImagePath = title_image_path + tvshowId + "/seasons/" + seasonNumber + "/episodes/" + foundEpisode.episodeNumber + "/image";
    }

    const [seasonImage, setSeasonImage]  = useState(seasonImagePath);
    const [episodeImage, setEpisodeImage]  = useState(episodeImagePath);
    
    let form;

    const handleSubmit = event => {


        event && event.preventDefault();
        
        if (form.checkValidity()) {

            if (type === 0) {
                foundSeason.imageContent = seasonImage === default_portrait_image ? null : seasonImage;
                dispatch(actions.updateSeason(foundSeason, () => history.go(0), errors => setBackendErrors(errors)))
            } else if (type === 1) {
                foundEpisode.imageContent = episodeImage === default_header_image ? null : episodeImage;
                dispatch(actions.updateEpisode(foundEpisode, () => history.go(0), errors => setBackendErrors(errors)))
            }

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    if (!foundSeason && !foundEpisode)
        return null;


    const handleHeaderChange = img => {
        var fileReader= new FileReader();
    
        fileReader.addEventListener("load", function(e) {
            setEpisodeImage(e.target.result);
        }); 
        
        fileReader.readAsDataURL(img);

        setMadeChanges(true);
    }

    const handlePortraitChange = img => {
        var fileReader= new FileReader();
    
        fileReader.addEventListener("load", function(e) {
            setSeasonImage(e.target.result);
        }); 
        
        fileReader.readAsDataURL(img);

        setMadeChanges(true);
    }

    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col user-info" noValidate onSubmit={e => handleSubmit(e)}>

                <div className="row shadow-container p-3 mb-3">

                    {type === 0 &&
                        <div className="col-12 row mb-3 center-element">
                            <div className="col-4 update-portrait-container">
                                <img id="portrait-img" src={seasonImage} className="portrait-update-image" alt="portrait img"/>
                                <input id="portraitFile" type="file" name="image" className="inputfile" 
                                    onChange={e => handlePortraitChange(e.target.files[0])} 
                                    accept="image/*"
                                    multiple={false} />
                                <div className="row mt-3 w-100 center-element">
                                    <div className="col-12 ">
                                        <label htmlFor="portraitFile" className="label-image" >
                                            <FormattedMessage id="project.users.profileEdit.selectImage"/>
                                        </label>
                                    </div>
                                    <div className="col-12">
                                        <span className="label-image red-label" onClick={() => {setSeasonImage(default_portrait_image); setMadeChanges(true);}}>
                                            <span><FormattedMessage id="project.users.profileEdit.deleteImage"/></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-7 ml-4">
                                <span className="row title-img-type-color p-0 mb-3"><FormattedMessage id="project.titles.update.images.portrait"/></span>
                                <span className="row mb-2"><FormattedMessage id="project.titles.update.images.portrait.instructions"/></span>
                                <ul className="row">
                                    <li><FormattedMessage id="project.titles.update.images.portrait.minRes"/></li>
                                    <li><FormattedMessage id="project.titles.update.images.portrait.ratio"/></li>
                                </ul>
                            </div>
                        </div>
                    }

                    {type === 1 &&
                        <div className="col-12 row mb-5 center-element">
                            <div className="col-7 update-header-container">
                                <img id="header-img center-element" src={episodeImage} className="header-update-image" alt="User img"/>
                                <input id="headerFile" type="file" name="image" className="inputfile" 
                                    onChange={e => handleHeaderChange(e.target.files[0])} 
                                    accept="image/*"
                                    multiple={false} />
                                <div className="row mt-3 w-100 center-element">
                                    <div className="col-6 ">
                                        <label htmlFor="headerFile" className="label-image-update" >
                                            <FormattedMessage id="project.users.profileEdit.selectImage"/>
                                        </label>
                                    </div>
                                    <div className="col-6 ">
                                        <span className="label-image-update red-label" onClick={() => {setEpisodeImage(default_header_image); setMadeChanges(true);}}>
                                            <span><FormattedMessage id="project.users.profileEdit.deleteImage"/></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className="col ml-4">
                                <span className="row title-img-type-color p-0 mb-3"><FormattedMessage id="project.titles.update.images.header"/></span>
                                <span className="row mb-2"><FormattedMessage id="project.titles.update.images.header.instructions"/></span>
                                <ul className="row">
                                    <li><FormattedMessage id="project.titles.update.images.header.minRes"/></li>
                                    <li><FormattedMessage id="project.titles.update.images.header.ratio"/></li>
                                </ul>
                            </div>
                        </div>
                    }

                    <div className="w-100 row mb-3">
                        <div className="col title-submit-btn-container p-0">
                            <div>
                                <button type="submit" className="btn btn-success float-right">
                                    <FormattedMessage id="project.titles.create.buttons.add"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );

}

export default EditSeasonEpisodeImage;