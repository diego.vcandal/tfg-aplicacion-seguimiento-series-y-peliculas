/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams, Link} from 'react-router-dom';

import {PageNotFound} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import Seasons from './Seasons';

const AllSeasons = () => {

    const {id, externalId} = useParams();
    const dispatch = useDispatch();
    const seasons = useSelector(selectors.getFoundSeasons);
    const title = useSelector(selectors.getFoundTitle);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findTVShowById(foundTitle));
            }

        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findExternalTVShowById(foundTitle));
            }
        }

        return () => dispatch(actions.clearFoundTitle());

    }, [id, externalId, dispatch]);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findSeasons(foundTitle));
            }
        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findExternalSeasons(foundTitle));
            }
        }

        return () => dispatch(actions.clearFoundSeasons());

    }, [id, externalId, dispatch]);

    if (!title || !seasons || seasons.length === 0) {
        return <PageNotFound />;
    }

    return (
        <div>
            <div className="row mt-4">

                {!id ?
                    <span className="TMDB-type-tag font-weight-600 ml-3">
                        <FormattedMessage id='project.global.type.TMDB'/>
                    </span>
                :
                    null
                }
                
                <span className="search-title-name ml-2">
                    {!title.title ? title.originalTitle + " - " : title.title + " - "}

                    <FormattedMessage id='project.titles.info.seasonCount'/>
                    
                    {title.seasonNumber ? " (" +title.seasonNumber + ")" : null}
                </span>
            </div>

            <div className="row">
                <Link className="link-bold ml-3" to={id ? "/tvshows/" + title.id : "/tmdb/tvshows/" + title.externalId}>
                    ← <FormattedMessage id='project.titles.buttons.return'/>
                </Link>
            </div>

            <div className="row mt-5 center-element w-80">
                <Seasons seasons={seasons} titleId={id ? title.id : title.externalId} preview={false} external={id ? false : true} lastSeason={false}/>
            </div>
        </div>
    );

}

export default AllSeasons;