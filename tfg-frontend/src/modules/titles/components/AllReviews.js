/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';
import {useHistory, useParams} from 'react-router-dom';

import {Pager} from '../../common';
import * as selectors from '../selectors';
import * as ratingActions from '../ratingActions';
import * as actions from '../actions';
import Review from './Entity/Review';


const AllReviews = () => {

    const dispatch = useDispatch();
    const {movieId, tvshowId} = useParams();

    const foundReviews = useSelector(selectors.getFoundReviews);
    const foundTitle = useSelector(selectors.getFoundTitle);

    const history = useHistory();

    const query = new URLSearchParams(useLocation().search);
    let page = query.get("page");

    useEffect(() => {

        if (movieId) {
            const foundTitle = Number(movieId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findMovieById(foundTitle));
            }

        } else if (tvshowId) {
            const foundTitle = Number(tvshowId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findTVShowById(foundTitle));
            }

        }

        return () => dispatch(actions.clearFoundTitle());

    }, [movieId, tvshowId, dispatch]);

    useEffect(() => {

        let foundTitle;
        let p = !Number.isNaN(Number(page)) ? Number(page) : 0;

        if (movieId) {
            foundTitle = Number(movieId);
        } else if (tvshowId) {
            foundTitle = Number(tvshowId);
        }

        dispatch(ratingActions.findReviews({id: foundTitle, page: p}));

        return () => dispatch(ratingActions.clearReviewSearch());

    }, [movieId, tvshowId, page, dispatch]);

    if (1 === 0) {
        return (
            <div className="alert alert-danger" role="alert">
                <FormattedMessage id='project.search.FindUsers.noUsersFound'/>
            </div>
        );
    }

    const handleNextPrevius = type => {
        let path = "/search/users?page=" + (Number(page) + type);
        history.push(path);
    };

    if (!foundReviews || !foundTitle)
        return null;

    return (

        <div className="center-element w-80 mt-4">

            <div className="row w-100 mb-2 ml-0 ">
                <span className="font-size-update-title">
                    <FormattedMessage id='project.titles.reviews.all'/>
                    {!foundTitle.title ? " - " + foundTitle.originalTitle: " - " + foundTitle.title}
                </span>
            </div>

            <div className="row w-100 mb-4 ml-0">
                <a className="link-primary link-pointer link-bold font-size-update-title-tag" onClick={() => history.push((movieId ? '/movies/' : '/tvshows/') + foundTitle.id)}>
                    ← <FormattedMessage id='project.common.return'/>
                </a>
            </div>

            {foundReviews.result.items.map(review => <Review review={review}/>)}

            <Pager 
                back={{
                    enabled: foundReviews.criteria.page >= 1 >= 1,
                    onClick: () => handleNextPrevius(-1) }}
                next={{
                    enabled: foundReviews.result.existMoreItems,
                    onClick: () => handleNextPrevius(1) }}/>

        </div>

    );

}

export default AllReviews;