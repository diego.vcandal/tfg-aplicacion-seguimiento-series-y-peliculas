import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory, Link, useLocation} from 'react-router-dom';

import * as selectors from '../selectors';
import * as ratingActions from '../ratingActions';

import users from '../../user';

import ReactStars from "react-rating-stars-component";
import Review from './Entity/Review';

const TitleReviews = ({type, external}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation().pathname;

    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const myReview = useSelector(selectors.getFoundReview);
    const foundTitle = useSelector(selectors.getFoundTitle);
    const foundReviews = useSelector(selectors.getFoundReviews);

    const [, setBackendErrors] = useState(null);

    const [titleReview, setTitleReview]  = useState(myReview !== null ? myReview.containsReview ? myReview.title : '' : '');
    const [content, setContent]  = useState(myReview !== null ? myReview.containsReview ? myReview.content : '' : '');
    const [rating, setRating] = useState(myReview !== null ? myReview.rating : 1);

    let form, modal;

    useEffect(() => {

        if (!external) {
            dispatch(ratingActions.findReviews({id: foundTitle.id, page: 0}));
        }

        return () => dispatch(ratingActions.clearReviewSearch());

    }, [foundTitle, external, dispatch]);

    const handleClose = () => {
        setBackendErrors(null);
        form.classList = "";
    }

    const handleSubmit = event => {

        event.preventDefault();
        
        if (form.checkValidity()) {

            let review = {
                reviewTitle: titleReview,
	            content: content,
	            rating: rating,
            };

            if (external) {

                if (type === 0) {
                    dispatch(ratingActions.addNewMoviewReview(foundTitle.externalId, review,
                        () => history.go(0), 
                        errors => setBackendErrors(errors)));
                } else {
                    dispatch(ratingActions.addNewTVShowReview(foundTitle.externalId, review,
                        () => history.go(0), 
                        errors => setBackendErrors(errors)));
                }
    
            } else {
            
                if (myReview === null) {
                    dispatch(ratingActions.addNewReview(foundTitle.id, review,
                        () => history.go(0), 
                        errors => setBackendErrors(errors)));
                }else{
                    dispatch(ratingActions.editReview(foundTitle.id, review,
                        () => history.go(0), 
                        errors => setBackendErrors(errors)));
                }
    
            }

            modal.click();

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }
        
    return (

        <div className="row mt-4 w-90 center-element">

            <div className="row ml-5 mb-2 w-100">
                <span className="title-pre-title-box"></span>
                <h3 className="m-0 main-cast-title label-v-class">
                    <FormattedMessage id='project.titles.reviews.title'/>
                    {" (" + (!foundReviews ? 0 : foundReviews.result.items.length) + ")"}
                </h3>

                {isLoggedIn && (myReview === null || !myReview.containsReview) &&
                    <div className="row">
                        <div className="col ml-3 mr-3 ">
                            <button type="button" className="btn btn-success pt-1 pb-1"
                                data-toggle="modal" data-target={"#add-panel"}>
                                <FormattedMessage id="project.global.buttons.addReview"/>
                            </button>
                        </div>
                    </div>
                }

                {foundReviews && foundReviews.result.items.length > 0 &&
                    <Link className="link-bold m-float-right label-v-class mr-5" to={location + "/reviews"}>
                        <FormattedMessage id='project.titles.reviews.seeAll'/>
                    </Link>
                }
            </div>
            
            {foundReviews &&
                <div className="w-90 center-element">
                    {myReview === null || !myReview.containsReview ?
                        foundReviews.result.items.length > 0 ? 
                                <Review review={foundReviews.result.items[0]}></Review>
                            :
                                <div></div>
                        :

                            <Review review={myReview}></Review>
                    }
                </div>
            }
            
            <div id="add-panel" className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog modal-max-width-1000" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title"><FormattedMessage id="project.titles.reviews.add.title"/></h5>
                        </div>
                        
                        <div className="modal-body">
                            <form ref={node => form = node}>
                                <div className="row">
                                    <div className="col-7 pl-4 align-input-center">
                                        <FormattedMessage id="project.titles.create.title">
                                            {
                                                (msg) => <input type="text" id="userName" className="form-control "
                                                onChange={e => setTitleReview(e.target.value)}
                                                autoFocus
                                                value={titleReview}
                                                placeholder={msg}
                                                required />
                                            }
                                        </FormattedMessage>
                                    </div>
                                    <div className="col-4 align-input-center align-rating-review-end m-float-right">
                                        <ReactStars size={30} count={10} activeColor="gold" color="grey" value={rating}
                                                isHalf={false} edit={true} classNames="noselect " onChange={(newRating) => setRating(newRating)}/>
                                    </div>
                                    <span className="col-1 rating-input-rate-col-rating label-v-class pl-0">({rating})</span>
                                </div>
                                <div className="row">
                                    <div className="col-12 mr-3 mt-3">
                                        <label htmlFor="titleDescription" className="col-12 title-label-color">
                                            <FormattedMessage id="project.titles.reviews.add.content"/>
                                        </label>
                                        <div className="col-12 align-input-center">
                                            <textarea className="form-control review-textarea" id="titleDescription" rows="5" 
                                                value={content}
                                                minLength="50"
                                                maxLength="2500"
                                                required
                                                onChange={e => setContent(e.target.value)}></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                                        
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal"
                                onClick={() => handleClose()}>
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="submit" className="btn btn-success"
                                onClick={(e) => handleSubmit(e)}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                            <button type="button" hidden={true}
                                ref={node => modal = node} 
                                data-dismiss="modal">
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    );

}

export default TitleReviews;