/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {FormattedMessage, FormattedDate} from 'react-intl';
import {useHistory} from 'react-router-dom';

import users from '../../../user';
import * as ratingActions from '../../ratingActions';
import ReactStars from 'react-rating-stars-component';
import DeleteReviewModal from '../CreateAndEdit/Modals/DeleteReviewModal';

const Review = ({review}) => {

    const history = useHistory();
    const dispatch = useDispatch();

    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const actualUser = useSelector(users.selectors.getUser);
    const [, setBackendErrors] = useState(null);
    const [liked, setLiked] = useState(review.liked);
    const [likeCount, setLikedCount] = useState(review.likeCount);

    const text_max_len = 600;

    const [titleReview, setTitleReview]  = useState(review.title);
    const [content, setContent]  = useState(review.content);
    const [rating, setRating] = useState(review.rating);

    const deleteReviewModal = "delete-review-modal";

    let form, modal;

    const parseText = str => {
        if (str !== null) {
            if(str.length <= text_max_len) 
                return str;
            else
                return str.substring(0, text_max_len) + "...";
        }

        return "";
    }

    const handleAddRemoveLike = () => {

        if(!isLoggedIn || review.userId === actualUser.id)
            return;

        if (liked) {
            dispatch(ratingActions.removeLike(review.id, () => {
                setLiked(false);
                setLikedCount(likeCount - 1);
            }, errors => setBackendErrors(errors)))
        } else {
            dispatch(ratingActions.addLike(review.id, () => {
                setLiked(true);
                setLikedCount(likeCount + 1);
            }, errors => setBackendErrors(errors)))
        }

    }

    const handleClose = () => {
        setBackendErrors(null);
        form.classList = "";
    }

    const handleSubmit = (event, titleId) => {

        event.preventDefault();
        
        if (form.checkValidity()) {

            let review = {
                reviewTitle: titleReview,
	            content: content,
	            rating: rating,
            };

            dispatch(ratingActions.editReview(titleId, review,
                () => history.go(0), 
                errors => setBackendErrors(errors)));

            modal.click();

        } else {
            setBackendErrors(null);
            form.classList.add('was-validated');
        }

    }

    const doRemoveDispatch = (titleId) => {
        dispatch(ratingActions.deleteReview(titleId,
            () =>  history.go(0)));
    }

    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var person_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/${review.userId}/image`;


    return(
        <div className="w-90 center-element">
                
            <div key={review.id} className="row mt-3 mb-4 shadow-container p-2" >
                <div className="col-1">
                    <img className="image-review border-radius-15 link-pointer " alt="person portrait image"
                        onClick={() => history.push('/users/' + review.userId)} 
                        src={person_image_path}
                        onError={e => {e.target.src = user_default_image}}/>
                </div>

                <div className="col">
                    <h4 className="font-weight-600 word-break-content"> {" " + review.title}</h4>
                    <p>
                        <FormattedMessage id="project.titles.reviews.writtenThe"/>
                        <FormattedDate value={new Date(review.addedDate)}/>
                        {". "} 
                        <span className="link-bold link-pointer-text"
                            onClick={() => history.push('/users/' + review.userId)}>
                                {review.userName}
                                {isLoggedIn && review.userId === actualUser.id && 
                                    <span>(<FormattedMessage id="project.titles.info.avgRating.you"/>)</span>
                                }
                        </span> 
                    </p>
                </div>
                
                <div className="col-4">
                    <div className="row">
                        <div className="col-7 p-0 m-float-right">
                            <div className="row p-0 align-rating-right">
                                <span className="font-weight-600 label-v-class"> {"(" + likeCount + ")"}</span>
                                <button className="btn no-btn-outline text-right" type="button" title="Delete"
                                    onClick={() => handleAddRemoveLike()}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" 
                                        className={(liked ? "active" : "") + (!isLoggedIn || (isLoggedIn && review.userId === actualUser.id) ? "nohover active" : "" ) + " bi bi-hand-thumbs-up-fill thumbs-up-btn"} 
                                        viewBox="0 0 16 16">
                                        <path d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a9.84 9.84 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733.058.119.103.242.138.363.077.27.113.567.113.856 0 .289-.036.586-.113.856-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.163 3.163 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16H8c-.605 0-1.07-.081-1.466-.218a4.82 4.82 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>

                        {isLoggedIn && review.userId === actualUser.id &&
                            <div className="col m-float-right p-0 text-right">
                                <button className="btn btn-success mt-1" type="button" title="Edit"
                                    data-toggle="modal" data-target={"#add-panel" + review.id}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </button>
                                <button className="btn btn-danger ml-3 mt-1 mr-2" type="button" title="Edit"
                                    data-toggle="modal" data-target={"#" + deleteReviewModal + review.id}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                    </svg>
                                </button>
                            </div>
                            
                        }
                    </div>

                    <div className="col-12  ">
                        <span className="row align-rating-fullreview-end m-float-right">
                            <ReactStars size={23} count={10} activeColor="gold" color="grey" value={rating}
                                isHalf={false} edit={false} classNames="noselect"/>
                            <span className="ml-3 label-v-class">({rating})</span>
                        </span>
                    </div>
                </div>

                <div className="col-11 m-float-right mt-2">
                    <p className="word-break-content">{parseText(review.content)}</p>
                </div>
                
            </div>

            <div id={"add-panel" + review.id } className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog modal-max-width-1000" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title"><FormattedMessage id="project.titles.reviews.update.title"/></h5>
                        </div>
                        
                        <div className="modal-body">
                            <form ref={node => form = node}>
                                <div className="row">
                                    <div className="col-7 pl-4 align-input-center">
                                        <FormattedMessage id="project.titles.create.title">
                                            {
                                                (msg) => <input type="text" id="userName" className="form-control "
                                                onChange={e => setTitleReview(e.target.value)}
                                                autoFocus
                                                value={titleReview}
                                                placeholder={msg}
                                                required />
                                            }
                                        </FormattedMessage>
                                    </div>
                                    <div className="col-4 align-input-center align-rating-review-end m-float-right">
                                        <ReactStars size={30} count={10} activeColor="gold" color="grey" value={rating}
                                                isHalf={false} edit={true} classNames="noselect " onChange={(newRating) => setRating(newRating)}/>
                                    </div>
                                    <span className="col-1 rating-input-rate-col-rating label-v-class pl-0">({rating})</span>
                                </div>
                                <div className="row">
                                    <div className="col-12 mr-3 mt-3">
                                        <label htmlFor="titleDescription" className="col-12 title-label-color">
                                            <FormattedMessage id="project.titles.reviews.add.content"/>
                                        </label>
                                        <div className="col-12 align-input-center">
                                            <textarea className="form-control review-textarea" id="titleDescription" rows="5" 
                                                value={content}
                                                minLength="50"
                                                maxLength="2500"
                                                required
                                                onChange={e => setContent(e.target.value)}></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                                        
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal"
                                onClick={() => handleClose()}>
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="submit" className="btn btn-success"
                                onClick={(e) => handleSubmit(e, review.titleId)}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                            <button type="button" hidden={true}
                                ref={node => modal = node} 
                                data-dismiss="modal">
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>

            <DeleteReviewModal id={deleteReviewModal + review.id} onConfirm={() => doRemoveDispatch(review.titleId)} />

        </div>
    );
    

};

Review.propTypes = {
    review: PropTypes.object.isRequired
};

export default Review;