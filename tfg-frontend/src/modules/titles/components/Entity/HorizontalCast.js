/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

const HorizontalCast = ({cast}) => {

    const history = useHistory();

    var person_default_image = `${process.env.PUBLIC_URL}/images/person_default_image.png`;
    var person_image_path = `${process.env.REACT_APP_BACKEND_URL}/people/`;
    var external_api_image_path = "https://image.tmdb.org/t/p/w500";

    var arrow_left_image = `${process.env.PUBLIC_URL}/images/left_arrow.png`;
    var arrow_right_image = `${process.env.PUBLIC_URL}/images/right_arrow.png`;

    const arrow_style = "align-self-center p-2 ";

    const [arrow_left_active, setArrowLeftActive]  = useState(false);
    const [arrow_right_active, setArrowRightActive]  = useState(cast.length > 7);

    const text_max_len = 30;
    const scrollWidth = 1012.5;
    let scroll;

    const parseText = str => {
        if(str.length <= text_max_len) 
            return str;
        else
            return str.substring(0, text_max_len) + "...";
    }

    const handleDisplacement = direction => {

        if (direction === 0 && arrow_left_active){
            scroll.scrollLeft -= scrollWidth;
            setArrowRightActive(true);

            if ((scroll.scrollLeft -= scrollWidth) > 0)
                setArrowLeftActive(true);
            else 
                setArrowLeftActive(false);

        } else if(direction === 1 && arrow_right_active){
            scroll.scrollLeft += scrollWidth;
            setArrowLeftActive(true);

            if ((scroll.scrollLeft += scrollWidth) < (scroll.scrollWidth - scrollWidth))
                setArrowRightActive(true);
            else 
                setArrowRightActive(false);

        }

    };


    return(
        <div className="row center-element main-cast-container">

            <div className="image-arrow-container p-0 col-1">
                <img className={arrow_style + (arrow_left_active ? "image-arrow link-pointer" : "image-arrow-fade")} src={arrow_left_image} 
                    onClick={() => handleDisplacement(0)} alt="arrow left" />
            </div>
        
            <div className="mt-3 horizontal-row-scroll col-10 p-0">

                <div className="row horizontal-row-cast-container m-0" ref={node => scroll = node} >

                    {cast.map(cast =>

                        <div key={cast.id} className="horizontal-cast border-radius-15" >

                            <div className="mb-2">
                                <img className="image-medium link-pointer"
                                onClick={() => cast.sourceType === 0 ? history.push('/people/' + cast.peopleId) : history.push('/tmdb/people/' + cast.externalId)} 
                                    alt="cast portrait" src={cast.sourceType !== 0 ?
                                        external_api_image_path + cast.externalImage
                                    :
                                        cast.image ? 
                                            person_image_path + cast.id + "/image"
                                        : 
                                            person_default_image}
                                    />
                            </div>

                            <div>
                                <p className="ml-1 mr-1 link-pointer-text mb-1" 
                                    onClick={() => cast.sourceType === 0 ? history.push('/people/' + cast.peopleId) : history.push('/tmdb/people/' + cast.externalId)} >
                                    {parseText(cast.name)}
                                </p>

                                <p className="ml-1 mr-1 cast-sub-info-name mb-0">
                                    {parseText(cast.charName)}
                                </p>

                                {cast.episodeNumber > 0 &&
                                    <p className="ml-1 mr-1 cast-sub-info-episodes mb-0">
                                        (<FormattedMessage id='project.titles.info.episodeCount'/>
                                        {": " + cast.episodeNumber + ")"}
                                    </p>
                                }
                            </div>
                            
                        </div>

                    )}

                </div>
                
            </div>

            <div className="image-arrow-container p-0 col-1">
                <img className={arrow_style + (arrow_right_active ? "image-arrow link-pointer" : "image-arrow-fade")} src={arrow_right_image} 
                    onClick={() => handleDisplacement(1)} alt="arrow rigth" />
            </div>

        </div>
    );
    
    
};

HorizontalCast.propTypes = {
    cast: PropTypes.array.isRequired
};

export default HorizontalCast;