/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {FormattedMessage, FormattedDate} from 'react-intl';
import {useHistory, useLocation} from 'react-router-dom';

import users from '../../../user';
import * as actions from '../../actions';
import * as selectors from '../../selectors';
import AdminOptionsModal from '../CreateAndEdit/Modals/AdminOptionsModal';

const Episodes = ({titleId, episodes}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation().pathname;
    
    const [, setBackendErrors] = useState(null);

    const isAdmin = useSelector(users.selectors.isAdmin);
    const season = useSelector(selectors.getFoundSeason);
    const title = useSelector(selectors.getFoundTitle);

    const deleteModal = "delete-modal";

    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image_wide.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;
    var external_api_imgage_path = "https://image.tmdb.org/t/p/w500";

    const text_max_len = 280;

    const parseText = str => {
        if(str.length <= text_max_len) 
            return str;
        else
            return str.substring(0, text_max_len) + "...";
    }

    const handleCacheEpisode = (episodeNumber) => {
        dispatch(actions.createCacheEpisode(title.externalId, season.seasonNumber, episodeNumber, {
                externalSeasonId: season.externalId
            },
            title.id === -1 ? () => history.push(`/tmdb/tvshows/${title.externalId}`) : () => history.go(0), 
            errors => setBackendErrors(errors)));
    };

    if (!episodes && episodes.length < 1) {
        return null;
    }

    return(
        <div className="w-100">

            {episodes.map(episode =>
                <div>
                    <div key={episode.id} className={"row season-info-container p-2 " + (isAdmin && episode.setForDelete ? "set-for-delete-container" : "")}>

                        <div className="col-3">
                            <img className="episode-preview-img" alt="season poster image" 
                                src={episode.sourceType !== 0 ?
                                        episode.imagePath ?
                                            external_api_imgage_path + episode.imagePath
                                        :
                                            title_default_image
                                    :
                                        episode.image ? 
                                            title_image_path + titleId + "/seasons/" + season.seasonNumber + "/episodes/" + episode.episodeNumber + "/image"
                                        :
                                            title_default_image
                                    
                                } />
                        </div>
                        <div className="col">
                            <div className="row">
                                <div className="col ">
                                    <div className="row">
                                        <span className="season-preview-title-name ">{episode.episodeNumber ?
                                            episode.episodeNumber + " - ": null
                                        }</span>

                                        <span className="season-preview-title-name ml-2">{!episode.title ?
                                            episode.originalTitle
                                        :
                                            episode.title
                                        }</span>

                                        {episode.airDate && episode.airDate !== 0 ?
                                            <span className="season-preview-title-year ml-2">
                                                (<FormattedDate value={new Date(episode.airDate)}/>)
                                            </span>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="col-4 p-0">
                                    {isAdmin && episode.id && episode.id === -1 &&
                                        <div className="col p-0">
                                            <button className="btn btn-success btn-cache-title w-100" 
                                                type="button" title="Create"
                                                onClick={() => handleCacheEpisode(episode.episodeNumber)}>
                                                <FormattedMessage id='project.titles.button.create.episode'/>
                                            </button>
                                        </div>
                                    }
                                    {isAdmin && title.sourceType === 0 && episode.id && episode.id !== -1 &&
                                        <div className="col admin-panel mr-1">
                                            {isAdmin && episode.setForDelete && 
                                                <span className="deletion-tag mb-1"><FormattedMessage id='project.titles.setForDeletion'/></span>
                                            }
                                            {!episode.setForDelete &&
                                                <button className="btn btn-danger btn-sm ml-2" type="button" title="Delete"
                                                    data-toggle="modal" data-target={"#" + deleteModal + "-" + episode.id}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                    </svg>
                                                </button>
                                            }
                                            <button className="btn btn-success btn-sm ml-2" type="button" title="Edit"
                                                    onClick={() => history.push(location + "/episodes/" + episode.id + '/update')}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                </svg>
                                            </button>
                                        </div> 
                                    }
                                </div>
                            </div>

                            <div className="row align-text-justify ml-2 mt-2">
                                <span className="mr-1">{!episode.description ?
                                    !episode.description ?
                                        <FormattedMessage id='project.titles.info.noData'/>
                                    :
                                        parseText(episode.originalDescription)
                                :
                                    parseText(episode.description)
                                }</span>
                            </div>
                        </div>
                      
                    </div>

                    <hr className="row hr-seasons-list"/>

                    <AdminOptionsModal id={deleteModal + "-" + episode.id} type={0} 
                        onConfirm={() => dispatch(actions.setEpisodeForDeletion(episode.id, () => history.go(0), null))} />
                </div>

            )}

        </div>
    );
    

};

Episodes.propTypes = {
    titleId: PropTypes.number.isRequired,
    episodes: PropTypes.array.isRequired
};

export default Episodes;