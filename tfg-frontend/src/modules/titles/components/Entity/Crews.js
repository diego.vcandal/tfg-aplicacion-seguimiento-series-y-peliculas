/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';
import { CrewTypes } from '../../util/CrewTypes';


const Crews = ({crews}) => {

    const history = useHistory();
    var person_default_image = `${process.env.PUBLIC_URL}/images/person_default_image.png`;
    var person_image_path = `${process.env.REACT_APP_BACKEND_URL}/people/`;
    var external_api_image_path = "https://image.tmdb.org/t/p/w500";

    let crewLists = [];

    CrewTypes.map(ct => crewLists.push({id: ct.id, type: ct.type, name: ct.name, crews: []}));

    crews.forEach(crew => {
        let added = false;
        crewLists.forEach(crewList => {
            if (crew.crewTypeId === crewList.id || crew.department === crewList.type) {
                crewList.crews.push(crew);
                added = true;
            }
        });

        if (!added)
            crewLists.find(cl => cl.id === 1).crews.push(crew);
    });

    return(
        <div>
            {crewLists.map(crewList => 

                <div>
                    {crewList.crews.length > 0 &&
                        <div className="mt-3 ml-3 mb-5">
                            <h3 className="full-cast-department mb-2"><FormattedMessage id={crewList.name}/></h3>
                            {crewList.crews.map(person => 
                                <div key={person.id} className="row mt-3 mb-4 ml-3" >

                                    <div className="col-2 image-person-details-container">
                                        <img className="image-small border-radius-15 link-pointer" alt="person portrait image"
                                            onClick={() => person.sourceType === 0 ? history.push('/people/' + person.peopleId) : history.push('/tmdb/people/' + person.externalId)}
                                            src={person.sourceType !== 0 ?
                                                external_api_image_path + person.externalImage
                                            :
                                                person.image ? 
                                                    person_image_path + person.id + "/image"
                                                : 
                                                    person_default_image}
                                            onError={e => {e.target.src = person_default_image}}/>
                                    </div>
                                    <div className="col ml-1 mr-1 mb-1">
                                        <p className="row ml-2 search-name link-pointer-text mt-1"
                                            onClick={() => person.sourceType === 0 ? history.push('/people/' + person.peopleId) : history.push('/tmdb/people/' + person.externalId)}>
                                                {person.name}
                                        </p>
                                        {person.job &&
                                            <p className="row ml-3 cast-sub-info-name mb-0">
                                                {person.job}
                                            </p>
                                        }
                                        {person.episodeNumber > 0 &&
                                            <p className="ml-3 mr-1 cast-sub-info-episodes mb-0">
                                                (<FormattedMessage id='project.titles.info.episodeCount'/>
                                                {": " + person.episodeNumber + ")"}
                                            </p>
                                        }
                                    </div>
                                    
                                </div>
                            )}
                        </div>
                    }
                </div>

            )}
        </div>
    );
    

};

Crews.propTypes = {
    crews: PropTypes.array.isRequired
};

export default Crews;