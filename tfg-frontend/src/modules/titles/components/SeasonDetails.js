/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory, useParams, Link} from 'react-router-dom';

import {PageNotFound} from '../../common';
import users from '../../user';
import * as actions from '../actions';
import * as selectors from '../selectors';
import Episodes from './Entity/Episodes';

const SeasonDetails = () => {

    const {id, externalId} = useParams();
    const {seasonNumber} = useParams();
    const dispatch = useDispatch();
    const history = useHistory();

    const [, setBackendErrors] = useState(null);

    const isAdmin = useSelector(users.selectors.isAdmin);
    const season = useSelector(selectors.getFoundSeason);
    const title = useSelector(selectors.getFoundTitle);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findTVShowById(foundTitle));
            }
        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findExternalTVShowById(foundTitle));
            }

        }

        return () => dispatch(actions.clearFoundTitle());

    }, [id, externalId, dispatch]);

    useEffect(() => {

        const foundNumber = Number(seasonNumber);

        if (id) {
            const foundTitle = Number(id);
            
            if (!Number.isNaN(foundTitle) && !Number.isNaN(foundNumber)) {
                dispatch(actions.findSeasonByNumber(foundTitle, foundNumber));
            }

        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findExternalSeasonByNumber(foundTitle, foundNumber));
            }

        }

        return () => dispatch(actions.clearFoundSeason());

    }, [id, externalId, seasonNumber, dispatch]);

    if (!title || !season || !season.episodes) {
        return <PageNotFound />;
    }

    const handleCacheSeason = (externalSeasonId, seasonNumber) => {
        dispatch(actions.createCacheSeason(title.externalId, seasonNumber, {
                externalSeasonId: externalSeasonId
            },
            title.id === -1 ? () => history.push(`/tmdb/tvshows/${title.externalId}`) : () => history.go(0),  
            errors => setBackendErrors(errors)));
    };

    const handleCacheAllEpisodes = (externalSeasonId, seasonNumber) => {
        dispatch(actions.createCacheAllEpisodes(title.externalId, seasonNumber, {
                externalSeasonId: externalSeasonId
            },
            title.id === -1 ? () => history.push(`/tmdb/tvshows/${title.externalId}`) : () => history.go(0), 
            errors => setBackendErrors(errors)));
    };

    return (
        <div className="mt-4">
            <div className="row">
                <div className="col">
                    <div className="row">
                        {!id ?
                            <span className="TMDB-type-tag font-weight-600 ml-3">
                                <FormattedMessage id='project.global.type.TMDB'/>
                            </span>
                        :
                            null
                        }

                        <span className="search-title-name ml-2">
                            {!season.title ? season.originalTitle + " - " : season.title + " - "}

                            <FormattedMessage id='project.titles.info.episodeCount'/>
                            
                            {" (" + season.episodes.length + ")"}
                        </span>
                    </div>

                    <div className="row ">
                        <div className="col">
                            <Link className="link-bold ml-3" to={id ? "/tvshows/" + title.id + "/seasons" : "/tmdb/tvshows/" + title.externalId + "/seasons" }>
                                ← <FormattedMessage id='project.titles.buttons.seasonsReturn'/>
                            </Link>
                        </div>
                    </div>
                </div>

                {isAdmin && 
                    <div className="col-5 text-right">
                        {season.id && season.id === -1 &&
                            <button className="col-4 btn btn-success btn-cache-title ml-2 mr-2 mb-4 w-100" 
                                type="button" title="Create"
                                onClick={() => handleCacheSeason(season.externalId, season.seasonNumber)}>
                                <FormattedMessage id='project.titles.button.create'/>
                            </button>
                        }
                        {season.episodes && season.episodes.length > 0 && 
                            season.episodes.filter(e => e.id === -1).length > 0 &&
                            
                            <button className="col-5 btn btn-primary btn-cache-title ml-2 mr-2 mb-4 w-100" 
                                type="button" title="Create"
                                onClick={() => handleCacheAllEpisodes(season.externalId, season.seasonNumber)}>
                                <FormattedMessage id='project.titles.button.create.all'/>
                            </button>
                        }
                        {season.id && season.id !== -1 && title.sourceType === 0 &&
                            <button className="col-4 btn btn-success btn-cache-title ml-2 mr-2 mb-4 w-100" 
                                type="button" title="Create"
                                onClick={() => history.push(`/tvshows/${title.id}/seasons/${season.seasonNumber}/create-episode`)}>
                                <FormattedMessage id='project.titles.buttons.add.Episode'/>
                            </button>
                        }
                    </div>
                }
            </div>

            <div className="row mt-5 center-element w-80">
                <Episodes episodes={season.episodes} titleId={title.id} />
            </div>

        </div>
    );

}

export default SeasonDetails;