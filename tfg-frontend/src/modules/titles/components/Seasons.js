/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {useHistory, useLocation} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';

import users from '../../user';
import * as actions from '../actions';
import * as selectors from '../selectors';
import AdminOptionsModal from './CreateAndEdit/Modals/AdminOptionsModal';

const Seasons = ({titleId, seasons, preview, external, lastSeason}) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const isAdmin = useSelector(users.selectors.isAdmin);
    const fountTitle = useSelector(selectors.getFoundTitle);
    const location = useLocation().pathname;

    const [, setBackendErrors] = useState(null);
    const deleteModal = "delete-modal";

    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;
    var external_api_imgage_path = "https://image.tmdb.org/t/p/w500";

    const text_max_len = 280;

    let basePath = "/tvshows/";

    if (external) {
        basePath = "/tmdb" + basePath;
    }

    const parseText = str => {
        if(str.length <= text_max_len) 
            return str;
        else
            return str.substring(0, text_max_len) + "...";
    }

    const handleCacheSeason = (externalSeasonId, seasonNumber) => {
        dispatch(actions.createCacheSeason(fountTitle.externalId, seasonNumber, {
                externalSeasonId: externalSeasonId
            },
            fountTitle.id === -1 ? () => history.push(`/tmdb/tvshows/${fountTitle.externalId}`) : () => history.go(0), 
            errors => setBackendErrors(errors)));
    };

    return(
        <div className="w-100">

            {seasons.map(season =>
                <div>
                    <div key={season.id} className={"row season-info-container mb-3 p-2 " + (isAdmin && season.setForDelete && !preview ? "set-for-delete-container" : "")}>

                        <div className={preview ? "col-3" : "col-2"}>
                            <img className="season-preview-img link-pointer" alt="season poster image" 
                                src={season.sourceType !== 0 ?
                                        season.imagePath ?
                                            external_api_imgage_path + season.imagePath
                                        :
                                            title_default_image
                                    :
                                        season.image ? 
                                            title_image_path + titleId + "/seasons/" + season.seasonNumber + "/image"
                                        :
                                            title_default_image
                                    
                                } onClick={() => history.push(basePath + titleId + "/seasons/" + season.seasonNumber)} />
                        </div>

                        <div className={preview ? "col-8" : "col"}> 
                            <div className="row">
                                <div className="col">
                                    <div className="row">
                                        <span className="season-preview-title-name link-pointer"
                                            onClick={() => history.push(basePath + titleId + "/seasons/" + season.seasonNumber)}>
                                        
                                        {!season.title ?
                                            season.originalTitle
                                        :
                                            season.title
                                        }</span>

                                        <span className="season-preview-title-year ml-2">
                                            {season.year && season.year > 0 && "(" + season.year + ")"}
                                        </span>
                                    </div>

                                    <div className="row">
                                        <span className="title-sub-info-title">
                                            {season.episodeCount} <FormattedMessage id='project.titles.info.episodes'/>
                                        </span>
                                    </div>
                                </div>

                                {isAdmin && !lastSeason && season.id && season.id === -1 &&
                                    <div className="col-3">
                                        <button className="btn btn-success btn-cache-title ml-2 mr-2 mb-4 w-100" 
                                            type="button" title="Create"
                                            onClick={() => handleCacheSeason(season.externalId, season.seasonNumber)}>
                                            <FormattedMessage id='project.titles.button.create'/>
                                        </button>
                                    </div>
                                }

                                {isAdmin && fountTitle.sourceType === 0 && !lastSeason && season.id && season.id !== -1 &&
                                    <div className="col-4 admin-panel mr-1">
                                        {isAdmin && season.setForDelete && 
                                            <span className="deletion-tag"><FormattedMessage id='project.titles.setForDeletion'/></span>
                                        }
                                        {!season.setForDelete &&
                                            <button className="btn btn-danger btn-sm ml-2 mb-2" type="button" title="Delete"
                                                data-toggle="modal" data-target={"#" + deleteModal + "-" + season.id}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                </svg>
                                            </button>
                                        }
                                        <button className="btn btn-success btn-sm ml-2 mb-2" type="button" title="Edit"
                                                onClick={() => history.push(location + "/" + season.id + '/update')}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                            </svg>
                                        </button>
                                    </div> 
                                }
                            </div>

                            <div className="row align-text-justify ml-2 mt-2">
                                <span className="mr-1">{!season.description ?
                                    !season.description ?
                                        <FormattedMessage id='project.titles.info.noData'/>
                                    :
                                        parseText(season.originalDescription)
                                :
                                    parseText(season.description)
                                }</span>
                            </div>

                        </div>
                    </div>

                    {!preview &&
                        <hr className="row hr-seasons-list"/>
                    }

                    <AdminOptionsModal id={deleteModal + "-" + season.id} type={0} 
                        onConfirm={() => dispatch(actions.setSeasonForDeletion(season.id, () => history.go(0), null))} />

                </div>

            )}


        </div>
    );
    

};

Seasons.propTypes = {
    titleId: PropTypes.number.isRequired,
    seasons: PropTypes.array.isRequired,
    preview: PropTypes.bool.isRequired,
    external: PropTypes.bool.isRequired,
    lastSeason: PropTypes.bool.isRequired,
};

export default Seasons;