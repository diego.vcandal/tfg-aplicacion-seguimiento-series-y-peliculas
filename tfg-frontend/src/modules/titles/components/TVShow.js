/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useParams, Redirect} from 'react-router-dom';

import {PageNotFound} from '../../common';
import * as actions from '../actions';
import * as followingListActions from '../followingListActions';
import * as ratingActions from '../ratingActions';
import * as selectors from '../selectors';
import MainCast from './MainCast';
import TitleCommonInfo from './TitleCommonInfo';
import TitleReviews from './TitleReviews';

const TVShow = () => {

    const {id, externalId} = useParams();
    const dispatch = useDispatch();
    const title = useSelector(selectors.getFoundTitle);
    const seasons = useSelector(selectors.getFoundSeasons);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findTVShowById(foundTitle));
            }
            

        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findExternalTVShowById(foundTitle));
            }

        }

        return () => dispatch(actions.clearFoundTitle());

    }, [id, externalId, dispatch]);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findLastSeason(foundTitle));
            }
        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findLastExternalSeason(foundTitle));
            }
        }

        return () => dispatch(actions.clearFoundSeasons());

    }, [id, externalId, dispatch]);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(ratingActions.findMyReview(foundTitle));
            }
        }

        return () => dispatch(ratingActions.clearFoundReview());

    }, [id, dispatch]);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) 
                dispatch(followingListActions.getFollowingListTitleInfo(foundTitle));
            
        }

        return () => dispatch(followingListActions.clearFollowingListTitleInfo());
    }, [id, dispatch]);

    if (!title) {
        return <PageNotFound />;
    }

    if (title && (title.externalId === Number(externalId)) && title.id > 0) {
        return <Redirect to={"/tvshows/" + title.id} />;
    }

    return (
        <div className="mt-4">
            <div className="row">
                <TitleCommonInfo title={title} type={1} seasons={seasons} external={id ? false : true}/>
            </div>
            <div className="row">
                <MainCast type={1}/>
            </div>
            <div className="row">
                <TitleReviews type={1} external={id ? false : true}/>
            </div>
        </div>
    );

}

export default TVShow;