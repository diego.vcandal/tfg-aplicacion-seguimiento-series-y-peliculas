/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useParams, Redirect} from 'react-router-dom';

import {PageNotFound} from '../../common';
import * as actions from '../actions';
import * as followingListActions from '../followingListActions';
import * as ratingActions from '../ratingActions';
import * as selectors from '../selectors';
import TitleCommonInfo from './TitleCommonInfo';
import MainCast from './MainCast';
import TitleReviews from './TitleReviews';

const Movie = () => {

    const {id, externalId} = useParams();
    const dispatch = useDispatch();
    
    const title = useSelector(selectors.getFoundTitle);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findMovieById(foundTitle));
            }

        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                dispatch(actions.findExternalMovieById(foundTitle));
            }

        }

        return () => dispatch(actions.clearFoundTitle());

    }, [id, externalId, dispatch]);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                dispatch(ratingActions.findMyReview(foundTitle));
            }
        }

        return () => dispatch(ratingActions.clearFoundReview());

    }, [id, dispatch]);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) 
                dispatch(followingListActions.getFollowingListTitleInfo(foundTitle));
            
        }

        return () => dispatch(followingListActions.clearFollowingListTitleInfo());
    }, [id, dispatch]);

    if (!title) {
        return <PageNotFound />;
    }

    if (title && (title.externalId === Number(externalId)) && title.id > 0) {
        return <Redirect to={"/movies/" + title.id} />;
    }

    return (
        <div className="mt-4">
            <div className="row">
                <TitleCommonInfo title={title} type={0} seasons={null} external={id ? false : true} />
            </div>
            <div className="row">
                <MainCast type={0}/>
            </div>
            <div className="row">
                <TitleReviews type={0} external={id ? false : true}/>
            </div>
        </div>
        
    );

}

export default Movie;