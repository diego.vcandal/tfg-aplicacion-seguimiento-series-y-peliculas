import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useParams} from 'react-router-dom';

import * as selectors from '../selectors';
import * as actions from '../actions';
import AllCasts from './AllCasts';

const AllCastAndCrew = ({type}) => {

    const dispatch = useDispatch();

    const {id, externalId} = useParams();

    const foundTitle = useSelector(selectors.getFoundTitle);

    useEffect(() => {

        if (id) {
            const foundTitle = Number(id);

            if (!Number.isNaN(foundTitle)) {
                if (type === 0)
                    dispatch(actions.findMovieById(foundTitle, () => null));
                else
                    dispatch(actions.findTVShowById(foundTitle, () => null)); 
            }

        } else if (externalId) {
            const foundTitle = Number(externalId);

            if (!Number.isNaN(foundTitle)) {
                if (type === 0)
                    dispatch(actions.findExternalMovieById(foundTitle, () => null));
                else
                    dispatch(actions.findExternalTVShowById(foundTitle, () => null));
            }
        }

        return () => dispatch(actions.clearFoundTitle());

    }, [id, externalId, type, dispatch]);

    if (!foundTitle) {
        return null;
    }

    return (

        <div className="mt-4 w-90 center-element">
            <AllCasts type={type} />
        </div>
    );

}

export default AllCastAndCrew;