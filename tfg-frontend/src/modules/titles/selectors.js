const getModuleState = state => state.titles;

export const getFoundTitle = state => 
    getModuleState(state).foundTitle;

export const getFoundSeasons = state => 
    getModuleState(state).foundSeasons;

export const getFoundSeason = state => 
    getModuleState(state).foundSeason;

export const getFoundEpisode = state => 
    getModuleState(state).foundEpisode;

export const getFoundCast = state => 
    getModuleState(state).foundCast;

export const getFoundCrew = state => 
    getModuleState(state).foundCrew;

export const getFoundReview = state => 
    getModuleState(state).foundReview;

export const getFoundReviews = state => 
    getModuleState(state).foundReviews;

export const getFoundFollowingListTitle = state => 
    getModuleState(state).foundFollowingListTitle;