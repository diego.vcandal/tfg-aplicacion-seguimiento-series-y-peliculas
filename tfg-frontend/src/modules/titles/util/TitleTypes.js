export const TitleTypes = [
    {id: 0, type: "movie", name: "project.title.movie"},
    {id: 1, type: "tvshow", name: "project.title.tvshow"},
]