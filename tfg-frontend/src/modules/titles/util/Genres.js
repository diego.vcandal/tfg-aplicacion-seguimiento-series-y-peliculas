export const Genres = [
    {id: 1, externalId: 28, name: "project.title.genre.action"},
    {id: 2, externalId: 12, name: "project.title.genre.adventure"},
    {id: 3, externalId: 16, name: "project.title.genre.animation"},
    {id: 4, externalId: 35, name: "project.title.genre.comedy"},
    {id: 5, externalId: 80, name: "project.title.genre.crime"},
    {id: 6, externalId: 99, name: "project.title.genre.documentary"},
    {id: 7, externalId: 18, name: "project.title.genre.drama"},
    {id: 8, externalId: 10751, name: "project.title.genre.family"},
    {id: 9, externalId: 14, name: "project.title.genre.fantasy"},
    {id: 10, externalId: 37, name: "project.title.genre.history"},
    {id: 11, externalId: 27, name: "project.title.genre.horror"},
    {id: 12, externalId: 10402, name: "project.title.genre.music"},
    {id: 13, externalId: 9648, name: "project.title.genre.mistery"},
    {id: 14, externalId: 10749, name: "project.title.genre.romance"},
    {id: 15, externalId: 878, name: "project.title.genre.sciencefiction"},
    {id: 16, externalId: 10770, name: "project.title.genre.tvmovie"},
    {id: 17, externalId: 53, name: "project.title.genre.thriller"},
    {id: 18, externalId: 10752, name: "project.title.genre.war"},
    {id: 19, externalId: 37, name: "project.title.genre.western"},
    {id: 20, externalId: 10759, name: "project.title.genre.actionadventure"},
    {id: 21, externalId: 10762, name: "project.title.genre.kids"},
    {id: 22, externalId: 10763, name: "project.title.genre.news"},
    {id: 23, externalId: 10764, name: "project.title.genre.reality"},
    {id: 24, externalId: 10765, name: "project.title.genre.scififantasy"},
    {id: 25, externalId: 10766, name: "project.title.genre.soap"},
    {id: 26, externalId: 10767, name: "project.title.genre.talk"},
    {id: 27, externalId: 10768, name: "project.title.genre.warpolitics"}
]

export const getGenreNameCode = (id, externalId) => {
    return Genres.find(g => g.id === id || g.externalId === externalId);
}

export const getFullGenre = (id) => {
    return Genres.find(g => g.id === id);
}
