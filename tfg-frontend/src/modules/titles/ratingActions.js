import * as actionTypes from './actionTypes';
import backend from '../../backend';


export const findReviewCompleted = foundReview => ({
    type: actionTypes.FIND_REVIEW_COMPLETED,
    foundReview
});

export const createdReviewCompleted = newReview => ({
    type: actionTypes.CREATE_REVIEW_COMPLETED,
    newReview
});

export const clearFoundReview = () => ({
    type: actionTypes.CLEAR_FOUND_REVIEW
});

export const findMyReview = (id, onSuccess, onErrors) => dispatch => {
    backend.ratingService.getMyReview(id, 
        (foundReview) => {
            dispatch(findReviewCompleted(foundReview));
        }, onErrors);
}

export const rateTitle = (id, rating, onSuccess, onErrors) => dispatch => {
    backend.ratingService.rateTitle(id, rating, () => {
        dispatch(createdReviewCompleted({rating: rating, containsReview: false}));
        onSuccess();
    }, onErrors);
}

export const updateRating = (id, rating, onSuccess, onErrors) => dispatch => {
    backend.ratingService.updateRating(id, rating, () => {
        dispatch(createdReviewCompleted({rating: rating, containsReview: false}));
        onSuccess();
    }, onErrors);
}

export const deleteRating = (id, onSuccess, onErrors) => () => {
    backend.ratingService.deleteRating(id, onSuccess, onErrors);
}

export const rateExternalMovie = (id, rating, onSuccess, onErrors) => dispatch => {
    backend.ratingService.rateExternalMovie(id, rating, () => {
        dispatch(createdReviewCompleted({rating: rating, containsReview: false}));
        onSuccess();
    }, onErrors);
}

export const rateExternalTVShow = (id, rating, onSuccess, onErrors) => dispatch => {
    backend.ratingService.rateExternalTVShow(id, rating, () => {
        dispatch(createdReviewCompleted({rating: rating, containsReview: false}));
        onSuccess();
    }, onErrors);
}



export const addNewReview = (id, review, onSuccess, onErrors) => dispatch => {
    backend.ratingService.addNewReview(id, review,
        (foundReview) => {
            dispatch(createdReviewCompleted(foundReview));
            onSuccess();
        }, onErrors);
}

export const editReview = (id, review, onSuccess, onErrors) => dispatch => {
    backend.ratingService.editReview(id, review,
    () => {
        dispatch(createdReviewCompleted(review));
        onSuccess();
    }, onErrors);
}

export const deleteReview = (id, onSuccess, onErrors) => () => {
    backend.ratingService.deleteReview(id, onSuccess, onErrors);
}

export const addNewMoviewReview = (id, review, onSuccess, onErrors) => dispatch => {
    backend.ratingService.addNewMoviewReview(id, review,
        (foundReview) => {
            dispatch(createdReviewCompleted(foundReview));
            onSuccess();
        }, onErrors);
}

export const addNewTVShowReview = (id, review, onSuccess, onErrors) => dispatch => {
    backend.ratingService.addNewTVShowReview(id, review, 
        (foundReview) => {
            dispatch(createdReviewCompleted(foundReview));
            onSuccess();
        }, onErrors);
}



export const searchReviewsCompleted = foundReviews => ({
    type: actionTypes.SEARCH_REVIEWS_COMPLETED,
    foundReviews
});

export const clearReviewSearch = () => ({
    type: actionTypes.CLEAR_REVIEW_SEARCH
});

export const findReviews = criteria => dispatch => {
    backend.ratingService.findReviews(criteria,
        result => dispatch(searchReviewsCompleted({criteria, result})));
}

export const addedLikeCompleted = () => ({
    type: actionTypes.ADD_LIKE_COMPLETED
});

export const removedLikeCompleted = () => ({
    type: actionTypes.REMOVE_LIKE_COMPLETED
});

export const addLike = (id, onSuccess, onErrors) => dispatch => {
    backend.ratingService.addLike(id, () => {
        dispatch(addedLikeCompleted());
        onSuccess();
    }, onErrors);
}

export const removeLike = (id, onSuccess, onErrors) => dispatch => {
    backend.ratingService.removeLike(id, () => {
        dispatch(removedLikeCompleted());
        onSuccess();
    }, onErrors);
}