export const GET_TITLE_COMPLETED = "project/titles/findTitleByIdCompleted";
export const CLEAR_FOUND_TITLE = "project/titles/clearFoundTitle";

export const FIND_SEASONS_COMPLETED = "project/titles/findSeasonsCompleted";
export const CLEAR_FOUND_SEASONS = "project/titles/clearFoundSeasons";
export const FIND_SEASON_COMPLETED = "project/titles/findSeasonByNumberCompletes";
export const CLEAR_FOUND_SEASON = "project/titles/clearFoundSeason";

export const CREATED_TITLE_COMPLETED = "project/titles/createTitleCompleted";
export const UPDATE_TITLE_COMPLETED = "project/titles/updateTitleCompleted";

export const CREATED_EPISODES_COMPLETED = "project/titles/createEpisodesCompleted";
export const CREATED_SEASON_COMPLETED = "project/titles/createSeasonCompleted";

export const CLEAR_FOUND_EPISODE = "project/titles/clearFoundEpisode";
export const FIND_EPISODE_COMPLETED = "project/titles/findEpisodeCompleted";

export const UPDATE_EPISODE_COMPLETED = "project/titles/updateEpisodeCompleted";
export const UPDATE_SEASON_COMPLETED = "project/titles/updateSeasonCompleted";

export const CREATED_CAST_COMPLETED = "project/titles/createCastCompleted";
export const UPDATE_CAST_COMPLETED = "project/titles/updateCastCompleted";
export const DELETE_CAST_COMPLETED = "project/titles/deleteCastCompleted";

export const FIND_CAST_COMPLETED = "project/titles/findCastCompleted";
export const CLEAR_FOUND_CAST = "project/titles/clearFoundCast";

export const FIND_PEOPLE_COMPLETED = "project/titles/findPeopleCompleted";

export const CREATED_CREW_COMPLETED = "project/titles/createCrewCompleted";
export const UPDATE_CREW_COMPLETED = "project/titles/updateCrewCompleted";
export const DELETE_CREW_COMPLETED = "project/titles/deleteCrewCompleted";

export const FIND_CREW_COMPLETED = "project/titles/findCrewCompleted";
export const CLEAR_FOUND_CREW = "project/titles/clearFoundCrew";

export const FIND_EXTERNAL_CAST_AND_CREW_COMPLETED = "project/titles/findExternalCastAndCrewCompleted";


export const FIND_REVIEW_COMPLETED = "project/titles/findReviewCompleted";
export const CLEAR_FOUND_REVIEW = "project/titles/clearFoundReview";
export const CREATE_REVIEW_COMPLETED = "project/titles/createdReviewCompleted";

export const SEARCH_REVIEWS_COMPLETED = "project/titles/searchReviewsCompleted";
export const CLEAR_REVIEW_SEARCH = "project/titles/clearReviewSearch";

export const ADD_LIKE_COMPLETED = "project/titles/addedLikeCompleted";
export const REMOVE_LIKE_COMPLETED = "project/titles/removedLikeCompleted";


export const GET_FOLLOWING_LIST_TITLE_INFO_COMPLETED = "project/titles/getFollowingListTitleInfoCompleted";
export const CLEAR_FOLLOWING_LIST_TITLE_INFO = "project/titles/clearFollowingListTitleInfo";

export const CREATE_FOLLOWING_LIST_TITLE_COMPLETED = "project/titles/addToListCompleted";
export const EDIT_FOLLOWING_LIST_TITLE_COMPLETED = "project/titles/editTitleFromListCompleted";
export const DELETE_FOLLOWING_LIST_TITLE_COMPLETED = "project/titles/deleteFromListCompleted";
