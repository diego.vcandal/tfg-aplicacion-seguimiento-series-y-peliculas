import * as actionTypes from './actionTypes';
import backend from '../../backend';

export const getFollowingListTitleInfoCompleted = (newListTitle) => ({
    type: actionTypes.GET_FOLLOWING_LIST_TITLE_INFO_COMPLETED,
    newListTitle
});

export const clearFollowingListTitleInfo = () => ({
    type: actionTypes.CLEAR_FOLLOWING_LIST_TITLE_INFO
});

export const getFollowingListTitleInfo = (id, onErrors) => dispatch => {
    backend.followingListService.getFollowingListTitleInfo(id, 
        (newListTitle) => {
            dispatch(getFollowingListTitleInfoCompleted(newListTitle));
        }, onErrors);
}

export const addToListCompleted = (newListTitle) => ({
    type: actionTypes.CREATE_FOLLOWING_LIST_TITLE_COMPLETED,
    newListTitle
});

export const editTitleFromListCompleted = (newListTitle) => ({
    type: actionTypes.EDIT_FOLLOWING_LIST_TITLE_COMPLETED,
    newListTitle
});

export const deleteFromListCompleted = () => ({
    type: actionTypes.DELETE_FOLLOWING_LIST_TITLE_COMPLETED
});


export const addToList = (id, params, onSuccess, onErrors) => dispatch => {
    backend.followingListService.addToList(id, params,
        () => {
            dispatch(addToListCompleted({titleId: id, status: params.status}));
            onSuccess();
        }, onErrors);
}

export const editTitleFromList = (id, params, onSuccess, onErrors) => dispatch => {
    backend.followingListService.editTitleFromList(id, params,
        () => {
            dispatch(editTitleFromListCompleted({...params, titleId: id}));
            onSuccess();
        }, onErrors);
}

export const deleteTitleFromList = (id, onSuccess, onErrors) => dispatch => {
    backend.followingListService.deleteTitleFromList(id,
        () => {
            dispatch(deleteFromListCompleted());
            onSuccess();
        }, onErrors);
}


export const addExternalMovieToList = (id, params, onSuccess, onErrors) => dispatch => {
    backend.followingListService.addExternalMovieToList(id, params,
        () => {
            dispatch(addToListCompleted({titleId: id, status: params.status}));
            onSuccess();
        }, onErrors);
}

export const addExternalTVShowToList = (id, params, onSuccess, onErrors) => dispatch => {
    backend.followingListService.addExternalTVShowToList(id, params,
        () => {
            dispatch(addToListCompleted({titleId: id, status: params.status}));
            onSuccess();
        }, onErrors);
}