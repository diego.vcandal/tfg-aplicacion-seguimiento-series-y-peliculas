import * as actionTypes from './actionTypes';
import backend from '../../backend';

const findTitleByIdCompleted = foundTitle => ({
    type: actionTypes.GET_TITLE_COMPLETED,
    foundTitle
});

export const findMovieById = (id, onSuccess, onErrors) => dispatch => {

    backend.titleService.findMovie(id,
        (foundTitle) => {
            dispatch(findTitleByIdCompleted(foundTitle));
        },
        onErrors);

}

export const findTVShowById = (id, onSuccess, onErrors) => dispatch => {

    backend.titleService.findTVShow(id,
        (foundTitle) => {
            dispatch(findTitleByIdCompleted(foundTitle));
        },
        onErrors);

}

export const clearFoundTitle = () => ({
    type: actionTypes.CLEAR_FOUND_TITLE
});



const findSeasonsCompleted = foundSeasons => ({
    type: actionTypes.FIND_SEASONS_COMPLETED,
    foundSeasons
});

export const findSeasons = (id, onErrors) => dispatch => {

    backend.titleService.findSeasons(id,
        foundSeasons => dispatch(findSeasonsCompleted(foundSeasons)),
        onErrors);

}

export const clearFoundSeasons = () => ({
    type: actionTypes.CLEAR_FOUND_SEASONS
});

export const findLastSeason = (id, onErrors) => dispatch => {

    backend.titleService.findLastSeason(id,
        foundSeasons => dispatch(findSeasonsCompleted(foundSeasons)),
        onErrors);

}

const findSeasonByNumberCompleted = foundSeason => ({
    type: actionTypes.FIND_SEASON_COMPLETED,
    foundSeason
});

export const findSeasonByNumber = (id, seasonNumber, onErrors) => dispatch => {

    backend.titleService.findSeasonByNumber(id, seasonNumber,
        foundSeason => dispatch(findSeasonByNumberCompleted(foundSeason)),
        onErrors);

}

export const clearFoundSeason = () => ({
    type: actionTypes.CLEAR_FOUND_SEASON
});



export const findExternalMovieById = (id, onSuccess, onErrors) => dispatch => {

    backend.titleService.findExternalMovie(id,
        foundTitle => {
            dispatch(findTitleByIdCompleted(foundTitle));
        },
        onErrors);

}

export const findExternalTVShowById = (id, onSuccess, onErrors) => dispatch => {

    backend.titleService.findExternalTVShow(id,
        foundTitle => {
            dispatch(findTitleByIdCompleted(foundTitle));
        },
        onErrors);

}

export const findLastExternalSeason = (id, onErrors) => dispatch => {

    backend.titleService.findLastExternalSeason(id,
        foundSeasons => dispatch(findSeasonsCompleted(foundSeasons)),
        onErrors);

}

export const findExternalSeasons = (id, onErrors) => dispatch => {

    backend.titleService.findExternalSeasons(id,
        foundSeasons => dispatch(findSeasonsCompleted(foundSeasons)),
        onErrors);

}

export const findExternalSeasonByNumber = (id, seasonNumber, onErrors) => dispatch => {

    backend.titleService.findExternalSeasonByNumber(id, seasonNumber,
        foundSeason => dispatch(findSeasonByNumberCompleted(foundSeason)),
        onErrors);

}

export const createTitleCompleted = () => ({
    type: actionTypes.CREATED_TITLE_COMPLETED,
});

export const createMovie = (movie, onSuccess, onErrors) => dispatch => {
    backend.titleService.createMovie(movie, 
        ({id}) => {
            dispatch(createTitleCompleted());
            onSuccess(id);
        }, onErrors);
}

export const createTVShow = (tvshow, onSuccess, onErrors) => dispatch => {
    backend.titleService.createTVShow(tvshow, 
        ({id}) => {
            dispatch(createTitleCompleted());
            onSuccess(id);
        }, onErrors);
}


export const findFullInternalMovieById = (id, onErrors) => dispatch => {

    backend.titleService.findFullInternalMovie(id,
        foundTitle => dispatch(findTitleByIdCompleted(foundTitle)),
        onErrors);

}

export const findFullInternalTVShowById = (id, onErrors) => dispatch => {

    backend.titleService.findFullInternalTVShow(id,
        foundTitle => dispatch(findTitleByIdCompleted(foundTitle)),
        onErrors);

}

export const updateTitleCompleted = () => ({
    type: actionTypes.UPDATE_TITLE_COMPLETED,
});

export const updateMovie = (movie, onSuccess, onErrors) => dispatch => {
    backend.titleService.updateMovie(movie, 
        () => {
            dispatch(updateTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const updateTVShow = (tvshow, onSuccess, onErrors) => dispatch => {
    backend.titleService.updateTVShow(tvshow, 
        () => {
            dispatch(updateTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const makePublic = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.makePublic(id, 
        () => {
            dispatch(updateTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const changeSourceType = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.changeSourceType(id, 
        () => {
            dispatch(updateTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const setForDeletion = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.setForDeletion(id, 
        () => {
            dispatch(updateTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const createCacheMovie = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.createCacheMovie(id, 
        () => {
            dispatch(createTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const createCacheTVShow = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.createCacheTVShow(id, 
        () => {
            dispatch(createTitleCompleted());
            onSuccess();
        }, onErrors);
}

export const createSeasonCompleted = () => ({
    type: actionTypes.CREATED_SEASON_COMPLETED,
});

export const createEpisodesCompleted = () => ({
    type: actionTypes.CREATED_EPISODES_COMPLETED,
});

export const createCacheSeason = (id, seasonNumber, params, onSuccess, onErrors) => dispatch => {
    backend.titleService.createCacheSeason(id, seasonNumber, params,
        () => {
            dispatch(createSeasonCompleted());
            onSuccess();
        }, onErrors);
}

export const createCacheEpisode = (id, seasonNumber, episodeNumber, params, onSuccess, onErrors) => dispatch => {
    backend.titleService.createCacheEpisode(id, seasonNumber, episodeNumber, params, 
        () => {
            dispatch(createEpisodesCompleted());
            onSuccess();
        }, onErrors);
}

export const createCacheAllEpisodes = (id, seasonNumber, params, onSuccess, onErrors) => dispatch => {
    backend.titleService.createCacheAllEpisodes(id, seasonNumber, params,
        () => {
            dispatch(createEpisodesCompleted());
            onSuccess();
        }, onErrors);
}

export const createSeason = (id, params, onSuccess, onErrors) => dispatch => {
    backend.titleService.createSeason(id, params,
        () => {
            dispatch(createSeasonCompleted());
            onSuccess();
        }, onErrors);
}

export const createEpisode = (id, seasonNumber, params, onSuccess, onErrors) => dispatch => {
    backend.titleService.createEpisode(id, seasonNumber, params, 
        () => {
            dispatch(createEpisodesCompleted());
            onSuccess();
        }, onErrors);
}

export const findFullInternalSeasonId = (id, onErrors) => dispatch => {

    backend.titleService.findFullInternalSeason(id,
        foundSeason => dispatch(findSeasonByNumberCompleted(foundSeason)),
        onErrors);

}

export const clearFoundEpisode = () => ({
    type: actionTypes.CLEAR_FOUND_EPISODE
});

const findEpisodeCompleted = foundEpisode => ({
    type: actionTypes.FIND_EPISODE_COMPLETED,
    foundEpisode
});

export const findFullInternalEpisodeById = (id, onErrors) => dispatch => {

    backend.titleService.findFullInternalEpisode(id,
        foundEpisode => dispatch(findEpisodeCompleted(foundEpisode)),
        onErrors);

}

export const updateSeasonCompleted = () => ({
    type: actionTypes.UPDATE_SEASON_COMPLETED,
});

export const updateSeason = (season, onSuccess, onErrors) => dispatch => {
    backend.titleService.updateSeason(season, 
        () => {
            dispatch(updateSeasonCompleted());
            onSuccess();
        }, onErrors);
}

export const updateEpisodeCompleted = () => ({
    type: actionTypes.UPDATE_EPISODE_COMPLETED,
});

export const updateEpisode = (episode, onSuccess, onErrors) => dispatch => {
    backend.titleService.updateEpisode(episode, 
        () => {
            dispatch(updateEpisodeCompleted());
            onSuccess();
        }, onErrors);
}

export const setSeasonForDeletion = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.setSeasonForDeletion(id, 
        () => {
            dispatch(updateSeasonCompleted());
            onSuccess();
        }, onErrors);
}


export const setEpisodeForDeletion = (id, onSuccess, onErrors) => dispatch => {
    backend.titleService.setEpisodeForDeletion(id, 
        () => {
            dispatch(updateEpisodeCompleted());
            onSuccess();
        }, onErrors);
}
