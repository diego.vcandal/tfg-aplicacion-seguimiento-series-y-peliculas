/* eslint-disable import/no-anonymous-default-export */
import * as actions from './actions';
import * as actionTypes from './actionTypes';
import reducer from './reducer';
import * as selectors from './selectors';

export {default as Movie} from './components/Movie';
export {default as TVShow} from './components/TVShow';
export {default as AllSeasons} from './components/AllSeasons';
export {default as SeasonDetails} from './components/SeasonDetails';
export {default as CreateTitle} from './components/CreateAndEdit/CreateTitle';
export {default as EditTitle} from './components/CreateAndEdit/EditTitle';
export {default as CreateSeason} from './components/CreateAndEdit/CreateSeason';
export {default as CreateEpisode} from './components/CreateAndEdit/CreateEpisode';
export {default as EditSeason} from './components/CreateAndEdit/EditSeason';
export {default as EditEpisode} from './components/CreateAndEdit/EditEpisode';
export {default as AllCastAndCrew} from './components/AllCastAndCrew';
export {default as AllReviews} from './components/AllReviews';

export default {actions, actionTypes, reducer, selectors};