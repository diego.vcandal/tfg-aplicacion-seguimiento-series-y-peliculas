import * as actionTypes from './actionTypes';
import backend from '../../backend';

export const createCastCompleted = (newCast) => ({
    type: actionTypes.CREATED_CAST_COMPLETED,
    newCast
});

export const updateCastCompleted = (newCast) => ({
    type: actionTypes.UPDATE_CAST_COMPLETED,
    newCast
});

export const deleteCastCompleted = (deletedCastId) => ({
    type: actionTypes.DELETE_CAST_COMPLETED,
    deletedCastId
});


export const createCast = (cast, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.createCast(cast, 
        ({id}) => {
            dispatch(createCastCompleted({...cast, id: id}));
            onSuccess(id);
        }, onErrors);
}

export const updateCast = (cast, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.updateCast(cast, 
        () => {
            dispatch(updateCastCompleted(cast));
            onSuccess();
        }, onErrors);
}

export const deleteCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.deleteCast(id, 
        () => {
            dispatch(deleteCastCompleted(id));
            onSuccess();
        }, onErrors);
}


export const createEpisodeCastCompleted = (newCast) => ({
    type: actionTypes.CREATED_CAST_COMPLETED,
    newCast
});

export const deleteEpisodeCastCompleted = (deletedCastId) => ({
    type: actionTypes.DELETE_CAST_COMPLETED,
    deletedCastId
});

export const createEpisodeCast = (cast, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.createEpisodeCast(cast, 
        ({id}) => {
            dispatch(createEpisodeCastCompleted({...cast, id: id}));
            onSuccess(id);
        }, onErrors);
}

export const deleteEpisodeCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.deleteEpisodeCast(id, 
        () => {
            dispatch(deleteEpisodeCastCompleted(id));
            onSuccess();
        }, onErrors);
}


export const findCastCompleted = foundCast => ({
    type: actionTypes.FIND_CAST_COMPLETED,
    foundCast
});

export const clearFoundCast = () => ({
    type: actionTypes.CLEAR_FOUND_CAST
});


export const findMainCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findMainCast(id, 
        (foundCast) => {
            dispatch(findCastCompleted(foundCast));
            onSuccess()
        }, onErrors);
}

export const findAllCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findAllCast(id, 
        (foundCast) => {
            dispatch(findCastCompleted(foundCast));
            onSuccess();
        }, onErrors);
}

export const findInternalCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findInternalCast(id, 
        (foundCast) => {
            dispatch(findCastCompleted(foundCast));
        }, onErrors);
}

export const findEpisodeCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findEpisodeCast(id, 
        (foundCast) => {
            dispatch(findCastCompleted(foundCast));
        }, onErrors);
}

const findPeopleCompleted = () => ({
    type: actionTypes.FIND_PEOPLE_COMPLETED
})

export const findPeople = (name, onSuccess) => dispatch => {
    backend.peopleService.findPeople(name,
        (foundPeople) => {
            dispatch(findPeopleCompleted());
            onSuccess(foundPeople);
        });
}




export const createCrewCompleted = (newCrew) => ({
    type: actionTypes.CREATED_CREW_COMPLETED,
    newCrew
});

export const updateCrewCompleted = (newCrew) => ({
    type: actionTypes.UPDATE_CREW_COMPLETED,
    newCrew
});

export const deleteCrewCompleted = (deletedCrewId) => ({
    type: actionTypes.DELETE_CREW_COMPLETED,
    deletedCrewId
});


export const createCrew = (crew, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.createCrew(crew, 
        ({id}) => {
            dispatch(createCrewCompleted({...crew, id: id}));
            onSuccess(id);
        }, onErrors);
}

export const updateCrew = (crew, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.updateCrew(crew, 
        () => {
            dispatch(updateCrewCompleted(crew));
            onSuccess();
        }, onErrors);
}

export const deleteCrew = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.deleteCrew(id, 
        () => {
            dispatch(deleteCrewCompleted(id));
            onSuccess();
        }, onErrors);
}

export const createEpisodeCrewCompleted = (newCrew) => ({
    type: actionTypes.CREATED_CREW_COMPLETED,
    newCrew
});

export const deleteEpisodeCrewCompleted = (deletedCrewId) => ({
    type: actionTypes.DELETE_CREW_COMPLETED,
    deletedCrewId
});

export const createEpisodeCrew = (crew, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.createEpisodeCrew(crew, 
        ({id}) => {
            dispatch(createEpisodeCrewCompleted({...crew, id: id}));
            onSuccess(id);
        }, onErrors);
}

export const deleteEpisodeCrew = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.deleteEpisodeCrew(id, 
        () => {
            dispatch(deleteEpisodeCrewCompleted(id));
            onSuccess();
        }, onErrors);
}


export const findCrewCompleted = foundCrew => ({
    type: actionTypes.FIND_CREW_COMPLETED,
    foundCrew
});

export const clearFoundCrew = () => ({
    type: actionTypes.CLEAR_FOUND_CREW
});

export const findAllCrew = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findAllCrew(id, 
        (foundCrew) => {
            dispatch(findCrewCompleted(foundCrew));
            onSuccess();
        }, onErrors);
}

export const findInternalCrew = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findInternalCrew(id, 
        (foundCrew) => {
            dispatch(findCrewCompleted(foundCrew));
        }, onErrors);
}

export const findEpisodeCrew = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findEpisodeCrew(id, 
        (foundCrew) => {
            dispatch(findCrewCompleted(foundCrew));
        }, onErrors);
}


export const findMainExternalMovieCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findMainExternalMovieCast(id, 
        (foundCast) => {
            dispatch(findCastCompleted(foundCast));
            onSuccess();
        }, onErrors);
}

export const findMainExternalTVShowCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findMainExternalTVShowCast(id, 
        (foundCast) => {
            dispatch(findCastCompleted(foundCast));
            onSuccess();
        }, onErrors);
}

export const findExternalCastAndCrewCompleted = foundCastAndCrew => ({
    type: actionTypes.FIND_EXTERNAL_CAST_AND_CREW_COMPLETED,
    foundCastAndCrew
});

export const findAllExternalMovieCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findAllExternalMovieCast(id, 
        (foundCastAndCrew) => {
            dispatch(findExternalCastAndCrewCompleted(foundCastAndCrew));
            onSuccess();
        }, onErrors);
}

export const findAllExternalTVShowCast = (id, onSuccess, onErrors) => dispatch => {
    backend.castAndCrewService.findAllExternalTVShowCast(id, 
        (foundCastAndCrew) => {
            dispatch(findExternalCastAndCrewCompleted(foundCastAndCrew));
            onSuccess();
        }, onErrors);
}

