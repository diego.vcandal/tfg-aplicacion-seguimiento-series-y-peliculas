import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    foundTitle: null,
    foundFollowingListTitle: null,
    foundSeasons: null,
    foundSeason: null,
    foundEpisode: null,
    foundCast: null,
    foundCrew: null,
    foundReview: null,
    foundReviews: null,
};

const foundTitle = (state = initialState.foundTitle, action) => {

    switch (action.type) {

        case actionTypes.GET_TITLE_COMPLETED:
            return action.foundTitle;
            
        case actionTypes.CLEAR_FOUND_TITLE:
            return initialState.foundTitle;

        default:
            return state;

    }

}

const foundFollowingListTitle = (state = initialState.foundFollowingListTitle, action) => {

    switch (action.type) {

        case actionTypes.GET_FOLLOWING_LIST_TITLE_INFO_COMPLETED:
            return action.newListTitle;
            
        case actionTypes.CLEAR_FOLLOWING_LIST_TITLE_INFO:
            return initialState.foundFollowingListTitle;

        case actionTypes.CREATE_FOLLOWING_LIST_TITLE_COMPLETED:
            return action.newListTitle;

        case actionTypes.EDIT_FOLLOWING_LIST_TITLE_COMPLETED:
            return action.newListTitle;

        case actionTypes.DELETE_FOLLOWING_LIST_TITLE_COMPLETED:
            return initialState.foundFollowingListTitle;

        default:
            return state;

    }

}

const foundSeasons = (state = initialState.foundSeasons, action) => {

    switch (action.type) {

        case actionTypes.FIND_SEASONS_COMPLETED:
            return action.foundSeasons;
            
        case actionTypes.CLEAR_FOUND_SEASONS:
            return initialState.foundSeasons;

        default:
            return state;

    }

}

const foundSeason = (state = initialState.foundSeason, action) => {

    switch (action.type) {

        case actionTypes.FIND_SEASON_COMPLETED:
            return action.foundSeason;
            
        case actionTypes.CLEAR_FOUND_SEASON:
            return initialState.foundSeason;

        default:
            return state;

    }

}

const foundEpisode = (state = initialState.foundEpisode, action) => {

    switch (action.type) {

        case actionTypes.FIND_EPISODE_COMPLETED:
            return action.foundEpisode;
            
        case actionTypes.CLEAR_FOUND_EPISODE:
            return initialState.foundEpisode;

        default:
            return state;

    }

}

const foundCast = (state = initialState.foundCast, action) => {

    switch (action.type) {

        case actionTypes.FIND_CAST_COMPLETED:
            return action.foundCast;
            
        case actionTypes.CLEAR_FOUND_CAST:
            return initialState.foundCast;
        
        case actionTypes.CREATED_CAST_COMPLETED:
            return state == null ? null : [...state, action.newCast];
            
        case actionTypes.UPDATE_CAST_COMPLETED:
            return state.map(c => c.id === action.newCast.id ? action.newCast : c);
        
        case actionTypes.DELETE_CAST_COMPLETED:
            return state.filter(c => c.id !== action.deletedCastId);

        case actionTypes.FIND_EXTERNAL_CAST_AND_CREW_COMPLETED:
            return action.foundCastAndCrew.cast;

        default:
            return state;

    }

}

const foundCrew = (state = initialState.foundCrew, action) => {

    switch (action.type) {

        case actionTypes.FIND_CREW_COMPLETED:
            return action.foundCrew;
            
        case actionTypes.CLEAR_FOUND_CREW:
            return initialState.foundCrew;
        
        case actionTypes.CREATED_CREW_COMPLETED:
            return state == null ? null : [...state, action.newCrew];
            
        case actionTypes.UPDATE_CREW_COMPLETED:
            return state.map(c => c.id === action.newCrew.id ? action.newCrew : c);
        
        case actionTypes.DELETE_CREW_COMPLETED:
            return state.filter(c => c.id !== action.deletedCrewId);

        case actionTypes.FIND_EXTERNAL_CAST_AND_CREW_COMPLETED:
            return action.foundCastAndCrew.crew;

        default:
            return state;

    }

}

const foundReview = (state = initialState.foundReview, action) => {

    switch (action.type) {

        case actionTypes.FIND_REVIEW_COMPLETED:
            return action.foundReview;
            
        case actionTypes.CLEAR_FOUND_REVIEW:
            return initialState.foundReview;

        case actionTypes.CREATE_REVIEW_COMPLETED:
            return action.newReview;

        default:
            return state;

    }

}

const foundReviews = (state = initialState.foundReviews, action) => {

    switch (action.type) {

        case actionTypes.SEARCH_REVIEWS_COMPLETED:
            return action.foundReviews;
            
        case actionTypes.CLEAR_REVIEW_SEARCH:
            return initialState.foundReviews;

        default:
            return state;

    }

}


const reducer = combineReducers({
    foundTitle,
    foundFollowingListTitle,
    foundSeasons,
    foundSeason,
    foundEpisode,
    foundCast,
    foundCrew,
    foundReview,
    foundReviews
});

export default reducer;
