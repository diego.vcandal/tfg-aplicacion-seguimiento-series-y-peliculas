const getModuleState = state => state.lists;

export const getFoundList = state =>
    getModuleState(state).foundList;