import * as actionTypes from './actionTypes';
import backend from '../../backend';

export const createCustomListCompleted = () => ({
    type: actionTypes.CREATE_CUSTOM_LIST_COMPLETED,
});

export const createCustomList = (list, onSuccess, onErrors) => dispatch => {
    backend.customListService.createCustomList(list, 
        ({id}) => {
            dispatch(createCustomListCompleted());
            onSuccess(id);
        }, onErrors);
}

const findCustomListByIdCompleted = foundList => ({
    type: actionTypes.FIND_CUSTOM_LIST_COMPLETED,
    foundList
});

export const clearFoundCustomList = () => ({
    type: actionTypes.CLEAR_FOUND_CUSTOM_LIST
});

export const findCustomListById = (id, onErrors) => dispatch => {
    backend.customListService.getCustomList(id,
        (foundList) => dispatch(findCustomListByIdCompleted(foundList)),
        onErrors);
}


const findCustomListTitlesSearchCompleted = () => ({
    type: actionTypes.FIND_CUSTOM_LIST_TITLES_SEARCH_COMPLETED
})

export const findTitles = (titleName, onSuccess) => dispatch => {
    backend.customListService.searchMergedTitles(titleName,
        (foundTitles) => {
            dispatch(findCustomListTitlesSearchCompleted());
            onSuccess(foundTitles);
        });
}


const addTitleToListCompleted = () => ({
    type: actionTypes.ADD_TITLE_TO_LIST_COMPLETED
})

export const addTitleToList = (id, titleId, onSuccess, onErrors) => dispatch => {
    backend.customListService.addToList(id, titleId,
        () => {
            dispatch(addTitleToListCompleted());
            onSuccess();
        }, onErrors);
}

export const addExternalMovieToList = (id, titleId, onSuccess, onErrors) => dispatch => {
    backend.customListService.addExternalMovieToList(id, titleId,
        ({id}) => {
            dispatch(addTitleToListCompleted());
            onSuccess(id);
        }, onErrors);
}

export const addExternalTVShowToList = (id, titleId, onSuccess, onErrors) => dispatch => {
    backend.customListService.addExternalTVShowToList(id, titleId,
        ({id}) => {
            dispatch(addTitleToListCompleted());
            onSuccess(id);
        }, onErrors);
}


export const updateCustomListOrderCompleted = (newList) => ({
    type: actionTypes.UPDATE_CUSTOM_LIST_ORDER_COMPLETED,
    newList
});

export const updateCustomListOrder = (id, list, onSuccess, onErrors) => dispatch => {
    backend.customListService.updateCustomListOrder(id, list, 
        () => {
            dispatch(updateCustomListOrderCompleted(list));
            onSuccess(list);
        }, onErrors);
}

export const updateCustomListCompleted = newList => ({
    type: actionTypes.UPDATE_CUSTOM_LIST_COMPLETED,
    newList
});

export const updateCustomList = (list, onSuccess, onErrors) => dispatch => {
    backend.customListService.updateCustomList(list, 
        () => {
            dispatch(updateCustomListCompleted(list));
            onSuccess();
        }, onErrors);
}

export const deleteCustomListCompleted = () => ({
    type: actionTypes.DELETE_CUSTOM_LIST_COMPLETED
});

export const deleteCustomList = (id, onSuccess, onErrors) => dispatch => {
    backend.customListService.deleteCustomList(id, 
        () => {
            dispatch(deleteCustomListCompleted());
            onSuccess();
        }, onErrors);
}


const deleteFromListCompleted = () => ({
    type: actionTypes.DELETE_TITLE_TO_LIST_COMPLETED
})

export const deleteFromList = (id, titleId, onSuccess, onErrors) => dispatch => {
    backend.customListService.deleteFromList(id, titleId,
        () => {
            dispatch(deleteFromListCompleted());
            onSuccess();
        }, onErrors);
}

export const addedLikeToListCompleted = () => ({
    type: actionTypes.ADD_LIKE_TO_LIST_COMPLETED
});

export const removedLikeFromListCompleted = () => ({
    type: actionTypes.REMOVE_LIKE_FROM_LIST_COMPLETED
});

export const addLike = (id, onErrors) => dispatch => {
    backend.customListService.addLike(id, () => {
        dispatch(addedLikeToListCompleted());
    }, onErrors);
}

export const removeLike = (id, onErrors) => dispatch => {
    backend.customListService.removeLike(id, () => {
        dispatch(removedLikeFromListCompleted());
    }, onErrors);
}