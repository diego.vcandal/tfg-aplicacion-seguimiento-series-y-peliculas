import React, {useState, useEffect, useRef, useCallback } from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';

import {toast} from 'react-toastify';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import PropTypes from 'prop-types';

const CreateListTitles = ({update, madeChanges, setMadeChanges}) => {

    const dispatch = useDispatch();

    const foundList = useSelector(selectors.getFoundList);

    foundList.titles && foundList.titles.sort(function(a,b) {
        if (a.position < b.position) return -1;
        if (a.position > b.position) return 1;
        return 0;
    })
    
    const [list, setList]  = useState(foundList.titles);

    const [keywords, setKeywords] = useState('');
    const [searched, setSearched] = useState(false);
    const [titles, setTitles] = useState(null);

    const searchResultsPanel = useRef(null);
    const searchButton = useRef(null);

    const [backendErrors, setBackendErrors] = useState(null);

    let form;

    var arrow_down_image = `${process.env.PUBLIC_URL}/images/down_arrow.png`;
    var arrow_up_image = `${process.env.PUBLIC_URL}/images/up_arrow.png`;
    var external_api_imgage_path = "https://image.tmdb.org/t/p/w500/";
    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;

    const handleClick = useCallback((e) => {

        if(!searchResultsPanel.current || searchResultsPanel.current === null)
            return;
        
        if (searchResultsPanel.current.contains(e.target) || searchButton.current.contains(e.target))
            return;

        resetSearch();
    }, [searchResultsPanel, searchButton]);

    useEffect(() => {
        document.addEventListener("click", handleClick);  
        return () => document.removeEventListener("click", handleClick)
    }, [handleClick]);

    const resetSearch = () => {
        setSearched(false);
        setTitles(null);
    }

    const handleSubmit = event => {
        
        event && event.preventDefault();
        
        if (form.checkValidity()) {

        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const moveUpDown = (from, to) => {

        if (to < 0 || to > (list.length - 1))
            return;

        // JSON.parse(JSON.stringify( state-items )) --> deep copy to avoid state mutations
        let newList = JSON.parse(JSON.stringify(list));
        newList.splice(to, 0, newList.splice(from, 1)[0])

        newList[from].position = from + 1;
        newList[to].position = to + 1;

        setMadeChanges(true);
        setList(newList);
    }

    const moveToPosition = (title, to) => {

        let foundTitle = list.find(t => t.id === title.id);
        if (foundTitle && foundTitle.actualPosition === to)
            return;

        let boundsTo = to;

        if (to < 1)
            boundsTo = 1;

        if (to > (list.length))
            boundsTo = list.length;

        if ((foundTitle && foundTitle.actualPosition === boundsTo) || (to === '')){
            setList(list.map(t => t.id === title.id ? {...t, position: t.actualPosition} : t));
            return;
        }

        let newList = [];
        let index = 1;
        list.forEach(t => {

            if(index === boundsTo) {
                newList.push({...title, position: index, actualPosition: index});
                index++;
            }

            if (title.id !== t.id) {
                newList.push({...t, position: index, actualPosition: index});
                index++;
            }
        });

        if (boundsTo === list.length)
            newList.push({...title, position: list.length, actualPosition: list.length});

        setMadeChanges(true);    
        setList(newList);
    }

    const handleEnterMoveToPosition = (event, title, to) => {
        if (event.keyCode === 13){
            moveToPosition(title, to);
            document.activeElement.blur();
        }
    }

    const handleSearch = () => {

        if (keywords !== '')

        dispatch(actions.findTitles(keywords,
            foundTitles => {
                setSearched(true);
                setTitles(foundTitles.items);
            }));
    }

    const handleEnterClick = (event) => {
        resetSearch();
        if (event.keyCode === 13)
            handleSearch();
    }

    const handleSelect = (title) => {

        switch (Number(title.sourceType)){
            case 0:
            case 1:
                dispatch(actions.addTitleToList(foundList.id, title.id, () => {
                    addTitleToList(title);
                }, errors => toast.error(errors.globalError)))
                break;

            case 2:
                if (Number(title.titleType) === 0)
                    dispatch(actions.addExternalMovieToList(foundList.id, title.id, (id) => {
                        addTitleToList({...title, id: id});
                    }, errors => toast.error(errors.globalError)))
                else
                    dispatch(actions.addExternalTVShowToList(foundList.id, title.id, (id) => {
                        addTitleToList({...title, id: id});
                    }, errors => toast.error(errors.globalError)))
                break;
            default:
                break;
        }

        resetSearch();

    }

    const addTitleToList = (title) => {

        let newList = JSON.parse(JSON.stringify(list));

        let titleInList = newList.find(t => t.id === title.id);
            
        if (!titleInList) {
            let pos = (newList.length + 1);
            newList.push({...title, position: pos, actualPosition: pos}); 
            setList(newList);
        }

    }

    const deleteTitleFromList = (title) => {

        let index = 1;
        let newList = JSON.parse(JSON.stringify(list))
            .filter(t => t.id !== title.id)
            .map(function(t) {return {...t, position: index, actualPosition: index++}});

        setList(newList);

    }

    const getImage = (title) => {
        
        switch (Number(title.sourceType)){
            case 0:
                if (title.image)  
                    return title_image_path + title.id + "/images/portrait";
                else 
                    return title_default_image;
            
            case 1:
                return title_image_path + title.id + "/externalImage";

            case 2:
                if (title.imageExternalPath != null)
                    return external_api_imgage_path + title.imageExternalPath;
                else
                    return title_default_image;

            default:
                return title_default_image;
        }

    };

    const handlePositionChange = (title, e) => {
        const re = /^[0-9\b]+$/;

        if (e.target.value === '' || re.test(e.target.value))
            setList(list.map(t => t.id === title.id ? {...t, position: 
                (e.target.value === '' ? '' : Number(e.target.value))} : t));
        
    }

    if (!foundList.titles)
        return;

    let key = 1;
    let listKeys = 1;
    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col shadow-container p-3" noValidate onSubmit={e => handleSubmit(e)}>

                <div className="row input-group center-element">
                    <FormattedMessage id="project.list.create.title">
                        {
                            (msg) => <input type="text" id="keywords" className="form-control col-10 search-title-input-text"
                            onChange={e => setKeywords(e.target.value)}
                            autoFocus
                            onKeyUp={(e) => handleEnterClick(e)}
                            value={keywords}
                            placeholder={msg}
                            required />
                        }
                    </FormattedMessage>
                    
                    <button type="button" className="btn btn-primary col-2 " ref={searchButton}
                        onClick={(e) => handleSearch()}>
                        <FormattedMessage id='project.global.buttons.search'/>
                    </button>

                    <div ref={searchResultsPanel} className="col-12 p-0" >
                        {searched && titles && titles.length > 0 &&
                            <div className="col-10 container-border mt-0 add-title-search-container" >
                                {titles.map(title =>
                                    <div key={key++} className="row pb-2 pt-2 add-title-search-item link-pointer" onClick={() => handleSelect(title)}>
                                        <div className="col-1 image-user-details-container">
                                            <img className="image-title-add-search" alt="person portrait"
                                                src={getImage(title)}
                                                onError={e => {e.target.src = title_default_image}}/>
                                        </div>
                                        <div className="col ml-2">
                                            <p className="search-title-add-search-text">
                                                {title.titleName ? title.titleName : title.originalTitle}
                                            </p>
                                        </div>
                                    </div>
                                )}
                            </div>
                        }
                    </div>
                    
                </div>
                <div className="col">
                    <div className="row mt-5 mb-3">
                        <h4 className="font-weight-600 light-gray-dark col p-0 ml-1">
                            <FormattedMessage id="project.list.create.elements"/>
                        </h4>
                        <button className="btn btn-success" type="button" title="Update"
                            onClick={() => madeChanges && dispatch(actions.updateCustomListOrder(foundList.id, list, 
                                () => {
                                    setMadeChanges(false); 
                                    toast.success(<FormattedMessage id="project.list.create.updateToast"/>);
                                }, errors => setBackendErrors(errors)))}>
                                <FormattedMessage id="project.list.create.updateOrder"/>
                        </button>
                    </div>
                    <div className="row">
                        <div className="col-10 center-element">
                            {list.map(title =>

                                <div key={listKeys++} className="row custom-list-edit-elements-container w-100 p-2 mb-3">
                                
                                    <span className="custom-list-edit-elements-delete-button">
                                        <button className="btn btn-danger" type="button" title="Delete"
                                            onClick={() => 
                                                dispatch(actions.deleteFromList(foundList.id, title.id, () => {
                                                    deleteTitleFromList(title);
                                                }, errors => setBackendErrors(errors)))}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg center-v-button-icon" viewBox="0 0 16 16">
                                                <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
                                            </svg>
                                        </button>
                                    </span>

                                    <div className="col-2 title-list-edit-arrow-container">
                                        <img className="row title-list-edit-arrows center-element p-2 link-pointer" alt="arrow up"
                                            onClick={() => moveUpDown(title.position - 1, title.position - 2)}
                                            src={arrow_up_image}/>
                                        <input  id="id" className="form-control center-element p-0"
                                            value={title.position}
                                            onBlur={() => moveToPosition(title, title.position)}
                                            onChange={(e) => handlePositionChange(title, e)}
                                            onKeyUp={(e) => handleEnterMoveToPosition(e, title, title.position)}
                                            min={1}/>
                                        <img className="row title-list-edit-arrows center-element p-2 link-pointer" alt="arrow down"
                                            onClick={() => moveUpDown(title.position - 1, title.position)}
                                            src={arrow_down_image}/>
                                    </div>
                                    <div className="col-2 label-v-class">
                                        <img className="row image-title-list-edit-panel center-element" alt="title portrait"
                                            onClick={() => handleSelect(title)}
                                            src={getImage(title)}
                                            onError={e => {e.target.src = title_default_image}}/>
                                    </div>
                                    <div className="col p-0">
                                        <p className="text-title-list-edit-panel mb-1">
                                            {title.titleName ? title.titleName : title.originalTitle}
                                        </p>

                                        {title.sourceType !== 0 &&
                                            <span className="TMDB-type-tag-details font-weight-600 mr-2">
                                                <FormattedMessage id='project.global.type.TMDB'/>
                                            </span>
                                        }
                                        
                                        <span className="title-type-tag-details font-weight-600 mr-2">
                                            {title.titleType === 0 ?
                                                <FormattedMessage id='project.global.type.movie'/>
                                            :
                                                <FormattedMessage id='project.global.type.tv'/>
                                            }
                                        </span>

                                    </div>
                                </div>

                            )}
                        </div>
                    </div>
                </div>
            </form>

        </div>
    );

}

CreateListTitles.propTypes = {
    update: PropTypes.bool.isRequired,
    madeChanges: PropTypes.bool.isRequired,
    setMadeChanges: PropTypes.func.isRequired
}

export default CreateListTitles;