/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import ReactTooltip from "react-tooltip";

const CustomListTitle = ({titles}) => {

    const history = useHistory();

    var title_default_image = `${process.env.PUBLIC_URL}/images/title_default_image.png`;
    var title_image_path = `${process.env.REACT_APP_BACKEND_URL}/titles/`;
    var external_api_image_path = "https://image.tmdb.org/t/p/w500/";

    const getImage = (title) => {
        
        switch (Number(title.sourceType)){
            case 0:
                if (title.image)  
                    return title_image_path + title.id + "/images/portrait";
                else 
                    return title_default_image;
            
            case 1:
                return title_image_path + title.id + "/externalImage";

            case 2:
                if (title.imageExternalPath != null)
                    return external_api_image_path + title.imageExternalPath;
                else
                    return title_default_image;

            default:
                return title_default_image;
        }

    };

    const getLink = (title) => {
        
        switch (Number(title.titleType)){
            case 0:
                history.push('/movies/' + title.id);
                break;

            case 1:
                history.push('/tvshows/' + title.id);
                break;

            default:
                break;
        }

    };

    if (!titles) {
        return null;
    }

    titles.sort(function(a,b) {
        if (a.position < b.position) return -1;
        if (a.position > b.position) return 1;
        return 0;
    })

    return(
        <div className="row p-0 center-element custom-list-elements">

            {titles.map(title => 
                <div className="mb-4 pl-3 pr-3" data-tip={title.titleName ? title.titleName : title.originalTitle} >
                    <ReactTooltip place="top" type="dark" effect="solid"/>
                    <div className="col p-0 custom-list-shadow-container center-element">
                        <div>
                            <img className="custom-list-image link-pointer" 
                                alt="title poster image" src={getImage(title)} 
                                onError={e => {e.target.src = title_default_image}}
                                onClick={() => getLink(title)} />
                        </div>

                        <div className="custom-list-bottom-info">
                            <span>{title.position}</span>

                            {title.timesRated > 0 &&
                                <span className="custom-list-bottom-rating text-right">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-fill mr-2" viewBox="0 0 16 16">
                                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                    </svg>
                                    {title.avgRating}
                                </span>
                            }
                        </div>
                        
                        
                    </div>
                </div>

            )}
        </div>
    );

};

CustomListTitle.propTypes = {
    titles: PropTypes.array.isRequired,
};

export default CustomListTitle;