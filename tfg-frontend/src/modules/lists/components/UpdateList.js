/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useParams, useHistory, useLocation} from 'react-router-dom';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';

import CreateList from './CreateList';
import PropTypes from 'prop-types';
import LooseChanges from '../../common/components/LooseChanges';
import CreateListTitles from './CreateListTitles';

const UpdateList = ({create}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const query = new URLSearchParams(useLocation().search);
    let panel = query.get("panel");
    
    const [isInitialPanel, setIsInitialPanel] = useState(true);
    const [actualPanel, setActualPanel] = useState(panel === "1" ? 1 : 0);
    const {listId} = useParams();
    const [backendErrors, setBackendErrors] = useState(null);
    const [madeChanges, setMadeChanges] = useState(false);   

    const foundList = useSelector(selectors.getFoundList);

    let dialog, selectedCode;
    let dialogId = "looseAllChanges";
    let list = [{id: 0, ref: null}, 
        {id: 1, ref: null}];

    const baseClass = "list-group-item list-group-item-action col";

    useEffect(() => {

        if (!create) {
            const foundListId = Number(listId);

            if (!Number.isNaN(foundListId)) {
                dispatch(actions.findCustomListById(foundListId));
            }
        }

        return () => dispatch(actions.clearFoundCustomList());

    }, [listId, create, dispatch]);

    
    const handleUpdatePanel = (code) => {
        selectedCode = code;

        if (madeChanges) 
            dialog.click();
        else
            onDialogConfirm();

    };

    if (!create && !foundList) {
        return null;
    }

    const onDialogConfirm = () => {
        setActualPanel(selectedCode);
        setIsInitialPanel(false);
        list.map(e => e.id === selectedCode ? e.ref.className = baseClass + " active" : e.ref.className = baseClass)
        setMadeChanges(false);
    };

    return (
        <div className="row mt-4">

            {!create &&
                <div className="row w-100 mb-4 ml-0 ">
                    <span className="font-size-update-title">
                        <a className="link-primary" onClick={() => history.push("/lists/" + foundList.id)}>
                            {foundList.title}
                        </a>
                    </span>
                </div>
            }

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <div className="side-update-menu-container shadow-container col-2 list-group">

                <button type="button" className={baseClass + (((isInitialPanel && panel !== "1") || (!isInitialPanel && actualPanel === 0)) ? " active" : "")} ref={node => list[0].ref = node}
                    onClick={e => handleUpdatePanel(0)}>
                    <div className="col-10">
                        <FormattedMessage id='project.list.update.menu.details'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

                <button type="button" className={baseClass + (((isInitialPanel && panel === "1") || (!isInitialPanel && actualPanel === 1)) ? " active" : "")} ref={node => list[1].ref = node}
                    onClick={e => handleUpdatePanel(1)} disabled={create}>
                    <div className="col-10">
                        <FormattedMessage id='project.list.update.menu.elements'/>
                    </div>
                    <div className="col-2">➤</div>
                </button>

            </div>

            {((isInitialPanel && panel !== "1") || (!isInitialPanel && actualPanel === 0)) &&
                <CreateList update={!create} setMadeChanges={setMadeChanges}/>
            }

            {((isInitialPanel && panel === "1") || (!isInitialPanel && actualPanel === 1)) &&
                <CreateListTitles update={!create} setMadeChanges={setMadeChanges} madeChanges={madeChanges}/>
            }

            <button className="btn btn-danger button-align-right button-width-50" 
                ref={node => dialog = node} hidden={true}
                data-toggle="modal" data-target={"#" + dialogId}>
            </button >
            
            <LooseChanges id={dialogId} onConfirm={onDialogConfirm}></LooseChanges>

        </div>

    );

}

UpdateList.propTypes = {
    create: PropTypes.bool.isRequired
}

export default UpdateList;