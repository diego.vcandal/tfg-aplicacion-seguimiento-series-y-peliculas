import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useHistory} from 'react-router-dom';

import {toast} from 'react-toastify';

import {Errors} from '../../common';
import * as actions from '../actions';
import * as selectors from '../selectors';
import PropTypes from 'prop-types';

const CreateList = ({update, setMadeChanges}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const foundList = useSelector(selectors.getFoundList);
    const canUpdate = update && foundList;

    const list_default_image = `${process.env.PUBLIC_URL}/images/list_default_image.png`;
    let list_image;

    if(foundList && foundList.hasImage)
        list_image = `${process.env.REACT_APP_BACKEND_URL}/lists/${foundList.id}/image`;

    const [visible, setVisible]  = useState(foundList ? foundList.visible : false);
    const [title, setTitle]  = useState(foundList ? foundList.title : '');
    const [description, setDescription]  = useState(foundList ? foundList.description : '');
    const [image, setImage]  = useState(canUpdate && foundList.hasImage ? list_image : list_default_image);

    const [backendErrors, setBackendErrors] = useState(null);

    let form;

    const handleSubmit = event => {
        
        event && event.preventDefault();
        
        if (form.checkValidity()) {

            var img = image === list_image || image === list_default_image ? null : image;

            let customList = {
                visible: visible,
                title: title,
                description: description,
                image: img
            }

            if (!update) {
                dispatch(actions.createCustomList(customList, (id) => 
                    history.push(`/lists/${id}/update?panel=1`), 
                errors => setBackendErrors(errors)));
            } else {
                foundList.image = image === list_image ? null : image === list_default_image ? "" : image;

                dispatch(actions.updateCustomList({...foundList, visible: visible, title: title, description: description}, () => {
                    setMadeChanges(false);
                    toast.success(<FormattedMessage id="project.list.update.updateToast"/>);
                }, errors => setBackendErrors(errors)));
            }
            
        } else {

            setBackendErrors(null);
            form.classList.add('was-validated');

        }

    }

    const handleImageChange = img => {
        var fileReader= new FileReader();
    
        fileReader.addEventListener("load", function(e) {
            setImage(e.target.result);
        }); 
        
        fileReader.readAsDataURL(img);

        canUpdate && setMadeChanges(true);
    }

    const handleMarkVisible = event => {
        setVisible(event);
        canUpdate && setMadeChanges(true);
    }

    return (
        <div className="center-element col-9">

            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>

            <form ref={node => form = node}
                className="needs-validation col shadow-container" noValidate onSubmit={e => handleSubmit(e)}>

                <div className="row p-3">
                    <div className="col-8">
                        <FormattedMessage id="project.list.create.title">
                            {
                                (msg) => <input type="text" id="userName" className="col form-control name-input ml-0 mb-3"
                                onChange={e => {setTitle(e.target.value); canUpdate && setMadeChanges(true)}}
                                autoFocus
                                value={title}
                                placeholder={msg}
                                required />
                            }
                        </FormattedMessage>
                    
                        <div className="w-100 row mr-3 ml-1">
                            <label htmlFor="description" className="col-12 title-label-color">
                                <FormattedMessage id="project.list.create.description"/>
                            </label>
                            <div className="col align-input-center">
                                <textarea className="form-control" id="description" rows="5" 
                                    value={description}
                                    maxLength="300"
                                    onChange={e => {setDescription(e.target.value); canUpdate && setMadeChanges(true)}}>
                                </textarea>
                            </div>
                        </div>

                        <div className="w-100 row mr-3 mr-5 mt-4 custom-checkbox justify-content-end">
                                <input type="checkbox" id="checkbox-language" className="custom-control-input" 
                                    checked={visible}
                                    onChange={e => handleMarkVisible(e.target.checked)} />
                                
                                <label htmlFor="checkbox-language" className="custom-control-label title-label-color">
                                    <FormattedMessage id="project.list.create.visible"/>
                                </label>
                        </div>

                        <div className="col ml-4 mt-2">
                            <span className="row w-100 title-img-type-color p-0 mb-3"><FormattedMessage id="project.people.create.update.images.portrait"/></span>
                            <span className="row w-100 mb-2"><FormattedMessage id="project.people.create.update.images.portrait.instructions"/></span>
                            <ul className="row w-100">
                                <li><FormattedMessage id="project.people.create.update.images.portrait.minRes"/></li>
                                <li><FormattedMessage id="project.people.create.update.images.portrait.ratio"/></li>
                            </ul>
                        </div>

                    </div>

                    <div className="col mt-3">

                        <div className="col update-portrait-container ">
                            <img id="portrait-img" src={image} className="list-update-image border-radius-15 center-element" alt="portrait img"/>
                            <input id="portraitFile" type="file" name="image" className="inputfile" 
                                onChange={e => handleImageChange(e.target.files[0])} 
                                accept="image/*"
                                multiple={false} />
                            <div className="row mt-3 w-95 center-element">
                                <div className="col-12 ">
                                    <label htmlFor="portraitFile" className="label-image" >
                                        <FormattedMessage id="project.users.profileEdit.selectImage"/>
                                    </label>
                                </div>
                                <div className="col-12">
                                    <span className="label-image red-label" onClick={() => {setImage(list_default_image); canUpdate && setMadeChanges(true);}}>
                                        <span><FormattedMessage id="project.users.profileEdit.deleteImage"/></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col title-submit-btn-container mt-4">
                        <div className="row justify-content-end">
                        {!update &&
                            <button type="button" className="btn btn-secondary mr-2" 
                                    onClick={() => {history.goBack()} }>
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                        }
                        <button type="submit" className="btn btn-success float-right">
                            <FormattedMessage id="project.titles.create.buttons.add"/>
                        </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );

}

CreateList.propTypes = {
    update: PropTypes.bool.isRequired,
    setMadeChanges: PropTypes.func.isRequired
}

export default CreateList;