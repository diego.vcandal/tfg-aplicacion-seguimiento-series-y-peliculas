/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FormattedMessage, FormattedDate, FormattedTime} from 'react-intl';
import {useParams} from 'react-router-dom';
import {useHistory, useLocation} from 'react-router-dom';

import * as actions from '../actions';
import * as selectors from '../selectors';

import users from '../../user';
import CustomListTitle from './Entity/CustomListTitle';
import { PageNotFound } from '../../common';

const CustomList = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation().pathname;

    const actualUser = useSelector(users.selectors.getUser);
    const isLoggedIn = useSelector(users.selectors.isLoggedIn);
    const foundList = useSelector(selectors.getFoundList);
    
    let canLike = foundList && !foundList.liked && isLoggedIn && foundList.userId !== actualUser.id;
    let canRemoveLike = foundList && foundList.liked && isLoggedIn && foundList.userId !== actualUser.id;

    const list_default_image = `${process.env.PUBLIC_URL}/images/list_default_image.png`;
    var list_image_path = `${process.env.REACT_APP_BACKEND_URL}/lists/`;

    const {id} = useParams();
    
    var user_default_image = `${process.env.PUBLIC_URL}/images/user_default_image.png`;
    var user_image_path = `${process.env.REACT_APP_BACKEND_URL}/users/`;

    useEffect(() => {

        const foundListId = Number(id);

        if (!Number.isNaN(foundListId))
            dispatch(actions.findCustomListById(foundListId));

        return () => dispatch(actions.clearFoundCustomList());

    }, [id, dispatch]);

    if (!foundList) {
        return <PageNotFound />;
    }

    const handleDelete = () => {
        dispatch(actions.deleteCustomList(id, () => history.push(`/users/${actualUser.id}/lists`)));
    };

    const handleAddRemoveLike = () => {

        if(!isLoggedIn || foundList.userId === actualUser.id)
            return;

        if (foundList.liked)
            dispatch(actions.removeLike(foundList.id))
        else
            dispatch(actions.addLike(foundList.id))

    }

    return (
        <div className="center-element w-90">
            <div className="row mt-4">
                <div className="col-2">
                    <img className="w-100 border-radius-20" alt="user profile image" 
                        src={list_image_path + `${foundList.id}/image`}
                        onError={e => {e.target.src = list_default_image}}/>
                </div>

                <div className="ml-4 col">

                    <div className="row person-username-title d-flex">
                        <div className="col-10 p-0">
                            <span className="line-height-initial">
                                {!foundList.visible &&
                                    <span className="custom-list-private-type-tag font-weight-600 mr-2">
                                        <FormattedMessage id='project.users.info.private'/>
                                    </span>
                                }
                                <span>
                                    {foundList.title}
                                </span>
                                    
                            </span>
                        </div>
                        <div className="col text-right">
                                {isLoggedIn && foundList.userId === actualUser.id && 
                                    <button className="btn btn-success btn-sm ml-2" type="button" title="Edit"
                                        onClick={() => history.push(location + '/update')}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square btn-icon" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </button>
                                }
                                {isLoggedIn && foundList.userId === actualUser.id && 
                                    <button className="btn btn-danger btn-sm ml-2" type="button" title="Delete"
                                        data-toggle="modal" data-target={"#delete-modal"}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill btn-icon" viewBox="0 0 16 16">
                                            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                        </svg>
                                    </button>
                                }
                        </div>
                    </div>

                    <div className="row mt-3">
                        <img className="image-user-list border-radius-15 link-pointer " alt="person portrait image"
                            onClick={() => history.push('/users/' + foundList.userId)} 
                            src={user_image_path + foundList.userId + "/image"}
                            onError={e => {e.target.src = user_default_image}}/>

                        <span className="link-bold link-pointer-text ml-2 mt-1"
                            onClick={() => history.push('/users/' + foundList.userId)}>
                                {foundList.userName}
                                {isLoggedIn && foundList.userId === actualUser.id && 
                                    <span>{" "}(<FormattedMessage id="project.titles.info.avgRating.you"/>)</span>
                                }
                        </span> 

                        <span className="ml-3 label-v-class">
                            <FormattedMessage id="project.users.info.lastUpdate"/>
                            {": "}
                            <FormattedTime value={new Date(foundList.lastUpdated)}/>
                            {" - "}
                            <FormattedDate value={new Date(foundList.lastUpdated)}/>
                        </span>
                    </div>

                    <div className="row search-title-description ml-1 mt-3">
                        <span className="mr-5">{
                            !foundList.description ?
                                <FormattedMessage id='project.search.findTitles.noDescription'/>
                        :
                            foundList.description
                        }</span>
                    </div>
                    
                    <div className="row">
                        <div className="col p-0 list-search-like-container align-div-bottom-absolute ml-2">
                            <div className="row w-95 ">
                                <div className={(
                                        !foundList.liked ?
                                            canLike ? "add-like" : ""
                                        :
                                            canRemoveLike ? "liked" : ""
                                    )}  onClick={() => handleAddRemoveLike()} >

                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart-fill middle-v-button-icon" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                    </svg>

                                    {!foundList.liked && canLike && <span className="ml-2"><FormattedMessage id="project.global.buttons.likes.add"/></span>}
                                    {foundList.liked && canRemoveLike && <span className="ml-2 info"><FormattedMessage id="project.global.buttons.likes.liked"/></span>}
                                    {foundList.liked && canRemoveLike && <span className="ml-2 delete-like"><FormattedMessage id="project.global.buttons.likes.remove"/></span>}
                                </div>

                                <span className="font-weight-600 ml-2"> {"(" + foundList.likeCount + ")"}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="delete-modal" className="modal fade" tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title"><FormattedMessage id="project.global.dialog.adminOptions.delete.Title"/></h5>
                        </div>
                        <div className="modal-body">
                            <p><FormattedMessage id="project.global.dialog.adminOptions.delete.Body"/></p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" 
                                data-dismiss="modal">
                                <FormattedMessage id="project.global.buttons.cancel"/>
                            </button>
                            <button type="button" className="btn btn-danger" 
                                data-dismiss="modal"
                                onClick={() => handleDelete()}>
                                <FormattedMessage id="project.global.buttons.ok"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        
            <br /><br />

            <div className="row custom-list-elements-title center-element ml-0">
                <FormattedMessage id='project.list.update.menu.elements'/>
                {" (" + foundList.titles.length + ")"}
            </div>
            
            <br />

            {foundList.titles.length > 0 && 
                <CustomListTitle titles={foundList.titles} />
            }

        </div>
    );

}

export default CustomList;