export const CREATE_CUSTOM_LIST_COMPLETED = "project/lists/createCustomListCompleted";
export const FIND_CUSTOM_LIST_COMPLETED = "project/lists/findCustomListByIdCompleted";
export const CLEAR_FOUND_CUSTOM_LIST = "project/lists/clearFoundCustomList";
export const FIND_CUSTOM_LIST_TITLES_SEARCH_COMPLETED = "project/lists/findCustomListTitlesSearchCompleted";
export const ADD_TITLE_TO_LIST_COMPLETED = "project/lists/addTitleToListCompleted";
export const UPDATE_CUSTOM_LIST_ORDER_COMPLETED = "project/lists/updateCustomListOrderCompleted";
export const UPDATE_CUSTOM_LIST_COMPLETED = "project/lists/updateCustomListCompleted";
export const DELETE_CUSTOM_LIST_COMPLETED = "project/lists/deleteCustomListCompleted";
export const DELETE_TITLE_TO_LIST_COMPLETED = "project/lists/deleteFromListCompleted";

export const ADD_LIKE_TO_LIST_COMPLETED = "project/lists/addedLikeToListCompleted";
export const REMOVE_LIKE_FROM_LIST_COMPLETED = "project/lists/removedLikeFromListCompleted";