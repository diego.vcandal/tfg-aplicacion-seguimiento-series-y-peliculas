import {combineReducers} from 'redux';

import * as actionTypes from './actionTypes';

const initialState = {
    foundList: null
};

const foundList = (state = initialState.foundList, action) => {

    switch (action.type) {

        case actionTypes.FIND_CUSTOM_LIST_COMPLETED:
            return action.foundList;
            
        case actionTypes.CLEAR_FOUND_CUSTOM_LIST:
            return initialState.foundList;

        case actionTypes.UPDATE_CUSTOM_LIST_ORDER_COMPLETED:
            return {...state, titles: action.newList};

        case actionTypes.UPDATE_CUSTOM_LIST_COMPLETED:
            return action.newList;

        case actionTypes.DELETE_CUSTOM_LIST_COMPLETED:
            return initialState.foundList;
        
        case actionTypes.ADD_LIKE_TO_LIST_COMPLETED:
            return {...state, likeCount: state.likeCount + 1, liked: true};

        case actionTypes.REMOVE_LIKE_FROM_LIST_COMPLETED:
            return {...state, likeCount: state.likeCount - 1, liked: false};

        default:
            return state;

    }

}

const reducer = combineReducers({
    foundList,
});

export default reducer;
