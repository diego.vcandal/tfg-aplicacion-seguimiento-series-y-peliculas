import React from 'react';
import { FormattedMessage } from 'react-intl';

const PageNotFound = () => {

    return (

        <div className="not-found-error-container mt-4">
            <h2><FormattedMessage id="project.error.NotFound.title"/></h2>
            <p><FormattedMessage id="project.error.NotFound.description"/></p>
        </div>

    );
    
};

export default PageNotFound;